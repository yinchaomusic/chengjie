<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">



</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>
<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>域名融资</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="g-content u-cls">
	<div class="m-content">



		<section>
			<div class="container nrye">
				<div class="row">



					<div class="col-sm-2">
						<!--sidebar-menu-->
						<!--sidebar-menu-->
						<div id="sidebar">
							<a href="#" class="submenu_bar  visible-xs-block">
								<ol class="breadcrumb">
									<li><i class="iconfont submenu_d"></i></li>
									<li>首页</li>
									<li class="active">我的账户</li>
								</ol>
							</a>
							<ul style="display: block;"><li class="submenu"><a href="{pigcms{:U('Financing/index')}"><i class="iconfont">&#xe613;</i><span>我的账户</span></a> </li>
								<li class="submenu  active open">
									<a href="{pigcms{:U('Investment/lend')}"><i class="iconfont "></i>
										<span>我要理财</span><span class="label  visible-xs-block">3</span></a>
									<ul>
										<li ><a href="{pigcms{:U('Investment/lend')}">我要借出</a></li>
										<li ><a href="{pigcms{:U('Investment/lendList')}">借出列表</a></li>
										<li class="active"><a href="{pigcms{:U('Investment/automatic')}">自动投标</a></li>
										<li><a href="{pigcms{:U('Investment/keepInvest')}">回款续投</a></li>
									</ul>
								</li>
								<li class="submenu "><a href="{pigcms{:U('Loan/borrow')}"><i class="iconfont "></i><span>我要借款</span><span class="label  visible-xs-block">3</span></a>
									<ul>
										<li><a href="{pigcms{:U('Loan/borrow')}">我要借入</a></li>
										<li><a href="{pigcms{:U('Loan/borrowList')}">借入列表</a></li>
										<li><a href="{pigcms{:U('Loan/pledge')}">质押管理</a></li>
									</ul>
								</li>
								<li class="submenu "><a href="#"><i class="iconfont "></i><span>资金管理</span><span class="label  visible-xs-block">6</span></a>
									<ul>
										<li><a href="{pigcms{:U('Account/Account')}">账户充值</a></li>
										<li><a href="{pigcms{:U('Account/withdrawal')}">账户提现</a></li>
										<li><a href="{pigcms{:U('Account/withdrawal')}">提现银行</a></li>
										<li><a href="{pigcms{:U('Account/record')}">资金记录</a></li>
										<li><a href="{pigcms{:U('Fund/freezes')}">冻结记录</a></li>
									</ul>
								</li>
								<li class="submenu "><a href="#"><i class="iconfont "></i><span>账户管理</span><span class="label  visible-xs-block">7</span></a>
									<ul>
										<li ><a href="{pigcms{:U('Profile/update')}">个人信息</a></li>
										<li><a href="{pigcms{:U('Profile/avatar')}">上传头像</a></li>
										<li><a href="{pigcms{:U('Profile/IdCard')}">身份认证</a></li>
										<li><a href="{pigcms{:U('Account/updatepwd')}">登录密码</a></li>
										<li><a href="{pigcms{:U('Profile/transactionPassword')}">交易密码</a></li>
										<li><a href="{pigcms{:U('Account/bind')}">绑定手机</a></li>
										<!--											<li><a href="/Member/InfoCenter">站内信</a></li>-->
									</ul>
								</li>
							</ul>
						</div>
						<!--sidebar-menu end-->
					</div>

					<div class="col-sm-10">
						<div class="container_page padding-sm-none">
							<section>
								<h2>自动投标</h2>
								<div class="tab_table page_min_h zdtb">
									<if condition="$count_user_times lt 5">
									<a href="{pigcms{:U('Investment/createAutomatic')}" class="btn btn-primary3 btn-warning btn-xs-block">+ 添加自动投标</a>
									</if>
									<ul class="nav nav-tabs">
										<li role="presentation" <if condition="$state eq 1"> class="active" </if> >
											<a href="{pigcms{:U('Investment/automatic',array('invalid'=>1))}">有效配置</a>
										</li>
										<li role="presentation" <if condition="$state eq 2"> class="active" </if>>
											<a href="{pigcms{:U('Investment/automatic',array('invalid'=>2))}">失效配置</a>
										</li>
									</ul>
									<div class="table-responsive">
										<table class=" table table-bordered table-striped table-hover ">
											<thead>
											<tr>
												<th>投标总额</th>
												<th>可用金额</th>
												<th>已投资金额</th>
												<th>期数</th>
												<th><if condition="$state eq 1">最低利率<elseif condition="$state eq 2" />利率</if></th>
												<th>类型</th>
												<th>操作</th>
											</tr>
											</thead>
											<tbody class="text-center">
											<if condition="is_array($automaticList)">
												<volist name="automaticList" id="vo">
												<tr>
													<td>￥{pigcms{$vo.total_amount}</td>
													<td>￥{pigcms{$vo.surplus_meoney}</td>
													<td>￥{pigcms{$vo.has_meoney}</td>
													<td>{pigcms{$vo.LoanCycle|default=0} 天</td>
													<td><if condition="$state eq 1">{pigcms{$vo.MinRate}%<elseif condition="$state eq 2" /> {pigcms{$vo.Rate|default=0}%</if> </td>
													<td><if condition="$vo['autotype'] eq 1">自动投标<elseif condition="2"/>回款续投</if></td>
													<td><a href="javascript:void(0);" id="nc_{pigcms{$vo.aid}" onclick="automatic('{pigcms{$vo.aid}',1);return false;">删除</a> </td>
												</tr>
												</volist>
											<else/>
											<!--暂无记录-->
											<tr>
												<td colspan="7">暂无记录</td>
											</tr>
											<!--暂无记录-->
											</if>
											</tbody>
										</table>
									</div>
								</div>

							</section>

						</div>
					</div>
				</div>
			</div>
		</section>



	</div>
</div>
<include file="Public:footer"/>

<script src="{pigcms{$static_path}js/unslider.js"></script>
<script src="{pigcms{$static_path}js/bootstrap.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/rz_common.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/matrix.js"></script>
<script type="text/javascript">

	var submit_check="{pigcms{:U('Investment/automatic')}";
	function automatic(id,type) {
		var title,text,configbutton;
		if(type ==1){
			title = "你确定要删除么？";
			text =  "若删除自动投标配置，您账户内对应的冻结金额将自动转换为可用余额";
			configbutton = "删除";
		}else if(type == 2){
			title = "你确定要发布么？";
			text = "如在设定的借款周期内满标，是无法取消，是否需要发布？"
			configbutton = "我要发布"
		}

		swal({
			title: title,
			text: text,
			type: "info",
			showCancelButton: true,
			cancelButtonText:"取消",
			confirmButtonText:configbutton,
			closeOnConfirm: false,
			showLoaderOnConfirm: true,
		}, function(){
			//setTimeout(function(){
			var nowcheckdomain = $('#nc_'+id);
			nowcheckdomain.html("<img src='{pigcms{$static_path}images/index/loading.gif' width='20' height='20' />");
			$.post(submit_check, {'id': id,'type':type}, function (result) {
				result = $.parseJSON(result);
				if (result) {
					if (result.error == 0) {
						nowcheckdomain.parent().parent().remove();
						swal(result.msg);
					} else {
						swal(result.msg);return false;
					}
				} else {
					swal(result.msg);return false;
				}
			});
			//}, 2000);
		});
	}


	$(function () {
		var $s_area = $("#filter-specs");
		$(".s_bar").click(function () {
			$s_area.addClass("box-filter-expanded");
			$("body").addClass("phone_s");


		});
		$(".close_s").click(function () {
			$s_area.removeClass("box-filter-expanded");
			$("body").removeClass("phone_s");

		});

		if (/*navigator.userAgent.match(/mobile/i) && */$(window).width() < 768) {
			// alert("手机")

			$("#filter-specs dd").height($(window).height() - 100);
			$(window).resize(function () {
				$("#filter-specs dd").height($(window).height() - 100);
			});
			/*选项卡*/
			var $tab_b = $(".tab_s_box dt"), $tab_c = $(".tab_s_box dd");
			$tab_b.click(function () {
				if (!$(this).hasClass("active")) {
					$(this).siblings("dt").removeClass("active");
					$(this).addClass("active");
					$tab_c.removeClass("active");
					$(this).next("dd").addClass("active");
				};
			});
		} else {
			$("#filter-specs dd").height("auto");
		};



	});


</script>


</body>
</html>