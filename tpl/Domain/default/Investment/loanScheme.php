<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">



</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>
<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>域名融资</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="g-content u-cls">
	<div class="m-content">


		<section class="">
			<div class="container jkxq">
				<div class="row">



					<div class="col-sm-2">
						<!--sidebar-menu-->
						 
						<!--sidebar-menu end-->
					</div>

				</div>
				<div class="row">
					<ol class="breadcrumb hidden-xs">
						<li><a href="/">首页</a></li>
						<li><a href="{pigcms{:U('Investment/lend')}">我要理财</a></li>
						<li class="active">{pigcms{$borrowsList.did}</li>
					</ol>
					<div class="col-sm-12 jkxq_info">
						<div class="col-sm-8 left">
							<div class="info">
								<h2>{pigcms{$borrowsList.dname} 贷款 ￥{pigcms{$borrowsList.Amount|number_format}元</h2>
								<table>
									<tbody>
									<tr>
										<td>
											<h3>年利率</h3>
											<span>{pigcms{$borrowsList.Rate}%</span>
                                        <span class="small">
	       <if condition="$borrowsList['LoanCycleDay'] neq 0">天利率<elseif condition="$borrowsList['LoanCycleMonth'] neq 0"/>月利率</if>
	                                         <br />
           <if condition="$borrowsList['LoanCycleDay'] neq 0">{pigcms{$borrowsList.tianlx}%<elseif  condition="$borrowsList['LoanCycleMonth'] neq 0"/>{pigcms{$borrowsList.yuelx}%</if>
                                        </span>
										</td>
										<td>
											<h3>借款金额</h3>
											￥{pigcms{$borrowsList.Amount|number_format}元
										</td>
										<td>
											<h3>借款期限</h3>
											<if condition="$borrowsList['LoanCycleDay'] neq 0">{pigcms{$borrowsList.LoanCycleDay}天<elseif condition="$borrowsList['LoanCycleMonth'] neq 0"/>{pigcms{$borrowsList.LoanCycleMonth} 个月</if>
										</td>
										<td>
											<h3>还款方式</h3>
											每月还息
										</td>
									</tr>
									</tbody>
								</table>
								<ul>
									<li>
										<div class="labels">投资进度：</div>
										<div class="progress_area">
											<div class="progress">
												<div class="progress-bar progress-bar-success progress-bar-striped active " role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {pigcms{$borrowsList.invesing}%;">
												</div>
											</div>
										</div>
										<span class="progress_info">{pigcms{$borrowsList.invesing}%</span>
										<div class="clear"></div>
									</li>
									<li>
										<div class="labels">发标时间：</div>
										{pigcms{$borrowsList.fabiaoTime|date='Y-m-d H:i:s',###} &nbsp;&nbsp; 剩余时间：{pigcms{$borrowsList.surplus_date}天
										<div class="clear"></div>
									</li>
									<li>
										<div class="labels">还需：</div>
										<span class="amount-pay-out">￥{pigcms{$borrowsList.needAmount}</span>
										<div class="clear"></div>
									</li>
									<li>
										<div class="labels">借款描述：</div>
										<div class="description">
											{pigcms{$borrowsList.Description}
										</div>
										<div class="clear"></div>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-sm-4 right">
							<div class="info">
								<p>
									当前状态：<span class="state"><if condition="$borrowsList['status'] eq 1"><span class="font_success">正在投标</span><elseif condition="$borrowsList['status'] eq 2"/><span class="font_default">等待还款</span><elseif condition="$borrowsList['status'] eq 3"/><span class="font_default">还款结束</span><elseif condition="$borrowsList['status'] eq 4"/><span class="font_default">已过期</span><elseif condition="$borrowsList['status'] eq 5"/><span class="font_default">等待审核</span><elseif condition="$borrowsList['status'] eq 6"/><span class="font_success">审核通过</span><elseif condition="$borrowsList['status'] eq 7"/><span class="font_warning"><a href="javascript:void(0);" title="{pigcms{$vo.intro}">申请拒绝</a></span><elseif condition="$borrowsList['status'] eq 9"/><span class="font_warning">已经满标</span></if></span><br />
									投资<span class="amount-pay-out">100</span>元，
									期限<span class="font_c_blue"><if condition="$borrowsList['LoanCycleDay'] neq 0">{pigcms{$borrowsList.LoanCycleDay}天<elseif condition="$borrowsList['LoanCycleMonth'] neq 0"/>{pigcms{$borrowsList.LoanCycleMonth} 个月</if></span>，
									可获得利息收益
                            <span class="amount-pay-in">￥<if condition="$borrowsList['LoanCycleDay'] neq 0">{pigcms{$borrowsList.tianhlx}<elseif condition="$borrowsList['LoanCycleMonth'] neq 0"/>{pigcms{$borrowsList.yuehlx}</if></span>
								</p>
								<if condition="$borrowsList['status'] eq 1">
				 <a href="{pigcms{:U('Investment/investing',array('dkid'=>$borrowsList['bid']))}" class="btn btn-primary2 btn-xs-block">立即投资</a>
									</if>
								<p class=" font_c_grey">最小投资额：¥100.00    总投资数：{pigcms{$borrowsList.investor_tatol}</p>
							</div>
						</div>
						<div class="double_line"></div>
						<div class="user_info col-sm-12">
							<div class="row">
								<div class="col-sm-4">
									<div class="user_photo">
										<image <if condition="$userinfo['avatar']">src="/upload/avatar/{pigcms{$userinfo['avatar']}" <else /> src="{pigcms{$static_path}/images/account/default_avatar.png"
										</if>  />
									</div>
									<p>
										姓名：<strong>{pigcms{$userinfo.nickname}</strong><br />
										性别：<if condition="$userinfo['sex'] eq 1">男<elseif condition="$userinfo['sex'] eq 2"/>女<else/>保密</if> <br />
										年龄：{pigcms{$userinfo.birthday} <br />
										地区： {pigcms{$userinfo.youaddress}
									</p>

								</div>
								<div class="col-sm-4">
									<p>
										共发布贷款数：{pigcms{$pledgeList.borrow_total}<br />
										是否有逾期： <strong><if condition="$pledgeList['is_overdue']">有<else/> 无</if> </strong><br />
										认证信息：<strong>实名认证</strong><br />
										注册日期：{pigcms{$userinfo.add_time|date='Y-m-d',###}
									</p>
								</div>
								<div class="col-sm-4">
									<p>
										质押域名：{pigcms{$pledgeList.pledge_number}个<br />
										授信总额：￥{pigcms{$pledgeList.credit_all|number_format}元<br />
										已用授信：￥{pigcms{$pledgeList.brrowing_total|number_format}元<br />
										可用授信：<strong>￥{pigcms{$pledgeList.credit_now|number_format}元</strong><br />
										借款荣誉：<a <if condition="$pledgeList['honor'] eq 1">class="level level_1"
											<elseif condition="$pledgeList['honor'] eq 2"/>class="level level_2"
											<elseif condition="$pledgeList['honor'] eq 3"/>class="level level_3"
										</if>
										 href="{pigcms{:U('Financing/level')}">
											<if condition="$pledgeList['honor'] eq 1">普通会员
												<elseif condition="$pledgeList['honor'] eq 2"/>钻石会员
												<elseif condition="$pledgeList['honor'] eq 3"/>皇冠会员
											</if>
											</a>
									</p>

								</div>
							</div>
						</div>
					</div>
					<div class="table_list">
						<h2>质押域名</h2>
						<div class="table-responsive">
							<table class=" table table-bordered table-striped table-hover">
								<thead>
								<tr>
									<th>域名</th>
									<th>授信总额</th>
									<th>总使用比例</th>
								</tr>
								</thead>
								<tbody class="text-center">
								<volist name="pledgeDomainList" id="vo">
								<tr>
									<td>
										{pigcms{$vo.domain}
										<a href="{pigcms{:U('Whois/index',array('keyword'=>$vo['domain']))}" target="_blank">[Whois查询]</a>
									</td>
									<td>￥{pigcms{$vo.credit_total|number_format}元</td>
									<td>100%</td>
								</tr>
								</volist>
								</tbody>
							</table>
						</div>
						<h2>还款记录</h2>
						<table class=" table table-bordered table-striped table-hover">
							<thead>
							<tr>
								<th>期数</th>
								<th>还款本息</th>
								<th>应还款时间</th>
								<th>还款时间</th>
								<th>状态</th>
							</tr>
							</thead>
							<tbody class="text-center">
							<if condition="is_array($payment_history)">
								<volist name="payment_history" id="vo">
									<tr>
										<td>第{pigcms{$vo['LoanCycle']}期</td>
										<td>￥{pigcms{$vo.hk_benxi}</td>
										<td>{pigcms{$vo.yh_time}</td>
										<td>{pigcms{$vo.hk_time|date='Y-m-d',###}</td>
										<td><if condition="$vo['status'] eq 1">已经还清<elseif condition="$vo['status'] eq 2"/>逾期还款<elseif condition="$vo['status'] eq 0"/>正在还款</if></td>
									</tr>
								</volist>
								<else/>
								<tr><td colspan="5">暂无记录</td></tr>
							</if>


							</tbody>
						</table>
						<h2>还款信用</h2>
						<table class=" table table-bordered table-striped table-hover">
							<thead>
							<tr>
								<th>还款状态</th>
								<th>最近一周</th>
								<th>最近1月</th>
								<th>最近6月</th>
								<th>总计</th>
							</tr>
							</thead>
							<tbody class="text-center">
							<tr>
								<td class="font_c_orange">逾期</td>
								<td class="font_c_orange">0</td>
								<td class="font_c_orange">0</td>
								<td class="font_c_orange">0</td>
								<td class="font_c_orange">0</td>
							</tr>
							<tr>
								<td class="font_c_green">提前</td>
								<td class="font_c_green">0</td>
								<td class="font_c_green">18</td>
								<td class="font_c_green">95</td>
								<td class="font_c_green">134</td>
							</tr>
							<tr>
								<td>正常</td>
								<td>15</td>
								<td>67</td>
								<td>416</td>
								<td>610</td>
							</tr>

							</tbody>
						</table>
						<h2>投资记录</h2>
						<div class="table-responsive">
							<table class=" table table-bordered table-striped table-hover">
								<thead>
								<tr>
									<th>投资人</th>
									<th>投资金额</th>
									<th>投资时间</th>

									<th>状态</th>
								</tr>
								</thead>
								<tbody class="text-center">
								<if condition="is_array($investmentList)">
									<volist name="investmentList" id="vo">
								<tr>
									<td>
										{pigcms{:msubstr($vo['iname'],0,1,0)}**
									</td>
									<td>￥{pigcms{$vo.Amount}</td>
									<td>{pigcms{$vo.itime|date='Y-m-d',###}</td>

									<td><if condition="$vo['status'] eq 1">正在投标<elseif condition="$vo['status'] eq 2"/>正在收款 </if></td>
								</tr>
									</volist>
								<else/>
									<tr><td colspan="4"></td></tr>
								</if>
								</tbody>
							</table>
						</div>
						<h2>认证信息</h2>
						<div class="table-responsive">
							<table class=" table table-bordered table-striped table-hover">
								<thead>
								<tr>
									<th>认证类型</th>
									<th>认证情况</th>

								</tr>
								</thead>
								<tbody class="text-center">
								<tr>
									<td>手机认证</td>
									<td>
										<if condition="$userinfo['is_check_phone']">
											<span class="pass">已认证</span>
										<else/>
											<span  >未认证</span>
										</if>

									</td>
								</tr>
								<tr>
									<td>实名认证</td>
									<td>
										<if condition="$userinfo['idcard']">
											<span class="pass">已认证</span>
											<else/>
											<span  >未认证</span>
										</if>
									</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>



	</div>
</div>
<include file="Public:footer"/>
<include file="Public:account_footer"/>
<script src="{pigcms{$static_path}js/unslider.js"></script>
<script src="{pigcms{$static_path}js/bootstrap.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/rz_common.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/matrix.js"></script>



</body>
</html>