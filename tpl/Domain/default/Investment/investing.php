<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">

	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">


</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>
<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>域名融资</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="g-content u-cls">
	<div class="m-content">


		<section>
			<div class="container nrye">
				<div class="row">

					<div class="col-sm-2">
						<!--sidebar-menu-->
						<div id="sidebar">
							<a href="#" class="submenu_bar  visible-xs-block">
								<ol class="breadcrumb">
									<li><i class="iconfont submenu_d"></i></li>
									<li>首页</li>
									<li class="active">我的账户</li>
								</ol>
							</a>
							<ul style="display: block;"><li class="submenu"><a href="{pigcms{:U('Financing/index')}"><i class="iconfont">&#xe613;</i><span>我的账户</span></a> </li>
								<li class="submenu  active open">
									<a href="{pigcms{:U('Investment/lend')}"><i class="iconfont "></i>
										<span>我要理财</span><span class="label  visible-xs-block">3</span></a>
									<ul>
										<li ><a href="{pigcms{:U('Investment/lend')}">我要借出</a></li>
										<li><a href="{pigcms{:U('Investment/lendList')}">借出列表</a></li>
										<li><a href="{pigcms{:U('Investment/automatic')}">自动投标</a></li>
										<li><a href="{pigcms{:U('Investment/keepInvest')}">回款续投</a></li>
									</ul>
								</li>
								<li class="submenu "><a href="{pigcms{:U('Loan/borrow')}"><i class="iconfont "></i><span>我要借款</span><span class="label  visible-xs-block">3</span></a>
									<ul>
										<li><a href="{pigcms{:U('Loan/borrow')}">我要借入</a></li>
										<li><a href="{pigcms{:U('Loan/borrowList')}">借入列表</a></li>
										<li><a href="{pigcms{:U('Loan/pledge')}">质押管理</a></li>
									</ul>
								</li>
								<li class="submenu "><a href="#"><i class="iconfont "></i><span>资金管理</span><span class="label  visible-xs-block">6</span></a>
									<ul>
										<li><a href="{pigcms{:U('Account/Account')}">账户充值</a></li>
										<li><a href="{pigcms{:U('Account/withdrawal')}">账户提现</a></li>
										<li><a href="{pigcms{:U('Account/withdrawal')}">提现银行</a></li>
										<li><a href="{pigcms{:U('Account/record')}">资金记录</a></li>
										<li><a href="{pigcms{:U('Fund/freezes')}">冻结记录</a></li>
									</ul>
								</li>
								<li class="submenu "><a href="#"><i class="iconfont "></i><span>账户管理</span><span class="label  visible-xs-block">7</span></a>
									<ul>
										<li ><a href="{pigcms{:U('Profile/update')}">个人信息</a></li>
										<li><a href="{pigcms{:U('Profile/avatar')}">上传头像</a></li>
										<li><a href="{pigcms{:U('Profile/IdCard')}">身份认证</a></li>
										<li><a href="{pigcms{:U('Account/updatepwd')}">登录密码</a></li>
										<li><a href="{pigcms{:U('Profile/transactionPassword')}">交易密码</a></li>
										<li><a href="{pigcms{:U('Account/bind')}">绑定手机</a></li>
										<!--											<li><a href="/Member/InfoCenter">站内信</a></li>-->
									</ul>
								</li>
							</ul>
						</div>
						<!--sidebar-menu end-->
					</div>
					<form action="{pigcms{:U('Investment/investing_now')}" id="investingForm" method="post" novalidate="true">
						<input type="hidden" name="bid" value="{pigcms{$borrowsInfo.bid}">
					<div class="col-sm-10">
						<div class="container_page  padding-sm-none">
							<section>
								<h2>我要借出</h2>
								<div class="wylc_jc_area">
									<div class="wylc_jc">
										<strong>您的账户余额<span class="amount-pay-in" id="ye" data-amount="{pigcms{$user_session['now_money']}">￥{pigcms{:number_format($user_session['now_money'])}</span><br />
											该笔贷款还需<span class="font_c_r" id="hc" data-amount="{pigcms{$borrowsInfo.needAmount}">{pigcms{$borrowsInfo.needAmount}</span>
										</strong>
										<div class="form-inline">
											<div class="form-group">
												<label for="Amount"><i>*</i>输入金额：</label>
												<input class="form-control" id="Amount" name="Amount" type="number" value="0" />
												<span id="Amountdx">元</span>
												<span class="form-control-tips"><i class="iconfont">&#xe60a;</i>投资金额必须是100的整数倍</span>
											</div>
											<div class="table_area">
												<table class=" table table-bordered text-center">
													<tbody>
													<tr>
														<th scope="row">年利率</th>
														<td >
															{pigcms{$borrowsInfo.Rate}%
															<input id="Rate" name="Rate" type="hidden" value="{pigcms{$borrowsInfo.RateFloat}" />
															<input id="LoanCycle" name="LoanCycle" type="hidden" value="{pigcms{$borrowsInfo.LoanCycle}" />
														</td>
													</tr>
													<tr>
														<th scope="row">每期利息收益</th>
														<td id="nsy"></td>
													</tr>
													</tbody>
												</table>
											</div>
											<input type="submit" class="btn btn-primary2 btn-warning btn-xs-block" value="确定投资">
										</div>
									</div>
									<p class="info-box">
										<span>投资金额条件：</span><br />
										1.投资金额必须为最低投资金额的整数倍，如果不限最低投资则必须为100的整数倍！<br />
										2.最低投资100.00元，最高投资不限<br />
										3.资金来源明确合法<br />
									</p>
									<p class="info-p">
										如借入者未按合同约定的时间还款，以域名质押物清偿债务(我们在授信域名时)，如实际价款不足，投资人需自行承担该风险与损失。
										诚借在给某域名评估授信额度时，通常会以此域名市场折扣价来进行授信。例如A域名市场价值在20万元左右，根据一定的折扣后，
										A域名的授信额度可能为10万元-12万元左右。
									</p>
								</div>
							</section>
						</div>
					</div>
					</form>

				</div>
			</div>
		</section>


	</div>
</div>
<include file="Public:footer"/>
<script src="{pigcms{$static_path}js/unslider.js"></script>
<script src="{pigcms{$static_path}js/bootstrap.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/rz_common.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/matrix.js"></script>
<script type="text/javascript">

//	formSubmitSuccess = function () {
//		window.location = "/Member/Investment/Index";
//	}

	$(function () {
		var nsy,
			nq = $("#LoanCycle").val() > 30 ? 30 :   $("#LoanCycle").val();
			ye = $("#ye").attr("data-amount") * 1,
			hc = $("#hc").attr("data-amount") * 1,
			ze = ye > hc ? hc : ye;
			ze = parseInt(ze / 100) * 100

		var calculate = function () {
			var amount = $("#Amount");
			if (amount.val() > 100) {
				amount.val(parseInt(amount.val() / 100) * 100);

			} else if (amount.val() != "") {
				amount.val(100);
			}
			$("#Amountdx").text(NoToChinese(amount.val()) + "元");

			$("#nsy").text("￥" + ($("#Rate").val() * amount.val() / 360 * nq).toFixed(4).replace(/(.*)\d\d/, "$1"));
		}

		$("#Amount").on("keyup", function () {
			if ($(this).val() > ze) {
				$(this).val(ze);
			}

		});
		$("#Amount").on("blur", function () {
			calculate();
		});

		calculate();

		var formSubmitSuccess;
		var formSubmitBefore;
		$('#investingForm').submit(function () {
			var $form = $("form");
			var url = $form.attr("action");
			var submitButtons = $form.find(":submit");
			var texts = $form.find(":text,input[type=date],input[type=number],input[type=tel],input[type=email]");
			var hiddens = $form.find(":hidden");
			var data = {};

			//hiddens
			var hiddenItems = {};
			for (var i = 0; i < hiddens.length; i++) {
				var name = hiddens.eq(i).attr("name");
				var values = hiddenItems[name];
				if (!values) {
					values = [];
				}
				values.push(hiddens.eq(i).val());
				hiddenItems[name] = values;
			}
			for (var item in hiddenItems) {
				data[item] = hiddenItems[item].join(',');
			}
			//texts
			var textItems = {};
			for (var i = 0; i < texts.length; i++) {
				var name = texts.eq(i).attr("name");
				var values = textItems[name];
				if (!values) {
					values = [];
				}
				values.push(texts.eq(i).val());
				textItems[name] = values;
			}
			for (var item in textItems) {
				data[item] = textItems[item].join(',');
			}

			if (formSubmitBefore) {
				var isBrack = formSubmitBefore(url, data);
				if (!isBrack)
					return false;
			}

			data['nsy']    = $("#nsy").text().replace(/￥/ig,'');//每期利息收益
			data['Amountdx']   = $("#Amountdx").text();//投资中文数字

			if(  $("#nsy").text() == ''|| $("#Amount").val() =='' || $("#Amount").val() =='0'  )
			{
				swal("错误！","请认真填写所需要的信息",'error');
				submitButtons.removeAttr("disabled");
				return false;
			}

			submitButtons.attr("disabled", "true");

			swal({
				title: "确定投资该贷款?",
				text: "如果是第一次投资，请务必确认应设置交易密码! 以下是您的投资信息【投资：<span style='color:#FFA200'>"+$("#Amountdx").text()+"<\/span> 每期利息收益：：<span style='color:#FFA200'>"+$("#nsy").text()+"<\/span>】",
				html: true,
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "我已经设置",
				cancelButtonText: "在哪里设置？",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm){
				if (isConfirm) {
					swal({
						title: "投资确认!",
						text: "请您认真输入您的交易密码:",
						type: "input",
						inputType:"password",
						showCancelButton: true,
						cancelButtonText: "取消",
						closeOnConfirm: false,
						animation: "slide-from-top",
						inputPlaceholder: "在这里输入您的交易密码",
						showLoaderOnConfirm:true
					}, function(inputValue){
						if (inputValue === false) {submitButtons.removeAttr("disabled");return false;}
						if (inputValue === "") {
							swal.showInputError("请输入正确的交易密码!");
							submitButtons.removeAttr("disabled");
							return false
						}
						data['trade_password']   = inputValue;
						$.post(url,
							data,
							function (result) {
								//result = $.parseJSON(result);
								if (result) {

									if (result.error == -1) {
										swal(result.title,result.msg,'error');
										if(result.reloadurl){
											setTimeout(function(){    window.location = result.reloadurl; }, 3000);
										}
										submitButtons.removeAttr("disabled");
										return;
									}
									if(result.error == 0){
										//swal(result.title,result.msg,'success');
										swal({   title: result.title,   text: result.msg, timer:3000,  imageUrl: "images/thumbs-up.jpg",showCancelButton: false,confirmButtonColor: "#FFA200",confirmButtonText:"OK" }, function (isConfirm) {
											window.location = result.reloadurl;
										});
										submitButtons.removeAttr("disabled");
									}
								}

								if (formSubmitSuccess) {
									formSubmitSuccess($form, result);
								} else {
									//window.location = window.location;
								}
							});
						return false;
					});
				} else {
					swal("稍等", "正在带您去设置交易密码 :)", "error");
					window.location="{pigcms{:U('Profile/transactionPassword')}";
				}
			});
			return false;
		});

	});

</script>


</body>
</html>