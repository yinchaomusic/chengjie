<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">



</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>
<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>域名融资</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="g-content u-cls">
	<div class="m-content">


		<section>
			<div class="container nrye">
				<div class="row">

					<div class="col-sm-2">
						<!--sidebar-menu-->
						<div id="sidebar">
							<a href="#" class="submenu_bar  visible-xs-block">
								<ol class="breadcrumb">
									<li><i class="iconfont submenu_d"></i></li>
									<li>首页</li>
									<li class="active">我的账户</li>
								</ol>
							</a>
							<ul style="display: block;"><li class="submenu"><a href="{pigcms{:U('Financing/index')}"><i class="iconfont">&#xe613;</i><span>我的账户</span></a> </li>
								<li class="submenu  active open">
									<a href="{pigcms{:U('Investment/lend')}"><i class="iconfont "></i>
										<span>我要理财</span><span class="label  visible-xs-block">3</span></a>
									<ul>
										<li class="active"><a href="{pigcms{:U('Investment/lend')}">我要借出</a></li>
										<li><a href="{pigcms{:U('Investment/lendList')}">借出列表</a></li>
										<li><a href="{pigcms{:U('Investment/automatic')}">自动投标</a></li>
										<li><a href="{pigcms{:U('Investment/keepInvest')}">回款续投</a></li>
									</ul>
								</li>
								<li class="submenu "><a href="{pigcms{:U('Loan/borrow')}"><i class="iconfont "></i><span>我要借款</span><span class="label  visible-xs-block">3</span></a>
									<ul>
										<li><a href="{pigcms{:U('Loan/borrow')}">我要借入</a></li>
										<li><a href="{pigcms{:U('Loan/borrowList')}">借入列表</a></li>
										<li><a href="{pigcms{:U('Loan/pledge')}">质押管理</a></li>
									</ul>
								</li>
								<li class="submenu "><a href="#"><i class="iconfont "></i><span>资金管理</span><span class="label  visible-xs-block">6</span></a>
									<ul>
										<li><a href="{pigcms{:U('Account/Account')}">账户充值</a></li>
										<li><a href="{pigcms{:U('Account/withdrawal')}">账户提现</a></li>
										<li><a href="{pigcms{:U('Account/withdrawal')}">提现银行</a></li>
										<li><a href="{pigcms{:U('Account/record')}">资金记录</a></li>
										<li><a href="{pigcms{:U('Fund/freezes')}">冻结记录</a></li>
									</ul>
								</li>
								<li class="submenu "><a href="#"><i class="iconfont "></i><span>账户管理</span><span class="label  visible-xs-block">7</span></a>
									<ul>
										<li ><a href="{pigcms{:U('Profile/update')}">个人信息</a></li>
										<li><a href="{pigcms{:U('Profile/avatar')}">上传头像</a></li>
										<li><a href="{pigcms{:U('Profile/IdCard')}">身份认证</a></li>
										<li><a href="{pigcms{:U('Account/updatepwd')}">登录密码</a></li>
										<li><a href="{pigcms{:U('Profile/transactionPassword')}">交易密码</a></li>
										<li><a href="{pigcms{:U('Account/bind')}">绑定手机</a></li>
										<!--											<li><a href="/Member/InfoCenter">站内信</a></li>-->
									</ul>
								</li>
							</ul>
						</div>
						<!--sidebar-menu end-->
					</div>

					<div class="col-sm-10 top_layer">
						<div class="container_page  ">
							<section>
								<h2>我要借出</h2>
								<div class="s_bar visible-xs-block">
									筛选条件 : <span>贷款周期</span>  | <span>贷款金额</span>
								</div>
								<!--筛选区域-->
								<div class="s_area clearfix phone_s" id="filter-specs">
									<div class="tab_s_box_title">筛选<i class="close_s iconfont pull-right">&#xe620;</i></div>
									<div class="tab_s_box">
										<dl>
											<dt class="active">贷款周期：</dt>
											<dd class="active">
						<a href="{pigcms{:U('Investment/lend')}" <if condition="$lc eq 0">class="active"</if>>全部</a>
						<a href="{pigcms{:U('Investment/lend',array('lc'=>7))}" <if condition="$lc eq 7">class="active"</if>>7天</a>
						<a href="{pigcms{:U('Investment/lend',array('lc'=>15))}" <if condition="$lc eq 15">class="active"</if>>15天</a>
						<a href="{pigcms{:U('Investment/lend',array('lc'=>30))}" <if condition="$lc eq 30">class="active"</if>>1个月</a>
						<a href="{pigcms{:U('Investment/lend',array('lc'=>60))}"<if condition="$lc eq 60">class="active"</if>>2个月</a>
						<a href="{pigcms{:U('Investment/lend',array('lc'=>90))}"<if condition="$lc eq 90">class="active"</if>>3个月</a>
						<a href="{pigcms{:U('Investment/lend',array('lc'=>120))}" <if condition="$lc eq 120">class="active"</if>>4个月</a>
						<a href="{pigcms{:U('Investment/lend',array('lc'=>150))}" <if condition="$lc eq 150">class="active"</if>>5个月</a>
						<a href="{pigcms{:U('Investment/lend',array('lc'=>180))}" <if condition="$lc eq 180">class="active"</if>>6个月</a>
												<a href="{pigcms{:U('Investment/lend',array('lc'=>210))}" <if condition="$lc eq 210">class="active"</if>>7个月</a>
												<a href="{pigcms{:U('Investment/lend',array('lc'=>240))}" <if condition="$lc eq 240">class="active"</if>>8个月</a>
												<a href="{pigcms{:U('Investment/lend',array('lc'=>270))}" <if condition="$lc eq 270">class="active"</if>>9个月</a>
												<a href="{pigcms{:U('Investment/lend',array('lc'=>300))}" <if condition="$lc eq 300">class="active"</if>>10个月</a>
												<a href="{pigcms{:U('Investment/lend',array('lc'=>330))}" <if condition="$lc eq 330">class="active"</if>>11个月</a>
												<a href="{pigcms{:U('Investment/lend',array('lc'=>360))}" <if condition="$lc eq 360">class="active"</if>>12个月</a>
											</dd>

											<dt>贷款金额：</dt>
											<dd>
									<a href="{pigcms{:U('Investment/lend')}" <if condition="$mx eq 0">class="active"</if>>全部</a>
								<a href="{pigcms{:U('Investment/lend',array('maxa'=>9999))}" <if condition="$mx eq 1">class="active"</if>>1万元以内</a>
												<a href="{pigcms{:U('Investment/lend',array('mina'=>10000,'maxa'=>100000))}" <if condition="$mx eq 2">class="active"</if>>1万元-10万元 </a>
												<a href="{pigcms{:U('Investment/lend',array('mina'=>100001))}" <if condition="$mx eq 3">class="active"</if>>10万元以上</a>
											</dd>
										</dl>
										<div class="box-filter-ft">
											<a class="btn" href="{pigcms{:U('Investment/lend')}" target="_self"><i class=""></i>重置</a>
										</div>
										<div class="clear"></div>
									</div>
								</div>
								<!--筛选区域-->
								<h3 class="table_title">搜索结果  <a href="{pigcms{:U('Investment/lend')}" class="small">| 刷新此页面</a>
								</h3>
								<div class="table-responsive">
									<table class=" table table-bordered table-striped table-hover  wyjc_table">
										<thead>
										<tr>
											<th><a <empty name="t">href="{pigcms{:U('Investment/lend',array('t'=>1))}"<else />href="{pigcms{:U('Investment/lend',array('t'=>$t))}"</empty> <if condition="$t eq 2"> class="down" <elseif condition="$t eq 1"/>class="up" </if>>贷款编号/截止时间</a></th>
											<th><a <empty name="tt">href="{pigcms{:U('Investment/lend',array('tt'=>3))}"<else />href="{pigcms{:U('Investment/lend',array('tt'=>$tt))}"</empty> <if condition="$tt eq 3"> class="down" <elseif condition="$tt eq 4"/>class="up" </if>>金额</a></th>
											<th><a <empty name="ttt">href="{pigcms{:U('Investment/lend',array('ttt'=>5))}"<else />href="{pigcms{:U('Investment/lend',array('ttt'=>$ttt))}"</empty><if condition="$ttt eq 5"> class="down" <elseif condition="$ttt eq 6"/>class="up" </if>>利率</a></th>
											<th>投标进度/状态</th>
											<th>周期/还款方式</th>
											<th>操作</th>
										</tr>
										</thead>
										<tbody>
									 <if condition="is_array($borrowsList)">
										 <volist name="borrowsList" id="vo">
										<tr>
											<td>
												<a <a <if condition="($vo['status'] eq 0) OR ($vo['status'] eq 3) OR ($vo['status'] eq 4) OR ($vo['status'] eq 5) OR ($vo['status'] eq 7) OR ($vo['status'] eq 6)"> href="javascript:void(0);"<else/> href="{pigcms{:U('Investment/loanScheme',array('dkid'=>$vo['bid']))}" target="_blank"</if> >[{pigcms{$vo.did}] 贷款 {pigcms{$vo.Amountdx}元</a>
												<br />
												贷款人：{pigcms{$vo.dname} <br />
												截止时间：{pigcms{$vo.ExpiredAt}
											</td>
											<td class="text-center amount-pay-out">￥{pigcms{$vo.Amount}</td>
											<td class="text-center">年利率：
												<span class=" amount-pay"> {pigcms{$vo.Rate}% </span>
											</td>
											<td>
												<div class="progress">
													<div class="progress-bar progress-bar-warning progress-bar-striped active " role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {pigcms{$vo.invesing}%; color: #666">
														{pigcms{$vo.invesing}%
													</div>
												</div>
												还需 ￥{pigcms{$vo.needAmount}<br />
												剩余时间：{pigcms{$vo.surplus_date}天</td>
											<td class="text-center">
												<if condition="$vo['LoanCycleDay'] neq 0">{pigcms{$vo.LoanCycleDay}天<elseif condition="$vo['LoanCycleMonth'] neq 0"/>{pigcms{$vo.LoanCycleMonth} 个月</if>
												<br />
												每月还息
											</td>
											<td class="text-center">
							<a href="{pigcms{:U('Investment/investing',array('dkid'=>$vo['bid']))}" class="btn btn-warning">马上投资</a>
											</td>
										</tr>
										 </volist>
										 <else />
										 <tr><td colspan="6">暂无记录</td></tr>
										  </if>

										</tbody>
									</table>
								</div>



							</section>
						</div>
					</div>
				</div>
			</div>
		</section>


	</div>
</div>
<include file="Public:footer"/>

<script src="{pigcms{$static_path}js/unslider.js"></script>
<script src="{pigcms{$static_path}js/bootstrap.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/rz_common.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/matrix.js"></script>
<script type="text/javascript">
	$(function () {
		var $s_area = $("#filter-specs");
		$(".s_bar").click(function () {
			$s_area.addClass("box-filter-expanded");
			$("body").addClass("phone_s");


		});
		$(".close_s").click(function () {
			$s_area.removeClass("box-filter-expanded");
			$("body").removeClass("phone_s");

		});

		if (/*navigator.userAgent.match(/mobile/i) && */$(window).width() < 768) {
			// alert("手机")

			$("#filter-specs dd").height($(window).height() - 100);
			$(window).resize(function () {
				$("#filter-specs dd").height($(window).height() - 100);
			});
			/*选项卡*/
			var $tab_b = $(".tab_s_box dt"), $tab_c = $(".tab_s_box dd");
			$tab_b.click(function () {
				if (!$(this).hasClass("active")) {
					$(this).siblings("dt").removeClass("active");
					$(this).addClass("active");
					$tab_c.removeClass("active");
					$(this).next("dd").addClass("active");
				};
			});
		} else {
			$("#filter-specs dd").height("auto");
		};

	});
</script>


</body>
</html>