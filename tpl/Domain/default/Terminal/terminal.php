<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的终端域名 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
        <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>我的终端域名</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>我的终端</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 选项卡 -->
            <div class="m-acc-select u_be9 s-bg-white">
                <!-- 标题 -->
                <div class="m-acc-title s-bg-fc">
                    <div class="u-fl"><a class="active" href="{pigcms{:U('Terminal/terminal')}">审核</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Terminal/ongoing')}">推荐中</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Terminal/succeed')}">推荐成功</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Terminal/failure')}">推荐失败</a></div>
                    <input type="hidden" id="checktype" value="3">
                    <div class="u-cls"></div>
                </div>
                <!-- 内容 -->
                    <div class="m-acc-cons s-bg-fc  u_mall1">
                        <h3 class="s-3c s-bg-fe">审核中</h3>
                        <table>
                            <tbody>
                                <tr>
                                    <th width="16px"></th>
                                    <th width="150px" class="u_tl">域名</th>
                                    <th width="120px" class="u_tr">最低价格</th>
                                    <th width="74px"></th>
                                    <th width="140px" class="u_tl">申请时间</th>
                                    <th width="160px" class="u_tr">操作</th>
                                    <th width="16px"></th>
                                </tr>
                                
                            <volist id="vo" name="list">
                                <if condition="$vo.status eq 0">
                                    <tr>
                                        <td width="16px"></td>
                                        <td width="150px" class="u_tl">{pigcms{$vo.domain_name}</td>
                                        <td width="120px" class="u_tr">￥{pigcms{$vo.min_price|number_format=###}</td>
                                        <td width="74px"></td>
                                        <td width="140px" class="u_tl">{pigcms{$vo.add_time|date="Y-m-d H:i:s",###}</td>
                                        <td width="160px" class="u_tr">
                                            <a href ="{pigcms{:U('Terminal/index',array('id'=>$vo['id']))}" >修改</a>
                                            <a id="cancel" data-cancelid="{pigcms{$vo.id}">取消</a>
                                        </td>
                                        <td width="16px"></td>
                                    </tr>
                                </if>
                            </volist>
                                
<!--                                <tr>
                                    <td class="u_nb u_tc u_norecord" colspan="7">暂无记录！</td>
                                </tr>   -->
                            
                            </tbody>
                        </table>
                    </div>
                    <div class="m-acc-cons s-bg-fc  u_mall2">
                        <h3 class="s-3c s-bg-fe">审核成功</h3>
                        <table>
                            <tbody>
                                <tr>
                                    <th width="16px"></th>
                                    <th width="150px" class="u_tl">域名</th>
                                    <th width="110px" class="u_tr">最低价格</th>
                                    <th width="24px"></th>
                                    <th width="90px" class="u_tl">申请时间</th>
                                    <th width="270px" class="u_tr">操作</th>
                                    <th width="16px"></th>
                                </tr>
                            
                            
                            <volist id="vo" name="list">
                                <if condition="$vo.status eq 1">
                                    <tr>
                                        <td width="16px"></td>
                                        <td width="150px" class="u_tl">{pigcms{$vo.domain_name}</td>
                                        <td width="120px" class="u_tr">￥{pigcms{$vo.min_price|number_format=###}</td>
                                        <td width="74px"></td>
                                        <td width="140px" class="u_tl">{pigcms{$vo.add_time|date="Y-m-d H:i:s",###}</td>
                                        <td width="160px" class="u_tr"><a  onclick="getListID({pigcms{$vo.id})">删除</a></td>
                                        <td width="16px"></td>
                                    </tr>
                                </if>
                            </volist>
                            
<!--                            <tr>
                                <td class="u_nb u_tc u_norecord" colspan="7">暂无记录！</td>
                            </tr>                        -->
                            </tbody>
                        </table>
                    </div>
                    <div class="m-acc-cons s-bg-fc  u_mall2">
                        <h3 class="s-3c s-bg-fe">审核失败</h3>
                        <table class="tb_ap_fail">
                            <tbody><tr>
                                <th width="16px"></th>
                                <th width="150px" class="u_tl">域名</th>
                                <th width="120px" class="u_tr">最低价格</th>
                                <th width="48px"></th>
                                <th width="110px" class="u_tl">申请时间</th>
                                <th width="150px" class="u_tl">原因</th>
                                <th width="150px" class="u_tr">操作</th>
                                <th width="16px"></th>
                            </tr>
                            
                            <volist id="vo" name="list">
                                <if condition="$vo.status eq 2">
                                    <tr>
                                        <td width="16px"></td>
                                        <td width="150px" class="u_tl">{pigcms{$vo.domain_name}</td>
                                        <td width="120px" class="u_tr">￥{pigcms{$vo.min_price|number_format=###}</td>
                                        <td width="48px"></td>
                                        <td width="110px" class="u_tl">{pigcms{$vo.add_time|date="Y-m-d H:i:s",###}</td>
                                        <td width="150px" class="u_tr">{pigcms{$vo.reason}</td>
                                        <td width="150px" class="u_tr"><a  onclick="getListID({pigcms{$vo.id})">删除</a></td>
                                        <td width="16px"></td>
                                    </tr>
                                </if>
                            </volist>
                            
                            
<!--                        <tr><td class="u_nb u_tc u_norecord" colspan="8">暂无记录！</td></tr>-->
                        </tbody>
                        </table>
                    </div>
                <!-- 分页 -->
                <div class="g-padding g-padding1" id="paging"></div>
            </div>
        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
    <script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">
	<script>
            
            function getListID(obj){
                swal({   
                        title: "删除?",   
                        text: "您确定要删除此条信息吗!",   
                        type: "warning",   
                        showCancelButton: true,   
                        confirmButtonColor: "#DD6B55",   
                        confirmButtonText: "Yes, 我要删除!",   
                        cancelButtonText: "No, 我点错了!",  
                        closeOnConfirm: false,   
                        closeOnCancel: true
                }, 
                function(isConfirm){   
                        if (isConfirm) {     
                                var id= obj;
                                $.post("{pigcms{:U('Terminal/delete')}",{"id":id},function(data){
                                        if(data == 1){
                                                swal("成功!", "此条信息删除成功.", "success");
                                                location.href = location.href;
                                        }else{
                                                swal("异常", "信息删除异常，请重试 :)", "error");  
                                        }
                                })

                        }
                });

            }
            
		
		$("#cancel").click(function(){
			swal({   
				title: "取消?",   
				text: "您确定要取消此条信息吗!",   
				type: "warning",   
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",   
				confirmButtonText: "Yes, 我要取消!",   
				cancelButtonText: "No, 我点错了!",  
				closeOnConfirm: false,   
				closeOnCancel: true 
			}, 
			function(isConfirm){   
				if (isConfirm) {     
					var id= jQuery("#cancel").data('cancelid');
					$.post("{pigcms{:U('Terminal/cancel')}",{"id":id},function(data){
						if(data == 1){
							swal("成功!", "此条信息取消成功.", "success");
							location.href = location.href;
						}else{
							swal("异常", "信息取消异常，请重试 :)", "error");  
						}
					})
				}
			});
		})
		
		
		
	</script>
	<include file="Public:sidebar"/>
</body>
</html>
