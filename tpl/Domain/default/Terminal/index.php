<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>终端域名 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/common.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/terminal.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-ban marB10" style="background:url({pigcms{$static_path}images/index/s-ban-zd.jpg) 50% 0 no-repeat;">
		<div class="m-width">
			<h1>千里马常有，伯乐不常有！{pigcsm{$config.site_name}常有，终端亦常有！</h1>
			<p>垃圾邮件骚扰？错失终端？把域名交给我们 我们帮你卖终端！</p>
			<a href="#submitDM" class="btn">现在就去提交</a>
		</div>
	</div>
<div class="m-width marB10">
	<div class="m-term-l">
		<div class="u_be2 s-bg-white marB6">
			<div class="m-ta">
				<div class="title">
					<div class="s-47">
						<span class="z-img-zd"></span>
						<h2>终端域名</h2>
					</div>
				</div>
			</div>
			<ul class="term-i-list">
				<li>
					<p class="icon i1"></p>
					<p class="n">多种沟通渠道</p>
				</li>
				<li>
					<p class="icon i2"></p>
					<p class="n">精准价格预估</p>
				</li>
				<li>
					<p class="icon i3"></p>
					<p class="n">强大终端筛选</p>
				</li>
				<li>
					<p class="icon i4"></p>
					<p class="n">专业谈判能力</p>
				</li>
			</ul>
		</div>
		<div class="u_be2 s-bg-white">
			<div class="m-ta">
				<div class="title">
					<div class="s-47">
						<h2 id="submitDM">提交域名</h2>
					</div>
				</div>
			</div>
			<div class="term-submit">
				<ul class="text marB10">
					<li class="b">注意事项：</li>
					<li>请保证所提交的域名归您自己个人所持有，不存在争议和纠纷</li>
					<li>一个有吸引力的报价可能能让终端更有兴趣</li>
					<li>交易手续费为8%，卖方支付</li>
				</ul>
					<div class="form">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="marB10">
							<tr>
								<th width="15"></th>
								<th>域名</th>
								<th width="100">最低价格</th>
								<th width="100">期望价格</th>
								<th>希望的终端？（可不填）</th>
								<th width="15"></th>
							</tr>
							<tr class="record">
								<td class="no">1</td>
								<td><span class="u-ipt-d" style="">
                                    <input type="text" maxlength="50" class="domainName" name="domain_name" id="domain_name" value="{pigcms{$info.domain_name}" ></span></td>
								<td><span class="u-ipt-d">
                                    <input type="text" maxlength="10" class="min_price" name="min_price" id="min_price" value="{pigcms{$info.min_price}"></span></td>
								<td><span class="u-ipt-d">
                                    <input type="text" maxlength="10" class="max_price" name="max_price" id="max_price" value="{pigcms{$info.max_price}"></span></td>
								<td><span class="u-ipt-d">
                                    <input type="text" maxlength="50" class="ipt_terminal" name="hope" id="hope" value="{pigcms{$info.hope}"></span></td>
<!--								<td><span class="addbtn"></span></td>-->
							</tr>
						</table>
						<div class="add-DM">
<!--							<span class="addbtn">添加域名<span></span></span>-->
							<div class="u-cls"></div>
							<div class="ly">
								<label>留言：</label>
                                <span class="textarea">
                                    <textarea id="tt_msg" name="tt_msg" maxlength="100">{pigcms{$info.tt_msg}</textarea></span>
							</div>
						</div>
					</div>
<!--					<div class="agr">
						<input id="ckb_agree" type="checkbox" value="">
						我已阅读并同意<a target="_blank" href="/regulation?v=escrow">《域名中介服务协议》</a>
					</div>-->
					<div class="submitBtn">
						<input type="hidden" id="t_id" name="t_id" value="{pigcms{$info.id}" />
<!--						<input type="hidden" name="status" value="{pigcms{$info.status}" />-->
						<input type="submit" id="btn_sub" value="提交域名">
					</div>
			</div>
		</div>
	</div>
	<div class="m-term-r">
		<div class="u_be2 s-bg-white marB6">
			<div class="m-ta">
				<div class="title">
					<div class="s-47">
						<h2>推荐流程</h2>
					</div>
				</div>
			</div>
			<ul class="term-pro">
				<li><span>1</span>提交域名</li>
				<li><span>2</span>平台审核</li>
				<li><span>3</span>授权推荐</li>
				<li><span>4</span>推给终端</li>
				<li><span>5</span>达成交易</li>
			</ul>
		</div>
		<div class="u_be2 s-bg-white marB6">
			<div class="m-ta">
				<div class="title">
					<div class="s-47">
						<h2>我们已成功推荐给</h2>
					</div>
				</div>
			</div>
			<div class="term-guys" id="animate3">
				<a class="next"></a><a class="prev"></a>
				<div class="animate3 box">
					<ul class="list">
						<volist name="adver_list" id="vo">
							<li>
								<a rel="nofollow" href="{pigcms{$vo.url}" title="{pigcms{$vo.name}" target="_blank" class="bwWrapper">
									<img alt="{pigcms{$vo.name}" src="upload/adver/{pigcms{$vo.pic}" />
								</a>
							</li>
						</volist>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="u-cls"></div>
</div>
<div class="s-bg-f7"></div>
<include file="Public:footer"/>
<include file="Public:sidebar"/>
<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">
<script type="text/javascript" src="{pigcms{$static_path}js/jquery.cxscroll.min.js"></script>
<script type="text/javascript">
    
   
    
    
	$(function () {
		$(".term-submit .form .addbtn").bind("click", addRecord);
		$(".term-submit .form").on("click", ".delbtn", delRecord);

		$(".min_price,.max_price").live("keyup", function () {
			clearNoNum1(this);
		})






		$("#btn_sub").click(function () {
			if ($("#userId").length < 1) {
				showDialog(lang.a66);
				return false;
			}
//			if (!$("#ckb_agree").attr("checked")) {
//				showDialog(lang.a194);
//				return false;
//			}

			var f = m = false;
			$(".domainName,.min_price,.max_price").each(function () {
				if ($(this).val().trim() == "") {
					$(this).focus();
					f = true;
				}
				if ($(this).hasClass("max_price")) {
					if (Number($(this).val()) < Number($(this).parents(".record").find(".min_price").val())) {
						m = true;
						$(this).focus();
					}
				}
			})
			if (f) {
				showDialog(lang.a18);
				return false;
			}
			if (m) {
				showDialog(lang.a195);
				return false;
			}
                        
    
    var domain_name = $("#domain_name").val();
    var max_price = $("#max_price").val();
    var min_price = $("#min_price").val();
    var hope = $("#hope").val();
    var tt_msg = $("#tt_msg").val();
    var t_id = $("#t_id").val();
    
    $.post("{pigcms{:U('Terminal/add')}",{"domain_name":domain_name,"max_price":max_price,"min_price":min_price,"hope":hope,"tt_msg":tt_msg,"t_id":t_id},function(data){
        if(data == 1){
            $("#domain_name").val("");
            $("#max_price").val("");
            $("#min_price").val("");
            $("#hope").val("");
            $("#tt_msg").val("");
            $("#t_id").val("");
            swal({   title: "推送成功!",   text: "等待经纪人审核.",   timer: 2000,   showConfirmButton: false });
        }else{
            sweetAlert("异常...", "操作异常，请重试!", "error");
        }
    });
    
                        
                        
		});
		var error = getCookie("error");
		if (error == "1") {
			delCookie("error");
			showDialog(lang.a196);
		} else if (error == "2") {
			delCookie("error");
			showDialog(lang.a23);
		} else if (error == "3") {
			delCookie("error");
			showDialog("提交失败，域名重复");
		}
                
                
                



	});
	function addRecord() {
		if ($(".domainName").length > 9) {
			showDialog(lang.a197);
			return false;
		}
		var addHtml = "<tr class=\"record\"><td class=\"no\">" + ($(".term-submit .form .record:last").index() + 1) + "</td><td><span class=\"u-ipt-d\"><input type=\"text\" maxlength=\"50\" class=\"domainName\" name=\"domainName\"></span></td><td><span class=\"u-ipt-d\"><input type=\"text\" maxlength=\"10\" class=\"min_price\" name=\"min_price\"></span></td><td><span class=\"u-ipt-d\"><input type=\"text\" maxlength=\"10\" class=\"max_price\" name=\"max_price\"></span></td><td><span class=\"u-ipt-d\"><input type=\"text\" maxlength=\"50\" class=\"ipt_terminal\" name=\"ipt_terminal\"></span></td><td><span class=\"delbtn\"></span></td></tr>";
		$(".term-submit .form .record:last").after(addHtml);
	}
	function delRecord() {
		$(this).parents(".record").remove();
		$(".term-submit .form .no").each(function (i) {
			$(this).text(i + 1);
		});
	}
	$("#animate3").cxScroll({ direction: "bottom", step: 2, speed: 800, time: 2000 });
</script>
</body>
</html>