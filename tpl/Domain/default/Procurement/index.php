<!DOCTYPE html>
<html>
<head>	
	<meta charset="utf-8"/>
	<title>域名代购 - 代购的优势 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/common.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>域名代购</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
		<div class="m-content">
			<!-- 左边 -->
			<div class="g-a-container">
				<div class="m-ta s-bg-white u_be9">
					<div class="title">
						<a class="s-47" href="javascript:void(0)">
							<span class="z-img-t11"></span>
							<span>域名代购</span>
						</a>
					</div>
					<div class="u-cls"></div>
				</div>
				<div class="m-insa s-bg-fc u_be89">
					<ul>
						<li><span class="u_fw">1.</span>保证金代购失败或成功都会退还，申请方违约则不退还。</li>
						<li><span class="u_fw">2.</span>申请方带代购达成前，可以取消代购。代购达成后取消则视为违约。</li>
						<li><span class="u_fw">3.</span>代购手续费为固定8%，申请方支付</li>
					</ul>
				</div>
				<div class="m-insb s-bg-fc u_be89" id="animateb">
					<span class="f-img-dg1">代购申请</span>
					<span class="f-img-dg6"></span>
					<span class="f-img-dg2">审核申请</span>
					<span class="f-img-dg6"></span>
					<span class="f-img-dg3">缴纳保证金</span>
					<span class="f-img-dg6"></span>
					<span class="f-img-dg4">跟进谈判</span>
					<span class="f-img-dg6"></span>
					<span class="f-img-dg5">谈判结束</span>
				</div>
				<div class="m-transa s-bg-fc u_be89">
					<p class="f-p-t6 s-bg-white u_bb0 s-3c">发起代购交易</p>
<!--					<form method="post" action="{pigcms{:U('Procurement/add')}" id="form">-->
						<input type="hidden"  name="type" value="0"/>
						<div class="transa">
							<p class="u_mb16">
								<span class="u-txj">您想要购买的域名：</span>
								<span class="u-ipt-d">
									<input type="text" name="domain" class="domainName"  ph="{pigcms{$now_site_short_url}" value="" />
								</span>
							</p>
							<p class="u_mb16">
								<span class="u-txj">您的心理预算：</span>
								<span class="u-ipt-d">
									<input type="text" name="buyers_price" id="buyers_price" class="u_vm u_prise" ph="最低5000元人民币起" value="{pigcms{$d}" />
								</span>
								<span class="u_m2 s-64">￥</span>
							</p>
							<p>
								<span class="u-txj">留言或其它附加信息：</span>
								<span class="u_txera s-bg-white">
									<textarea class="message" name="message"></textarea>
								</span>
							</p>
							<p>
								<br />
								<input type="submit" value="提交申请" id="btn_pro" class="u-btn3 s-bg-2a" />
							</p>
						</div>
<!--					</form>-->
				</div>
			</div>
			<!-- 右边 -->

			<div class="g-b-container">
				<div class="m-bb-container">
					<div class="m-bb-title s-bg-fb">
						<a href="javascript:void(0);" class="u-tia s-3c">域名代购案例</a>
					</div>
					<div class="m-apply-dg m-apply-dga">
						<div class="m-apply-title">
							<span class="u-fl">域名</span>
							<span class="u-fr">价格</span>
							<div class="u-cls"></div>
						</div>
						<div class="m-animate3" id="animate3">
							<div class="animate3 box">
								<ul class="list">
                                    <volist id="vo" name="ph_list">
                                        <li>
											<div class="spanl"><a href="http://{pigcms{$vo.domain}" target="_blank" rel="nofollow">{pigcms{$vo.domain}</a></div>
											<div class="spanr">￥{pigcms{$vo.buyers_price|number_format}</div>
											<div class="u-cls"></div>
										</li>
                                   </volist>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<include file="Public:footer"/>
	<include file="Public:sidebar"/>
	<div class="s-bg-f7"></div>
        <script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css"/>
        <script>
            var post_url = "{pigcms{:U('Procurement/add')}";
        </script>
	<script type="text/javascript" src="{pigcms{$static_path}js/escrow.js"></script>
        
	<script type="text/javascript" src="{pigcms{$static_path}js/jquery.cxscroll.min.js"></script>
	<script type="text/javascript" src="{pigcms{$static_path}js/plugs_popupBox.js"></script>
	<script type="text/javascript">
		$("#animate3").cxScroll({ direction: "bottom", step: 2, speed: 800, time: 2000 });
	</script>
</body>
</html>