<include file="Public:header"/>
<div class="main-content">
    <!-- 内容头部 -->
    <div class="breadcrumbs" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-group"></i>
                <a href="{pigcms{:U('User/index')}">业主管理</a>
            </li>
            <li class="active">业主列表</li>
        </ul>
    </div>
    <!-- 内容头部 -->
    <div class="page-content">
        <div class="page-content-area">
        	<button class="btn btn-success" onclick="importUser()">导入业主</button>
        	<button class="btn btn-success" onclick="importUserDetail()">导入业主每月帐单明细</button>
            <style>
                .ace-file-input a {display:none;}
            </style>
            <div class="row">
                <div class="col-xs-12">
                    <div id="shopList" class="grid-view">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="10%">物业编号</th>
                                    <th width="10%">姓名</th>
                                    <th width="10%">手机号</th>
                                    <th width="20%">住址</th>
                                    <th width="15%">待缴费用</th>
                                    <th width="5%">停车位</th>
                                    <th width="10%">房子大小</th>
                                    <th class="button-column" width="20%">操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                <if condition="$user_list">
                                    <volist name="user_list['user_list']" id="vo">
                                        <tr class="<if condition="$i%2 eq 0">odd<else/>even</if>">
                                            <td><div class="tagDiv">{pigcms{$vo.usernum}</div></td>
                                            <td><div class="tagDiv">{pigcms{$vo.name}</div></td>
                                            <td><div class="tagDiv">{pigcms{$vo.phone}</div></td>
                                            <td><div class="tagDiv">{pigcms{$vo.address}</div></td>
                                            <td>
												<div class="tagDiv">
													物业费：￥{pigcms{:floatval($vo['property_price'])}<br/>
													水费：￥{pigcms{:floatval($vo['water_price'])}<br/>
													电费：￥{pigcms{:floatval($vo['electric_price'])}<br/>
													燃气费：￥{pigcms{:floatval($vo['gas_price'])}<br/>
													停车费：￥{pigcms{:floatval($vo['park_price'])}<br/>
												</div>
											</td>
                                            <td><div class="shopNameDiv"><if condition="$vo.park_flag eq '1' ">有<else />无</if></div></td>
                                            <td><div class="shopNameDiv">{pigcms{$vo.housesize} ㎡</div></td>
                                            <td class="button-column">
                                                <a style="width: 60px;" class="label label-sm label-info" title="编辑" href="{pigcms{:U('User/edit',array('pigcms_id'=>$vo['pigcms_id'],'usernum'=>$vo['usernum']))}">编辑</a>
                                                <a style="width: 60px;" class="label label-sm label-info" title="欠费明细" href="{pigcms{:U('User/pay_detail',array('pigcms_id'=>$vo['pigcms_id'],'usernum'=>$vo['usernum']))}">欠费明细</a>
                                           		<if condition="$vo['uid'] neq 0">
                                           		<a style="width: 60px;" class="label label-sm label-info" title="缴费明细" href="{pigcms{:U('User/orders',array('uid'=>$vo['uid']))}">缴费明细</a>
                                           		</if>
                                           </td>
                                        </tr>
                                    </volist>
                                <else/>
                                    <tr class="odd"><td class="button-column" colspan="12" >没有任何业主。</td></tr>
                                </if>
                            </tbody>
                        </table>
                        {pigcms{$user_list.pagebar}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{pigcms{$static_public}js/artdialog/jquery.artDialog.js"></script>
<script type="text/javascript" src="{pigcms{$static_public}js/artdialog/iframeTools.js"></script>
<script>
function importUser(){
	window.location.href = "{pigcms{:U('User/user_import')}";
}
function importUserDetail(){
	window.location.href = "{pigcms{:U('User/detail_import')}";
}
</script>
<include file="Public:footer"/>
