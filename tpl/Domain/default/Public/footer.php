<div class="s-bg-2a">
	<div class="g-b-banner">
		<ul>
			<li class="u_mr52">
				<p>7X24 <span>小时</span></p><p class="f-pa1   s-d1">全年无休</p>
			</li>
			<li class="u_mr60">
				<p>一对一服务</p><p class="f-pa1   s-d1">经纪人跟进协助</p>
			</li>
			<li class="u_mr72">
				<p>交易额领先</p><p class="f-pa1   s-d1">年交易额过数亿元</p>
			</li>
			<li>
				<p>4年服务经验</p><p class="f-pa1   s-d1">服务超5万客户</p>
			</li>
		</ul>
	</div>
</div>
<div class="s-bg-white">
	<div class="g-footer-nav">
		<div class="g-f-container u_ml34">
			<p class="f-img-t7"><a href="{pigcms{:U('News/aboutus',array('page'=>'Abouts'))}" rel="nofollow" class="s-def">关于我们</a></p>
			<ol>
				<volist name="footer_aoubt_list" id="vo">
				<li><a href="{pigcms{:U('News/aboutus',array('page'=>$vo['page']))}" rel="nofollow">{pigcms{$vo.title}</a></li>
				</volist>

			</ol>
		</div>
		<div class="g-f-container u_ml145">
			<p class="f-img-t8"><a href="{pigcms{:U('News/aboutus',array('page'=>'Abouts'))}" rel="nofollow" class="s-def">帮助中心</a></p>
			<ol>
				<volist name="footer_guide_list" id="vo">
				<li><a href="{pigcms{:U('News/guide',array('news_id'=>$vo['news_id']))}" rel="nofollow">{pigcms{$vo.news_title}</a></li>
				</volist>
				<li><a href="{pigcms{:U('News/guide',array('news_id'=>$footer_guide_list[0]['news_id']))}" rel="nofollow">更多...</a></li>
			</ol>
		</div>
		<div class="g-f-container u_ml138">
			<p class="f-img-t9"><a href="{pigcms{:U('News/aboutus',array('page'=>'contact'))}" rel="nofollow" class="s-def">服务与支持</a></p>
			<ol>
				<li><a href="javascript:void(0)" class="u_fw s-2a f-img-l1">{pigcms{$config.site_phone}</a></li>
				<li><a href="javascript:void(0)" class="f-img-l2">{pigcms{$config.site_email}</a></li>
				<li><a href="{pigcms{:U('Staff/login')}" class="f-img-l3">经纪人登录</a></li>
				<li><a rel="nofollow" href="{pigcms{:U('Account/recharge')}" class="f-img-l4">付款方式</a></li>
			</ol>
		</div>
		<div class="g-f-container u_ml96">
			<p class="f-img-t10">关注我们</p>
			<div class="m-focus" style="background:url({pigcms{$config.wechat_qrcode});width:95px; height:95px;padding-top:0;margin-left:10px;
			-webkit-background-size: 100%;background-size: 100%;"></div>
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="s-bg-fd">
	<div class="g-footer-certi">

		<p class="f-p5" style="text-align:center">
			<span>Copyright © 2015-2016 IWO Inc.南通端米网络科技有限公司 版权所有
 |  &nbsp;<a href="http://www.miitbeian.gov.cn/"  target="_blank" ></span>
			{pigcms{$config.site_show_footer}</a>  </span>
			
		</p>
		{pigcms{$config.site_footer}
	</div>
</div>
<script src="{pigcms{$static_path}js/jquery-1.8.2.min.js"></script>
<!--<script src="//apps.bdimg.com/libs/jquery/2.0.0/jquery.min.js"></script>-->

<script src="{pigcms{$static_path}js/common.js"></script>