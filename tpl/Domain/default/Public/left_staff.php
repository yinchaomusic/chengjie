<!-- 左边 -->    
<div class="u-fl">
	
	
    <div class="m-processList1" id="processList">
        <h3 style="background-position: 0 -455px;">交易管理</h3>
        <div class="process1">
            <ul>
                <li><a href="{pigcms{:U('Staffdeal/new_client')}">接入新客户</a></li>
                <li><a href="{pigcms{:U('Staffdeal/new_deal')}">发起新的交易</a></li>
                <li><a href="{pigcms{:U('Staffdeal/ongoing_deal')}">进行中的交易</a></li>
                <li><a href="{pigcms{:U('Staffdeal/over_deal')}">已经完成的交易</a></li>
                <li><a href="{pigcms{:U('Staffdeal/purchase_list')}">代购列表</a></li>
                <li><a href="{pigcms{:U('Staffdeal/bulk_trading')}">批量交易审核</a></li>
                <li><a href="{pigcms{:U('Staffdeal/whoischeck')}">人工审核域名</a></li>
                <li><a href="{pigcms{:U('Staffwebsite/listing_purchasing')}">网站审核</a></li>
                <li><a href="{pigcms{:U('IntermediaryOrder/listing_buyers')}">中介交易</a></li>
                <li><a href="{pigcms{:U('Staffdeal/terminal')}">终端域名</a></li>
                <li><a href="{pigcms{:U('Staffdeal/disputes_deal')}">交易纠纷</a></li>
                <li><a href="{pigcms{:U('Staffdeal/add_dispute')}">添加中介纠纷</a></li>
                <li><a href="{pigcms{:U('Staffdeal/bargain')}">优质域名列表</a></li>
                <li><a href="{pigcms{:U('Staffdeal/call')}">呼叫经纪人列表</a></li>
            </ul>
        </div>
    </div>
</div>