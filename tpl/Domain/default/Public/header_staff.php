<div class="s-bg-topa">
	<div class="g-top">
		<div class="m-language u-fl" id="lan">
			<p class="f-img-lan"><span></span></p>
			<div class="f-b-rlt">
				<p kvalue="zh-cn" style="background-position: 0 0px"></p>
				<p kvalue="en-us" style="background-position: 0 -23px"></p>
				<p kvalue="zh-hk" style="background-position: 0 -46px"></p>
			</div>
		</div>
		<div class="m-top-nav u-fr">
			<!-- 登录前 -->
			<ul>
				<if condition="$staff_session">
					<li><a rel="nofollow" href="{pigcms{:U('Staff/index')}">{pigcms{$staff_session.name}</a></li>
					<li><a rel="nofollow" href="{pigcms{:U('Staff/logout')}">安全退出</a></li>
				<else/>
					<li><a href="{pigcms{:U('Staff/login')}" >登录</a></li>
				</if>
			</ul>

		</div>
		<div class="u-cls"></div>
	</div>
</div>

<div class="s-bg-white">
	<div class="g-header">
		<div class="m-logo u-fl">
			<a href="/">
				<img src="{pigcms{$config.site_logo}" alt="{pigcms{$config.site_name}" />
				<h1 style="position: absolute;top: -50px">域名交易</h1>
			</a>
		</div>
		<div class="m-search u-fl">
                <span class="u-ipt-search">
                    <span class="u-slt-a" ym="a">淘域名</span>
                    <input type="text" ph="请输入域名关键字" value="" class="u-ipt-txta" />
                </span>
			<input type="button" value="搜索" class="u-btn-search s-bg-6c" />
			<div class="f-a-rlt">
				<p ym="a">淘域名</p>
				<p ym="b">查whois</p>
			</div>
			<div class="f-search-rlt">
			</div>
		</div>
		<div class="m-contact u-fr">
			<p class="f-p1">{pigcms{$config.site_phone}</p>
			<p class="s-txt-coa u_pl20">7x24小时，全年无休</p>
		</div>
		<div class="m-slogan u-fr">
			<img alt="域名和世界，都近在咫尺" src="{pigcms{$static_path}images/index/u-txt.png" />
		</div>
	</div>
</div>