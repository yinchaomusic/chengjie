<div class="s-bg-topa">
	<div class="g-top">
		<div class="m-language u-fl" id="lan">
			<!--p class="f-img-lan"><span></span></p>
			<div class="f-b-rlt">
				<p kvalue="zh-cn" style="background-position: 0 0px"></p>
				<p kvalue="en-us" style="background-position: 0 -23px"></p>
				<p kvalue="zh-hk" style="background-position: 0 -46px"></p>
			</div-->
			<ul>
				<li><a href="{pigcms{$config.site_url}" style="padding:0 12px;">首页</a></li>
				<li><a href="{pigcms{:U('Escrow/index')}" style="padding:0 12px;">域名中介</a></li>
				<li><a href="{pigcms{:U('Procurement/index')}" style="padding:0 12px;">域名代购</a></li>
			<li><a target="_blank" href="http://dk.chengjie.com">域名融资</a></li> 
			</ul>
		</div>
		<div class="m-top-nav u-fr">
			<!-- 登录前 -->
			<ul>
				<if condition="$user_session">
					<li><a rel="nofollow" href="{pigcms{:U('Account/index')}" id="userId">{pigcms{$user_session.uid}({pigcms{$user_level_info.lname})</a></li>
					<li><a rel="nofollow" href="{pigcms{:U('Login/logout')}">安全退出</a></li>
				<else/>
					<li><a href="javascript:void(0);" class="active" id="login" style="padding:0 12px;">登录</a></li>
					<li><a rel="nofollow" href="{pigcms{:U('Login/reg')}" style="padding:0 12px;">注册</a> </li>
				</if>
				<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
				<!--li><a href="#" target="_blank">域名工具</a></li>
				<li><a href="#">域名估价</a></li>
				<li><a href="#" target="_blank">域名贷款</a></li-->
			</ul>

		</div>
		<div class="u-cls"></div>
	</div>
	<div class="s-bg-login" id="s-bg-login">
		<div class="g-login">
			<div class="m-reg u-fl">
				<p class="u_mb26 u_ml7" style="font-size:20px;">注册一个新账户</p>
				<p class="f-p2 u_ml7">欢迎加入中国优质域名交易平台，您需要免费注册成为会员，才能享受我们提供的以下服务</p>
				<a rel="nofollow" href="{pigcms{:U('Login/reg')}" class="u-abtn-res s-bg-fb75">免费注册新会员</a>
			</div>
			 

			<div class="m-login u-fl" style="margin-left:100px;padding-left:134px;border-left:1px solid #d0d0d0;">
				<p class="u_mb36" style="font-size:20px;">已是会员，请登录！</p>
				<p class="f-p3 u_mb15">
					<span class="s-b15 bt1">会员ID或E-mail：</span>
                        <span class="u-ipt-emial">
                            <input type="text" ph="邮箱/手机" id="email-id" />
                        </span>
				</p>
				<p class="f-p3">
					<span class="s-b15 bt2">请输入您的密码：</span>
                        <span class="u-ipt-password">
                            <input type="password" placeholder="6-20位密码" class="password" id="password" />
                        </span>
				</p>
				<p class="f-prompt-p1">
					<span class="s-red u-fl ts_denglu"></span>
					<a rel="nofollow" href="{pigcms{:U('Login/findpsw')}" target="_blank" class="s-7c u-fr">忘记密码?</a>
					<a rel="nofollow" href="{pigcms{:U('Login/index')}" >微信登陆</a>
				</p>
				<p class="u-cls">
					<input type="button" class="s-btn-2a u-btn-login" id="deng-lu" value="登录">
				</p>
			</div>
			<div class="g-close u-fr"></div>
			<div class="u-cls"></div>
		</div>
	</div>
</div>
<div class="s-bg-white">
	<div class="g-header">
		<div class="m-logo u-fl">
			<a href="{pigcms{$config.site_url}">
				<img src="{pigcms{$config.site_logo}" alt="{pigcms{$config.site_name}" />
				<h1 style="position: absolute;top: -50px">域名交易</h1>
			</a>
		</div>
		<div class="m-search u-fl" >
                <span class="u-ipt-search" style="height: auto;">
                    <span class="u-slt-a" ym="a" style="width: auto;">淘域名</span>
                    <input type="text" ph="请输入域名关键字" value="" class="u-ipt-txta" />
                </span>
			<input type="button" value="搜索" class="u-btn-search s-bg-6c" />
			<div class="f-a-rlt" style="    width: 95px;">
				<p ym="a" style="width: auto">淘域名</p>
				<p ym="b" style="width: auto">查whois</p>
			</div>
			<div class="f-search-rlt">
			</div>
		</div>
		<div class="m-contact u-fr">
			<p class="f-p1">{pigcms{$config.site_phone}</p>
			<p class="s-txt-coa u_pl20">7x24小时，全年无休</p>
		</div>
		<div class="m-slogan u-fr">
			<img alt="域名和世界，都近在咫尺" src="{pigcms{$static_path}images/index/u-txt.png" />
		</div>
	</div>
</div>