<div class="s-bg-nav u-cls">
	<div class="g-nav">
		<ul>
			<li><a href="/">首页</a></li>
			<li class="nav_fa"><a href="{pigcms{:U('BuyDomains/index')}">买域名</a>
				<ul>
					<li><a href="{pigcms{:U('BuyDomains/index')}">议价域名</a></li>
					<li><a href="{pigcms{:U('Fastbid/index')}">极速竞价</a></li>
					<li><a href="{pigcms{:U('Hotsale/index')}">一口价域名</a></li>
					<li><a href="{pigcms{:U('Bargain/index')}">优质域名</a></li>
				</ul>
			</li>
			<li><a href="{pigcms{:U('SellDomain/index')}">卖域名</a></li>
			<li><a href="{pigcms{:U('Bargain/index')}">优质域名</a></li>
			<li><a href="{pigcms{:U('Escrow/index')}">域名中介</a></li>
			<li><a href="{pigcms{:U('Procurement/index')}">域名代购</a></li>
			<li><a href="{pigcms{:U('Financing/index')}">域名融资</a></li>
			<li><a href="{pigcms{:U('Terminal/index')}"><span></span>终端域名</a></li>
			<li><a href="{pigcms{:U('Pldomain/index')}" class="f-img-new"><span></span>批量交易</a></li>

			<li><a rel="nofollow" href="{pigcms{:U('Staff/index')}" class="f-img-account" id="account">我的账户</a></li>
		</ul>
	</div>
<!--	<div class="s-bg-account" id="s-bg-account">
		<div class="g-account">

			<div class="g-acc-container s-line1">
				<h3 class="u_mb17 s-b15 f-img-t3">交易管理</h3>
				<ul>
					<li><a href="{pigcms{:U('Procurement/index')}">发起中介交易</a></li>
					<li><a href="{pigcms{:U('Account/deals')}">我的所有交易</a></li>
				</ul>
				<br />
				<h3 class="u_mb17 s-b15 f-img-t6">问题反馈</h3>
				<ul>
					<li><a href="{pigcms{:U('Problem/Problem_add')}">提交问题</a></li>
					<li><a href="{pigcms{:U('Problem/index')}">我的提问</a></li>
				</ul>
			</div>
			<div class="g-acc-container s-line1">
				<h3 class="u_mb17 s-b15 f-img-t4">资金管理</h3>
				<ul>
					<li><a href="{pigcms{:U('Account/recharge')}">账户充值</a></li>
					<li><a href="{pigcms{:U('Account/withdrawal')}">提现</a></li>
					<li><a href="{pigcms{:U('Account/record')}">财务明细</a></li>
					<li><a href="{pigcms{:U('Account/frozendetail')}">冻结明细</a></li>
				</ul>
			</div>
			<div class="g-acc-container">
				<h3 class="u_mb17 s-b15 f-img-t5">账户管理</h3>
				<ul>
					<li><a href="{pigcms{:U('Account/bind')}">账号安全</a></li>
					<li><a href="{pigcms{:U('Account/whoisemail')}">Whois邮箱</a></li>
					<li><a href="{pigcms{:U('Account/updateprofile')}">修改资料</a></li>
					<li><a href="{pigcms{:U('Account/updatepwd')}">修改密码</a></li>
				</ul>
			</div>
			<div class="g-close u-fr u_mt_8"></div>
			<div class="u-cls"></div>
		</div>
	</div>-->
</div>