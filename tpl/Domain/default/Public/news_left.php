<div class="help_menu">
    <div class="m-ta s-bg-white u_bb0">帮助中心</div>
    <ul class="menu_ul">
        <li><a href="{pigcms{:U('News/aboutus',array('page'=>'Abouts'))}" style="background-position: 15px -2px; margin-top: 0;">关于我们</a></li>
        <li><a href="{pigcms{:U('News/aboutus',array('page'=>'culture'))}" style="background-position: 15px -1351px; margin-top: 0;">企业文化</a></li>
        <li><a href="{pigcms{:U('News/aboutus',array('page'=>'contact'))}" style="background-position: 15px -100px;">联系我们</a></li>
        <li><a href="{pigcms{:U('News/staff_list')}" style="background-position: 15px -200px;">域名经纪人</a></li>
        <li><a href="javascript:void(0)" class="expand" style="background-position: 15px -400px;">新手指南</a>
            <ul>
                <volist name="left_guide_list" id="vo">
                <li><a href="{pigcms{:U('News/guide',array('news_id'=>$vo['news_id']))}">{pigcms{$vo.news_title}</a></li>
                </volist>
            </ul>
        </li>

        <li><a href="{pigcms{:U('News/index',array('type'=>'notice'))}" style="background-position: 15px -1250px;">站内公告</a>
        </li>
        <li><a href="{pigcms{:U('News/index',array('type'=>'index_news_top1'))}" style="background-position: 15px -900px;">{pigcms{$config.site_name}先知道</a></li>
        <li><a href="{pigcms{:U('Account/recharge')}" style="background-position: 15px -301px;">付款方式</a></li>
        <li><a href="{pigcms{:U('News/links')}" style="background-position: 15px -800px;">友情链接</a></li>

    </ul>
</div>
