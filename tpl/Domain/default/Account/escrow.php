<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>我的账户</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
        <!-- 左边 -->
        <div class="g-a-container">
            <div class="m-ta s-bg-white u_be9">
                <div class="title">
                    <a class="s-3c" href="javascript:void(0)">
                        <span class="z-img-t12"></span>
                        <span>域名中介</span>
                    </a>
                </div>
                <div class="u-cls"></div>
            </div>
            <div class="m-insa s-bg-fc u_be89">
                <ul>
                    <li><span class="u_fw">1.</span>买家向{pigcms{$now_site_name}（{pigcms{$now_site_short_url}）支付域名全款后，卖家才开始域名过户。</li>
<li><span class="u_fw">2.</span>买家拿到域名且完成交易后，{pigcms{$now_site_name}（{pigcms{$now_site_short_url}）方解冻款项释放给卖家。</li>
<li><span class="u_fw">3.</span>手续费按照支付方会员等级收取，交易未达成不收取手续费。</li>
                </ul>
            </div>
            <div class="m-insb s-bg-fc u_be89" id="animateb">
                <span class="f-img-dg7">发起交易</span>
                <span class="f-img-dg6"></span>
                <span class="f-img-dg8">同意条款</span>
                <span class="f-img-dg6"></span>
                <span class="f-img-dg9">买家付款</span>
                <span class="f-img-dg6"></span>
                <span class="f-img-dg10">过户域名</span>
                <span class="f-img-dg6"></span>
                <span class="f-img-dg11">买家确认</span>
            </div>
            <div class="m-transa s-bg-fc u_be89">
                <p class="f-p-t6 s-bg-white u_bb0 s-3c">发起中介交易</p>
                <form method="post" action="/escrowoperation" id="form">
                    <div class="transb">
                        <div class="u_bb0 u_pl110">
                            <div class="t-left">交易域名：</div>
                            <div class="t-right">
                                <div class="t-pri" id="t-pri">
                                    <p>
                                        <span class="u-ipt-d u_mr60">
                                            <input type="text" class="domainName" name="domainName" maxlength="50" /></span>
                                        <span class="pri-txt">价格：</span>
                                        <span class="u-ipt-d">
                                            <input type="text" name="money" class="money" /></span>
                                    </p>
                                </div>
                                <div class="t-tol">
                                    <div class="u-btn4 s-2a" id="addDomain">添加多个域名</div>
                                    <div class="u-txm s-f1">合计：</div>
                                    <div class="u-fl">
                                        <p class="u_mb10">
                                            <span class="u-txn">域名费用：</span>
                                            <span class="u_fw s-75b money_total">￥0</span>
                                        </p>
                                        <p class="u_mb10">
                                            <span class="u-txn">平台手续费：</span>
                                            <span class="u_fw s-75b money_sxf" sxf="3.5">￥0</span>
                                        </p>
                                        <p>
                                            <span class="u-txn money_result_m"></span>
                                            <span class="u_fw s-75b money_result"></span>
                                        </p>
                                    </div>
                                    <div class="u-cls"></div>
                                </div>
                            </div>
                            <div class="u-cls"></div>
                        </div>
                        <div class="t-roles">
                            <p class="u_mb10">
                                <span class="u-txo">对方的会员ID：</span>
                                <span class="u-ipt-d">
                                    <input type="text" class="user" maxlength="10" name="user" />
                                </span>
                                <span style="color:#ff6d00; margin-left:5px;display:none" class="user_tips" >请输入正确的会员ID</span>
                            </p>
                            <p>
                                <span class="u-txo">您的角色：</span>
                                <label>
                                    <input type="radio" name="dprice" value="buy" class="u-rd1" />
                                    <span class="u-txp">买家</span>
                                </label>
                                <label class="u_ml85">
                                    <input type="radio" name="dprice" value="sell" class="u-rd1" />
                                    <span class="u-txp">卖家</span>
                                </label>
                            </p>
                            <p>
                                <span class="u-txo">由谁承担中介费：</span>
                                <label>
                                    <input type="radio" name="roledprice" value="one" class="u-rd1" />
                                    <span class="u-txp">买家</span>
                                </label>
                                <label class="u_ml85">
                                    <input type="radio" name="roledprice" value="two" class="u-rd1" />
                                    <span class="u-txp">卖家</span>
                                </label>
                                <label class="u_ml85">
                                    <input type="radio" name="roledprice" value="three" class="u-rd1" />
                                    <span class="u-txp">买卖双方各付50%</span>
                                </label>
                            </p>
                            <p>
                                <br />
                                <input type="checkbox" style="margin-left:138px;vertical-align: middle; cursor:pointer;" id="agree" /> <span style="vertical-align: middle;">我已阅读并同意<a href="/regulation?v=deal" target="_blank" title="阅读" style="color:#2aa3ce;">《优名网域名交易服务协议》</a></span>                             
                                <input type="submit" style="margin-top:20px;" value="提交申请" class="u-btn3 s-bg-2a escrow_sub" />
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- 右边 -->
        
<div class="g-b-container">
    <div class="m-bb-container">
        <div class="m-bb-title s-bg-fb">
            <a href="javascript:void(0);" class="u-tia s-3c">域名中介案例</a>
        </div>
        <div class="m-apply-dg m-apply-dga">
            <div class="m-apply-title">
                <span class="u-fl">域名</span>
                <span class="u-fr">价格</span>
                <div class="u-cls"></div>
            </div>
            <div class="m-animate3" id="animate3">
                <div class="animate3 box">
                    <ul class="list">
                            <li>
                                <div class="spanl"><a href="http://biao.com" target="_blank" rel="nofollow">biao.com</a></div>
                                <div class="spanr">500万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://ww.com" target="_blank" rel="nofollow">ww.com</a></div>
                                <div class="spanr">1000万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://345.com" target="_blank" rel="nofollow">345.com</a></div>
                                <div class="spanr">500万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://33.com" target="_blank" rel="nofollow">33.com</a></div>
                                <div class="spanr">1000万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://71.com" target="_blank" rel="nofollow">71.com</a></div>
                                <div class="spanr">500万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://64.com" target="_blank" rel="nofollow">64.com</a></div>
                                <div class="spanr">500万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://68.com" target="_blank" rel="nofollow">68.com</a></div>
                                <div class="spanr">1000万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://30.com" target="_blank" rel="nofollow">30.com</a></div>
                                <div class="spanr">500万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://63.com" target="_blank" rel="nofollow">63.com</a></div>
                                <div class="spanr">500万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://73.com" target="_blank" rel="nofollow">73.com</a></div>
                                <div class="spanr">500万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://80.com" target="_blank" rel="nofollow">80.com</a></div>
                                <div class="spanr">500万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://32.com" target="_blank" rel="nofollow">32.com</a></div>
                                <div class="spanr">500万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://81.com" target="_blank" rel="nofollow">81.com</a></div>
                                <div class="spanr">500万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://06.com" target="_blank" rel="nofollow">06.com</a></div>
                                <div class="spanr">500万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://08.com" target="_blank" rel="nofollow">08.com</a></div>
                                <div class="spanr">500万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://0.cn" target="_blank" rel="nofollow">0.cn</a></div>
                                <div class="spanr">500万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://6wan.com" target="_blank" rel="nofollow">6wan.com</a></div>
                                <div class="spanr">50万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://yingshi.com" target="_blank" rel="nofollow">yingshi.com</a></div>
                                <div class="spanr">200万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://99.cn" target="_blank" rel="nofollow">99.cn</a></div>
                                <div class="spanr">200万以上</div>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <div class="spanl"><a href="http://21.cn" target="_blank" rel="nofollow">21.cn</a></div>
                                <div class="spanr">100万以上</div>
                                <div class="u-cls"></div>
                            </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<script src="{pigcms{$static_path}js/jquery-1.8.2.min.js"></script>
	<script src="{pigcms{$static_path}js/common.js"></script>
	<script src="{pigcms{$static_path}js/account.js"></script>
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}
	</script>
	<include file="Public:sidebar"/>
</body>
</html>
