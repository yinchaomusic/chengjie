<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>开通服务</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
			<!-- 标题&验证 -->
			<div class="s-bg-fc u_be9" style="width: 702px;">
				<div class="m-ta s-bg-white">
					<div class="title">
						<a class="s-3c" href="javascript:void(0)">
							<span>开通优质服务</span>
						</a>
					</div>
					<div class="u-cls"></div>
				</div>
			</div>
			<!-- 开通数据 -->
			<div class="m-open-data s-bg-fc">
				<div class="openimg">
					<span class="f-img-o1">优势</span>
				</div>
				<div class="opentxt">
					<ul>
						<li class="u_mb12a"><span class="u_fw">1.&nbsp;</span>品质精益求精，卓尔不群。</li>
						<li class="u_mb12a"><span class="u_fw">2.&nbsp;</span>价格透明真实，物美价廉。</li>
						<li class="u_mb12a"><span class="u_fw">3.&nbsp;</span>全程人工协助，专业服务。</li>
						<li><span class="u_fw">4.&nbsp;</span>点击即可购买，极速交易。</li>
					</ul>
				</div>
				<div class="u-cls"></div>
			</div>
			<div class="m-open-data s-bg-fc">
				<div class="openimg">
					<span class="f-img-o2">资费</span>
				</div>
				<div class="opentxt">
					<table>
						<tbody><tr>
							<td>开通优质服务只需冻结。<span class="u_fw">￥1,000</span>元，当您申请关闭时，我们将释放您的<span class="u_fw">￥1,000</span>元资金。</td>
						</tr>
					</tbody></table>
				</div>
				<div class="u-cls"></div>
			</div>
			<div class="m-open-data s-bg-fc">
				<div class="openimg">
					<span class="f-img-o3">注意</span>
				</div>
				<div class="opentxt opentxt1">
					<ul>
						<li class="u_mb18 u_li18">
							<span class="u_fw">1.&nbsp;</span>用户可以随时下架正在展示中的优质域名，但再次展示需要再次提交申请
						</li>
						<li class="u_li18">
							<span class="u_fw">2.&nbsp;</span>若优质域名发起交易后卖家(您)违约，则 1000元 保证金作为违约金不予退 还，同时 下架该用户所有优质域名
						</li>
					</ul>
				</div>
				<div class="u-cls"></div>
			</div>
			<div class="m-open-data s-bg-fc">
				<div class="openimg">
					<span class="f-img-o4">钱包</span>
				</div>
				<div class="opentxt">
					<table>
						<tbody><tr>
							<td>您的可用余额：<span class="u_fw s-75b">￥{pigcms{$user_session.now_money}</span></td>
						</tr>
					</tbody></table>
				</div>
				<div class="u-cls"></div>
			</div>
			<div class="m-open-btn s-bg-f5">
				<input type="button" class="u-btn12 s-bg-2a kt_ykj_yz" k="1" value="立即开通">
			</div>
		</div>
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<include file="Public:sidebar"/>
</body>
</html>
