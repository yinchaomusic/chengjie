<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我是卖家 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/common.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>我是卖家&nbsp;&gt;&nbsp;</li>
					<li>域名添加结果</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr" style="width: 704px">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>域名添加结果</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 提交失败的域名 -->
			<if condition="$errorArr">
                <div class="m-account-data1 u_be9 s-bg-fc u_hi408">
                    <h4 class="s-bg-white">
                        <a class="s-3c m-ta2">
                            <span class="sp1">提交失败的域名</span>
                            <span class="sp2"><b></b>{pigcms{:count($errorArr)}</span>
                        </a>
                        <div class="u-cls"></div>
                    </h4>
                   <table>
                        <tr>
                            <th width="16px"></th>
                            <th width="240px" class="u_tl">域名</th>
                            <th width="160px"></th>
                            <th width="150px" class="u_tl">失败原因</th>
                            <th width="16px"></th>
                        </tr>
						<volist name="errorArr" id="vo">
                            <tr>
                                <td class="u_nb"></td>
                                <td class="u_nb"><a class="u-txa3">{pigcms{$vo.domain}</a></td>
                                <td class="u_nb"></td>
                                <td class="u_nb"><span class="u-span12">{pigcms{$vo.msg}</span></td>
                                <td class="u_nb"></td>
                            </tr>
						</volist>
                    </table>
                </div>
				<if condition="count($errorArr) gt 5">
					<div class="m-showorhide">
						加载全部
					</div>
				</if>
			</if>
            <!-- 提交成功的域名 -->
			<if condition="$okArr">
                <div class="m-account-data1 u_be9 s-bg-fc u_hi408">
                    <h4 class="s-bg-white">
                        <a class="s-3c m-ta2">
                            <span class="sp1">提交成功的域名</span>
                            <span class="sp2"><b></b>{pigcms{:count($okArr)}</span>
                        </a>
                        <div class="u-cls"></div>
                    </h4>
                    <table>
                        <tr>
                            <th width="16px"></th>
                            <th width="550px" class="u_tl">域名</th>
                            <th width="16px"></th>
                        </tr>
						<volist name="okArr" id="vo">
                            <tr>
                                <td class="u_nb"></td>
                                <td class="u_tl u_nb"><a class="u-txa2">{pigcms{$vo}</a></td>
                                <td class="u_nb"></td>
                            </tr>
						</volist>
                    </table>
                </div>
			</if>
        </div>
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<script>
        $(function () {
            
            $(".btn_tj").click(function () {
                var f = $(".file").val();
                var ph = $(".txta_v").attr("ph");
                if (ph)
                    ph = ph.replace(/\r/g, "");
                var d = $(".txta_v").val().replace(/\r/g, "") == ph ? "" : $(".txta_v").val();
                if (f && !/.*\.txt$|.*\.xls|.*\.xlsx$/i.test(f)) {
                    showDialog(lang.a45);
                    return false;
                }
                if (!f && d == "") {
                    showDialog(lang.a46);
                    return false;
                }
            })
            $('#sbt-pltj').click(function (e) {
                return $('.check-ipt').each(function () {
                    var _iptText, _ref;
                    if ($.trim($(this).val()) === '') {
                        e.preventDefault();
                        _iptText = (_ref = $(this).attr('check-info')) != null ? _ref : '';
                        showDialog(_iptText + '不能为空');
                        return false;
                    }
                });
            });
        })

        $(function () {
            $(".m-showorhide").click(function () {
                $(this).prev().removeClass("u_hi408");
                $(this).css("display", "none");
            });
        })
    </script>
	<include file="Public:sidebar"/>
</body>
</html>
