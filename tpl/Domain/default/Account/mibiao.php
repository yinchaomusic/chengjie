<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>
<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>我的账户</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="g-content u-cls">
	<div class="m-content">
		<include file="Account:sidebar"/>
		<!-- 右边 -->
		<div class="u-fr">
			<!-- 标题&验证 -->
			<div class="s-bg-fc u_be9" style="width: 702px;">
				<div class="m-ta s-bg-white">
					<div class="title">
						<a class="s-3c" href="javascript:void(0)">
							<span>米表管理</span>
						</a>
					</div>
					<div class="u-cls"></div>
				</div>
			</div>
			<!-- 选项卡 -->
			<div class="m-acc-select u_be9 s-bg-white">
				<!-- 标题 -->
				<div class="m-acc-title s-bg-fc">
					<div class="u-fl"><a href="{pigcms{:U('Account/mibiao')}" class="active">设置米表</a></div>
					<div class="u-fl"><a href="{pigcms{:U('Mibiao/index',array('mmid'=>$uid))}" target="_blank">进入米表</a></div>
					<input type="hidden" value="0" id="checktype">
					<div class="u-cls"></div>
				</div>
				<div class="m-insc s-bg-fc u_be89">
					<ul>
						<li class="u_fw"><span>1.</span>请输入您的米表关键字，便于别人搜索，如：买域名,卖域名,购买域名,域名出售等。</li>
<!--						<li class="u_fw"><span>2.</span>如果是第一次进来该页面，确认信息之后，请先提交保存一次。</li>-->
					</ul>
				</div>
				<!-- 内容 -->
				<form action="{pigcms{:U('Account/mibiaoupdate')}" method="post">
					<input type="hidden" name="mid" value="{pigcms{$mibiaoinfo.mid}">
					<div class="m-acc-cons s-bg-fc u_mall3">
						<ul class="data-mi">
							<li>
								<div class="u-mi-txt">米表名称：</div>
								<div class="u_txer1">
									<textarea class="txer1" name="title">{pigcms{$mibiaoinfo.title|default='域名交易平台:域名交易,域名出售,域名竞价,域名拍卖,域名经纪,域名中介,中国优质域名交易平台'}</textarea>
								</div>
								<div class="u-cls"></div>
							</li>
							<li>
								<div class="u-mi-txt">米表联系邮箱：</div>
								<div class="u-ipt-l">
									<input type="text" value="{pigcms{$mibiaoinfo.email}" name="email">
								</div>
								<div class="u-cls"></div>
							</li>
							<li>
								<div class="u-mi-txt">米表联系QQ：</div>
								<div class="u-ipt-l">
									<input type="text" value="{pigcms{$mibiaoinfo.qq}" name="qq">
								</div>
								<div class="u-cls"></div>
							</li>
							<li>
								<div class="u-mi-txt">米表联系电话：</div>
								<div class="u-ipt-l">
									<input type="text" value="{pigcms{$mibiaoinfo.phone}" name="phone">
								</div>
								<div class="u-cls"></div>
							</li>
							<li>
								<div class="u-mi-txt">米表关键字优化：</div>
								<div class="u-fl">
									<div class="u_txer2">
										<textarea ph="" name="keyword">{pigcms{$mibiaoinfo.keyword|default='域名交易,域名拍卖,域名竞价,域名经纪,域名中介,域名抢注,域名询价,域名买卖,合肥彼岸互联信息技术有限公司,域名中国,域名查询,域名注册,域名工具,域名买卖,域名代购,Whois查询,域名出售页面,域名Whois,域名应用,域名新闻,域名门户,域名资讯,中文域名,cn域名,com域名'}</textarea>
									</div>
									<div class="u_ts1">(请输入您的米表关键字，多个关键字，请以逗号隔开)</div>
								</div>
								<div class="u-cls"></div>
							</li>
							<li>
								<input type="submit" value="提交" class="u-btn12 s-bg-2a">
							</li>
							<ul>
							</ul></ul>
					</div>
				</form>
				<!-- 分页 -->
				<div id="paging" class="g-padding g-padding1">{pigcms{$pagebar}</div>
			</div>
		</div>

		<div class="u-cls"></div>
	</div>
</div>
<include file="Public:footer"/>
<include file="Public:account_footer"/>
<include file="Public:sidebar"/>
</body>
</html>