<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我是卖家 - 我的所有域名</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>我是卖家&nbsp;&gt;&nbsp;</li>
					<li>我的一口价域名</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
			<!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>我的一口价</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 选项卡 -->
            <div class="m-acc-select u_be9 s-bg-white">
                <!-- 标题 -->
                <div class="m-acc-title s-bg-fc">
                    <div class="u-fl"><a href="{pigcms{:U('Account/aprice')}" class="active">出售中</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/aprice',array('type'=>'1'))}">已出售</a></div>
                    <input type="hidden" value="{pigcms{$_GET.type|intval=###}" id="checktype" />
                    <div class="u-cls"></div>
                </div>
                    <!-- 内容 -->
					<div style="text-align:right; margin:7px 12px -10px 0;"><a href="{pigcms{:U('Account/allsell')}" style="display:inline-block;color:#fff;" class="u-btn10">提交一口价</a></div>
                    <div class="m-acc-cons s-bg-fc u_mall1">
						<table>
							<tbody><tr>
								<th width="16px"></th>
								<th width="22px"></th>
								<th width="110px" class="u_tl">域名</th>
								<th width="120px" class="u_tr">价格</th>
								<th width="25px"></th>
								<th width="95px" class="u_tl">下架时间</th>
								<th width="25px"></th>
								<th width="127px" class="u_tl">原因</th>
								<th width="120px;" class="u_tr">操作</th>
								<th width="16px"></th>
							</tr>
						<tr><td colspan="10" class="u_nb u_tc u_norecord">暂无记录！</td></tr></tbody></table>
					</div> 
                    <!-- 分页 -->
                    <div id="paging" class="g-padding g-padding1"></div>   
                    <div class="m-operation4">
                        <div class="u-fl operation4-l">
                            <label>
                                <input type="checkbox" class='curPage' />
                                <span>选中当前页</span>
                            </label>
                            <!--label>
                                <input type="checkbox" class='allPage' />
                                <span>选中所有</span>
                            </label-->
                        </div>
                        <div class="u-fl operation3-r">
                            <span>操作已选域名：</span>
                            <input type="button" value="批量下架" class="u-btn10 btn_delete btn_pl" v="sold" k="{pigcms{:U('Account/pl_operation',array('type'=>'1'))}"/>
                            <input type="button" value="批量设置" class="u-btn10 btn_delete btn_pl" k="{pigcms{:U('Account/pl_ykj')}"/>
                        </div>
                        <div class="u-cls"></div>
                    </div>   
            </div>

        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<script type="text/javascript">
        $(function () {
			var type = $("#checktype").val();
			getDataList_aprice(1, type);

            $("#paging a").live("click", function () {
                if (!$(this).attr("k"))
                    return;
                var pageindex = $(this).attr("k");
                getDataList_aprice(pageindex, type);
            });

            $(".sold_out").live("click", function () {
                if (confirm(lang.a49 + "[" + $(this).parent().parent().children().eq(2).text() + "]？")) {
                    $.post("/account/apriceOperation", { "id": $(this).parent().parent().find("input").attr("value") }, function (data, status) {
                        if (data.data == 1)
                            location.href = location.href;
                        else
                            showDialog(lang.a23);
                    })
                }
            })

            
            $(document.body).click(function () {
                $(".m-group-result,.m-prise-result").css("display", "none");
                $("#group,#prise").css("background-color", "#fcfcfc");
            });
        })
    </script>
	<include file="Public:sidebar"/>
</body>
</html>
