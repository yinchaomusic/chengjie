<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>我的账户</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
			<!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>提现</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 选项卡 -->
            <div class="m-acc-select u_be9 s-bg-white">
                <!-- 标题 -->
                <div class="m-acc-title s-bg-fc">
                    <div class="u-fl"><a href="{pigcms{:U('Account/withdrawal')}" class=active>申请提现</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/withdrawal_record')}" >提现记录</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/setbankcard')}" >设置银行卡</a></div>
                    <div class="u-cls"></div>
                </div>
                    <div class="m-record2 s-bg-fc u_be9">
                        <p class="f-p-t13 s-ff6">请注意：</p>
                        <ul>
                            <li><span class="u_fw">1.</span>提现平台代收1%手续费，美金提现需要18美金/笔的手续费。</li>
                            <li><span class="u_fw">2.</span>为了确保资金安全，只支持本人帐号进行提现，我们将核对收款人姓名。</li>
                            <li><span class="u_fw">3.</span>账户不允许从事无真实交易背景的虚假交易，信用卡套现或洗钱等禁止的交易行为，否则充值款项将不能提现</li>
                        </ul>
                    </div>
                    <div class="m-bankcard s-bg-fc u_be9">
                        
                        <form action="{pigcms{:U('Account/withdrawal_data')}" method='post'>
                        <div class="account u_mb15">
                            <span class="u-span25">提现账户：</span>
                            <if condition="is_array($bank_card_list)">
                                <select class="u-ipt-da u_down" name="bank_card_id" >
                                    <volist name="bank_card_list" id="vo">
                                        <option  value="{pigcms{$vo.id}">{pigcms{$vo.account}</option>
                                    </volist>
                                </select>
                                <else/>
                                <input type="hidden" value="" id="bid"/>
                            </if>
                                    
                            <a href="{pigcms{:U('Account/setbankcard')}" class="u-abtn12 s-2a">新增银行卡</a>
                            <div class="account-result">
                            </div>
                            <div class="u-cls"></div>
                        </div>
                        <div class="account u_mb15">
                            <span class="u-span25">可提现金额：</span>
                            <span class="u_m6">￥{pigcms{$user_info.now_money}</span>
                            <div class="u-cls"></div>
                        </div>
                        <div class="account u_mb15">
                            <span class="u-span25">提现金额：</span>
                            <span class="u-ipt-da" id="u-money">
                                <input type="text" id="txtmoeny" name="txtmoeny" maxlength="10" onKeyUp="this.value=this.value.replace(/\D/g,'')" onpaste="return false"/>
                            </span>
                            <span class="cell">元</span>
                            <div class="u_cell_money">￥</div>
                            <div class="u-cls"></div>
                        </div>
                        <div class="account">
                            <input type="submit" value="提交" class="u-btn12 s-bg-2a u_mall10 btn_tj1" />
                        </div>
                        </form>
                    </div>   
            </div>
        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<script src="{pigcms{$static_path}js/account.js"></script>
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}
	</script>
        
           
<script type="text/javascript">
                //提现
                $(".btn_tj1").click(function () {
                   
                    if ($("#bid").val() == "") {
                        showDialog(lang.a30);
                        return false;
                    }
                    
                    if ($("#txtmoeny").val() == "") {
                        showDialog(lang.a33);
                        return false;
                    }
                    
                    if ($("#txtmoeny").val() == "0") {
                        showDialog('金额要大于￥0元');
                        return false;
                    }
                    
                    
                    
                    var m = $(".u_m6").text().replace(/[$￥€,]/g, "");
                    if (Number($("#txtmoeny").val()) > Number(m)) {
                        showDialog(lang.a26);
                        return false;
                    }
                })
            </script>
        
	<include file="Public:sidebar"/>
</body>
</html>
