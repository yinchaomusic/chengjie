<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>帐户信息 - Whois邮箱</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>账户管理&nbsp;&gt;&nbsp;</li>
					<li>Whois邮箱</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
			<!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>Whois邮箱</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <div class="m-insf s-bg-fc">
                <p>
                    若您的不同域名在多个whois邮箱管理下，您可以把这些邮箱和您的账户进行绑定。
                </p>
                 <p>
                    在添加域名出售时，若添加的域名whois邮箱已绑定成功，则添加的域名会自动通过系统验证。
                </p>
            </div>
            <!--您绑定的邮箱  -->
            <form id="form1" action="{pigcms{:U('Account/bindwhoisemail')}" method="post">
                <div class="m-bindemail s-bg-fc">
                    <span>您绑定的邮箱：</span>
                    <span class="u-ipt-o">
                        <input type="text" name="txtemail" />
                    </span>
                    <input type="submit" value="绑定新邮箱" class="u-btn2 s-bg-2a sub_bind" />
                </div>
            </form>
            <!-- 数据 -->
            <div class="m-account-data2 s-bg-fc u_be9">
                <table>
                    <tr class="s-bg-white">
                        <th width="16px"></th>
                        <th width="295px" class="u_tl">邮箱</th>
                        <th width="120px" class="u_tl">是否已验证</th>
                        <th width="255" class="u_tr">操作</th>
                        <th width="16px"></th>
                    </tr>
					<volist name="whoisemail_list" id="vo">
						<tr>
                            <td></td>
                            <td class="u_tl">{pigcms{$vo.email}</td>
                            <td class="u_tl"><if condition="$vo['is_check']">是<else/>否</if></td>
                            <td class="u_tr">
                                <if condition="!$vo['is_check']"><a class="u-abtn8 btn_res" href="javascript:void(0)" idx="{pigcms{$vo.mail_id}">重新验证</a></if>
                                <a class="u-abtn8 btn_del" href="javascript:void(0)" idx="{pigcms{$vo.mail_id}">删除</a>
                            </td>
                            <td></td>
                        </tr>
					</volist>
                </table>
            </div>
        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<include file="Public:sidebar"/>

    <script>
        $(".btn_res").click(function () {
            $.post("{pigcms{:U('Account/revalidationemail')}",{"id":$(this).attr("idx")},function(result){
                if(result.status == 1){
                    window.location.reload();
                }else{
                    alert(result.info);
                }
            })
        })

        $(".btn_del").click(function () {
            if (confirm(lang.a22 + "？")) {
                $.post("{pigcms{:U('Account/delwhoisemail')}", { "id": $(this).attr("idx") }, function (result) {
                    if(result.status == 1){
                        window.location.reload();
                    }else{
                        alert(result.info);
                    }
                })
            }
        });

        $(".sub_bind").click(function () {
            var rex = /^[0-9a-zA-Z\._-]+@(([0-9a-zA-Z]+)[.])+[a-z]{2,4}$/;
            if (!rex.test($("input[name='txtemail']").val())) {
                showDialog(lang.a16);
                return false;
            }
        })
    </script>
</body>
</html>
