<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>代购列表&nbsp;&gt;&nbsp;</li><li>详情修改</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>修改代购信息</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
           <!-- 左边 -->
            <div class="m-insf s-bg-fc">
                <p>1.保证金代购失败或成功都会退还，申请方违约则不退还。</p>
                <p>2.申请方带代购达成前，可以取消代购。代购达成后取消则视为违约。</p>
                <p>3.代购手续费为固定8%，申请方支付</p>
            </div>
            <!--您绑定的邮箱  -->
            <!-- 数据 -->
            <form method="post" action="{pigcms{:U('Account/pay_edit')}">
                <div class="m-question m-question1 s-bg-fc u_be9">
                    <ul>
                        <div class="transa">
                                <p class="u_mb16">
                                        <span class="u-txj">客户会员ID：</span>
                                        <span class="u-ipt-d">
                                            {pigcms{$ph_info.uid}
                                            <input type="hidden" name="ph_id" value="{pigcms{$ph_info.ph_id}">
                                            <input type="hidden" name="uid" value="{pigcms{$ph_info.uid}">
                                            
                                        </span>
                                </p>
                                <p class="u_mb16">
                                        <span class="u-txj">想购买的域名：</span>
                                        <span class="u-ipt-d">
                                            <input type="text" name="domain" value="{pigcms{$ph_info.domain}" >
                                        </span>
                                </p>
                                <p class="u_mb16">
                                        <span class="u-txj">报价：</span>
                                        <span class="u-ipt-d">
                                            <input type="text" value="{pigcms{$ph_info.buyers_price|floor}" class="money" name="buyers_price">
                                        </span>
                                </p>
                                <p>
                                        <span class="u-txj">留言或其它附加信息：</span>
                                        <span class="u_txera s-bg-white">
                                            <textarea name="message">{pigcms{$ph_info.message}</textarea>
                                        </span>
                                </p>
                                <li>
                            <div class="u_mall12">
                                <input type="submit" class="u-btn12 s-bg-2a" value="保存修改">
                            </div>
                        </li>
                               
                        </div>
                    </ul>
                    <div class="u-cls"></div>
                </div>
                </form>
        <!-- 右边 -->
        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<script src="{pigcms{$static_path}js/jquery-1.8.2.min.js"></script>
	<script src="{pigcms{$static_path}js/common.js"></script>
	<script src="{pigcms{$static_path}js/account.js"></script>
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}
	</script>
        
        <script>
                            $("#yes").click(function () {
                                var id = $("#ph_id").val();
                                var why=$("#why").val();
                                
                                $.post("{pigcms{:U('Staffdeal/purchase_audit')}",{"ph_id":id,"status":2,"why":why},function(data){
                                    
                                    if(data == 1){
                                       showDialog('操作成功', location.href);
                                    }else{
                                        showDialog('操作异常', location.href);
                                    }
                                })
                            })
                            $("#no").click(function () {
                                var id = $("#ph_id").val();
                                var why=$("#why").val();
                                $.post("{pigcms{:U('Staffdeal/purchase_audit')}",{"ph_id":id,"status":3,"why":why},function(data){
                                    
                                    if(data == 1){
                                       showDialog('操作成功', location.href);
                                    }else{
                                        showDialog('操作异常', location.href);
                                    }
                                })
                            })
                            
//              $(.yes).click(funciton(){
//                  
//              })
        </script>
	<include file="Public:sidebar"/>
</body>
</html>
