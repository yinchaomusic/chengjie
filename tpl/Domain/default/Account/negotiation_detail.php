<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我是买家 - 我给出的报价</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/jquery-ui.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>我是买家&nbsp;&gt;&nbsp;</li>
					<li>我给出的报价&nbsp;&gt;&nbsp;</li>
					<li>{pigcms{$now_quote.domain}</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>谈判详情</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 交易详情 -->
            <div class="m-seller s-bg-fc u_be9">
                <h4 class="f-img-seller s-bg-white">你是买方</h4>
                <h3 class="u_bt2">
                    <input type="hidden" value="{pigcms{$now_quote.quote_id}" id="nid"/>
                    <span class="s-2a domain">{pigcms{$now_quote.domain}</span>
                    <span class="s-ff6 z-wait">
						<if condition="$now_quote['status'] eq 2">
							({pigcms{$now_quote.last_time|date='Y-m-d H:i:s',###} 谈判失败，<if condition="$now_quote['last_offer'] eq 1">卖家<else/>买家</if>取消了本次议价)
						<elseif condition="$now_quote['status'] eq 1"/>
							谈判成功
						<elseif condition="$now_quote['last_offer'] eq 1"/>
							正在等待您的回复
						<else/>
							正在等待卖家回复
						</if>
					</span>
                </h3>
				<div class="m-insd">
					<if condition="$now_quote['status'] eq '1'">
						<p class="u_btn_container2"> 
							<a class="u-btn13" href="{pigcms{:U('Account/deals')}">谈判成功，查看交易列表</a>
						</p>
					<elseif condition="$now_quote['status'] neq '2'"/>
						<ul class="insd">
							<li>该报价将于 <span class="u_fw">{pigcms{$now_quote.invalid_time|date='Y-m-d H:i:s',###}</span> 过期失效</li>
							<li>在这个期间，您可以有以下几个选择：</li>
						</ul>
						<if condition="$now_quote['last_offer'] eq '0'">
							<p class="u_btn_container2"> 
								<a class="u-btn13" id="outprice">再次出价</a>
								<a class="u-btn13" id="cancel">取消议价</a>
							</p>
						<else/>
							<p class="u_btn_container2"> 
								<a class="u-btn13" id="okprice">接受报价</a>
								<a class="u-btn13" id="outprice">回复报价</a>
								<a class="u-btn13" id="cancel">取消议价</a>
							</p>
						</if>
					<else/>
						<p class="u_btn_container2"> 
							<a class="u-btn13" href="{pigcms{:U('Buydomains/detail',array('id'=>$now_quote['domain_id']))}">重新报价</a>
						</p>
					</if>
				</div> 
				<div class="m-pop s-bg-fa" id="pop16">
                    <p class="f-p-t7 u_bb0">
                        <span class="u-fl">给出一个心动价格,更能促成交易</span>
                        <span class="u-fr close"></span>
                        <span class="u-cls"></span>
                    </p>
                    <div class="m-offer s-bg-white">
                        <div class="offer-txt">您的报价：</div>
                        <div class="offer-ipt">
                            <div class="u-ipt-o">
                                <input type="text" class="u_prise" id="_money">
                            </div>
                            <div class="offer-ts">您的价格要大于{pigcms{$now_quote.new_money}</div>
                        </div>
                        <div class="u_cell_money">￥</div>
                        <div class="u-cls"></div>
                    </div>
                    <div class="s-bg-fa u_pa15 u_tr">
                        <input type="button" value="确定" class="u-btn6 s-bg-2a" id="oneprice">
                    </div>
                </div>
                <div class="m-pop s-bg-fa" id="pop17">
                    <p class="f-p-t7 u_bb0">
                        <span class="u-fl">提醒：双方多次谈判后达成的交易，成交率较高</span>
                        <span class="u-fr close"></span>
                        <span class="u-cls"></span>
                    </p>
                    <div class="m-offer1 s-bg-white">
                        <ul>
                            <li>
                                <span class="u-span28">交易域名：</span>
                                <span class="u-span29 s-75b">{pigcms{$now_quote.domain}</span>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <span class="u-span28">交易金额：</span>
                                <span class="u-span29">￥{pigcms{$now_quote.new_money}</span>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <span class="u-span28">手续费：</span>
                                <span class="u-span29">卖家支付</span>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <span class="u-span28">实际收入：</span>
                                <span class="u-span29">￥{pigcms{$now_quote.new_money}</span>
                                <div class="u-cls"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="s-bg-fa u_pa15 u_tr">
                        <input type="button" value="接受报价" class="u-btn6 s-bg-2a" id="interval">
                    </div>
                </div>
            </div>
            <!-- 谈判标题 -->
            <div class="m-talking-title">
                <h3 class="s-bg-white">报价记录</h3>
                <h4 class="s-bg-fc"><span>卖家的挂牌价格：</span><span class="s-ff6"><if condition="$nowDomain['money']">{pigcms{$nowDomain.money}<else/>买家报价</if></span></h4>
            </div>
            <div class="m-talking m-talking1 s-bg-fc u_be9">
				<volist name="quote_list" id="vo">
					<if condition="$vo['is_sell']">
						<div class="reply">
							<div class="reply-img1 s-4d6">卖方</div>
							<div class="reply-cons s-bg-d9e">
								<p class="f-p28">￥{pigcms{$vo.money}</p>
								<p class="f-p29">{pigcms{$vo.time|date='Y-m-d H:i:s',###}</p>
								<b></b>
							</div>
							<div class="u-cls"></div>
						</div>
					<else/>
						<div class="myask">
							<div class="myask-img1 s-4d6">
								买方
							</div>
							<div class="myask-cons s-bg-d0">
								<p class="f-p28">￥{pigcms{$vo.money}</p>
								<p class="f-p29">{pigcms{$vo.time|date='Y-m-d H:i:s',###}</p>
								<b></b>
							</div>
							<div class="u-cls"></div>
						</div>  
					</if>
				</volist>
            </div>
        </div>
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<script type="text/javascript" src="{pigcms{$static_path}js/popupBox.js"></script>
	<script type="text/javascript">
        $("#okprice").click(function () {
            showBox("pop17");
            $("#pop17").css("left", (($(window).width() - 544)/2)+"px");
        });
        $(".close").click(function () {
            var id = $(this).parent().parent().attr("id");
            closeStr(id);
        });
        $("#outprice").click(function () {
            showBox("pop16");
            $("#pop16").css("left", (($(window).width() - 544)/2)+"px");
        });
        function closeStr(str){
            closeBox(str);
        }

        $("#cancel").click(function () {
            if (confirm(lang.a24 + "？")) {
                $.post("{pigcms{:U('Account/updatenegotiation')}", { type: 3, id: $("#nid").val() }, function (result) {
                    if (result.status == 1) {
                        showDialog(lang.a34, location.href);
                    } else {
                        showDialog(lang.a35, location.href);
                    }
                });
            }
        });

        $("#oneprice").click(function () {
            if ($("#_money").val().trim() == "")
                return false;
            $.post("{pigcms{:U('Account/updatenegotiation')}", { type: 2, id: $("#nid").val(), money: $("#_money").val() }, function (result) {
                closeBox("pop16");
                if (result.status == 1) {
                    showDialog(lang.a36, location.href);
                } else {
                    showDialog(result.info, location.href);
                }
            });
        });

        $("#interval").click(function () {
            $.post("{pigcms{:U('Account/updatenegotiation')}", { type: 1, id: $("#nid").val() }, function (result) {
                closeBox("pop17");
                if (result.status == 1) {//0.执行错误 1.没有该账单 2.添加成功 3.此域名为拍卖，失败 4.此域名为一口价，失败 5.此域名为优质，失败 6.此域名为议价，失败
                    showDialog(lang.a40, location.href);
                } else {
                    showDialog(result.info, location.href);
                }
            });
        });
    </script>
	<include file="Public:sidebar"/>
</body>
</html>
