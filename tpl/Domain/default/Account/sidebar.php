<!-- 左边 -->    
<div class="u-fl">
	<div class='m-pop s-bg-fa' id='pop_broker'>
		<p class='f-p-t7 u_bb0'>
			<span class='u-fl'>申请更换经纪人</span>
			<span class='u-fr close close_broker'></span>
			<span class='u-cls'></span>
		</p>
		<div style='padding: 10px;'>
			<dl>
				<volist name="staff_list" id="vo">
					<if condition="$vo['id'] neq $staff_show['id']">
						<dd>
							<input type="radio" id="r{pigcms{$vo.id}" value="{pigcms{$vo.id}" name="r_broker"/><label for="r{pigcms{$vo.id}">{pigcms{$vo.name}</label><a href="tencent://message/?uin={pigcms{$vo.qq}&amp;Site={pigcms{$config.site_name}"><img border="0" src="http://wpa.qq.com/pa?p=2:{pigcms{$vo.qq}:51" alt="QQ交谈" title="QQ交谈"/></a>
						</dd>
					</if>
				</volist>
			</dl>
			<p style="clear: both;">
				<br />
				<span style="margin: 10px 0 0 15px">更换原因：</span><div style="margin: 10px 20px 10px 15px; background: #fff;">
					<textarea id="reason" style="width: 100%; padding: 5px;" rows="3" maxlength="100"></textarea>
				</div>
			</p>
			<input id="btn_change" type="button" value="提交" class="u-btn10" style="padding: 0 20px; margin: 0 20px 0 316px;" /><input type="button" value="取消" class="u-btn10 close_broker" style="padding: 0 20px;" />
			<br />
			<br />
		</div>
	</div>
	<div class="m-callagent1 s-bg-fc">
		<h3>欢迎来到{pigcms{$config.site_name}！</h3>    
		<if condition="$staff_show">
			<div class="callagent">
					<p class="u_fw u_mb15">{pigcms{$staff_show.name} <span id="changeBroker">更换</span>                </p>
					<p class="u_mb15">电话：<span class="u_fw">{pigcms{$staff_show.phone}</span></p>
					<p class="u_mb20">邮件：<span class="u_fw">{pigcms{$staff_show.email}</span></p>
					<p>
						<a href='tencent://message/?uin={pigcms{$staff_show.qq}&amp;Site={pigcms{$config.site_name}' class="btn-qqtalk">QQ交谈</a>
						<a href="javascript:void(0)" id="calling" class="btn-callagent u-fl" title="经纪人会主动打电话联系您">呼叫经纪人</a>
					</p>
					<div class="u-cls"></div>
			</div>
			<else/>
				<div class="callagent">
					<p>
						<a href="{pigcms{:U('News/staff_list')}" class="btn-qqtalk">联系经纪人</a>
					</p>
					<div class="u-cls"></div>
				</div>
		</if>
	</div>
    <div class="m-processList1" id="processList">
        <h3 style="background-position: 0 -14px;">我是买家</h3>
        <div class="process1">
            <ul>
                <li><a href="{pigcms{:U('Account/negotiation')}">我给出的报价</a></li>
                <li><a href="{pigcms{:U('Account/partakefast')}">我参与的竞价</a></li>
                <li><a href="{pigcms{:U('Account/videoauction')}">我参与的视频竞价</a></li>
                <li><a href="{pigcms{:U('Account/pay')}">我委托的代购</a></li>
                <li><a href="{pigcms{:U('Account/deals_complete')}">已买到的域名</a></li>
                <li><a href="{pigcms{:U('Account/collection')}">我的收藏</a></li>
            </ul>
        </div>
        <h3 style="background-position: 0 -161px;">我是卖家</h3>
        <div class="process1">
            <ul>
                <li><a href="{pigcms{:U('Account/allsell')}">我的所有域名</a></li>
                <li><a href="{pigcms{:U('Account/adddomainss')}">添加域名出售</a></li>
                <li><a href="{pigcms{:U('Account/pldomain')}">我的批量交易</a></li>
                <li><a href="{pigcms{:U('Account/adddomains_pl')}" style="background-image:url({pigcms{$static_path}/css/img/new.png); background-repeat: no-repeat; background-position: 142px 1px;">添加批量交易</a></li>
                <li><a href="{pigcms{:U('Account/withcheck')}">待验证的域名</a></li>
                <li><a href="{pigcms{:U('Account/sellnegotiation')}">我收到的报价</a></li>
                <li><a href="{pigcms{:U('Account/fastbid')}">极速竞价管理</a></li>
                <li><a href="{pigcms{:U('Account/aprice')}">一口价管理</a></li>
                <li><a href="{pigcms{:U('Account/bargain')}">优质域名管理</a></li>
                <li><a href="{pigcms{:U('Terminal/terminal')}">我的终端域名</a></li>
                <li><a href="{pigcms{:U('Account/group')}">域名分组管理</a></li>
                <li><a href="{pigcms{:U('Account/mibiao')}">米表管理</a></li>
            </ul>
        </div>
        <h3 style="background-position: 0 -309px;">网站管理</h3>
        <div class="process1" id="web">
            <ul>
                <li><a id="entr1" class="entr" href="{pigcms{:U('WebEntrusts/webList',array('entr_type'=>1))}">代购的网站</a></li>
				<li><a id="entr2" class="entr" href="{pigcms{:U('WebEntrusts/webList',array('entr_type'=>2))}">代售的网站</a></li>
				<li><a id="entr" class="entr" href="{pigcms{:U('WebInterOrder/orderList',array('role'=>'1'))}">中介交易</a></li>
            </ul>
        </div>
        <h3 style="background-position: 0 -309px;">交易管理</h3>
        <div class="process1" >
            <ul>
                <li><a href="{pigcms{:U('Escrow/index')}">发起中介交易</a></li>
                <li><a href="{pigcms{:U('Account/deals')}">我的所有交易</a></li>
            </ul>
        </div>
        <h3 style="background-position: 0 -457px;">财务信息</h3>
        <div class="process1">
            <ul>
                <li><a href="{pigcms{:U('Account/recharge')}">账户充值</a></li>
                <li><a href="{pigcms{:U('Account/withdrawal')}">提现</a></li>
                <li><a href="{pigcms{:U('Account/record')}">财务明细</a></li>
                <li><a href="{pigcms{:U('Account/frozendetail')}">冻结明细</a></li>
            </ul>
        </div>
        <h3 style="background-position: 0 -604px;">账户管理</h3>
        <div class="process1">
            <ul>
                <li><a href="{pigcms{:U('Account/bind')}">账号安全</a></li>
                <li><a href="{pigcms{:U('Account/whoisemail')}">Whois邮箱</a></li>
                <li><a href="{pigcms{:U('Account/updateprofile')}">修改资料</a></li>
                <li><a href="{pigcms{:U('Account/updatepwd')}">修改密码</a></li>
            </ul>
        </div>
        <h3 style="background-position: 0 -752px;">问题反馈</h3>
        <div class="process1">
            <ul>
                <li><a href="{pigcms{:U('Problem/problem_add')}">提交问题</a></li>
                <li><a href="{pigcms{:U('Problem/index')}">问题列表</a></li>
            </ul>
        </div>
    </div>
</div>