<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>我的账户</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white u_bb0">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span class="z-img-t19"></span>
                            <span>我的账户</span>
                        </a>
                    </div>
                    <div class="lastTime">
                        <span>上次登录时间：</span><span>{pigcms{$user_session.last_time|date='Y-m-d H:i:s',###}</span>
                    </div>
                    <div class="lastIp">
                        <a href="{pigcms{:U('Account/loginlog')}">
                            <span>上次登录IP：</span><span class="s1" title="{pigcms{$user_session.last_ip|long2ip=###}({pigcms{$user_session.last.area})">{pigcms{$user_session.last_ip|long2ip=###}({pigcms{$user_session.last.country}{pigcms{$user_session.last.area})</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
                <div class="s-bg-fc m-validation"></div>
            </div>
            <!-- 个人信息 -->
            <div class="m-personinfo s-bg-fc u_be9">
                <div class="personinfo-l">
                    <p class="s-3c f-p24">您好，<span id="name">{pigcms{$user_session.nickname}</span>，欢迎回来！</p>
                    <p class="u_mb15">
                        <span class="u-span3">会员ID：</span>
                        <span class="u-span5">{pigcms{$user_session.uid}</span>
                    </p>
                    <p class="u_mb15">
                        <span class="u-span3">会员邮箱：</span>
                        <span class="u-span5">{pigcms{$user_session.email}</span>
                    </p>
                    <div style="position: relative;">
                        <span class="u-span3">会员等级：</span>
                        <span class="u-span5" style="overflow:inherit;">
							<div class="moneylink" style="display:inline;">
								<span class="link u_2a">{pigcms{$user_level_info.lname}手续费 ({pigcms{$user_level_info.boon}%)</span>
								<dl class="box">
									<dt><span>等级</span><span>手续费</span></dt>
									<volist name="level_list" id="vo">
										<dd><span>{pigcms{$vo.lname}</span><span>{pigcms{$vo.boon}</span></dd>
									</volist>
								</dl>
							</div>
						</span>
                    </div>
                </div>
                <div class="personinfo-r">
                    <p class="f-p25">
						<a href="{pigcms{:U('Account/bindEmail',array('type'=>'email'))}" class="<if condition="$user_session['is_check_email'] eq 1"> bind-email  s-2a <else/> unbind-email s-ab</if>" title="邮箱"><if condition="$user_session['is_check_email'] eq 1"> 已绑定<else/>未绑定 </if></a>
						<a href="{pigcms{:U('Account/bind')}" class="<if condition="$user_session['is_check_phone'] eq 1"> bind-phone s-2a<else/>unbind-phone s-ab</if>" title="手机"><if condition="$user_session['is_check_phone'] eq 1"> 已绑定<else/>未绑定 </if>
                        </a>
                    </p>
                    <p class="u_mb15">
                        <span class="u-span4">一口价服务：</span>
						<if condition="$user_session['ykjfw']">
							<a href="javascript:void(0);" k="0" class="bind-kt s-2a"><b></b>已开通</a>
						<else/>
							<a href="{pigcms{:U('Account/ykjfw')}" class="ubind-kt s-def"><b></b>现在去开通</a>
						</if>
                    </p>
                    <p class="u_mb15">
                        <span class="u-span4">优质服务：</span>
						<if condition="$user_session['yzfw']">
							<a href="javascript:void(0);" k="1" class="bind-kt s-2a"><b></b>已开通</a>
						<else/>
							<a href="{pigcms{:U('Account/yzfw')}" class="ubind-kt s-def"><b></b>现在去开通</a>
						</if>
                    </p>
                    <p>
                        <span class="u-span4 user_lv" style="cursor: pointer;">会员积分：</span>
                        <a class="s-def integral">{pigcms{$user_session.score_count}</a>
                    </p>
                </div>
                <div class="u-cls"></div>
            </div>
            <div class="m-account-data u_be9 s-bg-fc">
                <table>
                    <tr>
                        <th width="16px"></th>
                        <th width="90px" class="u_tl">货币单位</th>
                        <th width="200px" class="u_tr">账户余额</th>
                        <th width="200px" class="u_tr">冻结资金</th>
                        <th class="u_tr">可用余额</th>
                        <th width="16px"></th>
                    </tr>
                        <tr>
                            <td></td>
                            <td>人民币</td>
                            <td class="u_tr"><a href="{pigcms{:U('Account/record',array('type'=>'all'))}">￥{pigcms{:number_format($user_session['now_money']+$user_session['freeze_money'])}</a> </td>
                            <td class="u_tr"><a href="{pigcms{:U('Account/frozendetail')}">￥{pigcms{:number_format($user_session['freeze_money'])}</a></td>
                            <td class="u_tr"><a href="{pigcms{:U('Account/record',array('type'=>'all'))}">￥{pigcms{:number_format($user_session['now_money'])}</a></td>
                            <td></td>
                        </tr>
                </table>
                <div class="m-online-money">
                    <a href="{pigcms{:U('Account/recharge')}" class="u-abtn7 s-bg-2a">在线充值</a>
                    <a href="{pigcms{:U('Account/withdrawal')}" class="u-abtn7 s-bg-2a">申请提现</a>
                </div>
            </div>
            <div class="m-account-data1 u_be9 s-bg-fc">
                <h4 class="s-bg-white">
                    <a href="{pigcms{:U('Account/deals_dealing')}" class="s-3c m-ta2">
                        <span class="sp1">未完成的交易</span>
                        <if condition="$count eq 0"><else/><span class="sp2"><b></b>{pigcms{$count}</span></if>
                    </a>
                    <a href="{pigcms{:U('Account/deals_dealing')}" class="s-ae1 more">更多&gt;&gt;</a>
                    <div class="u-cls"></div>
                </h4>
                <table>
                    <tr>
                        <th width="16px"></th>
                        <th width="200px" class="u_tl">交易时间</th>
                        <th width="96px" class="u_tl">行为</th>
                        <th width="145px" class="u_tl">交易标题</th>
                        <th width="120px" class="u_tr">金额</th>
                        <th width="58px;"></th>
                        <th width="60px">操作</th>
                    </tr>
                    <if condition="$order_list">
						<volist name="order_list" id="vo">
							<tr>
								<td class="u_nb"></td>
								<td class="u_nb">{pigcms{$vo.addTime|date="Y-m-d H:i:s",###}</td>
								<td class="u_tl u_nb">
									<if condition="$vo.behavior eq 1">
										买入 
										<else/>
										卖出
									</if>
								</td>
								<td class="u_tl u_nb">
									<span class="u-span6">{pigcms{$vo.domainName}
                                                                        <if condition="$vo['domainName'] eq ''">
                                                                            批量交易买入
                                                                        </if>
                                                                        </span>
<!--									<span class="u-span7 s-c2"><font style="color:#ff6f02">等待<if condition="$vo.behavior eq 1">您付款<else/>买家付款</if></font></span>-->
								</td>
								<td class="u_tr u_nb">￥{pigcms{$vo.total_price|number_format}</td>
								<td class="u_nb"></td>
								<td class="u_nb"><a target="_blank" href="{pigcms{:U('Account/dealdetails',array('id'=>$vo['order_id']))}" class="u-abtn8">详情</a></td>
							</tr>
						</volist>
					<else/>
						<td class='u_nb u_tc u_norecord' colspan='8'>暂无记录</td>
                    </if>
                </table>
            </div>
            <div class="m-account-data1 u_be9 s-bg-fc">
                <h4 class="s-bg-white">
                    <a class="s-3c m-ta2" href="{pigcms{:U('Account/negotiation')}">
                        <span class="sp1">我的出价</span>
                    </a>
                    <a class="s-ae1 more" href="{pigcms{:U('Account/negotiation')}">更多&gt;&gt;</a>
                    <div class="u-cls"></div>
                </h4>
                <table>
                    <tr>
                        <th width="16px"></th>
                        <th width="160px" class="u_tl">域名</th>
                        <th width="140px" class="u_tl">当前状态</th>
                        <th width="120px" class="u_tr">最新报价</th>
                        <th width="170px" class="u_tr">最后报价时间</th>
                        <th width="28px"></th>
                        <th width="56px">操作</th>
                        <th width="16px"></th>
                    </tr>
					<if condition="$buyQuoteCount">
						<volist name="buyQuoteList" id="vo">
							<tr>
								<td class="u_nb"></td>
								<td class="u_nb"><a class="u-txa1" href="{pigcms{:U('Account/negotiation_detail',array('id'=>$vo['quote_id']))}" target="_blank" title="{pigcms{$vo.domain}">{pigcms{$vo.domain}</a></td>
								<td class="u_tl s-ff6 u_nb"><if condition="$vo['last_offer'] eq '0'">正在等待卖家的回复<else/>正在等待您的回复</if></td>
								<td class="u_tr u_nb">￥{pigcms{$vo.new_money|number_format=###}</td>
								<td class="u_tr u_nb">{pigcms{$vo.last_time|date='Y-m-d',###}</td>
								<td class="u_nb"></td>
								<td class="u_nb">
									<a class="u-abtn8" href="{pigcms{:U('Account/negotiation_detail',array('id'=>$vo['quote_id']))}" target="_blank">详情</a>
								</td>
								<td class="u_nb"></td>
							</tr>
						</volist>
					<else/>
						<td class='u_nb u_tc u_norecord' colspan='8'>暂无记录</td>
					</if>
                </table>
            </div>

            <div class="m-account-data1 u_be9 s-bg-fc">
                <h4 class="s-bg-white">
                    <a class="s-3c m-ta2" href="{pigcms{:U('Account/sellnegotiation')}">
                        <span class="sp1">收到的报价</span>
						<if condition="$sellQuoteCount"><span class="sp2"><b></b>{pigcms{$sellQuoteCount}</span></if>
                    </a>
                    <a class="s-ae1 more" href="{pigcms{:U('Account/sellnegotiation')}">更多&gt;&gt;</a>
                    <div class="u-cls"></div>
                </h4>
                <table>
                    <tr>
                        <th width="16px"></th>
                        <th width="160px" class="u_tl">域名</th>
                        <th width="140px" class="u_tl">当前状态</th>
                        <th width="120px" class="u_tr">最近报价</th>
                        <th width="170px" class="u_tr">最后报价时间</th>
                        <th width="28px"></th>
                        <th width="56px">操作</th>
                        <th width="16px"></th>
                    </tr>
					<if condition="$sellQuoteCount">
						<volist name="sellQuoteList" id="vo">
							<tr>
								<td class="u_nb"></td>
								<td class="u_nb"><a class="u-txa1" href="{pigcms{:U('Account/sellnegotiation_detail',array('id'=>$vo['quote_id']))}" target="_blank" title="{pigcms{$vo.domain}">{pigcms{$vo.domain}</a></td>
								<td class="u_tl s-ff6 u_nb"><if condition="$vo['last_offer'] eq '0'">正在等待您的回复<else/>正在等待买家的回复</if></td>
								<td class="u_tr u_nb">￥{pigcms{$vo.new_money|number_format=###}</td>
								<td class="u_tr u_nb">{pigcms{$vo.last_time|date='Y-m-d',###}</td>
								<td class="u_nb"></td>
								<td class="u_nb">
									<a class="u-abtn8" href="{pigcms{:U('Account/sellnegotiation_detail',array('id'=>$vo['quote_id']))}" target="_blank">详情</a>
								</td>
								<td class="u_nb"></td>
							</tr>
						</volist>
					<else/>
						<td class='u_nb u_tc u_norecord' colspan='8'>暂无记录</td>
					</if>
                </table>
            </div>
        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<script type="text/javascript">
        $(".integral,.user_lv").click(function () {
            showBox("pop10");
            $("#pop10").css("left", "29%");
        });
        $(".close").click(function () {
            closeBox("pop10");
        });
//        $.post("/account/vouchersSumInf", function (data, status) {
//            if (status == "success") {
//                $("#vouchers").html(formatMoney(data, 0, "￥"));
//            }
//        })
        //悬浮
        $(".moneylink").hover(function () {
            $(this).find(".box").stop().fadeIn("fast");
        }, function () {
            $(this).find(".box").stop().fadeOut("fast");
        });
    </script>
	<include file="Public:sidebar"/>
</body>
</html>
