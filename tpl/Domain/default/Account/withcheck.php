<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>我的账户</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
			<!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>待验证的域名列表</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 提示&警示 -->
            <div class="m-insa s-bg-fc u_be89">
                <ul>
                    <li><span class="u_fw">1.</span>添加出售的域名需要经过验证域名whois信息，以确保交易安全。</li>
                    <li><span class="u_fw">2.</span> {pigcms{$config.site_name} 会自动验证域名whois邮箱和用户邮箱（注册邮箱及绑定邮箱）是否匹配。</li>
                    <li><span class="u_fw">3.</span>若您的域名使用多个邮箱注册，请先在账户内<a href="{pigcms{:U('Account/whoisemail')}" style="color:#2aa3ce;font-weight:bold"> 绑定这些邮箱</a>
。</li>
                    <li><span class="u_fw">4.</span>若 {pigcms{$config.site_name} 无法获取域名whois信息、或您的域名启用whois隐私保护，请联系客服进行人工验证。</li>
                </ul>
            </div>
            <!-- 数据 -->
            <div class="m-account-data2 s-bg-fc u_be9">
                <table>
                    <tr class="s-bg-white">
                        <th width="16px"></th>
                        <th width="20px"></th>
                        <th width="150px" class="u_tl">域名</th>
                        <th width="185px" class="u_tl">状态</th>
                        <th width="135px" class="u_tl">添加日期</th>
                        <th width="180px" class="u_tr">操作</th>
                        <th width="16px"></th>
                    </tr>
                </table>
                <div id="paging" class="g-padding g-padding1"></div>
                <!--div class="m-operation3">
                    <div class="u-fl operation3-l">
                        <label>
                            <input type="checkbox" class="curPage" />
                            <span>选中当前页</span>
                        </label>
                        <label>
                            <input type="checkbox" class="allPage"/>
                            <span>选中所有</span>
                        </label>
                    </div>
                    <div class="u-fl operation3-r">
                        <span>操作已选域名：</span>
                        <input type="button" value="验证所有" class="u-btn10 btn_verify_all" />
                        <input type="button" value="批量删除" class="u-btn10 btn_pl_del" />
                    </div>
                    <div class="u-cls"></div>
                </div-->
            </div>

        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>

	<script src="{pigcms{$static_path}js/account.js"></script>
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}

        $(function () {
            getDataList_withcheck(1);

            $("#paging a").live("click", function () {
                if (!$(this).attr("k"))
                    return;
                var pageindex = $(this).attr("k");
                getDataList_withcheck(pageindex);
            });

            $(".btn_verify").live("click", function () {
                var k = $(this).attr("k");
                var id = $(this).parent().parent().find("input.curLine").attr("value");
                var td = $(this).parent().prev().prev();
                if (k == "0") {
                    td.html("<img src='{pigcms{$static_path}images/index/loading.gif' width='20' height='20' />");
                    $.post("{pigcms{:U('Account/checkwithdomain')}",{"type":k,"id":id},function(result){
						if(result.status == 1){
							td.html('验证成功');
						}else{
							td.html(lang.a59);
						}
                    });
                } else {
                    if (confirm(lang.a22 + "[" + td.prev().html() + "]？")) {
                        $.post("{pigcms{:U('Account/checkwithdomain')}", { "type": k, "id": id }, function (data) {
                            if (data == 1) {
                                location.href = location.href;
                            }
                        });
                    }
                }
            })

            $(".btn_verify_all").click(function () {
                $.post("/account/CheckDomain", function (data, status) {
                    if (status == "success") {
                        showDialog(lang.a60);
                    }
                })
            })

            $(".btn_pl_del").click(function () {
                var ckl = getCookie("selectAll");
                if (ckl != null) {
                    var resut_arr = ckl.match(/(.+?)\&(.*)/);
                    if (resut_arr != null && resut_arr[1] == href && resut_arr[2].replace(/\s/g, "") != "") {
                        if (confirm(lang.a22 + "？")) {
                            $.post("/account/checkwithdomain", { "type": "2" }, function (data, status) {
                                if (status == "success") {
                                    delCookie("selectAll");
                                    location.href = location.href;

                                }
                            })
                        }
                    }
                    else
                        showDialog(lang.a61);
                } else {
                    showDialog(lang.a61);
                }
            })

        })
    </script>
	<include file="Public:sidebar"/>
</body>
</html>
