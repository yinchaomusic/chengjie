<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>
<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>我的账户</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="g-content u-cls">
	<div class="m-content">
		<include file="Account:sidebar"/>
		<!-- 右边 -->
		<div class="u-fr">
			<!-- 标题&验证 -->
			<div class="s-bg-fc u_be9" style="width: 702px;">
				<div class="m-ta s-bg-white">
					<div class="title">
						<a class="s-3c" href="javascript:void(0)">
							<span>账号安全</span>
						</a>
					</div>
					<div class="u-cls"></div>
				</div>
			</div>
			<!-- 选项卡 -->
			<div class="m-acc-select u_be9 s-bg-white">
				<!-- 标题 -->
				<include file="Account:safe_nav"/>

				<div class="m-acc-cons s-bg-fc u_mall1">
					<div class="m-inse">
						<p class="f-p-t14">为了保护您的账号安全，您先回答这些安全问题！</p>
					</div>
				</div>



					<form action="{pigcms{:U('Account/logprotect_check')}" method="post">
						<div class="m-question s-bg-fc u_be9">
							<ul>
								<li class="u_mb15">
									<div class="question_txt">安全问题：</div>
									<div class="question_ipt s-bg-f5">
										<input type="text" readonly="readonly" value="{pigcms{$pro_check[0].question}">
									</div>
									<input name="id" id="id" type="hidden" value="{pigcms{$pro_check[0].id}">
									<div class="u-cls"></div>
								</li>
								<li class="u_mb15">
									<div class="question_txt">安全答案：</div>
									<div class="question_ipt s-bg-white">
										<input name="answer" style="color: rgb(216, 216, 216);" type="text" ph="请输安全答案">
									</div>
									<div class="u-cls"></div>
								</li>
								<li class="u_mb15">
									<div class="question_txt">您的登录密码：</div>
									<div class="question_ipt s-bg-white">
										<input name="pwd" type="password">
									</div>
									<div class="u-cls"></div>
								</li>
								<li>
									<div class="u_mall12">
										<input class="u-btn12 s-bg-2a" type="submit" value="验证">
									</div>
								</li>
							</ul>
						</div>
					</form>


			</div>

		</div>

		<div class="u-cls"></div>
	</div>
</div>
<include file="Public:footer"/>

<script src="{pigcms{$static_path}js/account.js"></script>

<include file="Public:sidebar"/>
</body>
</html>
