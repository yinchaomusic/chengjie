<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
        <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
        <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/jquery-ui.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>我的账户</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
			<!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>提现</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
<!-- 选项卡 -->
            <div class="m-acc-select u_be9 s-bg-white">
                <!-- 标题 -->
                <div class="m-acc-title s-bg-fc">
                    <div class="u-fl"><a href="{pigcms{:U('Account/withdrawal')}">申请提现</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/withdrawal_record')}" class="active">提现记录</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/setbankcard')}" >设置银行卡</a></div>
                    <div class="u-cls"></div>
                </div>
                <form action="{pigcms{:U('Account/withdrawal_record')}" method="post">
                    <div class="m-acc-search1">
                        <span class="u-ipt-ia u_fl" style="padding-left: 36px;">
                            <span class="f-imgt1">起止时间</span>
                            <input type="text" class="i2" id="startTime" style="margin-right:0;" name="startTime"/>
                            <span class="f-imgt2">至</span>
                            <input type="text" class="i3" name="endTime" id="endTime"/>
                        </span>
                        <input type="submit" value="搜索" class="u-btn8"/>
                    </div>
                </form>
                    <div class="m-acc-cons s-bg-fc u_mall1">
                        <table>
                            <tr class="s-bg-fe">
                                <th width="16px"></th>
                                <th width="140px" class="u_tl">申请时间</th>
                                <th width="110px" class="u_tr">提现金额</th>
                                <th width="20px"></th>
                                <th width="120px" class="u_tl">提现账户</th>
                                <th width="144px" class="u_tr">状态</th>
                                <th width="16px"></th>
                            </tr>
                            <volist name="wr_list" id="vo">
                                <tr class="s-bg-fe">
                                    <td width="16px"></td>
                                    <td width="140px" class="u_tl">{pigcms{$vo.addtime|date="Y-m-d H:i:s",###}</td>
                                    <td width="110px" class="u_tr">￥{pigcms{$vo.txtmoeny|number_format}</td>
                                    <td width="20px"></td>
                                    <td width="120px" class="u_tl">{pigcms{$vo.account}</td>
                                    <td width="144px" class="u_tr">
                                        <if condition="$vo.state eq 0">
                                            提现中
                                            <elseif condition="$vo.state eq 1"/>
                                            已成功
                                            <else/>
                                            提现失败
                                        </if>
                                    </td>
                                    <td width="16px"></td>
                                </tr>
                            </volist>
                            
                        </table>
                    </div>
                    <!-- 分页 -->
                    <div id="paging" class="g-padding g-padding1">{pigcms{$pagebar}</div>
            </div>





        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<script src="{pigcms{$static_path}js/account.js"></script>
        <script src="{pigcms{$static_path}js/jquery-ui-datepicker.js"></script>
        <script>
            $("#startTime").datepicker({
			maxDate: new Date(), prevText: "上个月", nextText: "下个月", onSelect: function (dateText, obj) {
				$("#endTime").datepicker("option", "minDate", dateText);
			}
		});
		$("#endTime").datepicker({
			maxDate: new Date(), prevText: "上个月", nextText: "下个月", onSelect: function (dateText, obj) {
				$("#startTime").datepicker("option", "maxDate", dateText);
			}
		});
        </script>
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}
	</script>
        
           
<script type="text/javascript">
                //提现
                $(".btn_tj1").click(function () {
                    
                    if ($("#txtmoeny").val() == "") {
                        showDialog(lang.a33);
                        return false;
                    }
                    var m = $(".u_m6").text().replace(/[$￥€,]/g, "");
                    if (Number($("#txtmoeny").val()) > Number(m)) {
                        showDialog(lang.a26);
                        return false;
                    }
                })
            </script>
        
	<include file="Public:sidebar"/>
</body>
</html>
