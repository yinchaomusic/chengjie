<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>我的账户</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>我委托的代购</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 选项卡 -->
            <div class="m-acc-select u_be9 s-bg-white">
                <!-- 标题 -->
                <div class="m-acc-title s-bg-fc">
                    <div class="u-fl"><a href="{pigcms{:U('Account/pay')}" class="active">审核</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/paying')}">代购中</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/paysuccess')}">代购成功</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/payend')}">代购失败</a></div>
                    <input type="hidden" value="0" id="checktype">
                    <div class="u-cls"></div>
                </div>
                <!-- 内容 -->
                    <div class="m-acc-cons s-bg-fc  u_mall1">
                        <h3 class="s-3c s-bg-fe">审核中</h3>
                        <table>
                            <tbody>
                             <tr>
                                <th width="16px"></th>
                                <th width="150px" class="u_tl">域名</th>
                                <th width="120px" class="u_tr">预算</th>
                                <th width="74px"></th>
                                <th width="140px" class="u_tl">申请时间</th>
                                <th width="160px" class="u_tr">操作</th>
                                <th width="16px"></th>
                            </tr>
                            
                            
                                <volist name="list" id="vo">
                                    <if condition="$vo['status'] lt  2">
                                         <tr>
                                            <td width="16px"></td>
                                            <td width="150px" class="u_tl">{pigcms{$vo.domain}</td>
                                            <td width="120px" class="u_tr">￥{pigcms{$vo.buyers_price|number_format}</td>
                                            <td width="74px"></td>
                                            <td width="140px" class="u_tl">{pigcms{$vo.add_time|date="Y-m-d H:i:s",###}</td>
                                            <td width="160px" class="u_tr"> <a href="{pigcms{:U('Account/pay_edit',array('ph_id'=>$vo['ph_id']))}">修改</a> |
                                                <a href="{pigcms{:U('Account/pay_del',array('ph_id'=>$vo['ph_id']))}" onclick="return confirm('您确认要删除吗')">删除</a></td>
                                            <td width="16px"></td>
                                        </tr>
                                    </if>
                               
                                </volist>
                            
<!--<tr><td colspan="7" class="u_nb u_tc u_norecord">暂无记录！</td></tr> -->
        </tbody></table>
                    </div>
                
                
                
                    <div class="m-acc-cons s-bg-fc  u_mall2">
                        <h3 class="s-3c s-bg-fe">审核成功</h3>
                        <table>
                            <tbody><tr>
                                <th width="16px"></th>
                                <th width="150px" class="u_tl">域名</th>
                                <th width="120px" class="u_tr">预算</th>
                                <th width="74px"></th>
                                <th width="140px" class="u_tl">申请时间</th>
                                <th width="160px" class="u_tr">操作</th>
                                <th width="16px"></th>
                            </tr>
                            
                            <volist name="list" id="vv">
                                    <if condition="$vv['status'] eq  2">
                                         <tr>
                                            <td width="16px"></td>
                                            <td width="150px" class="u_tl">{pigcms{$vv.domain}</td>
                                            <td width="120px" class="u_tr">￥{pigcms{$vv.buyers_price|floor}</td>
                                            <td width="74px"></td>
                                            <td width="140px" class="u_tl">{pigcms{$vv.add_time|date="Y-m-d H:i:s",###}</td>
                                            <td width="160px" class="u_tr"><a class="pay_margin" kid="{pigcms{$vv.ph_id}" id="pay_margin">缴纳保证金</a></td>
                                            <td width="16px"></td>
                                        </tr>
                                    </if>
                               
                                </volist>
                            
                        
                            </tbody></table>
                    </div>
                
                
                <div style="position: absolute; display: none; z-index: 1005; left: 29%; top: 331.5px;" class="m-pop2 s-bg-white" id="pop1">
                    <div class="pop-insa">
                        <ul>
                            <li>
                                <span class="u-span18 u-fl">1.</span>
                                <span class="u-span19 u-fl">后台经纪人审核通过。。
                                </span>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <span class="u-span18 u-fl">2.</span>
                                <span class="u-span19 u-fl">你需要提交报价的百分之八的定金。。</span>
                                <div class="u-cls"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="pop2-data1">
                        <ul>
                            <li>
                                <span class="u-span20">域名：</span>
                                <span class="s-75b u-span21 amount" id="domain"></span>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <span class="u-span20">用户id：</span>
                                <span class="s-75b u-span21 amount" id="uid"></span>
                                <input type="hidden" value="" id="pay_ph_id">
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <span class="u-span20">您需支付：￥</span>
                                <span class="s-75b u-span21 amount" id="margin"></span>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <span class="u-span20">我的账户余额：￥</span>
                                <span class="u-span21" id="now_money"></span>
                                <div class="u-cls"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="pop2-btn s-bg-fa">
                        <input id="paymargin" value="提交付款" class="u-btn12 s-bg-2a" type="button">
                    </div>
                    <div class="close"></div>
                </div>
                
                
                    <div class="m-acc-cons s-bg-fc  u_mall2">
                        <h3 class="s-3c s-bg-fe">审核失败</h3>
                        <table class="tb_ap_fail">
                            <tbody><tr>
                                <th width="16px"></th>
                                <th width="150px" class="u_tl">域名</th>
                                <th width="120px" class="u_tr">预算</th>
                                <th width="48px"></th>
                                <th width="110px" class="u_tl">申请时间</th>
                                <th width="150px" class="u_tl">原因</th>
                                <th width="150px" class="u_tr">操作</th>
                                <th width="16px"></th>
                            </tr>
                            
                            <volist name="list" id="voo">
                                    <if condition="$voo['status'] eq 3">
                                         <tr>
                                            <td width="16px"></td>
                                            <td width="150px" class="u_tl">{pigcms{$voo.domain}</td>
                                            <td width="120px" class="u_tr">￥{pigcms{$voo.buyers_price|floor}</td>
                                            <td width="48px"></td>
                                            <td width="110px" class="u_tl">{pigcms{$voo.add_time|date="Y-m-d H:i:s",###}</td>
                                            <td width="150px" class="u_tl">{pigcms{$voo.why}</td>
                                            <td width="150px" class="u_tr"><a href="{pigcms{:U('Account/pay_del',array('ph_id'=>$vo['ph_id']))}" onclick="return confirm('您确定要删除吗')">删除</a></td>
                                            <td width="16px"></td>
                                        </tr>
                                    </if>
                               
                                </volist>
                            
                        </tbody></table>
                    </div>
                <!-- 分页 -->
                <div id="paging" class="g-padding g-padding1"></div>
            </div>
        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>

	<script src="{pigcms{$static_path}js/account.js"></script>
        
        <script type="text/javascript" src="{pigcms{$static_path}js/popupBox.js"></script>
        <script>
            
            $("#paymargin").click(function(){
                var ph_id = $("#pay_ph_id").val();
                var margin = $("#margin").text();
                var now_money = $("#now_money").text();
                var uid = $("#uid").text();
                if( parseInt(now_money) < parseInt(margin) ){
                    alert(lang.a26);
                    location.href = location.href;
                }else{
//                    alert(123);
                    $.post("{pigcms{:U('Account/freeze_margin')}", {"ph_id":ph_id,"margin":margin,"uid":uid,"now_money":now_money}, function(data){
//                        alert(data);
                        if(data == 1){
                            alert("保证金提交完成");
                            location.href = location.href;
                        }else{
                            alert('异常');
                            location.href = location.href;
                        }
                    })
                }
            })
            
        //弹出 
		$(".pay_margin").click(function () {
			$.post("{pigcms{:U('Account/pay_margin')}", { "ph_id": $(this).attr("kid") }, function (data) {
//                            alert(data)
				if (data) {
                                        $("#domain").text(data.domain);
                                        $("#margin").text(data.margin);
                                        $("#now_money").text(data.now_money);
                                        $("#uid").text(data.uid);
                                        $("#pay_ph_id").val(data.ph_id);
                                        showBox("pop1");
                                        $("#pop1").css("left", "29%");
				}
			},'json')
		});
		$(".close").click(function () {
			closeBox("pop1");
			closeBox("pop1_bg");
		});
            </script>

	<include file="Public:sidebar"/>
</body>
</html>
