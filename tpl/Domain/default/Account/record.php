<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/jquery-ui.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>我的账户</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
			<!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>财务明细</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!--  -->
	        <form action="{pigcms{:U('Account/record')}" method="post">
            <div class="m-acc-search2 s-bg-fc" style="padding-bottom: 18px;">

                <span class="u-ipt-ia u_fl s-bg-white" style="padding-left: 36px; border: 1px solid rgb(235, 235, 235);">
                    <span class="f-imgt1">起止时间</span>
                    <input type="text" class="i2" id="startTime" name="startTime">
                    <span class="f-imgt2">至</span>
                    <input type="text" class="i3" id="endTime" name="endTime">
                </span>
	            <select class="u-btn18" name="SearchAlltype" id="SearchAlltype">
		            <option value=""   >所有类型</option>
		            <option value="1" <if condition="$SearchAlltype eq 1">selected</if> >充值</option>
		            <option value="2" <if condition="$SearchAlltype eq 2">selected</if>>提现</option>
		            <option value="3"<if condition="$SearchAlltype eq 3">selected</if>>交易收入</option>
		            <option value="4"<if condition="$SearchAlltype eq 4">selected</if>>交易支出</option>
		            <option value="5"<if condition="$SearchAlltype eq 5">selected</if>>违约补偿</option>
		            <option value="6"<if condition="$SearchAlltype eq 6">selected</if>>违约扣款</option>
		            <option value="7"<if condition="$SearchAlltype eq 7">selected</if>>下线提成</option>
		            <option value="12,13" <if condition="$SearchAlltype eq '12,13'">selected</if>>冻结记录</option>
	            </select>
                <input type="submit" value="搜索" class="u-btn8">
            </div>
	        </form>

            <!-- 数据 -->
            <div class="m-account-data2 s-bg-fc u_be9">
                <table>
                    <tr class="s-bg-white">

                        <th width="150px" style="padding-left: 15px;" class="u_tl">时间</th>
                        <th width="100px" class="u_tl">类型</th>
                        <th width="120px" class="u_tl">金额</th>
                        <th width="120px" class="u_tl" id="tswz">可用金额</th>
                        <th width="180" class="u_tl">说明</th>

                    </tr>

	                <if condition="is_array($user_money_list)">
		                <volist name="user_money_list" id="vo">
			                <tr>

				                <td style="padding-left: 15px;">{pigcms{$vo.time|date='Y-m-d H:i:s',###}</td>
				                <td>
					                <if condition="$vo['type'] eq 1">充值
					                <elseif condition="$vo['type'] eq 2"/>提现
					                <elseif condition="$vo['type'] eq 3"/>交易收入
					                <elseif condition="$vo['type'] eq 4"/>交易支出
					                <elseif condition="$vo['type'] eq 5"/>违约补偿
					                <elseif condition="$vo['type'] eq 6"/>违约扣款
					                <elseif condition="$vo['type'] eq 7"/>下线提成
					                <elseif condition="$vo['type'] eq 12"/>资金冻结
					                <elseif condition="$vo['type'] eq 13"/>资金解冻
						            <else/>-
					                </if>
				                </td>
				                <td>
					                <if condition="($vo['type'] eq 12) OR ($vo['type'] eq 4) OR ($vo['type'] eq 6) OR ($vo['type'] eq 7) OR
					                ($vo['type'] eq 2)">
						                <font color="red">-￥{pigcms{$vo.money|number_format}</font>
						                <else/>
						                <font color="blue">￥{pigcms{$vo.money|number_format}</font>
					                </if>
				                </td>
				                <td>￥{pigcms{$vo.now_money|number_format}</td>

				                <td>{pigcms{$vo.desc}</td>

			                </tr>
		                </volist>
		                <tr><td class="textcenter pagebar" style="padding-left: 15px;" colspan="11">{pigcms{$pagebar}</td></tr>
		                <else/>
		                <tr><td class="textcenter red" colspan="11">列表为空！</td></tr>
	                </if>

                </table>
            </div>
            <!-- fenye -->
            <div id="paging" class="g-padding g-padding2"></div>
        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>

	<script src="{pigcms{$static_path}js/jquery-ui-datepicker.js"></script>

	<script src="{pigcms{$static_path}js/account.js"></script>
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}

		//所有类型选择
		$("#selectStyle").click(function (event) {
			$("#selectresult").css("display", "block");
			event.stopPropagation();
		});
		$("#selectresult p").click(function (event) {
			$("#selectStyle").html($(this).text()).attr("value", $(this).attr("value"));
			$(this).parent().css("display", "none");
			getDataList_record(1);
			event.stopPropagation();
		});
		$(document.body).click(function () {
			$("#selectresult").css("display", "none");
		});

		$("#startTime").datepicker({
			maxDate: new Date(), prevText: "上个月", nextText: "下个月", onSelect: function (dateText, obj) {
				$("#endTime").datepicker("option", "minDate", dateText);
			}
		});
		$("#endTime").datepicker({
			maxDate: new Date(), prevText: "上个月", nextText: "下个月", onSelect: function (dateText, obj) {
				$("#startTime").datepicker("option", "maxDate", dateText);
			}
		});
//		$("#paging a").live("click", function () {
//			if (!$(this).attr("k"))
//				return;
//			var pageindex = $(this).attr("k");
//			getDataList_record(pageindex);
//		});
//
//		$(".u-btn8").click(function () {
//			getDataList_record(1);
//		})
//		getDataList_record(1);
	</script>

	<include file="Public:sidebar"/>

</body>
</html>
