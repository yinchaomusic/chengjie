<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>我的账户</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
			<!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>账户充值</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 选项卡 -->
            <div class="m-acc-select u_be9 s-bg-white">
                <!-- 标题 -->
                <div class="m-acc-title s-bg-fc">
                    <div class="u-fl"><a href="/account/recharge" class='active'>立即充值</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/record?type=1')}" class='' >充值记录</a></div>
                    <div class="u-cls"></div>
                </div>
                    <!-- 内容 -->
                    <div class="m-acc-cons s-bg-fc u_mall1">
                        <h3 class="s-3c s-bg-fe">在线充值</h3>
                        <div class="m-inse">
                            <p class="u_mb15">我们提供多种不同的付款方式，您可以在线充值或银行转帐 / 电汇！</p>
                            <p class="u_mb15 s-ff6">请注意</p>
                            <ul>
                                <li><span class="u_fw">1.</span>为保障资金安全，充值金额在 <span class="u_fw">200000</span>元 以上的，建议采用银行转账。</li>
                                <li><span class="u_fw">2.</span>本站交易价格均为未含税价格，如果需要开具发票，请另行交纳税点。</li>
                                <li class="u_fw"><span class="u_fw">3.</span>{pigcms{$config.site_name}账户不允许从事无真实交易背景的虚假交易、信用卡套现或洗钱等禁止的交易行为，否则充值款项将不能提现。</li>
                            </ul>
                        </div>
                    </div>
                    <!--充值 -->
                    <div class="m-interface s-bg-fc">
                        <div class="interface">
                            <div class="u-span22">支付接口：</div>
                            <div class="m-choise" id="choise" style="height:auto;">
								<volist name="pay_method" id="vo">
									<span data-paytype="{pigcms{$key}" class="{pigcms{$key} <if condition="$i eq 1">active</if>" style="background:url(./static/images/pay/{pigcms{$key}.gif) no-repeat;"></span>
								</volist>
                                <span class="cure"></span>
                            </div>
                            <div class="u-cls"></div>
                        </div>
                        <div class="">
                            <div class="u-span23">充值金额：</div>
                            <div class="m-m6">
                                <span class="u-ipt-d">
                                    <input type="text" maxlength="6" />
                                </span>
                                <span>元</span>
                            </div>
                            <div class="u_cell_money">￥</div>
                            <div class="u-cls"></div>
                        </div>
                        <div class="btn" style="padding: 45px 0 45px 192px;">
                            <input type="submit" value="提交" class="u-btn9 s-bg-2a" />
                        </div>
                    </div>
                    <!--充值 -->
            </div>

        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<script type="text/javascript">
            // 选择支付方式
            $("#choise span").click(function () {
                if($(this).attr("class")=="cure"){
                    return false;
				}
                $("#choise span").removeClass("active");
                $(this).addClass("active");
				$(".cure").css({"left":($(this).position().left+$(this).width()-5)+"px","top":($(this).position().top-3)+"px"});
            });
            // 钱
            $(".u-ipt-d input").keyup(function () {
                clearNoNum1(this);
            });

            $(".u-ipt-d input").keyup(function () {
                clearNoNum1(this);
            })
            $(".u-btn9").click(function () {
                var url = "{pigcms{:U('Account/recharge_save')}&pay_type=" + ($("#choise .active").data('paytype'));
                var money = $(".u-ipt-d input").val();
                if (money == "") {
                    showDialog(lang.a27);
                    return;
                }
                if (Number(money) > 200000) {
                    showDialog(lang.a28);
                    return;
                }



                $("#pop1_bg").css({ "display": "block", "height": $(document).height() + "px" });
                $("#msgDiv").css("display", "");
                window.open(url + "&money=" + money);
            })
            $(".sp_close").click(function () {
                $("#pop1_bg").css("display", "none");
                $("#msgDiv").css("display", "none");
            })
        </script> 
	<include file="Public:sidebar"/>
	<div id="msgDiv" style="width: 315px; height: 130px; z-index: 99999; position: absolute; top: 482px; left: 40%; display: none; background: rgb(39, 137, 200);">
        <div style="padding-left: 20px; height: 30px; line-height: 30px; font-size: 15px; font-weight: bold; color: white; text-align: left;">
            <span class="sp_close" style="display: inline-block; width: 30px; height: 30px; line-height: 30px; text-align: center; cursor: pointer; float: right;">×</span>
            提示
        </div>
        <div style="width: 281px; height: 75px; padding: 10px; background: white; margin: 0 auto; text-align: left; line-height: 21px;">
            <div>请您在新打开的网上银行页面进行支付，支付完成前请不要关闭该窗口。</div>
            <a href="{pigcms{:U('Account/index')}" class="btn_pay">已支付完成</a><a href="{pigcms{:U('Account/recharge')}" class="btn_pay">支付遇到问题</a>
        </div>
    </div>   
</body>
</html>
