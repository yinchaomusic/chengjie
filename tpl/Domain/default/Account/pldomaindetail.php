<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我是卖家 - 添加批量交易</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<style type="text/css">
        .domain {
            cursor: pointer;
            display: inline-block;
            padding-right: 60px;
            background: url('/images/index/gotoTop.png') no-repeat right -245px;
        }
        .m-seller dl {
            margin: 10px 20px;
            display: none;
        }
		.m-seller dl dd {
			float: left;
			width: 220px;
			margin-top: 7px;
			color: #2aa3ce;
			font-size: 15px;
		}
    </style>
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>我是卖家&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/pldomain')}" class="s-2a">批量交易</a>&nbsp;&gt;&nbsp;</li>
					<li>详情</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>当前状态</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 新增 -->
            <div class="m-insa s-bg-fc u_be89" style="width:676px;">
                <ul>
                    <li><span class="u_fw">1.</span>如果有审核未通过域名可能是您未添加该域名的whois邮箱，请添加后重新验证。</li>
                    <li><span class="u_fw">2.</span>如果添加了不属于您的域名请删除本次提交的批量交易重新添加。</li>
                    <li><span class="u_fw">3.</span>请联系经纪人验证域名。</li>
                </ul>
            </div>
            <div class="m-seller s-bg-fc u_be9">
                <h3 class="u_bt2 pljy-t-head">
                    <div class="s-2a pljy-title" style="background:none;">
					{pigcms{$trading.title}<span class="m">￥{pigcms{$trading.total_price|number_format}</span>
                    </div>
                    <span class="s-ff6 z-wait"></span>
                    <span class="pljy-intro">{pigcms{$trading.meaning}</span>
                </h3>
                <dl style="display:block;" id="ls_ym">
					<volist name="trading['domain_list']" id="vo">
                        <dd class="s">{pigcms{$vo}  <span> </span></dd>
                    </volist>
                </dl>
                <!-- 修改部分结束 -->
                <div style="clear: both;"></div>
                <div style="height:15px"></div>
                <div style="height: 15px"></div>
            </div>
        </div>
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<script type="text/javascript">
        $(function () {
           $('.z-wait').html('（'+getDataList_state({pigcms{$trading.status})+'）');
        })
        $(document).ready(function(){
            checkLeishi();
            function checkLeishi() {

                var check_domains = {pigcms{$jsondata};
                var whoid = {pigcms{$jsonwhoid};

                $.ajax({
                    type:"post",
                    url: "{pigcms{:U('Account/whois')}",
                    data: { "arr_yms": check_domains,"whoid":whoid},
                    success: function (response) {
                        var res =$.parseJSON(response);
                        var j = res.length;
                        for (var i = 0; i < j; i++) {
                            if (res[i] == "1") {
                                $("#ls_ym dd span")[i].innerHTML = "- 已验证";
                                $("#ls_ym dd span")[i].style.color = "#3394D8";
                            }else if(res[i] == "2"){
                                $("#ls_ym dd span")[i].innerHTML = "- 验证超时";
                                $("#ls_ym dd span")[i].style.color = "#213323";
                            }else{
                                $("#ls_ym dd span")[i].innerHTML = "- 验证未通过";
                                $("#ls_ym dd span")[i].style.color = "brown";
                            }
                        }
                    },
                    error: function (res) {

                    }
                });
            }
        });
    </script>
	<include file="Public:sidebar"/>
</body>
</html>
