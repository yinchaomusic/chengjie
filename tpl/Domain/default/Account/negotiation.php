<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我是买家 - 我给出的报价</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
        <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
        <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/jquery-ui.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>我是买家&nbsp;&gt;&nbsp;</li>
					<li>我给出的报价&nbsp;&gt;&nbsp;</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>我给出的报价</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 选项卡 -->
            <div class="m-acc-select u_be9 s-bg-white">
                <!-- 标题 -->
                <div class="m-acc-title s-bg-fc">
                    <div class="u-fl"><a href="{pigcms{:U('Account/negotiation')}" <if condition="!isset($_GET['status']) || $_GET['status'] heq ''">class="active"</if>>所有记录</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/negotiation',array('status'=>'0'))}" <if condition="$_GET['status'] heq '0'">class="active"</if>>正在议价</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/negotiation',array('status'=>'1'))}" <if condition="$_GET['status'] heq '1'">class="active"</if>>议价成功</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/negotiation',array('status'=>'2'))}" <if condition="$_GET['status'] heq '2'">class="active"</if>>议价失败</a></div>
                    <input type="hidden" value="{pigcms{$_GET.status}" id="checktype" />
                    <div class="u-cls"></div>
                </div>
                <!-- 搜索 -->
                <div class="m-acc-search">
                    <span class="u-ipt-i u_fl">
                        <input type="text" class="i1" id="txt_domain" ph="请输入要查询的域名" style="color:rgb(216,216,216);"/>
<!--                        <span class="f-imgt1">起止时间</span><input type="text" class="i2 hasDatepicker" id="startTime" style="margin-right:0px;"/>
                        <span class="f-imgt2">至</span><input type="text" class="i3 hasDatepicker" id="endTime"/>-->
                        
                        <span class="f-imgt1">起止时间</span>
                        <input type="text" class="i2" id="startTime" style="margin-right:0;" name="startTime"/></>
                        <span class="f-imgt2">至</span>
                        <input type="text" class="i3" name="endTime" id="endTime"/>
                                
                    </span>
                    <input type="button" value="搜索" class="u-btn8">
                </div>
                <!-- 内容 -->
                <div class="m-acc-cons s-bg-fc u_mall1">
                    <table>
                        <tbody><tr>
                            <th width="16px"></th>

                            <th width="60px" class="u_tl">来源</th>
                            <th width="150px" class="u_tl">域名</th>
                            <th width="127px" class="u_tl">当前状态</th>
                            <th width="120px" class="u_tr">最新报价</th>

                            <th width="28px"></th>
                            <th width="110px" class="u_tl">最后报价时间</th>
                            <th width="56px;">操作</th>
                            <th width="8px"></th>
                        </tr>
                    <tr><td class="u_nb u_tc u_norecord" colspan="9">暂无记录！</td></tr></tbody></table>
                </div>
                <!-- 分页 -->
                <div id="paging" class="g-padding g-padding1"></div>
            </div>
        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<script type="text/javascript" src="{pigcms{$static_path}js/jquery-ui-datepicker.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#startTime").datepicker({
                maxDate: new Date(), prevText: "上个月", nextText: "下个月", onSelect: function (dateText, obj) {
                    $("#endTime").datepicker("option", "minDate", dateText);
                }
            });
            $("#endTime").datepicker({
                maxDate: new Date(), prevText: "上个月", nextText: "下个月", onSelect: function (dateText, obj) {
                    $("#startTime").datepicker("option", "maxDate", dateText);
                }
            });

            //点击分页按钮
            $("#paging a").live("click", function () {
                if (!$(this).attr("k"))
                    return;
                var pageindex = $(this).attr("k");
                getData_negotiation(pageindex, 0);
            });

            $(".u-btn8").click(function () {
                getData_negotiation(1, 0);
            });
            getData_negotiation(1, 0);
        })        
    </script>
	<include file="Public:sidebar"/>
</body>
</html>
