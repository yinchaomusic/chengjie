<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
        <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>我的账户</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="s-bg-fa" id="pop1" style="display: none;">
        <p class="f-p-t7 u_bb0">
            <span class="u-fl">修改银行卡</span>
            <span class="u-fr close"></span>
            <span class="u-cls"></span>
        </p>
        <div style="height: 10px;"></div>
        <table>
            <tr>
                <td class="u-settxt">开户人姓名：</td>
                <td width="18px"></td>
                <td class="u-user u-ipt-d s-bg-white" style="width:118px;"><input type="text" id="upnickname" value=""/> </td>
            </tr>
            <tr>
                <td colspan="3" class="u_hi15"></td>
            </tr>
            <tr>
                <td class="u-settxt">选择银行：</td>
                <td></td>
                <td>
                    <div class="edit-m-slt-bank">
                        <h3>中国工商银行</h3>
                        <div class="edit-account-result" style="left: 0px;">
                            <p>中国银行</p>
                            <p>中国农业银行</p>
                            <p>中国工商银行</p>
                            <p>招商银行</p>
                            <p>中国建设银行</p>
                            <p>交通银行</p>
                            <p>平安银行</p>
                            <p>广发银行</p>
                            <p>光大银行</p>
                            <p>民生银行</p>
                            <p>浦东发展银行</p>
                            <p>华夏银行</p>
                            <p>中信银行</p>
                            <p>华夏银行</p>
                            <p>中国邮政储蓄</p>
                            <p>兴业银行</p>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="u_hi15"></td>
            </tr>
            <tr>
                <td class="u-settxt">选择地区：</td>
                <td></td>
                <td class="">
                    <div class="edit-f-odiva">
                        <h3 class="edit-provice">广东省</h3>
                        <h3 class="edit-city">深圳市</h3>
                        <h3 class="edit-area">南山区</h3>
                        <div class="edit-f-slta-container" idx="0" id="edit-provice" style="display: none;">
                        </div>
                        <div class="edit-f-sltb-container" idx="1" id="edit-city" style="display: none;">
                        </div>
                        <div class="edit-f-sltc-container" idx="2" id="edit-prefecture" style="display: none;">
                        </div>
                        <div class="edit-u-cls"></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="u_hi15"></td>
            </tr>
            <tr>
                <td class="u-settxt">支行名称：</td>
                <td></td>
                <td>
                    <div class="u-ipt-d s-bg-white">
                        <input type="text" id="upd_bank_sub" />
                        <input type="hidden" id="upd_bankid" />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="u_hi15"></td>
            </tr>
            <tr>
                <td class="u-settxt">银行账号：</td>
                <td></td>
                <td>
                    <div class="u-ipt-d s-bg-white">
                        <input type="text" id="upd_num" />
                    </div>
                </td>
            </tr>
        </table>
        <div class="u_mall11" style="margin-top: 20px;">
            <input type="submit" value="提交" id="upd_submit" class="u-btn12 s-bg-2a" />
        </div>
        <div style="height: 10px;"></div>
    </div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
    <!-- 左边 -->
        <div class="u-fl">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>提现</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 选项卡 -->
            <div class="m-acc-select u_be9 s-bg-white">
                <!-- 标题 -->
                <div class="m-acc-title s-bg-fc">
                    <div class="u-fl"><a href="{pigcms{:U('Account/withdrawal')}" >申请提现</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/withdrawal_record')}" >提现记录</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/setbankcard')}" class=active>设置银行卡</a></div>
                    <div class="u-cls"></div>
                </div>
                    <!-- 设置银行卡 -->
                    <div class="m-setbankcard s-bg-fc">
                        <table>
                            <tr>
                                <td class="u-settxt">开户人姓名：</td>
                                <td width="18px"></td>
                                <td class="u-user u-ipt-d s-bg-white" style="width:118px;"><input type="text" id="nickname" name="nickname" value="<?php echo $_SESSION['user']['nickname']?>"/> </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="u_hi15"></td>
                            </tr>
                            <tr>
                                <td class="u-settxt">选择银行：</td>
                                <td></td>
                                <td>
                                    <div class="m-slt-bank">
                                        <h3 class="f-slt-c">中国工商银行</h3>
                                        <div class="account-result" style="left: 0px;">
                                            <p>中国银行</p>
                                            <p>中国农业银行</p>
                                            <p>中国工商银行</p>
                                            <p>招商银行</p>
                                            <p>中国建设银行</p>
                                            <p>交通银行</p>
                                            <p>平安银行</p>
                                            <p>广发银行</p>
                                            <p>光大银行</p>
                                            <p>民生银行</p>
                                            <p>浦东发展银行</p>
                                            <p>华夏银行</p>
                                            <p>中信银行</p>
                                            <p>华夏银行</p>
                                            <p>中国邮政储蓄</p>                                            
                                            <p>支付宝</p>
                                            <p>兴业银行</p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="yhk">
                                <td colspan="3" class="u_hi15"></td>
                            </tr>
                            <tr class="yhk">
                                <td class="u-settxt">选择地区：</td>
                                <td></td>
                                <td class="">      
                                    <div class="f-odiva add_city">
                                        <h3 class="provice">广东省</h3>
                                        <h3 class="city">深圳市</h3>
                                        <h3 class="area">南山区</h3>
                                        <div class="f-slta-container" idx="0" id="provice" style="display: none;"></div>
                                        <div class="f-sltb-container" idx="1" id="city" style="display: none;"></div>
                                        <div class="f-sltc-container" idx="2" id="prefecture" style="display: none;"></div>
                                        <div class="u-cls"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="yhk">
                                <td colspan="3" class="u_hi15"></td>
                            </tr>
                            <tr class="yhk">
                                <td class="u-settxt">支行名称：</td>
                                <td></td>
                                <td>
                                    <div class="u-ipt-d s-bg-white" style="width:258px;">
                                        <input type="text" style="width:241px" class='subbank' ph="例如：南山支行" />
                                    </div>
                                </td>
                            </tr>
                            <tr class="yhk">
                                <td colspan="3" class="u_hi15"></td>
                            </tr>
                            <tr class="yhk">
                                <td class="u-settxt ">银行账号：</td>
                                <td></td>
                                <td style='position:relative'>
                                    <span class='card_high'></span>
                                    <div class="u-ipt-d s-bg-white" style="width:258px;">
                                        <input type="text" style="width:241px" class='bankcard' ph="输入您的银行卡号" />
                                    </div>
                                </td>
                            </tr>
                            <tr class="zfb" style="display:none;">
                                <td colspan="3" class="u_hi15"></td>
                            </tr>
                            <tr class="zfb" style="display:none;">
                                <td class="u-settxt ">支付宝帐号：</td>
                                <td></td>
                                <td style='position:relative'>
                                    <span class='card_high'></span>
                                    <div class="u-ipt-d s-bg-white" style="width:258px;">
                                        <input type="text" style="width:241px" maxlength="50" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="u_mall11">
                            <input type="submit" value="提交" class="u-btn12 s-bg-2a f-btn-submit" />
                        </div>
                    </div>
                    <!-- 提现银行-->
                    <div class="m-acc-cons s-bg-fc u_mall4">
                        <h3 class="s-bg-fe">提现银行</h3>
                        <table>
                            <tr>
                                <th width="16px"></th>
                                <th width="80px" class="u_tl">开户人</th>
                                <th width="120px" class="u_tl">开户银行</th>
                                <th width="170px" class="u_tl">帐号</th>
                                <th width="100px" class="u_tl">绑定日期</th>
                                <th width="80px">操作</th>
                                <th width="16px"></th>
                            </tr>
                            <volist name="info" id="vo">
                                <tr>
                                    <td width="16px"></th>
                                    <td width="80px" class="u_tl">{pigcms{$vo.nickname}</td>
                                    <td width="120px" class="u_tl">{pigcms{$vo.bankName}</td>
                                    <td width="170px" class="u_tl">{pigcms{$vo.account}</td>
                                    <td width="100px" class="u_tl">{pigcms{$vo.addtime|date="Y-m-d",###}</td>
                                    <td width="80px"><a href="{pigcms{:U('Account/delbank?id=')}{pigcms{$vo.id}" onclick="return confirm('确定要删除吗？')" class="delbank"  id="{pigcms{$vo.id}">删除</a>
                                        <if condition="$vo.bankName eq '支付宝'">
                                            <else/>
                                            <a href="javascript:void(0)" k="{pigcms{$vo.id}" class="u-abtn8 data-edit">修改</a>
                                        </if>
                                    </td>
                                    <td width="16px"></td>
                                </tr>
                            </volist>
                        </table>
                    </div>
            </div>
        </div>
        <!-- 右边 -->
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<include file="Public:sidebar"/>
	<script type="text/javascript" src="{pigcms{$static_path}js/city.min.js?v=20150709"></script>
	<script type="text/javascript" src="{pigcms{$static_path}js/bankcard.js"></script>        
	<script type="text/javascript" src="{pigcms{$static_path}js/popupBox.js"></script>
	<script type="text/javascript">
		//选银行卡
		$(".m-slt-bank h3,.edit-m-slt-bank h3").click(function (event) {
			$(this).css({ "border": "1px solid #2aa3ce", "background-position": "127px -618px" });
			$(this).next().css("display", "block");
			event.stopPropagation();
		});
		$(".account-result p").click(function (event) {
			$(".m-slt-bank h3").text($(this).text());
			$(this).parent().css("display", "none").prev().css({ "border": "1px solid #dbdbdb", "background-position": "127px -500px" });
			if ($(this).text() == "支付宝") {
				$(".zfb").css("display", "");
				$(".yhk").css("display", "none");
			}
			else {
				$(".zfb").css("display", "none");
				$(".yhk").css("display", "");
			}
			event.stopPropagation();
		});
		$(".edit-account-result p").click(function (event) {
			$(".edit-m-slt-bank h3").text($(this).text());
			$(this).parent().css("display", "none").prev().css({ "border": "1px solid #dbdbdb", "background-position": "127px -500px" });
			event.stopPropagation();
		});
		$(document.body).click(function () {
			$(".account-result").css("display", "none");
			$(".m-slt-bank h3").css({ "border": "1px solid #dbdbdb", "background-position": "127px -500px" });
		});

		//添加银行卡
		$(".f-btn-submit").click(function () {
			var bankName = $(".f-slt-c").text();
                        var nickname = $("#nickname").val();

			if (bankName == "支付宝") {
				var ac = $(".zfb input[type='text']").val().trim();
				if (ac == "") {
					showDialog(lang.a18);
					return;
				}
				$.post("{pigcms{:U('Account/setbankcard_data')}", { "bankName": bankName,"nickname":nickname, "account": ac,"city": "", "subBank":""}, function (data, status) {
					if (data == 1) {
						showDialog(lang.a31, location.href);
					}
					else {
						showDialog(lang.a23);
					}
				})
			}
			else {
				if ($(".subbank").val() == "" || $(".subbank").val() == $(".subbank").attr("ph")) {
					showDialog(lang.a29);
					return;
				}
				if ($(".bankcard").val().length < 16 || $(".bankcard").val() == $(".bankcard").attr("ph")) {
					showDialog(lang.a30);
					return;
				}
				var city = $(".add_city .provice").text() + "," + $(".add_city .city").text() + "," + $(".add_city .area").text();
				$.post("{pigcms{:U('Account/setbankcard_data')}", { "bankName": bankName,"nickname":nickname,"account": $(".bankcard").val(), "city": city, "subBank": $(".subbank").val() }, function (data, status) {
					if (data == 1) {
						showDialog(lang.a31, location.href);
					}
					else {
						showDialog(lang.a23);
					}
				})
			}
		})

		$(".bankcard").keyup(function () {
			clearNoNum1(this);
			var temp = "";
			var num = $(this).val();
			if (num == "")
				return;
			for (var i = 0; i < num.length; i++) {
				temp += num.charAt(i);
				if ((i + 1) % 4 == 0)
					temp += " ";
			}
			$(".card_high").html(temp).css("display", "block");
		}).focusout(function () {
			$(".card_high").css("display", "none");
		}).focus(function () {
			if ($(this).val() != "")
				$(".card_high").css("display", "block");
		})
		//弹出 
		var bankId = -1;
		$(".data-edit").click(function () {
			$.post("{pigcms{:U('Account/getbyIdbankcard')}", { "id": $(this).attr("k") }, function (data) {
				if (data) {
                                    var arr = data.city.split(",");
                                    $("#upnickname").val(data.nickname);
                                    $(".edit-m-slt-bank h3").text(data.bankName);
                                    $(".edit-provice").text(arr[0]);
                                    $(".edit-city").text(arr[1]);
                                    $(".edit-area").text(arr[2]);
                                    $("#upd_bank_sub").val(data.subBank);
                                    $("#upd_bankid").val(data.id);
                                    $("#upd_num").val(data.account);
                                    showBox("pop1");
                                    $("#pop1").css("left", "29%");
				}
			},'json')
		});
		$(".close").click(function () {
			closeBox("pop1");
			closeBox("pop1_bg");
		});
		//修改
		$("#upd_submit").click(function () {
                    
			if ($("#upd_bank_sub").val() == "") {
				showDialog(lang.a29);
				return;
			}
			if ($("#upd_num").val().length < 16) {
				showDialog(lang.a30);
				return;
			}
                        var nickname = $("#upnickname").val();
			var city = $(".edit-provice").text() + "," + $(".edit-city").text() + "," + $(".edit-area").text();
                        $.post("{pigcms{:U('Account/upbankcard')}", { "id": $("#upd_bankid").val(),"nickname":nickname, "bankName": $(".edit-m-slt-bank h3").text(), "account": $("#upd_num").val(), "city": city, "subBank": $("#upd_bank_sub").val() }, function (data) {
                                if (data == 1) {
					alert(lang.a58);
					location.href = location.href;
				}
				else {
					alert(lang.a35);
					location.href = location.href;
				}
			})
		})
	</script> 
</body>
</html>
