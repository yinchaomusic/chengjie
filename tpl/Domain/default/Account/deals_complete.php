<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
        <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
        
        <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/jquery-ui.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>我的账户</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
			<!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>我的所有交易</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 选项卡 -->
            <div class="m-acc-select u_be9 s-bg-white">
                <include file="Account/deals_nav"/>
                <!-- 搜索 -->
                <form action="{pigcms{:U('Account/deals_complete')}" method="post">
                    <div class="m-acc-search">
                        <span class="u-ipt-i u_fl" style="border: 1px solid rgb(235, 235, 235);">
                                <input type="text" name="domainName" class="i1 i_domain" ph="请输入要查询的域名" style="color: rgb(216, 216, 216);">
                                <span class="f-imgt1">起止时间</span>
                                <input type="text" class="i2" id="startTime" style="margin-right:0;" name="startTime"/></>
                                <span class="f-imgt2">至</span>
                                <input type="text" class="i3" name="endTime" id="endTime"/>
                        </span>
                        <input type="submit" value="搜索" class="u-btn8" id="my_parse_serch">
                    </div>
                </form>
                <!-- 内容 -->
                <div class="m-acc-cons s-bg-fc u_mall1" id="myalldeals">
                    <table>
                        <tr>
                            <th width="16px"></th>
                            <th width="150px" class="u_tl"><span order="title">域名</span></th>
                            <th width="120px" class="u_tr"><span order="buymoney">金额</span></th>
                            <th width="36px"></th>
                            <th width="100px" class="u_tl"><span order="_type">来源</span></th>
<!--                            <th width="90px" class="u_tl"><span order="behavior">行为</span></th>-->
                            <th width="180px" class="u_tl"><span order="_state" class="f-img-up">状态</span></th>
                            <th width="64px">操作</th>
                            <th width="8px"></th>
                        </tr>
                        
                        
                        <volist name="order_list" id="vo">
                            <tr>
                                <td width="16px"></td>
                                <td width="150px" class="u_tl"><span order="title"><if condition="$vo['domainName'] eq ''">批量交易</if> {pigcms{$vo.domainName}</span></td>
                                <td width="120px" class="u_tr"><span order="buymoney">￥{pigcms{$vo.total_price|number_format}</span></td>
                                <td width="36px"></td>
                                <td width="100px" class="u_tl"><span order="_type"><if condition="$vo['trade_type'] eq 0">交易<elseif condition="$vo['trade_type'] eq 1"/>中介<else/>批量</if></span></td>
<!--                                <td width="90px" class="u_tl"><span order="behavior"></span></td>-->
                                <td width="180px" class="u_tl"><span order="_state" class="f-img-up">
                                        
                                        <if condition="$vo['yes_no'] eq 1">
                                            等待买家同意条款
                                            <elseif condition="$vo['yes_no'] eq 2" />
                                            等待卖家同意条款
                                            <elseif condition="$vo['yes_no'] eq 3" />
                                            等待买家付钱
                                            <elseif condition="$vo['yes_no'] eq 4" />
                                            等待卖家转移域名
                                            <elseif condition="$vo['yes_no'] eq 5" />
                                            买家拒绝
                                            <elseif condition="$vo['yes_no'] eq 6" />
                                            卖家拒绝
                                            <elseif condition="$vo['yes_no'] eq 7" />
                                            后台关闭
                                            <elseif condition="$vo['yes_no'] eq 8" />
                                            经纪人关闭
                                            <elseif condition="$vo['yes_no'] eq 9" />
                                            等待买家确认收货
                                            <elseif condition="$vo['yes_no'] eq 10" />
                                            交易达成
                                        </if>
                                    </span></td>
                                <td width="64px"><a target="_blank" href="{pigcms{:U('Account/dealdetails',array('id'=>$vo['order_id']))}" class="u-abtn8">详情</a></td>
                                <td width="8px"></td>
                            </tr>
                        </volist>
                    </table>
                </div>
                <!-- 分页 -->
                <div id="paging" class="g-padding g-padding1">{pigcms{$pagebar}</div>
            </div>
        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
        <script src="{pigcms{$static_path}js/jquery-ui-datepicker.js"></script>
        <script>
            $("#startTime").datepicker({
			maxDate: new Date(), prevText: "上个月", nextText: "下个月", onSelect: function (dateText, obj) {
				$("#endTime").datepicker("option", "minDate", dateText);
			}
		});
		$("#endTime").datepicker({
			maxDate: new Date(), prevText: "上个月", nextText: "下个月", onSelect: function (dateText, obj) {
				$("#startTime").datepicker("option", "maxDate", dateText);
			}
		});
        </script>
	<include file="Public:sidebar"/>
</body>
</html>
