<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>我的账户</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>我委托的代购</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 选项卡 -->
            <div class="m-acc-select u_be9 s-bg-white">
                <!-- 标题 -->
                <div class="m-acc-title s-bg-fc">
                    <div class="u-fl"><a href="{pigcms{:U('Account/pay')}">审核</a></div>
                    <div class="u-fl"><a class="active" href="{pigcms{:U('Account/paying')}">代购中</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/paysuccess')}">代购成功</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/payend')}">代购失败</a></div>
                    <input type="hidden" id="checktype" value="1">
                    <div class="u-cls"></div>
                </div>
                <!-- 内容 -->
                    <div class="m-acc-cons s-bg-fc u_mall1">
                        <table class="tb_ap_fail">
                            <tbody>
                                <tr>
                                    <th width="16px"></th>
                                    <th width="150px" class="u_tl">域名</th>
                                    <th width="120px" class="u_tr">预算</th>
                                    <th width="120px"></th>
                                    <th width="197px" class="u_tl">申请时间</th>
                                    <th width="120px">操作</th>
                                </tr>
                                <if condition="is_array($paying_list)">
                                    <volist name="paying_list" id="vo">
                                        <tr>
                                            <td width="16px"></td>
                                            <td width="150px" class="u_tl">{pigcms{$vo.domain}</td>
                                            <td width="120px" class="u_tr">￥{pigcms{$vo.buyers_price|number_format}</td>
                                            <td width="120px"></td>
                                            <td width="197px" class="u_tl">{pigcms{$vo.add_time|date="Y-m-d H:i:s",###}</td>
                                            <td width="120px">
                                                
                                                <if condition="$vo['status'] eq 5">
                                                    已付款
                                                    <elseif condition="$vo['status'] eq 8"/>
                                                    <a id="confirm" data-myid="{pigcms{$vo.ph_id}" >确认收货</a> 
                                                    <elseif condition="$vo['status'] eq 9"/>
                                                    <a class="payment" kid="{pigcms{$vo.ph_id}" >付款</a> | 
                                                    <a href="{pigcms{:U('Account/pay_undo',array('ph_id'=>$vo['ph_id'],'freeze_id'=>$vo['freeze_id']))}" kid="{pigcms{$vo.ph_id}">取消</a>
                                                    <elseif condition="$vo['status'] eq 4"/>
                                                    等待经纪人联系卖家
                                                </if>
                                            </td>
                                        </tr>
                                    </volist>
                                    <else/>
                                    <tr><td class="u_nb u_tc u_norecord" colspan="7">暂无记录！</td></tr>
                                </if>
                            </tbody>
                        </table>
                    </div>   
                <!-- 分页 -->
                <div class="g-padding g-padding1" id="paging">{pigcms{$pagebar}</div>
            </div>
            
            
            
            
        <div style="position: absolute; display: none; z-index: 1005; left: 29%; top: 331.5px;" class="m-pop2 s-bg-white"  id="pop1">
        <div class="pop-insa">
            <ul>
                <li>
                    <span class="u-span18 u-fl">1.</span>
                    <span class="u-span19 u-fl">请支付尾款。。
                    </span>
                    <div class="u-cls"></div>
                </li>
                <li>
                    <span class="u-span18 u-fl">2.</span>
                    <span class="u-span19 u-fl">您需要缴纳保证金以外的尾款。。</span>
                    <div class="u-cls"></div>
                </li>
            </ul>
        </div>
        <div class="pop2-data1">
            <ul>
                <li>
                    <span class="u-span20">用户id：</span>
                    <span class="s-75b u-span21 amount" id="uid"></span>
                    <input type="hidden" value="" id="pay_ph_id">
                    <div class="u-cls"></div>
                </li>
                <li>
                    <span class="u-span20">我的账户余额：￥</span>
                    <span class="u-span21" id="now_money"></span>
                    <div class="u-cls"></div>
                </li>
                
                <li>
                    <span class="u-span20">报价总额：￥</span>
                    <span class="s-75b u-span21 amount" id="buyers_price"></span>
                    <div class="u-cls"></div>
                </li>
                
                <li>
                    <span class="u-span20">保证金：￥</span>
                    <span class="s-75b u-span21 amount" id="margin"></span>
                    <div class="u-cls"></div>
                </li>
                <li>
                    <span class="u-span20">平台手续费：￥</span>
                    <span class="s-75b u-span21 amount" id="poundage"></span>
                    <div class="u-cls"></div>
                </li>
                
                <li>
                    <span class="u-span20">还要支付：￥</span>
                    <span class="s-75b u-span21 amount" id="again_pay"></span>
                    <div class="u-cls"></div>
                </li>
                
            </ul>
        </div>
        <div class="pop2-btn s-bg-fa">
            <input id="payment" value="提交付款" class="u-btn12 s-bg-2a" type="button">
        </div>
        <div class="close"></div>
    </div>
            
            
            
            
            
            
            
            
            
        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<script src="{pigcms{$static_path}js/jquery-1.8.2.min.js"></script>
	<script src="{pigcms{$static_path}js/common.js"></script>
	<script src="{pigcms{$static_path}js/account.js"></script>
        
        <script type="text/javascript" src="{pigcms{$static_path}js/popupBox.js"></script>
        <script>
            //确认收货
            $("#confirm").click(function(){                    
                swal({   
                    title: "确认收货?",
                    text: "是否收到域名如果收到域名就确认收货!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "是我收到域名了!",
                    closeOnConfirm: false },
                function(){  
                    var myid= jQuery("#confirm").data('myid');
                    
                    $.post("{pigcms{:U('Account/pay_confirm')}",{"ph_id":myid},function(data){
                        if(data == 1){
                            swal("确认成功!", "已经成功收货.", "success"); 
                            location.href = location.href
                        }else{
                            sweetAlert("异常...", "确认收货异常!", "error");
                            location.href = location.href
                        }
                    })
//                    var confirm_id = $(this).attr("confirm_id");
//                    alert(confirm_id);
                });
            })
            
            //获取信息
            $(".payment").click(function(){
//                alert(123);
                var kid = $(this).attr("kid");
                $.post("{pigcms{:U('Account/payment_data')}",{"ph_id":kid},function(data){
                    if(data){
                        $("#margin").text(data.margin);
                        $("#now_money").text(data.now_money);
                        $("#uid").text(data.uid);
                        $("#again_pay").text(data.again_pay);
                        $("#poundage").text(data.poundage);
                        $("#buyers_price").text(data.buyers_price);
                        $("#pay_ph_id").val(data.ph_id);
                        showBox("pop1");
                        $("#pop1").css("left", "29%");
                    }
                },'json')
                
            })
            
            $("#payment").click(function(){
                var ph_id = $("#pay_ph_id").val();
                var now_money = $("#now_money").text();
                var again_pay = $("#again_pay").text();
                var poundage = $("#poundage").text();
                var margin = $("#margin").text();
                if( parseInt(now_money) < parseInt(again_pay) ){
                    alert(lang.a26);
                    location.href = location.href;
                }else{
                    $.post("{pigcms{:U('Account/payment')}",{"ph_id":ph_id},function(data){
//                        alert(data);
                        if(data == 3){
                            alert(lang.a26);
                            location.href = location.href;
                        }
                        if(data == 1){
                            alert('交易成功')
                            location.href = location.href;
                        }else{
                            alert('交易异常')
                            location.href = location.href;
                        }
                    })
                }
            })
            
            
            
            $(".close").click(function () {
			closeBox("pop1");
			closeBox("pop1_bg");
		});
            </script>
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}
	</script>
	<include file="Public:sidebar"/>
</body>
</html>
