<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>我的账户</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 左边 -->
        <div class="u-fl">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>交易详情</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 交易详情 -->
            <if condition="$order_info['behavior'] eq 1">
                <div class="m-seller s-bg-fc u_be9">
                    <h4 class="f-img-buyer s-bg-white">您是买方</h4>
                        <h3 class="u_bt2"><span class="s-2a domain">{pigcms{$order_info.domainName}</span><span class="s-ff6 z-wait">(
                                <if condition="$order_info['yes_no'] eq 1">
                                    等待您同意条款
                                    <elseif condition="$order_info['yes_no'] eq 3"/>
                                    等待您付款
                                    <elseif condition="$order_info['yes_no'] eq 2 "/>
                                    等待对方同意条款
                                    <elseif condition="$order_info['yes_no'] eq 4 "/>
                                    等待卖家提交域名
                                    <elseif condition="$order_info['yes_no'] eq 5 "/>
                                    您以拒绝
                                    <elseif condition="$order_info['yes_no'] eq 6 "/>
                                    卖家以拒绝
                                    <elseif condition="$order_info['yes_no'] eq 7 "/>
                                    后台关闭
                                    <elseif condition="$order_info['yes_no'] eq 8 "/>
                                    经纪人关闭
                                    <elseif condition="$order_info['yes_no'] eq 9 "/>
                                    等待您确认收货
                                    <elseif condition="$order_info['yes_no'] eq 10 "/>
                                    本次交易已经完成
                                </if>
                                )</span></h3>
                        <div class="m-insd">
                            <ul>
                                <li>
                                    <if condition="$order_info['payTye'] eq 1">买方来支付平台手续费
                                    <elseif condition="$order_info['payTye'] eq 2"/>卖方来支付平台手续费
                                    <elseif condition="$order_info['payTye'] eq 3"/>双方各一半的方式来支付平台手续费
                                    </if>
                                </li>
                            </ul>
                            <p class="u_btn_container2">
                                <if condition="$order_info['yes_no'] eq 1">
                                        <a class="u-btn13" id="save">修改</a>
                                        <a class="u-btn13" id="no">拒绝</a>
                                        <a class="u-btn13" id="yes">同意</a>
                                        <elseif condition="$order_info['yes_no'] eq 2 "/>
                                        <a class="u-btn13">等待对方同意条款</a> 
                                        <elseif condition="$order_info['yes_no'] eq 3 "/>
                                        <a class="u-btn13" id="sub">提交付款</a> 
                                        <elseif condition="$order_info['yes_no'] eq 4 "/>
                                        <a class="u-btn13">等待卖家提交域名</a>
                                        <elseif condition="$order_info['yes_no'] eq 5 "/>
                                        <a class="u-btn13">以拒绝</a>
                                        <elseif condition="$order_info['yes_no'] eq 6 "/>
                                        <a class="u-btn13">卖家以拒绝交易</a>
                                        <elseif condition="$order_info['yes_no'] eq 7 "/>
                                        <a class="u-btn13">后台关闭</a>
                                        <elseif condition="$order_info['yes_no'] eq 8 "/>
                                        <a class="u-btn13">经纪人关闭</a>
                                        <elseif condition="$order_info['yes_no'] eq 9 "/>
                                        <a class="u-btn13" href="{pigcms{:U('Account/successful',array('order_id'=>$order_info['order_id']))}">确认收货</a>
                                    </if>
                            </p>
                        </div>   
                </div>
            <else/>
                <div class="m-seller s-bg-fc u_be9">
                    <h4 class="f-img-buyer s-bg-white">您是卖方</h4>
                        <h3 class="u_bt2"><span class="s-2a domain">{pigcms{$order_info.domainName}</span><span class="s-ff6 z-wait">(
                                <if condition="$order_info['yes_no'] eq 1">
                                    等待对方同意条款
                                    <elseif condition="$order_info['yes_no'] eq 2"/>
                                    等待您同意条款
                                    <elseif condition="$order_info['yes_no'] eq 3"/>
                                    等待买家付款
                                    <elseif condition="$order_info['yes_no'] eq 4 "/>
                                    等待您提交域名
                                    <elseif condition="$order_info['yes_no'] eq 5 "/>
                                    买家以拒绝
                                    <elseif condition="$order_info['yes_no'] eq 6 "/>
                                    您以拒绝
                                    <elseif condition="$order_info['yes_no'] eq 7 "/>
                                    后台关闭
                                    <elseif condition="$order_info['yes_no'] eq 8 "/>
                                    经纪人关闭
                                    <elseif condition="$order_info['yes_no'] eq 9 "/>
                                    等待买家确认收货
                                    <elseif condition="$order_info['yes_no'] eq 10 "/>
                                    交易达成
                                </if>                                
                                )</span></h3>
                        <div class="m-insd">
                            <ul>
                                <li>
                                    <if condition="$order_info['payTye'] eq 1">买方来支付平台手续费
                                    <elseif condition="$order_info['payTye'] eq 2"/>卖方来支付平台手续费
                                    <elseif condition="$order_info['payTye'] eq 3"/>双方各一半的方式来支付平台手续费
                                    </if>
                                </li>
                            </ul>
                            <p class="u_btn_container2">
                                <if condition="$order_info['yes_no'] eq 2">
                                    <a class="u-btn13" id="save">修改</a>
                                    <a class="u-btn13" id="no">拒绝</a>
                                    <a class="u-btn13" id="yes">同意</a>
                                        <elseif condition="$order_info['yes_no'] eq 2"/>
                                        <a class="u-btn13" id="sub">等待对方同意条款</a> 
                                        <elseif condition="$order_info['yes_no'] eq 3"/>
                                        <a class="u-btn13" >等待买家付款</a> 
                                        <elseif condition="$order_info['yes_no'] eq 4 "/>

                                           <a class="u-btn13" href="javascript:void(0);"
                                              onclick="changedeal('{pigcms{$order_info.order_id}','{pigcms{$order_info['transfer_type']}');return false" >等待您提交域名</a>
                                        <elseif condition="$order_info['yes_no'] eq 5 "/>
                                        <a class="u-btn13">买家以拒绝交易</a>
                                        <elseif condition="$order_info['yes_no'] eq 6 "/>
                                        <a class="u-btn13">以拒绝</a>
                                        <elseif condition="$order_info['yes_no'] eq 7 "/>
                                        <a class="u-btn13">后台关闭</a>
                                        <elseif condition="$order_info['yes_no'] eq 8 "/>
                                        <a class="u-btn13">经纪人关闭</a>
                                        <elseif condition="$order_info['yes_no'] eq 9 "/>
                                        <a class="u-btn13">等待买家确认收货</a>
                                </if>
                                 
                            </p>
                        </div>   
                </div>
            </if>
            
            <!-- 交易数据 -->
            <div class="m-account-data1 u_be9 s-bg-fc">
                <h4 class="s-bg-white">
                    <a class="s-3c m-ta2" href="javascript:void(0);">
                        <span class="sp1">交易域名</span>
                    </a>
                    <div class="u-cls"></div>
                </h4>
                <table>
                    <tbody><tr>
                        <th width="16px"></th>
                        <th class="u_tl" width="150px">来源</th>
                        <th class="u_tl" width="200px">域名</th>
<!--                        <th class="u_tl" width="200px"><if condition="$order_info['transfer_type'] eq 1">卖家帐号<elseif condition="$order_info['transfer_type'] eq 2"/>注册商</if></th>-->
                        <th class="u_tl" width="200px">注册商ID/转移密码</th>
                            <th class="u_tr" width="120px">价格</th>
                            <th width="16px"></th>
                    </tr>
                        <tr>
                            <td></td>
                            <td class="u_tl">
                                <span class="s-2a">中介</span>
                            </td>
                            <td class="u_tl"><span class="s-2a"><if condition="$order_info['domainName'] eq ''">批量交易<else/>{pigcms{$order_info.domainName}</if></span></td>
<!--                            <td class="u_tl">{pigcms{$order_info.registrar}</td>-->
                            <td class="u_tl">{pigcms{$order_info.registrar}/{pigcms{$order_info.registrar_id}</td>
                            <td class="u_tr"><span class="s-75b">￥{pigcms{$order_info.total_price|number_format=###}</span></td>
                            <td></td>
                        </tr>  
                </tbody></table>
                <div class="m-total2">
                    <div class="u-fr">
                        <if condition="$order_info['my_payTye'] eq 1">
                                <p><span class="u-span15">报价总额：</span><span class="u_m4 s-75b u_mb15">￥{pigcms{$order_info.total_price|number_format=###}
                                    </span></p>
                                <p><span class="u-span15">手续费：</span><span class="u_m4 s-75b u_mb15">￥<?php echo $user_level_info['boon']/100*$order_info['total_price']?></span></p>
                            <elseif condition="$order_info['my_payTye'] eq 2"/>
                                <p><span class="u-span15">报价总额：</span><span class="u_m4 s-75b u_mb15">￥{pigcms{$order_info.price|number_format=###}
                                    </span></p>
                                <p><span class="u-span15">手续费：</span><span class="u_m4 s-75b u_mb15">￥<?php echo $user_level_info['boon']/100*$order_info['total_price']/2?></span></p>
                            <else/>
                                <p><span class="u-span15">报价总额：</span><span class="u_m4 s-75b u_mb15">￥{pigcms{$order_info.total_price|number_format=###}
                                    </span></p>
                                <p><span class="u-span15">手续费：</span><span class="u_m4 s-75b u_mb15">卖家支付</span></p>
                        </if>
                    </div>
                    <div class="u-fr txt-total">合计</div>

                    <div class="u-cls"></div>
                </div>
            </div>
            <!--交易进度-->
            <div class="m-account-data1 u_be9 s-bg-fc">
                <h4 class="s-bg-white">
                    <a class="s-3c m-ta2" href="javascript:void(0);">
                        <span class="sp1">交易进度</span>
                    </a>
                    <div class="u-cls"></div>
                </h4>
                <table>
                        <tbody>
                        <volist name="order_info_list" id="vv">
                            <tr>
                                <td></td>
                                <td><span class="u-span17">{pigcms{$vv.info}</span></td>
                                <td>{pigcms{$vv.time|date="Y-m-d H:i:s",###}</td>
                                <td></td>
                            </tr> 
                        </volist>
                </tbody></table>
            </div>
            
            <div style="position: absolute; display: none; z-index: 1005; left: 29%; top: 331.5px;" class="m-pop2 s-bg-white" id="pop15">
                    <div class="pop-insa">
                        <ul>
                            <li>
                                <span class="u-span18 u-fl">1.</span>
                                <span class="u-span19 u-fl">转移密码：由卖家提供，您在您的新注册商申请域名转入时提交填写。3~7个工作日即可完成过户。。
                                </span>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <span class="u-span18 u-fl">2.</span>
                                <span class="u-span19 u-fl">您提交易域名所在注册商的账号，卖家操作push,您接收即可。1小时内甚至几分钟即可完成过户（要求域名所在注册商支持push过户方式）。。</span>
                                <div class="u-cls"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="pop2-data1">
                        <ul>
                            <li>
                                <span class="u-span20">订单总额：￥</span>
                                <span class="s-75b u-span21 amount" id="total_price"></span>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <span class="u-span20">手续费：￥</span>
                                <span class="s-75b u-span21 amount" id="poundage"></span>
                                <div class="u-cls"></div>
                            </li>
                            
                            <li>
                                <span class="u-span20">账户金额：￥</span>
                                <span class="u-span21" id="now_money"></span>
                                <div class="u-cls"></div>
                            </li>
                            <li style="dislay:none">
                                <span class="u-span20">冻结保证金：￥</span>
                                <span class="u-span21" id="freezemoney"></span>
                                <div class="u-cls"></div>
                            </li>
                            
                            <li>
                                <span class="u-span20">过户方式：</span>
                                <label class="u_mr18">
                                    <input checked="checked" name="registrar" value="1" id="returnpwd" class="u_vb" type="radio">
                                    <span>卖家提供域名转移密码</span>
                                </label>
                                <label>
                                    <input name="registrar" class="u_vb" value="2" type="radio">
                                    <span>当前注册商内转移(PUSH)</span>
                                </label>
                                <div class="u-cls"></div>
                            </li>
                            <li style="display: none;" class="sltRegister">
                                <span class="u-span20"  style="line-height: 24px;">注册商：</span>
                                <span class="u-ipt-n">
                                    <input  id="transfer_one" value="" type="text">
                                </span>
                                <div class="u-cls"></div>
                            </li>
                            <li class="sltRegister" style="margin-bottom: 0px; display: none;">
                                <div class="u-span20" style="line-height: 24px;">注册商ID：</div>
                                <div class="u-fl">
                                    <span class="u-ipt-n">
                                        <input id="transfer_two" value="" type="text">
                                    </span>
                                    <span class="u_ts2">请填写您当前的注册商和注册商帐号</span>
                                </div>
                                <div class="u-cls"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="pop2-btn s-bg-fa">
                        <input id="payfor" k="1000000" value="提交付款" class="u-btn12 s-bg-2a" type="button">
                    </div>
                    <div class="close"></div>
                </div>
            <div style="position: absolute; display: none; z-index: 1005; left: 29%; top: 331.5px;" class="m-pop2 s-bg-white" id="pop14">
                    <form method="post" action="{pigcms{:U('Account/dealdetailsInfo')}" >
                        <if condition="$order_info['behavior'] eq 1">
                            <input type="hidden" id="sell_buy" name="up" value="buy" >
                            <else/>
                            <input type="hidden" id="sell_buy" name="up" value="sell" >
                        </if>
                        
                        <input type="hidden" name="id" id="el_id" value="{pigcms{$order_info.order_id}"/>
                    <div class="pop2-data1">
                        <ul>
                            <li>
                                <span class="u-span20">报价：</span>
                                <span class="s-75b u-span21 amount">￥{pigcms{$order_info.total_price}</span>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <span class="u-span20">平台手续费：</span>
                                <span class="u-span21">￥
                                    <if condition="$order_info['payTye'] eq  1">
                                        <?php echo $user_level_info['boon']/100*$order_info['total_price']?></span>
                                    <elseif condition="$order_info['payTye'] eq  2"/>
                                卖家支付
                                    <else/>
                                        <?php echo $user_level_info['boon']/100*$order_info['total_price']/2 ?></span>
                                    </if>
                                    
                                    
                                <div class="u-cls"></div>
                            </li>
                            
                            <li>
                                <span class="u-span20">过户方式：</span>
                                    <if condition="$order_info['payTye'] eq  1">
                                        <input type="radio" name="payTye" value="1"checked="checked" />买家支付
                                        <input type="radio" name="payTye" value="2" />卖家支付
                                        <input type="radio" name="payTye" value="3" />双方各付50%
                                    <elseif condition="$order_info['payTye'] eq  2"/>
                                        <input type="radio" name="payTye" value="1" />买家支付
                                        <input type="radio" name="payTye" value="2" checked="checked" />卖家支付
                                        <input type="radio" name="payTye" value="3" />双方各付50%
                                    <else/>
                                        <input type="radio" name="payTye" value="1" />买家支付
                                        <input type="radio" name="payTye" value="2" />卖家支付
                                        <input type="radio" name="payTye" value="3" checked="checked"/>双方各付50%
                                    </if>

                                <div class="u-cls"></div>
                            </li>
                            
                        </ul>
                    </div>
                    <div class="pop2-btn s-bg-fa">
                        <input type="submit"  value="提交修改" class="u-btn12 s-bg-2a" type="button">
                    </div> 
                    <div class="close" id="test"></div>
                    </form>
                </div>
        </div>
        <!-- 右边 -->
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">
    <script src="{pigcms{$static_path}js/popupBox.js"></script>
    <script>
         

          var submit_check="{pigcms{:U('Account/change_dealdetails')}";

          function changedeal(id,ttype){
              if(ttype == 1){
                  swal({   title: "提交域名",
                           text: "请填写您当前的注册商和注册商帐号，用逗号( , )分隔,\n注意：不支持有逗号的密码,如：1ad32,121",
                           type: "input",   showCancelButton: true,   closeOnConfirm: false,
                           animation: "slide-from-top",
                           inputPlaceholder: "例如：username,password"
                     }, function(inputValue){
                        if (inputValue === false) return false;
                      var inputVexploder = inputValue.split(",");
                         if ( (typeof(inputVexploder[0]) !="string" ) || (typeof(inputVexploder[1]) !="string" ) ) {
                             swal.showInputError("请填写您当前的注册商和注册商帐号!");     return false
                         }
                         //swal("提交成功!", "提交信息: " + inputValue, "success");
                      $.post(submit_check, {'order_id': id,'transfer_type':ttype,'registrar':inputVexploder[0],'registrar_id':inputVexploder[1]}, function (result) {
                          result = $.parseJSON(result);
                          if (result) {
                              if (result.error == 0) {
                                  swal('', '提交成功', 'success');
                                  setTimeout(function () {
                                      window.location.reload();
                                  }, 1000);
                              } else {
                                  swal({title: '提交出现错误', text: result.msg, type: "error", timer: 3000, showConfirmButton: true});
                              }
                          } else {
                              swal('', '提交出现异常，请重试！', 'error');
                          }
                      });
                  });
              }else if(ttype == 2) {
                  swal({
                          title: "确定要提交?", text: "买家已付款，等待您提供域名！",
                          type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55",
                          confirmButtonText: "提交", closeOnConfirm: false
                      },
                      function () {
                          $.post(submit_check, {'order_id': id,'transfer_type':ttype}, function (result) {
                              result = $.parseJSON(result);
                              if (result) {
                                  if (result.error == 0) {
                                      swal('', '提交成功', 'success');
                                      setTimeout(function () {
                                          window.location.reload();
                                      }, 1000);
                                  } else {
                                      swal({title: '提交出现错误', text: result.msg, type: "error", timer: 3000, showConfirmButton: true});
                                  }
                              } else {
                                  swal('', '提交出现异常，请重试！', 'error');
                              }
                          });
                      });
              }

          }
            
         $("#test").click(function () {
                closeBox("pop14");
            });
            
            $(".sltRegister").css("display", "none");
            
            
            $('#payfor').click(function () {
                if (confirm(lang.a178)) {
                    var total_price = $("#total_price").text();
                    var now_money = $('#now_money').text();
//                    alert(parseFloat(now_money));
//                    alert(parseFloat(total_price));
//                    if(now_money < parseFloat(total_price)){
//                        alert("您的余额不足请充值");
//                        location.href = location.href;
//                        die;
//                    }
//                    die;
                    var order_id = $("#el_id").val(); 
                    var transfer_type =$('input:radio[name="registrar"]:checked').val();
                    var registrar = $("#transfer_one").val();
                    var registrar_id = $("#transfer_two").val();
//alert(transfer_type);die;
                    $.post("{pigcms{:U('Account/order_pay_data')}",{"order_id":order_id,"transfer_type":transfer_type,"registrar":registrar,"registrar_id":registrar_id},function(data){
                        if(data == 1){
                             swal("成功!", "交易成功等待卖家提交域名!", "success")
                             location.href = location.href;
                        }else{
                            sweetAlert("异常...", "此次交易异常!", "error");
                        }
                        
                        if(data == 3){
                            sweetAlert("提示...", "您的余额不足请充值!", "error");
                            location.href = location.href;
                        }
                    })
                }
            });
            
            $("#sub").click(function () {
                var order_id = $("#el_id").val(); 
                $.post("{pigcms{:U('Account/order_pay')}",{"order_id":order_id},function(data){
                    $("#total_price").text(data.total_price);
                    $('#now_money').text(data.now_money);
                    $("#poundage").text(data.poundage);
                    $('#freezemoney').text(data.freezemoney);
                    
//                    alert(data);
                    showBox("pop15");
                    $("#pop15").css("left", "29%");
                },'json')
                
            });
            $(".close").click(function () {
                closeBox("pop15");
            });
            
            $("#save").click(function () {
                showBox("pop14");
                $("#pop14").css("left", "29%");
            });
            
            $("#yes").click(function () {
                var id = $("#el_id").val(); 
                var sell_buy = $("#sell_buy").val(); 
                
                if(sell_buy == 'buy'){
                    var info = '买家同意了交易条款，等待付款';
                }else{
                    var yes_no = '卖家同意了交易条款，等待买家付款';
                } 
                
                $.post("{pigcms{:U('Account/el_yes_no')}",{"order_id":id,"yes_no":3,info:info},function(data){
                    if(data == 1){
                        location.href = location.href;
                    }else{
                        showDialog(lang.a23);
                    }
                })
            });
            
            $("#no").click(function () {
                var id = $("#el_id").val(); 
                var sell_buy = $("#sell_buy").val(); 
                if(sell_buy == 'buy'){
                    var yes_no = 5;
                    var info = '买家拒绝了交易条款';
                }else{
                    var yes_no = 6;
                    var info = '卖家拒绝了交易条款';
                }
                $.post("{pigcms{:U('Account/el_yes_no')}",{"order_id":id,"yes_no":yes_no,info:info,status:3},function(data){
                    if(data == 1){
                        location.href = location.href;
                    }else{
                        showDialog(lang.a23);
                    }
                })
            });
           
            
            $(".pop2-data1 label").click(function () {
                var index = $(this).index();
                if (index == 2) {
                    $(".sltRegister").css("display", "block");
                } else {
                    $(".sltRegister").css("display", "none");
                }
            });
            
            
            
            
            
            
           

            $("#djq").change(function () {
                var djq = $("#djq option:selected").attr("pay");
                var amount = $("#payfor").attr("k");
                var result = Number(amount) - Number(djq);
                result = result < 0 ? 0 : result;
                $(".amount").html(formatMoney(result, 0, "￥"))
            })

        </script>   
    
    
    
	<include file="Public:sidebar"/>
</body>
</html>
