<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>
<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>我的账户</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="g-content u-cls">
	<div class="m-content">
		<include file="Account:sidebar"/>
		<!-- 右边 -->
		<div class="u-fr">
			<!-- 标题&验证 -->
			<div class="s-bg-fc u_be9" style="width: 702px;">
				<div class="m-ta s-bg-white">
					<div class="title">
						<a class="s-3c" href="javascript:void(0)">
							<span>账号安全</span>
						</a>
					</div>
					<div class="u-cls"></div>
				</div>
			</div>
			<!-- 选项卡 -->
			<div class="m-acc-select u_be9 s-bg-white">
				<!-- 标题 -->
				<include file="Account:safe_nav"/>

				<div class="m-acc-cons s-bg-fc u_mall1">
					<div class="m-inse">
						<p class="f-p-t14">为了保护您的账号安全，您可以添加最多5个安全问题。您在修改密码等操作时需要回答这些安全问题。！</p>
					</div>
				</div>

				<div class="m-acc-cons s-bg-fc u_mall2">
					<table>
						<tr>
							<th width="6px"></th>
							<th width="56px">编号</th>
							<th width="588px" class="u_tl">安全问题</th>
							<th width="56px">操作</th>
							<th width="16px"></th>
						</tr>
						<volist name="protect_list" id="vo">
						<tr>
							<td class="u_nb"></td>
							<td class="u_nb">{pigcms{$i}</td>
							<td class="u_tl u_nb">{pigcms{$vo.question}</td>
							<td class="u_nb">
			<a href="javascript:void(0);" class="u-abtn8 btn_del" onclick="del_pro('{pigcms{$vo.id}');return false">删除</a>
							</td>
							<td class="u_nb"></td>
						</tr>
						</volist>
					</table>
				</div>



				<form id="protect_from"   method='post'>
					<div class="m-question s-bg-fc u_be9">
						<ul>
							<li class="u_mb15">
								<div class="question_txt">安全问题：</div>
								<div class="question_ipt s-bg-white">
									<input type="text" id="question" name='question' ph="请输入您的问题" />
								</div>
								<div class="u-cls"></div>
							</li>
							<li class="u_mb15">
								<div class="question_txt">安全答案：</div>
								<div class="question_ipt s-bg-white">
									<input type="text" id="answer" name='answer' ph="请输安全答案" />
								</div>
								<div class="u-cls"></div>
							</li>
							<li class="u_mb15">
								<div class="question_txt">您的登录密码：</div>
								<div class="question_ipt s-bg-white">
									<input type="password" id="pwd" name='pwd' />
								</div>
								<div class="u-cls"></div>
							</li>
							<li>
								<div class="u_mall12">
									<input type="submit" value="提交" class="u-btn12 s-bg-2a" />
								</div>
							</li>
						</ul>
					</div>

				</form>
			</div>

		</div>

		<div class="u-cls"></div>
	</div>
</div>
<include file="Public:footer"/>
<include file="Public:account_footer"/>

<script type="text/javascript">
	var submit_check="{pigcms{:U('Account/logprotect')}",del_url="{pigcms{:U('Account/logprotect_del')}";

	function del_pro(id){
		swal({   title: "确定要删除?",   text: "如果删除了该安全问题，建议至少保留一个安全问题保证账号安全！",
			type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",
			confirmButtonText: "确定删除",cancelButtonText:"取消",   closeOnConfirm: false },
			function(){
			$.post(del_url,{'id':id},function(result){
				result = $.parseJSON(result);
				if(result){
					if(result.error == 0){
						swal('','删除成功','success');
						setTimeout(function(){
							window.location.reload();
						},1000);
					}else{
						$('#login_'+result.dom_id).focus();
						swal({   title: '删除出现错误' ,   text: result.msg ,type:"error",   timer: 3000,   showConfirmButton: true });
					}
				}else{
					swal('','删除出现异常，请重试！','error');
				}
			});
			});
	}

	$(function(){

		$('#protect_from').submit(function(){

			if($('#question').val()==''  || $('#question').val()==$('#question').attr('ph')){
				swal('','请输入您的问题','error');
				$('#question').focus();
				return false;
			}else if($('#answer').val()==''|| $('#answer').val()==$('#answer').attr('ph')){
				swal('','请输入安全答案','error');
				$('#answer').focus();
			}else if($('#pwd').val()==''){
				swal('','请输入登录密码','error');
				$('#pwd').focus();
			}else{
				$.post(submit_check,$("#protect_from").serialize(),function(result){
					result = $.parseJSON(result);
					if(result){
						if(result.error == 0){
							swal('','添加成功','success');
							setTimeout(function(){
								window.location.reload();
							},1000);
						}else{
							$('#login_'+result.dom_id).focus();
							swal({   title: '添加出现错误' ,   text: result.msg ,type:"error",   timer: 3000,   showConfirmButton: true });
						}
					}else{
						swal('','添加现异常，请重试！','error');
					}
				});
			}
			return false;
		});

	});

</script>
<include file="Public:sidebar"/>
</body>
</html>
