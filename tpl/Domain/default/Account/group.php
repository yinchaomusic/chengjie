<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我是卖家 - 域名分组管理</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>我是卖家&nbsp;&gt;&nbsp;</li>
					<li>域名分组管理</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
			<!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>域名分组管理</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 提示&警示 -->
            <div class="m-btn1-contrainer s-bg-fc u_be89" style="text-align:left;">
                <a class="u-abtn9 s-bg-2a">新建分组</a>
                <input type="hidden"  value=""/>
            </div>
            <!-- 数据 -->
            <div class="m-account-data2 s-bg-fc u_be9">
                <table>
                    <tr class="s-bg-white">
                        <th width="16px"></th>
                        <th width="170px" class="u_tl">组名</th>
                        <th width="100px" class="u_tr">域名数</th>
                        <th width="56px"></th>
                        <th width="180px" class="u_tl">备注</th>
                        <th width="180px" class="u_tr">操作</th>
                        <th width="16px"></th>
                    </tr>
					<volist name="group_list" id="vo">
						<tr>
							<td></td>
							<td class="u_tl"><span class="s-2a">{pigcms{$vo.group_name}</span></td>
							<td class="u_tr s-75b">{pigcms{$vo.domain_count}</td>
							<td></td>
							<td><span class="u-span14">{pigcms{$vo.group_desc}</span></td>
							<td class="u_tr">
								<a class="u-abtn8 btn_verify" d="{pigcms{$vo.group_id}" k="del">删除</a>
								<a class="u-abtn8 btn_verify" d="{pigcms{$vo.group_id}" k="update">设置</a>
							</td>
							<td></td>
						</tr>
					</volist>
                </table>
            </div>
            <div class="m-pop s-bg-fa" id="pop11">
                <p class="f-p-t7 u_bb0">
                    <span class="u-fl boxName">新建分组</span>
                    <span class="u-fr close"></span>
                    <span class="u-cls"></span>
                </p>
                <div class="s-bg-white pop-container">
                    <div class="u_mb15">
                        <div class="groupname u-fl">组名：</div>
                        <div class="u-ipt-j u-fl">
                            <input type="text" class="groupName" /></div>
                        <div class="u-cls"></div>
                    </div>
                    <div class="f-p28">
                        <div class="groupname u-fl">备注：</div>
                        <div class="u_txerb u-fl">
                            <textarea class="groupIntro"></textarea></div>
                        <div class="u-cls"></div>
                    </div>
                </div>
                <div class="s-bg-fa pop-btn">
                    <input type="submit" value="确定" class="u-btn12 s-bg-2a btn_post" />
                </div>
            </div>
        </div>
		<div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>

	<script src="{pigcms{$static_path}js/account.js"></script>
	<script src="{pigcms{$static_path}js/popupBox.js"></script>
	<script>


        $(".u-abtn9").click(function () {
            showBox("pop11");
            $("#pop11").css("left", "38%");
            $(".boxName").html(lang.a55);
            $(".groupName").val("");
            $(".groupIntro").val("");
            $(".btn_post").attr({ "k": "add", "d": "" });
        });
        $(".close").click(function () {
            closeBox("pop11");
        });

        $(".btn_verify").live("click", function () {
            var groupName = $(this).parent().prev().prev().prev().prev().text();
            var d = $(this).attr("d");
            var k = $(this).attr("k");
            if (k == "del") {
                if (confirm(lang.a22 + "[" + groupName + "]？")) {
                    $.post("./index.php?c=Account&a=group_del", { "id": d }, function (result) {
						if (result && result.status == 1) {
							window.location.reload();
						}else{
							alert(result.info);
						}
                    })
                }
            } else {
                showBox("pop11");
                $("#pop11").css("left","38%");
                $(".boxName").html(lang.a56);
                $(".groupName").val(groupName);
                $(".groupIntro").val($(this).parent().prev().find("span").html());
                $(".btn_post").attr({ "k": "update", "d": d });
            }
        })

        $(".btn_post").click(function () {
            var groupName = $(".groupName").val().trim();
            var groupIntro = $(".groupIntro").val();
            if (groupName == "") {
                alert(lang.a57);
                return;
            }
            $.post("./index.php?c=Account&a=group_" + $(this).attr("k"), { "id": $(this).attr("d"), "groupName": groupName, "intro": groupIntro }, function (result) {
                if (result && result.status == 1) {
					window.location.reload();
                }else{
					alert(result.info);
				}
            })
        })
    </script>

	<include file="Public:sidebar"/>
</body>
</html>
