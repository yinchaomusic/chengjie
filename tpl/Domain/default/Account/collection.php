<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>域名收藏列表&nbsp;&gt;&nbsp;</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
         <!-- 左边 -->
        <div class="u-fl">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                       
                            <span>域名收藏列表</span>
                            <b></b>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 数据 -->
            <div class="m-account-data2 s-bg-fc u_be9">
                <table>
                    <tr class="s-bg-white">
                        <th width="16px"></th>
                        <th width="300px" class="u_tl">域名id</th>
                        <th width="140px" class="u_tl">域名</th>
                        <th width="230px" class="u_tr">操作</th>
                        <th width="16px"></th>
                    </tr>
                    <volist name="list" id ="vo" >
                    <tr>
                        <td width="16px"></td>
                        <td width="300px" class="u_tl">{pigcms{$vo.domain_id}</td>
                        <td width="140px" class="u_tl">
                    <if condition="$vo['type'] eq 0">
                        <a target="_blank" href="{pigcms{:U('Buydomains/detail',array('id'=>$vo['domain_id']))}">{pigcms{$vo.domain}</a>
                        <elseif condition="$vo['type'] eq 1"/>
                        <a target="_blank" href="{pigcms{:U('Hotsale/selling',array('id'=>$vo['domain_id']))}">{pigcms{$vo.domain}</a>
                        <elseif condition="$vo['type'] eq 2"/>
                        <a target="_blank" href="{pigcms{:U('Fastbid/detail',array('id'=>$vo['domain_id']))}">{pigcms{$vo.domain}</a>
                        <elseif condition="$vo['type'] eq 3"/>
                        <a target="_blank" href="{pigcms{:U('Bargain/selling',array('id'=>$vo['domain_id']))}">{pigcms{$vo.domain}</a>
                    </if>
                            
                        </td>
                        <td width="230px" class="u_tr"><a onclick="getListID({pigcms{$vo.id})">删除</a></td>
                        <td width="16px"></td>
                    </tr>
                    </volist>
                </table>
            </div>
            <div id="paging" class="g-padding g-padding1">{pigcms{$pagebar}</div>
        </div>
        <!-- 右边 -->

		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<include file="Public:sidebar"/>
        <script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css"/>
        <script>
            function getListID(obj){
                swal({   
                    title: "是否删除?",   
                    text: "是否要删除这条域名收藏记录!",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "是，我要删除",   
                    cancelButtonText: "不，我点错了",
                    closeOnConfirm: false 
                }, 
                function(){   
                    $.post("{pigcms{:U('Account/del_collection')}",{'id':obj},function(data){
                        if(data == 1){
                            swal("成功!", "此条域名收藏已删除.", "success"); 
                            location.href = location.href;
                        }
                    })
                    
                });
                
                
                
            }
    </script>
</body>
</html>
