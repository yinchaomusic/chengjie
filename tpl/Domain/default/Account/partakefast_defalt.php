<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
        <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>我的账户</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
			<!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>我的所有交易</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 选项卡 -->
            <div class="m-acc-select u_be9 s-bg-white">
                <!-- 标题 -->
                <div class="m-acc-title s-bg-fc">
                    <div class="u-fl"><a href="{pigcms{:U('Account/partakefast')}">所有</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/partakefast_bidding')}">正在竞价</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/partakefast_success')}">已得标</a></div>
                    <div class="u-fl"><a class="active" href="{pigcms{:U('Account/partakefast_defalt')}">未得标</a></div>
                    <input type="hidden" value="0" id="checktype">
                    <div class="u-cls"></div>
                </div>
                <!-- 内容 -->
                    <div class="m-acc-cons s-bg-fc u_mall1">
                        <table>
                            <tbody>
                            <tr>
                                <th width="16px"></th>
                                <th width="150px" class="u_tl">域名</th>
                                <th width="120px" class="u_tr">报价金额</th>
                                <th width="20px"></th>
                                <th width="20px"></th>
                                <th width="180px" class="u_tl">报价时间</th>
                                <th width="16px"></th>
                            </tr>
                            <if condition="is_array($list)">
                                <volist id="vo" name="list">
                                    <tr>
                                        <td width="16px"></td>
                                        <td width="150px" class="u_tl">{pigcms{$vo.domain}</td>
                                        <td width="120px" class="u_tr">￥{pigcms{$vo.money|number_format}</td>
                                        <td width="20px"></td>
                                        <td width="20px"></td>
                                        <td width="180px" class="u_tl">{pigcms{$vo.time|date="Y-m-d H:i:s",###}</td>
                                        <td width="16px"></td>
                                    </tr>
                                </volist>
                                <else/>
                                    <tr><td class="u_nb u_tc u_norecord" colspan="10">暂无记录！</td></tr>
                            </if>
                            
                        </tbody></table>
                    </div>
                <!-- 分页 -->
                <div id="paging" class="g-padding g-padding1">{pigcms{$pagebar}</div>
            </div>
        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<include file="Public:sidebar"/>
</body>
</html>
