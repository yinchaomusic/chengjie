<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我是卖家 - 添加批量交易{pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>我是卖家&nbsp;&gt;&nbsp;</li>
					<li>添加批量交易</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
			<!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>添加批量交易</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
                <div class="m-insa s-bg-fc u_be89" style="width:676px;">
                    <ul>
                        <li><span class="u_fw">1.</span>如果有审核未通过域名可能是您未添加该域名的whois邮箱，请添加后重新验证。</li>
                        <li><span class="u_fw">2.</span>如果添加了不属于您的域名请删除本次提交的批量交易重新添加。</li>
                        <li><span class="u_fw">3.</span>请联系经纪人验证域名。</li>
                    </ul>
                </div>
            <!-- 选项卡 -->
            <form method="post" action="{pigcms{:U('Account/addDomainCheck_pl')}" enctype="multipart/form-data">
                <div class="m-addDomains u_be9 s-bg-fc">
<!--                    <p class="f-p26">请输入您的域名或者选择上传Txt或Excel文件。<a href="/demo/mibiao.rar" class="download-demo-btn" target="_blank">点击下载模板</a></p>-->
                    
                    <p class="f-p26"></p>
                    
                    <p class="f-p-t9">域名列表：</p>
                    <p class="f-p27">
                        <textarea class="txta_v" ph="域名一行一个，请输入顶级域名"  name="domain">{pigcms{$str_list}</textarea>
                    </p>
                    <input type="hidden" name="bulk_id" value="{pigcms{$bulks.bulk_id}">
<!--                    <p class="f-p-t10">
                        <span class="u_fw u-span11">上传Txt或Excel文件：</span>
                        <input type="file" class="file" name="fileField" />
                    </p>-->
                        <p class="f-p-t10" style="margin-top: 10px;">
                            <span class="u_fw u-span11">标题：</span>
                            <span class="u-ipt-d">
                                <input type="text" value="{pigcms{$bulks.title}" class="user check-ipt" check-info="标题" name="title" />
                            </span>
                        </p>
                        <p class="f-p-t10" style="margin-top: 10px;">
                            <span class="u_fw u-span11">总价格：</span>
                            <span class="u-ipt-d">
                                <input type="text"  value="{pigcms{$bulks.total_price}"  class="user check-ipt money" check-info="总价格" name="allprice" />
                            </span>
                        </p> 
                        <p class="f-p-t10" style="margin-top: 10px; height: auto;">
                            <span class="u_fw u-span11">含义：</span>
                            <textarea class="nofocus tarea-x" name="intro" id="" cols="30" rows="10">{pigcms{$bulks.meaning}</textarea>
                        </p>   
                        <div class="m-addoper">
                            <div class="u-fl addoper-l">
                            </div>
                            <div class="u-fl addoper-r">
                                <p class="u_mtb30">
                                    <input type="hidden" name="pl" id="pl" value="1" />
                                    <input type="submit" id="sbt-pltj" value="提交" class="u-btn9 btn_tj"/>
                                </p>
                            </div>
                            <div class="u-cls"></div>
                        </div>
                </div>
            </form>
        </div>
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<script type="text/javascript">
        $(function () {
            $(".btn_tj").click(function () {
                var f = $(".file").val();
                var ph = $(".txta_v").attr("ph");
                if (ph)
                    ph = ph.replace(/\r/g, "");
                var d = $(".txta_v").val().replace(/\r/g, "") == ph ? "" : $(".txta_v").val();
                if (f && !/.*\.txt$|.*\.xls|.*\.xlsx$/i.test(f)) {
                    showDialog(lang.a45);
                    return false;
                }
                if (!f && d == "") {
                    showDialog(lang.a46);
                    return false;
                }
            })
            $('#sbt-pltj').click(function (e) {
                return $('.check-ipt').each(function () {
                    var _iptText, _ref;
                    if ($.trim($(this).val()) === '') {
                        e.preventDefault();
                        _iptText = (_ref = $(this).attr('check-info')) != null ? _ref : '';
                        showDialog(_iptText + '不能为空');
                        return false;
                    }
                });
            });
        })
    </script>
	<include file="Public:sidebar"/>
</body>
</html>
