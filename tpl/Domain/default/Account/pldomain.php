<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我是卖家 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>我是卖家&nbsp;&gt;&nbsp;</li>
					<li>我的批量交易</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
			<!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width:702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>批量交易</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 选项卡 -->
            <div class="m-acc-select u_be9 s-bg-white">
                <!-- 标题 -->
                <div class="m-acc-title s-bg-fc">
                    <div class="u-fl"><a href="{pigcms{:U('Account/pldomain')}" <if condition="$_GET['status'] eq 0">class="active"</if>>全部</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/pldomain',array('status'=>'1'))}" <if condition="$_GET['status'] eq 1">class="active"</if>>待验证/修改</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/pldomain',array('status'=>'2'))}" <if condition="$_GET['status'] eq 2">class="active"</if>>待展示/修改</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/pldomain',array('status'=>'3'))}" <if condition="$_GET['status'] eq 3">class="active"</if>>出售中</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Account/pldomain',array('status'=>'4'))}" <if condition="$_GET['status'] eq 4">class="active"</if>>已出售</a></div>
                    <input type="hidden" value="{pigcms{$_GET.status|intval=###}" id="checktype" />
                    <div class="u-cls"></div>
                </div>
                <!-- 内容 -->
                <div class="m-acc-cons s-bg-fc u_mall1">
                    <table>
                        <tr>
                            <th width="16px"></th>
                            <th width="22px"></th>
                            <th width="110px" class="u_tl">标题</th>
                            <th width="120px" class="u_tr">价格</th>
                            <th width="25px"></th>
                            <th width="127px" class="u_tl">域名个数</th>
                            <th width="25px"></th>
                            <th width="95px" class="u_tl">提交时间</th>
                                <th width="120px;" class="u_tr">状态</th>   
                            <th width="16px"></th>
                        </tr>
                    </table>
                </div>
                <!-- 分页 -->
                <div id="paging" class="g-padding g-padding1">
                </div>
            </div>
        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<script type="text/javascript">
        $(function () {
            var type = $("#checktype").val();
//            alert(type);
            getDataList_pldomain(1, type);

            $("#paging a").live("click", function () {
                if (!$(this).attr("k"))
                    return;
                var pageindex = $(this).attr("k");
                getDataList_pldomain(pageindex, type);
            });

            $(".sold_out").live("click", function () {
                if (confirm(lang.a22 + "[" + $(this).parent().parent().children().eq(2).text() + "]？")) {
                    $.post("./index.php?c=Account&a=pldomainOperation", { "id": $(this).parent().parent().find("input").attr("value"),"type": 2 },function(result){
                        if (result.status == 1)
                            location.href = location.href;
                        else
                            showDialog(lang.a23);
                    })
                }
            })

            $(".btn_verify_all").click(function () {
                $.post("/account/CheckDomain", function (data, status) {
                    if (status == "success") {
                        showDialog(lang.a60);
                    }
                })
            })


            $(document.body).click(function () {
                $(".m-group-result,.m-prise-result").css("display", "none");
                $("#group,#prise").css("background-color", "#fcfcfc");
            });
        })
    </script>
	<include file="Public:sidebar"/>
</body>
</html>
