<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />


	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />



	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>
<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>我的账户</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="g-content u-cls">
	<div class="m-content">
		<include file="Account:sidebar"/>
		<!-- 右边 -->
		<div class="u-fr">
			<!-- 标题&验证 -->
			<div class="s-bg-fc u_be9" style="width: 702px;">
				<div class="m-ta s-bg-white">
					<div class="title">
						<a class="s-3c" href="javascript:void(0)">
							<span>冻结明细</span>
						</a>
					</div>
					<div class="u-cls"></div>
				</div>
			</div>
			<!--搜索-->
			<div class="m-insa s-bg-fc u_be89">
				<ul>
					<li><span class="u_fw">1.</span>下面列出的是正处于冻结状态的资金记录。</li>
				</ul>
			</div>

			<!-- 数据 -->
			<div class="m-account-data2 s-bg-fc u_be9">
				<table>
					<tr class="s-bg-white">

						<th width="150px" style="padding-left: 15px;" class="u_tl">冻结时间</th>
						<th width="100px" class="u_tl">冻结金额	</th>
						<th width="120px" class="u_tl">说明	</th>

					</tr>

					<if condition="is_array($user_freeze_list)">
						<volist name="user_freeze_list" id="vo">
							<tr>

								<td style="padding-left: 15px;">{pigcms{$vo.freezetime|date='Y-m-d H:i:s',###}</td>
								<td><font color="red">-￥{pigcms{$vo.freezemoney|number_format}</font></td>
								<td>{pigcms{$vo.info}</td>

							</tr>
						</volist>
						<tr><td class="textcenter pagebar" colspan="11">{pigcms{$pagebar}</td></tr>
						<else/>
						<tr><td class="textcenter red" colspan="11">列表为空！</td></tr>
					</if>

				</table>
			</div>
			<!-- fenye -->
			<div id="paging" class="g-padding g-padding2"></div>
		</div>

		<div class="u-cls"></div>
	</div>
</div>
<include file="Public:footer"/>

<script src="{pigcms{$static_path}js/account.js"></script>

<include file="Public:sidebar"/>

</body>
</html>
