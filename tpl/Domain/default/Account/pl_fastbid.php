<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我是卖家 - 申请竞价批量设置</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>我是卖家&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/allsell')}" class="s-2a">所有域名管理</a>&nbsp;&gt;&nbsp;</li>
					<li>申请竞价批量设置</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
		<div class="m-content">
			<div class="m-title-a u_be9 s-bg-fc u_mb6">
				<a href="javascript:void(0);">申请竞价批量设置</a>
			</div>
			<form action="{pigcms{:U('Account/pl_fastbid_operation')}" method="post" id="form">
				<input type="hidden" name="type" value="8">
				<div class="m-batch-data s-bg-fc" id="batch-data">
					<table>
						<tr class="s-bg-white u_bb0">
							<th width="16px"></th>
							<th></th>
							<th width="300px" class="u_tl">域名</th>
							<th width="230px" class="u_tl">保留价</th>
							<th width="338px" class="u_tl">简介</th>
							<th width="16px"></th>
						</tr>
						<tr class="u_bb0">
							<td></td>
							<td></td>
							<td><span class="u_fw">批量设置</span></td>
							<td>
								<div class="m-batch-setmoney">
									<div class="m-batch-ipt s-bg-white" style="border: 1px solid rgb(235, 235, 235);">
										<input type="text" class="s-75b money" name="pl_money">
									</div>
									<div class="batch-money">元</div>
									<div class="u-cls"></div>
								</div>
							</td>
							<td>
								<div class="m-batch-ipt2 s-bg-white" style="border: 1px solid rgb(235, 235, 235);">
									<input type="text" name="pl_desc"/>
								</div>
							</td>
							<td></td>
						</tr>
						<volist name="domainList" id="vo">
							<tr>
								<td><input type="hidden" name="id" value="{pigcms{$vo.domain_id}"/></td>
								<td><input type="button" class="ipt_remove" value="移除"/></td>
								<td>{pigcms{$vo.domain}</td>
								<td>
									<div class="m-batch-setmoney">
										<div class="m-batch-ipt s-bg-white" style="border: 1px solid rgb(235, 235, 235);">
											<input type="text" value="{pigcms{$vo.mark_money|floatval=###}" class="s-75b money" name="money"/>
										</div>
										<div class="batch-money">元</div>
										<div class="u-cls"></div>
									</div>
								</td>
								<td>
									<div class="m-batch-ipt2 s-bg-white" style="border: 1px solid rgb(235, 235, 235);">
										<input type="text" value="{pigcms{$vo.desc}" name="desc"/>
									</div>
								</td>
								<td></td>
							</tr>
						</volist>
					</table>
				</div>
				<div class="m-batch-tj s-bg-fc">
					<input type="button" class="u-btn16 s-bg-2a" onclick="javascript: history.go(-1)" style="margin-right:50px;" value="返回">
					<input type="submit" value="提交" class="u-btn16 s-bg-2a">
				</div>
			</form>

		</div>
	</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<include file="Public:sidebar"/>
</body>
</html>
