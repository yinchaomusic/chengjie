<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>我的账户</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
			<!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>修改资料</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <div class="m-insf s-bg-fc">
                <p>请确保您的联系方式真实有效，如果您的交易或帐户出现什么问题，我们将通过您提供的联系方式联系您！</p>
            </div>
            <!--您绑定的邮箱  -->
            <!-- 数据 -->
            <form action="{pigcms{:U('Account/updateprofile')}" method='post'>
                <div class="m-question m-question1 s-bg-fc u_be9">
                    <ul>
                        <li class="u_mb15">
                            <div class="question_txt">您的会员ID：</div>
                            <div class="question_ipt s-bg-f5">
                                <input type="text" name="uid" value="{pigcms{$user_data.uid}" readonly="readonly">
                            </div>
                            <div class="u-cls"></div>
                        </li>
                        <li class="u_mb15">
                            <div class="question_txt">电子邮件：</div>
                            <div class="question_ipt s-bg-f5">
                                <input type="text" name="email" value="{pigcms{$user_data.email}"  readonly="readonly"/>
                            </div>
                            <div class="u-cls"></div>
                        </li>
                        <li class="u_mb15">
                            <div class="question_txt">真实姓名：</div>
                            <div class="question_ipt s-bg-f5">
                                <input type="text" name="nickname" value="{pigcms{$user_data.nickname}" readonly="readonly">
                            </div>
                            <div class="u-cls"></div>
                        </li>
                        <li class="u_mb15">
                            <div class="question_txt">手机：</div>
                            <div class="question_ipt s-bg-f5">
                                <input type="text" name='phone' value="{pigcms{$user_data.phone}"  readonly=&#39;readonly&#39;>
                            </div>
                            <div class="u-cls"></div>
                        </li>
                        <li class="u_mb15">
                            <div class="question_txt">微信号：</div>
                            <div class="question_ipt s-bg-f5">
     <input type="text" name='weixin' value="{pigcms{$user_data.weixin}" <if condition="$user_data['weixin'] neq ''">readonly="readonly" </if> >
                            </div> <span>一经填写不可修改</span>
                            <div class="u-cls"></div>
                        </li>
                        <li class="u_mb15">
                            <div class="question_txt">YY号：</div>
                            <div class="question_ipt s-bg-f5">
      <input type="text" name='yy' value="{pigcms{$user_data.yy}"  <if condition="$user_data['yy'] neq ''">readonly="readonly" </if>>
                            </div><span>一经填写不可修改</span>
                            <div class="u-cls"></div>
                        </li>
                        <li class="u_mb15">
                            <div class="question_txt">QQ：</div>
                            <div class="question_ipt s-bg-white">
                                <input type="text" name="qq" value="{pigcms{$user_data.qq}" name='txtQQ' readonly="readonly">
                            </div>
                            <div class="u-cls"></div>
                        </li>
                        <li>
                            <div class="u_mall12">
                                <input type="submit" value="保存修改" class="u-btn12 s-bg-2a">
                            </div>
                        </li>
                    </ul>
                </div>
            </form>
        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<script src="{pigcms{$static_path}js/account.js"></script>
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}
	</script>
	<include file="Public:sidebar"/>
</body>
</html>
