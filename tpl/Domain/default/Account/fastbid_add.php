<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我是卖家 - 我提交的竞价</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>我是卖家&nbsp;&gt;&nbsp;</li>
					<li>极速竞价管理</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
		<div class="m-content">
			<include file="Account:sidebar"/>
			<!-- 右边 -->
			<div class="u-fr">
				<!-- 标题&验证 -->
				<div class="s-bg-fc u_be9" style="width: 702px;">
					<div class="m-ta s-bg-white">
						<div class="title">
							<a class="s-3c" href="javascript:void(0)">
								<span>我提交的竞价</span>
							</a>
						</div>
						<div class="u-cls"></div>
					</div>
				</div>
				<!-- 选项卡 -->
				<div class="m-acc-select u_be9 s-bg-white">
					<!-- 标题 -->
					<div class="m-acc-title s-bg-fc">
						<div class="u-fl"><a href="{pigcms{:U('Account/fastbid_add')}" class="active">申请竞价</a></div>
						<div class="u-fl"><a href="{pigcms{:U('Account/fastbid')}">审核</a></div>
						<div class="u-fl"><a href="/account/fastbid/bidding">正在竞价</a></div>
						<div class="u-fl"><a href="/account/fastbid/ready">即将开始</a></div>
						<div class="u-fl"><a href="/account/fastbid/end">竞价结束</a></div>
						<input type="hidden" value="7" id="checktype">
						<div class="u-cls"></div>
					</div>
					<!-- 内容 -->
					<div class="m-acc-cons s-bg-fc u_mall1">
						<table>
							<tr>
								<th width="16px"></th>
								<th width="22px" class="u_tl"></th>
								<th width="94px" class="u_tl">域名</th>
								<th width="125px" class="u_tl">
									<div class="m-group-domains">
										<p class="f-p-t11">
											<span class="u-slt-group" id="group" value="">域名分组</span>
											<a href="{pigcms{:U('Account/group')}" target="_blank" title="新建分组" class="u-abtn10"></a>
										</p>
										<div class="m-group-result s-bg-white">
											<p value='' title='域名分组'>域名分组</p>
											<p value='0' title='无分组'>无分组</p>
											<volist name="group_list" id="vo">
												<p value='{pigcms{$vo.group_id}' title='{pigcms{$vo.group_name}'>{pigcms{$vo.group_name}</p>
											</volist>
										</div>
									</div>
								</th>
								<th width="20px"></th>
								<th width="90px">
									<div class="m-group-price">
										<p class="f-p-t11">
											<span class="u-slt-group" id="prise" value="0">标价</span>
										</p>
										<div class="m-prise-result s-bg-white">
											<p value="0">标价</p>
											<p value="2">无标价</p>
											<p value="1">有标价</p>
										</div>
									</div>
								</th>
								<th width="20px"></th>
								<th width="130px" class="u_tl">简介</th>
								<th width="140px;" class="u_tr">操作</th>
								<th width="16px"></th>
							</tr>
						</table>
					</div>   
					<!-- 分页 -->
					<div id="paging" class="g-padding g-padding1"></div>   
					<div class="m-operation4">
						<div class="u-fl operation4-l">
							<label>
								<input type="checkbox" class='curPage' />
								<span>选中当前页</span>
							</label>
							<!--label>
								<input type="checkbox" class='allPage' />
								<span>选中所有</span>
							</label-->
						</div>
						<div class="u-fl operation3-r">
							<span>操作已选域名：</span>
							<input type="button" value="申请竞价" class="u-btn10 btn_delete btn_pl" k="{pigcms{:U('Account/pl_fastbid')}" />
						</div>
						<div class="u-cls"></div>
					</div>   
				</div>

			</div>
			
		   <div class="u-cls"></div>
		</div>
	</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<script type="text/javascript">
        $(function () {
            var type = $("#checktype").val();
            if (type == "7")
                getDataList_all(1, type);
            else
                getDataList_bid(1, type);

            $("#paging a").live("click", function () {
                if (!$(this).attr("k"))
                    return;
                var pageindex = $(this).attr("k");
                if (type == "7")
                    getDataList_all(pageindex, type);
                else
                    getDataList_bid(pageindex, type);
            });

            $(".cancel").click(function () {
                if (confirm(lang.a24 + "？")) {
                    $.post("/account/fastbidOperation", { "type": 0, "id": $(this).attr("id") }, function (data, status) {
                        if (status == "success")
                            location.href = location.href;
                    })
                }
            });

            // 竞价时间
            if ($("#time_jj").length > 0) {
                setInterval(function () {
                    var r = $("#time_jj").html().match(/(\d+?):(\d+?):(\d+)/)
                    var h = Number(r[1]);
                    var m = Number(r[2]);
                    var s = Number(r[3]);
                    if (s == 0) {
                        if (h == 0 && m == 0) {
                            return;
                        }
                        s = 59;
                        if (m == 0) {
                            m = 59;
                            h -= 1;
                        }
                        else
                            m -= 1;

                    } else {
                        s -= 1;
                    }
                    $("#time_jj").html(bl(h) + ":" + bl(m) + ":" + bl(s));
                }, 1000)
            }

            $(document.body).click(function () {
                $(".m-group-result,.m-prise-result").css("display", "none");
                $("#group,#prise").css("background-color", "#fcfcfc");
            });
        })
    </script>
	<include file="Public:sidebar"/>
</body>
</html>
