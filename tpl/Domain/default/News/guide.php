<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>{pigcms{$config.seo_title}</title>
	<meta name="keywords" content="{pigcms{$config.seo_keywords}" />
	<meta name="description" content="{pigcms{$config.seo_description}" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="{pigcms{$config.site_url}/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="{pigcms{$config.site_url}/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/common.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/help.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>



<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="/" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>{pigcms{$cate_guide_left.cat_name}  </li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>

		<div class="u-cls"></div>
	</div>
</div>


<div class="content">
	<div class="help_ctt_l">
		<div class="help_content">
			<p class="abt62">{pigcms{$get_guide_list.news_title}</p>
			<div class="h_c_a">
				<div class="he_content bor_t_b">
					{pigcms{$get_guide_list.news_content}
				</div>

			</div>
		</div>
	</div>

</div>

<include file="Public:news_left"/>

<include file="Public:footer"/>

<div class="s-bg-f7">
</div>
<!---->

<script type="text/javascript" src="{pigcms{$static_path}js/help.js"></script>


</body>
</html>