<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>{pigcms{$config.seo_title}</title>
	<meta name="keywords" content="{pigcms{$config.seo_keywords}" />
	<meta name="description" content="{pigcms{$config.seo_description}" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="{pigcms{$config.site_url}/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="{pigcms{$config.site_url}/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/common.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/help.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>



<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="/" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>{pigcms{$aoubt_list.title}</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>

		<div class="u-cls"></div>
	</div>
</div>


<div class="content">
	<div class="help_ctt_l">
		<div class="help_content">
			<p class="abt62">{pigcms{$aoubt_list.title}</p>
			<div class="h_c_a">
				<div class="he_content bor_t_b">
					{pigcms{$aoubt_list.content}
				</div>

			</div>
		</div>
	</div>

</div>

<include file="Public:news_left"/>

<include file="Public:footer"/>

<div class="s-bg-f7">
</div>
<!---->

<script type="text/javascript" src="{pigcms{$static_path}js/help.js"></script>
<script type="text/javascript">
	$("#processList h3").next().slideUp();
	n = "yijia";
	var sld_a = $("#processList h3[data-url$='" + n + "']");
	if (sld_a.length == 0) {
		$(".process_list h3:first").addClass("cli");
		$(".process_list h3:first").next().slideDown();
	} else {
		$(sld_a).addClass("cli");
		$(sld_a).next().slideDown();
	}
	$("#processList h3").click(function () {
		$("#processList h3").next().slideUp();
		$("#processList h3").removeClass("cli");
		var ul = $(this).next();
		console.log(ul.css("display"));
		if (ul.css("display") == "none") {
			$(this).addClass("cli");
			ul.slideDown("fast");
		} else {
			$(this).removeClass("cli");
			ul.slideUp("fast");
		}
	});
	//对应变颜色
	$(".u_num").each(function (i, n) {
		$(n).mouseover(function () {
			$(".m-insa li").eq(i).css("color", "#2aa3ce");
		}).mouseout(function () {
			$(".m-insa li").eq(i).css("color", "#656567");
		});
	});
	//弹出
	$(".u-abtn1").click(function () {
		showBox("pop10");
		$("#pop10").css("left", "29%");
	});
	$(".close").click(function () {
		closeBox("pop10");
	});
</script>

</body>
</html>