<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>{pigcms{$config.seo_title}</title>
	<meta name="keywords" content="{pigcms{$config.seo_keywords}" />
	<meta name="description" content="{pigcms{$config.seo_description}" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="{pigcms{$config.site_url}/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="{pigcms{$config.site_url}/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/common.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/help.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>


<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="/" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>{pigcms{$cate_list['cat_name']}</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>

		<div class="u-cls"></div>
	</div>
</div>
<div class="content">   
    
    <div class="c-a">
        <div class="help_content">
            <p class="abt62"><span>{pigcms{$cate_list['cat_name']}</span></p>
            <div class="h_c_a">
                <div class="he_content">
                    <ul class="ul1">
                        <volist name="news_list" id="vo">
	                        <li class='cls'><span class='c_c5'>■
			                    <a target='_blank' href="{pigcms{:U('News/show',array('news_id'=>$vo['news_id']))}">{pigcms{$vo
	                                    .news_title}</a>
                                    </span>
		                        <span class='fr'>{pigcms{$vo.add_time|date='Y-m-d H:i:s',###}</span>
	                        </li>

                        </volist>
                    </ul>
                </div>
                <!-- 分页 -->
                <div id="paging" class="g-padding" style="width:670px;"></div>
                <div class="u-cls"></div>
            </div>
        </div>
    </div>
    
    
    
<include file="Public:news_left"/>
    <div class="u-cls"></div>

</div>



<include file="Public:footer"/>

<div class="s-bg-f7">
</div>


<script type="text/javascript" src="{pigcms{$static_path}js/help.js"></script>


</body>
</html>