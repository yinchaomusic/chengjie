<!DOCTYPE html>
<html>
<head>
	<title>卖域名 - 域名出售 - 域名价值 - {pigcms{$now_site_short_url}{pigcms{$now_site_name} </title>
	<meta name="keywords" content="卖域名,域名出售,域名价值,域名交易" />
	<meta name="description" content"{pigcsm{$config.site_name}为您提供优质的域名出售服务，卖优质域名或者寻找域名出售担保交易就来{pigcsm{$config.site_name}，优质域名一网打尽！" />

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<link rel="icon" href="{pigcms{$static_path}images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="{pigcms{$static_path}images/favicon.ico" type="image/x-icon" />
	<meta charset="utf-8">
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />

	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
    <include file="Public:sidebar"/>
    <include file="Public:header"/>
    <include file="Public:nav"/>


<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{:U('Index/index')}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>经纪人</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>

		<div class="u-cls"></div>
	</div>
</div>
<div class="content">   
    
    <div class="c-a">
        <div class="help_content">
            <p class="abt62"><span>经纪人列表</span></p>
            <div class="h_c_a">
                <ul class="ul2">
                    <volist name="staff_list" id="vo">
                        <li class='mr0'>
                            <div class='bro_t'>
                                <p style="text-indent:0px;"><span>域名经纪人：</span><span class='c-18'>{pigcms{$vo.name}</span></p>
                                <p style="text-indent:0px;"><span>联系电话：</span><span>{pigcms{$vo.phone}</span></p>
                                <p style="text-indent:0px;"><span>Email：</span><span>{pigcms{$vo.email}</span></p>
                            </div>
                            <div class='bro_b' style=''>
                                <a href='tencent://message/?uin={pigcms{$vo.qq}&amp;Site={pigcms{$config.site_name}'>
                                    <img border='0' src='http://wpa.qq.com/pa?p=2:{pigcms{$vo.qq}:51' alt='QQ交谈' title='QQ交谈'>
                                </a><span>QQ: {pigcms{$vo.qq}</span>
                            </div>
                        </li>
                        </volist>
                </ul>
                <div class="u-cls"></div>
            </div>
        </div>
    </div>
    
    
    <include file="Public:news_left"/>
    


</div>









<include file="Public:footer"/>
<div class="s-bg-f7">
</div>
<script type="text/javascript" src="./tpl/Domain/default/static/js/help.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/plugs_popupBox.js"></script>
<script type="text/javascript">
	$("#processList h3").next().slideUp();
	n = "yijia";
	var sld_a = $("#processList h3[data-url$='" + n + "']");
	if (sld_a.length == 0) {
		$(".process_list h3:first").addClass("cli");
		$(".process_list h3:first").next().slideDown();
	} else {
		$(sld_a).addClass("cli");
		$(sld_a).next().slideDown();
	}
	$("#processList h3").click(function () {
		$("#processList h3").next().slideUp();
		$("#processList h3").removeClass("cli");
		var ul = $(this).next();
		console.log(ul.css("display"));
		if (ul.css("display") == "none") {
			$(this).addClass("cli");
			ul.slideDown("fast");
		} else {
			$(this).removeClass("cli");
			ul.slideUp("fast");
		}
	});
	//对应变颜色
	$(".u_num").each(function (i, n) {
		$(n).mouseover(function () {
			$(".m-insa li").eq(i).css("color", "#2aa3ce");
		}).mouseout(function () {
			$(".m-insa li").eq(i).css("color", "#656567");
		});
	});
	//弹出
	$(".u-abtn1").click(function () {
		showBox("pop10");
		$("#pop10").css("left", "29%");
	});
	$(".close").click(function () {
		closeBox("pop10");
	});
</script>

</body>
</html>