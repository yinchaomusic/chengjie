<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>{pigcms{$config.seo_title}</title>
	<meta name="keywords" content="{pigcms{$config.seo_keywords}" />
	<meta name="description" content="{pigcms{$config.seo_description}" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="{pigcms{$config.site_url}/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="{pigcms{$config.site_url}/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/common.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/help.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>



<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="/" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li><a  class="s-2a" href="{pigcms{:U('News/index',array('type'=>$cate['cat_key']))}">{pigcms{$cate.cat_name}</a> &nbsp;&gt;&nbsp;{pigcms{$news_show.news_title}</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>

		<div class="u-cls"></div>
	</div>
</div>


<div class="content">    
    <div class="c-a">
        <div class="help_content news_left">
            <div class="news_a">
                <p class="p_title">{pigcms{$news_show.news_title}</p>
                <div class="new_source">
	                <if condition="$nocontents neq 1">
                    {pigcms{$news_show.add_time|date='Y-m-d H:i:s',###} &nbsp;&nbsp;&nbsp;&nbsp;
		                </if>


                </div>
                <div class="news_a_fi"></div>
                <div class="newsInner_content">
                    <p style="text-align:center;font-family:'Helvetica Neue', Helvetica, 'Hiragino Sans GB', 'Microsoft YaHei', Arial, sans-serif;font-size:16px;text-indent:2em;">
	                    <if condition="$news_show['pic'] neq ''">
                    <span style="line-height:1.6;"><img src="./upload/news/{pigcms{$news_show.pic}" alt="" /><br /></span>
	                    </if>
					</p>
						{pigcms{$news_show.news_content}
	                <if condition="$nocontents neq 1">
	                <p class="reprint c-e pb15">转载请注明出处:   {pigcms{$config.site_url}{pigcms{:U('News/show',array('news_id'=>$news_show['news_id']))}</p>
		            <else/>
		                ——……—— 没有更多的内容了  ——……——
	                </if>
                </div>
                <div class="up_next">
                    <p class='up'>
                        <if condition="$pre eq 1">
	                     <else/>
	                        <span>上一篇&nbsp;&nbsp;</span><a class='c-e' href="{pigcms{:U('News/show',array('news_id'=>$news_pre_show['news_id']))}">{pigcms{$news_pre_show.news_title}</a>
                        </if>
                        
                    </p><p class='next'>
                        <if condition="$later neq 1">
                            <span>下一篇&nbsp;&nbsp;</span><a class='c-e' href="{pigcms{:U('News/show',array('news_id'=>$news_later_show['news_id']))}">{pigcms{$news_later_show.news_title}</a>
                            <else/>

                        </if>
                    </p> </div>
            </div>
        </div>
    </div>
    
        <div class="help_menu news_right" style="background-color:#f0f1f1;">
<div class='infor '>
                <p class='abt62'><span class='w95'>域名资讯</span></p>
                <div class='infor_words'>
                    <ul class='ul4'>
	                    <volist name="get_news_domains_info" id="vo">
	                 <li><a href="{pigcms{:U('News/show',array('news_id'=>$vo['news_id']))}" target='_blank' title="{pigcms{$vo
	                 .news_title}">{pigcms{$vo
			                 .news_title}</a></li>
	                    </volist>

                    </ul></div>
</div>



        </div>
        <div style="clear:both;"></div>
</div>



<include file="Public:footer"/>

<div class="s-bg-f7">
</div>
<!---->

<script type="text/javascript" src="{pigcms{$static_path}js/help.js"></script>


</body>
</html>