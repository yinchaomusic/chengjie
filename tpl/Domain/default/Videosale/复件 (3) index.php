<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title>尊拍域名视频拍卖-{pigcms{$config.site_name}</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
		<meta http-equiv="Pragma" content="no-cache"/>
		<meta http-equiv="Expires" content="0"/>
		<link rel="stylesheet" href="{pigcms{$static_path}videosale/css/global.css?t=120" type="text/css"/>
		<style>
			.ms-controller {
			  visibility: hidden;
			}
		</style>
		<link href="{pigcms{$static_path}videosale/css/auction.css?t=120" rel="stylesheet" type="text/css"/>
	</head>
	<body style="overflow:hidden;">
		<div class="s-bg-white">
		<!--	<pigcms:adver cat_key="video_top_banner" limit="1" var_name="video_top_banner">
				<a href="{pigcms{$vo.url}" target="_blank">
				<img src="{pigcms{$vo.pic}"/>
				</a>
			</pigcms:adver>  -->
			<div class="g-header">
				<div class="m-logo u-fl">
					<a href="{pigcms{$config.site_url}" target="_blank">
						<img src="http://www.chengjie.com/static/zunpailogo.gif" alt="尊拍拍卖">
						<h1 style="position: absolute;top: -50px">域名交易</h1>
					</a>
				</div>
				<div class="m-search u-fl">
						<span class="u-ipt-search">
							<span class="u-slt-a" ym="a">淘域名</span>
							<input type="text" ph="请输入域名关键字" value="" class="u-ipt-txta"/>
						</span>
					<input type="button" value="搜索" class="u-btn-search s-bg-6c">
					<div class="f-a-rlt" style="display: none;">
						<p ym="a">淘域名</p>
						<p ym="b">查whois</p>
					</div>
					<div class="f-search-rlt">
					</div>
				</div>
				<div class="m-contact u-fr" <if condition="!$user_session">style="padding-top:26px;"</if>>
					<if condition="!$user_session">
						<a class="login btn" href="http://www.chengjie.com/index.php?c=Login&a=index">登录 / 注册</a>
					<else/>
						<a class="logout btn" href="{pigcms{:U('Domain/Login/logout')}">注销</a>
						<a class="user-info btn" href="{pigcms{:U('Account/index')}" target="new" style="width: 150px;">
							<img alt="" class="user-header" src="{pigcms{$config.site_url}/upload/User_level/{pigcms{$user_level_info.img}"/>
							<div class="user-account" style="width: 100px;">
								<div>ID：<span>{pigcms{$user_session.uid}</span> </div>
								<div><span>{pigcms{$user_session.nickname}</span> </div>
							</div>
							<div class="clr"></div>
						</a>
					</if>
				</div>
			</div>
		</div>
		<div class="layout-wrap">
			<div class="layout">
              	<div>
					<div class="col member-wrap">
						<div class="member-list">
							<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto;">
								<div id="memberDom" class="slimscroll" style="overflow-y:auto; width: auto;"></div>
							</div>
						</div>
					</div>	        
				</div>
              	<div>
					<div class="col auction-wrap">
						<div class="col-title icon_clock">
						出价记录
						<a class="sound_switch" title="出价声音提示开关"></a>
						</div>
						<div class="auction-list" style="height: 705px; position: relative;">
							<div class="loading" style="display: none;"></div>
							<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto;">
								<div class="slimscroll" id="bid_container" style="overflow-y:auto;width: auto;"></div>
							</div>
						</div>
						<!--div class="bottom-wrap  bid-price-wrap">
							<div class="bid-price" <if condition="!empty($_SESSION['system'])">style="display:none;"</if>>
								<a class="btn-subtract disable">-</a>
								<input type="text" class="price-input" id="bidding" maxlength="6"/>
								<div class="fl w5 price" id="showPrice">&nbsp;</div>
								<a class="btn-add">+</a>
								<a class="button button-blue w25 fr " style="*margin-top:-15px" id="sendPrice">出价</a>
								<div class="clr"></div>
							</div>
							<if condition="!empty($_SESSION['system'])">
								<div class="bid-price">
									<a class="button button-blue w25 fr " style="*margin-top:-15px;margin-right:60px;" id="finishVideoSale">结束此局</a>
									<a class="button button-blue w25 fr " style="*margin-top:-15px;margin-right:60px;" id="lockScreen" title="锁住用户的屏幕禁止用户操作进行交易保护，防止按下结束按钮时用户进行提交价格，建议三秒钟">锁屏保护</a>	
									<div class="clr"></div>
								</div>
							</if>
						</div-->
					</div>	      
				</div>
				<div>
					<div class="col video-wrap">
						<div class="col-title icon_flag">{pigcms{$now_term.term_name}</div>
						<div class="video-box">
							<iframe src="{pigcms{:U('Videosale/framevideo')}" width="100%" height="100%" id="video" frameborder="0" scrolling="no"></iframe>
							<a class="refresh-btn" onclick="document.getElementById('video').contentWindow.location.reload();">视频直播有延迟。如无法播放，请尝试点击刷新</a>
						</div>
						<div class="tab-wrap">
							<div class="tab boder-right" style="color: rgb(255, 102, 0);">拍卖域名信息</div>
							<div class="tab" style="color: rgb(102, 102, 102);">待拍域名列表</div>
							<div class="clr"></div>
						</div>
						<div class="tab-content" style="height: 393px;">
							<div class="domain-info-wrap" style="display: block;">
								<div style="height: 100%;">
									<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
										<div class="slimscroll" style="overflow: hidden; width: auto; height: 100%;">
											<div class="pd20 domain-info">
												<div class="current-domain">当前拍卖域名：</div>
												<div class="domain-description current-domain-description"></div>
												<div class="domain-info-item" style="margin-right:-3%">
													<div class="box2">
														<div class="value gray06" id="qipaijia">无</div>
														<div class="key">起拍价</div>
													</div>
													<div class="box2">
														<div class="value green04" id="jiajiafudu">无</div>
														<div class="key">加价幅度</div>
													</div>
													<div class="clr"></div>
												</div>
												<div class="domain-info-item" style="margin-right:-3%">
													<div class="box2" style="background-color:#ecf9ff;">
														<div class="value green04" id="zuigaojia">无</div>
														<div class="key">当前最高出价</div>
													</div>
													<div class="box2">
														<div class="value red06" id="myzuigaojia">无</div>
														<div class="key">我的当前出价</div>
													</div>
													<div class="clr"></div>
												</div>
												<div class="bottom-wrap  bid-price-wrap">
													<div class="bid-price" <if condition="!empty($_SESSION['system'])">style="display:none;"</if>>
														<a class="btn-subtract disable">-</a>
														<input type="text" class="price-input" id="bidding" maxlength="8"/>
														<div class="fl w5 price" id="showPrice">&nbsp;</div>
														<a class="btn-add">+</a>
														<a class="button button-blue w25 fr " style="*margin-top:-15px" id="sendPrice">出价</a>
														<div class="clr"></div>
													</div>
													<if condition="!empty($_SESSION['system'])">
														<div class="bid-price">
															<a class="button button-blue w25 fr " style="*margin-top:-15px;margin-right:15%;" id="finishVideoSale">结束此局</a>
															<a class="button button-blue w25 fr " style="*margin-top:-15px;margin-right:20%;" id="lockScreen" title="锁住用户的屏幕禁止用户操作进行交易保护，防止按下结束按钮时用户进行提交价格，建议三秒钟">锁屏保护</a>	
															<div class="clr"></div>
														</div>
													</if>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="domain-list-wrap" style="display: none;">
								<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
									<div class="slimscroll" id="milist_container" style="overflow-y:auto; width: auto; height: 100%;">
										<div class="pd20 domain-list"></div>
									</div>
								</div>
							</div>
						</div>
					</div>	
				</div>
              	<div>
					<div class="col chat-wrap">
						<div class="col-title icon_notification">公告
							<div id="miniScreenAccount" class="u-fr" style="margin-right:15px;display:none;">
								<if condition="!$user_session">
									<a class="login btn" href="http://www.chengjie.com/index.php?c=Login&a=index" style="font-size:12px;color:red;font-weight:normal;">登录 / 注册</a>
								<else/>
									<a class="login btn" href="{pigcms{:U('Account/index')}" style="font-size:12px;font-weight:normal;" target="_blank">{pigcms{$user_session.nickname}</a>&nbsp;&nbsp;
									<a class="login btn" href="{pigcms{:U('Login/logout')}" style="font-size:12px;color:red;font-weight:normal;">注销</a>
								</if>
							</div>
						</div>
						<div class="notification-wrap">
							<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 120px;">
								<div class="slimscroll" style="overflow-y:auto; width: auto; height: 120px;">
									<div class="pd15">{pigcms{$now_term.term_notice|nl2br=###}</div>
								</div>
							</div>
						</div>
						<div class="col-title icon_chat">聊天</div>
						<!--聊天内容-->
						<div class="chat-list" style="position: relative;">
							<div class="loading" style="display: none;"></div>
							<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto;">
								<div class="slimscroll chat-list-wrap" style="overflow-y:auto; width: auto; height: 545px;">
									<div class="empty"></div>
								</div>
							</div>
						</div>
						<!--聊天内容-->
						<style type="text/css">
							.bottom-wrap{border-top:1px solid #f1ebdf;padding:15px 10px 0 10px;min-height:65px;position:relative;zoom:1;background-color:#f5f1e9}
							.input{border:1px solid #999;box-shadow:inset 1px 1px 1px 1px #eee;height:48px;border-radius:3px;background:#fff;padding:0;text-indent:20px;font-size:15px;width:100%;display:block;outline:0;transition:border 1s}
							.chat-input{float:left;width:60%;white-space:nowrap;overflow-y:auto;line-height:50px;word-wrap:normal;color:#000}
							.chat-input img{max-width:480px;max-height:100%;text-align:left}
							.w55{width:55%!important}
						</style>
						<div class="bottom-wrap">
							<!--表情-->
							<div class="chat-emotion">
								<div class="emotion-wrap" style="display:none;">
									<for start="0" end="108">
										<img class="emotion-img" src="{pigcms{$static_public}emoticons/{pigcms{$i}.gif"/>
									</for>
								</div>
							</div>
							<!--表情-->
							<div contenteditable="true" class="input chat-input w55" id="chat-input"></div>
							<input type="hidden" contenteditable="true" name="chatMsg" class="input chat-input w55" placeholder="聊天内容" id="sxw" value=""/>
							<a class="button button-orange w20 fr" style="*margin-top:-15px; padding:0px;" id="sendMsg">发送</a>
							<div class="clr"></div>
						</div>
						<script type="text/javascript">
							(function(){
							    var imgReader = function( item ){
							        var blob = item.getAsFile(),
							            reader = new FileReader();
							        reader.onload = function( e ){
							            var img = new Image();
							            img.src = e.target.result;
							            console.log(img);
							            document.getElementById('chat-input').appendChild( img );
							        };
							        reader.readAsDataURL( blob );
							    };
							
							    document.getElementById( 'chat-input' ).addEventListener( 'paste', function( e ){
							        //windowie下获取黏贴的内容，火狐谷歌下获取黏贴的内容
							        var clipboardData = e.clipboardData,//谷歌
							            i = 0,
							            items, item, types;
							             console.log('0')
							
							        if( clipboardData ){
							             console.log('1')
							            items = clipboardData.items;
							            if( !items ){
							                console.log(2)
							                return;
							            }
							            console.log(3)
							            item = items[0];
							            types = clipboardData.types || [];
							            for( ; i < types.length; i++ ){
							                if( types[i] === 'Files' ){
							                    item = items[i];
							                    break;
							                }
							            }
							            if( item && item.kind === 'file' && item.type.match(/^image\//i) ){
							                imgReader( item );
							            }
							         }
							     },false);
							})(); 
							</script>
					</div>
				</div>
				<div class="clr"></div>
			</div>
		</div>
  		<div class="footerBar">
	        <div class="footer-wrap">
				<div class="fl ml10">
				©Copyright 2016 {pigcms{$now_site_short_url} {pigcms{$config.site_icp} 
				<a class="ml10 mr10" href="{pigcms{$config.site_url}">网站首页</a> 
				</div>
				<!--div class="fr mr10">
					友情链接：
					<a href="#" target="_new"> #</a>
				</div-->
			</div>
	    </div>
<script src="{pigcms{$static_path}js/jquery-1.8.2.min.js"></script>
<script src="{pigcms{$static_public}js/layer-v2.1/layer.js"></script>
<script>
	if(/(iphone|ipod|android|windows phone)/.test(navigator.userAgent.toLowerCase())){
		alert('视频拍卖手机端正在开发测试中！当前访问的是PC端页面，使用PC浏览器访问可获得最佳效果。');
	}
	// window.onerror=function(sMessage, sUrl, sLine){
		// alert(sMessage + sUrl + sLine + "\n");
		// return true;
	// }
	// 1fsda
	var pleaseMoney = 0;
	var plusPrice = 0;
	var nowDomainId = 0;
	var term_id = {pigcms{$now_term.term_id};
	var myUid = <if condition="$user_session">{pigcms{$user_session.uid}<else/>0</if>;
	var lastAjaxTime = 0;
	var indexShade = layer.load(1,{shade:[0.3,'#000']});
	var showSound = true;
	var lockScreen = false;
	var lockScreenIndex = null;
	// alert($(window).height());
	if($(window).height() > 880 && $(window).height() < 930){
		$('.footerBar').remove();
	}
	if($(window).height() < 880){
		$('.s-bg-white').remove();
		$('#miniScreenAccount').show();
	}
	if($(window).height() < 770){
		$('.footerBar').remove();
		//$('.video-box').css({'height':'210'});
	}
	if($(window).height() < 720){
		$('.video-wrap .col-title').remove();
		//$('.video-box').css({'height':'210'});

	}
	if($(window).height() < 700){
		$('.domain-info .domain-info-item .box2').css({'padding-bottom':'2%','padding-top':'2%'});
		//$('.video-box').css({'height':'210'});
	}
	if($(window).height() < 680){
		$('.domain-info .domain-info-item .box2').css({'padding-bottom':'2%','padding-top':'2%'});
		$('.video-box').css({'height':'210'});
	}
	$('.col,.member-list,.slimScrollDiv,.slimscroll').height($(window).height() - $('.s-bg-white').height() - $('.headerBar').height() - $('.footerBar').height());
	$('.auction-list').height($('.auction-wrap').height() - $('.icon_clock').height() - $('.bid-price-wrap').height() - 20);
	$('.chat-list .slimScrollDiv,.chat-list .chat-list-wrap').height($('.chat-list').height()-283);
	$('.video-wrap .tab-content,#milist_container').height($('.video-wrap').height() - $('.video-wrap .col-title').height() - $('.video-wrap .video-box').height() - $('.video-wrap .tab-wrap').height() -26);
	var canShowChujiaRows = parseInt($('.auction-list').height() / 65);
	/*$(window).resize(function(){
		location.reload();
	});*/
	//搜索下拉
	$(".u-slt-a").click(function(event){
        $(".f-a-rlt").css("display")=="none"?$(".f-a-rlt").css("display","block"):$(".f-a-rlt").css("display","none");
        event.stopPropagation();
    });
    $(".f-a-rlt p").click(function(){
        $(this).parent().parent().find(".u-slt-a").html($(this).text());
        $(".u-slt-a").attr("ym",$(this).attr("ym"));
        $(this).parent().hide();
    });
	//点击按钮搜索
    var searchValue = "";
    $(".u-btn-search").click(function () {
        var keyword=$(".u-ipt-txta").val();
        if (keyword == $(".u-ipt-txta").attr("ph"))
            keyword="";
        var cla = $(".u-slt-a").attr("ym");
        var href = "./index.php?c=Whois&a=index&keyword=";
        if (cla == "b") {
            if (keyword.indexOf(".") <0)
                keyword+=".com";
            window.open(href + keyword.replace(/\s*/g,""));
        } else {
            location.href = "{pigcms{:U('Buydomains/index')}&keyword=" + keyword.replace(/\s*/g, "");
        }
    });
	$(".u-ipt-txta").focusout(function () {
        $(".f-search-rlt").css("display", "none");
        $(this).parent().css({"border":"2px solid #FFA200","border-right":"0"});
    }).focus(function () {
        $(this).parent().css({"border":"2px solid #FFA200","border-right":"0"});
        var cla =$(".u-slt-a").attr("ym");
        if (cla == "b") return false;
        if ($(this).val() != "" && $(this).val()!=$(this).attr("ph")) {
            $(".f-search-rlt").css("display", "block");
        } else {
            $(".f-search-rlt").css("display", "none");
        }
    });
	//语言切换
    $(document.body).click(function () {
        $(".f-a-rlt").hide();
        $(".f-b-rlt").stop(true,true).hide();
    });
    $(".f-b-rlt p").click(function(event){
        event.stopPropagation();
    });
	$.post("{pigcms{:U('Videosale/getFirstDatas')}",{term_id:term_id},function(result){
		//在线会员
		var userHtml = '';
		$.each(result.info.onlineUsers,function(i,item){
			userHtml+= '<div class="member member_'+item.uid+'" data-memberid="'+item.uid+'"><img alt="" class="member-header" data-index="0" src="{pigcms{$config.site_url}/upload/User_level/'+item.pic+'" data-userid="'+item.uid+'"/><div class="member-info"><div class="name">'+item.name+'</div><div class="id">ID：'+item.uid+'</div></div><div class="clr"></div></div>';
		});
		$('#memberDom').html(userHtml);
		
		//当前拍卖
		var domainList = result.info.domainList;
		if(domainList == 'null'){
			layer.alert('该期拍卖下没有了域名，系统将跳转首页。',{icon: 3}, function(index){
				location.href = "{pigcms{:U('Index/index')}";
			});
			return;
		}
		$('.current-domain').html('当前拍卖域名：<span class="suffix">'+domainList[0].domains_suffix+'</span> '+domainList[0].domains);
		$('.current-domain-description').html(domainList[0].domains_desc);
		$('#qipaijia').html(domainList[0].mark_money!='0' ? domainList[0].mark_money : '无');
		$('#zuigaojia').html(domainList[0].now_money!='0' ? '￥'+domainList[0].now_money : '无');
		plusPrice = domainList[0].plusPrice;
		$('#jiajiafudu').html('￥'+plusPrice);
		pleasePrice = domainList[0].pleasePrice;
		nowDomainId = domainList[0].domain_id;
		$('#bidding').val(pleasePrice);
		
		//待拍域名列表
		var domainListHtml = '';
		$.each(result.info.domainList,function(i,item){
			domainListHtml += '<div class="domain-wrap domain-'+item.domain_id+'"><span class="domain-name '+(item.domain_id == nowDomainId ? 'bidding' : 'passed')+'" title="'+item.domains_desc+'">'+item.domains+'</span><span class="domain-description" title="'+item.domains_desc+'">'+item.domains_desc+'</span><div class="clr"></div></div>';
		});
		$('.domain-list').html(domainListHtml);
		
		//价格历史
		var chujiaHtml = '';
		$.each(result.info.priceHistory,function(i,item){
			chujiaHtml += '<div class="bid animated fadeIn price-'+item.uninxtime+'-'+item.uid+'"><div class="mark"><img src="{pigcms{$config.site_url}/upload/User_level/'+item.pic+'"></div><div class="temporaryClass"><span class="price">出价: ￥'+toThousands(item.price)+'</span></div><div class="info"><div class="id">ID: '+item.uid + ((item.is_sys_op == 1) ? (' - 同步助手- <font color=green>'+item.chujia_name + '</font>') :'') +' </div><div class="time">'+item.time+'</div><div ' +
			'class="clr"></div></div></div>';
			//console.log(item.uid+'xxxx:'+item.is_sys_op+'=====');
			if(item.uid == myUid && item.is_sys_op != 1){
				$('#myzuigaojia').html('￥'+toThousands(item.price));
				$('#sendPrice').addClass('disable').data('errTips','您已经出过价，需要等待其他人出价后您才能再次出价。');
			}else{
				$('#sendPrice').removeClass('disable');
			}
		});
		$('#bid_container').html(chujiaHtml);
		if(result.info.priceHistory.length > canShowChujiaRows){
			$('#bid_container .bid:lt('+(result.info.priceHistory.length - canShowChujiaRows)+')').remove();
		}
		$('#bid_container .bid:last .temporaryClass').append('&nbsp;&nbsp;<img class="temporary_img" src="{pigcms{$static_path}videosale/img/1st.png"/>');
		
		//聊天历史
		var imHtml = '';
		$.each(result.info.msgHistory,function(i,item){
			imHtml += '<div class="chat-content-wrap red msg-'+item.uninxtime+'-'+item.uid+'"><div class="chat-header"><img src="{pigcms{$config.site_url}/upload/User_level/'+item.pic+'"/></div><div class="chat-content"><div class="chat-id">'+item.uid+' - '+item.name+' <span class="time">'+item.time+'</span> '+((item.is_sys_op == 1) ? (' - 同步助手- <font color=green>'+item.chujia_name + '</font>') :'') +' </div><div class="chat-msg">'+item.msg+'</div></div><div class="clr"></div></div>';
		});
		if(imHtml != ''){
			$('.chat-list-wrap').html(imHtml);
			$('.chat-list-wrap').scrollTop($('.chat-list-wrap').height()+10000);
		}
		
		//锁住屏幕
		if(result.info.lockScreen == '1'){
			<if condition="empty($_SESSION['system'])">lockScreenFunction();</if>
			lockScreen = true;
			<if condition="!empty($_SESSION['system'])">$('#lockScreen').html('解锁屏幕');</if>
		}else{
			lockScreen = false;
			<if condition="!empty($_SESSION['system'])">$('#lockScreen').html('锁屏保护');</if>
		}
		layer.close(indexShade);
		lastAjaxTime = result.info.lastAjaxTime;
	});
	<if condition="!empty($_SESSION['system'])">
	function lockScreenFunction(){
		if(lockScreenIndex){
			layer.close(lockScreenIndex);
		}
		lockScreenIndex = layer.open({
			type: 1,
			closeBtn: 0, //不显示关闭按钮
			shift: 2,
			shadeClose: false, //开启遮罩关闭
			content: '<div style="padding:40px;">拍卖主持人关闭了屏幕操作</div>'
		});
	}
	$('#lockScreen').click(function(){
		layer.confirm(lockScreen ? '您确定要解锁用户的屏幕吗？' : '您确定要锁住用户的屏幕吗？', {
			icon: 3, 
			title:'确认提示：',
			btn: ['确定','关闭'] //按钮
		},function(){
			indexShade = layer.load(1,{shade:[0.3,'#000']});
			$.post("{pigcms{:U('Videosale/lockScreen')}",{lockScreen: lockScreen ? '0' : '1'},function(result){
				if(lockScreen){
					lockScreen = false;
					$('#lockScreen').html('锁屏保护');
				}else{
					lockScreen = true;
					$('#lockScreen').html('解锁屏幕');
				}
				layer.closeAll();
			});
		}, function(){
			
		});
	});
	$('#finishVideoSale').click(function(){
		layer.confirm('您确定要结束此域名的拍卖吗？', {
			icon: 3, 
			title:'确认提示：',
			btn: ['确定','关闭'] //按钮
		},function(){
			indexShade = layer.load(1,{shade:[0.3,'#000']});
			$.post("{pigcms{:U('Videosale/finishDomain')}",{term_id:term_id,domainId:nowDomainId},function(result){
				layer.closeAll();
				if(result.status == 1){
					layer.alert('结束成功', {icon:1});
				}else{
					layer.alert(result.info, {icon:5});
				}
			});
		}, function(){
			
		});
	});
	</if>
	$('.sound_switch,.sound_off').click(function(){
		if(showSound == true){
			showSound = false;
			$(this).addClass('sound_off');
		}else{
			showSound = true;
			$(this).removeClass('sound_off');
		}
	});
	
	$('.video-wrap .tab-wrap .tab').click(function(){
		$(this).css('color','rgb(255,102,0)').siblings('div').css('color','rgb(102,102,102)');
		if($(this).index() == 1){
			$('.video-wrap .tab-content .domain-list-wrap').show();
			$('.video-wrap .tab-content .domain-info-wrap').hide();
		}else{
			$('.video-wrap .tab-content .domain-info-wrap').show();
			$('.video-wrap .tab-content .domain-list-wrap').hide();
		}
	});
	$('.btn-add').click(function(){
		$('.btn-subtract').removeClass('disable');
		$('#bidding').val(parseInt($('#bidding').val())+plusPrice);
	});
	$('.btn-subtract').click(function(){
		if($('.btn-subtract').hasClass('disable')){
			return false;
		}
		var biddingMoney = parseInt($('#bidding').val())-plusPrice;
		if(biddingMoney == pleasePrice){
			$('.btn-subtract').addClass('disable');
		}
		$('#bidding').val(biddingMoney);
	});
	$('#sendPrice').click(function(){
		if(myUid == 0){
			layer.confirm('请您先进行登录！', {
				icon: 0, 
				title:false,
				btn: ['去登录','关闭'] //按钮
			},function(){		
				window.location.href = "http://www.chengjie.com/index.php?c=Login&a=index";
			}, function(){
			
			});
			return false;
		}
		if($('#sendPrice').hasClass('disable')){
			layer.alert($(this).data('errTips'), {icon:0});
			return false;
		}
		layer.confirm('您确定出价 <span style="color:red;font-size:18px;">￥'+toThousands($('#bidding').val())+'</span> 嘛？', {
			icon: 3, 
			title:'确认提示：',
			btn: ['确定','关闭'] //按钮
		},function(){
			indexShade = layer.load(1,{shade:[0.3,'#000']});
			$.post("{pigcms{:U('Videosale/sendprice')}",{term_id:term_id,domainId:nowDomainId,offer:$('#bidding').val()},function(result){
				layer.close(indexShade);
				if(result.status == 1){
					$('#myzuigaojia,#zuigaojia').html('￥'+toThousands(result.info));
					$('#sendPrice').addClass('disable').data('errTips','您已经出过价，需要等待其他人出价后您才能再次出价。');
					layer.alert('出价成功', {icon:1});
				}else{
					if(result.info[0] == 1004){
						$('#bidding').val(result.info[1]);
						var errMsg = '您的报价已被超越';
					}else{
						var errMsg = result.info[1];
					}
					layer.alert(errMsg, {icon:5});
				}
			});
		}, function(){
			
		});
	});
	$('.chat-emotion').toggle(function(){
		$('.emotion-wrap').show();
	},function(){
		$('.emotion-wrap').hide();
	});
	$('.emotion-wrap .emotion-img').click(function(){
		var chatMsg = $('input[name=chatMsg]').val();

		var url="<img src=\""+$(this).attr('src')+"\" />";
		chatMsg+=url;
		if('<span class="placeholder">聊天内容</span>' == $(".chat-input[contenteditable=true]").html()){
			$(".chat-input[contenteditable=true]").html('');
		}
		$(".chat-input[contenteditable=true]").html(chatMsg);
		$("input.chat-input[contenteditable=true]").val(chatMsg);
	});
	$('#sendMsg').click(function(){
		sendMsg(0);
	});
	/*$('#chat-input').bind('input propertychange', function(){
		$("input.chat-input[contenteditable=true]").val($("div.chat-input[contenteditable=true]").html());
	});  */
	$('#chat-input').keyup(function(e){
		var chatMsg = $('input[name=chatMsg]').val();
		if (chatMsg && e.keyCode == 13) {
			// $('input[name=chatMsg]').val($("div.chat-input[contenteditable=true]").html());
			sendMsg(1);
			$("div.chat-input[contenteditable=true]").empty();
			$("input.chat-input[contenteditable=true]").val("");
		}
		
	});
	function toThousands(num) {
		var num = (num || 0).toString(), result = '';
		while (num.length > 3) {
			result = ',' + num.slice(-3) + result;
			num = num.slice(0, num.length - 3);
		}
		if (num) { result = num + result; }
		return result;
	}
	function sendMsg(kind){
		if(myUid == 0){
			layer.confirm('请您先进行登录！', {
				icon: 0, 
				title:false,
				btn: ['去登录','关闭'] //按钮
			},function(){		
				window.location.href = "http://www.chengjie.com/index.php?c=Login&a=index";
			}, function(){
			
			});
			return false;
		}
		$("input.chat-input[contenteditable=true]").val($("div.chat-input[contenteditable=true]").html());
		var chatMsg = $('input[name=chatMsg]').val();
		//firefox  不兼容
		// if(chatMsg.indexOf('img','0')<0){
		//     chatMsg = document.getElementById('sxw').previousSibling.innerText;
		//     if (!chatMsg.trim() || !chatMsg.replace(/&nbsp;/g,"").trim()) { avalon.log('nothing...'); return; }
		// }

		chatMsg =  chatMsg.replace(/<P>&nbsp;<\/P>/g,"").replace(/(^\s*)|(\s*$)/g, "");
		if(chatMsg.length < 1){
			alert('随便写点东西');
			$("div.chat-input[contenteditable=true]").empty();
			$("input.chat-input[contenteditable=true]").val("");
			return;
		}
		if(chatMsg.length>10000000000000000000000000000000){
			alert('发送内容不能超过100个字符，表情少发点试试');
			$("div.chat-input[contenteditable=true]").empty();
			$("input.chat-input[contenteditable=true]").val("");
			return;
		}
		$("input.chat-input[contenteditable=true]").val("");
		if(kind != 1){
			$("div.chat-input[contenteditable=true]").empty();
			$("div.chat-input[contenteditable=true]").innerHTML='<span class="placeholder">聊天内容</span>';
		}else{
			$("div.chat-input[contenteditable=true]").empty();
		}
		$.post("{pigcms{:U('Videosale/sendmsg')}",{msg:chatMsg,domainId:nowDomainId},function(result){
			$("input.chat-input[contenteditable=true]").val("");
			if(kind != 1){
				$("div.chat-input[contenteditable=true]").empty();
				$("div.chat-input[contenteditable=true]").innerHTML='<span class="placeholder">聊天内容</span>';
			}else{
				$("div.chat-input[contenteditable=true]").empty();
			}
		});
	}
	var ajaxTimeout = null;
	function ajaxGetData(){
		if(term_id != 0){
			clearTimeout(ajaxTimeout);
			$.post("{pigcms{:U('Videosale/ajaxCheck')}",{term_id:term_id,domainId:nowDomainId,lastAjaxTime:lastAjaxTime,lockScreen:lockScreen ? '1' : '0'},function(result){
				//在线会员
				if(result.info.onlineUsers){
					var userHtml = '';
					$.each(result.info.onlineUsers,function(i,item){
						if($('.member_'+item.uid).size() == 0){
							userHtml+= '<div class="member member_'+item.uid+'" data-memberid="'+item.uid+'"><img alt="" class="member-header" data-index="0" src="{pigcms{$config.site_url}/upload/User_level/'+item.pic+'" data-userid="'+item.uid+'"/><div class="member-info"><div class="name">'+item.name+'</div><div class="id">ID：'+item.uid+'</div></div><div class="clr"></div></div>';
						}
					});
					$('#memberDom').append(userHtml);
				}
				//下线会员
				if(result.info.unlineUsers){
					$.each(result.info.unlineUsers,function(i,item){
						$('.member_'+item.uid).remove();
					});
				}
				//当前拍卖
				if(result.info.nowDomainInfo){
					var nowDomainInfo = result.info.nowDomainInfo;
					if(nowDomainInfo != 'null'){
						$('.current-domain').html('当前拍卖域名：<span class="suffix">'+nowDomainInfo.domains_suffix+'</span> '+nowDomainInfo.domains);
						$('.current-domain-description').html(nowDomainInfo.domains_desc);
						$('#qipaijia').html(nowDomainInfo.mark_money!='0' ? nowDomainInfo.mark_money : '无');
						$('#zuigaojia').html(nowDomainInfo.now_money!='0' ? '￥'+nowDomainInfo.now_money : '无');
						plusPrice = nowDomainInfo.plusPrice;
						$('#jiajiafudu').html('￥'+plusPrice);
						pleasePrice = nowDomainInfo.pleasePrice;
						if(nowDomainInfo.domain_id != nowDomainId){
							$('#myzuigaojia').html('无');
							$('#bidding').val(pleasePrice);
							$('#bid_container').empty();
							$('.domain-'+nowDomainId+' .bidding').removeClass('bidding').addClass('passed');
							$('.domain-'+nowDomainId).next().find('.domain-name').removeClass('passed').addClass('bidding');
						}else if(parseInt($('#bidding').val()) < pleasePrice){
							$('#bidding').val(pleasePrice);
							$('.btn-subtract').addClass('disable');
						}
						nowDomainId = nowDomainInfo.domain_id;
						$('#sendPrice').removeClass('disable');
					}else{
						$('.bid-price').html('域名拍卖结束').css('text-align','center');
					}
				}
				
				
				//价格历史
				if(result.info.priceHistory){
					console.log(result.info.priceHistory);
					var chujiaHtml = '';
					$.each(result.info.priceHistory,function(i,item){
						if($('.price-'+item.uninxtime+'-'+item.uid).size() == 0){
							chujiaHtml += '<div class="bid animated fadeIn price-'+item.uninxtime+'-'+item.uid+'"><div class="mark"><img src="{pigcms{$config.site_url}/upload/User_level/'+item.pic+'"></div><div class="temporaryClass"><span class="price">出价: ￥'+toThousands(item.price)+'</span></div><div class="info"><div class="id">ID: '+item.uid+'</div><div class="time">'+item.time+ ((item.is_sys_op == 1) ? (' - 同步助手- <font color=green>'+item.chujia_name + '</font>') :'') +'</div><div class="clr"></div></div></div>';
						}
						console.log(item);
						if(item.uid == myUid && item.is_sys_op != 1){
							$('#sendPrice').addClass('disable').data('errTips','您已经出过价，需要等待其他人出价后您才能再次出价。');
						}else{
							$('#sendPrice').removeClass('disable');
						}
					});
					if(chujiaHtml != ''){
						$('#bid_container').append(chujiaHtml);
						if($('#bid_container .bid').size() > canShowChujiaRows){
							$('#bid_container .bid:lt('+($('#bid_container .bid').size() - canShowChujiaRows)+')').remove();
						}
						$('.temporary_img').remove();
						$('#bid_container .bid:last .temporaryClass').append('&nbsp;&nbsp;<img class="temporary_img" src="{pigcms{$static_path}videosale/img/1st.png"/>');
						if(showSound == true){
							$('#priceMedia').remove();
							$('body').append('<video controls="" autoplay="" name="media" id="priceMedia"><source src="{pigcms{$static_path}videosale/img/ring.mp3" type="audio/mpeg"></video>');
						}
					}
				}
				
				//聊天历史
				if(result.info.msgHistory){
					var imHtml = '';
					console.log(result.info.msgHistory);
					$.each(result.info.msgHistory,function(i,item){
						if($('.msg-'+item.uninxtime+'-'+item.uid).size() == 0){
							imHtml += '<div class="chat-content-wrap red msg-'+item.uninxtime+'-'+item.uid+'"><div class="chat-header"><img src="{pigcms{$config.site_url}/upload/User_level/'+item.pic+'"/></div><div class="chat-content"><div class="chat-id">'+item.uid+' - '+item.name+' <span class="time">'+item.time+'</span>' +((item.is_sys_op == 1) ? (' - 同步助手- <font color=green>'+item.chujia_name + '</font>') :'') +'</div><div class="chat-msg">'+item.msg+'</div></div><div class="clr"></div></div>';
						}
					});
					if(imHtml != ''){
						$('.chat-list-wrap .empty').remove();
						$('.chat-list-wrap').append(imHtml);
						$('.chat-list-wrap').scrollTop($('.chat-list-wrap').height()+10000);
					}
				}
				
				//锁住屏幕
				if(result.info.lockScreen){
					if(result.info.lockScreen == '1'){
						<if condition="empty($_SESSION['system'])">lockScreenFunction();</if>
						lockScreen = true;
						<if condition="!empty($_SESSION['system'])">$('#lockScreen').html('解锁屏幕');</if>
					}else{
						lockScreen = false;
						layer.close(lockScreenIndex);
						<if condition="!empty($_SESSION['system'])">$('#lockScreen').html('锁屏保护');</if>
					}
				}
				
				layer.close(indexShade);
				lastAjaxTime = result.info.lastAjaxTime;
				
				ajaxGetData();
			});
		}
	}
	ajaxTimeout = setInterval(function(){
		ajaxGetData();
	},3000);
</script>
<script>
/*
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
*/
</script>
</body>
</html>