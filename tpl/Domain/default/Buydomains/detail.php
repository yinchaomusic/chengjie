<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>{pigcms{$now_domain.domain} - 域名购买 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/common.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Buydomains/index')}" class="s-2a">议价域名</a>&nbsp;&gt;&nbsp;</li>
					<li>{pigcms{$now_domain.domain}</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
		<div class="m-content">
			<!-- 左边 -->
			<div class="u-fl">
				<div class="g-a-container">
					<div class="m-ta s-bg-white u_be9">
						<div class="title">
							<a class="s-3c" href="{pigcms{:U('Buydomains/index')}">
								<span class="z-img-t3"></span>
								<span>议价域名</span>
							</a>
						</div>
						<div class="u-cls"></div>
					</div>
					<div class="m-inner1 s-bg-fc u_be9">
						<h3 class="f-ha u_bb0">
							<a href="{pigcms{:U('Mibiao/index',array('mmid'=>$now_domain['uid']))}" style="float:right;font-size:14px;" target="_blank">查看该用户其它域名</a>
							<a href="{pigcms{:U('Whois/index',array('keyword'=>$now_domain['domain']))}" class="s-2a" target="_blank" title="点击查询Whois信息">{pigcms{$now_domain.domain}</a>
						</h3>
						<div class="u_instr2">
							<div class="instr2-l">
								如果您感兴趣，请在下面提交您的报价，卖家就会收到 {pigcms{$config.site_name} 的邮件通知并对您的报价作出相应回复。
							</div>
							<div class="instr2-r">
                                                            <if condition="$collection_show eq 1">
                                                                <a title="收藏域名" id="collection" style="width: 25px; background-image:url({pigcms{$static_path}images/account/sc.png)"></a>
                                                                <else/>
                                                                <a title="收藏域名" id="collection" style="width: 25px;background-image:url({pigcms{$static_path}images/account/wsc.png)"></a>
                                                            </if>
                                                            <input type="hidden" value="{pigcms{$now_domain.domain_id}" id="domainid" />
                                                            <input type="hidden" value="{pigcms{$now_domain.domain}" id="domain_name" />
                                                            <input type="hidden" value="{pigcms{$now_domain.type}" id="domain_type" />
								<a href="{pigcms{:U('Whois/index',array('keyword'=>$now_domain['domain']))}" target="_blank" title="Whois查询" class="f-img-i6"></a>
								<a href="javascript:laLock($('.f-ha a.s-2a'))" target="_blank" title="切换大小写" class="f-img-i1"></a>
								<a href="http://www.baidu.com/s?wd={pigcms{$now_domain.domain}" target="_blank" title="百度搜索" class="f-img-i2"></a>
								<!--span class="jiathis_style"><a class="jiathis_button_weixin f-img-i4" title="分享到微信"><span class="jiathis_txt jtico jtico_weixin"></span></a></span>
								<a href="javascript:share_qqwb('fdgt.cn 域名正在出售中','#')" target="_blank" title="分享到腾讯微博" class="f-img-i3"></a>
								<a href="javascript:share_sinawb('fdgt.cn 域名正在出售中','#')" target="_blank" title="分享到新浪微博" class="f-img-i5"></a-->
							</div>
							<div class="u-cls"></div>
						</div>
					</div>
					<div class="m-inner2 s-bg-fc u_be9">
						<div class="instr3-l s-bg-fea">{pigcms{$now_domain.desc}<p>浏览次数：{pigcms{$now_domain.hits}</p></div>
						<div class="instr3-r">
							<p class="u_mt33">
								<span class="u-txr2">卖家标价：</span>
								<span class="s-75b u-txr3 u_ml6"><if condition="$now_domain['money']">{pigcms{$now_domain.money}<else/>买家报价</if></span>
							</p>
							<p>
								<span class="u-txr2">您的报价：</span>
								<span class="u-ipt-h u_ml6 u_vt s-bg-white">
									<input type="text" class="u_vm u_prise">
								</span>
								<span class="u_m2 s-ae">￥</span>
							</p>
							<p style="line-height: 20px; margin: 12px 0 2px 19px;display:none;">
								<input type="checkbox" style="margin-left: 1px; vertical-align: middle; cursor: pointer;" id="agree" checked="checked"/>
								<span style="vertical-align: middle; font-size: 14px;">我已阅读并同意<a href="#" target="_blank" title="阅读" style="color: #2aa3ce;">《域名交易服务协议》</a></span></p>
							<p style="margin-top:12px;"><input type="button" value="提交报价" class="s-bg-2a u-btn7a" id="bj"/></p>
							<input type="hidden" value="{pigcms{$now_domain.domain_id}" id="domainid"/>
						</div>
						<div class="u-cls"></div>
					</div>
					<div class="m-inner3 s-bg-fc u_be9">
						<div class="m-ta s-bg-white">
							<div class="title">
								<a class="s-47" href="javascript:void(0)">
									<span>您可能感兴趣的域名</span>
								</a>
							</div>
							<div class="more">
								<a href="{pigcms{:U('Buydomains/index')}" target="_blank">更多&gt;&gt;</a>
							</div>
						</div>
						<div class="m-iner-data1">
							<ul>
								<volist name="favList" id="vo">
									<li><a href="{pigcms{:U('Buydomains/detail',array('id'=>$vo['domain_id']))}" target="_blank" title="{pigcms{$vo.domain}">{pigcms{$vo.domain}</a></li>
								</volist>
							</ul>
							<div class="u-cls"></div>
						</div>
					</div>
				</div>
			</div>
			<!-- 右边 -->
			<div class="u-fr">
				<div class="g-c-container">
					<div class="m-processList u_mt1 u_mb6">
						<h3 class="cli">我要购买</h3>
						<div class="process" style="display: block;">
							<ul>
									<li>
										<span class="f-img-s3">1</span><span class="u-txh">点击购买</span>
									</li>
									<li>
										<span class="f-img-s3">2</span><span class="u-txh">支付定金</span>
									</li>
									<li>
										<span class="f-img-s3">3</span><span class="u-txh">发起交易</span>
									</li>
									<li>
										<span class="f-img-s3">4</span><span class="u-txh">支付全款</span>
									</li>
									<li>
										<span class="f-img-s3">5</span><span class="u-txh">域名过户</span>
									</li>
									<li class="u_mb0">
										<span class="f-img-s3">6</span><span class="u-txh">交易完成</span>
									</li>


							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<include file="Public:footer"/>
	<include file="Public:sidebar"/>
	<div class="s-bg-f7"></div>
	<script type="text/javascript" src="{pigcms{$static_path}js/detail.js"></script>
        <script>
            $("#collection").click(function(){
                var domain_id = $("#domainid").val();
                var domain = $("#domain_name").val();
                var type = $("#domain_type").val();
                $.post("{pigcms{:U('Hotsale/collection')}",{'domain_id':domain_id,'domain':domain,'type':type}, function(data){
                    if(data == 1){
                        alert('收藏成功');
                        location.href=location.href;
                    }else if(data == 3){
                        alert('已经收藏');
                    }else{
                        alert('失败请重试');
                    }
                })
            })
    </script>
</body>
</html>