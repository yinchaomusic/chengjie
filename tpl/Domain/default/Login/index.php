<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>会员登录 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link type="text/css" href="{pigcms{$static_path}css/register.css" rel="stylesheet"/>
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>登录</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls u_ptb18">
		<div class="m-content2">
			<div class="m-title2 s-bg-fb">
				<div class="ut3">
					<p class="s-3c f-p-t8">会员登录</p>
					<p class="f-p18 s-ac">立即登录开始购买、出售域名、管理您的账户！</p>
				</div>
			</div>
			<div class="m-login1 s-bg-white">
				<form id="login_form" method="post">
					<div class="log-l">
						<ul>
							<li>
								<span class="u-txs">账户：</span>
								<span class="u-ipt-g f-img-login1">
									<input type="text" ph="请输入您的邮箱/手机/ID" id="login_account" name="account"/>
								</span>
							</li>
							<li>
								<span class="u-txs">密码：</span>
								<span class="u-ipt-g f-img-login2">
									<input type="password" id="login_pwd" name="pwd"/>
								</span>

								<a href="{pigcms{:U('Login/findpsw')}" class="u_forget">忘记密码？</a>

							</li>
							<li><input type="submit" value="登录" class="s-bg-2a u-btn5a"/></li>
							<li class="u_pl71">还不是会员？<a href="{pigcms{:U('Login/reg')}" class="u_forget">新用户注册</a></li>
						</ul>
					</div>
					 
					<div class="log-r">
					<p>您还可以通过以下方式登录</p>
					<a class="log-qq s-def oauth__link--weixin" href="">用手机微信扫码登录</a>
                    <br>
					<!--a class="log-xl s-def" href="">微博登陆</a-->
				   </div>
				  
				</form>

				<div class="u-cls"></div>
			</div>
		</div>
	</div>
	<include file="Public:footer"/>
	<script src="{pigcms{$static_public}js/artdialog/jquery.artDialog.js"></script>
	<script src="{pigcms{$static_public}js/artdialog/iframeTools.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			if($('body').height() < $(window).height()){
				$('.site-info-w').css({'position':'absolute','width':'100%','bottom':'0'});
			}
			
		
			$('.oauth__link--weixin').click(function(){
				art.dialog.open("{pigcms{:U('Index/Recognition/see_login_qrcode',array('referer'=>urlencode($referer)))}&"+Math.random(),{
					init: function(){
						var iframe = this.iframe.contentWindow;
						window.top.art.dialog.data('login_iframe_handle',iframe);
					},
					id: 'login_handle',
					title:'请使用微信扫描二维码登录',
					padding: 0,
					width: 430,
					height: 433,
					lock: true,
					resize: false,
					background:'black',
					button: null,
					fixed: false,
					close: null,
					left: '50%',
					top: '38.2%',
					opacity:'0.4'
				});
				return false;
			});
		});
	</script>
	<script type="text/javascript">
		var static_public="{pigcms{$static_public}",static_path="{pigcms{$static_path}",post_reset="{pigcms{:U('Login/resetmail')}",login_check="{pigcms{:U('Login/check')}",
			reg_check="{pigcms{:U('Login/reg_check')}",domain_index="{pigcms{$config.site_url}",domain_login="{pigcms{:U('Login/index')}",user_index="<if condition="!empty($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'],'c=Login') eq false">{pigcms{$_SERVER.HTTP_REFERER}<else/>{pigcms{:U('Account/index')}</if>";
	</script>
        
	<script type="text/javascript" src="{pigcms{$static_path}login/login.js"></script>
	<include file="Public:sidebar"/>
</body>
</html>
