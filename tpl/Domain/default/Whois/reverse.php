<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8"/>
	<title>Whois - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}whois/css/reset.css" rel="stylesheet" />
	<link type="text/css" href="{pigcms{$static_path}whois/css/common.css" rel="stylesheet" />
	<link type="text/css" href="{pigcms{$static_path}whois/css/whios_pr.css" rel="stylesheet" />

</head>

<body>
<form method="post" action="" id="form1" autocomplete="off">
	<div class="aspNetHidden">

		<input type="hidden" name="keyword" id="keyword" value="{pigcms{$Think.get.keyword}" />
	</div>





	<include file="Whois:whois_nav"/>

	<div class="content">
		<div class="c_serch">
			<input name="txtvalue" type="text" value="{pigcms{$Think.get.keyword}" id="txtvalue" class="t_input" placeholder="请输入域名、注册人、邮箱">

			<select name="dropselect" id="dropselect">
				<option selected="selected" value="ym">通过域名</option>
<!--				<option value="email">通过邮箱</option>-->
<!--				<option value="_owner">通过注册者</option>-->

			</select>
			<input type="submit" name="btnSerch" value="查询" id="btnSerch" class="btn_c_serch">

		</div>


		<div id="Panel1">



			<div class="c_sec">
				<ol>
					<li class="t wx_260">
						<div class="fl ym"></div>
						<div class="fl ymz">域名相关信息</div>
					</li>
					<li class="wx_690">{pigcms{$reverseinfo.domainName}</li>
				</ol>
				<div class="se_cont">
					<ul>
						<li class="hi_25"></li>
						<li class="pb_15 active">
							<span>域名</span>
							<a class="c_2f sp_1" href="javascript:void();" target="_blank">{pigcms{$reverseinfo.domainName}</a>
							<span>域名相关信息&nbsp;&nbsp;</span>
							<a class="c_2f" id="check_whois" href="{pigcms{:U('Whois/index',array('keyword'=>$reverseinfo['domainName']))}" target="_blank">[查看WHOIS]</a>
						</li>
						<li class="pb_15">
							<span>注册人</span>
							<span class="sp_6">{pigcms{$reverseinfo.registrarName}
						</li>
						<li class="pb_15 mt_40 active">
							<div>注册人邮箱</div>
							<div class="sp_5" style="max-width:480px;height:45px;" title="walolaw@gmail.com">{pigcms{$reverseinfo.registrarEmail}</div>
						</li>
					</ul>
				</div>
			</div>

		</div>




	</div>


	<div class="footer">
		<p><span class="n_m">{pigcms{$config.site_icp} |&nbsp; {pigcms{$config.site_show_footer}</span></p>
	</div>
		<script src="{pigcms{$static_path}js/jquery-1.8.2.min.js"></script>
		<script src="{pigcms{$static_path}whois/js/whois_pr.js"></script>


</form>

</body>
</html>