<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8"/>
	<title>Whois - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}whois/css/reset.css" rel="stylesheet" />
	<link type="text/css" href="{pigcms{$static_path}whois/css/common.css" rel="stylesheet" />
	<link type="text/css" href="{pigcms{$static_path}whois/css/whios_pr.css" rel="stylesheet" />


</head>

<body>
<form method="post" action="{pigcms{:U('Whois/index')}" id="form1" autocomplete="off">
	<div class="aspNetHidden">

		<input type="hidden" name="keyword" id="keyword" value="{pigcms{{$Think.get.keyword}" />
	</div>



	<include file="Whois:whois_nav"/>






	<div class="content">
		<!--whois概况 -->
		<div style="width:980px;margin:0px auto;">

			<div class="c_sec pm fl wx_708" id="gk_whois">
				<ol>
					<li class="t">whois概况</li>
					<li class="wx_522">

						<div id="Repeater1_Panel1_0">

							以下信息缓存于{pigcms{$whoisResult['update_time']|date='Y-m-d H:i:s',###}，点击此处&nbsp;&nbsp;

							<a id="Repeater1_LinkButton1_0" class="c_2f" href="{pigcms{:U('Whois/index',array('keyword'=>$whoisResult['domainName'],'type'=>'now'))}">立即更新</a>&nbsp;&nbsp;whois
						</div>




					</li>
				</ol>
				<div class="se_cont">
					<ul>
						<li class="hi_25"></li>
						<li class="pb_15">
							<span>域名</span>
							<a class="c_2f pl_200 pr_18" href="javascript:void()" title="{pigcms{$whoisResult['domainName']}-Whois历史"
							   target="_self" >{pigcms{$whoisResult.domainName}</a>
							<a href="{pigcms{:U('Procurement/index',array('d'=>$whoisResult['domainName']))}" target="_blank"
							   style="width:60px;"
							   class="btn_fc
							mt_15">代购</a>
						</li>
						<li class="pb_15">
							<span>注册商</span>
							<span class="pl_186 img" style="width:516px;"> {pigcms{$whoisResult.registrar}</span>
						</li>
						<li class="pb_15">
							<span>注册人</span>
							<span class="pl_186 pr_18 img" style="max-width:380px;">  {pigcms{$whoisResult.registrarName} </span>
<!--							<a href="{pigcms{:U('Whois/reverse',array('keyword'=>$whoisResult['domainName'],'type'=>'rsname'))}"-->
<!--							   target="_blank"-->
<!--							   class="btn_fc-->
<!--							mt_15">反查该注册人</a>-->
						</li>
						<li class="pb_15">
							<span>注册人邮箱</span>
							<span class="pl_156 pr_18 img" href="javascript:void();" target="_blank" style="max-width:380px;"> {pigcms{$whoisResult
								.registrarEmail}
							</span>
<!--							<a href="{pigcms{:U('Whois/reverse',array('keyword'=>$whoisResult['domainName'],'type'=>'rsemail'))}"-->
<!--							   target="_blank"-->
<!--							   class="btn_fc mt_15">反查该邮箱</a>-->
						</li>
						<li class="pb_15">
							<span>注册时间</span>
							<span class="pl_170">{pigcms{$whoisResult.creationDate}</span>
						</li>
						<li class="pb_15">
							<span>更新时间</span>
							<span class="pl_170 pr_18">{pigcms{$whoisResult.updatedDate}</span>

						</li>
						<li class="pb_15">
							<span>过期时间</span>
							<span class="pl_170">{pigcms{$whoisResult.expirationDate}</span>
						</li>
						<li class="pb_15 hi_auto mt_40">
							<div class="stu_ym">域名状态</div>
							<div class="names_ym">
								<pre>{pigcms{$whoisResult.domainStatus}</pre>
							</div>
						</li>
						<li class="pb_15 hi_auto mt_40">
							<div class="stu_ym">DNS服务器</div>
							<div  class="names_ym">
								<pre>{pigcms{$whoisResult.nameServer}</pre>

							</div>
						</li>
					</ul>
				</div>
				<div class="se_info_zhuce">
					<a id="zk_info_zhuce">
						<span class="cli_more">点击展开注册信息</span>
						<img src="{pigcms{$static_path}whois/images/round_01.png" >
					</a>
					&nbsp;&nbsp;</div>
				<div class="se_info wx_708" style="display:none;">
					<div class="infos">
						<pre>{pigcms{$whoisResult.all}</pre>
					</div>

				</div>
			</div>




			<div class="c_thi fl">
				<div class="leishi">
					<h3>类似域名注册情况</h3>
					<table id="ls_ym">
					</table>
				</div>
				<!--div class="leishi loer">
					<h3>注册者相关域名</h3>
					<table id="leisitb">
						<tr>
							<td width="33.33%"></td>
							<td width="33.33%" style="text-align:center;">
								<img src="{pigcms{$static_path}whois/images/loading.gif"  style='width:20px;margin-top:15px;'></td>
							<td width="33.33%"></td>
						</tr>
					</table>
				</div-->
			</div>

		</div>

	</div>


	<div class="footer">
		<p><span class="n_m">{pigcms{$config.site_icp} |&nbsp; {pigcms{$config.site_show_footer}</span></p>
	</div>
	<script src="{pigcms{$static_path}js/jquery-1.8.2.min.js"></script>
	<script src="{pigcms{$static_path}whois/js/whois_pr.js"></script>

</form>

<script type="text/javascript">
	(function ($) {
		$.getUrlParam = function (name) {
			var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
			var r = window.location.search.substr(1).match(reg);
			if (r != null) return unescape(r[2]); return null;
		}
	})(jQuery);

	$(document).ready(function () {
		$(".btn_radio[idx]").removeClass("t");
		$(".btn_radio[idx=0]").addClass("t");
		$("input[idx=0]").attr("flag", "true");
		var url = location.href, str = "";
		if (url.match(/.+?(\..+)/) != null) {
			var arr_url = url.split("/");
			str = arr_url[arr_url.length - 1];
			$(".ser_text").val($.getUrlParam('keyword'));
		}
		//数据加底色
		$(".se_cont ul li:odd").addClass("active");
		// 展开历史信息
		$("#zk_info_zhuce").click(function () {
			var src_fi = $(this).find("img").attr("src");
			if (src_fi.indexOf("round_01") > 0) {
				$(".se_info").show("fast");
				$(this).find("img").attr("src", "{pigcms{$static_path}whois/images/round_02.png");
			} else {
				$(".se_info").hide("fast");
				$(this).find("img").attr("src", "{pigcms{$static_path}whois/images/round_01.png");
			}
		});

		$(".btn_ser").click(function () {
			var text_val = $(".ser_text").val();
			if (ser_text.indexOf(".") == "-1") { ser_text = ser_text + ".com" };
			if (text_val && text_val != "") {
				if ($(".ser_text").val() == decodeURI(str)) {
				} else {
					location.href = "404/error.php" + decodeURI(text_val);
				}
			}

		});
		//检查类似的域名
		checkLeishi();
		function checkLeishi() {
			var check_val = $(".ser_text").val();
			var arr_hzs = [".com.cn", ".com", ".cn", ".net", ".org"];
			var str = "";
			if (check_val && check_val.indexOf(".cc") > 0) {
				zsj(str, check_val, arr_hzs);
			} else if (check_val && check_val.indexOf(".cc") == -1) {
				arr_hzs.pop();
				zsj(str, check_val, arr_hzs);
			}
		}
		function zsj(str, check_val, arr_hzs) {
			var arr_vals = [];
			for (var i = 0, j = arr_hzs.length; i < j; i++) {
				arr_vals.push((check_val.split(".")[0] + arr_hzs[i]));
				str += "<tr><td width='50%' class='t_l'><div class='wx-100'><a href='/index.php?c=Whois&a=index&keyword=" + decodeURI(check_val.split(".")[0]) + arr_hzs[i] + "' target='_self' title='" + decodeURI(check_val.split(".")[0]) + arr_hzs[i] + "'>" + decodeURI(check_val.split(".")[0]) + arr_hzs[i] + "</a></div><td class='t_r'><span><img src='./tpl/Domain/default/static/whois/images/loading.gif' style='width:20px;margin-top:15px;'></span></td><td><a target='_blank' href='/index.php?c=Procurement&a=index&d=" + arr_vals[i] + "' class='dg'>代购</a></tr>";
			}
			$("#ls_ym").append(str);
			$("#ls_ym tr:odd").addClass("active");
			// 发送Ajax检查当前域名的注册状态，返回时写上当前状态替换旋转的图片
			$.ajax({
				type:"post",
				url: "{pigcms{:U('Whois/isreg')}",
				data: { "arr_yms": arr_vals.join(",")},
				success: function (response) {
					var res =$.parseJSON(response);
					console.log(res);
					var j = res.length;
					for (var i = 0; i < j; i++) {
						if (res[i][2] == "210") {
							$("#ls_ym td.t_r span")[i].innerHTML = "未注册";
							$("#ls_ym td.t_r span")[i].style.color = "#3394D8";
						}else{
							$("#ls_ym td.t_r span")[i].innerHTML = "已注册";
						}
					}
				},
				error: function (res) {

				}
			});
			/*var arr_srcs = $(".se_cont li span img[alt]");
			$.ajax({
					type: "post",
					url: "/index.php?fdasfas===",
					data: { "ym": check_val,"owner":arr_srcs[1].alt,"email":arr_srcs[2].alt },
					datatype: "json",
					success: function (data) {
						if (data) {
							var res = $.parseJSON(data);
							var html = '';
							for (i in res) {
								if (isNaN(i) == false) {
									html += '<tr>' +
										'<td class="t_l"><div><a href="/index.php?c=Whois&a=index&keyword=' + res[i].domainName + '" target="_self" title="' + res[i].domainName + '">' + res[i].domainName + '</a></div></td><td class="t_r"><a href="/index.php?c=Whois&a=index&keyword=' + res[i].domainName + '" title="whois" target="_blank" class="whois">[whois]</a></td></tr>';

								}
							}

							$("#leisitb").html(html);
							$("#leisitb tr:odd").addClass("active");
						} else {
							$("#leisitb").html("<tr><td colspan='3'>无数据</td></tr>");
						}
					}, error: function (res) {
					}

				}
			);*/


		}
		//没有图片清除按钮
		clearButton();
		function clearButton() {
			var arr_srcs = $(".se_cont li span img[alt='']") || $(".se_cont li span img[alt=DADBB376C2386736]");
			for (var i = 0, j = arr_srcs.length; i < j; i++) {
				var m = $($(arr_srcs[i]).parents()[1]).children().length;
				if (m > 1) {
					$($($(arr_srcs[i]).parents()[1]).children()[2]).remove();

				}
			}
		}
	});
</script>
</body>
</html>
