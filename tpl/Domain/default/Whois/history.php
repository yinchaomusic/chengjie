<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8"/>
	<title>Whois - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}whois/css/reset.css" rel="stylesheet" />
	<link type="text/css" href="{pigcms{$static_path}whois/css/common.css" rel="stylesheet" />
	<link type="text/css" href="{pigcms{$static_path}whois/css/whios_pr.css" rel="stylesheet" />
	<script>
		function dsa() {

		}

	</script>

</head>

<body>
<form method="post" action="{pigcms{:U('Whois/index')}" id="form1" autocomplete="off">
	<div class="aspNetHidden">

		<input type="hidden" name="keyword" id="keyword" value="{pigcms{$Think.get.keyword}" />
	</div>





	<include file="Whois:whois_nav"/>





	<div class="content" id="content">

	<div class="c_sec pm">
		<ol class="wx_949">
			<li class="t">Whois历史</li>
			<li class="wx_720"><if condition="$from_updatedDate[0]['domainName']">{pigcms{$from_updatedDate.0.domainName}<else /> {pigcms{$Think.get.keyword}</if></li>
		</ol>


		<table class="tab_fi">
			<tbody><tr>
				<th class="pl-30" width="213px">注册商</th>
				<th width="114px">注册人</th>
				<th width="120px">注册人邮箱</th>
				<th width="90px">注册日期</th>
				<th width="90px">过期日期</th>
				<th width="90px">记录时间</th>
				<th width="50px" class="pl-10">&nbsp;&nbsp;&nbsp;&nbsp;查看</th>
			</tr>
			<if condition="is_array($from_updatedDate)">
					<volist name="from_updatedDate" id="vo">
							<tr class="active">
								<td class="pl-30"><span class="wx_213" title="{pigcms{$vo.registrar}">{pigcms{$vo.registrar}</span></td>
								<td><span class="wx_114" title={pigcms{$vo.registrarName}">{pigcms{$vo.registrarName}</span></td>
								<td class="c_2f"><span class=" wx_120" title="{pigcms{$vo.registrarEmail}">{pigcms{$vo.registrarEmail}</span></td>
								<td>{pigcms{$vo.creationDate|substr=###,0,10}</td>
								<td>{pigcms{$vo.expirationDate|substr=###,0,10}</td>
								<td>{pigcms{$vo.update_time|date='Y-m-d',###}</td>
								<td class="pl-10"><input type="button" value="查看" ind="{pigcms{$vo.cache_id}" class="btn_ck"> </td>
							</tr>
					</volist>
				<else/>
				<tr class="active">
					<td colspan="8" style="text-align:center;">
						<span id="Repeater1_lblEmpty" style="color:Red;font-size:Large;font-weight:bold;">暂无数据!</span>
					</td></tr>
				</if>


			<tr class="active">
				<td colspan="8" style="text-align:center;">

				</td></tr>
			</tbody></table>


		<div>


		</div>

		<table class="tab_sec n_b" id="history_duibi" style="display:none">

		</table>


		<div id="info_history" style="display:block;">

		</div>
	</div>


	<div class="footer">
			<p><span class="n_m">{pigcms{$config.site_icp} |&nbsp; {pigcms{$config.site_show_footer}</span></p>
	</div>
	<script src="{pigcms{$static_path}js/jquery-1.8.2.min.js"></script>
	<script src="{pigcms{$static_path}whois/js/whois_pr.js"></script>
		<script src="{pigcms{$static_path}whois/js/history.js"></script>

</form>

</body>
</html>