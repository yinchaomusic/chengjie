<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">

	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">

</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>
<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>域名融资</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="g-content u-cls">
	<div class="m-content">



		<form action="" method="post" novalidate="true">
			<section>
				<div class="container nrye">
					<div class="row">



						<div class="col-sm-2">
							<!--sidebar-menu-->
							<div id="sidebar">
								<a href="#" class="submenu_bar  visible-xs-block">
									<ol class="breadcrumb">
										<li><i class="iconfont submenu_d"></i></li>
										<li>首页</li>
										<li class="active">我的账户</li>
									</ol>
								</a>
								<ul style="display: block;">
									<li class="submenu active"  ><a href="{pigcms{:U('Financing/index')}"><i class="iconfont">&#xe613;
											</i><span>我的账户</span></a> </li>
									<li class="submenu  ">
										<a href="{pigcms{:U('Investment/lend')}"><i class="iconfont "></i>
											<span>我要理财</span><span class="label  visible-xs-block">3</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Investment/lend')}">我要借出</a></li>
											<li ><a href="{pigcms{:U('Investment/lendList')}">借出列表</a></li>
											<li><a href="{pigcms{:U('Investment/automatic')}">自动投标</a></li>
											<li ><a href="{pigcms{:U('Investment/keepInvest')}">回款续投</a></li>
										</ul>
									</li>
									<li class="submenu "><a href="{pigcms{:U('Loan/borrow')}"><i class="iconfont "></i><span>我要借款</span><span class="label  visible-xs-block">3</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Loan/borrow')}">我要借入</a></li>
											<li ><a href="{pigcms{:U('Loan/borrowList')}">借入列表</a></li>
											<li ><a href="{pigcms{:U('Loan/pledge')}">质押管理</a></li>
										</ul>
									</li>
									<li class="submenu "><a href="#"><i class="iconfont "></i><span>资金管理</span><span class="label  visible-xs-block">6</span></a>
										<ul>
											<li><a href="{pigcms{:U('Account/Account')}">账户充值</a></li>
											<li><a href="{pigcms{:U('Account/withdrawal')}">账户提现</a></li>
											<li><a href="{pigcms{:U('Account/withdrawal')}">提现银行</a></li>
											<li><a href="{pigcms{:U('Account/record')}">资金记录</a></li>
											<li><a href="{pigcms{:U('Fund/freezes')}">冻结记录</a></li>
										</ul>
									</li>
									<li class="submenu "><a href="#"><i class="iconfont "></i><span>账户管理</span><span class="label  visible-xs-block">7</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Profile/update')}">个人信息</a></li>
											<li><a href="{pigcms{:U('Profile/avatar')}">上传头像</a></li>
											<li><a href="{pigcms{:U('Profile/IdCard')}">身份认证</a></li>
											<li><a href="{pigcms{:U('Account/updatepwd')}">登录密码</a></li>
											<li><a href="{pigcms{:U('Profile/transactionPassword')}">交易密码</a></li>
											<li><a href="{pigcms{:U('Account/bind')}">绑定手机</a></li>
											<!--											<li><a href="/Member/InfoCenter">站内信</a></li>-->
										</ul>
									</li>
								</ul>
							</div>
							<!--sidebar-menu end-->
						</div>

						<div class="col-sm-10">
							<div class="container_page  padding-sm-none">
								<section>
									<h2>最近一个月内还需还款记录</h2><p>提醒：良好的还款记录会增加您的信用，提高下次借款成功率</p>
									<div class="tab_table page_min_h">
										<ul class="nav nav-tabs">
											<li role="presentation" <if condition="$state eq 0"> class="active"</if>><a href="{pigcms{:U('Loan/paymentHistory')}">最近需还款</a></li>
											<li role="presentation" <if condition="$state eq 1"> class="active"</if>><a href="{pigcms{:U('Loan/paymentHistory',array('state'=>1))}">已逾期</a></li>
<!--											<li role="presentation" <if condition="$state eq 2"> class="active"</if>><a href="{pigcms{:U('Loan/paymentHistory',array('state'=>2))}">已还清</a></li>-->
<!--											<li role="presentation" <if condition="$state eq 3"> class="active"</if>><a href="{pigcms{:U('Loan/paymentHistory',array('state'=>3))}">还款结束</a></li>-->
<!--											<li role="presentation" <if condition="$state eq 4"> class="active"</if>><a href="{pigcms{:U('Loan/paymentHistory',array('state'=>4))}">已过期</a></li>-->
										</ul>
										<div class="table-responsive">
											<table class=" table table-bordered table-striped table-hover "  style="font-size: 13px;">
												<thead>
												<tr>
													<th>借入信息</th>
													<th>状态</th>
													<th>应还款日期</th>
													<th>还款日期</th>
													<th>期数</th>
													<th>总还本息/管理费</th>
													<th>已还本息/管理费</th>
													<th>操作</th>
												</tr>
												</thead>
												<tbody class="text-center" id="nowborrowslist">
												<if condition="is_array($now_borrows)">
													<volist name="now_borrows" id="vo">
														<tr id ="delbid_{pigcms{$vo.bid}">
															<td><a href="{pigcms{:U('Investment/loanScheme',array('dkid'=>$vo['bid']))}" target="_blank">
																{pigcms{$vo.Amountdx}元
																￥ {pigcms{$vo.Amount|number_format}
																</a> <br />
																贷款号：{pigcms{$vo.did} <br/>
																贷款人：{pigcms{$vo.dname}<br />
																年利率:{pigcms{$vo.Rate}% <br/>
																<if condition="$yuqi">
																	<font color="red">已经逾期 {pigcms{$vo.surplus_date}天</font><br/>
																	<font color="#f4a460">违约金：￥{pigcms{$vo.breachMoney}</font><br/>
	<a href="javascript:void(0);" id="detail_{pigcms{$vo.bid}" onclick="showdetail('{pigcms{$vo.bid}');return false;">点击我查看违约信息</a><br/>
																<else/>
																	还差<font color="red">{pigcms{$vo.surplus_date}</font>天逾期<br/>
																</if>


																总利息：￥{pigcms{$vo.zlx} <br />
																总管理费：￥{pigcms{$vo.zglf}<br />
																每月还款：￥{pigcms{$vo.myfk} <br />
																(利息：￥{pigcms{$vo.lx} ,<br />
																管理费：￥{pigcms{$vo.myglf} )<br />
																最后本金需还款：￥{pigcms{$vo.zhhk} <br />
																还款方式：每月还息
															</td>
															<td>
																<if condition="$vo['status'] eq 1"><span class="font_success">正在投标</span><elseif condition="$vo['status'] eq 2"/><span class="font_default">等待还款</span><elseif condition="$vo['status'] eq 3"/><span class="font_default">还款结束</span><elseif condition="$vo['status'] eq 4"/><span class="font_default">已过期</span><elseif condition="$vo['status'] eq 5"/><span class="font_default">等待审核</span><elseif condition="$vo['status'] eq 6"/><span class="font_success">审核通过</span><elseif condition="$vo['status'] eq 7"/><span class="font_warning"><a href="javascript:void(0);" title="{pigcms{$vo.intro}">申请拒绝</a></span></if>
															</td>
															<td><if condition="$vo['repayDate']">{pigcms{$vo.repayDate}</if> </td>
															<td>{pigcms{$vo.ExpiredAt}</td>
															<td><if condition="$vo['LoanCycleDay'] neq 0">{pigcms{$vo.LoanCycleDay}天<elseif condition="$vo['LoanCycleMonth'] neq 0"/>{pigcms{$vo.LoanCycleMonth} 个月</if> </td>
															<td>￥{pigcms{$vo['Amount'] + $vo['zlx']}/￥{pigcms{$vo.zglf}</td>
															<td>￥{pigcms{$vo.yh_menory}/￥{pigcms{$vo.yh_zglfees}</td>
															<td>
																<if condition="$vo['status'] eq 1">
																	正在投标
																	<elseif condition="$vo['status'] eq 3"/>
																	还款结束
																	<elseif condition="$vo['status'] eq 4"/>
																	已过期
																	<elseif condition="$vo['status'] eq 2"/>
																	<a href="javascript:void(0);" id="nc_{pigcms{$vo.bid}" onclick="borrowoption('{pigcms{$vo.bid}',2);return false;">还款</a>
																</if>

															</td>
														</tr>
													</volist>
													<else/>
													<tr>
														<td colspan="8">暂无记录</td>
													</tr>
												</if>
												</tbody>
											</table>
										</div>
									</div>




								</section>
							</div>
						</div>


					</div>
				</div>
			</section>
		</form>



	</div>
</div>
<include file="Public:footer"/>
<style>
	.customClassset{
		width: 960px;
		text-align: center;
	}
</style>
<script src="{pigcms{$static_path}js/unslider.js"></script>
<script src="{pigcms{$static_path}js/bootstrap.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/rz_common.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/matrix.js"></script>
<script type="text/javascript">

	function showdetail(id){
		swal({   title: "玩命加载中...",   text: "客官，请耐心等待",      showConfirmButton: false });
		var submit_getdata = "{pigcms{:U('Loan/getBreachs')}";
		$.post(submit_getdata, {'id': id}, function (result) {
			//result = $.parseJSON(result);
			if (result) {
				if (result.error == 0) {
					swal({
						title: result.title,
						confirmButtonColor: "#FFA200",
						confirmButtonText: "关闭",
						customClass:"customClassset",
						text: result.htmldata,
						html:true
					});
				} else {
					swal(result.msg);
					return false;
				}
			} else {
				swal(result.msg);
				return false;
			}
		});

	}

	var submit_check="{pigcms{:U('Loan/month_borrowo')}";
	function borrowoption(id,type) {
		var title,text,configbutton;
		/*if(type ==1){
			title = "你确定要删除么？";
			text =  "确认则删除该贷款记录信息";
			configbutton = "删除";
		}else */
		if(type == 2){
			title = "我要还款";
			text = "请确认账号有足够的资金。如果提前还款，良好的还款记录会增加您的信用，提高下次借款成功率。"
			configbutton = "是的，我要还款"
		}

		swal({
			title: title,
			text: text,
			type: "info",
			showCancelButton: true,
			cancelButtonText:"取消",
			confirmButtonText:configbutton,
			closeOnConfirm: false,
			showLoaderOnConfirm: true,
		}, function(){
			//setTimeout(function(){
			var nowcheckdomain = $('#nc_'+id);
			nowcheckdomain.html("<img src='{pigcms{$static_path}images/index/loading.gif' width='20' height='20' />");
			$.post(submit_check, {'id': id,'type':type}, function (result) {
				result = $.parseJSON(result);
				if (result) {
					if (result.error == 0) {
						//nowcheckdomain.parent().parent().remove();
						swal('',result.msg,'success');
						window.location = window.location;

					} else {
						swal('',result.msg,'error');
						window.location = window.location;
					}
				} else {
					swal('',result.msg,'error');
					window.location = window.location;
				}
			});
			//}, 2000);
		});
	}

</script>
</body>
</html>