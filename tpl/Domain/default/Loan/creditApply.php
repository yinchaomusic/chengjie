<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">


</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>
<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>域名融资</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="g-content u-cls">
	<div class="m-content">



		    <section>
				<div class="container nrye">
					<div class="row">



						<div class="col-sm-2">
							<!--sidebar-menu-->
							<div id="sidebar">
								<a href="#" class="submenu_bar  visible-xs-block">
									<ol class="breadcrumb">
										<li><i class="iconfont submenu_d"></i></li>
										<li>首页</li>
										<li class="active">我的账户</li>
									</ol>
								</a>
								<ul style="display: block;"><li class="submenu"><a href="{pigcms{:U('Financing/index')}"><i class="iconfont">&#xe613;</i><span>我的账户</span></a> </li>
									<li class="submenu  ">
										<a href="{pigcms{:U('Investment/lend')}"><i class="iconfont "></i>
											<span>我要理财</span><span class="label  visible-xs-block">3</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Investment/lend')}">我要借出</a></li>
											<li ><a href="{pigcms{:U('Investment/lendList')}">借出列表</a></li>
											<li><a href="{pigcms{:U('Investment/automatic')}">自动投标</a></li>
											<li ><a href="{pigcms{:U('Investment/keepInvest')}">回款续投</a></li>
										</ul>
									</li>
									<li class="submenu active open"><a href="{pigcms{:U('Loan/borrow')}"><i class="iconfont "></i><span>我要借款</span><span class="label  visible-xs-block">3</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Loan/borrow')}">我要借入</a></li>
											<li><a href="{pigcms{:U('Loan/borrowList')}">借入列表</a></li>
											<li class="active"><a href="{pigcms{:U('Loan/pledge')}">质押管理</a></li>
										</ul>
									</li>
									<li class="submenu "><a href="#"><i class="iconfont "></i><span>资金管理</span><span class="label  visible-xs-block">6</span></a>
										<ul>
											<li><a href="{pigcms{:U('Account/Account')}">账户充值</a></li>
											<li><a href="{pigcms{:U('Account/withdrawal')}">账户提现</a></li>
											<li><a href="{pigcms{:U('Account/withdrawal')}">提现银行</a></li>
											<li><a href="{pigcms{:U('Account/record')}">资金记录</a></li>
											<li><a href="{pigcms{:U('Fund/freezes')}">冻结记录</a></li>
										</ul>
									</li>
									<li class="submenu "><a href="#"><i class="iconfont "></i><span>账户管理</span><span class="label  visible-xs-block">7</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Profile/update')}">个人信息</a></li>
											<li><a href="{pigcms{:U('Profile/avatar')}">上传头像</a></li>
											<li><a href="{pigcms{:U('Profile/IdCard')}">身份认证</a></li>
											<li><a href="{pigcms{:U('Account/updatepwd')}">登录密码</a></li>
											<li><a href="{pigcms{:U('Profile/transactionPassword')}">交易密码</a></li>
											<li><a href="{pigcms{:U('Account/bind')}">绑定手机</a></li>
											<!--											<li><a href="/Member/InfoCenter">站内信</a></li>-->
										</ul>
									</li>
								</ul>
							</div>
							<!--sidebar-menu end-->
						</div>
						<form action="{pigcms{:U('Loan/creditApply')}" method="post"  >
						<div class="col-sm-10 top_layer">
							<div class="container_page user_page_area ">
								<section>
									<h2>申请授信</h2>
									<p>质押是指借入者将持有域名过户至域名融资平台，包括域名的管理权和所有权，以获取授信额度申请贷款。</p>
									<if condition="is_array($okArr)">
										<p>

										<div class="m-account-data1 u_be9 s-bg-fc u_hi408" style="width: auto;">
											<h4 class="s-bg-white">
												<a class="s-3c m-ta2">
													<span class="sp1">提交成功的域名</span>
													<span class="sp2"><b></b>{pigcms{:count($okArr)}</span>

												</a>
												<div class="u-cls"></div>
											</h4>
											<table>
												<tbody><tr>
													<th width="16px"></th>
													<th width="240px" class="u_tl">域名</th>
													<th width="160px"></th>
												</tr>
												<volist name="okArr" id="vo">
													<tr>
														<td class="u_nb"></td>
														<td class="u_nb"><a class="u-txa3">{pigcms{$vo}</a></td>
														<td class="u_nb"></td>
													</tr>
												</volist>
												</tbody></table>
										</div>

										</p>
									</if>
									<if condition="is_array($errorArr)">
										<p>

												<div class="m-account-data1 u_be9 s-bg-fc u_hi408" style="width: auto;">
													<h4 class="s-bg-white">
														<a class="s-3c m-ta2">
															<span class="sp1">提交失败的域名</span>
															<span class="sp2"><b></b>{pigcms{:count($errorArr)}</span>

														</a>
														<div class="u-cls"></div>
													</h4>
													<table>
														<tbody><tr>
															<th width="16px"></th>
															<th width="240px" class="u_tl">域名</th>
															<th width="160px"></th>
															<th width="150px" class="u_tl">失败原因</th>
															<th width="16px"></th>
														</tr>
														<volist name="errorArr" id="vo">
														<tr>
															<td class="u_nb"></td>
															<td class="u_nb"><a class="u-txa3">{pigcms{$vo.domain}</a></td>
															<td class="u_nb"></td>
															<td class="u_nb"><span class="u-span12">{pigcms{$vo.msg}</span></td>
															<td class="u_nb"></td>
														</tr>
														</volist>
														</tbody></table>
												</div>

										</p>
									<else/>
										<div class="m-processList2 u_be9" style="width:auto;">
											<div class="txt1">添加域名</div>
											<div class="txt2">平台进行审核</div>
											<div class="txt3">
												<span class="u-span9">进行估价</span>
											</div>
											<div class="txt4">
												<span class="u-span9">域名转移</span>
											</div>
											<div class="txt5">
												<span class="u-span9">质押完成</span>
											</div>
											<div class="txt6">
												<span class="u-span9">授信额度</span>
											</div>
										</div>

									</if>

									<div class="m-addDomains u_be9 s-bg-fc" style="width:auto;">
										<p class="f-p26"></p>
										<p class="f-p-t9">域名列表：</p>
										<p class="f-p27" style="border: 1px solid rgb(235, 235, 235);">
											<textarea class="txta_v" ph="域名一行一个，请输入顶级域名" name="domain" id="domain" style="color: rgb(216, 216, 216);"></textarea>
										</p>
<!--										<p class="f-p-t9">备注信息：</p>-->
<!--										<p   style="border: 1px solid rgb(235, 235, 235);    margin: 0px auto 19px auto;height: 50px;width: 654px;">-->
<!--											<textarea class="txta_v"  ph="这里输入您的备注" name="intro" style="color: rgb(216, 216, 216);height: 50px;width: 654px;padding: 10px;" maxlength="255"></textarea>-->
<!--										</p>-->
										<div class="m-addoper">
											<div class="u-fl addoper-l"> </div>
											<div class="u-fl addoper-r">
												<p class="u_mtb30">
													<input type="submit" id="check_domain" value="提交" class="u-btn9 btn_tj">
												</p>
											</div>
											<div class="u-cls"></div>
										</div>
									</div>

								</section>
							</div>
						</div>
						</form>

					</div>
				</div>
			</section>




	</div>
</div>
<include file="Public:footer"/>

<script src="{pigcms{$static_path}js/unslider.js"></script>
<script src="{pigcms{$static_path}js/bootstrap.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/rz_common.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/matrix.js"></script>

<script>
	$(function () {
		$('#check_domain').click(function(){
			//alert($('#domain').val());
			if($('#domain').val() == '' || $('#domain').val() == $('#domain').attr('ph')){
				swal('','必须填写至少一个域名','error');
				return false;
			}
		})
	})
</script>


</body>
</html>