<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">


</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>
<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>域名融资</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="g-content u-cls">
	<div class="m-content">



		   <section>
				<div class="container nrye">
					<div class="row">



						<div class="col-sm-2">
							<!--sidebar-menu-->
							<div id="sidebar">
								<a href="#" class="submenu_bar  visible-xs-block">
									<ol class="breadcrumb">
										<li><i class="iconfont submenu_d"></i></li>
										<li>首页</li>
										<li class="active">我的账户</li>
									</ol>
								</a>
								<ul style="display: block;"><li class="submenu"><a href="{pigcms{:U('Financing/index')}"><i class="iconfont">&#xe613;</i><span>我的账户</span></a> </li>
									<li class="submenu  ">
										<a href="{pigcms{:U('Investment/lend')}"><i class="iconfont "></i>
											<span>我要理财</span><span class="label  visible-xs-block">3</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Investment/lend')}">我要借出</a></li>
											<li ><a href="{pigcms{:U('Investment/lendList')}">借出列表</a></li>
											<li><a href="{pigcms{:U('Investment/automatic')}">自动投标</a></li>
											<li ><a href="{pigcms{:U('Investment/keepInvest')}">回款续投</a></li>
										</ul>
									</li>
									<li class="submenu active open"><a href="{pigcms{:U('Loan/borrow')}"><i class="iconfont "></i><span>我要借款</span><span class="label  visible-xs-block">3</span></a>
										<ul>
											<li class="active"><a href="{pigcms{:U('Loan/borrow')}">我要借入</a></li>
											<li><a href="{pigcms{:U('Loan/borrowList')}">借入列表</a></li>
											<li><a href="{pigcms{:U('Loan/pledge')}">质押管理</a></li>
										</ul>
									</li>
									<li class="submenu "><a href="#"><i class="iconfont "></i><span>资金管理</span><span class="label  visible-xs-block">6</span></a>
										<ul>
											<li><a href="{pigcms{:U('Account/Account')}">账户充值</a></li>
											<li><a href="{pigcms{:U('Account/withdrawal')}">账户提现</a></li>
											<li><a href="{pigcms{:U('Account/withdrawal')}">提现银行</a></li>
											<li><a href="{pigcms{:U('Account/record')}">资金记录</a></li>
											<li><a href="{pigcms{:U('Fund/freezes')}">冻结记录</a></li>
										</ul>
									</li>
									<li class="submenu "><a href="#"><i class="iconfont "></i><span>账户管理</span><span class="label  visible-xs-block">7</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Profile/update')}">个人信息</a></li>
											<li><a href="{pigcms{:U('Profile/avatar')}">上传头像</a></li>
											<li><a href="{pigcms{:U('Profile/IdCard')}">身份认证</a></li>
											<li><a href="{pigcms{:U('Account/updatepwd')}">登录密码</a></li>
											<li><a href="{pigcms{:U('Profile/transactionPassword')}">交易密码</a></li>
											<li><a href="{pigcms{:U('Account/bind')}">绑定手机</a></li>
											<!--											<li><a href="/Member/InfoCenter">站内信</a></li>-->
										</ul>
									</li>
								</ul>
							</div>
							<!--sidebar-menu end-->
						</div>

						<!-- {pigcms{:U('Loan/borrow')} --->
						<form  id="BorrowForm" action="{pigcms{:U('Loan/borrow')}" method="post" novalidate="true">
						<div class="col-sm-10 ">
							<div class="container_page wyjr_area">
								<section>
									<h2>我要借入</h2>
									<p class="info-box" <if condition="$result_idcard['status'] eq 1"> style="display: none;" </if> >
										您还没有质押域名获取授信额度，不能申请贷款。 <a href="{pigcms{:U('Loan/pledge')}"><strong>马上质押</strong></a>
									</p>
									<div class="form-horizontal form-wyjr">
										<div class="bg_grey clearfix">
											<div id="result" class="m160">
												<div class="content">
													<span class="popover-close">&#xe620;</span>
													<ul >
														<li>总利息：<span id="zlx" class="font_c_orange"></span>
															总管理费：<span id="zglf" class="font_c_orange"></span></li>
														<li>每月还款：<span id="myfk" class="font_c_orange"></span> (利息：<span id="lx" class="font_c_orange"></span>,管理费：<span id="myglf" class="font_c_orange"></span>)</li>
														<li>最后本金需还款：<span id="zhhk" class="font_c_orange"></span></li>
													</ul>
												</div>
											</div>

											<div class="form-group" style="margin-bottom: 10px;">
												<label for="input6" class=" control-label col-sm-1"><i>*</i>质押物：</label>
												<div class="col-sm-8">
													<div class="select_list_area" style="height: 104px">
														<ul style="list-style: none;margin: 0;">
															<volist name="pledgelist" id="vo">
														<li  ><label  ><input  type="checkbox" value="{pigcms{$vo.pid}" data-amount="{pigcms{$vo.credit_total}" name="pids[]" >{pigcms{$vo.domain} - 授信额度:￥{pigcms{$vo.credit_total}</label></li>
															</volist>

														</ul>
													</div>
												</div>
												<span class="form-control-tips " style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
											</div>
											<div class="form-group <if condition='is_array($pledgelist)'> <else />hide</if>">
												<label for="sall" class="sall_area">
													<input name="sall" id="sall" type="checkbox" />
													<span>选择全部</span>
												</label>

											</div>
											<div class="form-group">
												<label for="Amount" class=" control-label col-sm-1"><i>*</i>借款总额：</label>
												<div class="col-sm-2 Amount_area">
													<input class="form-control" id="Amount" name="Amount" type="number" value="" />
													<span id="Amountdx"></span>
												</div>
												<p class="form-control-static">
													元
												借款总额不能超过可用额度：<span class="amount-pay-out">￥<i id="Aquota">{pigcms{$pledges.credit_now}</i></span>
												</p>
												<span class="form-control-tips " style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
											</div>
											<div class="form-group">
												<label for="input6" class=" control-label col-sm-1 "><span class="select_l"><i>*</i>借款周期：</span></label>
												<div class="col-sm-6 select-inline">
													<label class="radio-inline">
														<input type="radio" name="LoanCycleDay" id="LoanCycleDay1" value="7">7天
													</label>
													<label class="radio-inline">
														<input type="radio" name="LoanCycleDay" id="LoanCycleDay2" value="15">15天
													</label>
													<label class="radio-inline">
														<input type="radio" name="LoanCycleDay" id="LoanCycleDay3" value="0" checked>&nbsp;
													</label>
													<select name="LoanCycleMonth" class="form-control" id="LoanCycleMonth">
														<option value="1">1个月</option>
														<option value="2">2个月</option>
														<option value="3">3个月</option>
														<option value="4">4个月</option>
														<option value="5">5个月</option>
														<option value="6">6个月</option>
														<option value="7">7个月</option>
														<option value="8">8个月</option>
														<option value="9">9个月</option>
														<option value="10">10个月</option>
														<option value="11">11个月</option>
														<option value="12">12个月</option>
													</select>
												</div>
												<span class="form-control-tips "><i class="iconfont">&#xe60a;</i>提示文字</span>
											</div>
											<div class="form-group">
												<label for="Rate" class=" control-label col-sm-1"><i>*</i>年利率：</label>
												<div class="col-sm-2">
													<input class="form-control Decimal" id="Rate" name="Rate" type="number" value="" />
													<span id="autoRate" title="autoRate" class="iconfont">&#xe635;</span>
												</div>
												<p class="form-control-static">%    年利息不能超多25%</p>
												<span class="form-control-tips"><i class="iconfont">&#xe60a;</i><span class="error-text"></span></span>
											</div>
											<div class="form-group">
												<label for="input6" class=" control-label col-sm-1 "><i>*</i>还款方式：</label>
												<div class="col-sm-5 select-inline">
													<label class="radio-inline">
														<input type="radio" name="inlineOptions" checked id="inlineRadio2" value="1">
														每月还息
													</label>
												</div>
												<span class="form-control-tips " style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
											</div>
											<div class="form-group">
												<label for="ExpiredAt" class=" control-label col-sm-1"><i>*</i>截止日期：</label>
												<div class="col-sm-2">
													<input class="form-control" id="ExpiredAt" name="ExpiredAt" type="date" value="" />
												</div>
												<p class="form-control-static">如果超过截止日期仍未完成100%投标，则自动取消该笔借款</p>
												<span class="form-control-tips" style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
											</div>
										</div>
										<div class="pad">
											<div class="form-group">
												<label for="Description" class=" control-label col-sm-1">借款描述：</label>
												<div class="col-sm-9">
                                            <textarea class="form-control" cols="20" id="Description" name="Description" rows="6"></textarea>
												</div>
											</div>
											<div class="btn_area">
												<input type="submit" id="LoanApply" class="btn btn-warning btn-xs-block" value="申请贷款">
											</div>
										</div>
									</div>
								</section>

							</div>
						</div>
						</form>
					</div>
				</div>
			</section>




	</div>
</div>
<include file="Public:footer"/>

<script src="{pigcms{$static_path}js/unslider.js"></script>
<script src="{pigcms{$static_path}js/bootstrap.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/rz_common.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/matrix.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/reckon.js"></script>

<script type="text/javascript">
	$(function () {
		if ($Mbrowser.versions.ios || $Mbrowser.versions.XiaoMi || ($Mbrowser.versions.mobile && $Mbrowser.versions.Chrome)) {
			/* console.log("语言版本: " + $Mbrowser.language);
			 console.log(" 是否为移动终端: " + $Mbrowser.versions.mobile);
			 console.log(" ios终端: " + $Mbrowser.versions.ios);
			 console.log(" android终端: " + $Mbrowser.versions.android);
			 console.log(" 是否为iPhone: " + $Mbrowser.versions.iPhone);
			 console.log(" 是否iPad: " + $Mbrowser.versions.iPad);
			 console.log(navigator.userAgent);

			 alert("支持")                     */

		} else {
			/*alert("js日历控件")*/
			$("#ExpiredAt").attr("type", "text");
			laydate({
				elem: '#ExpiredAt',
				min:laydate.now(+1),
				festival: true

			});
		};

		var formSubmitSuccess;
		var formSubmitBefore;
		var sendCommandSuccess;
		var sendCommandBefore;
		var getSuccess;
		var getBefore;
		$('#BorrowForm').submit(function () {
			var $form = $("form");
			var url = $form.attr("action");
			var submitButtons = $form.find(":submit");
			var texts = $form.find(":text,input[type=date],input[type=number],input[type=tel],input[type=email]");
			var textareas = $form.find("textarea");
			var passwords = $form.find(":password");
			var radios = $form.find(":radio:checked");
			var checkboxs = $form.find(":checkbox:checked");
			var selects = $form.find("select");

			var data = {};


			//texts
			var textItems = {};
			for (var i = 0; i < texts.length; i++) {
				var name = texts.eq(i).attr("name");
				var values = textItems[name];
				if (!values) {
					values = [];
				}
				values.push(texts.eq(i).val());
				textItems[name] = values;
			}
			for (var item in textItems) {
				data[item] = textItems[item].join(',');
			}
			//textareas
			for (var i = 0; i < textareas.length; i++) {
				data[textareas.eq(i).attr("name")] = textareas.eq(i).val();
			}
			//passwords
			for (var i = 0; i < passwords.length; i++) {
				data[passwords.eq(i).attr("name")] = passwords.eq(i).val();
			}
			//radios
			for (var i = 0; i < radios.length; i++) {
				data[radios.eq(i).attr("name")] = radios.eq(i).val();
			}
			//selects
			for (var i = 0; i < selects.length; i++) {
				var selected = selects.eq(i).find(":selected");
				var selectedValue = [];
				for (var j = 0; j < selected.length; j++) {
					selectedValue.push(selected.eq(j).val());
				}
				data[selects.eq(i).attr("name")] = selectedValue.join(",");
			}
			//checkboxs
			var checkboxItems = {};
			for (var i = 0; i < checkboxs.length; i++) {
				var name = checkboxs.eq(i).attr("name");
				var values = checkboxItems[name];
				if (!values) {
					values = [];
				}
				values.push(checkboxs.eq(i).val());
				checkboxItems[name] = values;
			}
			for (var item in checkboxItems) {
				data[item] = checkboxItems[item].join(',');
			}

			if (formSubmitBefore) {
				var isBrack = formSubmitBefore(url, data);
				if (!isBrack)
					return false;
			}

			 data['zlx']    = $("#zlx").text();//总利息
			 data['zglf']   = $("#zglf").text();//总管理费
			 data['myfk']   = $("#myfk").text();//每月还款
			 data['lx']     = $("#lx").text();//每月利息
			 data['myglf']  = $("#myglf").text();//每月管理费
			 data['zhhk']   = $("#zhhk").text();//最后本金需还款
			 data['Amountdx']   = $("#Amountdx").text();//贷款中文数字
			if(  $("#zlx").text() == ''
				|| $("#zglf").text() ==''
				||$("#myfk").text()==''
				|| $("#lx").text() ==''
				|| $("#myglf").text() ==''
				|| $("#zhhk").text() =='' )
			{
				swal("错误！","请认真填写借款所需要的信息",'error');
				submitButtons.removeAttr("disabled");
				return false;
			}
			if($('#Amount').val() == ''){
				swal("错误！","请认真填写借款总额",'error');
				submitButtons.removeAttr("disabled");
				return false;
			}
			if($('#Rate').val() == ''){
				swal("错误！","请认真填写年利率",'error');
				submitButtons.removeAttr("disabled");
				return false;
			}

			submitButtons.attr("disabled", "true");
			$.post(url,
				data,
				function (result) {

					result = $.parseJSON(result);

					if (result) {

						if (result.error == -1) {
							swal(result.title,result.msg,'error');
							submitButtons.removeAttr("disabled");
							return;
						}
						if(result.error == 0){
							//swal(result.title,result.msg,'success');
							swal({   title: result.title,   text: result.msg, timer:3000,  imageUrl: "images/thumbs-up.jpg",showCancelButton: false,confirmButtonColor: "#FFA200",confirmButtonText:"OK" }, function (isConfirm) {
								window.location = window.location;
							});
							submitButtons.removeAttr("disabled");
						}
					}

					if (formSubmitSuccess) {
						formSubmitSuccess($form, result);
					} else {
						//window.location = window.location;
					}
				});
			return false;
		});


	});

</script>


</body>
</html>