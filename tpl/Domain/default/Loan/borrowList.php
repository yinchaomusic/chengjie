<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">

	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">

</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>
<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>域名融资</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="g-content u-cls">
	<div class="m-content">



		<form action="" method="post" novalidate="true">
			<section>
				<div class="container nrye">
					<div class="row">



						<div class="col-sm-2">
							<!--sidebar-menu-->
							<div id="sidebar">
								<a href="#" class="submenu_bar  visible-xs-block">
									<ol class="breadcrumb">
										<li><i class="iconfont submenu_d"></i></li>
										<li>首页</li>
										<li class="active">我的账户</li>
									</ol>
								</a>
								<ul style="display: block;">
							<li class="submenu"><a href="{pigcms{:U('Financing/index')}"><i class="iconfont">&#xe613;</i><span>我的账户</span></a> </li>
									<li class="submenu  ">
										<a href="{pigcms{:U('Investment/lend')}"><i class="iconfont "></i>
											<span>我要理财</span><span class="label  visible-xs-block">3</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Investment/lend')}">我要借出</a></li>
											<li ><a href="{pigcms{:U('Investment/lendList')}">借出列表</a></li>
											<li><a href="{pigcms{:U('Investment/automatic')}">自动投标</a></li>
											<li ><a href="{pigcms{:U('Investment/keepInvest')}">回款续投</a></li>
										</ul>
									</li>
									<li class="submenu active open"><a href="{pigcms{:U('Loan/borrow')}"><i class="iconfont "></i><span>我要借款</span><span class="label  visible-xs-block">3</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Loan/borrow')}">我要借入</a></li>
											<li class="active"><a href="{pigcms{:U('Loan/borrowList')}">借入列表</a></li>
											<li ><a href="{pigcms{:U('Loan/pledge')}">质押管理</a></li>
										</ul>
									</li>
									<li class="submenu "><a href="#"><i class="iconfont "></i><span>资金管理</span><span class="label  visible-xs-block">6</span></a>
										<ul>
											<li><a href="{pigcms{:U('Account/Account')}">账户充值</a></li>
											<li><a href="{pigcms{:U('Account/withdrawal')}">账户提现</a></li>
											<li><a href="{pigcms{:U('Account/withdrawal')}">提现银行</a></li>
											<li><a href="{pigcms{:U('Account/record')}">资金记录</a></li>
											<li><a href="{pigcms{:U('Fund/freezes')}">冻结记录</a></li>
										</ul>
									</li>
									<li class="submenu "><a href="#"><i class="iconfont "></i><span>账户管理</span><span class="label  visible-xs-block">7</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Profile/update')}">个人信息</a></li>
											<li><a href="{pigcms{:U('Profile/avatar')}">上传头像</a></li>
											<li><a href="{pigcms{:U('Profile/IdCard')}">身份认证</a></li>
											<li><a href="{pigcms{:U('Account/updatepwd')}">登录密码</a></li>
											<li><a href="{pigcms{:U('Profile/transactionPassword')}">交易密码</a></li>
											<li><a href="{pigcms{:U('Account/bind')}">绑定手机</a></li>
											<!--											<li><a href="/Member/InfoCenter">站内信</a></li>-->
										</ul>
									</li>
								</ul>
							</div>
							<!--sidebar-menu end-->
						</div>

						<div class="col-sm-10">
							<div class="container_page  padding-sm-none">
								<section>
									<h2>借入列表</h2><p>提醒：如果[状态]是失败的，可以把鼠标悬浮在连接上即可看到失败原因</p>
									<div class="tab_table page_min_h">
										<ul class="nav nav-tabs">
	<li role="presentation" <if condition="$state eq 0"> class="active"</if>><a href="{pigcms{:U('Loan/borrowList')}">全部</a></li>
	<li role="presentation" <if condition="$state eq 1"> class="active"</if>><a href="{pigcms{:U('Loan/borrowList',array('state'=>1))}">正在投标</a></li>
	<li role="presentation" <if condition="$state eq 2"> class="active"</if>><a href="{pigcms{:U('Loan/borrowList',array('state'=>2))}">等待还款</a></li>
	<li role="presentation" <if condition="$state eq 3"> class="active"</if>><a href="{pigcms{:U('Loan/borrowList',array('state'=>3))}">还款结束</a></li>
	<li role="presentation" <if condition="$state eq 4"> class="active"</if>><a href="{pigcms{:U('Loan/borrowList',array('state'=>4))}">已过期</a></li>
										</ul>
										<div class="table-responsive">
											<table class=" table table-bordered table-striped table-hover "  style="font-size: 13px;">
												<thead>
												<tr>
													<th>借入信息</th>
													<th>状态</th>
													<th>还款日期</th>
													<th>满标日期</th>
													<th>期数</th>
													<th>总还本息/管理费</th>
													<th>已还本息/管理费</th>
													<th>操作</th>
												</tr>
												</thead>
												<tbody class="text-center" id="nowborrowslist">
												<if condition="is_array($borrowsList)">
													<volist name="borrowsList" id="vo">
													<tr id ="delbid_{pigcms{$vo.bid}">
													<td><a <if condition="($vo['status'] eq 0) OR ($vo['status'] eq 3) OR ($vo['status'] eq 4) OR ($vo['status'] eq 5) OR ($vo['status'] eq 7) OR ($vo['status'] eq 6)"> href="javascript:void(0);"<else/> href="{pigcms{:U('Investment/loanScheme',array('dkid'=>$vo['bid']))}" target="_blank"</if>>
															[{pigcms{$vo.did}]
															{pigcms{$vo.Amountdx}元
														</a> <br />
														贷款人：{pigcms{$vo.dname}<br />
														年利率:{pigcms{$vo.Rate}% <br/>
														<if condition="$vo['surplus_date'] egt 0"> 剩余时间: <if condition="$vo['surplus_date'] eq 0"> <font color="red">{pigcms{$vo.surplus_date}</font> <else/> {pigcms{$vo.surplus_date}</if>天 </if><br/>
														总利息：￥{pigcms{$vo.zlx} <br />
														总管理费：￥{pigcms{$vo.zglf}<br />
														每月还款：￥{pigcms{$vo.myfk} <br />
														(利息：￥{pigcms{$vo.lx} ,<br />
														管理费：￥{pigcms{$vo.myglf} )<br />
														最后本金需还款：￥{pigcms{$vo.zhhk} <br />
														还款方式：每月还息
													</td>
													<td>
														<if condition="$vo['status'] eq 1"><span class="font_success">正在投标</span><elseif condition="$vo['status'] eq 2"/><span class="font_default">等待还款</span><elseif condition="$vo['status'] eq 3"/><span class="font_default">还款结束</span><elseif condition="$vo['status'] eq 4"/><span class="font_default">已过期</span><elseif condition="$vo['status'] eq 5"/><span class="font_default">等待审核</span><elseif condition="$vo['status'] eq 6"/><span class="font_success">审核通过</span><elseif condition="$vo['status'] eq 7"/><span class="font_warning"><a href="javascript:void(0);" title="{pigcms{$vo.intro}">申请拒绝</a></span></if>
													</td>
													<td><if condition="$vo['repayDate']">{pigcms{$vo.NextRepayDate}</if> </td>
													<td>{pigcms{$vo.ExpiredAt}</td>
													<td>{pigcms{$vo.periods}</td>
													<td>￥{pigcms{$vo['Amount'] + $vo['zlx']}/￥{pigcms{$vo.zglf}</td>
													<td>￥{pigcms{$vo.yh_menory}/￥{pigcms{$vo.yh_zglfees}</td>
													<td>
							<if condition="$vo['status'] eq 1">
								正在投标
							<elseif condition="$vo['status'] eq 3"/>
								还款结束
							<elseif condition="$vo['status'] eq 4"/>
								已过期
							<elseif condition="$vo['status'] eq 2"/>
								<a href="javascript:void(0);" id="nc_{pigcms{$vo.bid}" onclick="borrowoption('{pigcms{$vo.bid}',2);return false;">还清欠款</a>
							<elseif condition="$vo['status'] eq 7"/>
							<a href="javascript:void(0);" id="nc_{pigcms{$vo.bid}" onclick="borrowoption('{pigcms{$vo.bid}',1);return false;">删除</a>
							</if>

													</td>
													</tr>
													</volist>
												<else/>
												<tr>
													<td colspan="8">暂无记录</td>
												</tr>
												</if>
												</tbody>
											</table>
										</div>
									</div>




								</section>
							</div>
						</div>


					</div>
				</div>
			</section>
		</form>



	</div>
</div>
<include file="Public:footer"/>

<script src="{pigcms{$static_path}js/unslider.js"></script>
<script src="{pigcms{$static_path}js/bootstrap.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/rz_common.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/matrix.js"></script>
<script type="text/javascript">
	var submit_check="{pigcms{:U('Loan/borrowoption')}";
	function borrowoption(id,type) {
		var title,text,configbutton;
		if(type ==1){
			title = "你确定要删除么？";
			text =  "确认则删除该贷款记录信息";
			configbutton = "删除";
		}else if(type == 2){
			title = "一次还清欠款";
			text = "确认想一次还清欠款？利息还是照常算，不会有利息减免哦。如果提前还款，良好的还款记录会增加您的信用，提高下次借款成功率。"
			configbutton = "是的，一次还清"
		}

		swal({
			title: title,
			text: text,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: configbutton,
			cancelButtonText: "取消",
			closeOnConfirm: false,
			closeOnCancel: false
		}, function(isConfirm){
			if (isConfirm) {
				if(type == 1){
					var nowcheckdomain = $('#nc_'+id);
					nowcheckdomain.html("<img src='{pigcms{$static_path}images/index/loading.gif' width='20' height='20' />");
					$.post(submit_check, {'id': id,'type':type}, function (result) {
						result = $.parseJSON(result);
						if (result) {
							if (result.error == 0) {
								nowcheckdomain.parent().parent().remove();
								swal('',result.msg,'success');
								if(result.state == 1){
									window.location = result.reloadurl;
								}
								if(result.isreload){
									window.location = window.location;
								}
							} else {
								swal('',result.msg,'error');
								window.location = window.location;
							}
						} else {
							swal('',result.msg,'error');
							window.location = window.location;
						}
					});
					return false
				}else{
					swal({
						title: "一次还清该笔贷款确认!",
						text: "请您认真输入您的交易密码:",
						type: "input",
						inputType:"password",
						showCancelButton: true,
						closeOnConfirm: false,
						animation: "slide-from-top",
						cancelButtonText: "取消",
						inputPlaceholder: "在这里输入您的交易密码"
					}, function(inputValue){
						if (inputValue === false) return false;
						if (inputValue === "") {
							swal.showInputError("请输入正确的交易密码!");
							return false
						}

						$.post(submit_check, {'id': id,'type':type,'inputVal':inputValue}, function (result) {
							result = $.parseJSON(result);
							if (result) {
								if (result.error == 0) {
									//nowcheckdomain.parent().parent().remove();
									swal('',result.msg,'success');

								} else {
									swal('',result.msg,'error');
									window.location = window.location;
								}
							} else {
								swal('',result.msg,'error');
								window.location = window.location;
							}
						});

					});
				}


			} else {
				swal("取消成功", "您点击了取消 :)", "error");window.location = window.location;
			}
		});

	}

</script>
</body>
</html>