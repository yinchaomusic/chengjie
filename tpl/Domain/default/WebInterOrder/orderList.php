<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我是卖家 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>网站交易&nbsp;&gt;&nbsp;</li>
					<li>
    					中介交易
					</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
			<!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>
    							中介交易
    						</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 选项卡 -->
            <div class="m-acc-select u_be9 s-bg-white">
                <!-- 标题 -->
                <div class="m-acc-title s-bg-fc">
                    <div class="u-fl"><a href="{pigcms{:U('WebInterOrder/orderList',array('role'=>'1'))}" class=" set" id="type1">我是买家</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('WebInterOrder/orderList',array('role'=>'2'))}" class=" set" id="type2">我是卖家</a></div>
                    <input id="checktype" type="hidden" value="{pigcms{$_GET.role}" />
                    <div class="u-cls"></div>
                </div>
                    <!-- 搜索 -->
                    <!-- <div class="m-acc-search">
                        <span class="u-ipt-k u_fl">
                            <input type="text" class="i1 u_vm" ph="请输入要查询的域名" />
                        </span>
                        <input type="button" value="搜索" class="u-btn8" id="my_parse_serch" />
                    </div> -->
                    <!-- 内容 -->
                    <div class="m-acc-cons s-bg-fc u_mall1">
                        <table width="100%" cellspacing="0">
				<colgroup><col><col><col><col><col><col><col><col width="140" align="center"> </colgroup>
				<thead>
				<tr>
					<th>编号</th>
					<th>网站域名</th>
					<th>价格</th>
					<th>手续费</th>
					<th>经纪人ID</th>
					<th>网站类型</th>
					<th>创建日期</th>
					<th class="textcenter">操作</th>
				</tr>
				</thead>
				<tbody>
				<if condition="is_array($group_list)">
					<volist name="group_list" id="vo">
						<tr>
							<td>{pigcms{$vo.id}</td>
							<td>{pigcms{$vo.domain}</td>
							<td>
    							<font color="green" size="5">￥{pigcms{$vo.price|number_format}</font> 
							</td>
							<td>
								<if condition="$_GET['role'] eq 1">
    								<font color="green" size="5">￥{pigcms{$vo.buy_poundage|number_format}</font> 
    								<elseif  condition="$_GET['role'] eq 2"/>
									<font color="green" size="5">￥{pigcms{$vo.sell_poundage|number_format}</font> 
								</if>
							</td>
							<td>
								<if condition="$vo['check_user_id']">
									<font color="green">{pigcms{$vo.check_user_id}</font><else/><font color="red">无</font>
								</if>
							</td>
							<td>
								<if condition="$vo['web_type'] eq 1"><font color="red">行业门户 </font>
    								<elseif  condition="$vo['web_type'] eq 2"/>音乐影视
    								<elseif  condition="$vo['web_type'] eq 3"/>游戏小说
    								<elseif  condition="$vo['web_type'] eq 4"/>女性时尚
    								<elseif  condition="$vo['web_type'] eq 5"/>QQ/娱乐
    								<elseif  condition="$vo['web_type'] eq 6"/>商城购物
    								<elseif  condition="$vo['web_type'] eq 7"/>地方网站
    								<elseif  condition="$vo['web_type'] eq 8"/>其他网站
								</if>
							</td>
							<td><div style="height:24px;line-height:24px;">{pigcms{$vo.createdate|date='Y-m-d ',###}</div></td>
							<td class="textcenter">
										<if condition="$vo['status'] eq 0">
            								<a class="u-btn13">等待经纪人接入</a> 
            							</if>
            							
            							<if condition="($vo['status'] eq 1)  AND ($_GET['role'] eq 1)">
            								<if condition="$vo['buy_price_pay'] eq 0">
						                        <input type="hidden" name="id" id="el_id" value="{pigcms{$vo.id}"/>
						                        <input type="hidden" id="role" value="1"/>
            									<a class="u-btn13" id="sub">提交付款</a> 
            								</if>
            								<if condition="$vo['buy_price_pay'] eq 1">
        										<if condition="$vo['plat_fee_obj'] neq 1">
                									<if condition="$vo['sell_poun_pay'] eq '0'">
                    									<a class="u-btn13">卖家交保证金</a> 
            									</if>
            								</if>
        								</if>
            							
            							<if condition="($vo['status'] eq 2)  AND ($_GET['role'] eq 1)">
            								<if condition="$vo['buy_rece_status'] eq 0">
            									<if condition="$vo['sell_rece_status'] eq '1' ">
                									 <input type="hidden" name="id" id="el_id" value="{pigcms{$vo.id}"/>
                									 <a class="u-btn13 sureGet" >确认收货</a> 
                								</if>
            									<if condition="$vo['sell_rece_status'] eq '0' ">
                									 <a class="u-btn13">待卖家发货</a> 
                								</if>
            								</if>
            							</if>
            							
            							
            							
            							
            							<if condition="($vo['status'] eq 1)  AND ($_GET['role'] eq 2)">
            								<if condition="$vo['plat_fee_obj'] neq 1">
            									<if condition="$vo['sell_poun_pay'] eq 0">
                									<input type="hidden" name="id" id="el_id" value="{pigcms{$vo.id}"/>
    						                        <input type="hidden" id="role" value="2"/>
                									<a class="u-btn13" id="sub">提交付款</a> 
                								</if>
                								<if condition="($vo['sell_poun_pay'] eq 1) AND ($vo['buy_price_pay'] eq 0)">
                									<a class="u-btn13">买家交保证金</a> 
                								</if>
            								</if>
            								<if condition="$vo['plat_fee_obj'] eq 1">
            									<if condition="$vo['buy_price_pay'] eq 0">
            									 	<a class="u-btn13">买家交保证金</a> 
                								</if>
            								</if>
        								</if>
            							
            							<if condition="($vo['status'] eq 2)  AND ($_GET['role'] eq 2)">
            								<if condition="$vo['sell_rece_status'] eq 0">
            									<input type="hidden" name="id" id="el_id" value="{pigcms{$vo.id}"/>
            									 <a class="u-btn13 sureSend" >确认发货</a> 
            								</if>
            								<if condition="$vo['sell_rece_status'] eq 1">
            									<if condition="$vo['buy_rece_status'] eq 0">
                									 <a class="u-btn13">待买家收货</a> 
                								</if>
            								</if>
            							</if>
            							
            							<if condition="$vo['status'] eq 3">
            								<a class="u-btn13">待中介核实打款</a>
            							</if>
            							
            							<if condition="$vo['status'] eq 4">
            								<a class="u-btn13">交易结束</a>
            							</if>
            							
							</td>
						</tr>
					</volist>
            					<tr><td class="textcenter pagebar" colspan="8">{pigcms{$pagebar}</td></tr>
            					<else/>
            					<tr><td class="textcenter red" colspan="8">列表为空！</td></tr>
            				</if>
			</table>
                    </div>   
                    <!-- 分页 -->
            <div id="paging" class="g-padding g-padding1"></div> 
            </div>
        </div>
       <div class="u-cls"></div>
       <div style="position: absolute; display: none; z-index: 1005; left: 29%; top: 331.5px;" class="m-pop2 s-bg-white" id="pop15">
                    <div class="pop-insa">
                        <ul>
                            <li>
                                <span class="u-span18 u-fl"></span>
                                <span class="u-span19 u-fl">请核实交易详情。
                                </span>
                                <div class="u-cls"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="pop2-data1">
                        <ul>
                            <li>
                                <span class="u-span20">订单总额：￥</span>
                                <span class="s-75b u-span21 amount" id="total_price"></span>
                                <div class="u-cls"></div>
                            </li>
                            <li>
                                <span class="u-span20">手续费：￥</span>
                                <span class="s-75b u-span21 amount" id="poundage"></span>
                                <div class="u-cls"></div>
                            </li>
                            
                            <li>
                                <span class="u-span20">账户金额：￥</span>
                                <span class="u-span21" id="now_money"></span>
                                <div class="u-cls"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="pop2-btn s-bg-fa">
                        <input id="payfor" k="1000000" value="提交付款" class="u-btn12 s-bg-2a" type="button">
                    </div>
                    <div class="close"></div>
                </div>
    </div>
    
    <div style="position: absolute; display: none; z-index: 1005; left: 29%; top: 331.5px;" class="m-pop2 s-bg-white" id="pop16">
        <div class="pop-insa">
            <ul>
                <li>
                    <span class="u-span18 u-fl"></span>
                    	<span class="u-span19 u-fl">确认收货？
                    </span>
                    <div class="u-cls"></div>
                </li>
            </ul>
        </div>
        <div class="pop2-btn s-bg-fa">
            <input id="sureGet" k="1000000" value="确认" class="u-btn12 s-bg-2a" type="button">
        </div>
        <div class="close"></div>
    </div>
    <div style="position: absolute; display: none; z-index: 1005; left: 29%; top: 331.5px;" class="m-pop2 s-bg-white" id="pop17">
        <div class="pop-insa">
            <ul>
                <li>
                    <span class="u-span18 u-fl"></span>
                    	<span class="u-span19 u-fl">确认发货？
                    </span>
                    <div class="u-cls"></div>
                </li>
            </ul>
        </div>
        <div class="pop2-btn s-bg-fa">
            <input id="sureSend" k="1000000" value="确认" class="u-btn12 s-bg-2a" type="button">
        </div>
        <div class="close"></div>
    </div>
</div>
    
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<include file="Public:sidebar"/>
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">
    <script src="{pigcms{$static_path}js/popupBox.js"></script>
	<script>
		$( document ).ready(function () {

			
        	var type = $("#checktype").val();
        	if(type !=''){
    			$(".set").removeClass('active');
       		 	$("#type"+type).addClass('active');
        	};
        	$(".process1").css("display", "none");
        	$("#web").css("display", "block");
			$(".entr").removeClass('t');
   		 	$("#entr").addClass('t');


      		 $("#sub").click(function () {
                 var order_id = $("#el_id").val(); 
                 var role = $("#role").val(); 
                 $.post("{pigcms{:U('WebInterOrder/order_pay')}",{"order_id":order_id,"role":role},function(data){
                     $("#total_price").text(data.total_price);
                     $('#now_money').text(data.now_money);
                     $("#poundage").text(data.poundage);
                     
//                     alert(data);
                     showBox("pop15");
                     $("#pop15").css("left", "29%");
                 },'json')
                 
             });
             $(".close").click(function () {
                 closeBox("pop15");
             });
   		 	

        $('#payfor').click(function () {
            if (confirm(lang.a178)) {
                var order_id = $("#el_id").val(); 
                var role = $("#role").val(); 
                $.post("{pigcms{:U('WebInterOrder/order_pay_data')}",{"order_id":order_id,"role":role},function(data){
                    if(data == 1){
                         swal("成功!", "交易成功等待卖家提交域名!", "success")
                         location.href = location.href;
                    }else{
                        sweetAlert("异常...", "此次交易异常!", "error");
                    }
                    if(data == 3){
                        sweetAlert("提示...", "您的余额不足请充值!", "error");
                        location.href = location.href;
                    }
                })
            }
        });

		});


        $('#sureGet').click(function () {
            var order_id = $("#el_id").val(); 
            $.post("{pigcms{:U('WebInterOrder/sureGet')}",{"order_id":order_id},function(data){
                if(data == 1){
                     swal("成功!", "等待经纪人沟通确认!", "success")
                     location.href = location.href;
                }else{
                    sweetAlert("异常...", "此次交易异常!", "error");
                }
            })
        });

		
		$(".sureGet").click(function () {
			showBox("pop16");
            $("#pop16").css("left", "29%");
        });


        $(".close").click(function () {
            closeBox("pop15");
        });


        
        $('#sureSend').click(function () {
            var order_id = $("#el_id").val(); 
            $.post("{pigcms{:U('WebInterOrder/sureSend')}",{"order_id":order_id},function(data){
                if(data == 1){
                     swal("成功!", "等待经纪人沟通确认!", "success")
                     location.href = location.href;
                }else{
                    sweetAlert("异常...", "此次交易异常!", "error");
                }
            })
        });

		
		$(".sureSend").click(function () {
			showBox("pop17");
            $("#pop16").css("left", "29%");
        });

        $(".close").click(function () {
            closeBox("pop17");
        });
        
		
    </script>
</body>
</html>
