<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<if condition="($webtype eq 1)">
<title>网站中介交易 -网站中介交易的优势 - {pigcms{$config.site_name}</title>
</if>
<if condition="($webtype eq 2)">
<title>网站中介交易 - 网站中介交易的优势 - {pigcms{$config.site_name}</title>
</if>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
<link rel="stylesheet" type="text/css"
	href="{pigcms{$static_path}css/common.css" />
<link rel="stylesheet" type="text/css"
	href="{pigcms{$static_path}css/skin.css" />
<link rel="stylesheet" type="text/css"
	href="{pigcms{$static_path}css/sell-web.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header" />
	<include file="Public:nav" />
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>网站网站中介交易</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d
					H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
		<div class="m-content">
			<!-- 左边 -->
			<div class="g-a-container">
				<div class="m-ta s-bg-white u_be9">
					<div class="title">
						<a class="s-47" href="javascript:void(0)"> <span class="z-img-t11"></span>
							<span>网站中介交易</span>
						</a>
					</div>
					<div class="u-cls"></div>
				</div>
				<div class="m-insb s-bg-fc u_be89" id="animateb">
					<span class="f-img-dg1">网站中介交易申请</span>
					<span class="f-img-dg6"></span> <span class="f-img-dg2">审核申请</span>
					<span class="f-img-dg6"></span> <span class="f-img-dg3">缴纳保证金及手续费</span>
					<span class="f-img-dg6"></span> <span class="f-img-dg4">跟进谈判</span>
					<span class="f-img-dg6"></span> <span class="f-img-dg5">谈判结束</span>
				</div>
				<div class="m-transa s-bg-fc u_be89">
					<p class="f-p-t6 s-bg-white u_bb0 s-3c">发起中介交易</p>
					<div class="transa">
						<div class="select fl">
                    <form method="post" id="webform1" action="{pigcms{:U('WebInterOrder/add')}">
                    <div class="tra tra1">
                        <span>交易角色</span>
                        <div class="button-holder">
                            <input type="radio" id="radio-2-1" name="inter_obj" value="1" class="_role_value regular-radio big-radio"
                             checked=""/><label for="radio-2-1">我是买家</label>
                            <input type="radio" id="radio-2-2" name="inter_obj" value="2" class="_role_value regular-radio big-radio"/>
                            <label for="radio-2-2">我是卖家</label>
                        </div>
                    </div>
                 <div class="tra tra6">
					<span><b style="color: red">*</b>网站类型</span>
					<label class="address address1 fl" style="width:54px;margin-top:10px;border:0px solid #b8bbc2;font-weight:bold;height:38px;">
					  <select name="web_type" style="width: 140px;border:1px solid #b8bbc2;height:38px;">
					   <option value="1">-行业门户-</option>
					   <option value="2">-音乐影视-</option>
					   <option value="3">-游戏小说-</option>
					   <option value="4">-女性时尚-</option>
					   <option value="5">-QQ/娱乐-</option>
					   <option value="6">-商城购物-</option>
					   <option value="7">-地方网站-</option>
					   <option value="8">-其他网站-</option>
					</select></label>
				</div>
                <div class="tra tra6">
                    <span><b style="color:red">*</b>域名</span>
                    <textarea name="domain" placeholder="请输入域名,一行一个" id="domainId"></textarea>
                </div>
                <div class="tra tra6">
                    <span>日ip</span>
                                                              移动端
                    <input type="text" name="ip_app" style="width: 100px" value="0" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
                    pc端
                    <input type="text" name="ip_pc" style="width: 100px" value="0" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
                </div>
                <div class="tra tra6"><span>PR值</span> 
                    <input type="text" name="pr" value="0" style="width: 150px"
                        onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
				</div>
                <div class="tra tra6">
                    <span>网站程序(cms)</span>
                    <input type="text" name="souce_code" placeholder="可填写phpcms v9,帝国7.2,织梦5.6,自研等"/>
                </div>

                <div class="tra tra6">
                    <span>月收入</span>
                    <input type="text" name="month_income" value="0" style="width: 150px" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/> 元
                </div>

                <div class="tra tra6">
                    <span>服务器信息</span>
                    <input type="text" name="server_info" placeholder="例如:阿里云/香港机房/centos/1g/1cpu/1m"/>
                </div>
                
                <div class="tra tra6">
                    <span>含第三方平台</span>
                    <input type="text" name="third_party" placeholder="例如:微信公众号/微博帐号等等"/>
                </div>
                
                <div class="tra tra6">
                    <span>更新方式</span>
                    <input type="text" name="update_type" placeholder="例如:编辑更新/自动采集等等"/>
                </div>
                
                <div class="tra tra6">
                    <span>广告联盟</span>
                    <input type="text" name="advert_aliance" placeholder="例如:百度联盟/360联盟等"/>
                </div>
                
                <div class="tra tra6">
                    <span>联盟帐号处理</span>
                    <input type="radio" id="radio-ad_union_account-1" name="aliance_acc_handle" value="1"
                     class="regular-radio big-radio" checked=""/>
                     <label for="radio-ad_union_account-1">删除帐号</label>
                    <input type="radio" id="radio-ad_union_account-2" name="aliance_acc_handle" value="2"
                     class="regular-radio big-radio"/><label for="radio-ad_union_account-2">交接帐号</label>
                </div>
                
                <div class="tra tra6">
                    <span>帮忙搬迁网站</span>
                    <input type="radio" id="radio-help_move-1" name="is_help_move" value="1" class="regular-radio big-radio"
                     checked=""/><label for="radio-help_move-1">不需要</label>
                    <input type="radio" id="radio-help_move-2" name="is_help_move" value="2" class="regular-radio big-radio"/>
                    <label for="radio-help_move-2">需要</label>
                </div>

                <div class="tra tra6">
                    <span>备注</span>
                    <textarea name="description" placeholder="请输入备注说明,大致描述交易信息" id="descriptionId"></textarea>
                </div>
                <div class="tra tra6">
                    <span><b style="color:red">*</b>价格</span>
                    <input type="text" name="price" id="price" value="0" class="email" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" />
                </div>

                <div class="tra trasxf">
                    <span>平台手续费</span>
                    <p id="fee">0</p>
                </div>

                <div class="tra tra1">
                    <span>平台手续费支付方</span>
                    <input type="radio" id="radio-3-1" name="plat_fee_obj" value="1" class="regular-radio big-radio" checked=""/>
                    <label for="radio-3-1">买家</label>
                    <input type="radio" id="radio-3-2" name="plat_fee_obj" value="2" class="regular-radio big-radio"/>
                    <label for="radio-3-2">卖家</label>
                    <input type="radio" id="radio-3-3" name="plat_fee_obj" value="3" class="regular-radio big-radio"/>
                    <label for="radio-3-3">各50%</label>
                </div>

                <div class="tra tra5">
                    <div class="shouru"><span id="cost_income">共支付</span>
                    <span class="money" id="total_money">0</span></div>
                    <div class="price"><p>价格（元）：<span id="total_price">0</span></p>
                    <p>手续费（元）：<span id="total_fee">0</span></p></div>
                </div>
                <!--联系方式-->
                <div class="tra tra6 _role _role_1 _role_2"">
                    <span id="contacterSpan"><b style="color:red">*</b>卖家平台UID</span>
                    <input type="text" name="other_user_id" placeholder="uid" class="email"/>
                </div>
               <div class="tra tra6">
                    <span id="contacterPhone">卖家联系方式</span>
                    <input type="text" name="phone" id="phone" value="" class="email"  placeholder="请输入对方的联系方式" />
                </div>
                <!--end-->
                <div class="tra tra7"><label>
                <input type="checkbox" name="is_agent" value="checked" required="" id="agreementId"/>
                <p>我已经阅读并同意《<a href="/Home/About/fwxy" target="_blank">chengjie.com服务协议</a>》</p></label></div>
                <div class="tra tra8">
                <input type="button" value="发起交易" id="buttonFormS"></input></div>
            </form>
        </div>
					</div>
				</div>
			</div>
			<!-- 右边 -->

			<div class="g-b-container">
				<div class="m-bb-container">
					<div class="m-bb-title s-bg-fb">
						<a href="javascript:void(0);" class="u-tia s-3c">网站代购代售案例</a>
					</div>
					<div class="m-apply-dg m-apply-dga">
						<div class="m-apply-title">
							<span class="u-fl">网站域名</span> <span class="u-fr">价格</span>
							<div class="u-cls"></div>
						</div>
						<div class="m-animate3" id="animate3">
							<div class="animate3 box">
								<ul class="list">
									<volist id="vo" name="alList">
									<li>
										<div class="spanl">
											<a href="http://{pigcms{$vo.domain}" target="_blank"
												rel="nofollow"> {pigcms{$vo.domain}</a>
										</div>
										<div class="spanr">￥{pigcms{$vo.price|number_format}</div>
										<div class="u-cls"></div>
									</li>
									</volist>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<include file="Public:footer" />
	<include file="Public:sidebar" />
	<div class="s-bg-f7"></div>
	<script
		src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css"
		href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css" />
	<script>
            var post_url = "{pigcms{:U('Procurement/add')}";
        </script>
	<script type="text/javascript" src="{pigcms{$static_path}js/escrow.js"></script>

	<script type="text/javascript"
		src="{pigcms{$static_path}js/jquery.cxscroll.min.js"></script>
	<script type="text/javascript"
		src="{pigcms{$static_path}js/plugs_popupBox.js"></script>
	<script type="text/javascript">

	$("._role_value").change(function() {
		var interObj = $('input[name="inter_obj"]').filter(':checked').val();
		if(interObj=="1" || interObj=="1") {
			$("#contacterSpan").html("<b style='color:red'>*</b>卖家平台UID");
			$("#cost_income").html("共支出");
		} else {
			$("#contacterSpan").html("<b style='color:red'>*</b>买家平台UID");
			$("#cost_income").html("共收入");
		}
	});
    function updateTotalMoney() {

    	var interObj = $('input[name="inter_obj"]').filter(':checked').val();
    	var price = $("#price").val();
    	var platFeeObj = $('input[name="plat_fee_obj"]').filter(':checked').val();
		if(price=='' || price==null) {
			price=0;
		}
		interObj=parseInt(interObj);
		platFeeObj=parseInt(platFeeObj);
		price=parseInt(price);
    	if(price==0) {
            $("#total_money").html(0);
            $("#total_price").html(0);
            $("#total_fee").html(0);
        } else {
            var total_money=0;//收入总额，支出总额
            var total_price=0;//价格 不包含手续费
            var total_fee=0;//手续费
            var fee = createTotolMoney(price);
            $("#fee").text(fee);
			if(platFeeObj==1 && interObj==1) {//买家付全部手续费
				total_money=parseInt(price)+parseInt(fee);
			}
			else if(platFeeObj==2 && interObj==1) {//卖家付全部手续费
				total_money=parseInt(price);
			}
			else if(platFeeObj==3 && interObj==1) {//买家卖家各付一半手续费
				total_money=(parseInt(price)+parseInt(fee/2));
			}
			else if(platFeeObj==1 && interObj==2) {//买家付全部手续费
				total_money=parseInt(price);
			}
			else if(platFeeObj==2 && interObj==2) {//卖家付全部手续费
				total_money=parseInt(price)-parseInt(fee);
			}
			else if(platFeeObj==3 && interObj==2) {//买家卖家各付一半手续费
				total_money=(parseInt(price)-parseInt(fee/2));
			}
            $("#total_money").html(total_money);
            $("#total_price").html(price);
            $("#total_fee").html(fee);
        }
    }

    function createTotolMoney(price) {
        if(price>0 &&price<1000) {
            var fee= parseInt(price/50);
            if(fee<1){
                return 1;
            }else {return fee;}
        } else {
        	var fee= parseInt(price/40);
			 if(fee<1){
	                return 1;
	         }else {return fee;}
        }
    }

    $("#price").change(function(){
    	updateTotalMoney();
    });
    $("input[name=inter_obj],input[name=plat_fee_obj]").change(function(){
        updateTotalMoney();
    })
    //表单提交
    
     //表单提交
        $("#buttonFormS").click(function(){
        	var price = $("#price").val();
        	if(parseInt(price)<0) {
        		showDialog("您输入的网站价格有误");
            	return "";
        	}
        	var cT = $('input[name="ctaction"]').filter(':checked').val();
            $("#webtypeId").val(cT);
        	var domain=$("#domainId").val();
        	if($.trim(domain).length==0) {
        		showDialog("请输入域名");
            	return "";
        	}
        	if($.trim(domain).length>100) {
        		showDialog("请输入200字符以内的域名");
            	return "";
        	}
        	var price=$("#price").val(); 
        	if($.trim(price).length==0) {
        		showDialog("请输入网站价格");
            	return "";
        	}
        	var descripton=$("#descriptionId").val(); 
        	if($.trim(descripton).length>300) {
        		showDialog("请输入300字符以内的描述");
            	return "";
        	}
        	if (!$('#agreementId').attr('checked')) {
 		    	showDialog("您是否同意服务协议");
            	return "";
        	}
        	$("#webform1").submit();
        });
	</script>
</body>
</html>