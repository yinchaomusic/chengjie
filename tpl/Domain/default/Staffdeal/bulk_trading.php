<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
        <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css"/>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header_staff"/>
	<include file="Public:nav_staff"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Staff/index')}" class="s-2a">经济人账户</a>&nbsp;&gt;&nbsp;</li>
                                        <li>批量交易审核</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Public:left_staff"/>
        <!-- 右边 -->
        <div class="u-fr">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>客户批量交易</span>
                        </a>
                    </div>

                    <div class="u-cls"></div>
                </div>
                <div class="m-insa s-bg-fc u_be89" style="width:676px;">
                    <ul>
                        <li><span class="u_fw">1.</span>如果有未通过域名验证的可能是系统查询域名的whois邮箱超时。</li>
                        <li><span class="u_fw">2.</span>如果其中有超时的验证，请您手动去whois里查询核查。</li>

                    </ul>
                </div>
            </div>
            <!-- 选项卡 -->
            <div class="m-acc-select u_be9 s-bg-white">
                <!-- 内容 -->
                <div class="m-acc-cons s-bg-fc u_mall1">
                    <table>
                        <tbody>
                            <tr>
<!--                                <th width="80px" class="u_tl">交易ID</th>-->
                                <th width="100px" class="u_tl">客户姓名</th>
                                <th width="150px" class="u_tl">标题</th>
                                <th width="40px" class="u_tl">数量</th>
                                <th width="60px" class="u_tl">总价</th>
                                <th width="100px" class="u_tl">添加时间</th>
                                <th width="75px" class="u_tl">域名列表</th>
                                <th width="80px;">操作</th>
                            </tr>
                            <if condition="is_array($bt_list)">
                                <volist name="bt_list" id="vo">
                                    <tr>
<!--                                        <td width="80px" class="u_tl">{pigcms{$vo.bulk_id}</td>-->
                                        <td width="100px" class="u_tl">{pigcms{$vo.nickname}</td>
                                        <td width="150px" class="u_tl">{pigcms{$vo.title}</td>
                                        <td width="40px" class="u_tl">{pigcms{$vo.amount}</td>
                                        <td width="100px" class="u_tl">￥{pigcms{$vo.total_price|number_format}</td>
                                        <td width="100px" class="u_tl">{pigcms{$vo.add_time|date='Y-m-d',###}</td>
                                        <td width="75px" class="u_tl"><a href="{pigcms{:U('Staffdeal/bt_list?bulk_id=')}{pigcms{$vo.bulk_id}">域名列表</a></td>
                                        <if condition="$vo.status eq 0">
                                            <td width="80px;">
<!--                                                <a  href="{pigcms{:U('Staffdeal/bt_check',array('bulk_id'=>$vo['bulk_id'],'status'=>1))}" onclick="return confirm('您确定要通过审核吗')"><font color="green">通过</font> </a> |
                                                <a href="{pigcms{:U('Staffdeal/bt_check',array('bulk_id'=>$vo['bulk_id'],'status'=>4))}" onclick="return confirm('你确定要拒绝这条信息吗')"><font color="red">拒绝</font></a>-->
                                        <a onclick="plAudit({pigcms{$vo.bulk_id},1,'通过')"><font color="green">通过</font> </a> | 
                                        <a onclick="plAudit({pigcms{$vo.bulk_id},4,'拒绝')"><font color="red">拒绝</font> </a>
                                            </td>
                                        <elseif condition="$vo.status eq 1"/>
                                            <td width="80px;">
                                                <a>出售中</a>
                                            </td>
                                        <elseif condition="$vo.status eq 2"/>
                                            <td width="80px;">
                                                <a>正在交易</a>
                                            </td>
                                        <elseif condition="$vo.status eq 4"/>
                                            <td width="80px;">
                                                <a>审核失败</a>
                                            </td>
                                        <else/>
                                            <td width="50px;">
                                                <a>交易达成</a>
                                            </td>
                                        </if>
                                        
                                    </tr>
                                </volist>
                            <else/>
                                    <tr><td class="u_nb u_tc u_norecord" colspan="9">暂无记录！</td></tr>
                            </if>
                            </tbody>
                    </table>
                </div>
                <!-- 分页 -->
                <div id="paging" class="g-padding g-padding1">{pigcms{$pagebar}</div>
            </div>
        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<script src="{pigcms{$static_path}js/account.js"></script>
        <script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
        <script>
            function plAudit(bulk_id,status,operation){
                swal({   
                    title: operation,   
                    text: "您是否要进行此次操作!",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "是,我要操作!",   
                    cancelButtonText: "不,我点错了!",
                    closeOnConfirm: false 
                }, 
                function(){   
                    $.post("{pigcms{:U('Staffdeal/bt_check')}",{"bulk_id":bulk_id,"status":status},function(data){
                        if(data == 1){
                            swal("成功!", "操作成功.", "success"); 
                            location.href=location.href;
                        }else{
                            sweetAlert("异常...", "操作异常请重试!", "error");
                        }
                    })
                    
                });
                
            }
        </script>
        
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}
	</script>
	<include file="Public:sidebar"/>
</body>
</html>
