<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header_staff"/>
	<include file="Public:nav_staff"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Staff/index')}" class="s-2a">经济人账户</a>&nbsp;&gt;&nbsp;</li><li>发起新交易</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Public:left_staff"/>
         <!-- 左边 -->
        <div class="g-a-container">
            <div class="m-ta s-bg-white u_be9">
                <div class="title">
                    <a class="s-3c" href="javascript:void(0)">
                        <span class="z-img-t12"></span>
                        <span>新交易</span>
                    </a>
                </div>
                <div class="u-cls"></div>
            </div>
            <div class="m-insa s-bg-fc u_be89">
                <ul>
                    <li><span class="u_fw">1.</span>买家向{pigcms{$now_site_name}（{pigcms{$now_site_short_url}）支付域名全款后，卖家才开始域名过户。</li>
<li><span class="u_fw">2.</span>买家拿到域名且完成交易后，{pigcms{$now_site_name}（{pigcms{$now_site_short_url}）方解冻款项释放给卖家。</li>
<li><span class="u_fw">3.</span>手续费按照支付方会员等级收取，交易未达成不收取手续费。</li>
                </ul>
            </div>
            <div class="m-insb s-bg-fc u_be89" id="animateb">
                <span class="f-img-dg7">发起交易</span>
                <span class="f-img-dg6"></span>
                <span class="f-img-dg8">同意条款</span>
                <span class="f-img-dg6"></span>
                <span class="f-img-dg9">买家付款</span>
                <span class="f-img-dg6"></span>
                <span class="f-img-dg10">过户域名</span>
                <span class="f-img-dg6"></span>
                <span class="f-img-dg11">买家确认</span>
            </div>
            <div class="m-transa s-bg-fc u_be89">
                <p class="f-p-t6 s-bg-white u_bb0 s-3c">发起中介交易</p>
                <form method="post" action="{pigcms{:U('Staffdeal/new_deal')}" id="form">
                    <div class="transb">
                        <div class="u_bb0 u_pl110">
                            <div class="t-left">交易域名：</div>
                            <div class="t-right">
                                <div class="t-pri" id="t-pri">
                                    <p>
                                        <span class="u-ipt-d u_mr60">
                                            <input type="text" class="domainName" name="domainName" maxlength="50" /></span>
                                        <span class="pri-txt">价格：</span>
                                        <span class="u-ipt-d">
                                            <input type="text" name="total_price" class="price" /></span>
                                    </p>
                                </div>
                                
                            </div>
                            <div class="u-cls"></div>
                        </div>
                        <div class="t-roles">
                            <p class="u_mb10">
                                <span class="u-txo">买家会员ID：</span>
                                <span class="u-ipt-d">
                                    <input type="text" class="user" maxlength="10" name="buy_id" />
                                </span>
                                <span style="color:#ff6d00; margin-left:5px;display:none" class="user_tips" >请输入正确的会员ID</span>
                            </p>
                            <p class="u_mb10">
                                <span class="u-txo">卖家会员ID：</span>
                                <span class="u-ipt-d">
                                    <input type="text" class="user" maxlength="10" name="sell_id" />
                                </span>
                                <span style="color:#ff6d00; margin-left:5px;display:none" class="user_tips" >请输入正确的会员ID</span>
                            </p>
                            <p>
                                <span class="u-txo">由谁承担中介费：</span>
                                <label>
                                    <input type="radio" name="payTye" value="1" class="u-rd1" />
                                    <span class="u-txp">买家</span>
                                </label>
                                <label class="u_ml85">
                                    <input type="radio" name="payTye" value="2" class="u-rd1" />
                                    <span class="u-txp">卖家</span>
                                </label>
                                <label class="u_ml85">
                                    <input type="radio" name="payTye" value="3" class="u-rd1" />
                                    <span class="u-txp">买卖双方各付50%</span>
                                </label>
                            </p>
                            <p>
                                <br />
                                <input type="submit" style="margin-top:20px;" value="提交申请" class="u-btn3 s-bg-2a escrow_sub" />
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- 右边 -->


		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<script src="{pigcms{$static_path}js/account.js"></script>
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}
	</script>
	<include file="Public:sidebar"/>
</body>
</html>
