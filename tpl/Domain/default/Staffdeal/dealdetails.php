<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header_staff"/>
	<include file="Public:nav_staff"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
                                        <li><a href="{pigcms{:U('Staff/index')}" class="s-2a">经济人账户</a>&nbsp;&gt;&nbsp;</li>
					<li>交易详情</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Public:left_staff"/>
        <!-- 左边 -->
        <div class="u-fl">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>交易详情</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 交易详情 -->
                <div class="m-seller s-bg-fc u_be9">
                    <h4 class="f-img-buyer s-bg-white">您是经纪人</h4>
                        <h3 class="u_bt2"><span class="s-2a domain">{pigcms{$order_info.domainName}</span><span class="s-ff6 z-wait">(
                                <if condition="$order_info['yes_no'] eq 1">
                                    等待买家同意条款
                                    <elseif condition="$order_info['yes_no'] eq 2"/>
                                    等待卖家同意条款
                                    <elseif condition="$order_info['yes_no'] eq 3 "/>
                                    等待买家付钱
                                    <elseif condition="$order_info['yes_no'] eq 4 "/>
                                    等待卖家转移域名
                                    <elseif condition="$order_info['yes_no'] eq 5 "/>
                                    买家拒绝
                                    <elseif condition="$order_info['yes_no'] eq 6 "/>
                                    卖家拒绝
                                    <elseif condition="$order_info['yes_no'] eq 7 "/>
                                    后台关闭
                                    <elseif condition="$order_info['yes_no'] eq 8 "/>
                                    经纪人关闭
                                    <elseif condition="$order_info['yes_no'] eq 9 "/>
                                    等待买家确认是否收到域名
                                </if>
                                )</span></h3>
                        <div class="m-insd">
                            <ul>
                                <li>
                                    <if condition="$order_info['payTye'] eq 1">买方来支付平台手续费
                                    <elseif condition="$order_info['payTye'] eq 2"/>卖方来支付平台手续费
                                    <elseif condition="$order_info['payTye'] eq 3"/>双方各一半的方式来支付平台手续费
                                    </if>
                                </li>
                            </ul>
                            <p class="u_btn_container2">
                            <if condition="$order_info['yes_no'] eq 1">
                                    <elseif condition="$order_info['yes_no'] eq 3 "/>
                                    <a class="u-btn13" href="{pigcms{:U('Staffdeal/operate',array('order_id'=>$order_info['order_id'],'yes_no'=>4))}">买家已付款</a>
                                    <elseif condition="$order_info['yes_no'] eq 4 "/>
                                    <a class="u-btn13" href="{pigcms{:U('Staffdeal/operate',array('order_id'=>$order_info['order_id'],'yes_no'=>9))}">卖家已转移域名</a>
                                </if>
                            </p>
                        </div>   
                </div>
            
            <!-- 交易数据 -->
            <div class="m-account-data1 u_be9 s-bg-fc">
                <h4 class="s-bg-white">
                    <a class="s-3c m-ta2" href="javascript:void(0);">
                        <span class="sp1">交易域名</span>
                    </a>
                    <div class="u-cls"></div>
                </h4>
                <table>
                    <tbody><tr>
                        <th width="16px"></th>
                        <th class="u_tl" width="150px">来源</th>
                        <th class="u_tl" width="200px">域名</th>
                            <th class="u_tr" width="120px">价格</th>
                            <th width="16px"></th>
                        </tr>
                            <tr>
                                <td></td>
                                <td class="u_tl">
                                    <span class="s-2a">中介</span>
                                </td>
                                <td class="u_tl"><span class="s-2a"><if condition="$order_info['domainName'] eq ''">批量交易<else/>{pigcms{$order_info.domainName}</if></span></td>
                                <td class="u_tr"><span class="s-75b">￥{pigcms{$order_info.total_price|number_format=###}</span></td>
                                <td></td>
                            </tr>  
                    </tbody>
                </table>
            </div>
            <!--交易进度-->
            <div class="m-account-data1 u_be9 s-bg-fc">
                <h4 class="s-bg-white">
                    <a class="s-3c m-ta2" href="javascript:void(0);">
                        <span class="sp1">交易进度</span>
                    </a>
                    <div class="u-cls"></div>
                </h4>
                <table>
                        <tbody>
                        <volist name="order_info_list" id="vv">
                            <tr>
                                <td></td>
                                <td><span class="u-span17">{pigcms{$vv.info}</span></td>
                                <td>{pigcms{$vv.time|date="Y-m-d H:i:s",###}</td>
                                <td></td>
                            </tr> 
                        </volist>
                </tbody></table>
            </div>
        </div>
        <!-- 右边 -->
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<include file="Public:sidebar"/>
</body>
</html>
