<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>客户批量交易 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<style type="text/css">
        .domain {
            cursor: pointer;
            display: inline-block;
            padding-right: 60px;
            background: url('/images/index/gotoTop.png') no-repeat right -245px;
        }
        .m-seller dl {
            margin: 10px 20px;
            display: none;
        }
		.m-seller dl dd {
			float: left;
			width: 220px;
			margin-top: 7px;
			color: #2aa3ce;
			font-size: 15px;
		}
    </style>
</head>
<body class="s-bg-global">
 <include file="Public:header_staff"/>
	<include file="Public:nav_staff"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Staffdeal/bulk_trading')}" class="s-2a">批量交易审核</a>&nbsp;&gt;&nbsp;</li>
					<li>域名列表</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Public:left_staff"/>
        <!-- 右边 -->
        <div class="u-fr">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>当前状态</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 新增 -->
            <div class="m-insa s-bg-fc u_be89" style="width:676px;">
                <ul>
                    <li><span class="u_fw">1.</span>如果有未通过域名验证的可能是系统查询域名的whois邮箱超时。</li>
                    <li><span class="u_fw">2.</span>如果其中有超时的验证，请您手动去whois里查询核查。</li>
                     
                </ul>
            </div>
            <div class="m-seller s-bg-fc u_be9">
                <h3 class="u_bt2 pljy-t-head">
                    <div class="s-2a pljy-title" style="background:none;">
                        {pigcms{$domain_list.title}<span class="m">￥{pigcms{$domain_list.total_price|number_format}</span>
                    </div>
                    <span class="s-ff6 z-wait"></span>
                    <span class="pljy-intro">{pigcms{$domain_list.meaning}</span>
                </h3>
                <dl style="display:block;" id="ls_ym">
					<volist name="domain_list['domain_list']" id="vo">
                        <dd class="s" >{pigcms{$vo} - <span  > </span> </dd>

                    </volist>
                </dl>
                <!-- 修改部分结束 -->
                <div style="clear: both;"></div>
                <div style="height:15px"></div>
                <div style="height: 15px"></div>
            </div>
        </div>
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<script type="text/javascript">
        $(function () {
           $('.z-wait').html('（'+getDataList_state({pigcms{$domain_list.status})+'）');
        });
				$(document).ready(function(){
					checkLeishi();
					function checkLeishi() {

						var check_domains = {pigcms{$jsondata};
						var whoid = {pigcms{$jsonwhoid};
						var str = "";
						$("#ls_ym").append(str);
						$("#ls_ym tr:odd").addClass("active");
						//发送Ajax检查当前域名的注册状态，返回时写上当前状态替换旋转的图片
						$.ajax({
							type:"post",
							url: "{pigcms{:U('Staffdeal/whois')}",
							data: { "arr_yms": check_domains,"whoid":whoid},
							success: function (response) {
								var res =$.parseJSON(response);
								var j = res.length;

								for (var i = 0; i < j; i++) {
									if (res[i] == "1") {
										$("#ls_ym dd span")[i].innerHTML = "- 已验证";
										$("#ls_ym dd span")[i].style.color = "#3394D8";
									}else if(res[i] == "2"){
										$("#ls_ym dd span")[i].innerHTML = "- 验证超时";
										$("#ls_ym dd span")[i].style.color = "#213323";
									}else{
										$("#ls_ym dd span")[i].innerHTML = "- 验证未通过";
										$("#ls_ym dd span")[i].style.color = "brown";
									}
								}
							},
							error: function (res) {

							}
						});
					}
				});
    </script>
	<include file="Public:sidebar"/>
</body>
</html>
