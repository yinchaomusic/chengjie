<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav_staff"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>我的账户</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Public:left_staff"/>
        <!-- 右边 -->
        <div class="u-fr">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>我的终端</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 选项卡 -->
            <div class="m-acc-select u_be9 s-bg-white">
                <!-- 标题 -->
                <div class="m-acc-title s-bg-fc">
                    <div class="u-fl"><a href="{pigcms{:U('Staffdeal/terminal')}">审核</a></div>
                    <div class="u-fl"><a class="active" href="{pigcms{:U('Staffdeal/ongoing')}">推荐中</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Staffdeal/succeed')}">推荐成功</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('Staffdeal/failure')}">推荐失败</a></div>
                    <input type="hidden" id="checktype" value="4">
                    <div class="u-cls"></div>
                </div>
                <!-- 内容 -->
                    <div class="m-acc-cons s-bg-fc u_mall1">
                        <table class="tb_ap_fail">
                            <tbody>
                            <tr>
                                <th width="16px"></th>
                                <th width="150px" class="u_tl">域名</th>
                                <th width="120px" class="u_tr">最低价格</th>
                                <th width="120px"></th>
                                <th width="197px" class="u_tl">申请时间</th>
                                <th width="56px">操作</th>
                                <th width="16px"></th>
                            </tr>
                            
                            
                            <if condition="is_array($list)">
                                <volist name="list" id="vo">
                                    <tr>
                                        <td width="16px"></td>
                                        <td width="150px" class="u_tl">{pigcms{$vo.domain_name}</td>
                                        <td width="120px" class="u_tr">￥{pigcms{$vo.min_price|number_format}</td>
                                        <td width="120px"></td>
                                        <td width="197px" class="u_tl">{pigcms{$vo.add_time|date="Y-m-d H:i:s",###}</td>
                                        <td width="56px"><a id="delete" data-id="{pigcms{$vo.id}">删除</a></td>
                                        <td width="16px"></td>
                                    </tr>
                                </volist>
                                <else/>
                                <tr><td class="u_nb u_tc u_norecord" colspan="7">暂无记录！</td></tr>
                            </if>
                            
                            
                            
                            </tbody>
                        </table>
                    </div>   
                <!-- 分页 -->
                <div class="g-padding g-padding1" id="paging"></div>
            </div>
        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<script src="{pigcms{$static_path}js/account.js"></script>
        
        <script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">
        <script>
            $("#delete").click(function(){
                swal({   
                    title: "删除?",   
                    text: "您确定要删除此条信息吗!",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "Yes, 我要删除!",   
                    cancelButtonText: "No, 我点错了!",  
                    closeOnConfirm: false,   
                    closeOnCancel: false 
                }, 
                function(isConfirm){   
                    if (isConfirm) {     
                        var id= jQuery("#delete").data('id');
                        $.post("{pigcms{:U('Terminal/delete')}",{"id":id},function(data){
                            if(data == 1){
                                swal("成功!", "此条信息删除成功.", "success");
                                location.href = location.href;
                            }else{
                                swal("异常", "信息删除异常，请重试 :)", "error");  
                            }
                        })
                        
                    } else {     
                        swal("Cancelled", "Your imaginary file is safe :)", "error");   
                    } 
                });
            })
        </script>
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}
	</script>
	<include file="Public:sidebar"/>
</body>
</html>
