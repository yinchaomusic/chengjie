<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我是卖家 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/common.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav_staff"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>经纪人介入</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Public:left_staff"/>
        <!-- 右边 -->
        <div class="u-fr">
			<div class="m-processList2 u_be9">
				<div class="txt1">添加域名</div>
				<div class="txt2">通过验证</div>
				<div class="txt3">
					<span class="u-span9">议价</span>
					<span class="u-span10">默认开通</span>
				</div>
				<div class="txt4">
					<span class="u-span9">一口价</span>
					<span class="u-span10">需开通服务</span>
				</div>
				<div class="txt5">
					<span class="u-span9">极速竞价</span>
					<span class="u-span10">需提交申请</span>
				</div>
				<div class="txt6">
					<span class="u-span9">优质</span>
					<span class="u-span10">需开通服务</span>
				</div>
			</div>
            <form method="post" action="{pigcms{:U('Account/addDomainCheck')}" enctype="multipart/form-data">
                <div class="m-addDomains u_be9 s-bg-fc">
                    <p class="f-p26">请输入您的域名或者选择上传Txt或Excel文件。<a href="/demo/mibiao.rar" class="download-demo-btn" target="_blank">点击下载模板{pigcms{$user_show.uid}</a></p>
                    <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&amp;uin={pigcms{:$user_show.qq}&amp;site=qq&amp;menu=yes"><img border="0" src="http://www.asy5.com/statics/images/hengfu/pa.gif" alt="联系用户" title="联系用户"></a>

                    <p class="f-p-t9">域名列表：</p>
                    <p class="f-p27">
                        <textarea class="txta_v" ph="域名一行一个，请输入顶级域名" name="domain"></textarea>
                    </p>
                    <p class="f-p-t10">
                        <span class="u_fw u-span11">上传Txt或Excel文件：</span>
                        <input type="file" class="file" name="fileField" />
                    </p>
					<p class="f-p-t10" style="margin-top: 10px;">
						<span class="u_fw u-span11">选择分组：</span>
						<select name="group">
							<option value="0">不分组</option>
							<volist name="group_list" id="vo">
								<option value="{pigcms{$vo.group_id}">{pigcms{$vo.group_name}</option>
							</volist>
						</select>
						&nbsp;&nbsp;&nbsp;<a target="_blank" href="{pigcms{:U('Account/group')}">新建分组</a>
					</p>
					<div class="m-addoper">
						<div class="u-fl addoper-l">
							<!--操作：-->
						</div>
						<div class="u-fl addoper-r">
							<!--p>
								<label>
									<input type="radio" name="type" value="0" checked="checked" /><span>添加域名</span></label>
							</p>
							<p>
								<label>
									<input type="radio" name="type" value="1" /><span>添加域名，同时删除库中不在以上列表的域名</span></label>
							</p>
							<p>
								<label>
									<input type="radio" name="type" value="2" /><span>删除库中以上列表中的域名</span></label>
							</p-->
							<p class="u_mtb30">
								<input type="submit" value="提交" class="u-btn9 btn_tj" />
							</p>
						</div>
						<div class="u-cls"></div>
					</div>
                </div>
            </form>
        </div>	
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<script src="{pigcms{$static_path}js/account.js"></script>
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}
        $(function () {
            
            $(".btn_tj").click(function () {
                var f = $(".file").val();
                var ph = $(".txta_v").attr("ph");
                if (ph)
                    ph = ph.replace(/\r/g, "");
                var d = $(".txta_v").val().replace(/\r/g, "") == ph ? "" : $(".txta_v").val();
                if (f && !/.*\.txt$|.*\.xls|.*\.xlsx$/i.test(f)) {
                    showDialog(lang.a45);
                    return false;
                }
                if (!f && d == "") {
                    showDialog(lang.a46);
                    return false;
                }
            })
            $('#sbt-pltj').click(function (e) {
                return $('.check-ipt').each(function () {
                    var _iptText, _ref;
                    if ($.trim($(this).val()) === '') {
                        e.preventDefault();
                        _iptText = (_ref = $(this).attr('check-info')) != null ? _ref : '';
                        showDialog(_iptText + '不能为空');
                        return false;
                    }
                });
            });
        })
	</script>
	<include file="Public:sidebar"/>
</body>
</html>
