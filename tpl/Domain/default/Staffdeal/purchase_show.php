<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header_staff"/>
	<include file="Public:nav_staff"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Staff/index')}" class="s-2a">经济人账户</a>&nbsp;&gt;&nbsp;</li>
                                        <li><a href="{pigcms{:U('Staffdeal/purchase_list')}" class="s-2a">代购列表</a>&nbsp;&gt;&nbsp;</li>
                                        <li>审核 详情</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Public:left_staff"/>
        <!-- 右边 -->
        <div class="u-fr">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>客户详情</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <div class="m-acc-select u_be9 s-bg-white">
           <!-- 左边 -->
            <div class="m-insf s-bg-fc">
                <p>请确保您的联系方式真实有效，如果您的交易或帐户出现什么问题，我们将通过您提供的联系方式联系您！</p>
            </div>
            <!--您绑定的邮箱  -->
            <!-- 数据 -->
                <div class="m-question m-question1 s-bg-fc u_be9">
                    <ul>
                        <div class="transa">
                                <p class="u_mb16">
                                        <span class="u-txj">客户会员ID：</span>
                                        <span class="u-ipt-d">{pigcms{$info.uid}</span>
                                </p>
                                <p class="u_mb16">
                                        <span class="u-txj">想购买的域名：</span>
                                        <span class="u-ipt-d">{pigcms{$info.domain}</span>
                                </p>
                                <p class="u_mb16">
                                        <span class="u-txj">报价：</span>
                                        <span class="u-ipt-d">{pigcms{$info.buyers_price}</span>
                                </p>
                                <p>
                                        <span class="u-txj">留言或其它附加信息：</span>
                                        <span class="u_txera s-bg-white">{pigcms{$info.message}</span>
                                </p>
                               
                        </div>
                        <input type="hidden" name="ph_id" value="{pigcms{$info.ph_id}" id="ph_id"/>
                        
                        <if condition="$info['status'] eq 1">
                             <li>
                                <div class="u_mall12">
                                    <input type="submit" id="yes" value="通过" class="u-btn12 s-bg-2a"> 
                                    <input type="submit" id="no" value="拒绝" class="u-btn12 s-bg-2a">
                                </div>
                            </li> 
                            <p>
                                    <span class="u-txj">理由</span>

                                    <span class="u_txera s-bg-white">
                                        <textarea name="why" id="why"></textarea>
                                    </span>
                            </p>
                            <elseif condition="$info['status'] eq 5"/>
                                <li>
                                    <div class="u_mall12">
                                        <input type="submit" value="卖家以成功交易域名" id="sub_sell" class="u-btn12 s-bg-2a"> 
                                    </div>
                                </li> 
                            <elseif condition="$info['status'] eq 4"/>
                                <li>
                                    <div class="u_mall12">
                                        <input type="submit" value="卖家同意" id="agree"  class="u-btn12 s-bg-2a"> 
                                        <input type="submit" value="卖家拒绝" id="refused" class="u-btn12 s-bg-2a">
                                    </div>
                                </li> 
                            <else/>
                        </if>
                    </ul>
                    <div class="u-cls"></div>
                </div>
        <!-- 右边 -->
        </div>
		</div>
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<script src="{pigcms{$static_path}js/account.js"></script>
        <script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
        <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">
        <script>
            $("#sub_sell").click(function(){
                swal({   
                    title: "卖家是否发货?",
                    text: "您提交之后代表卖家已经发货!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "是的卖家以发货!",
                    cancelButtonText: "不,我点错了!",
                    closeOnConfirm: false, 
                    closeOnCancel: false 
                },
                function(isConfirm){   
                    if (isConfirm) { 
                        var id = $("#ph_id").val();
                        $.post("{pigcms{:U('Staffdeal/purchase_sell_delivery')}", {"ph_id":id,"status":8}, function(data){
                            if(data == 1){
                                swal("提交成功!", "等待用户用户付款.", "success");  
                                location.href = location.href;
                            }else{
                                swal("异常", "您这次提交异常 :)", "error");   
                            }
                        })
                         
                    } else {     
                        swal("取消", "您取消了这次提交 :)", "error");   
                    } 
                });
            })
            
            
            $("#agree").click(function(){
                swal({   
                    title: "卖家同意出售?",
                    text: "您提交之后代表卖家已经同意出售此代购域名!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "是的卖家同意了!",
                    cancelButtonText: "不,我点错了!",
                    closeOnConfirm: false, 
                    closeOnCancel: false 
                },
                function(isConfirm){   
                    if (isConfirm) { 
                        var id = $("#ph_id").val();
                        $.post("{pigcms{:U('Staffdeal/purchase_sell_agree')}", {"ph_id":id,"status":9}, function(data){
                            if(data == 1){
                                swal("提交成功!", "等待用户确认.", "success");  
                            }else{
                                swal("异常", "您这次提交异常 :)", "error");   
                            }
                        })
                         
                    } else {     
                        swal("取消", "您取消了这次提交 :)", "error");   
                    } 
                });
            })
            
            
            $("#refused").click(function(){
                swal({   
                    title: "卖家不同意出售?",
                    text: "您提交之后代表卖家不同意出售此代购域名!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "是的卖家不同意!",
                    cancelButtonText: "不,我点错了!",
                    closeOnConfirm: false, 
                    closeOnCancel: false 
                },
                function(isConfirm){   
                    if (isConfirm) { 
                        var id = $("#ph_id").val();
                        $.post("{pigcms{:U('Staffdeal/purchase_sell_refused')}", {"ph_id":id,"status":7}, function(data){
                            alert(data);
                            if(data == 1){
                                swal("提交成功!", "等待用户确认.", "success");  
                            }else{
                                swal("异常", "您这次提交异常 :)", "error");   
                            }
                        })
                         
                    } else {     
                        swal("取消", "您取消了这次提交 :)", "error");   
                    } 
                });
            })
            
            
            
            
            
        </script>
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}
	</script>
        
        <script>
                            $("#yes").click(function () {
                                var id = $("#ph_id").val();
                                var why=$("#why").val();
                                
                                $.post("{pigcms{:U('Staffdeal/purchase_audit')}",{"ph_id":id,"status":2,"why":why},function(data){
                                    
                                    if(data == 1){
                                       showDialog('操作成功', location.href);
                                    }else{
                                        showDialog('操作异常', location.href);
                                    }
                                })
                            })
                            $("#no").click(function () {
                                var id = $("#ph_id").val();
                                var why=$("#why").val();
                                $.post("{pigcms{:U('Staffdeal/purchase_audit')}",{"ph_id":id,"status":3,"why":why},function(data){
                                    
                                    if(data == 1){
                                       showDialog('操作成功', location.href);
                                    }else{
                                        showDialog('操作异常', location.href);
                                    }
                                })
                            })
                            
//              $(.yes).click(funciton(){
//                  
//              })
        </script>
	<include file="Public:sidebar"/>
</body>
</html>
