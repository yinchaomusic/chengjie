<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>中介登录 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link type="text/css" href="{pigcms{$static_path}css/register.css" rel="stylesheet"/>
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header_staff"/>
	<include file="Public:nav_staff"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>经济人登录</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls u_ptb18">
		<div class="m-content2">
			<div class="m-title2 s-bg-fb">
				<div class="ut3">
					<p class="s-3c f-p-t8">中介登录</p>
					<p class="f-p18 s-ac">立即登录中介管理中心、管理您的客户！</p>
				</div>
			</div>
			<div class="m-login1 s-bg-white">
				<form id="login_form" action='{pigcms{:U('Staff/checkLogin')}' method="post">
					<div class="log-l">
						<ul>
							<li>
								<span class="u-txs">账户：</span>
								<span class="u-ipt-g f-img-login1">
									<input type="text" ph="请输入您的帐号" id="login_account" name="uname"/>
								</span>
							</li>
							<li>
								<span class="u-txs">密码：</span>
								<span class="u-ipt-g f-img-login2">
									<input type="password" id="login_pwd" name="password"/>
								</span>
								
							</li>
							<li><input type="submit" value="登录" class="s-bg-2a u-btn5a"/></li>
						</ul>
					</div>
				</form>
				<div class="log-r">
					中介管理中心
				</div>
				<div class="u-cls"></div>
			</div>
		</div>
	</div>
	<include file="Public:footer"/>
	<script type="text/javascript">
		var static_public="{pigcms{$static_public}",static_path="{pigcms{$static_path}",login_check="{pigcms{:U('Staff/checkLogin')}",domain_index="{pigcms{$config.site_url}",domain_login="{pigcms{:U('Login/index')}",user_index="{pigcms{:U('Account/index')}";
	</script>
	<include file="Public:sidebar"/>
</body>
</html>
