<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header_staff"/>
	<include file="Public:nav_staff"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Staff/index')}" class="s-2a">经济人账户</a>&nbsp;&gt;&nbsp;</li>
                                        <li><a href="{pigcms{:U('Staffdeal/my_client')}" class="s-2a">我的客户</a>&nbsp;&gt;&nbsp;</li>
                                        <li>客户待审核域名</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Public:left_staff"/>
        <!-- 右边 -->
        <div class="u-fr">
            <div class="m-acc-select u_be9 s-bg-white">
                     <!-- 左边 -->
        <div class="u-fl" style="width: 704px">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>客户域名列表</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 提示&警示 -->
            <div class="m-insa s-bg-fc u_be89">
                <ul>
                    <li><span class="u_fw">1.</span>添加出售的域名需要经过验证域名whois信息，以确保交易安全。</li>
                    <li><span class="u_fw">2.</span> {pigcms{$now_site_name}({pigcms{$now_site_short_url})会自动验证域名whois邮箱和用户邮箱（注册邮箱及绑定邮箱）是否匹配。</li>
                    <li><span class="u_fw">3.</span>若客户的域名使用多个邮箱注册，请先让客户在账户内<a href="/account/whoisemail" style="color:#2aa3ce;font-weight:bold"> 绑定这些邮箱</a>
。</li>
                    <li><span class="u_fw">4.</span>若{pigcms{$now_site_name}({pigcms{$now_site_short_url})无法获取域名whois信息、或您的客户的域名启用whois隐私保护，则需要客服进行人工验证。</li>
                </ul>
            </div>
            <!-- 数据 -->
            <div class="m-account-data2 s-bg-fc u_be9">
                <table>
                    <tr class="s-bg-white">
                        <th width="16px"></th>
                        <th width="20px"></th>
                        <th width="150px" class="u_tl">域名</th>
                        <th width="185px" class="u_tl">状态</th>
                        <th width="180px" class="u_tl">添加日期</th>
                        <th width="60px" class="u_tr">操作</th>
                        <th width="16px"></th>
                    </tr>
                    <volist name="list" id="vo">
                        <tr>

                            <td width="16px"></th>
                            <td width="20px"></td>
                            <td width="150px" class="u_tl">{pigcms{$vo.domain}</td>
                            <td width="185px" class="u_tl">{pigcms{$vo.is_check}</td>
                            <td width="180px" class="u_tl">{pigcms{$vo.add_time|date='Y-m-d H:i:s',###}</td>
                            <td width="60px" class="u_tl">
                                <a href="{pigcms{:U('Staffdeal/dmains_del',array('domain_id'=>$vo['domain_id']))}" onclick="return confirm('您确定要删除吗')">删除</a> | 
                                <a href="{pigcms{:U('Staffdeal/dmains_check_save',array('domain_id'=>$vo['domain_id'],'status'=>1,'uid'=>$vo['uid']))}" onclick="return confirm('是否要通过审核')">通过</a>
                                </td>
                            <td width="16px"></td>
                        </tr>
                    </volist>
                </table>
                <div id="paging" class="g-padding g-padding1">
                </div>
                <div class="m-operation3">

                    <div class="u-cls"></div>
                </div>
            </div>
        </div>
        <!-- 右边 -->

                <div id="paging" class="g-padding g-padding1">{pigcms{$pagebar}</div>
            </div>
        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<script src="{pigcms{$static_path}js/jquery-1.8.2.min.js"></script>
	<script src="{pigcms{$static_path}js/common.js"></script>
	<script src="{pigcms{$static_path}js/account.js"></script>
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}
	</script>
	<include file="Public:sidebar"/>
</body>
</html>
