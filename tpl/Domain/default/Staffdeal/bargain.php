<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
        <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header_staff"/>
	<include file="Public:nav_staff"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>经纪人&nbsp;&gt;&nbsp;</li><li>客户详情</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Public:left_staff"/>
           
                
                 <!-- 右边 -->
        <div class="u-fr">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>优质域名审核</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 选项卡 -->
            <div class="m-acc-select u_be9 s-bg-white">
                <!-- 搜索 -->
                <div class="m-acc-search">
                    <form action="{pigcms{:U('Staffdeal/bargain')}" method="post">
                        <span class="u-ipt-i u_fl">
                            <input type="text" class="i1" id="txt_domain" name="domain" ph="请输入要查询的域名" style="color: rgb(266, 216, 216);">
                        </span> 
                        <input type="submit" value="搜索" class="u-btn8">
                    </form>
                </div>
                <!-- 内容 -->
                <div class="m-acc-cons s-bg-fc u_mall1">
                    <table>
                        <tbody>
                            <tr>
                                <th width="16px"></th>
                                <th width="80px" class="u_tl">用户</th>
                                <th width="150px" class="u_tl">域名</th>
                                <th width="100px" class="u_tl">状态</th>
                                <th width="200px" class="u_tl">时间</th>
                                <th width="8px"></th>
                                <th width="100px;">操作</th>
                                
                            </tr>
                            <if condition="is_array($bargain_list)">
                                <volist name="bargain_list" id="vo">
                                    <tr>
                                        <td width="16px"></th>
                                        <td width="80px" class="u_tl">{pigcms{$vo.uid}</td>
                                        <td width="150px" class="u_tl">{pigcms{$vo.domain}</td>
                                        <td width="100px" class="u_tl"><if condition="$vo['status'] eq 0">待审核</if></td>
                                        <td width="200px" class="u_tl">{pigcms{$vo.add_time|date='Y-m-d H:i:s',###}</td>
                                        <td width="8px"></td>
                                        <td width="100px;">
                                            <a href="{pigcms{:U('Staffdeal/bargain_save',array('domain_id'=>$vo['domain_id'],'status'=>1))}" onclick="return confirm('确定要通过吗？')" class="bargain_ok" data-bargain_ok_id ="{pigcms{$vo.domain_id}">通过</a> |
                                            <a href="{pigcms{:U('Staffdeal/bargain_save',array('domain_id'=>$vo['domain_id'],'status'=>3))}" onclick="return confirm('确定要拒绝吗？')"  class="bargain_no" data-bargain_no_id ="{pigcms{$vo.domain_id}">拒绝</a>
                                        </td>
                                        
                                    </tr>
                                </volist>
                            <else/>
                                    <tr><td class="u_nb u_tc u_norecord" colspan="9">暂无记录！</td></tr>
                            </if>
                            </tbody>
                    </table>
                </div>
                <!-- 分页 -->
                <div id="paging" class="g-padding g-padding1">{pigcms{$pagebar}</div>
            </div>
        </div>
                
                

		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<script src="{pigcms{$static_path}js/account.js"></script>
        <script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">
<!--        
        <script>
            $(".bargain_ok").click(function(){
                swal({   
                    title: "确认通过?",   
                    text: "您是否要通过这条信息的审核!",   
                    type: "warning",  
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "是, 我要通过!",   
                    closeOnConfirm: false }, 
                function(){   
                    
                    var bargain_id= jQuery("#bargain_ok").data('bargain_ok_id');
                    alert(bargain_id);
                    swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
                });
            })
            
            
            $(".bargain_no").click(function(){
                swal({   
                    title: "确认通过?",   
                    text: "您是否要通过这条信息的审核!",   
                    type: "warning",  
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "是, 我要通过!",   
                    closeOnConfirm: false }, 
                function(){   
                    $jqery().data(bargain_id);
                    swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
                });
            })
            
            
    </script>-->
        
        
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}
	</script>
	<include file="Public:sidebar"/>
</body>
</html>
