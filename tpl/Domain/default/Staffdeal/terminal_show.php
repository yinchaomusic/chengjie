<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我是买家 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/jquery-ui.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav_staff"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>我是买家&nbsp;&gt;&nbsp;</li>
					<li>我给出的报价&nbsp;&gt;&nbsp;</li>
					<li>{pigcms{$now_quote.domain}</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Public:left_staff"/>
        <!-- 右边 -->
        <div class="u-fr">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>终端审核</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 交易详情 -->
            
            
            
            <div class="m-account-data1 u_be9 s-bg-fc">
                <table>
                    <tbody><tr>
                        <th width="16px"></th>
                        <th width="200px" class="u_tl">域名</th>
                        <th width="120px" class="u_tr">希望终端</th>
                        <th width="120px" class="u_tr">价格</th>
                        <th width="120px" class="u_tr">价格</th>
                        <th width="16px"></th>
                    </tr>
                        <tr>
                            <td></td>
                            <td class="u_tl"><span class="s-2a">{pigcms{$show.domain_name}</span></td>
                            <td class="u_tr"><span>{pigcms{$show.hope}</span></td>
                            <td class="u_tr"><span class="s-75b">￥{pigcms{$show.max_price}</span></td>
                            <td class="u_tr"><span class="s-75b">￥{pigcms{$show.min_price}</span></td>
                            <td></td>
                        </tr>  
                </tbody></table>
                
            </div>
            
            <div class="m-seller s-bg-fc u_be9">
				<div class="m-insd">
                                    <ul class="insd">
                                            <li>{pigcms{$show.tt_msg}</li>
                                            <input type="hidden" name="id" id="t_id" value="{pigcms{$show.id}"/>
                                    </ul>
                                    <if condition="$show['status'] lt 1">
                                        <p class="u_btn_container2"> 
                                        
                                            <a class="u-btn13" id="ok">通过</a>
                                            <a class="u-btn13" id="outprice">拒绝</a>
                                    </p>
                                    <else/>
                                       <p class="u_btn_container2"> 
                                                <a class="u-btn13">已审核</a>
                                        </p>
                                    </if>
                                    
				</div> 
                
                
                                <div class="m-pop s-bg-fa" id="pop16">
                                    <p class="f-p-t7 u_bb0">
                                        <span class="u-fl">提交一个拒绝的理由</span>
                                        <span class="u-fr close"></span>
                                        <span class="u-cls"></span>
                                    </p>
                                    <div style="padding: 10px;">
                                        <span style="margin: 10px 0 0 15px">拒绝理由：</span></p>
                                        <div style="margin: 10px 20px 10px 15px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 1px solid rgb(235, 235, 235);">
                                            <textarea maxlength="100" rows="3" style="width: 100%; padding: 5px;" id="reason"></textarea>
                                        </div>
                                    </div>
                                    <div class="s-bg-fa u_pa15 u_tr">
                                        <input type="button" value="确定" class="u-btn6 s-bg-2a" id="refuse">
                                    </div>
                                </div>
                
            </div>
            
            
        </div>
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<script type="text/javascript" src="{pigcms{$static_path}js/popupBox.js"></script>
        <script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">
	<script type="text/javascript">
            
            $("#refuse").click(function(){
                var reason = $("#reason").val();
                var id = $("#t_id").val();
                $.post("{pigcms{:U('Staffdeal/refuse')}",{"id":id,"reason":reason},function(data){
                    if(data == 1){
                        swal("成功!", "已成功拒绝此条终端域名!", "success")
                        location.href = location.href;
                    }else{
                        swal("异常", "此次审核异常 :)", "error");
                    }
                })
            })
            
            $("#ok").click(function(){
                var id = $("#t_id").val();
                $.post("{pigcms{:U('Staffdeal/pass')}",{"id":id},function(data){
                    if(data == 1){
                        swal("成功!", "已成功审核此条终端域名!", "success")
                        location.href = location.href;
                    }else{
                        swal("异常", "此次审核异常 :)", "error");
                    }
                })
            })
            
        $(".close").click(function () {
            var id = $(this).parent().parent().attr("id");
            closeStr(id);
        });
        function closeStr(str){
            closeBox(str);
        }
        
        
        $("#outprice").click(function () {
            showBox("pop16");
            $("#pop16").css("left", (($(window).width() - 544)/2)+"px");
        });
        
    </script>
	<include file="Public:sidebar"/>
</body>
</html>
