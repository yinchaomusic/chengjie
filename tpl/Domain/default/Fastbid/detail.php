<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>{pigcms{$now_domain.domain} - 极速竞价 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/common.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<style type="text/css">
        .instr2-r {
            position: absolute;
            right: 4px;
            bottom: 4px;
        }
		.instr2-r a {
			float: left;
		}
		table{
			border-collapse: collapse;
			border-spacing: 0;
		}
		td {
    font-size: 14px;
    line-height: 140%;
}
		td.td_bor {
    border-right: 2px solid #fff;
}
.bg_pink td {
    background: #fff2f2;
    text-align: center;
	font-weight:normal;
}
.bid_info td {
    padding: 10px;
    word-break: break-all;
	    vertical-align: middle;
}
.bid_info .bg_pink td em {
    color: #ff3535;
    font-size: 18px;
	font-style: normal;
}
.c_red {
    color: #ff3535;
}
.info_table_box table td {
    padding: 10px 5px;
    line-height: 26px;
    word-break: break-all;
	font-size:12px;
	font-weight:normal;
}
.bor_td table td {
    border-bottom: 1px solid #f1f1f1;
}
.info_table_box table .align_right {
    text-align: right;
    padding-right: 20px;
}
.bor_td table td {
    border-bottom: 1px solid #f1f1f1;
}
.info_table_box .com_input {
	border:1px solid #c2c2c2;
    padding: 7px 5px;
}
.btn_bor {
    border-top: 1px solid #f1f1f1;
    padding-top: 20px;
    text-align: center;
}
.bor_td .btn_bor, .bor_td .bor_none td {
    border: none;
}
#postPriceNum{
	border:none!important;
	display:inline-block;
	margin-right:10px;
}
    </style>

</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Fastbid/index')}" class="s-2a">极速竞价</a>&nbsp;&gt;&nbsp;</li>
					<li>{pigcms{$now_domain.domain}</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
		<div class="m-content">
			<!-- 左边 -->
			<div class="u-fl">
				<div class="g-a-container">
					<div class="m-ta s-bg-white u_be9">
						<div class="title">
							<a class="s-3c" href="/fast-bid">
								<span class="z-img-t4"></span>
								<span>极速竞价</span>
							</a>
						</div>
						<div class="u-cls"></div>
					</div>
					<div class="m-ta s-bg-white u_be9" style="margin-top:6px;">
						<h3 class=" u-h3b" style="line-height: 50px; position: relative">{pigcms{$now_domain.domain}
							<div class="instr2-r" style="bottom: 12px;">
                                                            <if condition="$collection_show eq 1">
                                                                <a title="收藏域名" id="collection" style="width: 25px; background-image:url({pigcms{$static_path}images/account/sc.png)"></a>
                                                                <else/>
                                                                <a title="收藏域名" id="collection" style="width: 25px;background-image:url({pigcms{$static_path}images/account/wsc.png)"></a>
                                                            </if>
                                                            <input type="hidden" value="{pigcms{$now_domain.domain_id}" id="domainid" />
                                                            <input type="hidden" value="{pigcms{$now_domain.domain}" id="domain_name" />
                                                            <input type="hidden" value="{pigcms{$now_domain.type}" id="domain_type" />
								<a href="" id="qqRemind" target="_blank" title="QQ提醒" style="background: none; width: auto; line-height: 24px;">
									<img src="http://i.gtimg.cn/snsapp/app/bee/widget/img/2_1.png" style="border: 0px;"/>
								</a>
								<a href="{pigcms{:U('Whois/index',array('keyword'=>$now_domain['domain']))}" target="_blank" title="Whois查询" class="f-img-i6"></a>
								<a href="http://www.baidu.com/s?wd={pigcms{$now_domain.domain}" target="_blank" title="百度搜索" class="f-img-i2"></a>
							</div>
						</h3>
					</div>
					<div class="m-ta s-bg-white u_be9" style="margin-top:6px;height:auto;padding:0;">
						<table width="100%" class="bid_info">
							<colgroup>
								<col width="25%">
								<col width="25%">
								<col width="25%">
								<col>
							</colgroup>
							<tbody>
								<tr class="bg_pink">
									<td class="td_bor">当前价格<br><em><span id="bidPrice"><if condition="$now_domain['lastFidId']">{pigcms{$now_domain['money']}<else/>{pigcms{$now_domain['mark_money']}</if></span></em>元</td>
									<td class="td_bor">剩余时间<br>
                                                                            <span id="leftTime" class="c_red countdown_new">
                                                                                <if condition="$now_domain['overplus_time_d']"><strong>{pigcms{$now_domain.overplus_time_d}</strong>天</if>
                                                                                <strong>{pigcms{$now_domain.overplus_time_h}</strong>时
                                                                                <strong>{pigcms{$now_domain.overplus_time_m}</strong>分
                                                                                <strong>{pigcms{$now_domain.overplus_time_ms}</strong>秒
                                                                            </span>
                                                                        </td>
									<td class="td_bor" id="nickName"><if condition="$fidList">当前领先<br/>{pigcms{$_SERVER.REQUEST_TIME|date='Ymd',###}-{pigcms{$fidList.0.uid}<else/>暂无人出价</if></td>
									<td>出价次数<br><span id="bidCount">{pigcms{:count($fidList)}</span>次</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="m-ta s-bg-white u_be9 info_table_box bor_td" style="margin-top:6px;height:auto;padding:0;">
						<table width="100%">
							<colgroup>
								<col width="20%">
								<col>
							</colgroup>
							<tbody>
								<tr>
									<td class="align_right">域名简介</td>
									<td>
										{pigcms{$now_domain.desc} 
										<span class="c_gray"> (简介仅供参考)</span>
									</td>
								</tr>
								<tr>
									<td class="align_right">交易类型</td>
									<td>
									竞价								</td>
								</tr>
								<tr>
									<td class="align_right">起拍价</td>
									<td><span id="askingPrice">{pigcms{$now_domain['mark_money']}</span> 元</td>							
								</tr>
								<tr>
									<td class="align_right">围观次数</td>
									<td><span id="askingPrice">{pigcms{$now_domain['hits']}</span> 次</td>							
								</tr>
								<tr>
									<td class="align_right">拍卖周期</td>
									<td><span id="transCycle">{pigcms{$now_domain.startTime|date='Y-m-d H:i',###} - {pigcms{$now_domain.endTime|date='Y-m-d H:i',###}</span></td>
								</tr>
								<tr class="last">
									<td class="align_right">加价幅度</td>
									<td><span id="amp">￥{pigcms{$now_domain.plusPrice.plusPrice}</span></td>
								</tr>
								<tr class="last">
									<td class="align_right">我要出价</td>
									<td>
										<span id="postPriceNum"><input type="text" style="width:80px;" name="postPrice" id="cph_txt_bj" readonly disabled value="{pigcms{$now_domain['pleasePrice']}" class="com_input w_100"/> 元</span>
										<span class="c_red">(将冻结 <span id="deposit">{pigcms{$now_domain.plusPrice.depositPrice}</span> 元作为保证金)</span><span id="agentPrice"></span>
										<input type="hidden" id="agentPriceVal" value="0">
									</td>
								</tr>
								<tr class="last" style="display:none;">
									<td colspan="2">
										<center>
											<input type="checkbox" id="makesure" name="makesure" value="1">
											<label for="makesure">我已阅读并同意  《<a href="#" target="_blank">域名交易服务协议</a>》和《<a href="#" target="_blank">临时模板使用协议</a>》</label>
										</center>
									</td>
								</tr>
								<tr class="last" id="msgarea" style="display: none;">
									<td class="align_right"></td>
									<td class="c_red" id="msgshow"></td>
								</tr><tr>
							</tr></tbody>
						</table>
						<div class="btn_bor">
							<p style="line-height: 20px; margin: 12px 0 2px 19px;display:none;">
								<input type="checkbox" style="margin-left: 1px; vertical-align: middle; cursor: pointer;" id="agree" checked="checked"/>
								<span style="vertical-align: middle; font-size: 14px;">我已阅读并同意<a href="#" target="_blank" title="阅读" style="color: #2aa3ce;">《域名交易服务协议》</a></span>
							</p>
							<input type="button" value="立即出价" class="s-bg-2a u-btn7a" id="jsbj" style="margin-left:0px;"/>
							<input type="button" value="刷新此页" class="s-bg-2a u-btn7a" onclick="history.go(0);" style="margin-left:10px;"/>
						</div>
					</div>
					<div class="m-ta s-bg-white u_be9 u_mt6">
						<div class="title">
							<a class="s-3c" href="javascript:void(0)">
								<span>出价记录</span>
							</a>
						</div>
						<div class="u-cls"></div>
					</div>
					<div class="m-iner-data s-bg-fc">
						<table>
							<tr>
								<th width="16px"></th>
								<th width="150px" class="u_tl">状态</th>
								<th width="165px" class="u_tl">竞价人</th>
								<th width="150px" class="u_tr">价格</th>
								<th width="200px" class="u_tr">出价时间</th>
								<th width="16px"></th>
							</tr>
							<volist name="fidList" id="vo">
								<tr>
									<td></td>
									<td class="u_tl">
										<if condition="$i eq 1">
											<span class="z-img-hot2 s-bg-2a">领先</span>
										<else/>
											<span class="z-img-hot2 s-bg">出局</span>
										</if>
									</td>
									<td class="u_tl"><span>{pigcms{$_SERVER.REQUEST_TIME|date='Ymd',###}-{pigcms{$vo.uid}</span></td>
									<td class="u_tr s-8f">￥{pigcms{$vo.money}</td>
									<td class="u_tr s-8f">{pigcms{$vo.time|date='Y-m-d H:i:s',###}</td>
									<td></td>
								</tr> 
							</volist>
						</table>
					</div>
				</div>
			</div>
			<!-- 右边 -->
			<div class="u-fr">
				<div class="g-c-container">
					<div class="m-processList u_mt1 u_mb6">
						<h3 class="cli">竞价流程</h3>
						<div class="process" style="display: block;">
							<ul>
								<li>
									<span class="f-img-s3">1</span><span class="u-txh">参与竞价</span>
								</li>
								<li>
									<span class="f-img-s3">2</span><span class="u-txh">持续竞价</span>
								</li>
								<li>
									<span class="f-img-s3">3</span><span class="u-txh">竞价结束</span>
								</li>
								<li>
									<span class="f-img-s3">4</span><span class="u-txh">最高出价者得标</span>
								</li>
								<li class="u_mb0">
									<span class="f-img-s3">5</span><span class="u-txh">平台自动发起交易</span>
								</li>
							</ul>
						</div>
					</div>
					<div class="m-bid-rules s-bg-eb">
						<h3 class="cli">竞价规则</h3>
						<div class="rules">
							<p class="f-p-t4">竞价时间：</p>
							<p class="f-p-t5">竞价时间固定为当天</p>
							<p class="f-p13 s-2a">00:00-23:00</p>
							<p class="f-p14 u_mb30">竞价时间与结束时间以{pigcms{$now_site_short_url}系统提示时间为准。</p>
							<!--p class="f-p-t4">代理竞价：</p>
							<p class="f-p14 u_mb30">当您的出价大于“当前价格+加价幅度”系统将记录您的出价(为代理价)。当其他买家出价时，系统自动帮您在他人出价的基础上以最小加价幅度出价，直到您的代理价被其他买家超过为止</p-->
							<p class="f-p-t4">竞价延时：</p>
							<p class="f-p14">竞价即将结束的5分钟内，如果有新的出价，系统将会自动延长5分钟。直到结束5分钟内无人出价为止，届时拍卖结束。</p>
						</div>
					</div>
				</div>
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<include file="Public:footer"/>
	<include file="Public:sidebar"/>
	<div class="s-bg-f7"></div>
	<script type="text/javascript" src="{pigcms{$static_path}js/popupBox.js"></script>
	<script type="text/javascript" src="{pigcms{$static_path}js/biddetail.js"></script>
	<input type="hidden" id="fastid" value="{pigcms{$now_domain.domain_id}" />
	<div class="m-pop s-bg-fa" id="pop2">
		<p class="f-p-t7 u_bb0">
			<span class="u-fl">竞拍出价阶梯表</span>
			<span class="u-fr close"></span>
			<span class="u-cls"></span>
		</p>
		<table>
			<tr>
				<th width="20px"></th>
				<th width="200px" class="u_tl">价格区间</th>
				<th width="150px" class="u_tr">出价保证金</th>
				<th width="153px" class="u_tr">加价幅度</th>
				<th width="20px"></th>
			</tr>
			<volist name="allPlusPrice" id="vo">
				<tr>
					<td></td>
					<td><span class='u-txka'>≥ {pigcms{$vo.minPrice|number_format=###} </span><span class='u-txka'><if condition="$vo['maxPrice']"> &lt; {pigcms{$vo.maxPrice|number_format=###}</if></span></td>
					<td class="u_tr s-75b">￥{pigcms{$vo.depositPrice}</td>
					<td class="u_tr s-75b">{pigcms{$vo.plusPrice}</td>
					<td></td>
				</tr>
			</volist>
		</table>
	</div>
	<input id="config_name" value="{pigcms{$config.site_name}"/>
        <script>
            $("#collection").click(function(){
                var domain_id = $("#domainid").val();
                var domain = $("#domain_name").val();
                var type = $("#domain_type").val();
                $.post("{pigcms{:U('Hotsale/collection')}",{'domain_id':domain_id,'domain':domain,'type':type}, function(data){
                    if(data == 1){
                        alert('收藏成功');
                        location.href=location.href;
                    }else if(data == 3){
                        alert('已经收藏');
                    }else if(data == 4){
                        alert('请先登录');
                    }else{
                        alert('失败请重试');
                    }
                })
            })
    </script>
</body>
</html>