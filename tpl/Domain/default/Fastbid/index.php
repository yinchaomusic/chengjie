<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>正在竞价的域名 - 域名购买 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/common.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bragin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>极速竞价</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
		<div class="m-content">
			<div class="m-ta u_be9 s-bg-fc u_mb6">
				<div class="title">
					<a class="s-bg-2a a1" href="{pigcms{:U('Fastbid/index')}">
						<span class="z-img-t6"></span>
						<span>极速竞价</span>
						<b></b>
					</a>
					<!--a class="a2 s-3c" href="{pigcms{:U('Fastbid/sold')}">
						<span class="z-img-t14"></span>
						<span>最近成交</span>
					</a-->
				</div>
				<div class="u-cls"></div>
			</div>
			<div class="m-conditions u_df s-bg-white">
				<div class="m-keyword-a s-bg-fb u_bb0">
					<div class="f-keytxt u-fl">域名关键字：</div>
					<div class="u-fl" id="keyworda">
						<span class="u-ipt-b u_b4 u_mr25">
							<input type="text" ph="输入您要搜索的关键字" />
						</span>
						<label class="f-label">
							<input type="radio" checked nu="0" name="keyword" /><span>包含</span>
						</label>
						<label class="f-label">
							<input type="radio" nu="1" name="keyword" /><span>开始</span>
						</label>
						<label class="f-label">
							<input type="radio" nu="2" name="keyword" /><span>结尾</span>
						</label>
						<input type="button" value="搜  索" k='2' class="button u-btn1 s-btn-2a" />
					</div>
					<div class="u-cls"></div>
				</div>
				<div id="g_sh">
					<div class="m-keyword-a u_bb0 u-cls">
						<div class="f-keytxt u-fl">排除关键字：</div>
						<div class="u-fl" id="keywordb">
							<span class="u-ipt-b u_b4 u_mr25">
								<input type="text" ph="输入您要排除的关键字" />
							</span>
							<label class="f-label">
								<input type="radio" nu="0" checked name="keyword1" /><span>包含</span>
							</label>
							<label class="f-label">
								<input type="radio" nu="1" name="keyword1" /><span>开始</span>
							</label>
							<label class="f-label">
								<input type="radio" nu="2" name="keyword1" /><span>结尾</span>
							</label>
						</div>
						<div class="u-cls"></div>
					</div>
					<div class="m-hz u_bb0 u-cls" id="m-hz" name="hz">
						<div class="f-keytxta u-fl">后缀：</div>
						<div class="f-hz u-fl">
							<span class="z-cell">全部</span>
							<volist name="suffixList" id="vo">
								<span idx="{pigcms{$vo.id}">{pigcms{$vo.suffix|substr=###,1}</span>
							</volist>
							<span idx="other">其它后缀</span>
						</div>
						<div class="u-cls"></div>
					</div>
					<div class="m-leng u_bb0 u-cls">
						<div class="f-keytxta u-fl">域名长度：</div>
						<div class="f-leng u-fl">
							<div class="f-slider u-fl" style="margin-right: 6px;">
								<div style="width: 380px; height: 7px; margin-top: 7px;" id="domian-len" name="len"></div>
								<ol>
									<li>1</li>
									<li>2</li>
									<li>3</li>
									<li>4</li>
									<li style="width: 46px; padding-left: 1px;">5</li>
									<li style="width: 45px; padding-left: 2px;">6</li>
									<li style="width: 45px; padding-left: 2px;">7</li>
									<li style="width: 38px; padding-left: 3px;">8</li>
									<li>不限</li>
								</ol>
							</div>
							<div class="f-range-sure u-fr">
								<span class="u-ipt-c">
									<input type="text" val="" />
								</span>
								<span class="u_ver_line"></span>
								<span class="u-ipt-c">
									<input type="text" val="" />
								</span>
								<input type="button" value="确定" class="u-btn2 s-bg-55b" />
							</div>
							<div class="u-cls"></div>
						</div>
						<div class="u-cls"></div>
					</div>
					<div class="m-leng u_bb0 u-cls">
						<div class="f-keytxta u-fl">当前价格：</div>
						<div class="f-leng u-fl">
							<div class="f-slider">
								<div style="width: 380px; height: 7px; margin-top: 7px;" id="domian-pri" name="pri"></div>
								<ol>
									<li style="width: 40px;">0</li>
									<li style="width: 40px;">500</li>
									<li>1000</li>
									<li>&nbsp;&nbsp;2万</li>
									<li>&nbsp;&nbsp;5万</li>
									<li>&nbsp;10万</li>
									<li>&nbsp;&nbsp;20万</li>
									<li style="width: 42px;">&nbsp;&nbsp;50万</li>
									<li style="text-align: right;">不限&nbsp;</li>
								</ol>
							</div>
						</div>
						<div class="f-range-sure u-fl u_ml19">
							<span class="u-ipt-c">
								<input type="text" val="" />
							</span>
							<span class="u_ver_line"></span>
							<span class="u-ipt-c">
								<input type="text" val="" />
							</span>
							<input type="button" value="确定" class="u-btn2 s-bg-55b" />
						</div>
						<div class="u-cls"></div>
					</div>
					<div class="m-style u_bb0 u-cls" name="ty">
						<div class="f-keytxta u-fl">类型：</div>
						<div class="f-style u-fl">
							<div class="f-contions" id="m-style">
								<span class="z-cell">全部</span>
								<span lt="0">数字</span>
								<span lt="1">字母</span>
								<span lt="2">杂</span>
							</div>
							<div class="s-bg-f9" id="data_style" name="ty">
								<div class="f-result">
									<span class="z-cell" idx="a" tag="all">全部</span>
									<span idx="a1">5个相同数字</span><span idx="a2">4个相同数字</span><span idx="a3">3个相同数字</span><span idx="a4">2个相同数字</span><span idx="a5">顺子</span><span idx="a6">区号</span><span idx="a7">升序</span><span idx="a8">降序</span><span idx="a9">不带4</span><span idx="a0">不带0</span> 
								</div>
								<div class="f-result" idx="b">
									<span idx="b" tag="all">全部</span>
									<span idx="b1">单拼</span><span idx="b2">双拼</span><span idx="b3">三拼</span><span idx="b4">叠拼</span><span idx="b5">cvcv</span>
								</div>
								<div class="f-result" idx="c">
									<span idx="c" tag="all">全部</span>
									<span idx="c1">数字在前</span><span idx="c2">字母在前</span><span idx="c3">NNL</span><span idx="c4">NLN</span><span idx="c5">LNN</span><span idx="c6">LLN</span><span idx="c7">LNL</span><span idx="c8">NLL</span>
								</div>
							</div>
						</div>
						<div class="u-cls"></div>
					</div>
					<div class="m-poperty u_bb0 u-cls" name="oper">
						<div class="f-keytxta u-fl">主题属性：</div>
						<div class="f-poperty u-fl" id="theme_oper">
							<span class="z-cell">全部</span>
							<volist name="treasureList" id="vo">
								<span idx="{pigcms{$vo.id}">{pigcms{$vo.name}</span>
							</volist>
						</div>
						<div class="u-cls"></div>
					</div>
					<div class="m-selconditions u-cls">
						<div class="f-keytxta u-fl">已选条件：</div>
						<div class="selconditions u-fl" id="selConditions">
						</div>
						<div class="u-cls"></div>
					</div>
				</div>
			</div>
			<div class="m-up-down u_mb6" id="m-up-down">
				<div class="up s-bg-white u_df0">收起</div>
				<div class="down s-bg-fb u_df0">展开更多[后缀 长度 价格 类型 主题]</div>
			</div>
			<div class="m-relsult2">
				<div class="g-a-container u_df1 s-bg-white" style="position:relative;width:100%;">
					<div class="m-sort s-bg-fb u_bb0" id="sort">
						<span idx='0' class='z-up'>按域名长度排序</span>
						<span idx='2' class="z-up">按当前价格排序</span>
						<span idx='5' class="z-active z-down">按剩余时间排序</span>
					</div>
					<table class="u-cls">
						<tr class='s-bg-white'>
							<th width='16px'></th>
							<th width='20px'></th>
							<th width='42px'></th>
							<th width='175px' class='u_tl'>域名</th>
							<th width='54px'>标签</th>
							<th width='198px'>主题</th>
							<th width='120px' class='u_tr'>当前价格</th>
							<th>剩余时间</th>
							<th width='80px'></th>
							<th width='16px'></th>
						</tr>
						<volist name="domainList" id="vo">
							<tr>
								<td></td>
								<td>{pigcms{$i}</td>
								<td>
									<if condition="$vo['type'] eq 1">
										<span class="z-img-b1" title="一口价"></span>
									<elseif condition="$vo['type'] eq 2"/>
										<span class="z-img-b4" title="极速竞价"></span>
									<else/>
										<span class='block fl'></span>
									</if>
								</td>
								<td class='u_tl'>
									<a title="{pigcms{$vo.domain}" href="{pigcms{$vo.url}" target='_blank' class='u-txa s-def'>{pigcms{$vo.domain}</a>
								</td>
								<td>{pigcms{$vo.label}</td>
								<td>其它</td>
								<td class='u_tr'><if condition="$vo['money']">￥{pigcms{$vo.money|floatval=###|number_format=###}<else/>买家报价</if></td>
								<td>{pigcms{$vo.over_time}</td>
								<td class='u_tr'><a href="{pigcms{$vo.url}" target='_blank' class='u-abtn-xq s-def'>详情</a></td>
								<td></td>
							</tr>
						</volist>
					</table>
				</div>
				<div class="u-cls"></div>
			</div>
			<div id="paging" class="g-padding" style="width:100%;">
				<div class="u-fl"><span class='u_fw'>{pigcms{$allCount}</span>条记录，<span class='u_fw'>{pigcms{$allPage}</span>页</div>
				<div class="u-fr">
					{pigcms{$page}
				</div>
				<div class='u-cls'></div>
			</div>
		</div>
		<div class="u-cls"></div>
	</div>
	<include file="Public:footer"/>
	<include file="Public:sidebar"/>
	<div class="s-bg-f7"></div>
	<script type="text/javascript" src="{pigcms{$static_path}js/jquery.color.js"></script>
	<script type="text/javascript" src="{pigcms{$static_path}js/jquery-ui.js"></script>
	<script type="text/javascript" src="{pigcms{$static_path}js/bragin.js"></script>
</body>
</html>