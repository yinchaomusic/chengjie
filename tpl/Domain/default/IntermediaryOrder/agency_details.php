<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<style type="text/css">
     span.u-ipt-d{
	   width:260px;
     }
   
    </style>
</head>
<body class="s-bg-global">
	<include file="Public:header_staff"/>
	<include file="Public:nav_staff"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Staff/index')}" class="s-2a">经济人账户</a>&nbsp;&gt;&nbsp;</li>
                                        <li><a href="{pigcms{:U('Staffdeal/purchase_list')}" class="s-2a">代购列表</a>&nbsp;&gt;&nbsp;</li>
                                        <li>中介详情</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Public:left_staff"/>
        <!-- 右边 -->
        <div class="u-fr">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>中介详情</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <div class="m-acc-select u_be9 s-bg-white">
           <!-- 左边 -->
           
            <!-- 数据 -->
             <div class="m-question m-question1 s-bg-fc u_be9">
                <ul>
                    <div class="transa">
                            <p class="u_mb16">
                                <span class="u-txj">发起人UID：</span>
                                <span class="u-ipt-d">{pigcms{$info.user_id}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">接受人UID：</span>
                                <span class="u-ipt-d">{pigcms{$info.other_user_id}</span>
                            </p>
                              <p class="u_mb16">
                                <span class="u-txj">服务类型：</span>
                                 <if condition="$info['inter_obj'] eq 1">
                                     <span class="u-ipt-d">买方</span> 
                                     <elseif condition="$info['inter_obj'] eq 2"/>
                                     <span class="u-ipt-d">卖方</span> 
                                     <else/>
                                	 <span class="u-ipt-d">未知</span> 
                                </if>
                            </p>
                            
                            <p class="u_mb16">
                                <span class="u-txj">域名：</span>
                                <span class="u-ipt-d">{pigcms{$info.domain}</span>
                            </p>
                     
                            <p class="u_mb16">
                                <span class="u-txj">日ip pc：</span>
                                <span class="u-ipt-d">{pigcms{$info.ip_pc}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">日ip app：</span>
                                <span class="u-ipt-d">{pigcms{$info.ip_app}</span>
                            </p>
                               <p class="u_mb16">
                                <span class="u-txj">源码：</span>
                                <span class="u-ipt-d">{pigcms{$info.souce_code}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">月收入元：</span>
                                <span class="u-ipt-d">{pigcms{$info.month_income}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">服务器信息：</span>
                                <span class="u-ipt-d">{pigcms{$info.server_info}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">第三方平台名称：</span>
                                <span class="u-ipt-d">{pigcms{$info.third_party}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">更新方式：</span>
                                <span class="u-ipt-d">{pigcms{$info.update_type}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">广告联盟：</span>
                                <span class="u-ipt-d">{pigcms{$info.advert_aliance}</span>
                            </p>
                           
                            <p class="u_mb16">
                                <span class="u-txj">联盟帐号处理：</span>
                                 <if condition="$info['aliance_acc_handle'] eq 1">
                                	 <span class="u-ipt-d">删除帐号</span> 
                                	<elseif condition="$info['aliance_acc_handle'] eq 2"/>
                                	 <span class="u-ipt-d">交接帐号</span> 
                                	<else/>
                                	 <span class="u-ipt-d">未知</span> 
                                </if>
                            </p>
                             <p class="u_mb16">
                                <span class="u-txj">帮忙搬迁：</span>
                                 <if condition="$info['is_help_move'] eq 1">
                                	 <span class="u-ipt-d">不需要</span> 
                                	<elseif condition="$info['is_help_move'] eq 2"/>
                                	 <span class="u-ipt-d">需要</span> 
                                	<else/>
                                	 <span class="u-ipt-d">未知</span> 
                                </if>
                            </p>
                             <p class="u_mb16">
                                <span class="u-txj">价格：</span>
                                <span class="u-ipt-d">{pigcms{$info.price}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">平台手续费：</span>
                                <span class="u-ipt-d">{pigcms{$info.platform_fee}</span>
                            </p>
                             <p class="u_mb16">
                                <span class="u-txj">总额：</span>
                                <span class="u-ipt-d">{pigcms{$info.all_price}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">手续费出资方：</span>
                                 <if condition="$info['plat_fee_obj'] eq 1">
                                	 <span class="u-ipt-d">买家付</span> 
                                	<elseif condition="$info['plat_fee_obj'] eq 2"/>
                                	 <span class="u-ipt-d">卖家付</span>
                                	<elseif condition="$info['plat_fee_obj'] eq 3"/>
                                	 <span class="u-ipt-d">各一半</span>  
                                	<else/>
                                	 <span class="u-ipt-d">未知</span> 
                                </if>
                            </p>
                            
                            <p class="u_mb16">
                                <span class="u-txj">手机号码：</span>
                                <span class="u-ipt-d">{pigcms{$info.phone}</span>
                            </p>
                              <p class="u_mb16">
                                <span class="u-txj">是否同意：</span>
                                 <if condition="$info['is_agent'] eq 0">
                                	 <span class="u-ipt-d">不同意</span> 
                                	<elseif condition="$info['is_agent'] eq 1"/>
                                	 <span class="u-ipt-d">同意</span> 
                                	<else/>
                                	 <span class="u-ipt-d">未知</span> 
                                </if>
                            </p>
                             <p class="u_mb16">
                                <span class="u-txj">网站类型：</span>
                               
                                <select name="web_type" width="250px">
                                   <option value="0">-未知-</option>
                                   <option value="1" <if condition="$info['web_type'] eq 1"> selected</if> >-行业门户-</option>
                                   <option value="2" <if condition="$info['web_type'] eq 2"> selected</if>>-音乐影视-</option>
                                   <option value="3" <if condition="$info['web_type'] eq 3"> selected</if>>-游戏小说-</option>
                                   <option value="4" <if condition="$info['web_type'] eq 4"> selected</if>>-女性时尚-</option>
                                   <option value="5" <if condition="$info['web_type'] eq 5"> selected</if>>-QQ/娱乐-</option>
                                   <option value="6" <if condition="$info['web_type'] eq 6"> selected</if>>-商城购物-</option>
                                   <option value="7" <if condition="$info['web_type'] eq 7"> selected</if>>-地方网站-</option>
                                   <option value="8" <if condition="$info['web_type'] eq 8"> selected</if>>-其他网站-</option>
                                </select>
                            </p>
                        
                            <p class="u_mb16">
                                <span class="u-txj">pr值：</span>
                                <span class="u-ipt-d">{pigcms{$info.pr}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">创建时间：</span>
                                <span class="u-ipt-d">{pigcms{$info.createdate}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">订单流水：</span>
                                <span class="u-ipt-d">{pigcms{$info.order_flow}</span>
                            </p>
                            <p>
                                    <span class="u-txj">备注信息：</span>
                                    <span class="u_txera s-bg-white">{pigcms{$info.description}</span>
                            </p>
                        </div>
                       
                        

                             <li>
                                <div class="u_mall12">
                                    <input type="hidden" name="id" value="{pigcms{$info.id}" id="id"/>
                                       <if condition="$info['status'] eq 0">
                                            <input type="submit" id="intervene" value="介入" class="u-btn12 s-bg-2a"> 
                                            <input type="submit" id="close" value="关闭" class="u-btn12 s-bg-2a">
                                        <elseif condition="$info['status'] eq 3"/>
                                            <input type="submit" id="transfer" value="划拨资金" class="u-btn12 s-bg-2a">
                                      </if>
                                </div>
                            </li> 
                            <if condition="$info['status'] eq 0">
                            <p>
                                <span class="u-txj">理由</span>
                                <span class="u_txera s-bg-white">
                                    <textarea name="fail_reason" id="fail_reason"></textarea>
                                </span>
                            </p>
                            </if>
                           
                    </ul>
                    <div class="u-cls"></div>
                </div>
        <!-- 右边 -->
        </div>
		</div>
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<script src="{pigcms{$static_path}js/account.js"></script>
        <script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
        <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">
        <script>
            $("#intervene").click(function(){
          	 var id = $("#id").val();
           	 swal({   
                 title: "介入?",   
                 text: "您确定要介入吗!",   
                 type: "warning",   
                 showCancelButton: true,   
                 confirmButtonColor: "#DD6B55",   
                 confirmButtonText: "Yes, 我要介入!",   
                 cancelButtonText: "No, 我点错了!",   
                 closeOnConfirm: false,   
                 closeOnCancel: true 
             }, 
             function(isConfirm){   
                 if (isConfirm) {     
                    $.post("{pigcms{:U('IntermediaryOrder/intervene')}", {"id":id}, function(data){
                        if(data == 1){
                         swal("成功!", "已成功介入.", "success"); 
                         location.reload() ;
                     }else{
                    	 swal("错误", "介入失败，服务异常 :)", "error"); 
                     }
                 })
                 }
             });
            })
            
            
             //关闭
       $("#transfer").click(function(){
    	 var id = $("#id").val();
         swal({   
            title: "划拨资金?",   
            text: "您确定要划拨资金吗!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, 我要划拨!",   
            cancelButtonText: "No, 我点错了!",   
            closeOnConfirm: false,   
            closeOnCancel: true 
        }, 
        function(isConfirm){   
          if (isConfirm) {     
            $.post("{pigcms{:U('IntermediaryOrder/transfer')}", {"id":id}, function(data){
                alert(data);
            	if(data == 1){
                    swal("成功!", "已成功划拨资金.", "success"); 
                    location.reload() ;
                }else if(data == 2){
               	    swal("错误", "中介状态必须是已发货,请联系管理员)", "error"); 
                }else if(data == 3){
               	    swal("错误", "买家冻结资金小于交易金额 :)", "error"); 
                }else if(data == 4){
               	     swal("错误", "卖家冻结金额小于交易金 :)", "error"); 
                }else if(data == 5){
               	     swal("错误", "没有查询到买家用户,请联系管理员)", "error"); 
                }else if(data == 6){
               	     swal("错误", "没有查询到卖家用户，请联系管理员)", "error"); 
                }else if(data == 7){
               	     swal("错误", "没有查询到该订单，请联系管理员)", "error"); 
                }else{
               	    swal("错误", "划拨失败,服务器异常 :)", "error"); 
                }
            })
            }
        });
     })
     
     
      $("#close").click(function(){
    	   var id = $("#id").val();
           var fail_reason=$("#fail_reason").val();
           if(fail_reason==""){
        	   swal("警告", "关闭理由必须输入 :)", "error"); 
        	   return ;
           }
           if(fail_reason.length > 220){
        	   swal("警告", "关闭理由长度小于220 :)", "error"); 
        	   return ;
           }
         swal({   
            title: "关闭?",   
            text: "您确定要关闭此申请吗!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, 我要关闭!",   
            cancelButtonText: "No, 我点错了!",   
            closeOnConfirm: false,   
            closeOnCancel: true 
        }, 
        function(isConfirm){   
          if (isConfirm) {     
            $.post("{pigcms{:U('IntermediaryOrder/close')}", {"id":id,"fail_reason":fail_reason}, function(data){
            	if(data == 1){
                    swal("成功!", "已成功关闭交易.", "success"); 
                    location.reload() ;
                }else{
               	    swal("错误", "关闭失败,服务器异常 :)", "error"); 
                }
            })
            }
        });
     })
     
     
     
     
   </script>
   
   
   
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}
	</script>
        
        
	<include file="Public:sidebar"/>
</body>
</html>
