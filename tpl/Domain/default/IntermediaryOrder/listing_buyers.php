<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header_staff"/>
	<include file="Public:nav_staff"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Staff/index')}" class="s-2a">经济人账户</a>&nbsp;&gt;&nbsp;</li>
                                        <li>买方中介交易</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Public:left_staff"/>
        <!-- 右边 -->
        <div class="u-fr">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>买方中介交易</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 选项卡 -->
            <div class="m-acc-select u_be9 s-bg-white">
                <!-- 标题 -->
                <div class="m-acc-title s-bg-fc">
                    <div class="u-fl"><a href="{pigcms{:U('IntermediaryOrder/listing_buyers')}" class="active">买方中介交易</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('IntermediaryOrder/listing_vendor')}">卖方中介交易</a></div>
                    <div class="u-cls"></div>
                </div>
                <!-- 搜索 -->
                <div class="m-acc-search">
                    <form action="{pigcms{:U('IntermediaryOrder/listing_buyers')}" method="post">
                        <span class="u-ipt-i u_fl" style="width: 200px;">
                            <input type="text" class="i1" id="txt_domain" name="keyword" style="color: rgb(266, 216, 216);">
                        </span>
                        <select class="u-btn8" name="search_type" style="background:#758282;width:120px">
                            <option value="uid" >买方UID</option>
                            <option value="phone">手机号</option>
                            <option value="order_flow">订单流水</option>
                        </select>
                        <input type="submit" value="搜索" class="u-btn8">
                    </form>
                </div>
                <!-- 内容 -->
                <div class="m-acc-cons s-bg-fc u_mall1">
                    <table>
                        <tbody>
                            <tr>
                                <th width="60px" class="u_tl">买方UID</th>
                                <th width="60px" class="u_tl">卖方UID</th>
                                <th width="90px" class="u_tl">客户手机</th>
                                <th width="110px" class="u_tl">创建时间</th>
                                <th width="100px" class="u_tl">网站域名</th>
                                <th width="80px" class="u_tl">订单流水</th>
                                <th width="60px;">操作</th>

                            </tr>
                            <if condition="is_array($bt_list)">
                                <volist name="bt_list" id="vo">
                                    <tr>
                                        <td width="60px" class="u_tl">{pigcms{$vo.user_id}</td>
                                        <td width="60px" class="u_tl">{pigcms{$vo.other_user_id}</td>
                                        <td width="90px" class="u_tl">{pigcms{$vo.phone}</td>
                                        <td width="110px" class="u_tl">{pigcms{$vo.createdate}</td>
                                        <td width="100px" class="u_tl">{pigcms{$vo.domain}</td>
                                        <td width="100px" class="u_tl">{pigcms{$vo.order_flow}</td>
                                        <td width="60px;"><a class="u-abtn8" href="{pigcms{:U('IntermediaryOrder/agency_details',array('id'=>$vo['id']))}">详情</a></td>
                                    </tr>
                                </volist>
                            <else/>
                                    <tr><td class="u_nb u_tc u_norecord" colspan="9">暂无记录！</td></tr>
                            </if>
                        </tbody>
                    </table>
                </div>
                <!-- 分页 -->
                <div id="paging" class="g-padding g-padding1">{pigcms{$pagebar}</div>
            </div>
        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<script src="{pigcms{$static_path}js/account.js"></script>
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}
	</script>
	<include file="Public:sidebar"/>
</body>
</html>
