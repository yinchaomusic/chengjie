<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">



</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>
<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>域名融资</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="g-content u-cls">
	<div class="m-content">



		<form action="/Member/Loan/Apply" method="post" novalidate="true">    <section>
				<div class="container nrye">
					<div class="row">



						<div class="col-sm-2">
							<!--sidebar-menu-->
							<div id="sidebar">
								<a href="#" class="submenu_bar  visible-xs-block">
									<ol class="breadcrumb">
										<li><i class="iconfont submenu_d"></i></li>
										<li>首页</li>
										<li class="active">我的账户</li>
									</ol>
								</a>
								<ul style="display: block;"><li class="submenu"><a href="{pigcms{:U('Financing/index')}"><i class="iconfont">&#xe613;</i><span>我的账户</span></a> </li>
									<li class="submenu  ">
										<a href="{pigcms{:U('Investment/lend')}"><i class="iconfont "></i>
											<span>我要理财</span><span class="label  visible-xs-block">3</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Investment/lend')}">我要借出</a></li>
											<li ><a href="{pigcms{:U('Investment/lendList')}">借出列表</a></li>
											<li><a href="{pigcms{:U('Investment/automatic')}">自动投标</a></li>
											<li ><a href="{pigcms{:U('Investment/keepInvest')}">回款续投</a></li>
										</ul>
									</li>
									<li class="submenu "><a href="{pigcms{:U('Loan/borrow')}"><i class="iconfont "></i><span>我要借款</span><span class="label  visible-xs-block">3</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Loan/borrow')}">我要借入</a></li>
											<li><a href="{pigcms{:U('Loan/borrowList')}">借入列表</a></li>
											<li ><a href="{pigcms{:U('Loan/pledge')}">质押管理</a></li>
										</ul>
									</li>
									<li class="submenu active open"><a href="#"><i class="iconfont "></i><span>资金管理</span><span class="label  visible-xs-block">6</span></a>
										<ul>
											<li><a href="{pigcms{:U('Account/Account')}">账户充值</a></li>
											<li><a href="{pigcms{:U('Account/withdrawal')}">账户提现</a></li>
											<li><a href="{pigcms{:U('Account/setbankcard')}">提现银行</a></li>
											<li  class="active"><a href="{pigcms{:U('Account/record')}">资金记录</a></li>
											<li><a href="{pigcms{:U('Account/frozendetail')}">冻结记录</a></li>
										</ul>
									</li>
									<li class="submenu "><a href="#"><i class="iconfont "></i><span>账户管理</span><span class="label  visible-xs-block">7</span></a>
										<ul>
											<li><a href="{pigcms{:U('Profile/update')}">个人信息</a></li>
											<li><a href="{pigcms{:U('Profile/avatar')}">上传头像</a></li>
											<li><a href="{pigcms{:U('Profile/IdCard')}">身份认证</a></li>
											<li><a href="{pigcms{:U('Account/updatepwd')}">登录密码</a></li>
											<li><a href="{pigcms{:U('Profile/transactionPassword')}">交易密码</a></li>
											<li><a href="{pigcms{:U('Account/bind')}">绑定手机</a></li>
											<!--											<li><a href="/Member/InfoCenter">站内信</a></li>-->
										</ul>
									</li>
								</ul>
							</div>
							<!--sidebar-menu end-->
						</div>

						<div class="col-sm-10">
							<div class="container_page ">
								<section>
									<h2>资金记录</h2>
									<div class="page_min_h">
										<div class="form-horizontal border_box form-zjjl">
											<div class="form-group">
												<label for="input6" class="control-label col-sm-1">时间： </label>
												<div class="col-sm-2">
													<input type="date" class="form-control" id="StartTime" value="2016-01-16">
												</div>
												<span class="col-sm-1 col-sm-_">—</span>
												<div class="col-sm-2">
													<input type="date" class="form-control" id="EndTime" value="2016-02-16">
												</div>
											</div>
											<div class="form-group">
												<label for="input6" class="control-label col-sm-1">资金类型： </label>
												<div class="col-sm-10" id="Purposes">
													<span class="a-sm-btn checkall">全部取消</span>
													<label class="checkbox-inline">
														<input type="checkbox"         checked
														       value="1">
														充值
													</label>
													<label class="checkbox-inline">
														<input type="checkbox"         checked
														       value="3">
														本金入款
													</label>
													<label class="checkbox-inline">
														<input type="checkbox"         checked
														       value="5">
														利息入款
													</label>
													<label class="checkbox-inline">
														<input type="checkbox"         checked
														       value="7">
														借入入款
													</label>
													<label class="checkbox-inline">
														<input type="checkbox"         checked
														       value="11">
														逾期入款
													</label>

													<br />
													<label class="checkbox-inline">
														<input type="checkbox"         checked
														       value="2">
														提现
													</label>
													<label class="checkbox-inline">
														<input type="checkbox"         checked
														       value="9">
														管理费扣款
													</label>
													<label class="checkbox-inline">
														<input type="checkbox"         checked
														       value="4">
														本金扣款
													</label>
													<label class="checkbox-inline">
														<input type="checkbox"         checked
														       value="6">
														利息扣款
													</label>
													<label class="checkbox-inline">
														<input type="checkbox"         checked
														       value="8">
														借出扣款
													</label>
													<label class="checkbox-inline">
														<input type="checkbox"         checked
														       value="10">
														逾期罚款
													</label>
												</div>
											</div>
											<div class="btn_area">
												<input type="button" id="Query" class="btn btn-primary btn-xs-block" value="查询">
											</div>
										</div>
										<div class="table-responsive">
											<table class=" table table-bordered table-striped table-hover ">
												<thead>
												<tr>
													<th>日期/标号</th>
													<th>类型</th>
													<th>收入</th>
													<th>支出</th>
													<th>账户总额</th>
													<th>说明</th>
												</tr>
												</thead>
												<tbody class="text-center">
												</tbody>
											</table>
										</div>
									</div>




								</section>
							</div>
						</div>



					</div>
				</div>
			</section>
		</form>



	</div>
</div>
<include file="Public:footer"/>
<include file="Public:account_footer"/>
<script src="{pigcms{$static_path}js/unslider.js"></script>
<script src="{pigcms{$static_path}js/bootstrap.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/rz_common.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/matrix.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/laydate.dev.js"></script>
<script type="text/javascript">
	$(function () {
		$(".checkall").click(function () {
			$(".checkbox-inline :checkbox").prop('checked', false);
		});
		$("#Query").click(function () {
			var url = "?";
			if ($("#StartTime").val() && $("#EndTime").val()) {
				url += "start=" + $("#StartTime").val() + "&end=" + $("#EndTime").val() + "&";
			}
			var checkeds = $("#Purposes :checked");
			var purposes = [];
			for (var i = 0; i < checkeds.length; i++) {
				purposes.push(checkeds.eq(i).val());
			}
			if (purposes.length > 0) {
				url += "purpose=" + purposes.join(",");
			}
			window.location = url;
		});


		if ($Mbrowser.versions.ios || $Mbrowser.versions.XiaoMi || ($Mbrowser.versions.mobile && $Mbrowser.versions.Chrome)) {
			/*
			 alert("支持")                     */
		} else {
			/*alert("js日历控件")*/
			$("#StartTime,#EndTime").attr("type", "text");
			laydate({
				elem: '#StartTime'
			});
			laydate({
				elem: '#EndTime'
			});
		};


	});

</script>


</body>
</html>