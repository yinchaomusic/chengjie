<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">



</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>
<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>域名融资</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="g-content u-cls">
	<div class="m-content">



		<form action="/Member/Loan/Apply" method="post" novalidate="true">    <section>
				<div class="container nrye">
					<div class="row">



						<div class="col-sm-2">
							<!--sidebar-menu-->
							<div id="sidebar">
								<a href="#" class="submenu_bar  visible-xs-block">
									<ol class="breadcrumb">
										<li><i class="iconfont submenu_d"></i></li>
										<li>首页</li>
										<li class="active">我的账户</li>
									</ol>
								</a>
								<ul style="display: block;"><li class="submenu"><a href="{pigcms{:U('Financing/index')}"><i class="iconfont">&#xe613;</i><span>我的账户</span></a> </li>
									<li class="submenu  ">
										<a href="{pigcms{:U('Investment/lend')}"><i class="iconfont "></i>
											<span>我要理财</span><span class="label  visible-xs-block">3</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Investment/lend')}">我要借出</a></li>
											<li ><a href="{pigcms{:U('Investment/lendList')}">借出列表</a></li>
											<li><a href="{pigcms{:U('Investment/automatic')}">自动投标</a></li>
											<li ><a href="{pigcms{:U('Investment/keepInvest')}">回款续投</a></li>
										</ul>
									</li>
									<li class="submenu "><a href="{pigcms{:U('Loan/borrow')}"><i class="iconfont "></i><span>我要借款</span><span class="label  visible-xs-block">3</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Loan/borrow')}">我要借入</a></li>
											<li><a href="{pigcms{:U('Loan/borrowList')}">借入列表</a></li>
											<li ><a href="{pigcms{:U('Loan/pledge')}">质押管理</a></li>
										</ul>
									</li>
									<li class="submenu active open"><a href="#"><i class="iconfont "></i><span>资金管理</span><span class="label  visible-xs-block">6</span></a>
										<ul>
											<li class="active"><a href="{pigcms{:U('Account/Account')}">账户充值</a></li>
											<li><a href="{pigcms{:U('Account/withdrawal')}">账户提现</a></li>
											<li><a href="{pigcms{:U('Account/setbankcard')}">提现银行</a></li>
											<li><a href="{pigcms{:U('Account/record')}">资金记录</a></li>
											<li><a href="{pigcms{:U('Account/frozendetail')}">冻结记录</a></li>
										</ul>
									</li>
									<li class="submenu "><a href="#"><i class="iconfont "></i><span>账户管理</span><span class="label  visible-xs-block">7</span></a>
										<ul>
											<li><a href="{pigcms{:U('Profile/update')}">个人信息</a></li>
											<li><a href="{pigcms{:U('Profile/avatar')}">上传头像</a></li>
											<li><a href="{pigcms{:U('Profile/IdCard')}">身份认证</a></li>
											<li><a href="{pigcms{:U('Account/updatepwd')}">登录密码</a></li>
											<li><a href="{pigcms{:U('Profile/transactionPassword')}">交易密码</a></li>
											<li><a href="{pigcms{:U('Account/bind')}">绑定手机</a></li>
											<!--											<li><a href="/Member/InfoCenter">站内信</a></li>-->
										</ul>
									</li>
								</ul>
							</div>
							<!--sidebar-menu end-->
						</div>

						<div class="col-sm-10 ">
							<div class="container_page blank">
								<section>
									<h2>账户充值</h2>
									<ul id="rechargeTabs" class="nav nav-tabs cz-nav-tabs" >
										<li id="lionline" role="presentation">在线充值</li>
										<li id="lioffline" role="presentation" class="active">线下充值</li>
									</ul>
									<div id="online" style="display:none;">
										<p class="info-box">
											<span class="font_c_orange">在线充值说明：</span><br />
											1、用户每个自然月在线充值总额上限为10万元；<br />
											2、用户在线充值不需手续费，第三方支付收取的手续费由平台承担。
										</p>
										<section>
											<div class="bg_violet clearfix form-zxzf">
												<div class="form-horizontal  ">
													<div class="form-group  zfjk-group">
														<label class="control-label col-sm-1">支付接口：</label>
														<ul class="list-zfjk">
															<li>
																<label style="padding:2px 6px;" class="active">
																	<img width="130" src="/Content/img/95epay.png" alt="双乾支付">
																	<input type="radio" name="zfjk" value="ShuangQianPay" checked>
																</label>
															</li>

														</ul>
														<script type="text/javascript">
															$('.list-zfjk input[type="radio"]').on('click', function () {
																$('.list-zfjk li label').removeClass('active');
																$(this).closest('label').addClass('active');
															})
														</script>
													</div>
													<div class="form-group">
														<label class=" control-label col-sm-1">充值金额：</label>
														<div class="col-sm-2">
															<input autocomplete="off" class="form-control Decimal" id="rechargeMoney" name="rechargeMoney" placeholder="不能小于100" type="number" value="" />
														</div>
														<p class="form-control-static">元&nbsp;&nbsp;
															<span id="spanRechargeAmount" data-amount="0">本月已充值￥0.00元</span></p>
													</div>

												</div>
											</div>
											<input type="button" onclick="postRecharge();" id="btnRecharge" class="btn  btn-primary3 btn-xs-block" value="充 值" style="margin-left: 102px;background-color:#91BE45;">
										</section>

									</div>
									<div id="offline">
										<p class="info-box">
											<span class="font_c_orange">线下充值(汇款人承担手续费)</span><br />
											请联系客服QQ：<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=2015120057&site=wrz.com&menu=yes">2015120057</a>
										</p>
										<dl class="clearfix">
											<dt class="gsyh">中国工商银行</dt>
											<dd>收款人：刘双<br />
												开户行：中国工商银行深圳市南山区麒麟支行<br />
												帐　号：6222 0840 0000 6145 083</dd>
											<dt class="zsyh">招商银行</dt>
											<dd>收款人：刘双<br />
												开户行：招商银行深圳市南山区科苑支行<br />
												帐　号：6214 8565 5524 7117</dd>
											<dt class="nyyh">中国农业银行</dt>
											<dd>收款人：刘双<br />
												开户行：中国农业银行深圳市南山区科苑支行<br />
												帐　号：6228 4801 2822 8957 378</dd>
											<dt class="zgyh">中国银行</dt>
											<dd>收款人：刘双<br />
												开户行：中国银行深圳市南山区艺园路支行<br />
												帐　号：6217 8520 0000 6956 760</dd>
											<dt class="jsyh">中国建设银行</dt>
											<dd>收款人：刘双<br />
												开户行：中国建设银行深圳市南山区科苑支行<br />
												帐　号：6217 0072 0002 2060 332</dd>

										</dl>
									</div>
								</section>

							</div>
						</div>



					</div>
				</div>
			</section>
		</form>



	</div>
</div>
<include file="Public:footer"/>
<include file="Public:account_footer"/>
<script src="{pigcms{$static_path}js/unslider.js"></script>
<script src="{pigcms{$static_path}js/bootstrap.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/rz_common.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/matrix.js"></script>
<script type="text/javascript">
	function postRecharge() {
		if ($("#rechargeMoney").val() == "") {
			alert('请输入充值金额'); $("#rechargeMoney").focus(); return;
		}
		var pay = $("input[name='zfjk']:checked").val();
		window.open("/Member/Fund/" + pay + "?rechargeMoney=" + $("#rechargeMoney").val());
	}
	$(function () {
		$("#rechargeTabs li").click(function () {
			if (this.id == "lionline") {
				$("#online").show(); $("#lionline").attr("class", "active");
				$("#offline").hide(); $("#lioffline").attr("class", "");
			}
			else {
				$("#offline").show(); $("#lioffline").attr("class", "active");
				$("#online").hide(); $("#lionline").attr("class", "");
			}
		});
		var rechargeAmount = $("#spanRechargeAmount").attr("data-amount");
		var canRechargeAmount = 100000 - rechargeAmount;
		$("#rechargeMoney").on("blur", function () {
			if ($(this).val() > 100) {
				if ($(this).val() > canRechargeAmount) $(this).val(canRechargeAmount);
			} else if ($(this).val() != "") {
				$(this).val(100);
			}
		});

	})
</script>

</body>
</html>