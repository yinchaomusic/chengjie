<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我是卖家 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>网站交易&nbsp;&gt;&nbsp;</li>
					<li><if condition="$_GET['entr_type'] eq 1">代购的网站
    						<elseif  condition="$_GET['entr_type'] eq 2"/>代售的网站
    					</if>
					</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <div class="u-fr">
			<!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>
    							<if condition="$_GET['entr_type'] eq 1">代购的网站
        							<elseif  condition="$_GET['entr_type'] eq 2"/>代售的网站
    							</if> 
    						</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 选项卡 -->
            <div class="m-acc-select u_be9 s-bg-white">
                <!-- 标题 -->
                <div class="m-acc-title s-bg-fc">
                    <div class="u-fl"><a href="{pigcms{:U('WebEntrusts/webList',array('entr_type'=>$_GET['entr_type']))}" class="active set">所有</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('WebEntrusts/webList',array('type'=>'1','entr_type'=>$_GET['entr_type']))}" class=" set" id="type1">初始状态</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('WebEntrusts/webList',array('type'=>'2','entr_type'=>$_GET['entr_type']))}" class=" set" id="type2">审核通过</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('WebEntrusts/webList',array('type'=>'3','entr_type'=>$_GET['entr_type']))}" class=" set" id="type3">审核不通过</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('WebEntrusts/webList',array('type'=>'4','entr_type'=>$_GET['entr_type']))}" class=" set" id="type4">已关闭</a></div>
                    <div class="u-fl"><a href="{pigcms{:U('WebEntrusts/webList',array('type'=>'5','entr_type'=>$_GET['entr_type']))}" class=" set" id="type5">已结束</a></div>
                    <input id="checktype" type="hidden" value="{pigcms{$_GET.type}" />
                    <input id="checkentr" type="hidden" value="{pigcms{$_GET.entr_type}" />
                    <div class="u-cls"></div>
                </div>
                    <!-- 搜索 -->
                    <!-- <div class="m-acc-search">
                        <span class="u-ipt-k u_fl">
                            <input type="text" class="i1 u_vm" ph="请输入要查询的域名" />
                        </span>
                        <input type="button" value="搜索" class="u-btn8" id="my_parse_serch" />
                    </div> -->
                    <!-- 内容 -->
                    <div class="m-acc-cons s-bg-fc u_mall1">
                        <table width="100%" cellspacing="0" style="text-align:center">
            				<colgroup><col><col><col><col><col width="200" align="center"> </colgroup>
            				<thead>
            				<tr>
            					<th>编号</th>
            					<th>网站域名</th>
            					<th>网站类型</th>
            					<th>创建日期</th>
            					<th class="textcenter">操作</th>
            				</tr>
            				</thead>
            				<tbody>
            				<if condition="is_array($group_list)">
            					<volist name="group_list" id="vo">
            						<tr>
            							<td>{pigcms{$vo.id}</td>
            							<td>{pigcms{$vo.domain}</td>
            							<td>
            								<if condition="$vo['web_type'] eq 1"><font color="red">行业门户 </font>
                								<elseif  condition="$vo['web_type'] eq 2"/>音乐影视
                								<elseif  condition="$vo['web_type'] eq 3"/>游戏小说
                								<elseif  condition="$vo['web_type'] eq 4"/>女性时尚
                								<elseif  condition="$vo['web_type'] eq 5"/>QQ/娱乐
                								<elseif  condition="$vo['web_type'] eq 6"/>商城购物
                								<elseif  condition="$vo['web_type'] eq 7"/>地方网站
                								<elseif  condition="$vo['web_type'] eq 8"/>其他网站
            								</if>
            							</td>
            							<td><div style="height:24px;line-height:24px;">{pigcms{$vo.createdate|date='Y-m-d ',###}</div></td>
            							<td class="textcenter">
									  	<input type="hidden" name="id" id="el_id" value="{pigcms{$vo.id}"/>
    									 <a class="u-btn13 sureDele" >删除</a> 
            						</tr>
            					</volist>
            					<tr><td class="textcenter pagebar" colspan="5">{pigcms{$pagebar}</td></tr>
            					<else/>
            					<tr><td class="textcenter red" colspan="5">列表为空！</td></tr>
            				</if>
            				</tbody>
            			</table>
                    </div>   
                    <!-- 分页 -->
                    <div id="paging" class="g-padding g-padding1"></div>   
            </div>
        </div>
       <div class="u-cls"></div>
       <div style="position: absolute; display: none; z-index: 1005; left: 29%; top: 331.5px;" class="m-pop2 s-bg-white" id="pop16">
            <div class="pop-insa">
                <ul>
                    <li>
                        <span class="u-span18 u-fl"></span>
                        	<span class="u-span19 u-fl">确认删除？
                        </span>
                        <div class="u-cls"></div>
                    </li>
                </ul>
            </div>
            <div class="pop2-btn s-bg-fa">
                <input id="sureDele" k="1000000" value="确认" class="u-btn12 s-bg-2a" type="button">
            </div>
            <div class="close"></div>
        </div>
    </div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<include file="Public:sidebar"/>
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">
    <script src="{pigcms{$static_path}js/popupBox.js"></script>
	<script>

		$( document ).ready(function () {
        	var type = $("#checktype").val();
        	if(type !=''){
    			$(".set").removeClass('active');
       		 	$("#type"+type).addClass('active');
        	};
        	$(".process1").css("display", "none");
        	$("#web").css("display", "block");
			var entr = $("#checkentr").val();
			if(entr !=''){
    			$(".entr").removeClass('t');
       		 	$("#entr"+entr).addClass('t');
        	};

    		
    		$(".sureDele").click(function () {
    			showBox("pop16");
                $("#pop16").css("left", "29%");
            });

            $(".close").click(function () {
                closeBox("pop16");
            });

            //删除行
    		$('#sureDele').click(function () {
                var id = $("#el_id").val(); 
                $.post("{pigcms{:U('WebEntrusts/del')}",{"id":id},function(data){
                    if(data == 1){
                         swal("成功!", "删除成功!", "success")
                         location.href = location.href;
                    }else{
                        sweetAlert("异常...", "删除失败!", "error");
                    }
                })
            });

        });
    </script>
</body>
</html>
