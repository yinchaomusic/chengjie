<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>买网站 - 网站购买 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/common.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bragin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>搜索网站</li>
				</ol>
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
<div class="g-content u-cls">
	<div class="m-content">
		<div class='m-title-a u_be9 s-bg-fc u_mb6'>

			<a href='javascript:void(0);'>
				<span class='z-imga-t3 u-fl'></span>
				<span class='u-fl s-05'>搜索网站</span>
			</a>        </div>
		<div class="m-conditions u_df s-bg-white">
			<div class="m-keyword-a s-bg-fb u_bb0">
				<div class="f-keytxt u-fl">网站关键字：</div>
				<div class="u-fl" id="keyworda">
                    <span class="u-ipt-b u_b4 u_mr25">
                        <input type="text" name="searchKey" id="searchKey"/>
                    </span>
					<label class="f-label">
						<input type="radio" checked nu="1" name="keyword" /><span>包含</span>
					</label>
					<label class="f-label">
						<input type="radio" nu="2" name="keyword" /><span>开始</span>
					</label>
					<label class="f-label">
						<input type="radio" nu="3" name="keyword" /><span>结尾</span>
					</label>
					<input type="button" value="搜  索" k='0' class="button u-btn1 s-btn-2a" />
				</div>
				<div class="u-cls"></div>
			</div>
			<div id="g_sh">
				<div class="m-hz u_bb0 u-cls" id="m-hz" name="hz">
					<div class="f-keytxta u-fl">交易类型：</div>
					<div class="f-hz u-fl f-hz-order_type">
					    <if condition="$entr_type eq 1">
						    <span class="z-cell" data='1'>网站购买</span>
						    <span idx="other" data='2'>网站出售</span>
    					<else/>
						    <span idx="other" data='1'>网站购买</span>
						    <span class="z-cell" data='2'>网站出售</span>
    					</if>
					</div>
					<div class="u-cls"></div>
				</div>
				
				<div class="m-hz u_bb0 u-cls" id="m-hz" name="hz" style="width: 300px;">
					<div class="f-keytxta u-fl">网站类型：
					</div>
					<div class="f-hz u-fl f-hz-web_type">
						<span idx="leixin-00" class="z-cell"  data="">不限</span>
						<span idx="leixin-00" data="1">行业门户</span>
						<span idx="leixin-00" data="2">音乐影音</span>
						<span idx="leixin-00" data="3">游戏小说</span>
						<span idx="leixin-00" data="4">女性时尚</span>
						<span idx="leixin-00" data="5">QQ/娱乐</span>
						<span idx="leixin-00" data="6">商城购物</span>
						<span idx="leixin-00" data="7">地方网站</span>
						<span idx="leixin-00" data="8">其他网站</span>
					</div>
					<div class="u-cls"></div>
				</div>
				
				<div class="m-leng u_bb0 u-cls">
					<div class="f-keytxta u-fl">百度权重:</div>
					<div class="f-leng u-fl">
						<div class="f-slider u-fl" style="margin-right: 6px;">
							<div style="width: 380px; height: 7px; margin-top: 7px;" id="domian-len" name="len"></div>
							<ol>
								<li>0</li>
								<li>1</li>
								<li>2</li>
								<li>3</li>
								<li style="width: 46px; padding-left: 1px;">4</li>
								<li style="width: 45px; padding-left: 2px;">5</li>
								<li style="width: 45px; padding-left: 2px;">6</li>
								<li style="width: 38px; padding-left: 3px;">7</li>
								<li>不限</li>
							</ol>
						</div>
						<div class="f-range-sure u-fr">
                            <span class="u-ipt-c">
                                <input type="text" val="" id="baiduBegin"/>
                            </span>
							<span class="u_ver_line"></span>
                            <span class="u-ipt-c">
                                <input type="text" val="" id="baiduEnd"/>
                            </span>
							<input type="button" value="确定" class="u-btn2 s-bg-55b" />
						</div>
						<div class="u-cls"></div>
					</div>
					<div class="u-cls"></div>
				</div>
				  <div class="m-leng u_bb0 u-cls">
						<div class="f-keytxta u-fl">当前价格：</div>
						<div class="f-leng u-fl">
							<div class="f-slider">
								<div style="width: 380px; height: 7px; margin-top: 7px;" id="domian-pri" name="pri"></div>
								<ol>
									<li style="width: 40px;">0</li>
									<li style="width: 40px;">500</li>
									<li>1000</li>
									<li>&nbsp;&nbsp;2万</li>
									<li>&nbsp;&nbsp;5万</li>
									<li>&nbsp;10万</li>
									<li>&nbsp;&nbsp;20万</li>
									<li style="width: 42px;">&nbsp;&nbsp;50万</li>
									<li style="text-align: right;">不限&nbsp;</li>
								</ol>
							</div>
						</div>
						<div class="f-range-sure u-fl u_ml19">
							<span class="u-ipt-c">
								<input type="text" val="" id="flowWebBegin"/>
							</span>
							<span class="u_ver_line"></span>
							<span class="u-ipt-c">
								<input type="text" val="" id="flowWebEnd"/>
							</span>
							<input type="button" value="确定" class="u-btn2 s-bg-55b" />
						</div>
						<div class="u-cls"></div>
					</div>
					
					
					<div class="m-leng u_bb0 u-cls">
					<div class="f-keytxta u-fl">PR值:</div>
					<div class="f-leng u-fl">
						<div class="f-slider u-fl" style="margin-right: 6px;">
							<div style="width: 380px; height: 7px; margin-top: 7px;" id="baidu-qz" name="qz"></div>
							<ol>
								<li>0</li>
								<li>1</li>
								<li>2</li>
								<li>3</li>
								<li style="width: 46px; padding-left: 1px;">4</li>
								<li style="width: 45px; padding-left: 2px;">5</li>
								<li style="width: 45px; padding-left: 2px;">6</li>
								<li style="width: 38px; padding-left: 3px;">7</li>
								<li>不限</li>
							</ol>
						</div>
						<div class="f-range-sure u-fr">
                            <span class="u-ipt-c">
                                <input type="text" val="" id="prBegin"/>
                            </span>
							<span class="u_ver_line"></span>
                            <span class="u-ipt-c">
                                <input type="text" val="" id="prEnd"/>
                            </span>
							<input type="button" value="确定" class="u-btn2 s-bg-55b" id="baiduButton"/>
						</div>
						<div class="u-cls"></div>
					</div>
					<div class="u-cls"></div>
				</div>
					
				<div class="m-selconditions u-cls">
					<div class="f-keytxta u-fl">已选条件：</div>
					<div class="selconditions u-fl" id="selConditions">
					</div>
					<div class="u-cls"></div>
				</div>
			</div>
		</div>
		<div class="m-up-down u_mb6" id="m-up-down">
			<div class="up s-bg-white u_df0">收起</div>
			<div class="down s-bg-fb u_df0">展开更多[后缀 长度 价格 类型 主题]</div>
		</div>
		<div class="m-relsult2">
			<div class="g-a-container u_df1 s-bg-white" style="position:relative;width: 980px;">
				<div class="m-sort s-bg-fb u_bb0" id="sort">
					<span data='1' class="z-up">按价格排序</span>
					<span data='2' class="z-active z-down">按时间排序</span>
				</div>
				<table class="u-cls" id="liebiao-list-web">
					
				</table>
			</div>
			<div class="u-cls"></div>
		</div>
		<div id="paging" class="g-padding">
			<div class="u-fl"><span class='u_fw'></span>条记录，
			<span class='u_fw'></span>页</div>
			<div class="u-fr">
				
			</div>
			<div class='u-cls'></div>
		</div>
	</div>
	<div class="u-cls"></div>
</div>
<include file="Public:footer"/>
<include file="Public:sidebar"/>
<div class="s-bg-f7"></div>
<script type="text/javascript" src="{pigcms{$static_path}js/jquery.color.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/jquery-ui.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/webbragin.js"></script>
<script language="javascript">
$(document).ready(function(){
  getData(1);			
  $("#baiduButton").click(function(){
	  getData(1);
  });
});
</script>
</body>
</html>
