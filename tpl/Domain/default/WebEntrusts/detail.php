<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>{pigcms{$now_domain.domain}-网站详情-网站代购-代售 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/common.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/webdetail.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('WebEntrusts/index')}" class="s-2a">网站详情</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="#" class="s-2a">{pigcms{$ar.description}</a></li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
		<div class="m-content">
			<!-- 左边 -->
			<div class="u-fl">
				<div class="g-a-container">
					<div class="m-ta s-bg-white u_be9">
						<div class="title">
							<a class="s-3c" href="{pigcms{:U('WebEntrusts/index')}">
								<span class="z-img-t3"></span>
								<span>网站详情</span>
							</a>
						</div>
						<div class="u-cls"></div>
					</div>
					<div class="m-inner1 s-bg-fc u_be9">
						<div class="u_instr2">
							<div class="instr2-l">
								如果您感兴趣，请联系专属经纪人购买或商谈，经纪人会为您提供此网站更详尽的信息。
							</div>
							<div class="instr2-r">
                                <if condition="$collection_show eq 1">
                                    <a title="收藏网站" id="collection" style="width: 25px; background-image:url({pigcms{$static_path}images/account/sc.png)"></a>
                                    <else/>
                                    <a title="收藏网站" id="collection" style="width: 25px;background-image:url({pigcms{$static_path}images/account/wsc.png)"></a>
                                </if>
                                <input type="hidden" value="{pigcms{$ar.id}" id="domainid" />
                                <input type="hidden" value="{pigcms{$ar.description}" id="domain_name" />
                                <input type="hidden" value="{pigcms{$ar.entr_type}" id="domain_type" />
								<a href="javascript:laLock($('.f-ha a.s-2a'))" target="_blank" title="切换大小写" class="f-img-i1"></a>
								<a href="http://www.baidu.com/s?wd={pigcms{$ar.domain}" target="_blank" title="百度搜索" class="f-img-i2"></a>
							</div>
							<div class="u-cls"></div>
						</div>
					</div>
					<div class="m-inner2 s-bg-fc u_be9">
							<div class="dndetail-l">
							<div class="dnms">
								<p class="mt30">
									<if condition="($ar['entr_type'] eq 1)"> <b>代购：</b>
									<span>{pigcms{$ar.description|substr=0,30}</span> </if>
									<if condition="($ar['entr_type'] eq 2)"> <b>代售：</b>
									<span>{pigcms{$ar.description|substr=0,30}</span> </equal> </if>
								</p>
								<p>
									<b>域名：</b><span>{pigcms{$ar.domain}</span>
								</p>
								<p>
									<b>浏览次数：</b><span>{pigcms{$ar.look_num}</span>
								</p>
								<p>
									<b>描述：</b><span>{pigcms{$ar.description}</span>
								</p>
								<p>
									<b>日ip</b> <span> 移动端:{pigcms{$ar.pc_ip}
										pc端:{pigcms{$ar.app_ip} </span>
								</p>
								<p>
									<b>网站程序</b> <span> {pigcms{$ar.souce_code} </span>
								</p>
								<p>
									<b>服务器</b> <span> {pigcms{$ar.server_info} </span>
								</p>
								<p>
									<b>广告联盟</b> <span>{pigcms{$ar.advert_aliance}</span>
								</p>
							</div>
							<div class="priceandeq">
								<div class="priceXq">
									<p>价格：</p>
									<h3>￥{pigcms{$ar.price}</h3>
									<label><input type="checkbox" checked="checked" id="agree">
										我已阅读并同意<a href="/Home/About/fwxy">《chengjie网服务协议》</a></label>
									<a href="tencent://message/?uin=162115887&Site=chengjie网" class="btn-button lxjjr" 
									 onclick="document.getElementById('light').style.display='block'">联系经纪人</a>
								</div>
							</div>
							</div>
						<div class="u-cls"></div>
					</div>
					<div class="m-inner3 s-bg-fc u_be9">
						<div class="m-ta s-bg-white">
							<div class="title">
								<a class="s-47" href="javascript:void(0)">
									<span>您可能感兴趣的网站</span>
								</a>
							</div>
							<div class="more">
							<if condition="($ar['entr_type'] eq 1)">
								<a href="{pigcms{:U('WebEntrusts/index',array('entr_type'=>1))}" target="_blank">更多&gt;&gt;</a>
							</if>
							<if condition="($ar['entr_type'] eq 2)">
							    <a href="{pigcms{:U('WebEntrusts/index',array('entr_type'=>2))}"  target="_blank">更多&gt;&gt;</a>
							</if>
							</div>
						</div>
						<div class="m-iner-data1">
							<ul>
								<volist name="web_list" id="vo">
								  <li> 
								    <a href="{pigcms{:U('WebEntrusts/detail',array('id'=>$vo['id']))}" style="padding:0 12px;">
								         {pigcms{$vo.description|substr=0,20}</a></li>
								 </volist>
							</ul>
							<div class="u-cls"></div>
						</div>
					</div>
				</div>
			</div>
			<!-- 右边 -->
			<div class="u-fr">
				<div class="g-c-container">
					<div class="m-processList u_mt1 u_mb6">
						<h3 class="cli">我要购买</h3>
						<div class="process" style="display: block;">
							<ul>
								<li>
									<span class="f-img-s3">1</span><span class="u-txh">联系专属经纪人</span>
								</li>
								<li>
									<span class="f-img-s3">2</span><span class="u-txh">双方确定细节</span>
								</li>
								<li>
									<span class="f-img-s3">3</span><span class="u-txh">发起交易</span>
								</li>
								<li>
									<span class="f-img-s3">4</span><span class="u-txh">支付全款</span>
								</li>
								<li>
									<span class="f-img-s3">5</span><span class="u-txh">域名过户</span>
								</li>
								<li class="u_mb0">
									<span class="f-img-s3">6</span><span class="u-txh">交易完成</span>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<include file="Public:footer"/>
	<include file="Public:sidebar"/>
	<div class="s-bg-f7"></div>
        <script>
            $("#collection").click(function(){
                var domain_id = $("#domainid").val();
                var domain = $("#domain_name").val();
                var type = $("#domain_type").val();
                $.post("{pigcms{:U('Hotsale/collection')}",{'domain_id':domain_id,'domain':domain,'type':type}, function(data){
                    if(data == 1){
                        alert('收藏成功');
                        location.href=location.href;
                    }else if(data == 3){
                        alert('已经收藏');
                    }else{
                        alert('失败请重试');
                    }
                })
            })
    </script>
</body>
<!-- <div class="helpRight leftline white_content" id="light">
		<div class="jjr_list fixfloat">
			<div class="jjr_list_z ">
				<div class="kuang fixfloat">
					<div class="jjr_img">
						<img src="/static/web/headergirl.jpg" alt="chengjie网">
					</div>
					<div class="jjr_intro">
						<p class="title">
							Penny <em>域名经纪人</em>
						</p>
						<p class="intro qq">
							2395260659<a
								href="tencent://message/?uin=2395260659&amp;Site=中介网">交谈</a>
						</p>
						<p class="intro dh">15380920443</p>
						<p class="intro yx">penny@zhongjie.com</p>
					</div>
				</div>
			</div>
			<div class="jjr_list_z mr0">
				<div class="kuang fixfloat">
					<div class="jjr_img">
						<img src="/static/web/headergirl.jpg" alt="中介网">
					</div>
					<div class="jjr_intro">
						<p class="title">
							Wendy <em>域名经纪人</em>
						</p>
						<p class="intro qq">
							3370735881<a
								href="tencent://message/?uin=3370735881&amp;Site=中介网">交谈</a>
						</p>
						<p class="intro dh">15366174260</p>
						<p class="intro yx">wendy@zhongjie.com</p>
					</div>
				</div>
			</div>
		</div>
		<div class="jjr_list fixfloat sc">
			<div class="jjr_list_z ">
				<div class="kuang fixfloat">
					<div class="jjr_img">
						<img src="/static/web/headerboy.jpg" alt="中介网">
					</div>
					<div class="jjr_intro">
						<p class="title">
							部长 <em class="sc">网站经纪人</em>
						</p>
						<p class="intro qq">
							2853389666<a
								href="tencent://message/?uin=2853389666&amp;Site=中介网">交谈</a>
						</p>
						<p class="intro dh">15559598855</p>
						<p class="intro yx">gh@zhongjie.com</p>
					</div>
				</div>
			</div>
			<div class="jjr_list_z mr0">
				<div class="kuang fixfloat">
					<div class="jjr_img">
						<img src="/static/web/headerboy.jpg" alt="中介网">
					</div>
					<div class="jjr_intro">
						<p class="title">
							政委 <em class="sc">网站经纪人</em>
						</p>
						<p class="intro qq">
							2853389678<a
								href="tencent://message/?uin=2853389678&amp;Site=中介网">交谈</a>
						</p>
						<p class="intro dh">13276669999</p>
						<p class="intro yx">zw@zhongjie.com</p>
					</div>
				</div>
			</div>
		</div>
		<a href="javascript:void(0)" id="aclose" onclick="document.getElementById('light').style.display='none'"> 
    关闭</a>
</div> -->
</html>