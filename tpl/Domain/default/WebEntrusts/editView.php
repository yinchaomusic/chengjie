<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<if condition="($webtype eq 1)">
<title>网站代购 - 代购的优势 - {pigcms{$config.site_name}</title>
</if>
<if condition="($webtype eq 2)">
<title>网站代售 - 代售的优势 - {pigcms{$config.site_name}</title>
</if>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
<link rel="stylesheet" type="text/css"
	href="{pigcms{$static_path}css/common.css" />
<link rel="stylesheet" type="text/css"
	href="{pigcms{$static_path}css/skin.css" />
<link rel="stylesheet" type="text/css"
	href="{pigcms{$static_path}css/sell-web.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header" />
	<include file="Public:nav" />
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<if condition="($webtype eq 1)">
					<li>网站代购</li>
					</if>
					<if condition="($webtype eq 2)">
					<li>网站代售</li>
					</if>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d
					H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
		<div class="m-content">
			<!-- 左边 -->
			<div class="g-a-container">
				<div class="m-ta s-bg-white u_be9">
					<div class="title">
						<a class="s-47" href="javascript:void(0)"> <span class="z-img-t11"></span>
							<if condition="($webtype eq 1)"> <span>网站代购</span> </if> <if
								condition="($webtype eq 2)"> <span>网站代售</span> </if>
						</a>
					</div>
					<div class="u-cls"></div>
				</div>
				<div class="m-insb s-bg-fc u_be89" id="animateb">
					<if condition="($webtype eq 1)"> <span class="f-img-dg1">代购申请</span>
					</if>
					<if condition="($webtype eq 2)"> <span class="f-img-dg1">代售申请</span>
					</if>
					<span class="f-img-dg6"></span> <span class="f-img-dg2">审核申请</span>
					<span class="f-img-dg6"></span> <span class="f-img-dg3">缴纳保证金</span>
					<span class="f-img-dg6"></span> <span class="f-img-dg4">跟进谈判</span>
					<span class="f-img-dg6"></span> <span class="f-img-dg5">谈判结束</span>
				</div>
				<div class="m-transa s-bg-fc u_be89">
					<if condition="($webtype eq 1)">
					<p class="f-p-t6 s-bg-white u_bb0 s-3c">发起代购交易</p>
					</if>
					<if condition="($webtype eq 2)">
					<p class="f-p-t6 s-bg-white u_bb0 s-3c">发起代售交易</p>
					</if>
					<div class="transa">
						<div class="select fl">
							<form method="post" id="webform1" action="{pigcms{:U('WebEntrusts/add')}">
					               <input type="hidden" name="entr_type" value="{pigcms{$webtype}" id="webtypeId" />
								<div class="tra tra1">
									<span>委托</span>
									<div class="button-holder">
										<if condition="($webtype eq 1)">
										 <input type="radio" name="ctaction" value="2" id="radio-action-0" class="regular-radio big-radio _change_type _role_value"/>
										 <label for="radio-action-0">代售</label> 
										 <input type="radio" name="ctaction" value="1" id="radio-action-1" class="regular-radio big-radio _change_type _role_value" checked=""/> 
										 <label for="radio-action-1">代购</label> </if>
										<if condition="($webtype eq 2)"> 
										 <input type="radio" name="ctaction" value="2" id="radio-action-0" class="regular-radio big-radio _change_type _role_value" checked=""/>
										 <label for="radio-action-0">代售</label> 
										 <input type="radio" name="ctaction" value="1" id="radio-action-1" class="regular-radio big-radio _change_type _role_value" /> 
										 <label for="radio-action-1">代购</label>
										</if>
									</div>
								</div>
								<div class="tra tra6">
									<span><b style="color: red">*</b>域名</span> <input type="text"
										name="domain" value="" placeholder="请输入你想代购/代售网站的域名" id="domainId"/>
								</div>
                                <div class="tra tra6">
									<span><b style="color: red">*</b>网站类型</span>
									<label class="address address1 fl" style="width:54px;margin-top:10px;border:0px solid #b8bbc2;font-weight:bold;height:38px;">
									<select name="web_type" style="width: 140px;border:1px solid #b8bbc2;height:38px;">
									   <option value="1">-行业门户-</option>
									   <option value="2">-音乐影视-</option>
									   <option value="3">-游戏小说-</option>
									   <option value="4">-女性时尚-</option>
									   <option value="5">-QQ/娱乐-</option>
									   <option value="6">-商城购物-</option>
									   <option value="7">-地方网站-</option>
									   <option value="8">-其他网站-</option>
									</select></label>
								</div>
								<div class="tra tra6">
									<span><b style="color: red">*</b>描述</span>
									<textarea name="description"
										placeholder="代售/代购的域名的简要说明,100字以内" id="descriptionId"></textarea>
								</div>
								<div class="tra tra6">
										<span>百度权重</span> pc端 <input type="text"
											name="pc_baidu" style="width: 100px" value="0"
											 onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
										移动端 <input type="text" name="app_baidu"
											style="width: 100px" value="0" 
											onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
								</div>
								<div class="tra tra6"><span>PR值</span> 
                                    <input type="text" name="pr" value="0" style="width: 150px"
                                        onkeyup="this.value=this.value.replace(/\D/g,'')"
                                             onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
								</div>
								<div class="_role _role_0" id="ddrole0" style="<if condition='($webtype eq 1)'>display:none;</if>">
	
									<div class="tra tra6">
										<span>日ip</span> pc端 <input type="text"
											name="pc_ip" style="width: 100px;" value="0"
											 onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/> 移动端
										<input type="text" name="app_ip"
											style="width: 100px;" value="0" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
									</div>

									<div class="tra tra6">
										<span>网站程序(cms)</span> <input type="text" name="souce_code"
											value="" placeholder="可填写phpcms v9,帝国7.2,织梦5.6,自研等">
									</div>
                                    
									<div class="tra tra6">
										<span>月收入</span>
										 <input type="text" name="month_income" value="0" style="width: 150px"
                            onkeyup="this.value=this.value.replace(/\D/g,'')" 
                                onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
										元
									</div>

									<div class="tra tra6">
										<span>服务器信息</span> <input type="text" name="server_info"
											value="" placeholder="例如:阿里云/香港机房/centos/1g/1cpu/1m">
									</div>

									<div class="tra tra6">
										<span>含第三方平台</span> <input type="text"
											name="third_party" value=""
											placeholder="例如:微信公众号/微博帐号等等">
									</div>

									<div class="tra tra6">
										<span>更新方式</span> <input type="text"
											name="update_type" value=""
											placeholder="例如:编辑更新/自动采集等等">
									</div>

									<div class="tra tra6">
										<span>广告联盟</span> <input type="text" name="advert_aliance"
											value="" placeholder="例如:百度联盟/360联盟等">
									</div>

									<div class="tra tra6">
										<span>联盟帐号处理</span> <input type="radio"
											id="radio-ad_union_account-1"
											name="aliance_acc_handle" value="删除帐号"
											class="regular-radio big-radio" checked=""/><label
											for="radio-ad_union_account-1">删除帐号</label> <input
											type="radio" id="radio-ad_union_account-2"
											name="aliance_acc_handle" value="交接帐号"
											class="regular-radio big-radio"/><label
											for="radio-ad_union_account-2">交接帐号</label>
									</div>
								</div>
								<div class="tra tra6">
									<span><b style="color: red">*</b>价格</span> <input type="text"
										name="price" id="price" value="{pigcms{$price}" class="email" 
										onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" /> 0议价
								</div>

								<div class="tra tra6">
									<span>联系邮箱</span> <input type="text" name="email" class="email"
										value="1379465520@qq.com">
								</div>

								<div class="tra tra6">
									<span>联系电话</span> <input type="text" name="phone" class="email"
										value="13823605273">
								</div>

								<!--end-->
								<div class="tra tra7">
									<label> <input type="checkbox" name="is_agent"
										id="agreementId" value="checked" required="">
										<p>
											我已经阅读并同意《<a href="/Home/About/fwxy" target="_blank">chengjie.com服务协议</a>》
										</p></label>
								</div>
								<div class="tra tra8">
									<input type="button" value="提交" id="buttonFormS">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- 右边 -->

			<div class="g-b-container">
				<div class="m-bb-container">
					<div class="m-bb-title s-bg-fb">
						<a href="javascript:void(0);" class="u-tia s-3c">网站代购代售案例</a>
					</div>
					<div class="m-apply-dg m-apply-dga">
						<div class="m-apply-title">
							<span class="u-fl">网站域名</span> <span class="u-fr">价格</span>
							<div class="u-cls"></div>
						</div>
						<div class="m-animate3" id="animate3">
							<div class="animate3 box">
								<ul class="list">
									<volist id="vo" name="alList">
									<li>
										<div class="spanl">
											<a href="http://{pigcms{$vo.domain}" target="_blank"
												rel="nofollow"> {pigcms{$vo.domain}</a>
										</div>
										<div class="spanr">￥{pigcms{$vo.price|number_format}</div>
										<div class="u-cls"></div>
									</li>
									</volist>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<include file="Public:footer" />
	<include file="Public:sidebar" />
	<div class="s-bg-f7"></div>
	<script
		src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css"
		href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css" />
	<script>
            var post_url = "{pigcms{:U('Procurement/add')}";
        </script>
	<script type="text/javascript" src="{pigcms{$static_path}js/escrow.js"></script>

	<script type="text/javascript"
		src="{pigcms{$static_path}js/jquery.cxscroll.min.js"></script>
	<script type="text/javascript"
		src="{pigcms{$static_path}js/plugs_popupBox.js"></script>
	<script type="text/javascript">
        //表单提交
        var formlock = false;//表单提交锁 防止重复提交
		$("#radio-action-0").click(function(){
			$("#ddrole0").css("display","block");
		});
        $("#radio-action-1").click(function(){
        	$("#ddrole0").css("display","none");
		});
        //表单提交
        $("#buttonFormS").click(function(){
        	var cT = $('input[name="ctaction"]').filter(':checked').val();
            $("#webtypeId").val(cT);
        	var domain=$("#domainId").val();
        	if($.trim(domain).length==0) {
        		showDialog("请输入域名");
            	return "";
        	}
        	if($.trim(domain).length>100) {
        		showDialog("请输入100字符以内的域名");
            	return "";
        	}
        	var descripton=$("#descriptionId").val(); 
        	if($.trim(descripton).length==0) {
        		showDialog("请输入描述");
            	return "";
        	}
        	if($.trim(descripton).length>100) {
        		showDialog("请输入100字符以内的描述");
            	return "";
        	}
        	if (!$('#agreementId').attr('checked')) {
 		    	showDialog("您是否同意服务协议");
            	return "";
        	}
        	$("#webform1").submit();
        });
	</script>
</body>
</html>