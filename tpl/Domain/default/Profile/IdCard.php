<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">


</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>
<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>域名融资</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="g-content u-cls">
	<div class="m-content">




			<section>
				<div class="container nrye">
					<div class="row">



						<div class="col-sm-2">
							<!--sidebar-menu-->
							<div id="sidebar">
								<a href="#" class="submenu_bar  visible-xs-block">
									<ol class="breadcrumb">
										<li><i class="iconfont submenu_d"></i></li>
										<li>首页</li>
										<li class="active">我的账户</li>
									</ol>
								</a>
								<ul style="display: block;"><li class="submenu"><a href="{pigcms{:U('Financing/index')}"><i class="iconfont">&#xe613;</i><span>我的账户</span></a> </li>
									<li class="submenu  ">
										<a href="{pigcms{:U('Investment/lend')}"><i class="iconfont "></i>
											<span>我要理财</span><span class="label  visible-xs-block">3</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Investment/lend')}">我要借出</a></li>
											<li ><a href="{pigcms{:U('Investment/lendList')}">借出列表</a></li>
											<li><a href="{pigcms{:U('Investment/automatic')}">自动投标</a></li>
											<li ><a href="{pigcms{:U('Investment/keepInvest')}">回款续投</a></li>
										</ul>
									</li>
									<li class="submenu "><a href="{pigcms{:U('Loan/borrow')}"><i class="iconfont "></i><span>我要借款</span><span class="label  visible-xs-block">3</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Loan/borrow')}">我要借入</a></li>
											<li><a href="{pigcms{:U('Loan/borrowList')}">借入列表</a></li>
											<li ><a href="{pigcms{:U('Loan/pledge')}">质押管理</a></li>
										</ul>
									</li>
									<li class="submenu"><a href="#"><i class="iconfont "></i><span>资金管理</span><span class="label  visible-xs-block">6</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Account/Account')}">账户充值</a></li>
											<li><a href="{pigcms{:U('Account/withdrawal')}">账户提现</a></li>
											<li><a href="{pigcms{:U('Account/setbankcard')}">提现银行</a></li>
											<li><a href="{pigcms{:U('Account/record')}">资金记录</a></li>
											<li><a href="{pigcms{:U('Account/frozendetail')}">冻结记录</a></li>
										</ul>
									</li>
									<li class="submenu  active open"><a href="#"><i class="iconfont "></i><span>账户管理</span><span class="label  visible-xs-block">7</span></a>
										<ul>
											<li ><a href="{pigcms{:U('Profile/update')}">个人信息</a></li>
											<li><a href="{pigcms{:U('Profile/avatar')}">上传头像</a></li>
											<li class="active"><a href="{pigcms{:U('Profile/IdCard')}">身份认证</a></li>
											<li><a href="{pigcms{:U('Account/updatepwd')}">登录密码</a></li>
											<li><a href="{pigcms{:U('Profile/transactionPassword')}">交易密码</a></li>
											<li><a href="{pigcms{:U('Account/bind')}">绑定手机</a></li>
											<!--											<li><a href="/Member/InfoCenter">站内信</a></li>-->
										</ul>
									</li>
								</ul>
							</div>
							<!--sidebar-menu end-->
						</div>
						<form id="IDcard" action="{pigcms{:U('Profile/IdCard')}"  method="post" enctype="multipart/form-data">
						<div class="col-sm-10 ">
							<div class="container_page  ">
								<section>
									<h2>身份认证</h2>

									<div class="form-default" <if condition="$result_idcard['card_id']"> <else />style="display: none;"</if> >
										<div class="form-group">
											<if condition="$result_idcard['status'] eq 1">
												恭喜您！实名认证申请审核通过。
											<else />
											您的申请已经提交，工作人员会在1-2个工作日内为您处理
											</if>
										</div>
									</div>

									<div class="form-default" <if condition="$result_idcard['card_id']"> style="display: none;"<else /></if>>
										<div class="form-group">
											<label for="input1" class=" control-label">身份证类型：</label>
											<div class="col-sm-3">
<!--												<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>-->
                                            <span class="font_c_grey" style="display: inline-block; vertical-align: middle;">
                                                <img src="{pigcms{$static_path}images/account/sf.gif"><br />
                                                二代身份证
                                            </span>
											</div>
										</div>
										<div class="form-group">
											<label for="input2" class=" control-label">身份证号码：</label>
											<span class="font_c_grey pull-left">(18位)</span>
											<div class="col-sm-4">
											<input class="form-control" id="Number" minlength="18" maxlength="18" name="id_number" required="required" type="text" value="" placeholder="请输入真实的身份证号" />
											</div>

										</div>
										<div class="form-group">
											<label for="input2" class="control-label">身份证照片：</label>
											<div class="col-sm-8 btn-xs-block">
												<span class="font_c_grey pull-left">(正面)</span>
												<input type="file" name="facadeFile" id="facadeFile" class="pull-left">
												<span class="font_c_grey pull-left" style="clear: both">(背面)</span>
												<input type="file" name="reverseSideFile" id="reverseSideFile" class="pull-left">
												<p class="up_info">
													证件必须是清晰彩色原件电子版，可以是扫描件或者是数码相机拍摄照片，不能使用复印件。<br />
													仅支持.jpg .jpge.bmp.png的图片格式。图片大小不超过2M。
												</p>
											</div>
										</div>
									</div>
									<div class="btn_area"  <if condition="$result_idcard['card_id']"> style="display: none;"<else /></if>>
										<input type="submit" class="btn btn-primary btn-warning btn-xs-block" value="提交">
									</div>
								</section>

							</div>
						</div>
						</form>


					</div>
				</div>
			</section>

	</div>
</div>
<include file="Public:footer"/>

<script src="{pigcms{$static_path}js/unslider.js"></script>
<script src="{pigcms{$static_path}js/bootstrap.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/rz_common.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/matrix.js"></script>



</body>
</html>