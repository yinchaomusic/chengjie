<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">



</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>
<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>域名融资</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="g-content u-cls">
	<div class="m-content">

		<section>
			<div class="container nrye">
				<div class="row">
					<div class="col-sm-2">
						<!--sidebar-menu-->
						<div id="sidebar">
							<a href="#" class="submenu_bar  visible-xs-block">
								<ol class="breadcrumb">
									<li><i class="iconfont submenu_d"></i></li>
									<li>首页</li>
									<li class="active">我的账户</li>
								</ol>
							</a>
							<ul style="display: block;"><li class="submenu  active"><a href="{pigcms{:U('Financing/index')}"><i class="iconfont">&#xe613;</i><span>我的账户</span></a> </li>
								<li class="submenu  ">
									<a href="{pigcms{:U('Investment/lend')}"><i class="iconfont "></i>
										<span>我要理财</span><span class="label  visible-xs-block">3</span></a>
									<ul>
										<li><a href="{pigcms{:U('Investment/lend')}">我要借出</a></li>
										<li><a href="{pigcms{:U('Investment/lendList')}">借出列表</a></li>
										<li><a href="{pigcms{:U('Investment/automatic')}">自动投标</a></li>
										<li><a href="{pigcms{:U('Investment/keepInvest')}">回款续投</a></li>
									</ul>
								</li>
								<li class="submenu "><a href="{pigcms{:U('Loan/borrow')}"><i class="iconfont "></i><span>我要借款</span><span class="label  visible-xs-block">3</span></a>
									<ul>
										<li><a href="{pigcms{:U('Loan/borrow')}">我要借入</a></li>
										<li><a href="{pigcms{:U('Loan/borrowList')}">借入列表</a></li>
										<li><a href="{pigcms{:U('Loan/pledge')}">质押管理</a></li>
									</ul>
								</li>
								<li class="submenu "><a href="#"><i class="iconfont "></i><span>资金管理</span><span class="label  visible-xs-block">6</span></a>
									<ul>
										<li><a href="{pigcms{:U('Account/Account')}">账户充值</a></li>
										<li><a href="{pigcms{:U('Account/withdrawal')}">账户提现</a></li>
										<li><a href="{pigcms{:U('Account/withdrawal')}">提现银行</a></li>
										<li><a href="{pigcms{:U('Account/record')}">资金记录</a></li>
										<li><a href="{pigcms{:U('Account/frozendetail')}">冻结记录</a></li>
									</ul>
								</li>
								<li class="submenu "><a href="#"><i class="iconfont "></i><span>账户管理</span><span class="label  visible-xs-block">7</span></a>
									<ul>
										<li ><a href="{pigcms{:U('Profile/update')}">个人信息</a></li>
										<li><a href="{pigcms{:U('Profile/avatar')}">上传头像</a></li>
										<li><a href="{pigcms{:U('Profile/IdCard')}">身份认证</a></li>
										<li><a href="{pigcms{:U('Account/updatepwd')}">登录密码</a></li>
										<li><a href="{pigcms{:U('Profile/transactionPassword')}">交易密码</a></li>
										<li><a href="{pigcms{:U('Account/bind')}">绑定手机</a></li>
										<!--											<li><a href="/Member/InfoCenter">站内信</a></li>-->
									</ul>
								</li>
							</ul>
						</div>
						<!--sidebar-menu end-->
					</div>

					<div class="col-sm-10">
						<div class="container_page user_page_area">
							<section>
								<h4><span>{pigcms{$user_session.nickname} [ID:{pigcms{$user_session.uid}]</span>，欢迎来到域名融资平台！</h4>
								<div class="user_input_box clearfix">
									<dl>
										<dt>授信总额：</dt>
										<dd>
											<a href="{pigcms{:U('Loan/pledge')}" class="a">￥{pigcms{$pledges.credit_all}</a>
											<if condition="$result_idcard['status'] neq 1">
											<a href="{pigcms{:U('Profile/IdCard')}" class="btn btn-default btn-sm">申请授信</a>
											</if>
										</dd>
										<dt>可用授信：</dt>
										<dd><a href="{pigcms{:U('Loan/borrowList')}" class="a">￥{pigcms{$pledges['credit_now']}</a></dd>
										<dt>借款总额：</dt>
										<dd><a href="{pigcms{:U('Loan/borrowList')}" class="a">￥{pigcms{$pledges['brrowing_total']}</a></dd>
										<dt>待还总额：</dt>
										<dd><a href="{pigcms{:U('Loan/borrowList',array('state'=>2))}" class="a">￥{pigcms{$pledges.stay_total}</a></dd>
										<dt>借款荣誉：</dt>
										<dd><a <if condition="$pledges['honor'] eq 1">class="level level_1"
												<elseif condition="$pledges['honor'] eq 2"/>class="level level_2"
												<elseif condition="$pledges['honor'] eq 3"/>class="level level_3"
											</if> href="{pigcms{:U('Financing/level')}">
												<if condition="$pledges['honor'] eq 1">普通会员
												<elseif condition="$pledges['honor'] eq 2"/>钻石会员
												<elseif condition="$pledges['honor'] eq 3"/>皇冠会员
												</if>
											</a></dd>
									</dl>
									<dl>
										<dt>账户总额：</dt>
										<dd><a href="{pigcms{:U('Account/record')}" class="a">￥{pigcms{$user_session['now_money']+$user_session['freeze_money']}</a></dd>
										<dt>冻结金额：</dt>
										<dd><a href="{pigcms{:U('Account/frozendetail')}" class="a">￥{pigcms{:number_format
												($user_session['freeze_money'],2)}</a></dd>
										<dt>可用余额：</dt>
										<dd>
											<span class="span">￥{pigcms{:number_format($user_session['now_money'],2)}</span>
											<a href="{pigcms{:U('Account/recharge')}" class="btn btn-default btn-sm">充值</a>
											<a href="{pigcms{:U('Account/withdrawal')}" class="btn btn-default btn-sm">提现</a>
										</dd>
										<dt>借出总额：</dt>
										<dd><a href="{pigcms{:U('Investment/lendList',array('state'=>2))}" class="a">￥{pigcms{$pledges.lend_total}</a></dd>
										<dt>理财荣誉：</dt>
										<dd><a <if condition="$pledges['honor'] eq 1">class="level level_1"
												<elseif condition="$pledges['honor'] eq 2"/>class="level level_2"
												<elseif condition="$pledges['honor'] eq 3"/>class="level level_3"
											</if> href="{pigcms{:U('Financing/level')}"><if condition="$pledges['honor'] eq 1">普通会员
												<elseif condition="$pledges['honor'] eq 2"/>钻石会员
												<elseif condition="$pledges['honor'] eq 3"/>皇冠会员
											</if></a></dd>
									</dl>
								</div>
								<div class="btn_area">
									<a href="{pigcms{:U('Investment/lend')}" class="btn btn-primary btn-warning">我要理财</a>
									<a href="{pigcms{:U('Loan/borrow')}" class="btn btn-green">我要借款</a>
									<a href="{pigcms{:U('Investment/lendList')}" class="btn btn-default">借出列表</a>
									<a href="{pigcms{:U('Loan/borrowList')}" class="btn btn-default">借入列表</a>
									<a href="{pigcms{:U('Loan/paymentHistory')}" class="btn btn-danger">最近一个月内还需还款记录</a>
								</div>
								<ul class="list-group security">
									<li class="list-group-item clearfix">
										<span class="iconfont">&#xe629;</span>
										<span class="lt">邮箱认证</span>

										<if condition="$user_session['is_check_email']">
										<span class="info">{pigcms{$user_session.email}</span>
										<span class="state">已经认证</span>
										<else />
											<span class="info">更好地核实您的身份，保障您的账户安全</span>
											<a href="{pigcms{:U('Account/bindEmail')}" class="state">马上认证</a>
										</if>
									</li>
									<li class="list-group-item clearfix">
										<span class="iconfont"></span>
										<span class="lt">手机认证</span>
										<if condition="$user_session['is_check_phone']">
											<span class="info">{pigcms{$user_session.phone}</span>
											<span class="state">已经认证</span>
										<else />
											<span class="info">更好地核实您的身份，保障您的账户安全</span>
											<a href="{pigcms{:U('Account/bind')}" class="state">马上认证</a>
										</if>
									</li>
									<li class="list-group-item clearfix">
										<span class="iconfont"></span>
										<span class="lt">身份认证</span>
										<if condition="$result_idcard['status'] eq 1">
											<span class="info">恭喜您身份证认证成功 </span>
											<span class="state">已经认证</span>
										<else/>
										<span class="info">更好地核实您的身份，保障您的账户安全</span>
										<a href="{pigcms{:U('Profile/IdCard')}" class="state">马上认证</a>
										</if>
									</li>
									<li class="list-group-item clearfix">
										<span class="iconfont"></span>
										<span class="lt">交易密码</span>
										<if condition="$is_set_trade_pass">
											<span class="info">为了保障您的资金安全，建议定期修改交易密码</span>
											<a href="{pigcms{:U('Profile/transactionPassword')}" class="state">我要修改</a>
										<else/>
											<span class="info">您未设置交易密码</span>
											<a href="{pigcms{:U('Profile/transactionPassword')}" class="state">马上设置</a>
										</if>

									</li>
								</ul>
							</section>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
<include file="Public:footer"/>
<include file="Public:account_footer"/>
<script src="{pigcms{$static_path}js/unslider.js"></script>
<script src="{pigcms{$static_path}js/bootstrap.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/rz_common.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/matrix.js"></script>
</body>
</html>