<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>{pigcms{$config.seo_title}</title>
	<meta name="keywords" content="{pigcms{$config.seo_keywords}" />
	<meta name="description" content="{pigcms{$config.seo_description}" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="{pigcms{$config.site_url}/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="{pigcms{$config.site_url}/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/common.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<script>
		if(/(iphone|ipod|android|windows phone)/.test(navigator.userAgent.toLowerCase())){
			if(confirm('系统检测到您可能正在使用手机访问，是否要跳转到手机版网站？')){
				window.location.href = "{pigcms{:U('Wap/Home/index')}";
			}
		}
	</script>
	<style>
		.advbox {
			margin:0 auto; text-align: center; width: 100%;border: 0;
		}
		.advbox .dt_toBig {
			background: url("{pigcms{$static_path}images/open.png"); margin: 5px 0px 0px 440px; left: 50%; width: 49px; height: 21px; position: absolute;  cursor: pointer;
		}
		.advbox .dt_toSmall {
			background: url("{pigcms{$static_path}images/close.png"); margin: 5px 0px 0px 440px; left: 50%; width: 49px; height: 21px; position: absolute; cursor: pointer;
		}
	</style>
</head>
<body class="s-bg-global">
<if condition="is_array($adverList)">
	<div class="advbox">
		<div style="display: none;background-color:{pigcms{$adverList[0].bg_color}" class="dt_small">
			<div style="display: none;" class="dt_toBig"></div>
			<a href="{pigcms{$adverList[0].url}" target="_blank">
				<img  src="upload/adver/{pigcms{$adverList[0].pic}" width="1000px" height="60px" style="border: 0;margin: 0;"></a>
		</div>
		<div class="dt_big" style="background-color:{pigcms{$adverList[1].bg_color}">
			<div class="dt_toSmall"></div>
			<a href="{pigcms{$adverList[1].url}"  target="_blank">
				<img id="actionimg" alt="" src="upload/adver/{pigcms{$adverList[1].pic}"  width="1000px" height="250px" style="border: 0;margin: 0;"></a>
		</div>
	</div>
</if>



	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div id="focusBar">
		<ul class="mypng">
			<pigcms:adver cat_key="domain_index_banner" limit="6" var_name="domain_index_banner">
				<li id="focusIndex{pigcms{$i-1}" style="background-color:{pigcms{$vo.bg_color};">
					<div class="focusL">
						<a href="{pigcms{$vo.url}"><img src="{pigcms{$vo.pic}"></a>
					</div>
					<div class="focusR">
						<a href="{pigcms{$vo.url}" style="display:none;"><img src="{pigcms{$vo.pic}"></a>
					</div>
					<div style="width:980px;height:210px;margin:0 auto;"></div>
				</li>
			</pigcms:adver>
		</ul>
		<div class="control" style="margin-left:-105px;"></div>
	</div>
	<div class="g-content">
		<!-- 优质 -->
		<div class="m-highQuality">
			<div class="g-a-container u_be2 s-bg-white">
				<div class="m-ta">
					<div class="title">
						<a class="s-47" href="{pigcms{:U('Fastbid/index')}">
							<span class="z-img-t4"></span>
							<h2>极速竞价</h2>
						</a>
					</div>
					<div class="more">
						<a href="{pigcms{:U('Fastbid/index')}" target="_blank">更多>></a>
					</div>
					<div class="control_a" id="k-spano">
						<for start="0" end="3">
							<if condition="ceil(count($domains_youzhi_list)/8) gt $i">
								<if condition="$i eq 0"><span class='t'></span><else/><span></span></if>
							</if>
						</for>
					</div>
				</div>
				<div class="g-animate-a">
					<div class="m-animate-a" id="m-highQuality">
						<volist name="domains_youzhi_list" id="vo" >
							<if condition="$key%8 eq 0">
								<ul class="u-udata-b">
							</if>
							<li>
								<a href="{pigcms{:U('Fastbid/detail',array('id'=>$vo['domain_id']))}" target="_blank">
									<h4 class="f-p-t1 s-bg-fb" title="{pigcms{$vo.domain}">{pigcms{$vo.domain}</h4>
									<div class="m-data-DomainList">
										<p class="f-p6" title="{pigcms{$vo.desc}">{pigcms{$vo.desc}</p>
										<p class="f-p7">
											<span>当前价：</span>
											<span class="s-75b money" title="<if condition="$vo['money'] gt 1">￥{pigcms{$vo.money|floor}<else/>议价</if>">
											<if condition="$vo['money'] gt 1">￥{pigcms{$vo.money|number_format=###}<else/>议价</if></span>
											<span <if condition="$vo['is_hot']"> class='z-dh' title='火'</if> ></span>
										</p>
									</div>
								</a>
							</li>
					   <if condition="$key%8 eq 7">
							</ul>
						</if>
						</volist>

					</div>
				</div>
			</div>
			<div class="g-b-container">
				<div class="m-ba-container">
					<p class="f-p-t2">申请代购</p>
					<p>
						<span class="u-ipt-a">
							<input type="text" ph="请输入你要代购的域名" id="dg_txt" />
						</span>
						<a href="javascript:void(0);" class="u-btn-dg s-bg-fb75" id="dg_btn" style="width: 83px; padding: 0; text-align: center;">代购</a>
					</p>
					<p><a class="u-abtn-zj s-bg-2a" target="_blank" href="{pigcms{:U('Escrow/index')}">发起中介交易</a></p>
				</div>
				<div class="m-bb-container">
					<div class="m-bb-title s-bg-fb">
						<a href="{pigcms{:U('News/index',array('type'=>$cate_list['cat_key']))}" class="u-tia u-fl">{pigcms{$cate_list['cat_name']}</a>
						<a href="{pigcms{:U('News/index',array('type'=>$cate_list['cat_key']))}" class="u-fr">更多>></a>
					</div>
					<ul class="u-cls">
						<volist name="news_list" id="vo">
						<li  <if condition="$i eq 1"> class="t"</if>>
							<span>{pigcms{$i}</span>
							<a href="{pigcms{:U('News/show',array('news_id'=>$vo['news_id']))}" target='_blank' title='{pigcms{$vo.news_title}'>{pigcms{$vo.news_title}</a>
						</li>
						</volist>
					</ul>
				</div>
			</div>
			<div class="u-cls"></div>
		</div>
		<!-- 一口价 -->
		<div class="m-oncePricing u-cls">
			<div class="g-a-container u_be2 s-bg-white">
				<div class="m-ta">
					<div class="title">
						<a class="s-47" href="{pigcms{:U('Hotsale/index')}">
							<span class="z-img-t2"></span>
							<h2>一口价域名</h2>
						</a>
					</div>
					<div class="more">
						<a href="{pigcms{:U('Hotsale/index')}" target="_blank">更多>></a>
					</div>
					<div class="control_a" id="k-spant">
						<for start="0" end="3">
							<if condition="ceil(count($domains_ykj_list)/8) gt $i">
								<if condition="$i eq 0"><span class='t'></span><else/><span></span></if>
							</if>
						</for>          
					</div>
					<div class="u-cls"></div>
				</div>
				<div class="g-animate-a">
					<div class="m-animate-a" id="m-oncePricing">
						<volist name="domains_ykj_list" id="vo" >
							<if condition="$key%8 eq 0">
								<ul class="u-udata-b">
							</if>
							<li>
								<a href="{pigcms{:U('Hotsale/selling',array('id'=>$vo['domain_id']))}" target="_blank">
									<h4 class="f-p-t1 s-bg-fb" title="{pigcms{$vo.domain}">{pigcms{$vo.domain}</h4>
									<div class="m-data-DomainList">
										<p class="f-p6" title="{pigcms{$vo.desc}">{pigcms{$vo.desc}</p>
										<p class="f-p7">
											<span>价格：</span>
											<span class="s-75b money" title="<if condition="$vo['money'] gt 1">￥{pigcms{$vo
											.money|floor}<else/>议价</if>"><if condition="$vo['money'] gt 1">￥{pigcms{$vo
													.money|number_format=###}<else/>议价</if></span>
											<span <if condition="$vo['is_hot']"> class='z-dh' title='火'</if> ></span>
										</p>
									</div>
								</a>
							</li>
							<if condition="$key%8 eq 7 || ($key%8 neq 7 && $i eq count($domains_ykj_list))">
								</ul>
							</if>
						</volist>
					</div>
				</div>
			</div>
			<div class="g-b-container">
				<div class="m-bb-container">
					<div class="m-bb-title s-bg-fb">
						<a href="javascript:void(0)" class="u-tia u-fl">最近成交</a>
					</div>
					<div class="m-apply-dg">
						<table>
							<tr class="s-35">
								<th width="135px" class="u_tl">域名</th>
								<th width="102px" class="u_tr"></th>
							</tr>
							<volist name="domains_news_list" id="vo">
							<tr>
								<td><a href='javascript:void(0)' class='u_tl' title='{pigcms{$vo.domain}'>{pigcms{$vo.domain}</a></td>
								<td><span class='u_tr s-75b' title='{pigcms{$vo.desc}'>{pigcms{$vo.desc} </span></td>
							</tr>
							</volist>
						</table>
					</div>
				</div>
			</div>
			<div class="u-cls"></div>
		</div>
		<!-- 议价域名 -->
		<div class="m-discuss-price">
			<div class="g-a-container u_be2 s-bg-white">
				<div class="m-ta">
					<div class="title">
						<a class="s-47" href="{pigcms{:U('Buydomains/index')}">
							<span class="z-img-t3"></span>
							<h2>议价域名</h2>
						</a>
					</div>
					<div class="more">
						<a href="{pigcms{:U('Buydomains/index')}">更多>></a>
					</div>
					<div class="u-cls"></div>
				</div>
				<div class="m-discussList">
					<volist name="domains_yj_list" id="vo" >
						<if condition="$key%6 eq 0">
					<table class="u-fl u_line_l">
						<tr class="s-bg-fb">
							<th width="14px"></th>
							<th width="113px" class="u_tl">域名</th>
							<th width="35px">热度</th>
							<th width="110px" class="u_tr">价格</th>
							<th width="62px" class="u_tr">主题</th>
							<th width="14px"></th>
						</tr>
						</if>
						<tr >
							<td></td>
							<td>
								<a class='' target='_blank' href="{pigcms{:U('Buydomains/detail',array('id'=>$vo['domain_id']))}" title='{pigcms{$vo.domain}'>{pigcms{$vo.domain}</a>
							</td>
							<td>
								<span <if condition="$vo['is_hot']">class='z-img-hota' title='火'<else />class='z-img-hot' title='火'</if> ></span>
							</td>
							<td>
								<span class='price '><if condition="$vo['money'] gt 1">￥{pigcms{$vo.money|number_format=###}<else/>议价</if></span>
							</td>
							<td><span class='style'>其它</span></td>
							<td></td>
						</tr>
						<if condition="$key%6 eq 5 || ($key%6 neq 5 && $i eq count($domains_yj_list))">
					</table>
						</if>
				</volist>
				</div>
			</div>
			<div class="g-b-container">
				<div class="m-bc-container">
					<div class="m-bb-title s-bg-fb">
						<a href="{pigcms{:U('News/index',array('type'=>$Xcate_list['cat_key']))}" class="u-tia u-fl">{pigcms{$Xcate_list['cat_name']}</a>
						<a rel="nofollow" href="{pigcms{:U('News/index',array('type'=>$Xcate_list['cat_key']))}" target="_target" class="u-fr">更多&gt;
							&gt;</a>
					</div>
					<ul class="u-cls" id="announcement">
						<volist name="Xnews_list" id="vo">
						<li <if condition="$i eq 1"> class="t" </if>><span>{pigcms{$i}</span>
							<a href="{pigcms{:U('News/show',array('type'=>$vo['cat_key'],'news_id'=>$vo['news_id']))}" target="_blank" title="{pigcms{$vo['news_title']}">{pigcms{$vo['news_title']}</a>
						</li>
						</volist>
						</ul>
				</div>
				<div class="u-cls"></div>
			</div>
			<div class="u-cls"></div>
		</div>
		<!-- 急速竞价 -->
		<!-- 经典案例 -->
		<div>
			<div class="g-classic-case">
				<div class="m-case-title u-fl">
					经典案例
				</div>
				<div id="classic_case">
					<div class="m-case box u-fl">
						<ul class="list">
							<volist name="adver_list" id="vo">
								<li>
									<a rel="nofollow" href="{pigcms{$vo.url}" title="{pigcms{$vo.name}" target="_blank" class="bwWrapper">
										<img alt="{pigcms{$vo.name}" src="upload/adver/{pigcms{$vo.pic}" />
									</a>
								</li>
							</volist>
						</ul>
					</div>
					<div class="cls"></div>
				</div>
				<div class="cls"></div>
			</div>
		</div>
	</div>
	<include file="Public:footer"/>
	<div class="s-bg-f7">
		<div class="s-bg-f7">
			<div class="g-footer-link">
				<p>
					<span><b>友情链接</b>：</span>
					<volist name="flink_list" id="vo">
						<a href="{pigcms{$vo.url}" title="{pigcms{$vo.info}" target="_blank">{pigcms{$vo.name}</a><if condition="$i lt count($flink_list)">&nbsp; - &nbsp;</if>
					</volist>
				</p>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="{pigcms{$static_path}js/jquery.cxscroll.min.js"></script>
	<script type="text/javascript" src="{pigcms{$static_path}js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="{pigcms{$static_path}js/jquery.color.js"></script>
	<script type="text/javascript" src="{pigcms{$static_path}js/banner.js"></script>
	<script type="text/javascript" src="{pigcms{$static_path}js/index.js"></script>
	<include file="Public:sidebar"/>


<if condition="is_array($adverList)">
	<script type="text/javascript">
		function advClick(){
			var a=1000;
			var b=3*1000;
			$(".dt_toSmall").click(function(){
				$(".dt_small").delay(a).slideDown(a);
				$(".dt_big").stop().slideUp(a);
				$(".dt_toSmall").stop().fadeOut("fast");
				$(".dt_toBig").delay(a*2).fadeIn("fast")
			});$
			(".dt_toBig").click(function(){
				$(".dt_big").delay(a).slideDown(a);
				$(".dt_small").stop().slideUp(a);
				$(".dt_toBig").stop().fadeOut("slow");
				$(".dt_toSmall").delay(a*2).fadeIn("fast")
			})
		}

		function advauto(){
			if($(".dt_big").length>0){
				var a=1000;
				var b=3*1000;
				$(".dt_big").delay(b).slideUp(a,function(){
					$(".dt_small").slideDown("slow");
					$(".dt_toBig").delay(a).fadeIn("fast")
				});
				$(".dt_toSmall").delay(b).fadeOut("fast")
			}
		}

		$(document).ready(function(){
			advClick();
		});
		//顶部通览可展开收起效果
		if($("#actionimg").length>0){
			$("#actionimg").onload=advauto();
		}

	</script>
</if>
</body>
</html>
