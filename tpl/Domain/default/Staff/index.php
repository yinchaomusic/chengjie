<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>经纪人管理中心 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header_staff"/>
	<include file="Public:nav_staff"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Staff/index')}" class="s-2a">经纪人管理中心</a></li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<!-- 左边 -->    
<include file="Public:left_staff"/>
        <!-- 右边 -->
        <div class="u-fr">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white u_bb0">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span class="z-img-t19"></span>
                            <span>我的账户</span>
                        </a>
                    </div>
                     <div class="lastTime">
                        <span>上次登录时间：</span><span>{pigcms{$staff.lastTime|date='Y-m-d H:i:s',###}</span>
                    </div>
                    <div class="lastIp">
                        <a href="/account/Loglog">
                            <span>上次登录IP：</span><span class="s1">{pigcms{$staff.lastIp}[ {pigcms{$staff.lastCity}]</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
                <div class="s-bg-fc m-validation">
                </div>
            </div>
            <!-- 个人信息 -->
            <div class="m-personinfo s-bg-fc u_be9">
                <div class="personinfo-l">
                    <p class="s-3c f-p24">您好，<span id="name">{pigcms{$staff.name}</span>，欢迎回来！</p>
                    <p class="u_mb15">
                        <span class="u-span3">中介ID：</span>
                        <span class="u-span5">{pigcms{$staff.id}</span>
                    </p>
                    <p class="u_mb15">
                        <span class="u-span3">会员邮箱：</span>
                        <span class="u-span5">{pigcms{$staff.email}</span>
                    </p>
                    <div style="position: relative;">
                        <span class="u-span3">客户数量： {pigcms{$user_count}</span>
                        <span class="u-span5" style="overflow:inherit;">
							<div class="moneylink" style="display:inline;">
								<span class="link u_2a"></span>
							</div>
						</span>
                    </div>
                </div>
                <div class="personinfo-r"> 
                    <p class="f-p25">
                            <a href="##" class="bind-email s-2a" title="邮箱">已绑定</a>
                            <a href="#" class="bind-phone s-2a" title="手机">已绑定</a>
                    </p>
                    <p class="u_mb15">
                        <span class="u-span4">总交易笔数</span>
                        <a href='#' k="2" class='ubind-kt s-def'>{pigcms{$order_sum.total_sum}</a>

                    </p>
                    <p class="u_mb15">
                        <span class="u-span4">交易成功数：</span>
                        <a href='/account/kt/yz' k="3" class='ubind-kt s-def'>{pigcms{$order_sum.succeed_sum}</a>
                    </p>
                    <p>
                        <span class="u-span4 user_lv" style="cursor: pointer;">交易失败数：</span>
                        <a class="s-def integral">{pigcms{$order_sum.failure_sum}</a>
                    </p>
                </div>
                <div class="u-cls"></div>
            </div>
            
            <div class="m-account-data1 u_be9 s-bg-fc">
                <h4 class="s-bg-white">
                    <a class="s-3c m-ta2" href="/account/deals/dealing">
                        <span class="sp1">进行中的交易</span>
                    </a>
                    <a class="s-ae1 more" href="{pigcms{:U('Staffdeal/ongoing_deal')}">更多&gt;&gt;</a>
                    <div class="u-cls"></div>
                </h4>
                <div class="m-acc-cons s-bg-fc u_mall1" id="myalldeals">
                    <table>
                        <tbody>
						<tr>
                            <th width="16px"></th>
                            <th width="150px" class="u_tl"><span order="title">域名</span></th>
                            <th width="120px" class="u_tr"><span order="buymoney">金额</span></th>
                            <th width="36px"></th>
                            <th width="100px" class="u_tl"><span order="_type">来源</span></th>
                            <th width="180px" class="u_tl"><span order="_state" class="f-img-up">状态</span></th>
                            <th width="64px">操作</th>
							<th width="8px"></th>
                        </tr>
							<if condition="$domains_list">
						<volist id='vo' name='domains_list'>
						<tr>
							<td></td>
							<td class="u_tl"><a href="{pigcms{:U('Staffdeal/dealdetails',array('order_id'=>$vo['order_id']))}" target="_blank" class="u-txa1" title="{pigcms{$vo.domainName}"><if condition="$vo['domainName'] eq ''">批量交易<else/>{pigcms{$vo.domainName}</if></a></td>
							<td class="u_tr s-75b">￥{pigcms{$vo.total_price}</td>
							<td></td>
							<td class="u_tl">中介</td>
							<td class="u_tl">
                                                        
                                                               <if condition="$vo['status'] eq 1">已介入<elseif condition="$vo['status'] eq 2"/><font color="red">交易完成</font><else/><font color="red">交易失败</font></if>
                                                        
                                                        </td>
							<td><a class="u-abtn8" href="{pigcms{:U('Staffdeal/dealdetails',array('order_id'=>$vo['order_id']))}" target="_blank">详情</a></td>
							<td></td>
						</tr>
						</volist>
                                                            <else/>
						
						<tr><td colspan="9" class="u_nb u_tc u_norecord">暂无记录！</td></tr>
                                                </if>
						
					</tbody>
					</table>
                </div>
            </div>
            

           
        </div>
		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<script src="{pigcms{$static_path}js/account.js"></script>
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}
	</script>
	<include file="Public:sidebar"/>
</body>
</html>
