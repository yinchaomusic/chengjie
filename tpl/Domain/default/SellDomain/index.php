<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>卖域名 - 域名出售 - 域名价值 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/common.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>卖域名</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
<div class="g-content u-cls">
	<div class="m-content">
		<!-- 左边 -->
		<div class="g-a-container">
			<div class="m-ta s-bg-white u_be9">
				<div class="title">
					<a class="s-3c" href="javascript:void(0)">
						<span class="z-img-t5"></span>
						<span>卖域名</span>
					</a>
				</div>
				<div class="u-cls"></div>
			</div>
			<div class="m-insa s-bg-fc u_be89">
				<p>
					<strong>{pigcms{$now_site_short_url}</strong>，优质域名交易平台从2015年创立至今，几年来依靠贴心优质的服务、安全放心的交易保障，自由诚信的宗旨，已积累了五万多海内外忠实用户，几十万在售域名。目前域名年交易金额达数亿元。在域名投资者和企业终端间，取得了颇高的知名度和良好的口碑。
				</p>
			</div>
			<div class="m-sell s-bg-white u_be2">
				<table>
					<tr>
						<th width="270px"><span class="u_fw">功能</span></th>
						<th width="106px">议价出售</th>
						<th width="106px">极速竞价</th>
						<th width="106px">一口价</th>
						<th width="106px" class="n_br">优质域名</th>
					</tr>
					<tr>
						<td class="u_num">
							<span class="u_txe">多重推荐</span>
							<span class="u_txf">[1]</span>
						</td>
						<td><span class="f-img-s1"></span></td>
						<td><span class="f-img-s1"></span></td>
						<td><span class="f-img-s1"></span></td>
						<td class="n_br"><span class="f-img-s1"></span></td>
					</tr>
					<tr>
						<td class="s-bg-e3 u_num">
							<span class="u_txe">域名出售</span>
							<span class="u_txf">[2]</span>
						</td>
						<td class="s-bg-f8"><span class="f-img-s1"></span></td>
						<td class="s-bg-f8"><span class="f-img-s1"></span></td>
						<td class="s-bg-f8"><span class="f-img-s1"></span></td>
						<td class="s-bg-f8 n_br"><span class="f-img-s1"></span></td>
					</tr>
					<tr>
						<td><span class="u_txe">自主议价</span></td>
						<td><span class="f-img-s1"></span></td>
						<td><span class="f-img-s2"></span></td>
						<td><span class="f-img-s2"></span></td>
						<td class="n_br"><span class="f-img-s1"></span></td>
					</tr>
					<tr>
						<td class="s-bg-e3 u_num">
							<span class="u_txe">人工审核</span>
							<span class="u_txf">[3]</span>
						</td>
						<td class="s-bg-f8"><span class="f-img-s2"></span></td>
						<td class="s-bg-f8"><span class="f-img-s1"></span></td>
						<td class="s-bg-f8"><span class="f-img-s2"></span></td>
						<td class="s-bg-f8 n_br"><span class="f-img-s1"></td>
					</tr>
					<tr>
						<td class="u_num">
							<span class="u_txe">联盟优势</span>
							<span class="u_txf">[4]</span>
						</td>
						<td><span class="f-img-s2"></span></td>
						<td><span class="f-img-s2"></span></td>
						<td><span class="f-img-s1"></span></td>
						<td class="n_br"><span class="f-img-s1"></span></td>
					</tr>
					<tr>
						<td class="s-bg-e3 u_num">
							<span class="u_txe">出售保证金</span>
							<span class="u_txf">[5]</span>
						</td>
						<td class="s-bg-f8"><span class="f-img-s2"></span></td>
						<td class="s-bg-f8"><span class="u-txg">保留价<span></span>*1%<br />
                            最多￥2000</span></td>
						<td class="s-bg-f8"><span class="u-txg">需￥1000<br />
                            需开通服务</span></td>
						<td class="s-bg-f8 n_br"><span class="u-txg">需￥1000<br />
                            需开通服务</span></td>
					</tr>
					<tr>
						<td>
                                                    <span class="u_txe">服务费用</span>
						</td>
                                                <if condition="is_array($user_level_info)">
                                                    <td><a href="javascript:void(0);" class="u-abtn1 s-2a u_2a">交易金额 {pigcms{$user_level_info.boon}%</a></td>
                                                    <td><a href="javascript:void(0);" class="u-abtn1 s-2a u_2a">交易金额 {pigcms{$user_level_info.boon}%</a></td>
                                                    <td><a href="javascript:void(0);" class="s-def" style="font-size: 14px;">交易金额 8%</a></td>
                                                    <td class="n_br"><a href="javascript:void(0);" class="u-abtn1 s-2a u_2a">交易金额 {pigcms{$user_level_info.boon}%</a></td>
                                                    <else/>
                                                    <td><a href="javascript:void(0);" class="u-abtn1 s-2a u_2a">交易金额 6%</a></td>
                                                    <td><a href="javascript:void(0);" class="u-abtn1 s-2a u_2a">交易金额 6%</a></td>
                                                    <td><a href="javascript:void(0);" class="s-def" style="font-size: 14px;">交易金额 8%</a></td>
                                                    <td class="n_br"><a href="javascript:void(0);" class="u-abtn1 s-2a u_2a">交易金额 6%</a></td>
                                                </if>
					</tr>
					<tr>
						<td class="s-bg-e3">
							<span class="u_txe">优势&amp;特色</span>
						</td>
						<td class="s-bg-f8"><span class="u-txg">双方议价 <br/>交易自由</span></td>
						<td class="s-bg-f8"><span class="u-txg">即拍即卖<br/> 价高者得</span></td>
						<td class="s-bg-f8"><span class="u-txg">明码标价<br/> 即刻购买</span></td>
						<td class="s-bg-f8 n_br"><span class="u-txg">珍惜精品<br/> 值得青睐</span></td>
					</tr>
					<tr>
						<td></td>
						<td><a href="{pigcms{:U('Account/index')}" class="u-abtn2 s-bg-2a">立即添加</a></td>
						<td><a href="{pigcms{:U('Account/index')}" class="u-abtn2 s-bg-2a">立即申请</a></td>
						<td><a href="{pigcms{:U('Account/index')}" class="u-abtn2 s-bg-2a">开通使用</a></td>
						<td class="n_br"><a href="{pigcms{:U('Account/index')}" class="u-abtn2 s-bg-2a">开通使用</a></td>
					</tr>
				</table>
			</div>
			<div class="m-insa s-bg-fc u_be89">
				<h4>备注：</h4>
				<ul>
					<li><span class="u_fw">1.</span> 根据域名性价比，免费在首页、功能分区、搜索等区域多重推荐。</li>
					<li><span class="u_fw">2.</span> 提供多种模板及自定义功能，为域名创建单独的展示页面。</li>
					<li><span class="u_fw">3.</span> 人工审核机制，提交后2-4小时内审核。</li>
					<li><span class="u_fw">4.</span> 您的域名将有机会在百度推广、爱站网、站长百科等网站展出</li>
					<li><span class="u_fw">5.</span> 保证金暂时冻结，正常取消或交易完成后全额退还。违约则扣除，未违约方和平台各获得违约金的50%。</li>
				</ul>
			</div>
			<div class="m-pop s-bg-fa" id="pop10">
				<p class="f-p-t7 u_bb0">
					<span class="u-fl">会员等级</span>
					<span class="u-fr close"></span>
					<span class="u-cls"></span>
				</p>
				<table>
					<tr>
						<th width="20px"></th>
						<th width="30px">等级</th>
						<th width="50px"></th>
						<th width="168px">积分</th>
						<th width="100px" class="u_tr">手续费</th>
						<th width="46px"></th>
						<th width="110px">一口价提交数量</th>
						<th width="20px"></th>
					</tr>
                                        <volist name="level_list" id="vo">
                                            
                                            <tr>
                                        
						<td></td>
						<td>{pigcms{$vo.lname}</td>
						<td></td>
						<td>
                                                        <span>{pigcms{$vo.integral}</span>
						</td>
						<td class='s-75b u_tr'>{pigcms{$vo.boon}%</td>
						<td></td>
						<td class='s-75b'>{pigcms{$vo.number}</td>
						<td></td>
					</tr>
                                            
                                        </volist>
					    </table>
			</div>
		</div>
		<!-- 右边 -->
		<div class="g-c-container">
			<div class="m-adver">
			</div>
			<div class="m-processList u_mt7" id="processList">
				<h3 data-url="yijia">议价域名出售</h3>
				<div class="process">
					<ul>
						<li>
							<span class="f-img-s3">1</span><span class="u-txh">添加域名</span>
						</li>
						<li>
							<span class="f-img-s3">2</span><span class="u-txh">系统验证</span>
						</li>
						<li>
							<span class="f-img-s3">3</span><span class="u-txh">验证成功</span>
						</li>
						<li class="u_mb0">
							<span class="f-img-s3">4</span><span class="u-txh">展示议价</span>
						</li>
					</ul>
				</div>
				<h3 data-url="jisu" class="">极速竞价域名出售</h3>
				<div class="process">
					<ul>
						<li>
							<span class="f-img-s3">1</span><span class="u-txh">添加域名</span>
						</li>
						<li>
							<span class="f-img-s3">2</span><span class="u-txh">系统验证</span>
						</li>
						<li>
							<span class="f-img-s3">3</span><span class="u-txh">验证成功</span>
						</li>
						<li>
							<span class="f-img-s3">4</span><span class="u-txh">提交申请</span>
						</li>
						<li>
							<span class="f-img-s3">5</span><span class="u-txh">平台审核通过</span>
						</li>
						<li>
							<span class="f-img-s3">6</span><span class="u-txh">进入待竞价区</span>
						</li>
						<li class="u_mb0">
							<span class="f-img-s3">7</span><span class="u-txh">正式竞价</span>
						</li>
					</ul>
				</div>
				<h3>一口价域名出售</h3>
				<div class="process">
					<ul>
						<li>
							<span class="f-img-s3">1</span><span class="u-txh">开通一口价服务</span>
						</li>
						<li>
							<span class="f-img-s3">2</span><span class="u-txh">添加域名</span>
						</li>
						<li>
							<span class="f-img-s3">3</span><span class="u-txh">系统验证</span>
						</li>
						<li>
							<span class="f-img-s3">4</span><span class="u-txh">验证成功</span>
						</li>
						<li>
							<span class="f-img-s3">5</span><span class="u-txh">提交一口价域名</span>
						</li>
						<li class="u_mb0">
							<span class="f-img-s3">6</span><span class="u-txh">进入展示</span>
						</li>
					</ul>
				</div>
				<h3 class="">优质域名出售</h3>
				<div class="process">
					<ul>
						<li>
							<span class="f-img-s3">1</span><span class="u-txh">添加域名</span>
						</li>
						<li>
							<span class="f-img-s3">2</span><span class="u-txh">系统验证</span>
						</li>
						<li>
							<span class="f-img-s3">3</span><span class="u-txh">验证成功</span>
						</li>
						<li>
							<span class="f-img-s3">4</span><span class="u-txh">提交优质域名</span>
						</li>
						<li>
							<span class="f-img-s3">5</span><span class="u-txh">平台审核通过</span>
						</li>
						<li class="u_mb0">
							<span class="f-img-s3">6</span><span class="u-txh">进入展示</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<include file="Public:footer"/>
<include file="Public:sidebar"/>

<script type="text/javascript" src="{pigcms{$static_path}js/plugs_popupBox.js"></script>
<script type="text/javascript">
	$("#processList h3").next().slideUp();
	n = "yijia";
	var sld_a = $("#processList h3[data-url$='" + n + "']");
	if (sld_a.length == 0) {
		$(".process_list h3:first").addClass("cli");
		$(".process_list h3:first").next().slideDown();
	} else {
		$(sld_a).addClass("cli");
		$(sld_a).next().slideDown();
	}
	$("#processList h3").click(function () {
		$("#processList h3").next().slideUp();
		$("#processList h3").removeClass("cli");
		var ul = $(this).next();
		console.log(ul.css("display"));
		if (ul.css("display") == "none") {
			$(this).addClass("cli");
			ul.slideDown("fast");
		} else {
			$(this).removeClass("cli");
			ul.slideUp("fast");
		}
	});
	//对应变颜色
	$(".u_num").each(function (i, n) {
		$(n).mouseover(function () {
			$(".m-insa li").eq(i).css("color", "#FFA200");
		}).mouseout(function () {
			$(".m-insa li").eq(i).css("color", "#656567");
		});
	});
	//弹出
	$(".u-abtn1").click(function () {
		showBox("pop10");
		$("#pop10").css("left", "29%");
	});
	$(".close").click(function () {
		closeBox("pop10");
	});
</script>

</body>
</html>