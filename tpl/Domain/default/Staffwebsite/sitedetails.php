<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<style type="text/css">
     span.u-ipt-d{
	   width:260px;
     }
   
    </style>
</head>
<body class="s-bg-global">
	<include file="Public:header_staff"/>
	<include file="Public:nav_staff"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Staff/index')}" class="s-2a">经济人账户</a>&nbsp;&gt;&nbsp;</li>
                                        <li><a href="{pigcms{:U('Staffdeal/purchase_list')}" class="s-2a">代购列表</a>&nbsp;&gt;&nbsp;</li>
                                        <li>网站详情</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Public:left_staff"/>
        <!-- 右边 -->
        <div class="u-fr">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="s-3c" href="javascript:void(0)">
                            <span>网站详情</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <div class="m-acc-select u_be9 s-bg-white">
           <!-- 左边 -->
           
            <!-- 数据 -->
             <div class="m-question m-question1 s-bg-fc u_be9">
                <ul>
                    <div class="transa">
                            <p class="u_mb16">
                                    <span class="u-txj">客户会员ID：</span>
                                    <span class="u-ipt-d">{pigcms{$info.user_id}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">服务类型：</span>
                                 <if condition="$info['entr_type'] eq 1">
                                	 <span class="u-ipt-d">网站代购</span> 
                                	<elseif condition="$info['entr_type'] eq 2"/>
                                	 <span class="u-ipt-d">网站代售</span> 
                                	<else/>
                                	 <span class="u-ipt-d">未知</span> 
                                </if>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">域名：</span>
                                <span class="u-ipt-d">{pigcms{$info.domain}</span>
                            </p>
                           <p class="u_mb16">
                                <span class="u-txj">百度权重pc：</span>
                                <span class="u-ipt-d">{pigcms{$info.pc_baidu}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">百度权重app：</span>
                                <span class="u-ipt-d">{pigcms{$info.app_baidu}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">日ip pc：</span>
                                <span class="u-ipt-d">{pigcms{$info.pc_ip}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">日ip app：</span>
                                <span class="u-ipt-d">{pigcms{$info.app_ip}</span>
                            </p>
                               <p class="u_mb16">
                                <span class="u-txj">源码：</span>
                                <span class="u-ipt-d">{pigcms{$info.souce_code}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">月收入元：</span>
                                <span class="u-ipt-d">{pigcms{$info.month_income}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">服务器信息：</span>
                                <span class="u-ipt-d">{pigcms{$info.server_info}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">第三方平台名称：</span>
                                <span class="u-ipt-d">{pigcms{$info.third_party}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">更新方式：</span>
                                <span class="u-ipt-d">{pigcms{$info.update_type}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">广告联盟：</span>
                                <span class="u-ipt-d">{pigcms{$info.advert_aliance}</span>
                            </p>
                           
                            <p class="u_mb16">
                                <span class="u-txj">联盟帐号处理：</span>
                                 <if condition="$info['aliance_acc_handle'] eq 1">
                                	 <span class="u-ipt-d">删除帐号</span> 
                                	<elseif condition="$info['aliance_acc_handle'] eq 2"/>
                                	 <span class="u-ipt-d">交接帐号</span> 
                                	<else/>
                                	 <span class="u-ipt-d">未知类型</span> 
                                </if>
                            </p>
                             <p class="u_mb16">
                                <span class="u-txj">表示议价：</span>
                                <span class="u-ipt-d">{pigcms{$info.price}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">email：</span>
                                <span class="u-ipt-d">{pigcms{$info.email}</span>
                            </p>
                              <p class="u_mb16">
                                <span class="u-txj">表示议价：</span>
                                <span class="u-ipt-d">{pigcms{$info.price}</span>
                            </p>
                             <p class="u_mb16">
                                <span class="u-txj">网站类型：</span>
                               
                                <select name="web_type" width="250px">
                                   <option value="0">-未知-</option>
                                   <option value="1" <if condition="$info['web_type'] eq 1"> selected</if> >-行业门户-</option>
                                   <option value="2" <if condition="$info['web_type'] eq 2"> selected</if>>-音乐影视-</option>
                                   <option value="3" <if condition="$info['web_type'] eq 3"> selected</if>>-游戏小说-</option>
                                   <option value="4" <if condition="$info['web_type'] eq 4"> selected</if>>-女性时尚-</option>
                                   <option value="5" <if condition="$info['web_type'] eq 5"> selected</if>>-QQ/娱乐-</option>
                                   <option value="6" <if condition="$info['web_type'] eq 6"> selected</if>>-商城购物-</option>
                                   <option value="7" <if condition="$info['web_type'] eq 7"> selected</if>>-地方网站-</option>
                                   <option value="8" <if condition="$info['web_type'] eq 8"> selected</if>>-其他网站-</option>
                                </select>
                            </p>
                             <p class="u_mb16">
                                <span class="u-txj">是否同意：</span>
                                 <if condition="$info['is_agent'] eq 0">
                                	 <span class="u-ipt-d">不同意</span> 
                                	<elseif condition="$info['is_agent'] eq 1"/>
                                	 <span class="u-ipt-d">同意</span> 
                                	<else/>
                                	 <span class="u-ipt-d">未知</span> 
                                </if>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">pr值：</span>
                                <span class="u-ipt-d">{pigcms{$info.pr}</span>
                            </p>
                            <p class="u_mb16">
                                <span class="u-txj">创建时间：</span>
                                <span class="u-ipt-d">{pigcms{$info.createdate}</span>
                            </p>
                            <p>
                                    <span class="u-txj">备注信息：</span>
                                    <span class="u_txera s-bg-white">{pigcms{$info.description}</span>
                            </p>
                        </div>
                       
                        

                             <li>
                                <div class="u_mall12">
                                    <input type="hidden" name="id" value="{pigcms{$info.id}" id="id"/>
                                      <if condition="$info['wstatus'] eq 1">
                                            <input type="submit" id="yes" value="通过" class="u-btn12 s-bg-2a"> 
                                            <input type="submit" id="back" value="驳回" class="u-btn12 s-bg-2a">
                                       <elseif condition="$info['is_agent'] eq 2"/>
                                            <input type="submit" id="over" value="结束" class="u-btn12 s-bg-2a">
                                      </if>
                                </div>
                            </li> 
                            <if condition="$info['wstatus'] eq 0">
                            <p>
                                <span class="u-txj">理由</span>
                                <span class="u_txera s-bg-white">
                                    <textarea name="fail_reason" id="fail_reason"></textarea>
                                </span>
                            </p>
                            </if>
                           
                    </ul>
                    <div class="u-cls"></div>
                </div>
        <!-- 右边 -->
        </div>
		</div>
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<script src="{pigcms{$static_path}js/account.js"></script>
        <script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
        <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">
        <script>
            $("#yes").click(function(){
          	 var id = $("#id").val();
           	 swal({   
                 title: "通过?",   
                 text: "您确定要通过吗!",   
                 type: "warning",   
                 showCancelButton: true,   
                 confirmButtonColor: "#DD6B55",   
                 confirmButtonText: "Yes, 我要通过!",   
                 cancelButtonText: "No, 我点错了!",   
                 closeOnConfirm: false,   
                 closeOnCancel: true 
             }, 
             function(isConfirm){   
                 if (isConfirm) {     
                    $.post("{pigcms{:U('Staffwebsite/pass')}", {"id":id}, function(data){
                      if(data == 1){
                         swal("成功!", "已成功通过审核.", "success"); 
                         location.reload() ;
                     }else{
                    	 swal("错误", "通过此申请异常 :)", "error"); 
                     }
                 })
                 }
             });
            })
            
            
             //关闭
       $("#back").click(function(){
    	   var id = $("#id").val();
           var fail_reason=$("#fail_reason").val();
           if(fail_reason==""){
        	   swal("警告", "不通过理由必须输入 :)", "error"); 
        	   return ;
           }
           if(fail_reason.length > 100){
        	   swal("警告", "不通过理由长度小于100 :)", "error"); 
        	   return ;
           }
         swal({   
            title: "不通过?",   
            text: "您确定要关闭此申请吗!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, 我要关闭!",   
            cancelButtonText: "No, 我点错了!",   
            closeOnConfirm: false,   
            closeOnCancel: true 
        }, 
        function(isConfirm){   
          if (isConfirm) {     
            $.post("{pigcms{:U('Staffwebsite/back')}", {"id":id,"fail_reason":fail_reason}, function(data){
            	if(data == 1){
                    swal("成功!", "已成功关闭申请.", "success"); 
                    location.reload() ;
                }else{
               	     swal("错误", "通过此申请异常 :)", "error"); 
                }
            })
            }
        });
     })
     
     
      $("#over").click(function(){
          	 var id = $("#id").val();
           	 swal({   
                 title: "结束?",   
                 text: "您确定要结束吗!",   
                 type: "warning",   
                 showCancelButton: true,   
                 confirmButtonColor: "#DD6B55",   
                 confirmButtonText: "Yes, 我要结束!",   
                 cancelButtonText: "No, 我点错了!",   
                 closeOnConfirm: false,   
                 closeOnCancel: true 
             }, 
             function(isConfirm){   
                 if (isConfirm) {     
                    $.post("{pigcms{:U('Staffwebsite/over')}", {"id":id}, function(data){
                      if(data == 1){
                         swal("成功!", "已成功结束.", "success"); 
                         location.reload() ;
                     }else{
                    	 swal("错误", "服务器异常,请联系管理员 :)", "error"); 
                     }
                 })
                 }
             });
            })
            
   </script>
   
   
   
	<script>
		var href = location.href.match(/\/\/.+?\/(.+)/)[1];
		var n = href;
		if (href.indexOf("account/adddomains") < 0) {
			try { n = href.match(/(.+?\/.+?)\//i)[1]; }
			catch (e) { };
		}
		switch (n) {
			case "account/Loglog":
				n = "account/bind";
				break;
			case "account/updatepage":
				n = "account/sellpage";
				break;
			case "account/addpage":
				n = "account/sellpage";
				break;
			case "account/feedback":
				n = "account/list";
				break;
		}
		var sld_a = $(".process1 li a[href*='" + n + "']");
		if (sld_a.length == 0) {
			$(".process1:first").css("display", "block");
		} else {
			sld_a.addClass("t");
			if (n == 'account/adddomains')
				$(".process1 li a[href*='account/adddomains/pl']").removeClass('t');
			var sld_h3 = sld_a.parent(0).parent(0).parent(0).prev();
			//var pos_y = getPos_y(sld_h3);
			//sld_h3.addClass("active_s").css("background-position", "0px " + (-pos_y - 74) + "px");
			sld_a.parent(0).parent(0).parent(0).css("display", "block");
		}
	</script>
        
        
	<include file="Public:sidebar"/>
</body>
</html>
