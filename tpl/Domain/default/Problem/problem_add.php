<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>我是卖家&nbsp;&gt;&nbsp;</li>
					<li>添加批量交易</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
        <!-- 左边 -->
        <div class="u-fl">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a class="a1 s-bg-2a">
                            <span>提交问题</span>
                            <b></b>
                        </a>
                        <a href="{pigcms{:U('Problem/index')}" class="a2 s-3c">
                            <span>问题列表</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 数据 -->
            <form action="{pigcms{:U('Problem/problem_add')}" enctype="multipart/form-data" method="post">
                <div class="m-question2 s-bg-fc u_be9">
                    <ul>
                        <li class="u_mb15">
                            <div class="question_txt">问题标题：</div>
                            <div class="question_ipt1 s-bg-white">
                                <input type="text" name="title" value="" id="title" maxlength="50" ph="请填写您的问题标题">
                            </div>
                            <div class="u-cls"></div>
                        </li>
                        <li class="u_mb15">
                            <div class="question_txt">详细描述：</div>
                            <div class="question_textaea s-bg-white">
                                <textarea maxlength="500" name="content" id="content" name="txtcontent"></textarea>
                            </div>
                            <div class="u-cls"></div>
                        </li>
                        <li>
                            <div class="u_mall12">
                                <input type="submit" value="立即提交" id="btn_sub" class="u-btn12 s-bg-2a">
                            </div>
                        </li>
                    </ul>
                </div>
            </form>
        </div>
        <!-- 右边 -->

		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<include file="Public:sidebar"/>
</body>
</html>
