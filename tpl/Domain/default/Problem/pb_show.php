<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>我是卖家&nbsp;&gt;&nbsp;</li>
					<li>添加批量交易</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
       <!-- 左边 -->
        <div class="u-fl">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a href="{pigcms{:U('Problem/problem_add')}" class="a1 s-bg-2a">
                            <span>提交问题</span>
                            <b></b>
                        </a>
                        <a href="{pigcms{:U('Problem/index')}" class="a2 s-3c">
                            <span>问题列表</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 查看问题 -->
            <div class="m-look-problem s-bg-fc">
                <h3 class="s-bg-white">查看问题</h3>
                <div class="lookproblems">
                    <div class="ask"><span><strong >您的提问：</strong></span>{pigcms{$pbshow.title}</div>
                    <br/>
                    <div class="ask"><span><strong >提问内容：</strong></span>{pigcms{$pbshow.content}</div>
                    <div class="askimgcon">
                        <div class="askTime">
                            提交时间：<span class="u_pr38">{pigcms{$pbshow.addtime|date='Y-m-d H:i:s',###}</span>
                            处理状态：<span class="s-ff6">
                                <if condition="$pbshow.state == 0">
                                    (未回复)
                                    <else/>
	                                <font color="#ff6d00"> (已回复)</font>
                                </if>
                            </span>
                        </div>
                        <div class="u-cls"></div>
                    </div>
                </div>
            </div>
                <!-- 暂无记录 -->
                <!-- 详细描述 -->
                <if condition="$pbshow.state == 1">
                    <div class="m-question2 s-bg-fc u_be9">
                       <div class="lookproblems">
                            <div class="ask"><span><strong >回复内容：</strong></span>{pigcms{$pbshow.reply}</div>
                            <div class="askimgcon">
                                <div class="askTime">
                                    回复时间：<span class="u_pr38">{pigcms{$pbshow.replytime|date='Y-m-d H:i:s',###}</span>
                                    </span>
                                </div>
                                <div class="u-cls"></div>
                            </div>
                        </div>
                    </div>
                                            <else/>
                                        </if>
                
                    
                
        </div>
        <!-- 右边 -->

		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<include file="Public:sidebar"/>
</body>
</html>
