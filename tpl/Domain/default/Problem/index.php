<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>我的帐户 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	
	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />

	
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li><a href="{pigcms{:U('Account/index')}" class="s-2a">我的账户</a>&nbsp;&gt;&nbsp;</li>
					<li>我是卖家&nbsp;&gt;&nbsp;</li>
					<li>添加批量交易</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
		<include file="Account:sidebar"/>
        <!-- 右边 -->
         <!-- 左边 -->
        <div class="u-fl">
            <!-- 标题&验证 -->
            <div class="s-bg-fc u_be9" style="width: 702px;">
                <div class="m-ta s-bg-white">
                    <div class="title">
                        <a href="{pigcms{:U('Problem/problem_add')}" class="a2 s-3c">
                            <span>提交问题</span>

                        </a>
                        <a href="{pigcms{:U('Problem/index')}" class="a1 s-bg-2a">
                            <span>问题列表</span>
                            <b></b>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
            </div>
            <!-- 数据 -->
            <div class="m-account-data2 s-bg-fc u_be9">
                <table>
                    <tr class="s-bg-white">
                        <th width="16px"></th>
                        <th width="300px" class="u_tl">问题标题</th>
                        <th width="140px" class="u_tl">提问时间</th>
                        <th width="230px" class="u_tr">处理状态</th>
                        <th width="16px"></th>
                    </tr>
                    <volist name="problem_list" id ="vo" >
                    <tr>
                        <td width="16px"></td>
                        <td width="300px" class="u_tl"><a href="{pigcms{:U('Problem/pb_show?id=')}{pigcms{$vo.id}">{pigcms{$vo.title}</a></td>
                        <td width="140px" class="u_tl">{pigcms{$vo.addtime|date='Y-m-d H:i:s',###}</td>
                        <td width="230px" class="u_tr">
                            <if condition="$vo.state eq 0">
                                未回复
                                <else/>
	                            <a href="{pigcms{:U('Problem/pb_show?id=')}{pigcms{$vo.id}"><font color="#ff6d00">已回复</font></a>
                            </if>
                            
                        </td>
                        <td width="16px"></td>
                    </tr>
                    </volist>
                </table>
            </div>
            <div id="paging" class="g-padding g-padding1">{pigcms{$pagebar}</div>
        </div>
        <!-- 右边 -->

		
       <div class="u-cls"></div>
    </div>
</div>
	<include file="Public:footer"/>
	<include file="Public:account_footer"/>
	<include file="Public:sidebar"/>
</body>
</html>
