<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>批量交易 - 域名购买 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/common.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>

					<li>{pigcms{$domain_list.title}</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
	<div class="g-content u-cls">
    <div class="m-content">
        <!-- 左边 -->
        <div class="u-fl">
            <div class="g-a-container">
                <div class="m-ta s-bg-white u_be9">
                    <div class="title">
                        <a class="s-3c" href="{pigcms{:U('Pldomain/index')}">
                            <span class="z-img-t14 icon-pljy" style="position: relative; bottom: -8px;"></span>
                            <span>批量交易</span>
                        </a>
                    </div>
                    <div class="u-cls"></div>
                </div>
                <div class="m-inner1 s-bg-fc u_be9">
                    <h3 class="f-ha u_bb0">
                        <a class="s-2a" target="_blank" title="点击查询Whois信息">{pigcms{$domain_list.title}</a>
                    </h3>
<!--                    <div class="u_instr2">
                        <div class="instr2-l" style="word-break: keep-all;">
                            
                        </div>

                        <div class="u-cls"></div>
                    </div>-->
                    <div class="u_instr2">
                            <div class="instr2-l">
                                    如果您感兴趣家就会收到{pigcms{$config.site_name}的邮件通知并对您的报价作出相应回复。
                            </div>
                            <div class="instr2-r">
<!--                                    <a class="f-img-i6" title="Whois查询" target="_blank" href="/index.php?c=Whois&amp;a=index&amp;keyword=8309.com"></a>-->
<!--                                    <a class="f-img-i1" title="切换大小写" target="_blank" href="javascript:laLock($('.f-ha a.s-2a'))"></a>-->
                                    <a class="f-img-i2" title="百度搜索" target="_blank" href="http://www.baidu.com/s?wd={pigcms{$domain_list.title}"></a>
                            </div>
                            <div class="u-cls"></div>
                    </div>
                    
                    
                    
                </div>
                <div class="m-inner2 s-bg-fc u_be9">
                    <div class="instr3-l s-bg-fea">{pigcms{$domain_list.meaning}</div>

                    <div class="instr3-r1">
                        <p class="u_mt33">
                            <span class="u-txr2" style="width: 120px;">当前价：￥</span>
                            <span class="s-75b u-txr3" id="total_price">{pigcms{$domain_list.total_price|number_format}</span>
                        </p>
                        <p>
                            <span class="u-txr2" style="width: 120px;">平均价：￥</span>
                            <span class="s-75b u-txr3">{pigcms{$domain_list.unit_price|number_format}</span>
                        </p>

                        <p>
                            <input type="hidden" class="domain_bulk_id" value="{pigcms{$domain_list.bulk_id}">
<!--                            <input type="text" class="boon" value="{pigcms{$domain_list.margin_boon}">-->
                            <input type="hidden" class="margin" value="{pigcms{$domain_list.margin_boon|number_format}"/>
                            <input type="button" value="立即购买" class="s-bg-6c u-btn7b" style="margin-left:126px;" id="buy">
                        </p>
                        <input type="hidden" value="￥6,800" id="bzj">
                        <input type="hidden" value="115" id="domainid">
                    </div>
                    <div class="u-cls"></div>
                </div>
                <!-- 增加部分 -->
                <div class="pl-domain-table-wrap">
                    <table class="pl-domain-table">
                        <thead>
                            <tr>
                                <th>域名</th>
                                <th>原标价</th>
                            </tr>
                        </thead>
                        <tbody>
                        <volist name="domain_list['domain_list']" id="vo">
                            <tr>
                                <td>{pigcms{$vo}</td>
                                <td>买家报价</td>
                            </tr>
                        </volist>
                        </tbody>
                    </table>
                    <div class="fy">
                        <input type="hidden" value="115" id="hidid">
                        <div id="paging" class="g-padding g-padding1">
                            <div class="u-fl"><span class="u_fw">3</span>条记录，<span class="u_fw">1</span>页</div><div class="u-fr"><a class="pa_disabled" title="上一页" href="javascript:void(0)"></a><a href="javascript:void(0)" class="s-def t">1</a><a class="pg_enabled" title="下一页" href="javascript:void(0)"></a></div><div class="u-cls"></div>
                        </div>
                    </div>
                </div>
                <style>
                    .pl-domain-table-wrap {
                        margin-top: 6px;
                        background-color: #FFF;
                    }

                        .pl-domain-table-wrap .fy {
                            border: 1px solid #e9e9e9;
                            border-top: none;
                        }

                    .pl-domain-table {
                        width: 100%;
                        border: 1px solid #e9e9e9;
                        border-collapse: collapse;
                        border-spacing: 0;
                    }

                        .pl-domain-table td, .pl-domain-table th {
                            padding: 12px 3px;
                            border: 1px solid #e9e9e9;
                            text-align: center;
                        }

                        .pl-domain-table thead tr {
                            background-color: #2AA3CE;
                            color: #FFF;
                        }

                        .pl-domain-table tbody tr:hover {
                            background-color: #f0f1f1;
                        }
                </style>
                <!-- 增加部分结束 -->
            </div>

        </div>
        <!-- 右边 -->
        <div class="u-fr">
			<div class="g-c-container">
				<div class="m-processList u_mt1 u_mb6">
					<h3 class="cli">我要购买</h3>
					<div class="process" style="display: block;">
						<ul>
								<li>
									<span class="f-img-s3">1</span><span class="u-txh">点击购买</span>
								</li>
								<li>
									<span class="f-img-s3">2</span><span class="u-txh">支付定金</span>
								</li>
								<li>
									<span class="f-img-s3">3</span><span class="u-txh">发起交易</span>
								</li>
								<li>
									<span class="f-img-s3">4</span><span class="u-txh">支付全款</span>
								</li>
								<li>
									<span class="f-img-s3">5</span><span class="u-txh">域名过户</span>
								</li>
								<li class="u_mb0">
									<span class="f-img-s3">6</span><span class="u-txh">交易完成</span>
								</li>


						</ul>
					</div>
				</div>
			</div>

		</div>

        <div class="u-cls"></div>
    </div>
</div>
	<div class="s-bg-f7"></div>
	<include file="Public:footer"/>
	<include file="Public:sidebar"/>

	<script type="text/javascript" src="{pigcms{$static_path}js/jquery.color.js"></script>
	<script type="text/javascript" src="{pigcms{$static_path}js/jquery-ui.js"></script>
	<script type="text/javascript" src="{pigcms{$static_path}js/jquery.cxscroll.min.js"></script>
	<script type="text/javascript" src="{pigcms{$static_path}js/pldomain.js"></script>
        
<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">     
        
        
        
<script>
    
    $("#buy").click(function(){
        var bulk_id = $(".domain_bulk_id").val();
        var margin = $(".margin").val();
        swal({   
            title: "确定要购买吗?", 
            text: "点击购买将会从您账户中扣取 ￥"+margin+" 元 作为保证金" ,
            type: "warning",   showCancelButton: true, 
            confirmButtonColor: "#DD6B55", 
            confirmButtonText: "Yes,我要购买!",  
            cancelButtonText: "No, 我点错了!", 
            closeOnConfirm: false, 
            closeOnCancel: true 
        }, 
        function(isConfirm){  
            if (isConfirm) {  
                $.post("{pigcms{:U('Pldomain/buy_margin')}",{'bulk_id':bulk_id},function(data){
//                    alert(data);
                    if(data == 1){
                        swal("成功!", "保证金支付成功.", "success"); 
                        location.href = "./index.php?c=Account&a=index";
                    }else if(data == 3){
                        swal("失败", "此域名正在出售中 :)", "error");
                        location.href = "./index.php?c=Pldomain&a=index";
                    }else if(data == 4){
                        swal("失败", "您的资金不足 :)", "error");  
                    }else if(data == 5){
                        swal("失败", "您不可以购买自己的域名 :)", "error");  
                    }else{
                        swal("异常", "此次交易异常，请刷新重试 :)", "error");  
                    }
                })
            }
        });
    })
    
    
    

</script>
        
        
        
	<script type="text/javascript">
		$("#animate3").cxScroll({ direction: "bottom", step: 2, speed: 800, time: 2000 });
	</script>
</body>
</html>