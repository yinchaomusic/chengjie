<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>批量交易 - 域名购买 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/common.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bragin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
	<div class="s-bg-f7">
		<div class="g-position">
			<div class="m-position u-fl">
				<ol>
					<li>当前位置：&nbsp;</li>
					<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
					<li>批量交易</li>
				</ol>
			</div>
			<div class="m-serverTime u-fr">
				服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
			</div>
			<div class="u-cls"></div>
		</div>
	</div>
<div class="g-content u-cls">
	<div class="m-content">
		<div class="m-relsult2">
			<!-- 修改2015 -->
			<div class="m-pl-left">
				<div class="m-ta m-title g-a-container s-bg-white">
					<div class="title">
						<a class="s-3c" href="{pigcms{:U('Pldomain/index')}">
							<span class="z-img-t14 icon-pljy"></span>
							<span>批量交易</span>
						</a>
					</div>
				</div>
				<div class="m-intro g-a-container s-bg-white">
					<p>批量交易是卖家将一定数量的热门域名打包在一起出售。请点击立即购买按钮查看卖家的介绍以及域名列表。确认购买发起交易时需冻结一定幅度的保证金，用于确保您是真实购买。</p>
				</div>
				<div class="g-a-container u_df1 s-bg-white" style="position: relative;">
					<ul class='u-udata-a' id='bidding'>
                                            <volist name="domain_list" id="vo">
                                                <li>
							<a href="{pigcms{:U('Pldomain/selling?bulk_id=')}{pigcms{$vo.bulk_id}" target="_blank">
								<h4 class="f-p-t1 s-bg-fb" title="{pigcms{$vo.title}。">{pigcms{$vo.title}</h4>
								<div class="m-data-DomainList">
									<p class="f-p6" title="{pigcms{$vo.amount}个域名">{pigcms{$vo.amount|number_format}个域名</p>
									<p class="f-p7">
										<span>价格：</span>
										<span class="s-75b money" title="￥{pigcms{$vo.total_price}">￥{pigcms{$vo.total_price|number_format}</span>
									</p>
								</div>
							</a>
						</li>
                                            </volist>
						
					</ul>                
                                </div>
			</div>
			<!-- 左侧结束 -->
			<div class="g-b-container">
				<div class="m-bb-container">
					<div class="m-title-b s-bg-55b">
						<span class="recommend">已售域名组</span>
					</div>
					<div class="m-apply-dg m-apply-dga" style="padding-top: 0; overflow: hidden;">
						<div class="m-animate3" id="animate3" style="margin-top: -1px;">
							<a class="next"></a><a class="prev"></a>
							<div class="animate3 box">
								<ul class="list">

                                                                    <volist id="vo" name="prosperity_list">
                                                                        <li>
										<div class="spanl"><a href="javascript:void(0)" rel="nofollow">{pigcms{$vo.title}</a></div>
										<div class="spanr">￥{pigcms{$vo.total_price|number_format}</div>
										<div class="u-cls"></div>
									</li>
                                                                    </volist>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="u-cls"></div>
		</div>
		<div id="paging" class="g-padding">
			<div class='u-fl'>{pigcms{$pagebar}</div>
		</div>
	</div>
	<div class="u-cls"></div>
</div>
<div class="s-bg-f7"></div>
<include file="Public:footer"/>
<include file="Public:sidebar"/>

<script type="text/javascript" src="{pigcms{$static_path}js/jquery.color.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/jquery-ui.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/jquery.cxscroll.min.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/pldomain.js"></script>
<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">



<script type="text/javascript">
	$("#animate3").cxScroll({ direction: "bottom", step: 2, speed: 800, time: 2000 });
</script>
</body>
</html>