$(function(){
    $('#switch_btn a').click(function(){
        $(this).addClass('on').siblings('a').removeClass('on');
        $('#'+$(this).attr('types')+'_form').show().siblings('form').hide();
    });
    $('#first_step').submit(function(){
        notice('发送邮件中~~~','loading');
        if($('#email').val().length < 1 || !/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/.test($('#email').val())){
            swal('','请输入有效的邮箱~','error');
            $('#email').focus();
        }
        if($('#email').val()=='' || $('#email').val()==$('#email').attr('ph')){
            swal('','请输入正确的邮箱~','error');
            $('#email').focus();
            return false;
        }else if($('#findpass_verify').val()==''){
            alert($('#findpass_verify').val());
            swal('','请输入验证码~','error');
            $('#findpass_verify').focus();
        }else{
            $.post(first_check,$("#first_step").serialize(),function(result){
                result = $.parseJSON(result);
                if(result){
                    if(result.error == 0){
                        swal('','验证码发送成功，请及时校验~','success');
                        setTimeout(function(){
                            window.parent.location = secondstep_url;
                        },1000);
                    }else{
                        $('#findpass_verify').focus();
                        swal({   title: '出现错误啦。' ,   text: result.msg ,type:"error",   timer: 3000,   showConfirmButton: true });
                    }
                }else{
                    swal('','登录出现异常，请重试！','error');
                }
            });
        }
        return false;
    });
    $('#seconde_check').submit(function(){

       if($('#find_pass_code').val().length != 6){
            swal("",'请输入  6 个字符的验证码~',"error");
            $('#find_pass_code').focus();
        }else if($('#find_pass_code').val() == ''){
            swal('','验证码不可以为空','error');
            $('#find_pass_code').focus();
        }else{
            $.post(seconde_check_post,$("#seconde_check").serialize(),function(result){

                result = $.parseJSON(result);
                if(result){
                    if(result.error == 0){
                        swal({   title: result.title ,   text: result.msg  ,type:"success",  timer: 2000,   showConfirmButton: false });
                        setTimeout(function(){
                            window.parent.location = thirdstep_url;
                        },1000);
                    }else{
                        $('#find_pass_code').focus();
                        swal({   title: '校验出现错误' ,   text: result.msg ,type:"error",   timer: 3000,   showConfirmButton: true });
                    }
                }else{
                    swal('','校验出现异常，请重试！','error');
                }
            });
        }
        return false;
    });

    $('#third_check').submit(function(){

        //先校验两次密码是否一

        if(($('#txtnewpwd').val().length > 30) ||($('#repeatpwd').val().length > 30) ){
            swal("",'密码设置过长了。',"error");
            $('#txtnewpwd').focus();return false;
        }else if(($('#txtnewpwd').val() == '') || ($('#repeatpwd').val() == '')){
            swal('','密码不可以为空','error');
            $('#txtnewpwd').focus();return false;
        }else if( $('#txtnewpwd').val() != $('#repeatpwd').val() ){
            swal('','两次密码不一致！','error');return false;
        }
        else{
            $.post(third_check_post,$("#third_check").serialize(),function(result){

                result = $.parseJSON(result);
                if(result){
                    if(result.error == 0){
                        swal({   title: result.title ,   text: result.msg  ,type:"success",  timer: 2000,   showConfirmButton: false });
                        setTimeout(function(){
                            window.parent.location = fourthstep_url;
                        },1000);
                    }else{
                        $('#find_pass_code').focus();
                        swal({   title: '重置密码出现错误' ,   text: result.msg ,type:"error",   timer: 3000,   showConfirmButton: true });
                    }
                }else{
                    swal('','重置米出现异常，请重试！','error');
                }
            });
        }
        return false;
    });

});
function login_fleshVerify(url){
    var time = new Date().getTime();
    $('#login_verifyImg').attr('src',url+"&time="+time);
}
function reg_fleshVerify(url){
    var time = new Date().getTime();
    $('#findpass_verifyImg').attr('src',url+"&time="+time);
}
var notice_timer = null;
function notice(msg,pic){
    if($(window).height() > $('body').height()){
        if(notice_timer) clearTimeout(notice_timer);
        $('.notice').remove();
        $('body').append('<div class="notice"><img src="'+static_path+'login/img/'+pic+'.gif" />'+msg+'</div>');
        notice_timer = setTimeout(function(){
            $('.notice').remove();
        },5000);
    }else{
        if(pic != 'loading'){
            alert(msg);
        }
    }
}