$("#m-style").css("border-bottom","0");
$(".m-keyword-a").removeClass("u_bb0");
showorhide();
// --------展开收起------------
$("#m-up-down .down,#m-up-down .up").click(function(){
       if($(this).hasClass("up")){
            $("#g_sh").slideUp(300);
            $(".m-keyword-a").removeClass("u_bb0");
            $("#m-up-down .down").removeClass("s-bg-white").addClass("s-bg-fb");
            $("#m-up-down .up").hide();
            $("#m-up-down .down").show();
       }else{
            $(".m-keyword-a").addClass("u_bb0");
            $("#m-up-down .down").removeClass("s-bg-fb").addClass("s-bg-white");
            $("#g_sh").slideDown(300);
            $("#m-up-down .down").hide();
            $("#m-up-down .up").show();
            $("html,body").animate({ scrollTop: 148 }, 250);
       }
});
// --------------------------------------------
var input = $(".f-range-sure input");

$(".u_b4 input").keyup(function () {
    if (event.keyCode == 13) {
        $(".u-btn1").click();
    }
})

$("#domian-len").slider({animate:true,range:true,min:1,max:9,step:1,values:[1,9],stop:function(event,ui){
    ui.values[0]>8?$(input[0]).val(""):$(input[0]).val(ui.values[0]);
    ui.values[1]>8?$(input[1]).val(""):$(input[1]).val(ui.values[1]);
    slideCon($(this),$(input[0]).val(),$(input[1]).val());
}});
$("#domian-pri").slider({animate:true,range:true,min:1,max:9,step:1,values:[0,9],stop:function(event,ui){
    ui.values[0]>8?$(input[3]).val(""):$(input[3]).val(zhuan(ui.values[0]));
    ui.values[1]>8?$(input[4]).val(""):$(input[4]).val(zhuan(ui.values[1]));
    slideCon($(this),$(input[3]).val(),$(input[4]).val());
}});
$(input[2]).click(function(){
    var name=$(this).parent().parent().prev().text();
    var str=$(this).parent().prev().find("li").eq(8).text();
    val1=Number($(input[0]).val());
    val2=Number($(input[1]).val());
    if(val2==""){
        addConditions("len",name,1,str,true);
    }
    if(val2<val1){
        addConditions("len",name,val1,str,true);
    }else{
        addConditions("len",name,val1,val2,true);
    }
    getData();//g
});
$(input[5]).click(function(){
    var name=$(this).parent().parent().find("div").eq(0).text();
    var str=$(this).parent().prev().find("li").eq(8).text();
    val3=Number($(input[3]).val());
    val4=Number($(input[4]).val());
    if(val4==""){
        addConditions("pri",name,0,str,true);
    }
    if(val4<val3){
        addConditions("pri",name,val3,str,true);
    }else{
        addConditions("pri",name,val3,val4,true);
    }
    getData();//g
});
//滑块的条件
function slideCon(obj,val,val1){
    var str=$(obj).next().find("li").eq(8).text();
    val=val==""?val=$(obj).next().find("li").eq(0).text():val;
    val1=val1==""?val1=str:val1;
    var str2=$(obj).attr("name"),name=$(obj).parent(0).parent(0).prev().text();
    addConditions(str2,name,val,val1,true);
    getData();
}
//转换
function zhuan(str){
    switch(str){
        case 1:
            str=0;
            break;
        case 2:
            str=500;
            break;
        case 3:
            str=1000;
            break;
        case 4:
            str=20000;
            break;
        case 5:
            str=50000;
            break;
        case 6:
            str=100000;
            break;
        case 7:
            str=200000;
            break;
        case 8:
            str=500000;
            break;  
    }
    return str;
}
//---------------------------------------------搜索条件-------------------------------
//后缀
var arr_hz=$("#m-hz span");
    $(arr_hz[0]).click(function(){
        var name=$(this).parent().parent().attr("name");
        $(arr_hz).removeClass("z-cell");
        $(this).addClass("z-cell");
        reduceConditions(name);
        getData(); //g
    });
    $(arr_hz.slice(1,arr_hz.length)).click(function(){
        $(arr_hz[0]).removeClass("z-cell");
         var name=$(this).parent().parent().attr("name"),idx=$(this).attr("idx"),text=$(this).text(),tname=$(this).parent().prev().text();
        if($(this).hasClass("z-cell")){
            $(this).removeClass("z-cell");
            checkstatus("#m-hz");
            reduceConditions(name,idx);
        }else{
            $(this).addClass("z-cell");
            addConditions(name,tname,text,idx);
        }
        getData();//g
    });
//类型
var arr_style=$("#m-style span"),odiv=$("#data_style div"),_spans=$("#data_style span");
    $(arr_style[0]).click(function(){
        $("#m-style").css("border-bottom","0");
        $(arr_style).removeClass("active");
        $(this).addClass("z-cell");
        $(odiv).css("display", "none");
        $("#selConditions").attr("tag", "");
        showorhide();
        getData();
    });
    $(arr_style.slice(1,arr_style.length)).click(function(){
        $("#m-style").css("border-bottom","1px solid #f2f2f2");
        var index = $(this).index();
        $("#selConditions").attr("tag", "");
        $("#selConditions").attr("tag", $(odiv[index - 1]).find("span").eq(0).attr("idx"));
        $(_spans).removeClass("z-cell");
        $(arr_style).removeClass("z-cell").removeClass("active");
        if($(this).hasClass("active")){
            return;
        }else{
            $(this).addClass("active");
            var name=$(this).parent().parent().parent().attr("name"),str=$(this).parent().parent().prev().text();
            reduceConditions(name);
        }
        $(odiv).css("display","none");
        $(odiv[index - 1]).css("display", "block").addClass("s-bg-f9").find("span").eq(0).addClass("z-cell");
        getData();//g
    });
    $(_spans).click(function(){
        var text=$(this).text(),name=$(this).parent().parent().attr("name"),tname=$(this).parent().parent().parent().prev().text(),idx=$(this).attr("idx");
          addConditions(name,tname,text,idx);
        var tag=$(this).attr("tag");
        if(tag==undefined){
            $(this).parent().find("span").eq(0).removeClass("z-cell");
            if($(this).hasClass("z-cell")){
                $(this).removeClass("z-cell");
                checkstatus($(this).parent());
                reduceConditions(name,idx);
            }else{
                $(this).addClass("z-cell");
            }
        }else{
            $(_spans).removeClass("z-cell");
            $(this).addClass("z-cell");
            $("#selConditions").attr("tag", "");
            $("#selConditions").attr("tag", $(this).attr("idx"));
            reduceConditions(name);
        }
        getData();//g
    });
    //主题属性
    var arr_opers=$("#theme_oper span");
    $(arr_opers[0]).click(function(){
        var name=$(this).parent().parent().attr("name");
        changeStyleTi(arr_opers,this,"z-cell");
        reduceConditions(name);
        showorhide();
        getData();//g
    });
    $(arr_opers.slice(1,arr_opers.length)).click(function(){
        var name=$(this).parent().parent().attr("name");
       if($(this).hasClass("z-cell")){
            $(this).removeClass("z-cell");
            $(arr_opers[0]).addClass("z-cell");
            reduceConditions(name);
       }else{
            changeStyleTi(arr_opers,this,"z-cell");
            var name=$(this).parent().parent().attr("name"),tname=$(this).parent().prev().text(),text=$(this).text(),idx=$(this).attr("idx");
            addConditions(name,tname,text,idx);
       }
       getData();//g
    });
    $("#selConditions a").live("click",function(){
        var name=$(this).attr("name"),idx=$(this).attr("idx");
        $(this).remove();
        $("#g_sh").find("div[name='"+name+"']").find("span[idx='"+idx+"']").removeClass("z-cell");
        if(name=="len"){
            $("#domian-len").slider({animate:true,range:true,min:1,max:9,step:1,values:[1,9]});
            $(input[0]).val("1"); $(input[1]).val("");
        }
        if(name=="pri"){
            $("#domian-pri").slider({animate:true   ,range:true,min:1,max:9,step:1,values:[1,9]});
            $(input[3]).val("0"); $(input[4]).val("");
        }
        if(name=="ty"){
            var obj=$("#g_sh").find("div[name='"+name+"']").find("span[idx='"+idx+"']").parent();
            checkstatus(obj);
        }else{
            checkstatus($("#g_sh").find("div[name='"+name+"']"));
        }
        getData();
    });
    // 排序
    $("#sort span").click(function(){
        $("#sort span").removeClass("z-active");
        $(this).addClass("z-active");
        if($(this).hasClass("z-up")){
            $(this).removeClass("z-up").addClass("z-down").attr("idx", (Number($(this).attr("idx")) + 1).toString());
            getData();
        }else if($(this).hasClass("z-down")){
            $(this).removeClass("z-down").addClass("z-up").attr("idx", (Number($(this).attr("idx")) - 1).toString());
            getData();
        }
    });
    $(".u-btn1").click(function(){
        getData();
    });
//------------------------------------------------------------------------------------
//添加选择条件
function addConditions(){
    if(arguments[0]=="oper"){
         $("#selConditions").find("a[name='"+arguments[0]+"']").remove();
    }
    var str="";
    if(arguments.length>4){
        reduceConditions(arguments[0],arguments[4]);
        str="<a name='"+arguments[0]+"' idx='"+arguments[4]+"' class='s-def'>"+arguments[1]+"<span class='s-2a'>"+arguments[2]+" - </span>"+"<span class='s-2a'>"+arguments[3]+"</span><b></b></a>"; 
        $("#selConditions").append(str);
    }else{
        str="<a name='"+arguments[0]+"' idx='"+arguments[3]+"' class='s-def'>"+arguments[1]+"<span class='s-2a'>"+arguments[2]+"</span><b></b></a>"; 
        $("#selConditions").append(str);
    }
    showorhide();
    //getData();
}
// 删除选择条件
function reduceConditions(){
    if(arguments.length<2){
        $("#selConditions").find("a[name='"+arguments[0]+"']").remove();
    }else{
        $("#selConditions").find("a[name='"+arguments[0]+"'][idx='"+arguments[1]+"']").remove();
    }
    showorhide();
    //getData();
}
//
function showorhide(){
    if($("#selConditions a").length==0){
        $("#selConditions").parent().css("display","none").prev().removeClass("u_bb0");
    }else{
        $("#selConditions").parent().css("display","block").prev().addClass("u_bb0");
    }
}
//检查状态
function checkstatus(){
    if($(arguments[0]).find("span[class='z-cell']").length==0){
        $(arguments[0]).find("span").eq(0).addClass("z-cell");
    };
    showorhide();
}
function changeStyleTi(){
    $(arguments[0]).removeClass(arguments[2]);
    $(arguments[1]).addClass(arguments[2]);
}
// 获取条件
function getValue(){
    var canshu=[];
    // 域名关键字
    $(".u-ipt-b input").each(function(){
        if($(this).val()==$(this).attr("ph")){
            canshu.push("");
        }else{
            canshu.push($(this).val().replace(/\s*/g, ""));
        }
    });
    //关系0：包含1：开始2：结尾
    canshu.push($('#keyworda input[type="radio"]:checked').attr("nu"));
    canshu.push($('#keywordb input[type="radio"]:checked').attr("nu"));
    // 范围依次读取（成对）
    $(".m-leng input[type='text']").each(function(i,n){
        canshu.push($(n).val());
    });
    // 后缀，类型,属性
    var hz="",ty="",po="";
    $("#selConditions a").each(function(i,n){
        var name=$(n).attr("name");
        switch(name){
            case "hz":
                hz+=$(n).attr("idx")+",";
                break;
            case "ty":
                ty+=$(n).attr("idx")+",";
                break;
            case "oper":
                po+=$(n).attr("idx")+",";
                break;
        }
    });
    var $flag = parseInt(hz.indexOf("other"));
    if ($flag != -1) {
        var allhz = ""
        $("#m-hz .f-hz span[idx]").each(function (i, n) {
            allhz += $(n).attr("idx") + ",";
        });
        canshu.push(allhz.substring(0, allhz.length - 1));
    } else {
        canshu.push(hz.substring(0, hz.length - 1));
    }
    if (ty == "") {
        ty = $("#selConditions").attr("tag");
        canshu.push(ty);
    } else {
        canshu.push(ty.substring(0, ty.length - 1));
    }
    canshu.push(po.substring(0,po.length-1));
    //顺序
    
    $("#sort span").each(function(){
        if($(this).hasClass("z-active")){
            canshu.push($(this).attr("idx"));
        }
    });
    //页码
    var pageIndex=1;
    if(arguments[0] != undefined){
        canshu.push(arguments[0]);
    }else{
        canshu.push(pageIndex);
    }
    //标识
    canshu.push($(".button").attr("k"));
    return canshu;
}
var biaoshi=$(".button").attr("k");
function getData(idx) {
    //loading
    showLodingImg("0", "50%", "750px", "-150px");
    var canshu = getValue(idx);
    $.post("./index.php?c=Buydomains&a=pagedomins", {
        "input_val_one": canshu[0],
        "input_val_two": canshu[1],
        "select_one": canshu[2],
        "select_two": canshu[3],
        "ym_length_one": canshu[4],
        "ym_length_two": canshu[5],
        "earge_parse_one": canshu[6],
        "earge_parse_two": canshu[7],
        "hznum": canshu[8],
        "style_ym": canshu[9],
        "oper": canshu[10],
        "order": canshu[11],
        "pageIndex": canshu[12],
        "type":canshu[13],
        "userid": $("#uid").val()
        }, function (result) {
            if (result.status == 1) {
                var json = result.info;
                if (biaoshi == "0") { //议价
                    //清空行
                    var tb = $(".g-a-container table");
                    $(".g-a-container table tr:not(:first)").remove();

                    $.each(json.rows, function (row, value) {
                        var r ="<tr><td></td>"
                            + "<td>" + (++row) + "</td>"
                            + "<td>" + getIco(value.type) + "</td>"
                            + "<td class='u_tl'><a title='" + value.domain + "' href='" + getUrl(value) + "' target='_blank' class='u-txa s-def'>" + value.domain + "</a></td>"
                            + "<td>" + value.label + "</td>"
                            + "<td>" + value.cls + "</td>"
                            + "<td class='u_tr'>" + (value.money == "0" ? lang.a89 : formatMoney(value.money, 0, "￥")) + "</td>"
                            + "<td class='u_tr'><a href='" + getUrl(value) + "' target='_blank' class='u-abtn-xq s-def'>" + (value.type == -1 ? lang.a135 : lang.a83) + "</a></td>"
                            + "<td></td></tr>"
                        tb.append(r);
                    });
                }else if (biaoshi == "4") {//米表
                    //清空行
                    var tb = $(".g-a-container table");
                    $(".g-a-container table tr:not(:first)").remove();

                    $.each(json.rows, function (row, value) {
                        var r = "<tr><td></td>"
                              + "<td>" + (++row) + "</td>"
                              + "<td>" + getIco(value.type) + "</td>"
                              + "<td class='u_tl'><a title='" + value.domain + "' href='" + getUrl(value) + "' target='_blank' class='u-txa s-def'>" + value.domain + "</a></td>"
                              + "<td>" + value.label + "</td>"
                              + "<td>" + value.cls + "</td>"
                              + "<td class='u_tr'>" + (value.money == "0" ? lang.a89 : formatMoney(value.money, 0, "￥")) + "</td>"
                              + "<td></td>"
                              + "<td class='u_tl'><span class='u-span-mibiao'>"+value.desc+"</span></td>"
                              + "<td class='u_tr'><a href='" + getUrl(value) + "' target='_blank' class='u-abtn-xq s-def'>" + (value.type == 0 ? lang.a135 : lang.a83) + "</a></td>"
                              + "<td></td></tr>"
                        tb.append(r);
                    });
                }else if (biaoshi == "2") { //拍卖
                    //清空行
                    var tb = $(".g-a-container table");
                    $(".g-a-container table tr:not(:first)").remove();

                    $.each(json.rows, function (row, value) {
                        var r ="<tr><td></td>"
                            + "<td>" + (++row) + "</td>"
                            + "<td>" + getIco(value.type) + "</td>"
                            + "<td class='u_tl'><a title='" + value.domain + "' href='" + getUrl(value) + "' target='_blank' class='u-txa s-def'>" + value.domain + "</a></td>"
                            + "<td>" + value.label + "</td>"
                            + "<td>" + value.cls + "</td>"
                            + "<td class='u_tr'>" + (value.money == "0" ? lang.a89 : formatMoney(value.money, 0, "￥")) + "</td>"
                            + "<td>" + (value.over_time) + "</td>"
                            + "<td class='u_tr'><a href='" + getUrl(value) + "' target='_blank' class='u-abtn-xq s-def'>" + (value.type == -1 ? lang.a135 : lang.a83) + "</a></td>"
                            + "<td></td></tr>"
                        tb.append(r);
                    });
                }else {// 1优质  2一口价
                    var url = biaoshi == "3" ? "./index.php?c=Bargain&a=selling&id=" : "./index.php?c=Hotsale&a=selling&id=";
                    var tb = $(".u-udata-a");
                    $(".u-udata-a li").remove();
                    var result = "";
                    $.each(json.rows, function (row, value) {
                        result += "<li><a href='" + (url + value.domain_id) + "' target='_blank'>"
                                + "<h4 class='f-p-t1 s-bg-fb' title='" + value.domain + "'>" + value.domain + "</h4>"
                                + "<div class='m-data-DomainList'>"
                                + "<p class='f-p6' title='" + value.desc + "'>" + value.desc + "</p>"
                                + "<p class='f-p7'><span>" + lang.a124 + "：</span><span class='s-75b money' title='" + (value.money == 0 ? lang.a89 : formatMoney(value.money, 0, "￥")) + "'>" + (value.money == 0 ? lang.a89 : formatMoney(value.money, 0, "￥")) + "</span><span class='" + (value.is_hot == '1'  ? "z-dh" : "") + " f' title='火'></span></p></div></a></li>";           
                    });
                    if (json.rows < 1)
                        result += "<p class='no-record'>" + lang.a77 + "</p>";
                    tb.html(result);
                }
                $("#paging").html(nextPage(json.paging[0].pageIndex, json.paging[0].total, json.paging[0].pageSize, 3));
                hideLodingImg();
            } else {
                hideLodingImg();
            }
    })
}

$(function () {
    //推荐 换一批
    $(".refresh").click(function () {
        showLodingImg("0", "50%",$(this).offset().top+65+"px", "350px");
        var id = "";
        $(".tj_domain a").each(function () {
            id += $(this).attr("k") + ","
        })
        $.post("./index.php?c=Buydomains&a=recom", { "type": biaoshi, "id": id }, function (result) {
            if (result.status == 1) {
                var da = result.info;
                var tb = $(".tj_domain");
                $(".tj_domain tr").remove();
                for (i in da) {
					var url = './index.php?c=Buydomains&a=selling&id=';
					switch(biaoshi){
						case "1":
							url = '';
							break;
						case "2":
							url = '';
							break;
						case "3":
							url = './index.php?c=Bargain&a=selling&id=';
							break;
					}
                    var r = "<tr><td width='16px'></td>"
                        + "<td width='135px'><div class='u-ina'><a target='_blank' k='" + da[i].domain_id + "' href='" + url + da[i].domain_id + "' title='" + da[i].domain + "' class='u-txb s-2a'>" + da[i].domain + "</a><span class='u-txc' title='" + da[i].desc + "'>" + da[i].desc + "</span></div></td>"
                        + "<td width='105px'><span class='s-75b u-txd'>" + (da[i].money == 0 ? lang.a89 : formatMoney(da[i].money, 0, "￥")) + "</span></td>"
                        + "<td width='16px'></td></tr>"
                    tb.append(r);
                };
                hideLodingImg();
            } else {
                hideLodingImg();
            }
        })
    })
    $(".tj_domain tr").live("mouseover", function () {
        $(this).css("cursor", "pointer")
    }).live("click", function () {
        window.open($(this).find(".u-ina a").attr("href"));
        return false;
    })


    $("#paging a").live("click",function () {
        getData($(this).attr("k"));
        $(document.body).scrollTop($(".m-relsult2").position().top-45);
    })

    $(".keyword,.txt").keyup(function () {
        if (event.keyCode == 13) {
            $(".button").click();
        }
    })
})

function getIco(type) {
    var result="<span class='block fl'></span>";
    switch(type){
        case '2':
            result = "<span class='z-img-b4' title='" + lang.a88 + "'></span>";
            break;
        case '1':
            result = "<span class='z-img-b1' title='" + lang.a87 + "'></span>";
            break;
        case '3':
            result = "<span class='z-img-b3' title='" + lang.a81 + "'></span>";
            break;
    }
    return result;
}

