﻿$("#paging a").live("click", function () {
    getData($(this).attr("k"));
    $(document.body).scrollTop($(".m-relsult2").position().top - 45);
})
function getData(idx) {
    if (!idx || idx == 0) {
        return;
    }
    //loading
    showLodingImg("0", "50%", "750px", "-150px");
    $.post("/pldomainInf", {
        "pageindex": idx
    }, function (res, status) {
        if (status == "success") {
            var json = $.parseJSON(res);
            var url = "/pldomain/selling/";
            var tb = $(".u-udata-a");
            $(".u-udata-a li").remove();
            var result = "";
            $.each(json.rows, function (row, value) {
                result += "<li><a href='" + (url + value.id) + "' target='_blank'>"
                        + "<h4 class='f-p-t1 s-bg-fb' title='" + value.title + "'>" + value.title + "</h4>"
                        + "<div class='m-data-DomainList'>"
                        + "<p class='f-p6' title='" + value.count + "个域名'>" + value.count + "个域名</p>"
                        + "<p class='f-p7'><span>" + lang.a124 + "：</span><span class='s-75b money' title='" + (value._money == "0" ? lang.a89 : formatMoney(value._money, 0, "￥")) + "'>" + (value._money == "0" ? lang.a89 : formatMoney(value._money, 0, "￥")) + "</span><span class=' f' title='火'></span></p></div></a></li>";
                if (json.rows < 1)
                    result += "<p class='no-record'>" + lang.a77 + "</p>";
                tb.html(result);
            });
            $("#paging").html(nextPage(json.paging[0].pageIndex, json.paging[0].total, json.paging[0].pageSize, 3));
            hideLodingImg();
        } else {
            hideLodingImg();
        }
    })
}