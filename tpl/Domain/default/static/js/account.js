$(function () {
    //------------------------------------------------ menu ------------------------------------------------------
    $(".m-processList1 h3").click(function () {
        var ul = $(this).next();
        if (ul.css("display") == "none") {
            $(".process1").slideUp("fast");
            ul.slideDown("fast");
        } else {
            ul.slideUp("fast");
        }
    }).mouseover(function () {
        var pos_y = getPos_y($(this));
        if (!$(this).hasClass("active"))
            $(this).addClass("active").css("background-position", "0px " + (-pos_y - 74) + "px");
    }).mouseout(function () {
        var pos_y = getPos_y($(this));
        if ($(this).hasClass("active"))
            $(this).removeClass("active").css("background-position", "0px " + (-pos_y + 74) + "px");
    })
    //------------------------------------------------ menu END ------------------------------------------------------

    //分组 
    $("#group").click(function (event) {
        $(this).css("background-color", "#fff");
        $(".m-group-result").css("display", "block");
        event.stopPropagation();
    });
    // 价格
    $("#prise").click(function (event) {
        $(this).css("background-color", "#fff");
        $(".m-prise-result").css("display", "block");
        event.stopPropagation();
    });
    //分组
    $(".m-group-result p").click(function () {
        $(".m-prise-result").css("display", "none");
        $("#group").html($(this).text()).css("background-color", "#fcfcfc").attr("value", $(this).attr("value"));
        getDataList_all(1, $("#checktype").val());
    });
    //价格
    $(".m-prise-result p").click(function () {
        $(".m-group-result").css("display", "none");
        $("#prise").html($(this).text()).css("background-color", "#fcfcfc").attr("value", $(this).attr("value"));
        getDataList_all(1, $("#checktype").val());
    });


    //------------------------------------------------ 开通/关闭服务 --------------------------------------------------
    //开通优质、一口价
    $(".kt_ykj_yz").click(function () {
        $.post("./index.php?c=Account&a=ktOperation",{"type":$(this).attr("k"),'action':'open'},function(result){
            if (result.status == 1) {
                showDialog(lang.a72, "./index.php?c=Account&a=index");
            }
            else
                showDialog(lang.a73);
        })
    })

    $(".bind-kt").click(function () {
        if (confirm(lang.a74)) {
            $.post("./index.php?c=Account&a=ktOperation", { "type": $(this).attr("k"),'action':'close'}, function (result) {
                if(result.status == 1){
                    showDialog(lang.a75, location.href);
                }else{
                    showDialog(result.info);
                }
            })
        }
    })

    //认领经纪人    
    $("#changeBroker").click(function () {  //#addBroker,#changeBroker
        $("#pop1_bg,#pop_broker").css("display", "block");
        if ($("#pop_broker dl").html() == "") {
            $.post("/account/ChangeBrokerInf", function (data) {
                var html = "";
                var da = $.parseJSON(data);
                for (i in da) {
                    html += "<dd><input type='radio' id='r" + i + "' value='" + da[i].id + "' name='r_broker' /><label for='r" + i + "'>" + da[i].name1 + "</label><a href='tencent://message/?uin=" + da[i].qq + "&amp;Site={pigcsm{$config.site_name}'>" +
                            "<img border='0' src='http://wpa.qq.com/pa?p=2:" + da[i].qq + ":51' alt='QQ交谈' title='QQ交谈'>" +
                            "</a></dd>";
                }
                $("#pop_broker dl").html(html);
            })
        }
    })
    $("#btn_addBroker").click(function () {
        var id = $("#pop_broker input[type='radio']:checked").val();
        if (!id) {
            alert("请选择经纪人");
            return false;
        }
        $.post("/account/ChangeBrrokerOperation", { "type": "0", "new": id, "reason": "" }, function (data, status) {
            if (status == "success") {
                location.href = location.href;
            }
            else
                alert(lang.a23);
        })
    })
    //更换经纪人
    $("#btn_change").click(function () {
		var id = $("#pop_broker input[type='radio']:checked").val();
        if (!id) {
            alert("请选择经纪人");
            return false;
        }
        $.post("./index.php?c=Account&a=replace", {"new": id,"reason": $("#reason").val() }, function (data, status) {
            if(data == 1){
                    showDialog("申请成功，我们会尽快帮您处理。", location.href);
                }else if(data == 2){
                    alert(lang.a23);
                }else{
                    showDialog("您已经申请过了，我们会尽快帮您处理。", location.href);
                }
        }
    )
    })
    $(".close_broker").live("click", function () {
        $("#pop1_bg,#pop_broker").css("display", "none");
    })

    //呼叫经纪人
    $("#calling").click(function () {
        $.post("index.php?c=Account&a=call", function (data, status) {
            if (status == "success") {
                if(data == 1){
                    showDialog(lang.a67);
                }else if(data == 2){
                    showDialog(lang.a70);
                }else{
                    showDialog(lang.a68);
                }
            }
        });
    })


    //搜索
    $(".i1").keyup(function (event) {
        if (event.keyCode == 13) {
            $(".u-btn8").click();
        }
    });

    //------------------------------------------ 批量操作 -----------------------------------------------

    //单个checkbox
    $(".curLine").live("click", function () {
        //获取|重置cookie
        var ckl = getCookie("selectAll");
        if (ckl == null || ckl.match(/(.+?)\&&(.*)/)[1] != href)
            ckl = href + "&&";
        if (this.checked) {//选中
            if (ckl.indexOf("&&all") > -1){
				alert(ckl);
                ckl = ckl.replace("," + this.value + ",", ",");
				alert(ckl);
            }else{
                ckl += this.value + ",";
			}
        } else {//取消选中
            if (ckl.indexOf("&&all") > -1)
                ckl += this.value + ",";
            else
                ckl = ckl.replace("&&" + this.value + ",", "&&").replace("," + this.value + ",", ",");
        }
        setCookie("selectAll", ckl, 2)
    })

    //选中当前页
    $(".curPage").click(function () {
        //获取|重置cookie
        var ckl = getCookie("selectAll");
        if (ckl == null || ckl.match(/(.+?)\&&(.*)/)[1] != href)
            ckl = href + "&&";
        if (this.checked) { //选中
            $(".curLine").not("input:checked").each(function () {
                if (ckl.indexOf("&all") > -1){
                    ckl = ckl.replace("," + this.value + ",", ",");
                    ckl = ckl.replace(this.value + ",", ",");
				}else{
                    ckl += this.value + ",";
				}
                $(this).attr("checked", true);
            })

        } else {  //取消选中
            $(".curLine:checked").each(function () {
                if (ckl.indexOf("&all") > -1)
                    ckl += this.value + ",";
                else
                    ckl = ckl.replace("&" + this.value + ",", "&").replace("," + this.value + ",", ",");
                $(this).attr("checked", false);
            })
        }
        setCookie("selectAll", ckl, 2)
    })

    //选中所有
    $(".allPage").click(function () {
        if (this.checked) {
            $(".curLine").attr("checked", true);
            var ck = href + "&&all";
            if ($(".u_vm").length > 0) {
                ck += "|" + ($(".u_vm").val() == $(".u_vm").attr("ph") ? "" : $(".u_vm").val());
                ck += "|" + $("#group").attr("value");
                ck += "|" + $("#prise").attr("value");
            } else{
                ck += "|||";
			}
            setCookie("selectAll", ck + ",", 2)
        } else {
            $(".curLine").attr("checked", false);
            setCookie("selectAll", href + "&&", 2)
        }
    })

    //设置
    $(".setting").live("click", function () {
        var ipt_ck = $(this).parent().parent().find("input.curLine");
        ipt_ck[0].checked = false;
        delCookie("selectAll");
        ipt_ck[0].click();
        location.href = $(this).attr("k");
    })

    //申请竞价 展示一口价 申请优质 批量设置
    $(".btn_pl").live("click", function () {
        var ckl = getCookie("selectAll");
        if (ckl != null) {
            var resut_arr = ckl.match(/(.+?)\&&(.*)/);
            if (resut_arr != null && resut_arr[1] == href && resut_arr[2].replace(/\s/g, "") != "") {
                var v = $(this).attr("v");
                if (v == "del") {
                    if (!confirm(lang.a22 + "？"))
                        return false;
                }
                if (v == "sold") {
                    if (!confirm(lang.a49 + "？"))
                        return false;
                }
                location.href = $(this).attr("k");
            }
            else
                showDialog(lang.a61);
        } else {
            showDialog(lang.a61);
        }
    })

    $(".yj_del").live("click", function () {
        if (confirm(lang.a22 + "[" + $(this).parent().parent().find(".domain").text() + "]？")) {
            $.post("./index.php?c=Account&a=allsellOperation", { "id": $(this).parent().parent().find("input").attr("value") }, function (result) {
                if (result.status == 1)
                    location.href = location.href;
                else
                    showDialog(lang.a23);
            })
        }
    })

    //批量-价钱
    $("input.money,input.money1").keyup(function () {
        clearNoNum1(this);
    })
    $("input[name='pl_money']").focusout(function () {
        $("input.money").val(this.value);
    })
    $("input[name='all_blj']").focusout(function () {
        $("input.money1").val(this.value);
    })
    //过滤价
    $("input.glmoney").keyup(function () {
        clearNoNum1(this);
    })
    $("input[name='all_blj']").focusout(function () {
        $("input.glmoney").val(this.value);
    })

    //批量-简介
    $("input[name='pl_desc']").focusout(function () {
        $("input[name='desc']").val(this.value);
    })

    //批量-模版名称
    $("select[name='all_group_id']").change(function () {
        var v = $(this).find("option:selected").val();
        $("select[name='group_id']").find("option[value='" + v + "']").attr("selected", true);
    })

    //批量-移除
    $(".ipt_remove").click(function () {
        $(this).parent().parent().remove();
    })

    $(".pl_submit").click(function () {
        if (/pl\/ykj|pl\/yz|pl_set\/ykjset|pl_set\/yzset/.test(location.href)) {
            var temp = false;
            $("input.money:not(:first)").each(function () {
                if (this.value == "" || this.value == "0") {
                    temp = true;
                    return false;
                }
            })
            if (temp) {
                showDialog(lang.a71);
                return false;
            }
        }
        //document.getElementById("form").submit();
    })
    //------------------------------------------ 批量操作 END -------------------------------------------
})//--

//我给出的报价  我收到的报价
function getData_negotiation(pageindex, type) {
    showLodingImg("0", "50%", "435px", "-150px");
    var domain = $("#txt_domain").val() == $("#txt_domain").attr("ph") ? "" : $("#txt_domain").val();
    $.post(type ? "./index.php?c=Account&a=mysell" : "./index.php?c=Account&a=mybuy", { "domain": domain, "t1": $("#startTime").val(), "t2": $("#endTime").val(), "page": pageindex, "type": $("#checktype").val() }, function (result) {
        if (result.status == '1') {
            var da = result.info.list, tb = $(".m-acc-cons table");
            $(".m-acc-cons table tr:not(:first)").remove();
            for (i in da) {
                var href = "./index.php?c=Account&a=" + (type ? "sell" : "") + "negotiation_detail&id=" + da[i].quote_id;
                var r = "<tr><td></td>"
                  + "<td class='u_tl'>" + (da[i]._type ? lang.a81 : lang.a82) + "</td>"
                  + "<td class='u_tl'><a href='" + href + "' target='_blank' class='u-txa1' title='" + da[i].domain + "'>" + da[i].domain + "</a></td>"
                  + "<td class='u_tl'>" + negotiateState(da[i].status, da[i].last_offer, type) + "</td>"
                  + "<td class='u_tr s-75b'>" + formatMoney(da[i].new_money, 0, "￥") + "</td>"
                  + "<td></td>"
                  + "<td class='u_tl'>" + da[i].last_time.substring(0, 10) + "</td>"
                  + "<td><a class='u-abtn8' href='" + href + "' target='_blank'>" + lang.a83 + "</a></td>"
                  + "<td></td></tr>"
                tb.append(r);
            }
            if (da.length == 0) {
                tb.append("<tr><td class='u_nb u_tc u_norecord' colspan='9'>" + lang.a77 + "</td></tr>");
            }
            $('#paging').html(nextPage(pageindex, result.info.total, 20, 3));
            hideLodingImg();
        } else {
            hideLodingImg();
        }
    })
}

//买家 我参与的竞价
function getDataList_partakefast(pageindex, type) {
    showLodingImg("0", "50%", "430px", "-150px");
    $.post("/account/partakefastinf", { "pageindex": pageindex, "type": type }, function (res, status) {
        if (status == "success") {
            var da = $.parseJSON(res.data), tb = $(".m-acc-cons table");
            $(".m-acc-cons table tr:not(:first)").remove();

            for (i in da) {
                var r = "<tr><td></td>"
                 + "<td class='u_tl'><a class='u-txa1' href='/fast-bid/detail/" + da[i].id + "' target='_blank'>" + da[i].domainName + "</a></td>"
                 + "<td class='s-75b u_tr'>" + formatMoney(da[i].maxmoney, 0, "￥") + "</td>"
                 + "<td></td>"
                 + "<td>" + da[i]._count + "</td>"
                 + "<td></td>"
                 + "<td class='u_tl'>" + da[i].overtime + "</td>"
                 + "<td class='u_tl'>" + (da[i]._state == 4 ? lang.a92 : (da[i]._count == 0 || (da[i].maxmoney < da[i].reservePrice)) ? lang.a90 : lang.a91) + "</td>"
                 + "<td><a class='u-abtn8' href='/fast-bid/detail/" + da[i].id + "' target='_blank'>" + lang.a83 + "</a></td>"
                 + "<td></td></tr>"
                tb.append(r);
            }
            if (da.length == 0) {
                tb.append("<tr><td class='u_nb u_tc u_norecord' colspan='10'>" + lang.a77 + "</td></tr>");
            }

            $('#paging').html(nextPage(pageindex, res.total, 20, 3));
            hideLodingImg();
        } else {
            hideLodingImg();
        }
    })
}

//买家 我委托的代购  0审核失败  1已付订金(代购中)  2代购成功  3代购失败
function getDataList_pay(pageindex, type) {
    showLodingImg("0", "50%", "395px", "-150px");

    $.post("/account/payinf", { "pageindex": pageindex, "type": type }, function (res, status) {
        if (status == "success") {
            var da = $.parseJSON(res.data)
            switch (type) {
                case "0":
                    var tb = $(".tb_ap_fail");
                    $(".tb_ap_fail tr:not(:first)").remove();
                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl'><a class='u-txa1 s-2a' href='/account/paydetail/" + da[i].id + "' target='_blank' title='" + da[i].domainName + "'>" + da[i].domainName + "</a></td>"
                          + "<td class='s-75b u_tr'>" + formatMoney(da[i]._money, 0, "￥") + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'>" + da[i].applyTime.substring(0, 10) + "</td>"
                          + "<td class='u_tl'><span class='u-span8' title='" + da[i].ly.replace('审核不通过,理由:', "") + "'>" + da[i].ly.replace('审核不通过,理由:', "") + "</span></td>"
                          + "<td class='u_tr'><a class='u-abtn8' href='/procurement/again/" + da[i].id + "' target='_blank'>" + lang.a93 + "</a></td>"
                          + "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='8' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                    }
                    break;
                case "1":
                case "2":
                case "3":
                    var tb = $(".tb_ap_fail");
                    $(".tb_ap_fail tr:not(:first)").remove();
                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl'><a class='u-txa1 s-2a' href='/account/paydetail/" + da[i].id + "' target='_blank' title='" + da[i].domainName + "'>" + da[i].domainName + "</a></td>"
                          + "<td class='s-75b u_tr'>" + formatMoney(da[i]._money, 0, "￥") + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'>" + da[i].applyTime.substring(0, 10) + "</td>"
                          + "<td><a class='u-abtn8' href='/account/paydetail/" + da[i].id + "' target='_blank'>" + lang.a83 + "</a></td>"
                          + "<td></td></tr>";
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='7' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                    }
                    break;
            }

            $('#paging').html(nextPage(pageindex, res.total, 20, 3));
            hideLodingImg();
        } else {
            hideLodingImg();
        }
    })
}

//卖家 我的终端域名  3审核失败 4推荐中 5推荐成功 6推荐失败
function get_terminal(pageindex, type) {
    showLodingImg("0", "50%", "395px", "-150px");

    $.post("/account/terminalInf", { "pageIndex": pageindex, "pageSize": "20", "state": type }, function (res, status) {
        if (status == "success") {
            var da = $.parseJSON(res.data)
            switch (type) {
                case "3":
                    var tb = $(".tb_ap_fail");
                    $(".tb_ap_fail tr:not(:first)").remove();
                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl'><a class='u-txa1 s-2a' href='/account/terminaldetail/" + da[i].id + "' target='_blank' title='" + da[i].domainName + "'>" + da[i].domainName + "</a></td>"
                          + "<td class='s-75b u_tr'>" + formatMoney(da[i].minMoney, 0, "￥") + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'>" + da[i].startTime.substring(0, 10) + "</td>"
                          + "<td class='u_tl'><span class='u-span8' title='" + da[i].remark.replace('审核不通过,理由:', "") + "'>" + da[i].remark.replace('审核不通过,理由:', "") + "</span></td>"
                          + "<td class='u_tr'><a class='u-abtn8' href='/terminal/again/" + da[i].id + "' target='_blank'>" + lang.a93 + "</a></td>"
                          + "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='8' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                    }
                    break;
                case "4":
                case "5":
                case "6":
                    var tb = $(".tb_ap_fail");
                    $(".tb_ap_fail tr:not(:first)").remove();
                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl'><a class='u-txa1 s-2a' href='/account/terminaldetail/" + da[i].id + "' target='_blank' title='" + da[i].domainName + "'>" + da[i].domainName + "</a></td>"
                          + "<td class='s-75b u_tr'>" + formatMoney(da[i].minMoney, 0, "￥") + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'>" + da[i].startTime.substring(0, 10) + "</td>"
                          + "<td><a class='u-abtn8' href='/account/terminaldetail/" + da[i].id + "' target='_blank'>" + lang.a83 + "</a></td>"
                          + "<td></td></tr>";
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='7' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                    }
                    break;
            }

            $('#paging').html(nextPage(pageindex, res.total, 20, 3));
            hideLodingImg();
        } else {
            hideLodingImg();
        }
    })
}

//卖家 我的代销域名   0审核失败 1代销中 2代销成功 3代销失败
function get_sale(pageindex, type) {
    showLodingImg("0", "50%", "395px", "-150px");
    switch (type) {//0审核中 1代销中 2审核失败 3代销成功 4代销失败
        case "0":
            type = "2";
            break;
        case "2":
            type = "3";
            break;
        case "3":
            type = "4";
            break;
    }

    $.post("/account/saleInf", { "pageIndex": pageindex, "pageSize": "20", "state": type }, function (res, status) {
        if (status == "success") {
            var da = $.parseJSON(res.data)
            switch (type) {
                case "2":
                    var tb = $(".tb_ap_fail");
                    $(".tb_ap_fail tr:not(:first)").remove();
                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl'><a class='u-txa1 s-2a' href='/account/saledetail/" + da[i].id + "' target='_blank' title='" + da[i].title + "'>" + da[i].title + "</a></td>"
                          + "<td class='s-75b u_tr'>" + formatMoney(da[i].minMoney, 0, "￥") + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'>" + da[i].applyTime.substring(0, 10) + "</td>"
                          + "<td class='u_tl'><span class='u-span8' title='" + da[i].remark.replace('审核未通过，理由:', "") + "'>" + da[i].remark.replace('审核未通过,理由:', "") + "</span></td>"
                          + "<td class='u_tr'><a class='u-abtn8' href='/sale/again/" + da[i].id + "' target='_blank'>" + lang.a93 + "</a></td>"
                          + "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='8' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                    }
                    break;
                default:
                    var tb = $(".tb_ap_fail");
                    $(".tb_ap_fail tr:not(:first)").remove();
                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl'><a class='u-txa1 s-2a' href='/account/saledetail/" + da[i].id + "' target='_blank' title='" + da[i].title + "'>" + da[i].title + "</a></td>"
                          + "<td class='s-75b u_tr'>" + formatMoney(da[i].minMoney, 0, "￥") + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'>" + da[i].applyTime.substring(0, 10) + "</td>"
                          + "<td><a class='u-abtn8' href='/account/saledetail/" + da[i].id + "' target='_blank'>" + lang.a83 + "</a></td>"
                          + "<td></td></tr>";
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='7' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                    }
                    break;
            }

            $('#paging').html(nextPage(pageindex, res.total, 20, 3));
            hideLodingImg();
        } else {
            hideLodingImg();
        }
    })
}

//卖家 待验证的域名
function getDataList_withcheck(pageindex) {
    showLodingImg("0", "50%", "500px", "-150px");
    $.post("./index.php?c=Account&a=withcheck", { "page": pageindex }, function (result) {
		if(result.status == 1){	
            var da = result.info.lists, tb = $(".m-account-data2 table");
            $(".m-account-data2 table tr:not(:first)").remove();

            for (i in da) {
                var r = "<tr><td></td>"
                  + "<td class='u_tl'><input type='checkbox' value='" + da[i].domain_id + "' class='box curLine'/></td>"
                  + "<td class='u_tl'>" + da[i].domain + "</td>"
                  + "<td class='u_tl'><span>" + lang.a84 + "</span></td>"
                  + "<td class='u_tl'>" + da[i].add_time + "</td>"
                  + "<td class='u_tr'><a class='u-abtn8 btn_verify' k='0'>" + lang.a86 + "</a>&nbsp;&nbsp;<a class='u-abtn8 btn_verify' k='1'>" + lang.a85 + "</a></td>"
                  + "<td></td></tr>"
                tb.append(r);
            }
            if (da.length == 0) {
                tb.append("<tr><td colspan='7' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                $(".m-operation3").css("display", "none");
            } else {
                $(".m-operation3").css("display", "block");
            }

            $('#paging').html(nextPage(pageindex, result.info.total, 20, 3));
            hideLodingImg();
            loadCookie();
        } else {
            hideLodingImg();
        }
    })
}

//卖家 我提交的竞价     0审核  3竞价结束
function getDataList_bid(pageindex, type) {
    $.post("./index.php?c=Account&a=fastbidinf", { "page": pageindex, "type": type }, function (result) {
        if(result.status == 1){
            switch (type) {
                case "0": //审核
                    showLodingImg("0", "50%", "580px", "-150px");
                    var da = result.info.list, tb = $(".bid_ap_fail");
                    $(".bid_ap_fail tr:not(:first)").remove();
                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl'>" + da[i].domain + "</td>"
                          + "<td class='u_tr s-75b'>" + formatMoney(da[i].money, 0, "￥") + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'>" + da[i].apply_time.substring(0, 10) + "</td>"
                          + "<td class='u_tl'><span class='u-span13' title='" + da[i].desc + "'>" + da[i].desc + "</span></td>"
                          + "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='7' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                    }
                    break;
                case "3": //竞价结束               
                    showLodingImg("0", "50%", "360px", "-150px");
                    var da = $.parseJSON(res.data), tb = $(".bid_ap_fail");
                    $(".bid_ap_fail tr:not(:first)").remove();

                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl'><a class='u-txa1' href='/fast-bid/detail/" + da[i].id + "' target='_blank'>" + da[i].domainName + "</a></td>"
                          + "<td class='u_tr s-75b'>" + formatMoney(da[i].maxmoney, 0, "￥") + "</td>"
                          + "<td class='u_tr s-75b'>" + formatMoney(da[i].reservePrice, 0, "￥") + "</td>"
                          + "<td>" + da[i]._count + "</td>"
                          + "<td class='u_tl'>" + da[i].overtime + "</td>"
                          + "<td class='u_tr'>" + ((da[i]._count == 0 || (da[i].maxmoney < da[i].reservePrice)) ? lang.a90 : lang.a91) + "</td>"
                          + "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='8' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                    }
                    break;
            }
            $('#paging').html(nextPage(pageindex, result.info.total, 20, 3));
            hideLodingImg();
            loadCookie();
        } else {
            hideLodingImg();
        }
    })
}

//卖家 我的一口价     0出售中  1已出售  3已下架
function getDataList_aprice(pageindex, type) {
    showLodingImg("0", "50%", "480px", "-150px");
    $.post("./index.php?c=Account&a=apriceinf", { "pageindex": pageindex, "type": type }, function (result) {
        if (result.status == 1) {
            var da = result.info.lists, tb = $(".m-acc-cons table");
            $(".m-acc-cons table tr:not(:first)").remove();
            switch (type) {
                case "0": //出售中
                    for (i in da) {
						var r = "<tr><td></td>"
						    + "<td class='u_tl'><input type='checkbox' value='" + da[i].domain_id + "' class='u_vt curLine' /></td>"
							+ "<td class='u_tl'><a class='u-txa1' title='" + da[i].domain + "' href='./index.php?c=Hotsale&a=selling&id=" + da[i].domain_id + "' target='_blank'>" + da[i].domain + "</a></td>"
							+ "<td class='u_tr s-75b'>" + formatMoney(da[i].money, 0, "￥") + "</td>"
							+ "<td></td>"
							+ "<td class='u_tl'><span class='u-span13' title='" + da[i].desc + "'>" + da[i].desc + "</span></td>"
							+ "<td></td>"
							+ "<td class='u_tl'>" + da[i].startTime.substring(0, 10) + "</td>"
							+ "<td class='u_tr'><a class='u-abtn8 btn_verify sold_ykj'>" + lang.a79 + "</a>&nbsp;&nbsp;<a class='u-abtn8 btn_verify setting' k='./index.php?c=Account&a=pl_ykj'>" + lang.a80 + "</a></td>"
							+ "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='10' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                        $(".m-operation4").css("display", "none");
                    } else {
                        $(".m-operation4").css("display", "block");
                    }
                    break;
                case "1": //已出售                        
                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl' title='" + da[i].domainName + "'>" + da[i].domainName + "</td>"
                          + "<td class='u_tr s-75b'>" + formatMoney(da[i]._money, 0, "￥") + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'><span class='u-span13' title='" + da[i].intro + "'>" + da[i].intro + "</span></td>"
                          + "<td></td>"
                          + "<td class='u_tl'>" + da[i].endTime.substring(0, 10) + "</td>"
                          + "<td><a class='u-abtn8 btn_verify' target='_blank' href='/account/dealdetail/" + da[i].dealsId + "'>" + lang.a76 + "</a></td>"
                          + "<td></td></tr>"

                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='9' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                        $(".m-operation4").css("display", "none");
                    } else {
                        $(".m-operation4").css("display", "block");
                    }
                    break;
                case "3": //已下架
                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl'><input type='checkbox' value='" + da[i].domainId + "' class='u_vt curLine' /></td>"
                          + "<td class='u_tl' title='" + da[i].domainName + "'>" + da[i].domainName + "</td>"
                          + "<td class='u_tr s-75b'>" + formatMoney(da[i]._money, 0, "￥") + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'>" + da[i].endTime.substring(0, 10) + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'><span class='u-span13' title='" + da[i].remark + "'>" + da[i].remark + "</span></td>"
                          + "<td class='u_tr'><a class='u-abtn8 btn_verify setting' href='javascript:void(0)' k='/account/pl/ykj'>" + lang.a78 + "</a></td>"
                          + "<td></td></tr>"

                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='10' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                        $(".m-operation4").css("display", "none");
                    } else {
                        $(".m-operation4").css("display", "block");
                    }
                    break;
            }
            $('#paging').html(nextPage(pageindex, result.info.total, 20, 3));
            hideLodingImg();
            loadCookie();
        } else {
            hideLodingImg();
        }
    })
}

//卖家 我的优质       4审核失败 2出售中 1已下架 0已出售
function getDataList_high(pageindex, type) {
    showLodingImg("0", "50%", "480px", "-150px");
    $.post("/account/highgradeinf", { "pageindex": pageindex, "type": type }, function (res, status) {
        if (status == "success") {
            var da = $.parseJSON(res.data), tb;
            switch (type) {
                case "0": //已出售
                    tb = $(".m-acc-cons table");
                    $(".m-acc-cons table tr:not(:first)").remove();
                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl' title='" + da[i].domainName + "'>" + da[i].domainName + "</td>"
                          + "<td class='u_tr s-75b'>" + formatMoney(da[i]._money, 0, "￥") + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'><span class='u-span13' title='" + da[i].intro + "'>" + da[i].intro + "</span></td>"
                          + "<td></td>"
                          + "<td class='u_tl'>" + da[i].publishtime.substring(0, 10) + "</td>"
                          + "<td><a class='u-abtn8' target='_blank' href='/account/dealdetail/" + da[i].dealsId + "'>" + lang.a76 + "</a></td>"
                          + "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='9' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                    }
                    break;
                case "1": //已下架
                    tb = $(".m-acc-cons table");
                    $(".m-acc-cons table tr:not(:first)").remove();
                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl'><input type='checkbox' value='" + da[i].domainId + "' class='u_vt curLine' /></td>"
                          + "<td class='u_tl' title='" + da[i].domainName + "'>" + da[i].domainName + "</td>"
                          + "<td class='u_tr s-75b'>" + formatMoney(da[i]._money, 0, "￥") + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'>" + da[i].publishtime.substring(0, 10) + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'><span class='u-span13' title='" + da[i].remarks + "'>" + da[i].remarks + "</span></td>"
                          + "<td class='u_tr'><a class='u-abtn8 btn_verify setting'  href='javascript:void(0)' k='/account/pl/yz'>" + lang.a78 + "</a></td>"
                          + "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='10' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                        $(".m-operation4").css("display", "none");
                    } else {
                        $(".m-operation4").css("display", "block");
                    }
                    break;
                case "2": //出售中
                    tb = $(".m-acc-cons table");
                    $(".m-acc-cons table tr:not(:first)").remove();
                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl'><input type='checkbox' class='u_vt curLine' value='" + da[i].id + "'/></td>"
                          + "<td class='u_tl'><a class='u-txa1' href='/bargain/selling/" + da[i].id + "' title='" + da[i].domainName + "'>" + da[i].domainName + "</a></td>"
                          + "<td class='u_tr s-75b'>" + formatMoney(da[i]._money, 0, "￥") + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'><span class='u-span13' title='" + da[i].intro + "'>" + da[i].intro + "</span></td>"
                          + "<td></td>"
                          + "<td class='u_tl'>" + da[i].publishtime.substring(0, 10) + "</td>"
                          + "<td class='u_tr'><a class='u-abtn8 btn_verify sold_out'>" + lang.a79 + "</a>&nbsp;&nbsp;<a class='u-abtn8 btn_verify setting' k='/account/pl_set/yzset'>" + lang.a80 + "</a></td>"
                          + "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='10' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                        $(".m-operation4").css("display", "none");
                    } else {
                        $(".m-operation4").css("display", "block");
                    }
                    break;
                case "4": //审核失败
                    tb = $(".u_mall2 table");
                    $(".u_mall2 table tr:not(:first)").remove();
                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl' title='" + da[i].domainName + "'>" + da[i].domainName + "</td>"
                          + "<td class='u_tr s-75b'>" + formatMoney(da[i]._money, 0, "￥") + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'>" + da[i].publishtime.substring(0, 10) + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'><span title='" + da[i].remarks + "'>" + da[i].remarks + "</span></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='7' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                    }
                    break;
            }
            $('#paging').html(nextPage(pageindex, res.total, 20, 3));
            hideLodingImg();
            loadCookie();
        } else {
            hideLodingImg();
        }
    })
}

//卖家 设置出售页
function getDataList_sellPage(pageindex) {
    showLodingImg("0", "50%", "670px", "-150px");
    $.post("/account/sellpageinf", { "pageindex": pageindex }, function (res, status) {
        if (status == "success") {
            var da = $.parseJSON(res.data), tb = $(".m-acc-cons1 table");
            $(".m-acc-cons1 table tr:not(:first)").remove();

            for (i in da) {
                var r = "<tr><td></td>"
                  + "<td class='u_tl'><input type='checkbox' class='curLine'  value='" + da[i].id + "'/></td>"
                  + "<td class='u_tl'><a class='u-txa1' href='http://" + da[i].domainName + "' target='_blank'>" + da[i].domainName + "</a></td>"
                  + "<td class='u_tl'>" + da[i].templateName + "</td>"
                  + "<td><a class='u-abtn8 btn_verify setting' k='/account/pl/sellpage'>" + lang.a80 + "</a></td>"
                  + "<td></td></tr>"
                tb.append(r);
            }
            if (da.length == 0) {
                tb.append("<tr><td colspan='6' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                $(".m-operation4").css("display", "none");
            } else {
                $(".m-operation4").css("display", "block");
            }

            $('#paging').html(nextPage(pageindex, res.total, 20, 3));
            hideLodingImg();
            loadCookie();
        } else {
            hideLodingImg();
        }
    })
}

//卖家 我的所有域名     0所有   1议价   2竞价   3一口价   4优质    5、6、7议价[提交一口价、优质、竞价]
function getDataList_all(pageindex, type) {
    showLodingImg("0", "50%", "490px", "-150px");
    //参数
    var domainName = $(".u_vm").val() == $(".u_vm").attr("ph") ? "" : $(".u_vm").val();
    var groupid = $("#group").attr("value");
    var haveprice = $("#prise").attr("value");
    var p_type = type;
    if (p_type == "5" || p_type == "6" || p_type == "7")
        p_type = "1";
    $.post("./index.php?c=Account&a=allsellinf", { "page": pageindex, "type": p_type, "domainName": domainName, "groupid": groupid, "haveprice": haveprice }, function (result) {
        if (result.status == 1) {
            var da = result.info.lists, tb = $(".m-acc-cons table");
            $(".m-acc-cons table tr:not(:first)").remove();
            switch (type) {
                case "0": //所有
                    for (i in da) {
                        var r = "<tr><td></td>"
						  + "<td>" + (da[i].type == 0 ? "<input type='checkbox' class='box curLine' value='" + da[i].domain_id + "' />" : "") + "</td>"
						  + "<td><span " + (da[i].type == 0 ? "" : da[i].type == 3 ? "class='z-img-b7' title='" + lang.a81 + "'" : da[i].type == 1 ? "class='z-img-b5' title='" + lang.a87 + "'" : "class='z-img-b8' title='" + lang.a88 + "'") + "></span></td>"
						  + "<td class='u_tl'><a class='u-txa1 domain' href='" + getUrl(da[i]) + "' target='_blank' title='" + da[i].domain + "'>" + da[i].domain + "</a></td>"
						  + "<td>" + (da[i].group_name == null ? "-" : da[i].group_name) + "</td>"
						  + "<td></td>"
						  + "<td class='u_tr s-75b'>" + (da[i].money == 0 ? lang.a89 : formatMoney(da[i].money, 0, "￥")) + "</td>"
						  + "<td></td>"
						  + "<td class='u_tl'><span class='u-span13' title='" + da[i].desc + "'>" + da[i].desc + "</span></td>"
						  + "<td class='u_tr'>" + (da[i].type == 0 ? "<a class='u-abtn8 btn_verify yj_del'>" + lang.a85 + "</a>&nbsp;&nbsp;<a class='u-abtn8 btn_verify setting' k='./index.php?c=Account&a=pl_yj_set'>" + lang.a80 + "</a>" : "") + "</td>"
						  + "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='11' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                        $(".m-operation4").css("display", "none");
                    } else {
                        $(".m-operation4").css("display", "block");
                    }
                    break;
                case "1": //议价
                    for (i in da) {
                        var r = "<tr><td></td>"
						  + "<td><input type='checkbox' class='u_vt curLine' value='" + da[i].domain_id + "' /></td>"
					      + "<td class='u_tl'> <a class='u-txa1 u_tl domain' href='" + getUrl(da[i]) + "' target='_blank' title='" + da[i].domain + "'>" + da[i].domain + "</a></td>"
						  + "<td>" + (da[i].group_name == null ? "-" : da[i].group_name) + "</td>"
						  + "<td></td>"
						  + "<td class='u_tr s-75b'>" + (da[i].money == 0 ? lang.a89 : formatMoney(da[i].money, 0, "￥")) + "</td>"
						  + "<td></td>"
						  + "<td class='u_tl'><span class='u-span13' title='" + da[i].desc + "'>" + da[i].desc + "</span></td>"
						  + "<td class='u_tr'><a class='u-abtn8 btn_verify yj_del'>" + lang.a85 + "</a>&nbsp;&nbsp;<a class='u-abtn8 btn_verify setting' k='./index.php?c=Account&a=pl_yj_set'>" + lang.a80 + "</a></td>"
						  + "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='10' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                        $(".m-operation4").css("display", "none");
                    } else {
                        $(".m-operation4").css("display", "block");
                    }
                    break;
                case "5": //5、6、7议价[提交一口价、优质、竞价]
                case "6":
                case "7":
                    var u = "/account/pl/ykj";
                    u = (type == "6") ? "./index.php?c=Account&a=pl_yz" : (type == "7") ? "./index.php?c=Account&a=pl_fastbid" : u;
                    for (i in da) {
                        var r = "<tr><td></td>"
						  + "<td><input type='checkbox' class='u_vt curLine' value='" + da[i].domain_id + "' /></td>"
					      + "<td class='u_tl'> <a class='u-txa1 u_tl domain' href='" + getUrl(da[i]) + "' target='_blank' title='" + da[i].domain + "'>" + da[i].domain + "</a></td>"
						  + "<td>" + (da[i].group_name == null ? "-" : da[i].group_name) + "</td>"
						  + "<td></td>"
						  + "<td class='u_tr s-75b'>" + (da[i].money == 0 ? lang.a89 : formatMoney(da[i].money, 0, "￥")) + "</td>"
						  + "<td></td>"
						  + "<td class='u_tl'><span class='u-span13' title='" + da[i].desc + "'>" + da[i].desc + "</span></td>"
						  + "<td class='u_tr'><a class='u-abtn8 btn_verify setting' k='" + u + "'>" + lang.a80 + "</a></td>"
						  + "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='10' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                        $(".m-operation4").css("display", "none");
                    } else {
                        $(".m-operation4").css("display", "block");
                    }
                    break;
                case "2": //一口价
                    for (i in da) {
                        var r = "<tr><td></td>"
						    + "<td class='u_tl'><input type='checkbox' value='" + da[i].domain_id + "' class='u_vt curLine' /></td>"
							+ "<td class='u_tl'><a class='u-txa1' title='" + da[i].domain + "' href='./index.php?c=Hotsale&a=selling&id=" + da[i].domain_id + "' target='_blank'>" + da[i].domain + "</a></td>"
							+ "<td class='u_tr s-75b'>" + formatMoney(da[i].money, 0, "￥") + "</td>"
							+ "<td></td>"
							+ "<td class='u_tl'><span class='u-span13' title='" + da[i].desc + "'>" + da[i].desc + "</span></td>"
							+ "<td></td>"
							+ "<td class='u_tl'>" + da[i].startTime.substring(0, 10) + "</td>"
							+ "<td class='u_tr'><a class='u-abtn8 btn_verify sold_ykj'>" + lang.a79 + "</a>&nbsp;&nbsp;<a class='u-abtn8 btn_verify setting' k='./index.php?c=Account&a=pl_ykj'>" + lang.a80 + "</a></td>"
							+ "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='10' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                        $(".m-operation4").css("display", "none");
                    } else {
                        $(".m-operation4").css("display", "block");
                    }
                    break;
				case "3": //极速竞价
                    for (i in da) {
                        var r = "<tr><td></td>"
						    + "<td class='u_tl'><input type='checkbox' value='" + da[i].domain_id + "' class='u_vt curLine' /></td>"
							+ "<td class='u_tl'><a class='u-txa1' title='" + da[i].domain + "' href='./index.php?c=Fastbid&a=detail&id=" + da[i].domain_id + "' target='_blank'>" + da[i].domain + "</a></td>"
							+ "<td class='u_tr s-75b'>" + formatMoney(da[i].money, 0, "￥") + "</td>"
							+ "<td></td>"
							+ "<td class='u_tl'><span class='u-span13' title='" + da[i].desc + "'>" + da[i].desc + "</span></td>"
							+ "<td></td>"
							+ "<td class='u_tl'>" + da[i].startTime.substring(0, 10) + "</td>"
							+ "<td class='u_tr'><a class='u-abtn8 btn_verify setting' k='./index.php?c=Account&a=pl_fastbid'>" + lang.a80 + "</a></td>"
							+ "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='10' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                        $(".m-operation4").css("display", "none");
                    } else {
                        $(".m-operation4").css("display", "block");
                    }
                    break;
                case "4": //优质
                    for (i in da) {
                        var r = "<tr><td></td>"
							+ "<td class='u_tl'><input type='checkbox' class='u_vt curLine' value='" + da[i].domain_id + "' /></td>"
							+ "<td class='u_tl'><a class='u-txa1' title='" + da[i].domain + "' href='./index.php?c=Bargain&a=selling&id=" + da[i].domain_id + "' target='_blank'>" + da[i].domain + "</a></td>"
							+ "<td class='u_tr s-75b'>" + formatMoney(da[i].money, 0, "￥") + "</td>"
							+ "<td></td>"
							+ "<td class='u_tl'><span class='u-span13' title='" + da[i].desc + "'>" + da[i].desc + "</span></td>"
							+ "<td></td>"
							+ "<td class='u_tl'>" + da[i].startTime.substring(0, 10) + "</td>"
							+ "<td class='u_tr'>"+(da[i].status==1 ? "<a class='u-abtn8 btn_verify sold_yz' >" + lang.a79 + "</a>&nbsp;&nbsp;<a class='u-abtn8 btn_verify setting' k='./index.php?c=Account&a=pl_yz'>" + lang.a80 + "</a>" : "正在审核")+"</td>"
							+ "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='10' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                        $(".m-operation4").css("display", "none");
                    } else {
                        $(".m-operation4").css("display", "block");
                    }
                    break;
            }
            $('#paging').html(nextPage(pageindex, result.info.total, 20, 3));
            hideLodingImg();
            loadCookie();
        } else {
            hideLodingImg();
        }
    })
}

//我的所有交易    0所有  1进行中  2已完成  3已取消
function getDataList_deals(pageindex, type) {
    showLodingImg("0", "50%", "490px", "-150px");
    //获取参数
    var domain = $(".i_domain").val();
    if (domain == $(".i_domain").attr("ph"))
        domain = "";
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var order = $(".m-acc-cons table th span[class]").attr("order");
    var ordertype = $(".m-acc-cons table th span[class]").attr("class") == "f-img-up" ? "asc" : "desc";

    $.post("/account/dealsinf", { "pageindex": pageindex, "type": type, "domainName": domain, "starttime": startTime, "endtime": endTime, "order": order, "ordertype": ordertype }, function (res, status) {
        if (status == "success") {
            var da = $.parseJSON(res.data), tb = $(".m-acc-cons table");
            $(".m-acc-cons table tr:not(:first)").remove();
            for (i in da) {
                var r = "<tr><td></td>"
                      + "<td class='u_tl'><a href='/account/dealdetail/" + da[i].id + "' target='_blank' class='u-txa1' title='" + da[i].title + "'>" + da[i].title + "</a></td>"
                      + "<td class='u_tr s-75b'>" + formatMoney(da[i]._money, 0, "￥") + "</td>"
                      + "<td></td>"
                      + "<td class='u_tl'>" + get_type(da[i]._type, da[i].pid) + "</td>"
                      + "<td class='u_tl'>" + (da[i].ib == 0 ? lang.a94 : lang.a95) + "</td>"
                      + "<td class='u_tl'>" + dealsState(da[i]._state, da[i].buyer) + "</td>"
                      + "<td><a class='u-abtn8' href='/account/dealdetail/" + da[i].id + "' target='_blank'>" + lang.a83 + "</a></td>"
                      + "<td></td></tr>"
                tb.append(r);
            }
            if (da.length == 0) {
                tb.append("<tr><td colspan='9' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
            }
            $('#paging').html(nextPage(pageindex, res.total, 20, 3));
            hideLodingImg();
        } else {
            hideLodingImg();
        }
    })
}

//充值记录
function getDataList_log(pageindex) {
    showLodingImg("0", "50%", "435px", "-150px");
    //获取参数
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    $.post("/account/rechargelog", { "pageindex": pageindex, "time1": startTime, "time2": endTime }, function (res, status) {
        if (status == "success") {
            var da = $.parseJSON(res.data), tb = $(".m-acc-cons table");
            $(".m-acc-cons table tr:not(:first)").remove();
            for (i in da) {
                var r = "<tr><td></td>"
                      + "<td class='u_tl'>" + da[i].publishtime + "</td>"
                      + "<td class='u_tr'>" + formatMoney(da[i]._money, 0, "￥") + "</td>"
                      + "<td class='u_tr s-75b'>" + formatMoney(da[i].accountAmount, 0, "￥") + "</td>"
                      + "<td class='u_tr'>" + da[i].remarks + "</td>"
                      + "<td></td></tr>"
                tb.append(r);
            }
            if (da.length == 0) {
                tb.append("<tr><td colspan='6' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
            }

            $('#paging').html(nextPage(pageindex, res.total, 20, 3));
            hideLodingImg();
        } else {
            hideLodingImg();
        }
    })
}

//提现记录
function getDataList_wdl(pageindex) {
    showLodingImg("0", "50%", "460px", "-150px");
    //获取参数
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();

    $.post("/account/withdrawallog", { "pageindex": pageindex, "time1": startTime, "time2": endTime }, function (res, status) {
        if (status == "success") {
            var da = $.parseJSON(res.data), tb = $(".m-acc-cons table");
            $(".m-acc-cons table tr:not(:first)").remove();
            for (i in da) {
                var r = "<tr><td class='u_nb'></td>"
                      + "<td class='u_tl u_nb'>" + da[i].publishtime + "</td>"
                      + "<td class='u_tr u_nb'>" + formatMoney(da[i]._money, 0, "￥") + "</td>"
                      + "<td class='u_tr u_nb s-75b'>" + formatMoney(da[i].accountAmount, 0, "￥") + "</td>"
                      + "<td class='u_nb'></td>"
                      + "<td class='u_tr u_nb'>" + da[i].cashaccount + "</td>"
                      + "<td class='u_tr u_nb'><span class='u-span26'>" + getState_wdl(da[i]._state) + "</span></td>"
                      + "<td class='u_nb'></td></tr>"
                tb.append(r);
            }
            if (da.length == 0) {
                tb.append("<tr><td colspan='8' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
            }

            $('#paging').html(nextPage(pageindex, res.total, 20, 3));
            hideLodingImg();
        } else {
            hideLodingImg();
        }
    })
}

//资金明细
function getDataList_record(pageindex) {
    showLodingImg("0", "50%", "385px", "-150px");
    //获取参数
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var state = $("#selectStyle").attr("value");
    var t = [2, 4, 6, 12];
    $.post("/account/recordlog", { "pageindex": pageindex, "time1": startTime, "time2": endTime, "state": state, "all": 0 }, function (res, status) {
        if (status == "success") {
            var da = $.parseJSON(res.data), tb = $(".m-account-data2 table");
            $(".m-account-data2 table tr:not(:first)").remove();
            for (i=0;i<da.length;i++) {
                var r = "<tr><td></td>"
					+ "<td class='u_tl'>" + da[i].publishtime + "</td>"
					+ "<td class='u_tl'>" + get_dealState(da[i]._type) + "</td>"
					+ "<td class='u_tr s-75b'>" + (t.indexOf(da[i]._type) > -1 ? "<span class='c-ff'>-" : "<span class='c-75'>") + formatMoney(da[i]._money, 0, "￥") + "</span></td>"
					+ "<td class='u_tr'>" + formatMoney(da[i].accountAmount, 0, "￥") + "</td>"
					+ "<td class='u_tr' style='padding-left:5px;'>" + da[i].remarks + "</td>"
					+ "<td></td></tr>"
                tb.append(r);
            }
            if (da.length == 0) {
                tb.append("<tr><td colspan='7' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
            }

            $('#paging').html(nextPage(pageindex, res.total, 20, 3));
            hideLodingImg();
        } else {
            hideLodingImg();
        }
    })
}

//代金券
function getDataList_vouchers(pageindex, type) {
    showLodingImg("0", "50%", "385px", "-150px");
    var pageSize = 10;

    $.post("/account/vouchersInf", { "pageindex": pageindex, "pageSize": pageSize, "status": type }, function (res, status) {
        if (status == "success") {
            var da = $.parseJSON(res.data), tb = $(".m-acc-cons table");
            $(".m-acc-cons table tr:not(:first)").remove();
            for (i in da) {
                var r = "<tr><td></td>"
					+ "<td class='u_tl'>" + da[i].id + "</td>"
					+ "<td class='u_tl'>" + formatMoney(da[i].amount, 0, "￥") + "</td>"
					+ "<td class='u_tl'>全站</td>"
					+ "<td class='u_tl'>" + da[i].expired + "</td>"
					+ "<td class='u_tl'>" + getVouchersStatus(type, da[i].status) + "</td>"
                    + "<td>" + (da[i].usedTime == null ? "-" : da[i].usedTime) + "</td>"
					+ "<td></td></tr>"
                tb.append(r);
            }
            if (da.length == 0) {
                tb.append("<tr><td colspan='8' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
            }

            $('#paging').html(nextPage(pageindex, res.total, pageSize, 3));
            hideLodingImg();
        } else {
            hideLodingImg();
        }
    })
}
function getVouchersStatus(type, status) {
    var result = "已过期";
    if (type != "2") {
        if (status == 0) {
            result = "可用";
        }
        else {
            result = "已使用";
        }
    }
    return result;
}


//下线推广
function getDataList_off(pageindex) {
    showLodingImg("0", "50%", "700px", "-150px");
    $.post("/account/offlinelog", { "pageindex": pageindex, "group": $("input[type='radio']:checked").attr("k") }, function (res, status) {
        if (status == "success") {
            var da = $.parseJSON(res.data), tb = $(".m-acc-cons table");
            $(".m-acc-cons table tr:not(:first)").remove();
            for (i in da) {
                var r = "<tr><td></td>"
                      + "<td class='u_tl'>" + da[i].dtime + "</td>"
                      + "<td class='u_tl'>" + da[i].wf + "</td>"
                      + "<td class='u_tl'>" + da[i].zc + "</td>"
                      + "<td class='u_tr s-75b'>" + formatMoney(da[i].cny, 0, "￥") + "</td>"
                      + "<td></td></tr>"
                tb.append(r);
            }
            if (da.length == 0) {
                tb.append("<tr><td colspan='6' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
            }

            $('#paging').html(nextPage(pageindex, res.total, 10, 3));
            hideLodingImg();
        } else {
            hideLodingImg();
        }
    })
}

//分销记录
function getDataList_resell(pageindex) {
    showLodingImg("0", "50%", "600px", "-150px");
    $.post("/account/DistributionOperation", { "pageindex": pageindex, "pageSize": "10", "type": $("input[type='radio']:checked").attr("k") }, function (res, status) {
        if (status == "success") {
            var da = $.parseJSON(res.data), tb = $(".m-acc-cons table");
            $(".m-acc-cons table tr:not(:first)").remove();
            for (i in da) {
                var r = "<tr><td></td>"
                      + "<td class='u_tl'>" + da[i].domainName + "</td>"
                      + "<td class='u_tl'>" + getTypeName(da[i].sell) + "</td>"
                      + "<td class='u_tl'>" + da[i].visitCount + "</td>"
                      + "<td class='u_tr s-75b'>" + formatMoney(da[i]._money, 0, "￥") + "</td>"
                      + "<td></td></tr>"
                tb.append(r);
            }
            if (da.length == 0) {
                tb.append("<tr><td colspan='6' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
            }

            $('#paging').html(nextPage(pageindex, res.total, 10, 3));
            hideLodingImg();
        } else {
            hideLodingImg();
        }
    })
}
function getTypeUrl(d, id) {
    switch (d) {
        case 1:
            return "/fast-bid/detail/" + id;
            break;
        case 2:
            return "/hotsale/selling/" + id;
            break;
        case 3:
            return "/bargain/selling/" + id;
            break;
        case 4:
            return "/buy-domains/detail/" + id;
            break;
    }
}
function getTypeName(d) {
    switch (d) {
        case 1:
            return lang.a88;
            break;
        case 2:
            return lang.a87;
            break;
        case 3:
            return lang.a81;
            break;
        case 4:
            return lang.a82;
            break;
    }
}

//登录日志
function getDataList_loginlog(pageindex) {
    showLodingImg("0", "50%", "420px", "-150px");

    $.post("/account/Loglogs", { "pageindex": pageindex }, function (res, status) {
        if (status == "success") {
            var da = $.parseJSON(res.data), tb = $(".m-acc-cons table");
            $(".m-acc-cons table tr:not(:first)").remove();
            for (i in da) {
                var r = "<tr><td></td>"
                      + "<td class='u_tl'>" + da[i].logintime + "</td>"
                      + "<td class='u_tl'>" + da[i].ip + "</td>"
                      + "<td class='u_tl'>" + (da[i].Province == null ? lang.a98 : da[i].Province + da[i].city) + "</td>"
                      + "<td class='u_tr'>" + (da[i]._state ? lang.a96 : lang.a97) + "</td>"
                      + "<td></td></tr>"
                tb.append(r);
            }
            if (da.length == 0) {
                tb.append("<tr><td colspan='6' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
            }

            $('#paging').html(nextPage(pageindex, res.total, 10, 3));
            hideLodingImg();
        } else {
            hideLodingImg();
        }
    })
}

//问题列表
function getDataList_que(pageindex) {
    showLodingImg("0", "50%", "370px", "-150px");

    $.post("/account/queslist", { "pageindex": pageindex }, function (res, status) {
        if (status == "success") {
            var da = $.parseJSON(res.data), tb = $(".m-account-data2 table");
            $(".m-account-data2 table tr:not(:first)").remove();
            for (i in da) {
                var r = "<tr><td></td>"
                      + "<td class='u_tl'><a href='/account/feedback/" + da[i].problemId + "' target='_blank' class='u-span27 u_cur'>" + da[i].title + "</a></td>"
                      + "<td class='u_tl'>" + da[i].questime + "</td>"
                      + "<td class='u_tr'><span " + (da[i]._state ? ">" + lang.a99 : da[i].ifreply ? "class='c-75'>" + lang.a100 : "class='c-ff'>" + lang.a101) + "</span></td>"
                      + "<td></td></tr>"
                tb.append(r);
            }
            if (da.length == 0) {
                tb.append("<tr><td colspan='5' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
            }

            $('#paging').html(nextPage(pageindex, res.total, 20, 3));
            hideLodingImg();
        } else {
            hideLodingImg();
        }
    })
}

//加载cookie  选中相关checkbox
function loadCookie() {
    $(".curPage").attr("checked", false);
    var ckl = getCookie("selectAll");
    if (ckl != null && ckl != "") {
        var reg_c = ckl.match(/(.+?)\&&(.*)/);
        if (href != reg_c[1]) //页面不匹配 return
            return;
        //页面匹配
		
        if (reg_c[2] != null && reg_c[2] != "") { //有值
            var checkList = reg_c[2].substring(0, reg_c[2].length - 1).split(',');
            if (checkList[0].indexOf("all") > -1) { //全部选中
                $(".curLine").attr("checked", true);
                $(".allPage").attr("checked", true);
                $(".curLine").each(function () {
                    if (checkList.indexOf(this.value) > -1) {
                        $(this).attr("checked", false);
                    }
                })
            }
            else { //单选
                $(".curLine").each(function () {
                    if (checkList.indexOf(this.value) > -1) {
                        $(this).attr("checked", true);
                    }
                })
            }
        }
    } else{
        setCookie("selectAll", href + "&&", 2)
	}
}

function get_dealState(d) {
    var result = "other";
    switch (d) {
        case 1:
            result = lang.a102;
            break;
        case 2:
            result = lang.a103;
            break;
        case 3:
            result = lang.a104;
            break;
        case 4:
            result = lang.a105;
            break;
        case 5:
            result = lang.a106;
            break;
        case 6:
            result = lang.a107;
            break;
        case 7:
            result = lang.a108;
            break;
        case 12:
            result = lang.a109;
            break;
        case 13:
            result = lang.a110;
            break;
    }
    return result;
}

function get_state(d) {
    var result = "<font style='color:#ff6f02'>" + lang.a111 + "</font>";//0等待买家同意条款 1等待卖家同意条款 2等待买家付款 3等待卖家转移 4等待买家确认 5交易完成 6交易取消
    switch (d.toString()) {
        case "5":
            result = lang.a112;
            break;
        case "6":
            result = lang.a113;
            break;
    }
    return result;
}

function get_type(d) {
    var result = lang.a82;//0购买  1一口价 2拍卖 3中介 4优质
    switch (d.toString()) {
        case "1":
            result = lang.a87;
            break;
        case "2":
            result = lang.a88;
            break;
        case "3":
            result = lang.a114;
            break;
        case "4":
            result = lang.a81;
            break;
    }
    return result;
}

function get_type(d, b) {
    var result = lang.a82;//0购买  1一口价 2拍卖 3中介 4优质
    switch (d.toString()) {
        case "1":
            result = lang.a87;
            break;
        case "2":
            result = lang.a88;
            break;
        case "3":
            if (b != 0)
                result = lang.a206;
            else
                result = lang.a114;
            break;
        case "4":
            result = lang.a81;
            break;
    }
    return result;
}

// 格式化时间
function formatDate(date, format) {
    if (!date) return;
    if (!format) format = "yyyy-MM-dd";
    switch (typeof date) {
        case "string":
            date = new Date(date.replace(/-/, "/"));
            break;
        case "number":
            date = new Date(date);
            break;
    }
    if (!date instanceof Date) return;
    var dict = {
        "yyyy": date.getFullYear(),
        "M": date.getMonth() + 1,
        "d": date.getDate(),
        "H": date.getHours(),
        "m": date.getMinutes(),
        "s": date.getSeconds(),
        "MM": ("" + (date.getMonth() + 101)).substring(1),
        "dd": ("" + (date.getDate() + 100)).substring(1),
        "HH": ("" + (date.getHours() + 100)).substring(1),
        "mm": ("" + (date.getMinutes() + 100)).substring(1),
        "ss": ("" + (date.getSeconds() + 100)).substring(1)
    };
    return format.replace(/(yyyy|MM?|dd?|HH?|ss?|mm?)/g, function () {
        return dict[arguments[0]];
    });
}

// 返回谈判状态
//state           0正在谈判 1谈判成功 2谈判失败</param>
//lastOffer       最后报价者 0买家 1卖家</param>
//type            0 我参与的议价   1 我收到的议价</param>
function negotiateState(state, lastOffer, type) {
    var result = "";
    switch (state) {
        case "0":
            {
                if (type == 0) {//我是买家
                    result = lastOffer!='0' ? "<font style='color:#ff6f02'>" + lang.a115 + "</font>" : lang.a116;
                }
                else {//我是卖家
                    result = lastOffer == '1' ? lang.a117 : "<font style='color:#ff6f02'>" + lang.a115 + "</font>";
                }
                break;
            }
        case "1":
            {
                result = lang.a118;
                break;
            }
        case "2":
            {
                result = lang.a119;
                break;
            }
    }
    return result;
}

function getState_wdl(o) {
    var result = "";
    switch (o.toString()) {
        case "0":
            result = lang.a146;
            break;
        case "1":
            result = lang.a147;
            break;
        case "2":
            result = lang.a148;
            break;
        case "3":
            result = lang.a112;
            break;
        case "4":
            result = lang.a113;
            break;
    }
    return result;
}

//0等待买家同意条款 1等待卖家同意条款 2等待买家付款 3等待卖家转移 4等待买家确认 5交易完成 6交易取消
//state 状态
//buyer 买家ID
function dealsState(state, buyer) {
    var userId = $("#userId").text().match(/(\d+).*/)[1];
    var result = "";
    switch (state.toString()) {
        case "0": {
            if (userId == buyer.toString())
                result = "<font style='color:#ff6f02'>" + lang.a182 + "</font>";
            else
                result = lang.a183;
            break;
        }
        case "1": {
            if (userId == buyer.toString())
                result = lang.a184;
            else
                result = "<font style='color:#ff6f02'>" + lang.a182 + "</font>";
            break;
        }
        case "2": {
            if (userId == buyer.toString())
                result = "<font style='color:#ff6f02'>" + lang.a185 + "</font>";
            else
                result = lang.a186;
            break;
        }
        case "3": {
            if (userId == buyer.toString())
                result = lang.a187;
            else
                result = "<font style='color:#ff6f02'>" + lang.a188 + "</font>";
            break;
        }
        case "4": {
            if (userId == buyer.toString())
                result = "<font style='color:#ff6f02'>" + lang.a189 + "</font>";
            else
                result = lang.a190;
            break;
        }
        case "5": {
            result = lang.a191;
            break;
        }
        case "6": {
            result = lang.a192;
            break;
        }
    }
    return result;
}

//卖家 我的批量出售   0验证中 1出售中 2交易中 3已出售
function getDataList_pldomain(pageindex, type) {
    showLodingImg("0", "50%", "480px", "-150px");
    $.post("./index.php?c=Account&a=pldomainInf", { "page": pageindex, "type": type }, function (result) {
        if (result.status == 1){
            var da = result.info.lists, tb = $(".m-acc-cons table");
            $(".m-acc-cons table tr:not(:first)").remove();
            switch (type) {
                case "0"://所有
                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl'><input type='hidden' value='" + da[i].bulk_id + "'></td>"
                          + "<td class='u_tl'><a class='u-txa1' title='" + da[i].title + "' href='./index.php?c=Account&a=pldomaindetail&id=" + da[i].bulk_id + "'>" + da[i].title + "</a></td>"
                          + "<td class='u_tr s-75b'>" + formatMoney(da[i].total_price, 0, "￥") + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'><span class='u-span13' title='" + da[i].amount + "'>" + da[i].amount + "</span></td>"
                          + "<td></td>"
                          + "<td class='u_tl'>" + da[i].add_time.substring(0, 10) + "</td>"
                          + "<td class='u_tr'>" + getDataList_state(da[i].status) + "</td>"
                          + "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='10' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                        $(".m-operation4").css("display", "none");
                    } else {
                        $(".m-operation4").css("display", "block");
                    }
                    break;
                case "1": //验证中
                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl'><input type='checkbox' class='u_vt curLine' value='" + da[i].bulk_id + "'></td>"
                          + "<td class='u_tl'><a class='u-txa1' title='" + da[i].title + "' href='./index.php?c=Account&a=pldomaindetail&id=" + da[i].bulk_id + "'>" + da[i].title + "</a></td>"
                          + "<td class='u_tr s-75b'>" + formatMoney(da[i].total_price, 0, "￥") + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'><span class='u-span13' title='" + da[i].amount + "'>" + da[i].amount + "</span></td>"
                          + "<td></td>"
                          + "<td class='u_tl'>" + da[i].add_time.substring(0, 10) + "</td>"
                          + "<td class='u_tr'><a class='u-abtn8 sold_out'>" + lang.a85 + "</a>&nbsp;<a class='u-abtn8 setting' href='./index.php?c=Account&a=addDomainCheck_pl&id="+da[i].bulk_id+"'>" + lang.a80 + "</a></td>"
                          + "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='10' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                        $(".m-operation4").css("display", "none");
                    } else {
                        $(".m-operation4").css("display", "block");
                    }
                    break;
                case "2": //出售中                       
                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl'><input type='hidden' value='" + da[i].bulk_id + "'></td>"
                          + "<td class='u_tl'><a class='u-txa1' title='" + da[i].title + "' href='./index.php?c=Account&a=pldomaindetail&id=" + da[i].bulk_id + "'>" + da[i].title + "</a></td>"
                          + "<td class='u_tr s-75b'>" + formatMoney(da[i].total_price, 0, "￥") + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'><span class='u-span13' title='" + da[i].amount + "'>" + da[i].amount + "</span></td>"
                          + "<td></td>"
                          + "<td class='u_tl'>" + da[i].add_time.substring(0, 10) + "</td>"
                          + "<td class='u_tr'><a class='u-abtn8 sold_out'>" + lang.a85 + "</a>&nbsp;<a class='u-abtn8 setting' href='./index.php?c=Account&a=addDomainCheck_pl&id="+da[i].bulk_id+"'>" + lang.a80 + "</a>&nbsp;</td>"
//                          + "<td class='u_tr'><a class='u-abtn8 sold_out'>" + lang.a85 + "</a>&nbsp;&nbsp;<a class='u-abtn8 setting' href='/account/pldomainset/" + da[i].bulk_id + "'>" + lang.a80 + "</a></td>"
                          + "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='10' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                        $(".m-operation4").css("display", "none");
                    } else {
                        $(".m-operation4").css("display", "block");
                    }
                    break;
                case "3": //已出售                      
                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl'><input type='hidden' value='" + da[i].bulk_id + "'></td>"
                          + "<td class='u_tl'><a class='u-txa1' title='" + da[i].title + "' href='./index.php?c=Account&a=pldomaindetail&id=" + da[i].bulk_id + "'>" + da[i].title + "</a></td>"
                          + "<td class='u_tr s-75b'>" + formatMoney(da[i].total_price, 0, "￥") + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'><span class='u-span13' title='" + da[i].amount + "'>" + da[i].amount + "</span></td>"
                          + "<td></td>"
                          + "<td class='u_tl'>" + da[i].add_time.substring(0, 10) + "</td>"
                          + "<td></td></tr>"
                        tb.append(r);
                    }
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='10' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                        $(".m-operation4").css("display", "none");
                    } else {
                        $(".m-operation4").css("display", "block");
                    }
                    break;
                case "4": //待展示                       
                    for (i in da) {
                        var r = "<tr><td></td>"
                          + "<td class='u_tl'><input type='hidden' value='" + da[i].bulk_id + "'></td>"
                          + "<td class='u_tl'><a class='u-txa1' title='" + da[i].title + "' href='./index.php?c=Account&a=pldomaindetail&id=" + da[i].bulk_id + "'>" + da[i].title + "</a></td>"
                          + "<td class='u_tr s-75b' data-money='" + da[i]._money + "'>" + formatMoney(da[i]._money, 0, "￥") + "</td>"
                          + "<td></td>"
                          + "<td class='u_tl'><span class='u-span13' title='" + da[i].count + "'>" + da[i].count + "</span></td>"
                          + "<td></td>"
                          + "<td class='u_tl'>" + da[i].add_time.substring(0, 10) + "</td>"
                          + "<td class='u_tr'><a class='u-abtn8 sold_out u-abtn-s'>" + lang.a85 + "</a>&nbsp;</td>"
                          + "<td></td></tr>"
                        tb.append(r);
                    }
                    //<a class='u-abtn-s u-abtn8' href='/account/pldomainset/" + da[i].bulk_id + "'>设置出售</a>
                    if (da.length == 0) {
                        tb.append("<tr><td colspan='10' class='u_nb u_tc u_norecord'>" + lang.a77 + "</td></tr>");
                        $(".m-operation4").css("display", "none");
                    } else {
                        $(".m-operation4").css("display", "block");
                    }
                    break;
            }
            $('#paging').html(nextPage(pageindex, result.info.total, 20, 3));
            hideLodingImg();
            loadCookie();
        } else {
            hideLodingImg();
        }
    })
}

function getDataList_state(j) {
    switch (j) {
        case 0:
            return lang.a84;
        case 1:
            return lang.a201;
        case 2:
            return lang.a202;
        case 3:
            return lang.a203;
        case 4:
            return lang.a204;
    }
}