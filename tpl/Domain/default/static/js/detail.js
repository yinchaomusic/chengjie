﻿$(document).ready(function () {
    $(".u_prise").keyup(function () { clearNoNum1(this); })
    
    var buyUrl = "/buyitnowdomain/";
    var bjUrl = "./index.php?c=Buydomains&a=quote";
	if (window.location.href.indexOf("c=Hotsale&a=selling") > 0) {
        buyUrl = "./index.php?c=Hotsale&a=buy";
        bjUrl = "/bargainbuydomain/";
    }
    if (/pldomain\/selling/i.test(location.href)) {
        buyUrl = "/pldomainbuy/";
    }

    //报价
    $("#bj").click(function () {
        var num = $(".u_prise").val();

        if (num == "" || num == "0") {
            showDialog(lang.a161);
            return false;
        }
        //标价
        var bij = Number($(".u-txr3").text().replace(/[$￥€,]/g, ""));
        if (Number(num) > bij || Number(num) < bij / 2) {
            showDialog(lang.a165);
            return false;
        }

        if (!$("#agree").attr("checked")) {
            showDialog(lang.a194);
            return false;
        }

        $.post(bjUrl +'&id='+ $("#domainid").val() + "&money=" + num, function (result) {
			if (result.status == 1){
				if(result.info == '1'){
					showDialog(lang.a169, './index.php?c=Account&a=negotiation');
				}else{
					location.href = './index.php?c=Account&a=negotiation';
					return false;
				}
			}else{
				if(result.info == '1'){
					location.href = './index.php?c=Login';					
				}else{
					showDialog(result.info);
				}
			}
        });
    });

    $("#buy").click(function () {
        if (!$("#agree").attr("checked")) {
            showDialog(lang.a194);
            return false;
        }
        if (confirm(lang.a163 + $("#bzj").val())) {
            $.post(buyUrl +'&id=' + $("#domainid").val(), function (result) {
                if (result.status == 1){
					if(result.info == '1'){
						showDialog(lang.a169, './index.php?c=Account&a=negotiation');
					}else{
						location.href = './index.php?c=Account&a=deals_dealing';
						return false;
					}
				}else{
					if(result.info == '1'){
						location.href = './index.php?c=Login';					
					}else{
						showDialog(result.info);
					}
				}
            });
        }
    });

    $(".f-img-i7").click(function () {
        if ($("#userId").length < 1) {
            setCookie("backUrl", location.pathname, 2);
            location.href = "./index.php?c=Login";
        }
    })
    if ($("#userId").length>0) {
        $(".f-img-i7").zclip({
            path: '/images/account/ZeroClipboard.swf',
            copy: function () { return document.title + "\r\n" + $(this).attr("k"); }
        });
        $(".zclip").attr("title", "点击复制分销链接：" + $(".f-img-i7").attr('k'));
    }

    $("#qqtalk").click(function () {
        if ($("#userId").length == 0) {
            setCookie("backUrl", location.pathname, 2);
            showDialog(lang.a66,"./index.php?c=Login");
            return false;
        }
    })
});