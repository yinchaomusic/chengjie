﻿//JS国际化--------------------------------------------------------------------------------------
var langStr = "zh-cn";
var buydomain_b = "13px";
var buyweb_b = "29px";
var account_b = "30px";
var str_script = "<script type='text/javascript' src='tpl/Domain/default/static/js/plugs_zh-cn.js'></script>";
//alert(str_script);
if (langStr = getCookie("lang")) {
    switch (langStr) {
        case "zh-hk": {
            str_script = "<script type='text/javascript' src='tpl/Domain/default/static/js/plugs_zh-hk.js'></script>";
            break;
        }
        case "en-us": {
            buydomain_b = "-4px";
			buyweb_b = "-4px";
            account_b = "25px";
            $("li.nav_fa a:first").css("background-position", buydomain_b + " -138px");
			 $("li.nav_fa2 a:first").css("background-position", buyweb_b + " -138px");
            $(".f-img-account").css("background-position", account_b + " -139px");
            str_script = "<script type='text/javascript' src='tpl/Domain/default/static/js/plugs_en-us.js'></script>";
            break;
        }
    }
}
document.write(str_script);
//JS国际化----------------------------------------End-------------------------------------------
if(/(iphone|ipod|android|windows phone)/.test(navigator.userAgent.toLowerCase())){
	alert('手机端页面正在开发测试中！当前访问的是PC端页面，使用PC浏览器访问可获得最佳效果。');
}
//渐变
$(".m-animate-a a,#bidding a,#bid-data a").live("mouseenter", function () {
    $(this).find("h4").stop(true,true).animate({ "backgroundColor" : "#fe8431","color":"#fff"},400);
    $(this).find("div").stop(true,true).animate({"borderColor":"#fe8431"},400);
}).live("mouseleave",function () {
    $(this).find("h4").stop(true,true).css({"background":"#fbfbfb","color":"#fe8431"});
    $(this).find("div").stop(true,true).css("border-color","#fff");
});
$("#bid-data .operbid").live("mouseenter",function(){
    $(this).find("a.s-bg-fb").stop(true,true).animate({ "backgroundColor" : "#fe8431","color":"#fff"},400);
    $(this).find("div").stop(true,true).animate({"borderColor":"#fe8431"},400);
}).live("mouseleave",function(){
    $(this).find("a.s-bg-fb").stop(true,true).css({"background":"#fbfbfb","color":"#fe8431"});
    $(this).find("div").stop(true,true).css("border-color","#fff");
});
//dialog 提示，关闭后要跳转的链接
function showDialog(msg, url) {
    $("#pop1_bg").css("display","block");
    $("#pop_ts").css({ "display": "block","opacity":0}).animate({ opacity:1,left:"50%", top: $(window).height() / 2 + $(window).scrollTop() - 150, "margin-left": "-280px" }, 200)
    $("#ts").html(msg).attr("k",url);
}
function closeDialog() {
    $("#pop1_bg").css("display", "none");
    $("#pop_ts").css("display", "none").animate({ "left": 0, "top": 0 }, 300)
    var url = $("#ts").attr("k");
    if (url)
        location.href = url;
}

//获取Y轴 background-position 数值(不含px和-号)  兼容IE
function getPos_y(obj) {
    var y = "0";
    try { y = obj.css("background-position").match(/.+?[-\s](\d+)px/)[1] }
    catch (e) { y = obj.css("background-position-y").replace("-").replace("px"); };
    return y;
}
//服务器时间
var serverTime = "";
var updTime = document.getElementById("mytime");
function getTime() {
    if (updTime) {
        //new Date无法转换 2014-01-01 01:01:01， - 替换成 / 之后转换正常
        serverTime = new Date(updTime.innerHTML.replace(/-/g, "/"));
        serverTime.setSeconds(serverTime.getSeconds() + 1);
        updTime.innerHTML = getTimeStr(serverTime);
    }
}
//输出时间格式  2014-01-01 00:01:01
function getTimeStr(t) {
    var result = t.getFullYear() + "-" + bl(t.getMonth() + 1) + "-" + bl(t.getDate()) + " " + bl(t.getHours()) + ":" + bl(t.getMinutes()) + ":" + bl(t.getSeconds());
    return result;
}
//补零
function bl(n) {
    return n.toString().length > 1 ? n : "0" + n;
}
//跳转
if (!location.href.match(/hotsale\/sold|bargain\/sold/)) {
    $("ol.fi div a,ol.fiv div a").live("click", function (event) {
        event.stopPropagation();
    })
    $("ol.fi div,ol.fiv div").live("click", function () {
        window.open($(this).find("a.ym").attr("href"));
    })
}

$(document).ready(function () {
    //对话框关闭
    $(".dialog_t span").live("click", function () {
        closeDialog();
    })

    var top = 0;
    switch (getCookie("lang")) {
        case "zh-hk": {
            $(".f-b-rlt  p").eq(2).remove();
            top = -46;
            break;
        }
        case "en-us": {
            $(".f-b-rlt  p").eq(1).remove();
            top = -23;
            break;
        }
        default: {
            $(".f-b-rlt  p").eq(0).remove();
            break;
        }
    }
    $(".f-img-lan").css("background-position", "0 " + top + "px");

	//头部固定
    $(window).scroll(function () {
        if ($(window).scrollTop() >= 1) {
            $(".s-bg-topa").addClass("fixedNav");
        } else {
            $(".s-bg-topa").removeClass("fixedNav");
        }
    });

    //输入框提示         placeholder
    $("input[type='text'],textarea").each(function () {
        var ph = $(this).attr("ph");
        if (!ph) return;
        if ($(this).val() == "") {
            $(this).val(ph).css("color", "#d8d8d8");
        } else{
            $(this).css("color", "#000");
		}
    })
    
    //input获得焦点
    $("input[type='text'],input[type='password'],textarea").live("focus", function () {
        if ($(this).hasClass("nofocus")){
            return false;}
       if($(this).parent().hasClass('tra')){
			 return false;
        }
         if($(this).hasClass("u-ipt-txta")){
            $(this).parent().css({"border":"2px solid #FFA200","border-right":"0px"});
        }else{
            $(this).parent().css("border","1px solid #FFA200");
        }
        var ph = this.getAttribute("ph");
        if (!ph) return false;
        ph = ph.replace(/\r/g, "")
        if (this.value.replace(/\r/g, "") == ph) {
            this.value = "";
            $(this).css("color", "#000");
        }
    });
    // 所有的报价
    $(".u_prise").keyup(function(){
        clearNoNum1(this);
    });

    //input失去焦点
    $("input[type='text'],input[type='password'],textarea").live("focusout", function () {
        if ($(this).hasClass("nofocus")){
            return false;}
        if($(this).parent().hasClass('tra')){
			 return false;
        }
        if($(this).hasClass("u-ipt-txta")){
            $(this).parent().css({"border":"2px solid ##6cbfdd","border-right":"0px"});
        }else{
            $(this).parent().css("border","1px solid #ebebeb");
        }
        var ph = this.getAttribute("ph");
        if (!ph) return false;
        ph = ph.replace(/\r/g, "")
        if (this.value.replace(/\r/g, "") == ph || this.value == "") {
            $(this).css("color", "#d8d8d8");
            this.value = ph;
        }
    })


    $("input.input_text").focus(function () {
        $(this).parent().css("border", "0");
    })
    $("input.input_text").live("focusout",function () {
        $(this).parent().css("border", "0");
    })

    //计时器
    setInterval(function () {
        //服务器时间
        getTime();

        //竞价倒计时
        $(".countdown").each(function () {
            var sp = $(this).find("strong");
            var h = parseInt(sp[0].innerHTML);
            var m = parseInt(sp[1].innerHTML);
            var s = parseInt(sp[2].innerHTML);
            if (s == 0) {
                if (h == 0 && m == 0) {
                    return;
                }
                s = 59;
                if (m == 0) {
                    m = 59;
                    h -= 1;
                }
                else
                    m -= 1;

            } else {
                s -= 1;
            }
            sp[0].innerHTML= bl(h);
            sp[1].innerHTML = bl(m);
            sp[2].innerHTML = bl(s);
        });

    }, 1000)
	var searchAnd = location.search.split('&');
    var menu_href = location.pathname+searchAnd[0];
    if (menu_href == "/")
        $(".g-nav a:first").addClass("t");
    else {
		menu_href+= '&a=index';
        switch (menu_href) {
            case "/index.php?c=Fastbid&a=index":
            case "/index.php?c=Hotsale&a=index":
            case "/index.php?c=Bargain&a=index":
                menu_href = "/index.php?c=Buydomains&a=index";
                break;
        }
        if (menu_href == "bargain" || menu_href == "sale")
            $(".g-nav  a[href*='" + menu_href + "']").eq(1).addClass("t");
        else
            $(".g-nav  a[href*='" + menu_href + "']").eq(0).addClass("t");
        //switch (menu_href) {
        //    case "bid-end":
        //    case "bidend":
        //        menu_href = "fast-bid";
        //        break;
        //}
        //$(".g-nav  a[href*='" + menu_href + "']").addClass("t");
    }
    //语言切换
    $(document.body).click(function () {
        $(".f-a-rlt").hide();
        $(".f-b-rlt").stop(true,true).hide();
    });
    //语言切换
    $(".f-img-lan").click(function(event){
        $(this).next().stop(true,true).slideDown("fast");
        event.stopPropagation();
    });
    $(".f-b-rlt p").click(function(event){
        var oper=$(this).attr("kvalue");
        lang_cut(oper);
        event.stopPropagation();
    });
//-----------------------------------------登录账户下拉-----------------------------------
var stopTimer={},stopTimer2={},stopBob=0;
$("#login,#account").mouseenter(function(){
    if (location.href.indexOf("account/index")>-1) {
        return;
    }
    if(stopBob){
        clearTimeout(stopTimer2);
        stopBob=0;
    }else{
        if($(this).hasClass("active")){
            changeStyle(this,".s-bg-login","z_ta");
            changeStyle(".s-bg-account");
        }else{
            changeStyle(this,".s-bg-account","f-img-accounth");
            changeStyle(".s-bg-login");
        }
    }
}).mouseleave(function(){
    if($(this).hasClass("active")){
        setTimer("#login",".s-bg-login","z_ta");
    }else{
        setTimer("#account",".s-bg-account","f-img-accounth");
    }
});
$("#s-bg-account").mouseenter(function(){
    clearTimeout(stopTimer);
}).mouseleave(function(){
    stopBob=1;
    stopTimer2=setTimeout(function(){
        changeStyle("#s-bg-account");
        $("#account").removeClass("f-img-accounth");
        stopBob=0;
    },100);
});
$("#s-bg-login").mouseenter(function(){
    clearTimeout(stopTimer);
}).mouseleave(function(){
    stopBob=1;
    stopTimer2=setTimeout(function(){
        changeStyle("#s-bg-login");
        $("#login").removeClass("z_ta");
        stopBob=0;
    },100);
});
$(".g-close").click(function(){
    $(this).parent().hasClass("g-login")?closeFrame("login","s-bg-login","z_ta"):closeFrame("account","s-bg-account","f-img-accounth");
});
function closeFrame(){
    $("#"+arguments[0]).removeClass(arguments[2]);
    $("."+arguments[1]).stop(true,true).slideUp("fast");
}
function changeStyle(){
    if(arguments.length<2){
        $(arguments[0]).stop(true,true).slideUp("fast");
    }else{
        $(arguments[0]).addClass(arguments[2]);
        $(arguments[1]).stop(true,true).slideDown("fast");  
    }
}
function setTimer(obj,str1,str2){
    stopTimer=setTimeout(function(){
        $(str1).stop(true,true).slideUp("fast");
        $(obj).removeClass(str2);
    },200);
}

//买域名下拉
$("li.nav_fa").mouseover(function () {
    $("li.nav_fa a:first").css({ "color": "#151e26", "background-color": "#e8e8e8", "background-position": buydomain_b+" -189px" })
    $("li.nav_fa ul").stop().slideDown("fast");
}).mouseout(function () {
    $("li.nav_fa a:first").css({ "color": "#fff", "background-color": "", "background-position": buydomain_b+" -138px" })
    $("li.nav_fa ul").stop().slideUp("fast");
})
$("li.nav_fa2").mouseover(function () {
    $("li.nav_fa2 a:first").css({ "color": "#151e26", "background-color": "#e8e8e8", "background-position": buyweb_b+" -189px" })
    $("li.nav_fa2 ul").stop().slideDown("fast");
}).mouseout(function () {
    $("li.nav_fa2 a:first").css({ "color": "#fff", "background-color": "", "background-position": buyweb_b+" -138px" })
    $("li.nav_fa2 ul").stop().slideUp("fast");
})

//---------------------------------------------------------------------------------------------------
    //搜索下拉
   $(".u-slt-a").click(function(event){
        $(".f-a-rlt").css("display")=="none"?$(".f-a-rlt").css("display","block"):$(".f-a-rlt").css("display","none");
        event.stopPropagation();
    });
    $(".f-a-rlt p").click(function(){
        $(this).parent().parent().find(".u-slt-a").html($(this).text());
        $(".u-slt-a").attr("ym",$(this).attr("ym"));
        $(this).parent().hide();
    });
    //点击按钮搜索
    var searchValue = "";
    $(".u-btn-search").click(function () {
        var keyword=$(".u-ipt-txta").val();
        if (keyword == $(".u-ipt-txta").attr("ph"))
            keyword="";
        var cla = $(".u-slt-a").attr("ym");
        var href = "./index.php?c=Whois&a=index&keyword=";
        if (cla == "b") {
            if (keyword.indexOf(".") <0)
                keyword+=".com";
            window.open(href + keyword.replace(/\s*/g,""));
        } else {
            location.href = "./index.php?c=Buydomains&a=index&keyword=" + keyword.replace(/\s*/g, "");
        }
    });
	$(".u-ipt-txta").focusout(function () {
        $(".f-search-rlt").css("display", "none");
        $(this).parent().css({"border":"2px solid #FFA200","border-right":"0"});
    }).focus(function () {
        $(this).parent().css({"border":"2px solid #FFA200","border-right":"0"});
        var cla =$(".u-slt-a").attr("ym");
        if (cla == "b") return false;
        if ($(this).val() != "" && $(this).val()!=$(this).attr("ph")) {
            $(".f-search-rlt").css("display", "block");
        } else {
            $(".f-search-rlt").css("display", "none");
        }
    });
    
	var keyupTime;
	$(".u-ipt-txta").keyup(function (event) {
	    clearTimeout(keyupTime);
	    var cla = $(".u-slt-a").attr("ym");
	    keyupTime = setTimeout(function () {
	        if (cla == "b") {
	            if (event.keyCode == 13)
	                $(".u-btn-search").click();
	        } else {
	            var num = $(".f-search-rlt > p").length;
	            switch (event.keyCode) {
	                case 40: {
	                    var idx = $(".f-search-rlt>p[class='t']").index();
	                    if (idx + 2 > num) {
	                        idx = -1;
	                    }
	                    var obj = $(".f-search-rlt p").eq(++idx);
	                    changeClass(obj);
	                    $(".u-ipt-txta").val(obj.text());
	                    break;
	                }
	                case 38: {
	                    var idx = $(".f-search-rlt>p[class='t']").index();
	                    if (idx < 1) {
	                        idx = num;
	                    }
	                    var obj = $(".f-search-rlt> p").eq(--idx);
	                    changeClass(obj);
	                    $(".u-ipt-txta").val(obj.text());
	                    break;
	                }
	                case 13: {// Enter
	                    $(".u-btn-search").click();
	                    break;
	                }
	                default: {// 其它按键
	                    if ($(".u-ipt-txta").val() != searchValue && $(".u-ipt-txta").val() != "") {
	                        //获取数据
	                        getSearchList($(".u-ipt-txta").val());
	                        $(".f-search-rlt").css("display", "block");
	                    }
	                    searchValue = $(".u-ipt-txta").val();
	                }
	            }
	        };
	        clearTimeout(keyupTime);
	    }, 300);
	});

    $(".f-search-rlt>p").live("mouseover",function(){
        changeClass($(this));
    });
    $(".f-search-rlt>p").live("mousedown",function(){
        $(".u-ipt-txta").val($(this).text());
        $(".f-search-rlt").css("display","none");
    });
    //切换背景色 obj是jquery对象
    function changeClass(obj) {
        $(".f-search-rlt p").each(function () {
            $(this).removeClass("t");
        })
        obj.addClass("t");
    } 
    function getSearchList(keyword){
        $.post("/toptao", { "keyword": keyword }, function (data) {
           
            if (data) {
                var json = jQuery.parseJSON(data);
                    var arr_data=[];
                    for (i in json) {
                        var str = "<p>" + json[i].domainName + "</p>";
                        arr_data.push(str);
                    }
                    $(".f-search-rlt").html(arr_data);
                }
        });
    }
//-----------------------------------------------------------------------------------
//会员登录
$("#email-id").blur(function(){
    var email_id = $(this).val();
    if (email_id != "" && email_id == $(this).attr("ph")) email_id = "";
    if(!email_id || email_id==""){
        $(".ts_denglu").html(lang.a5);
    }
}).focus(function(){
    $(".ts_denglu").html("");
});
$("#password").blur(function(){ //密码输入框
    var password=$(this).val();
    if(!password || password==""){
        $(".ts_denglu").html(lang.a6);
    }else if(password.length<5){
        $(".ts_denglu").html(lang.a3);
    }
}).focus(function(){
    $(".ts_denglu").html("");
}).keyup(function (event) {
    if (event.keyCode == 13) {
        $("#deng-lu").click();
    }
});
  $("#deng-lu").click(function () {
        var email_id = $("#email-id").val();
        var password = $("#password").val();
        if (email_id && password && email_id != "" && password != "") {
            // 0成功 1:未激活 2：被停用 3:账号或密码错误 4:错误次数过多:15分钟后再试 5:密码错误，您还有两次输入机会
            $.post("/index.php?c=Login&a=check",{"account":email_id,"pwd":password},function(result){
				result = $.parseJSON(result);
				if(result.error == 0){
					window.location.href = "/index.php?c=Account&a=index";
				}else{
					alert(result.msg);
					$(".ts_denglu").html(result.msg);
				}
            });
        } else {
            $(".ts_denglu").html(lang.a18);
        }
    });
//---------------------------------去顶部、去底部----------------------------------------
    //语言切换
	function lang_cut(value) {
	    var h = 24;
	    setCookie("lang", value, 24);
	    location = location;
	}
	var p_ini_w = 980;
    //页面可见区域高、可见区域宽、页面body高度 
	var pHeight = $(window).height();
	var pWidth = $(window).width();

    //指定位置
	$(".gototop_wrap").css("top", 0.5 * pHeight + "px").css("display","block");

    //显示 微信二维码
	$(".showWechat,.phone").mouseenter(function () {
	    $(this).children().stop(true,true).show(200);
	}).mouseleave(function () {
	    $(this).children().stop(true, true).hide(300);
	})

	function startScroll() {
	    var clientHeight = $(window).height();
	    var scrollTop = $(document).scrollTop()
	    if (scrollTop > 0) {
	        $(".gotoTop").css("display", "block");
	    } else {
	        $(".gotoTop").css("display", "none");
	    }

	    if ((clientHeight + scrollTop) / $(document).height() > 0.95) {
	        $(".gotoBottom").css("display", "none");
	    } else {
	        $(".gotoBottom").css("display", "block");
	    }
	}
	startScroll();
    //滚动
	$(window).scroll(function () {
	    startScroll();
	})

    //点击
	$(".gotoTop").click(function () {
	    $("html,body").animate({ scrollTop: 0 }, 250);
	})
	$(".gotoBottom").click(function () {
	    $("html,body").animate({ scrollTop: $(document).height() - pHeight }, 250);
	})
    //---------------------------------去顶部、去底部 END------------------------------------ 
});


function laLock(obj) {
    if (obj.attr("k") == "1") {
        obj.html(obj.html().toLocaleLowerCase());
        obj.attr("k", "0");
    }
    else {
        obj.html(obj.html().toLocaleUpperCase());
        obj.attr("k", "1");
    }
}




//lodingImg
function showLodingImg(top, left, mtop, mleft) {
    hideLodingImg();
    var h = "<div id='lodingImg'><img src='tpl/Domain/default/static/images/index/loading.gif'/></div>";
    $(document.body).append(h);
    $("#lodingImg").css({ "top": top, "left": left, "margin-top": mtop, "margin-left": mleft });
}
function hideLodingImg() {
    $("#lodingImg").remove();
}


function getUrl(value) {
    // 1拍卖域名  2一口价域名  3优质域名 4其他
    var result = "/procurement?d=" + value.domain;
	console.log(value);
    switch (value.type) {
		case "1":
            result = "./index.php?c=Hotsale&a=selling&id=" + value.domain_id;
			break;
        case "2":
            result = "./index.php?c=Fastbid&a=detail&id=" + value.domain_id;
            break;
        case '3':
			result = "./index.php?c=Bargain&a=selling&id=" + value.domain_id;
			break;
        case "0":
            result = "./index.php?c=Buydomains&a=detail&id=" + value.domain_id;
            break;
    }
    return result;
}

//数字转货币（数值 , 小数位数 , ￥|$|€ , 千位分隔符[默认,] , 小数分隔符[默认.]）
function formatMoney(number, places, symbol, thousand, decimal) {
    number = number || 0;
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    symbol = symbol !== undefined ? symbol : "$";
    thousand = thousand || ",";
    decimal = decimal || ".";
    var negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
}

//分页  (当前页,总条数,每页显示条数,当前页前后显示页数)
function nextPage(pageIndex, total, pageSize, temp) {
    var ReturnHtml = "";
    pageIndex = parseInt(pageIndex);
    total = parseInt(total);
    pageSize = parseInt(pageSize);
    temp = parseInt(temp);
    if (total == 0)
        return ReturnHtml;
    //计算总页数
    var pageCount = 0;
    if (total % pageSize == 0)
        pageCount = parseInt(total / pageSize);
    else
        pageCount = parseInt(total / pageSize) + 1;

    //计算当前位置
    ReturnHtml += "<div class='u-fl'><span class='u_fw'>" + total + "</span>" + lang.a120 + "<span class='u_fw'>" + pageCount + "</span>" + lang.a121 + "</div><div class='u-fr'>"

    //1    上一页 首页
    if (pageIndex == 1)
        ReturnHtml += "<a class='pa_disabled' title='" + lang.a122 + "' href='javascript:void(0)'></a>";
    else
        ReturnHtml += "<a class='pa_disabled' title=" + lang.a122 + " k='" + (pageIndex - 1) + "' href='javascript:void(0)'></a><a href='javascript:void(0)' k='1' class='s-def'>1</a>";

    //2    ...
    if (pageIndex > temp + 2)
        ReturnHtml += "<a href='javascript:void(0)' k='"+(pageIndex-4)+"' class='s-def'>...</a>";
    //3    本页前页码
    var frontPage = "";
    for (var i = pageIndex - 1; i > pageIndex - (temp + 1) && i > 1; i--)
    {
        frontPage = "<a href='javascript:void(0)' k='"+i+"' class='s-def'>"+i+"</a>" + frontPage;
    }
    ReturnHtml += frontPage;
    
    //4    本页页码
    ReturnHtml += "<a href='javascript:void(0)' class='s-def t'>"+pageIndex+"</a>";

    //5    页后页码
    for (var i = pageIndex + 1; i < pageIndex + (temp + 1) && i < pageCount; i++)
        ReturnHtml += "<a href='javascript:void(0)' k='"+i+"' class='s-def'>"+i+"</a>";

        //6    ...
    if (pageIndex + temp + 1 < pageCount)
        ReturnHtml += "<a href='javascript:void(0)' k='" + (pageIndex + temp + 1) + "' class='s-def'>...</a>";

        //7    尾页 下一页
    if (pageIndex == pageCount)
        ReturnHtml += "<a class='pg_enabled' title='" + lang.a123 + "' href='javascript:void(0)'></a>";
    else
        ReturnHtml += "<a href='javascript:void(0)' k='"+pageCount+"' class='s-def'>"+pageCount+"</a><a class='pg_enabled' title='" + lang.a123 + "' href='javascript:void(0)'  k='" + (pageIndex + 1) + "'></a>";
    ReturnHtml += "</div><div class='u-cls'></div>";

    return ReturnHtml;
}

//只允许输入[数字]
function clearNoNum1(obj) {
    //先把非数字的都替换掉
    obj.value = obj.value.replace(/[^\d]/g, "");
    //必须保证第一个为数字而不是0
    obj.value = obj.value.replace(/^[0]/g, "");
}

//读取cookies
function getCookie(name) {
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg))
        return unescape(arr[2]);
    else
        return null;
}
//设置cookie(cookie名 , 值 , 小时)
function setCookie(name, value, h) {
    var exp = new Date();
    exp.setTime(exp.getTime() + h * 60 * 60 * 1000);
    document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString() + ";path=/";
}
//删除cookie        
function delCookie(name) {
    document.cookie = name + "=;expires=" + (new Date(0)).toGMTString() + ";path=/";
}
