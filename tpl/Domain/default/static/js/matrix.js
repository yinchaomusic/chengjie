
$(document).ready(function(){
	// === Sidebar navigation === //

	$('.submenu > a').click(function(e)
	{

		//e.preventDefault();
		//alert($(this).attr("href"));
		//console.log($(this).attr("href"));
		if($(this).attr("href") =="#" ){
			e.preventDefault();
		}
		var submenu = $(this).siblings('ul');
		var li = $(this).parents('li');
		var submenus = $('#sidebar li.submenu ul');
		var submenus_parents = $('#sidebar li.submenu');

		if(li.hasClass('open'))
		{

		    submenu.slideUp("slow",function(){
				li.removeClass('open');
			});
			//li.removeClass('open');
		} else
		{
		 submenus.slideUp();
		 submenu.slideDown();

			submenus_parents.removeClass('open');
			li.addClass('open');
		}
	});

	var ul = $('#sidebar > ul');
	var sidebar = $('#sidebar');
	$('#sidebar .submenu_bar').on("click",function(e)
	{
		e.preventDefault();
		var $showmenu=$(".breadcrumb  .iconfont",this);
		if( $showmenu.hasClass("submenu_d")){
			 $showmenu.removeClass("submenu_d").addClass("submenu_up").html("&#xe62d;");
			 sidebar.addClass('open');
			 ul.show();
		}else{

			 $showmenu.removeClass("submenu_up").addClass("submenu_d").html("&#xe62c;");
			 sidebar.removeClass('open');
             ul.hide();
		}

	});

	// === Resize window related === //

	$(window).resize(function()
	{

		if($(window).width() < 768)
		{
			if($("#sidebar").hasClass("open")){
				return false;
			}
			ul.css({'display':'none'});
		}
		if($(window).width() > 768 && !$(".container").hasClass("jkxq"))
		{
			ul.css({'display':'block'});
		}
	});

	if($(window).width() < 768 )
	{
		ul.css({'display':'none'});
	}

	if($(window).width() > 768 && !$(".container").hasClass("jkxq") )
	{
		ul.css({'display':'block'});
	}



});

