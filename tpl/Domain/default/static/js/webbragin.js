$("#m-style").css("border-bottom","0");
$(".m-keyword-a").removeClass("u_bb0");
$(".m-keyword-a").addClass("u_bb0");
$("#m-up-down .down").removeClass("s-bg-fb").addClass("s-bg-white");
$("#g_sh").slideDown(300);
$("#m-up-down .down").hide();
$("#m-up-down .up").show();
$("html,body").animate({ scrollTop: 148 }, 250);
showorhide();
// --------展开收起------------
$("#m-up-down .down,#m-up-down .up").click(function(){
       if($(this).hasClass("up")){
            $("#g_sh").slideUp(300);
            $(".m-keyword-a").removeClass("u_bb0");
            $("#m-up-down .down").removeClass("s-bg-white").addClass("s-bg-fb");
            $("#m-up-down .up").hide();
            $("#m-up-down .down").show();
       }else{
            $(".m-keyword-a").addClass("u_bb0");
            $("#m-up-down .down").removeClass("s-bg-fb").addClass("s-bg-white");
            $("#g_sh").slideDown(300);
            $("#m-up-down .down").hide();
            $("#m-up-down .up").show();
            $("html,body").animate({ scrollTop: 148 }, 250);
       }
});
// --------------------------------------------
var input = $(".f-range-sure input");

$(".u_b4 input").keyup(function () {
    if (event.keyCode == 13) {
        $(".u-btn1").click();
    }
})

$("#domian-len").slider({animate:true,range:true,min:0,max:8,step:1,values:[0,8],stop:function(event,ui){
    ui.values[0]>7?$(input[0]).val(""):$(input[0]).val(ui.values[0]);
    ui.values[1]>7?$(input[1]).val(""):$(input[1]).val(ui.values[1]);
    slideCon($(this),$(input[0]).val(),$(input[1]).val());
}});
$("#domian-pri").slider({animate:true,range:true,min:1,max:9,step:1,values:[0,9],stop:function(event,ui){
    ui.values[0]>8?$(input[3]).val(""):$(input[3]).val(zhuan(ui.values[0]));
    ui.values[1]>8?$(input[4]).val(""):$(input[4]).val(zhuan(ui.values[1]));
    slideCon($(this),$(input[3]).val(),$(input[4]).val());
}});

$("#baidu-qz").slider({animate:true,range:true,min:0,max:8,step:1,values:[0,8],stop:function(event,ui){
    ui.values[0]>7?$(input[6]).val(""):$(input[6]).val(ui.values[0]);
    ui.values[1]>7?$(input[7]).val(""):$(input[7]).val(ui.values[1]);
    slideCon($(this),$(input[7]).val(),$(input[7]).val());
}});
$(function () {
  
    $("#paging a").live("click",function () {
		$(this).parent().children('a').removeClass();
        $(this).parent().children('a').addClass('s-def');
		$(this).addClass("t");
        getData($(this).attr("k"));
        $(document.body).scrollTop($(".m-relsult2").position().top-45);
	
	
    })
})

$(input[2]).click(function(){
    var name=$(this).parent().parent().prev().text();
    var str=$(this).parent().prev().find("li").eq(8).text();
    val1=Number($(input[0]).val());
    val2=Number($(input[1]).val());
    if(val2==""){
        addConditions("len",name,1,str,true);
    }
    if(val2<val1){
        addConditions("len",name,val1,str,true);
    }else{
        addConditions("len",name,val1,val2,true);
    }
    getData();//g
});
$(input[5]).click(function(){
    var name=$(this).parent().parent().find("div").eq(0).text();
    var str=$(this).parent().prev().find("li").eq(8).text();
    val3=Number($(input[3]).val());
    val4=Number($(input[4]).val());
    if(val4==""){
        addConditions("pri",name,0,str,true);
    }
    if(val4<val3){
        addConditions("pri",name,val3,str,true);
    }else{
        addConditions("pri",name,val3,val4,true);
    }
    getData();//g
});
//滑块的条件
function slideCon(obj,val,val1){
    var str=$(obj).next().find("li").eq(8).text();
    val=val==""?val=$(obj).next().find("li").eq(0).text():val;
    val1=val1==""?val1=str:val1;
    var str2=$(obj).attr("name"),name=$(obj).parent(0).parent(0).prev().text();
    addConditions(str2,name,val,val1,true);
    getData();
}
//转换
function zhuan(str){
    switch(str){
        case 1:
            str=0;
            break;
        case 2:
            str=500;
            break;
        case 3:
            str=1000;
            break;
        case 4:
            str=20000;
            break;
        case 5:
            str=50000;
            break;
        case 6:
            str=100000;
            break;
        case 7:
            str=200000;
            break;
        case 8:
            str=500000;
            break;  
    }
    return str;
}
//---------------------------------------------搜索条件-------------------------------
//后缀
var arr_hz=$("#m-hz span");
    $(arr_hz[0]).click(function(){
        var name=$(this).parent().parent().attr("name");
        $(arr_hz).removeClass("z-cell");
        $(this).addClass("z-cell");
        reduceConditions(name);
        getData(); //g
    });
    $(arr_hz.slice(1,arr_hz.length)).click(function(){
        $(arr_hz[0]).removeClass("z-cell");
         var name=$(this).parent().parent().attr("name"),idx=$(this).attr("idx"),text=$(this).text(),tname=$(this).parent().prev().text();
        if($(this).hasClass("z-cell")){
            $(this).removeClass("z-cell");
            checkstatus("#m-hz");
            reduceConditions(name,idx);
        }else{
            $(this).addClass("z-cell");
            addConditions(name,tname,text,idx);
        }
        getData();//g
    });
//类型
var arr_style=$("#m-style span"),odiv=$("#data_style div"),_spans=$("#data_style span");
    $(arr_style[0]).click(function(){
        $("#m-style").css("border-bottom","0");
        $(arr_style).removeClass("active");
        $(this).addClass("z-cell");
        $(odiv).css("display", "none");
        $("#selConditions").attr("tag", "");
        showorhide();
        getData();
    });
    $(arr_style.slice(1,arr_style.length)).click(function(){
        $("#m-style").css("border-bottom","1px solid #f2f2f2");
        var index = $(this).index();
        $("#selConditions").attr("tag", "");
        $("#selConditions").attr("tag", $(odiv[index - 1]).find("span").eq(0).attr("idx"));
        $(_spans).removeClass("z-cell");
        $(arr_style).removeClass("z-cell").removeClass("active");
        if($(this).hasClass("active")){
            return;
        }else{
            $(this).addClass("active");
            var name=$(this).parent().parent().parent().attr("name"),str=$(this).parent().parent().prev().text();
            reduceConditions(name);
        }
        $(odiv).css("display","none");
        $(odiv[index - 1]).css("display", "block").addClass("s-bg-f9").find("span").eq(0).addClass("z-cell");
        getData();//g
    });
    $(_spans).click(function(){
        var text=$(this).text(),name=$(this).parent().parent().attr("name"),tname=$(this).parent().parent().parent().prev().text(),idx=$(this).attr("idx");
          addConditions(name,tname,text,idx);
        var tag=$(this).attr("tag");
        if(tag==undefined){
            $(this).parent().find("span").eq(0).removeClass("z-cell");
            if($(this).hasClass("z-cell")){
                $(this).removeClass("z-cell");
                checkstatus($(this).parent());
                reduceConditions(name,idx);
            }else{
                $(this).addClass("z-cell");
            }
        }else{
            $(_spans).removeClass("z-cell");
            $(this).addClass("z-cell");
            $("#selConditions").attr("tag", "");
            $("#selConditions").attr("tag", $(this).attr("idx"));
            reduceConditions(name);
        }
        getData();//g
    });
    //主题属性
    var arr_opers=$("#theme_oper span");
    $(arr_opers[0]).click(function(){
        var name=$(this).parent().parent().attr("name");
        changeStyleTi(arr_opers,this,"z-cell");
        reduceConditions(name);
        showorhide();
        getData();//g
    });
    $(arr_opers.slice(1,arr_opers.length)).click(function(){
        var name=$(this).parent().parent().attr("name");
       if($(this).hasClass("z-cell")){
            $(this).removeClass("z-cell");
            $(arr_opers[0]).addClass("z-cell");
            reduceConditions(name);
       }else{
            changeStyleTi(arr_opers,this,"z-cell");
            var name=$(this).parent().parent().attr("name"),tname=$(this).parent().prev().text(),text=$(this).text(),idx=$(this).attr("idx");
            addConditions(name,tname,text,idx);
       }
       getData();//g
    });
    $("#selConditions a").live("click",function(){
        var name=$(this).attr("name"),idx=$(this).attr("idx");
        $(this).remove();
        $("#g_sh").find("div[name='"+name+"']").find("span[idx='"+idx+"']").removeClass("z-cell");
        if(name=="len"){
            $("#domian-len").slider({animate:true,range:true,min:1,max:9,step:1,values:[1,9]});
            $(input[0]).val("1"); $(input[1]).val("");
        }
        if(name=="pri"){
            $("#domian-pri").slider({animate:true   ,range:true,min:1,max:9,step:1,values:[1,9]});
            $(input[3]).val("0"); $(input[4]).val("");
        }
        if(name=="ty"){
            var obj=$("#g_sh").find("div[name='"+name+"']").find("span[idx='"+idx+"']").parent();
            checkstatus(obj);
        }else{
            checkstatus($("#g_sh").find("div[name='"+name+"']"));
        }
        getData();
    });
    // 排序
    $("#sort span").click(function(){
        $("#sort span").removeClass("z-active");
        $(this).addClass("z-active");
        if($(this).hasClass("z-up")){
            $(this).removeClass("z-up").addClass("z-down").attr("idx", (Number($(this).attr("idx")) + 1).toString());
            getData();
        }else if($(this).hasClass("z-down")){
            $(this).removeClass("z-down").addClass("z-up").attr("idx", (Number($(this).attr("idx")) - 1).toString());
            getData();
        }
    });
    $(".u-btn1").click(function(){
        getData();
    });
//------------------------------------------------------------------------------------
//添加选择条件
function addConditions(){
    if(arguments[0]=="oper"){
         $("#selConditions").find("a[name='"+arguments[0]+"']").remove();
    }
    var str="";
    if(arguments.length>4){
        reduceConditions(arguments[0],arguments[4]);
        str="<a name='"+arguments[0]+"' idx='"+arguments[4]+"' class='s-def'>"+arguments[1]+"<span class='s-2a'>"+arguments[2]+" - </span>"+"<span class='s-2a'>"+arguments[3]+"</span><b></b></a>"; 
        $("#selConditions").append(str);
    }else{
        str="<a name='"+arguments[0]+"' idx='"+arguments[3]+"' class='s-def'>"+arguments[1]+"<span class='s-2a'>"+arguments[2]+"</span><b></b></a>"; 
        $("#selConditions").append(str);
    }
    showorhide();
    //getData();
}
// 删除选择条件
function reduceConditions(){
    if(arguments.length<2){
        $("#selConditions").find("a[name='"+arguments[0]+"']").remove();
    }else{
        $("#selConditions").find("a[name='"+arguments[0]+"'][idx='"+arguments[1]+"']").remove();
    }
    showorhide();
    //getData();
}
//
function showorhide(){
    if($("#selConditions a").length==0){
        $("#selConditions").parent().css("display","none").prev().removeClass("u_bb0");
    }else{
        $("#selConditions").parent().css("display","block").prev().addClass("u_bb0");
    }
}
//检查状态
function checkstatus(){
    if($(arguments[0]).find("span[class='z-cell']").length==0){
        $(arguments[0]).find("span").eq(0).addClass("z-cell");
    };
    showorhide();
}
function changeStyleTi(){
    $(arguments[0]).removeClass(arguments[2]);
    $(arguments[1]).addClass(arguments[2]);
}
function getData(idx) {
    showLodingImg("0", "50%", "750px", "-150px");
    var web_typ_str="";
    $(".f-hz-web_type").children("span").each(function(i,n){
    	if($(n).hasClass('z-cell')) {
    		web_typ_str=web_typ_str+$(n).attr("data")+",";
    	}
    });
    var order_typ_str="";
    $(".f-hz-order_type").children("span").each(function(i,n){
    	if($(n).hasClass('z-cell')) {
    		order_typ_str=order_typ_str+$(n).attr("data");
    	}
    });
    var order=1;
    $("#sort span").each(function(){
        if($(this).hasClass("z-active")){
        	order=$(this).attr("data");
        }
    });
    //只剩下分页没做
    var description = $("#searchKey").val();
    var pr_begin = $("#prBegin").val();
    var pr_end = $("#prEnd").val();
    var flowWebBegin = $("#flowWebBegin").val();
    var flowWebEnd = $("#flowWebEnd").val();
    var keybaohan=$('#keyworda input[type="radio"]:checked').attr("nu");
    var parameter = {
    		"entr_type" :order_typ_str ,
    		"keybaohan" : keybaohan,
    		"description" : description,
    		"web_type":web_typ_str,
    		"pr_begin":pr_begin,
    		"pr_end" :pr_end,
    		"flowWebBegin" :flowWebBegin,
    		"flowWebEnd" :flowWebEnd,
    		"baiduBegin":$("#baiduBegin").val(),
                "pageIndex": idx,
    		"baiduEnd":$("#baiduEnd").val()
    }
    $('#liebiao-list-web').empty();
    $.ajax({
      url:	"./index.php?c=WebEntrusts&a=pagewebs",
      type:'POST',
      data:parameter,
      dataType:'json',
         success:function(result) {
        	    $('#liebiao-list-web').empty();
                var json = result['rows'];
                var htmlStr="";
                htmlStr="<tbody><tr class='s-bg-white' style='text-align:left;'>" +
                		"<th width='50%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站名称</th><th width='15%'>PR值</th>";
                htmlStr+="<th width='19%'>百度权重</th><th width='15%'>标价</th></tr>";
                if(json){
	                $.each(json, function (row, value) {
	                	htmlStr+="<tr style='text-align:left;'><td>";
	                	if(value.entr_type==1) {
	                		htmlStr+="<a href='./index.php?c=WebEntrusts&a=detail&id="+value.id+"' style='padding:0 12px;'>";
	                		htmlStr+="&nbsp;&nbsp;【购买】"+value.description+"</a>";
	                	} else if(value.entr_type==2) {
	                		htmlStr+="<a href='./index.php?c=WebEntrusts&a=detail&id="+value.id+"' style='padding:0 12px;'>";
	                		htmlStr+="&nbsp;&nbsp;【出售】"+value.description+"</a>";
	                	}
	                	htmlStr+="</td><td>"+value.pr+"</td>";
	                	htmlStr+="</td><td>PC-"+value.pc_baidu+"&nbsp;&nbsp;APP-"+value.app_baidu+"</td>";
	                	htmlStr+="<td>"+value.price+"</td>";
	                	htmlStr+="</tr>"
	                });
                }
                htmlStr+="</tbody>";
                $('#liebiao-list-web').html(htmlStr);
                var page = result['paging'];
                if(!page[0].total){
                	page[0].total = 0 ;
                }
                if(!page[0].pageIndex){
                	page[0].pageIndex = 0 ;
                }
                $("#paging").html(nextPage(page[0].pageIndex, page[0].total, page[0].pageSize, 3));
                hideLodingImg();
         },error:function(){
        	 alert("出错了");
        	 hideLodingImg();
         }
       });
}

function getIco(type) {
    var result="<span class='block fl'></span>";
    switch(type){
        case '2':
            result = "<span class='z-img-b4' title='" + lang.a88 + "'></span>";
            break;
        case '1':
            result = "<span class='z-img-b1' title='" + lang.a87 + "'></span>";
            break;
        case '3':
            result = "<span class='z-img-b3' title='" + lang.a81 + "'></span>";
            break;
    }
    return result;
}

