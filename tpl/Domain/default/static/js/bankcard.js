var url = window.location.pathname;
// if (url.indexOf("setbankcard") > 0) {
    var slt_cons = $(".f-odiva div");
    var arr_h3 = $(".f-odiva h3");
    var provice = getId("provice"), city = getId("city"), prefecture = getId("prefecture");
    /*默认情况下第一行省*/
    createElements(provice, dsy.Items[0]);
    /*默认情况下第一行市*/
    createElements(city, dsy.Items["0_19"]);
    /*默认情况下第一行区县*/
    createElements(prefecture, dsy.Items["0_19_1"]);
    $(document.body).click(function () {
        changeSltStyle(".f-odiva h3,.edit-f-odiva h3", { "background-position": "70px -500px", "border": "1px solid #e1e1e1" });
        $(".f-odiva div,.edit-f-odiva div").hide();
    });
	/*修改银行卡*/
	var e_slt_cons = $(".edit-f-odiva div");
    var e_arr_h3 = $(".edit-f-odiva h3");
	var e_provice = getId("edit-provice"), e_city = getId("edit-city"), e_prefecture = getId("edit-prefecture");
	/*默认情况下第一行省*/
    createElements(e_provice, dsy.Items[0]);
    /*默认情况下第一行市*/
    createElements(e_city, dsy.Items["0_19"]);
    /*默认情况下第一行区县*/
    createElements(e_prefecture, dsy.Items["0_19_1"]);
	// 省市区三级联动
    $(e_arr_h3).click(function (event) {
        var index = $(this).index();
        changeSltStyle(e_slt_cons, { "display": "none" })
        changeSltStyle(e_slt_cons[index], { "display": "block" });
        changeSltStyle(this, { "background-position": "70px -618px", "border": "1px solid #2aa3ce" });
        event.stopPropagation();
    });
	function e_setS(obj, n) {
        $($(".edit-f-odiva h3")[n]).text($(obj).text());
        changeSltStyle($(".edit-f-odiva h3")[n], { "background-position": "70px -500px", "border": "1px solid #e1e1e1" });
        $(obj).parent().hide();
    }
	/*-------*/
	
    // 省市区三级联动
    $(arr_h3).click(function (event) {
        var index = $(this).index();
        changeSltStyle(slt_cons, { "display": "none" })
        changeSltStyle(slt_cons[index], { "display": "block" });
        changeSltStyle(this, { "background-position": "70px -618px", "border": "1px solid #2aa3ce" });
        event.stopPropagation();
    });
    function createElements(obj, items) {
		console.log(obj);
        // 首先判断obj是否有option子节点,如果有，则先进行清除
        var child = getTagName(obj, "P");
        if (child.length != 0) {
            for (var i = child.length - 1; i > -1; i--) {
                obj.removeChild(child[i]);
            }
        }
        /*创建option*/
        for (var i = 0, j = items.length; i < j; i++) {
            var option = document.createElement("P");
            option.setAttribute("title", items[i]);
            option.innerHTML = items[i];
            obj.appendChild(option);
        }
        if (arguments.length > 2) {
            $(arguments[2]).text(items[0]);
        }
        if (arguments.length > 3) {
            $(arguments[2]).text(items[0]);
        }

    }
    function setS(obj, n) {
        $($(".f-odiva h3")[n]).text($(obj).text());
        changeSltStyle($(".f-odiva h3")[n], { "background-position": "70px -500px", "border": "1px solid #e1e1e1" });
        $(obj).parent().hide();
    }
    function change() {
        var str = "0";//初始化str索引
        var child = getTagName(arguments[0], "option");
        if (arguments.length < 3) {
            str += "_" + arguments[1];
            var items = provice.Items[str];
            createElements(arguments[0], items);
        } else if (arguments.length >= 3) {
            str += "_" + arguments[1] + "_" + arguments[2];
            var items = provice.Items[str];
            createElements(arguments[0], items);
        }
    }
    function getId(name) {
        if (typeof name == "string") {
            return document.getElementById(name);
        }
    }
    function getTagName() {
        if (arguments.length < 1) {
            document.getElementsByTagName(arguments[0]);
        } else if (arguments.length > 1) {
            if ((typeof arguments[0] == "object") && (typeof arguments[1] == "string")) {
                return arguments[0].getElementsByTagName(arguments[1]);
            }
        }
    }
    function changeSltStyle() {
        $(arguments[0]).css(arguments[1]);
    }
	
		$(slt_cons[0]).find("p").live("mousedown",function (event) {
            $(slt_cons[0]).find("p").removeAttr("idx");
            $(this).attr("idx", $(this).index());
            var n = $(this).parent().attr("idx");
            setS(this, n);
            createElements(city, dsy.Items["0_" + $(this).index()], arr_h3[1]);
            createElements(prefecture, dsy.Items["0_" + $(this).index() + "_0"], arr_h3[2], true);
            event.stopPropagation();
        });
        $(slt_cons[1]).find("p").live("mousedown",function (event) {
            var xb = $(slt_cons[0]).find("p[idx]").attr("idx");
            xb = xb == undefined ? "19" : xb;
            var n = $(this).parent().attr("idx");
            setS(this, n);
            createElements(prefecture, dsy.Items["0_" + xb + "_" + $(this).index()], arr_h3[2], true);
            event.stopPropagation();
        });
        $(slt_cons[2]).find("p").live("mousedown",function (event) {
            var n = $(this).parent().attr("idx");
            setS(this, n);
            event.stopPropagation();
        });
		
		$(e_slt_cons[0]).find("p").live("mousedown",function (event) {
            $(e_slt_cons[0]).find("p").removeAttr("idx");
            $(this).attr("idx", $(this).index());
            var n = $(this).parent().attr("idx");
            e_setS(this, n);
            createElements(e_city, dsy.Items["0_" + $(this).index()], e_arr_h3[1]);
            createElements(e_prefecture, dsy.Items["0_" + $(this).index() + "_0"], e_arr_h3[2], true);
            event.stopPropagation();
        });
        $(e_slt_cons[1]).find("p").live("mousedown",function (event) {
            var xb = $(e_slt_cons[0]).find("p[idx]").attr("idx");
            xb = xb == undefined ? "19" : xb;
            var n = $(this).parent().attr("idx");
            e_setS(this, n);
            createElements(e_prefecture, dsy.Items["0_" + xb + "_" + $(this).index()], e_arr_h3[2], true);
            event.stopPropagation();
        });
        $(e_slt_cons[2]).find("p").live("mousedown",function (event) {
            var n = $(this).parent().attr("idx");
            e_setS(this, n);
            event.stopPropagation();
        });