﻿$(function () {
    $("#amp").click(function () {
        showBox("pop2");
    });
    $("#jsjj_box").click(function () {
        showBox("pop10");
    })
    $(".close").click(function () {
        try{closeBox("pop2");}
        catch(e){};
        try { closeBox("pop10"); }
        catch (e) { };
    });
    
    $("#cph_txt_bj").keyup(function () { clearNoNum1(this); })
    $("#jsbj").click(function () {
        var num = $("#cph_txt_bj").val();
        if (num == "" || num == "0") {
            showDialog(lang.a71);
            return false;
        }
        if (!$("#agree").attr("checked")) {
            showDialog(lang.a194);
            return false;
        }
        $.post("./index.php?c=Fastbid&a=fastbidmoney", { auctionId: $("#fastid").val(), offer: num, domainName: $(".g_fw_f37").text() }, function (result) {
            if (result.status == 1) {
                switch (result.info) {
                    case '0': {
                        showDialog(lang.a171, location.href);	//竞价已结束
                        break;
                    }
                    case '1': {
                        showDialog(lang.a26);		//当前余额不足
                        break;
                    }
                    case '2': {
                        showDialog(lang.a172, location.href);		//当前价格已被超越
                        break;
                    }
                    case '3': {
                        showDialog(lang.a173, location.href);	//代理价设置成功
                        break;
                    }
                    case '4': {
                        showDialog(lang.a167);		//不能对自己的域名出价
                        break;
                    }
                    case '5': {
                        location.href = "./index.php?c=Login";	//登录
                        break;
                    }
                    case '7': {
                        showDialog(lang.a23, location.href);		//异常！
                        break;
                    }
                    default: {
                        showDialog(lang.a36, location.href);	//出价成功
                    }
                }
            }else{
				showDialog(result.info);
			}
        });
    });
    // $.get("/runbidend");

    //倒计时
	if($(".countdown_new strong").size() > 3){
		TimeCountDay(".countdown_new strong");
	}else{
		TimeCount(".countdown_new strong");
	}
    var __qqClockShare = {
        content: encodeURIComponent($("h3.u-h3b").text().trim()+' 极速竞价域名QQ提醒 - '+$("#config_name").val()),
        advance: 0,
        url: encodeURIComponent(location.href)
    };
    $("#qqRemind").attr("href", "http://qzs.qq.com/snsapp/app/bee/widget/open.htm#content=" + __qqClockShare.content + "&advance=" + __qqClockShare.advance + "&url=" + __qqClockShare.url);


    $(".f-img-i7").click(function () {
        if ($("#userId").length < 1) {
            setCookie("backUrl", location.pathname, 2);
            location.href = "/login";
        }
    })
    /*if ($("#userId").length > 0) {
        $(".f-img-i7").zclip({
            path: '/images/account/ZeroClipboard.swf',
            copy: function () { return document.title+"\r\n"+$(this).attr("k"); }
        });

        $(".zclip").attr("title", "点击复制分销链接："+$(".f-img-i7").attr('k'));
    }*/
})

function TimeCount(selecter) {
    var $timedom = $(selecter);
    var target_time = parseInt($timedom.eq(0).text()) * 3600 + parseInt($timedom.eq(1).text()) * 60 + parseInt($timedom.eq(2).text()); //总秒数
    //加上当前时间戳获得目标时间戳
    target_time += parseInt(new Date().getTime() / 1000);
    var now_time = parseInt(new Date().getTime() / 1000);
    if ((parseInt(target_time) - parseInt(now_time)) > 0) {
        var resTime = new Array(3);
        TimeOut(now_time);
        var timer1 = setInterval(function () {
            now_time = new Date().getTime() / 1000;
            TimeOut(now_time);
        },
        1000);
    }
    function TimeOut(now) {
        var seconds = parseInt(target_time) - parseInt(now);
        if (seconds >= 0) {
            resTime[0] = (seconds % (86400 * 24) - seconds % 86400 % 3600) / 3600;
            resTime[1] = parseInt(seconds % 86400 % 3600 / 60);
            resTime[2] = seconds % 86400 % 3600 % 60;
            for (var i in resTime) {
                if (parseInt(resTime[i]) < 10) {
                    resTime[i] = "0" + resTime[i];
                };
            };
            $timedom.eq(0).text(resTime[0]);
            $timedom.eq(1).text(resTime[1]);
            $timedom.eq(2).text(resTime[2]);
        }
    };
};
function TimeCountDay(selecter) {
    var $timedom = $(selecter);
    var target_time = parseInt($timedom.eq(0).text()) * 86400 + parseInt($timedom.eq(1).text()) * 3600 + parseInt($timedom.eq(2).text()) * 60 + parseInt($timedom.eq(3).text()); //总秒数
    //加上当前时间戳获得目标时间戳
    target_time += parseInt(new Date().getTime() / 1000);
    var now_time = parseInt(new Date().getTime() / 1000);
    if ((parseInt(target_time) - parseInt(now_time)) > 0) {
        var resTime = new Array(3);
        TimeOutDay(now_time);
        var timer1 = setInterval(function () {
            now_time = new Date().getTime() / 1000;
            TimeOutDay(now_time);
        },
        1000);
    }
    function TimeOutDay(now) {
        var seconds = parseInt(target_time) - parseInt(now);
        if (seconds >= 0) {
			// console.log(seconds);
            resTime[0] = parseInt(seconds / 86400);
            resTime[1] = parseInt(seconds % 86400 / 3600);
            resTime[2] = parseInt(seconds % 86400 % 3600 / 60);
            resTime[3] = seconds % 86400 % 3600 % 60;
            for (var i in resTime) {
                if (parseInt(resTime[i]) < 10) {
                    resTime[i] = "0" + resTime[i];
                };
            };
            $timedom.eq(0).text(resTime[0]);
            $timedom.eq(1).text(resTime[1]);
            $timedom.eq(2).text(resTime[2]);
            $timedom.eq(3).text(resTime[3]);
        }
    };
};