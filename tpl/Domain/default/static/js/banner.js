﻿   //图片banner
(function () {
	var Timer={};
	var index = 0;
	var running = false; //动画不在运行中
   	var arr_lis=$("#focusBar li");
    //添加控制按钮
   	for (var i = 0; i < arr_lis.length; i++) {
   	    $(".control").append("<span></span>");
   	}
   	$(".control span:first").addClass("t");
   	$(".control").css("margin-left", arr_lis.length < 2 ? 0 : -(arr_lis.length - 1) * 35);

	starFocustAm();
	$(arr_lis[0]).show();
	$(arr_lis).css("width",$(window).width());
	$(arr_lis).mouseover(function(){stopFocusAm();}).mouseout(function(){starFocustAm();});
	$(".control span").live("mouseover", function () { stopFocusAm(); }).live("click", function () {changeFocus($(this).index());});

	function changeFocus(skip) {
	    if (arr_lis.length <= 1 || running || index == skip) return;
	    running = true;
	    var num = skip != null ? skip : (index == $(arr_lis).length - 1 ? 0 : index + 1);

        //准备下一张
		$(arr_lis[index]).css("width",$(window).width());
		$(arr_lis[num]).css("width",$(window).width());
		$(arr_lis[num]).css("left",$(window).width());
		$("#focusIndex"+num+" .focusL").css("left",$(window).width()/2);
		$("#focusIndex"+num+" .focusR").css("left",$(window).width()/2);
		$(arr_lis[index]).show();
		$(arr_lis[num]).show();
			
        //切换控制按钮
		$(".control span").removeClass("t");
		$(".control span").eq(num).addClass("t");
        //现有张 移走
		$("#focusIndex"+index+" .focusL").animate({left: -($(window).width()/2+980)},200,'easeInExpo');
		$("#focusIndex" + index + " .focusR").animate({ left: -($(window).width() / 2 + 980) }, 400, 'easeInExpo', function () {
            //下一张 展示
		    $("#focusIndex" + num + " .focusL").animate({ left: -490 }, 800, 'easeInOutCirc');
		    $("#focusIndex" + num + " .focusR").animate({ left: -490 }, 1200, 'easeInOutCirc');
					
		    $("#focusIndex" + index).animate({ left: -$(window).width() }, 800, 'easeOutExpo');
		    $("#focusIndex" + num).animate({ left: 0 }, 800, 'easeOutExpo', function () {
				$("#focusIndex"+index).hide();
				index = num;
				running = false;
			});
		});

	}
	function starFocustAm(){
		Timer = setInterval(timer_tickF,5000);
	}
	function stopFocusAm(){
		clearInterval(Timer);
	}
	function timer_tickF() {
		changeFocus();
	}
})();