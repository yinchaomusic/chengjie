/**
 * on 2015/6/6.
 */

$(function () {
    var $Aquota=$("#Aquota"),Aquotas=0,  //可用额度
        $Amount=$("#Amount"),  //借款总额
        MinRate = 1;//最低年利息
    $(".popover-close").click(function(){
        $("#result").hide();
    });
    $("#LoanCycleMonth").click(function () {
        $("#LoanCycleDay3").prop("checked",true);
        MinRate=1;
        checkRate();
    }).change(function () {
        reckon();
    });

    $("#LoanCycleDay1,#LoanCycleDay2").change(function () {
        $("#LoanCycleMonth").val(1);
        reckon();
    });
    $("#LoanCycleDay1").change(function () {
        $(".error-text").text("7天标 最低利率不得低于16%");
        MinRate = 16;
        checkRate();
    });
    $("#LoanCycleDay2").change(function () {
        $(".error-text").text("15天标 最低利率不得低于14%");
        MinRate = 14;
        checkRate();
    });
    $("#LoanCycleDay3").change(function () {
        MinRate=1;
        checkRate();
         $("#LoanCycleMonth").focus();

    });
  

    $("#Amount").on("blur",function(){
        var $this =	$(this);
        if($(this).val() > Aquotas ){
            if(Aquotas){$(this).val(Aquotas);}else{$(this).val("");
              //  alert("请选择有额度的质押物");
                swal({   title: "错误!",   text: "请选择有额度的质押物.",   timer: 2000,   showConfirmButton: false });
                return ;
            }
        }
        if ($(this).val() > 100) {
            $(this).val(parseInt($this.val()/100)*100);
        } else if ($(this).val()!="") {
            $(this).val(100);
        }
         $("#Amountdx").text(NoToChinese($(this).val()));
        reckon();
    });

    $("#Amountdx").click(function(){
        $Amount.focus();
    });
    $("#sall").change(function () {
        Aquota = 0;
        if ($(this).is(":checked")) {
            $(this).next().text("取消全部");
            $(".select_list_area input[type='checkbox']").prop('checked', true).parents("li").addClass("select");
        } else {
            $(this).next().text("选择全部");
            $(".select_list_area input[type='checkbox']").prop('checked', false).parents("li").removeClass("select");
        }
        $(".select_list_area .select input[type='checkbox']").each(function (i) {
            Aquota += $(this).attr("data-amount") * 1;
        });
        $Aquota.text(Aquota + ".00【 " + NoToChinese(Aquota) + " 】");
        Aquotas = Aquota;
        reckon();

    });
    $(".select_list_area input[type='checkbox']").change(function(){
        var $this =$(this),Aquota=0;
        if($this.is (":checked")){
            $this.parents("li").addClass("select");
        }else{
            $this.parents("li").removeClass("select");
        }
        $(".select_list_area .select input[type='checkbox']").each(function (i) {
            Aquota+=$(this).attr("data-amount")*1;
        });
        if($Amount.val()> Aquota){
            if(Aquota){
                $Amount.val(Aquota);
                $("#Amountdx").text(NoToChinese(Aquota))
            }else{
                $Amount.val("").focus();
            }

        }
        $Aquota.text(Aquota+".00【 "+ NoToChinese(Aquota)+" 】");
        Aquotas=Aquota;
        reckon();
        //console.log(Aquotas);
    });

    $("#Rate").on("keyup",function(){
        if($(this).val() > 25){
            $(this).val(25);
        }
        checkRate();
        reckon();
    });
    $("#autoRate").click(function(){
        $nls.val(MinRate).parents(".form-group").removeClass("has-error");
        reckon();
    });
    var $result=$("#result"),
        $zlx=$("#zlx"),//总利息
        $zglf=$("#zglf"),//总管理费
        $myfk=$("#myfk"),//每月还款
        $lx=$("#lx"),//每月利息
        $myglf=$("#myglf"),//每月管理费
        $zhhk=$("#zhhk"),//最后本金需还款
        glf=0.005,//管理费
        $nls=$("#Rate");//年利息

    function checkRate(){
        if($nls.val() !=""){
            if($nls.val()< MinRate){
                $nls.parents(".form-group").addClass("has-error");
            }else{
                $nls.parents(".form-group").removeClass("has-error");
            }
        }
    }
    function reckon(){
        var $qishu= $("input:radio[name='LoanCycleDay']:checked").val(),//期数 单位天
            r_zhhk = ($Amount.val()*1).toFixed(4).replace(/(.*)\d\d/, "$1"),
            r_mylx,//每月利息
            r_zlx,//总利息
            r_myglf,//每期的管理费
            r_zglf;//总管理费


        //每月利息 //年利率*借款总额/360* (7,15,30 每一期)
        r_mylx = ($nls.val()/100*$Amount.val()/360*$qishu).toFixed(4).replace(/(.*)\d\d/, "$1");
        r_myglf= ($Amount.val()*glf).toFixed(4).replace(/(.*)\d\d/, "$1");
        if($qishu == "0"){
            r_zglf= ($Amount.val()*glf*$("#LoanCycleMonth").val()).toFixed(4).replace(/(.*)\d\d/, "$1");
            r_mylx = ($nls.val()/100*$Amount.val()/360*30).toFixed(4).replace(/(.*)\d\d/, "$1");
            r_zlx =($nls.val()/100*$Amount.val()/360*$("#LoanCycleMonth").val()*30).toFixed(4).replace(/(.*)\d\d/, "$1");
        }else{
            r_zlx = r_mylx;
            r_zglf = r_myglf;

        }
        $lx.text(r_mylx);//每月利息
        $zlx.text(r_zlx);//总利息
        $myglf.text(r_myglf);//每期的管理费
        $zglf.text(r_zglf);//总管理费
        $myfk.text((r_mylx*1+r_myglf*1).toFixed(4).replace(/(.*)\d\d/, "$1"));//总管理费
        $zhhk.text(r_zhhk);
        if($result.is(":hidden")){$result.show();}



        //==========================================================================


    }
 });