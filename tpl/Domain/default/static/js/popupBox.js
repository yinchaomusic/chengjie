var popupBoxZindex = 1001;
var popupBoxbgZindex = 1000;
function getScrollTop() {
	if(document.documentElement.scrollTop > 0) return document.documentElement.scrollTop;
	else return document.body.scrollTop; // 由于chrome不支持document.documentElement.scrollTop
}
function showBox(id) {
	var Idiv=document.getElementById(id);
	Idiv.style.position="absolute";
	Idiv.style.display="block";
	popupBoxZindex = popupBoxZindex + 2;
	Idiv.style.zIndex=popupBoxZindex;
	//以下部分要将弹出层居中显示
	var left = (document.documentElement.clientWidth-Idiv.clientWidth)/2+document.documentElement.scrollLeft;
	var top = (document.documentElement.clientHeight-Idiv.clientHeight)/2+getScrollTop();
	Idiv.style.left=(left<0?0:left)+"px";
	Idiv.style.top=(top<0?0:top)+"px";
	//以下部分使整个页面至灰不可点击
	popupBoxbgZindex = popupBoxbgZindex + 2;
	var procbg = document.createElement("div"); //首先创建一个div
	procbg.setAttribute("id",id+"_bg"); //定义该div的id
	if(!!window.ActiveXObject) { // IE
		var width = $(window).width(),height = $(document).height();
		if(!window.XMLHttpRequest) {  // IE6
			procbg.style.cssText="background:#A9A9A9;width:"+width+";height:"+height+";position:fixed!important;position:absolute;top:0;left:0;z-index:"+popupBoxbgZindex+";opacity:0.2;filter:Alpha(opacity=70);";
		} else {
			procbg.style.cssText="background:#A9A9A9;width:100%;height:100%;position:fixed;top:0;left:0;z-index:"+popupBoxbgZindex+";opacity:0.6;filter:Alpha(opacity=70);";
		}
		$(procbg).prepend('<iframe style="position:absolute;z-index:-1;width:'+width+'px;height:'+height+'px;filter:alpha(opacity=0);-moz-opacity:0;"></iframe>');
	} else {
		procbg.style.cssText="background:#A9A9A9;width:100%;height:100%;position:fixed;top:0;left:0;z-index:"+popupBoxbgZindex+";opacity:0.6;filter:Alpha(opacity=70);";
	}
	document.body.appendChild(procbg); //背景层加入页面
	
	var titleObj=$("#"+id).find("[name=title]");
	if (titleObj.length > 0) {
		titleObj.css({"cursor":"move"});
		//以下部分实现弹出层的拖拽效果
		var posX;
		var posY;
		titleObj.mousedown(function(e) {
			if(!e) e = window.event; //IE
			posX = e.clientX - parseInt(Idiv.style.left);
			posY = e.clientY - parseInt(Idiv.style.top);
			document.onmousemove = mousemove;
		}).mouseup(function() {
			document.onmousemove = null;
		});
		function mousemove(ev) {
			if(ev==null) ev = window.event;//IE
			var curX = ev.clientX - posX;
			var curY = ev.clientY - posY;
			if(curX >= 0 && document.documentElement.clientWidth >= curX+Idiv.clientWidth+4)
				Idiv.style.left = curX + "px";
			if(curY >= 0 && document.documentElement.clientHeight+getScrollTop() >= curY+Idiv.clientHeight+4)
				Idiv.style.top = curY + "px";
		}
	}
}
function closeBox(id) {//关闭弹出层
	var Idiv=document.getElementById(id);
	Idiv.style.display="none";
	var body = document.getElementsByTagName("body");
	var mybg = document.getElementById(id+"_bg");
	body[0].removeChild(mybg);
}
