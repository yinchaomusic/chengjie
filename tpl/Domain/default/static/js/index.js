	$("#classic_case").cxScroll({direction: "right", step: 2,speed:800,time:2000 });
		// 数据列表区滑动
		$(function () {
		    var arr_span = $("#k-spano span");
		    var arr_span1 = $("#k-spant span");
		    var arr_ol = $("#m-highQuality ul");
		    var arr_ol1 = $("#m-oncePricing ul");
		    var n = 0, j = 0, Timer_ol = {}, Timer_a = {};
		    k1();
		    k2();
		    // 页面加载之后自动运行滑动效果
		    function k1() {
		        if (arr_ol.length <= 1) {
		            return;
		        }
		        Timer_ol = setInterval(function () {
		            n++;
		            if (n >arr_ol.length - 1) {
		                n = 0;
		            }
		            chan(arr_span, n);
		            startMove("m-highQuality", n);
		        }, 5000);
		    }
		    function k2() {
		        if (arr_ol1.length <= 1) {
		            return;
		        }
		        Timer_a = setInterval(function () {
		            j++;
		            if (j >arr_ol1.length - 1) {
		                j =0 ;
		            }
		            chan(arr_span1, j);
		            startMove("m-oncePricing", j);
		        }, 5000);
		    }
		    // 控制滑动
		    $(arr_span).mouseover(function () {
		        clearInterval(Timer_ol);
		        $(arr_span).removeClass("t");
		        $(this).addClass("t");
		        n = $(this).index();
		        startMove("m-highQuality", n);
		    }).mouseout(function () {
		        k1();
		    });
		    $(arr_span1).mouseover(function () {
		        clearInterval(Timer_a);
		        $(arr_span1).removeClass("t");
		        $(this).addClass("t");
		        j = $(this).index();
		        startMove("m-oncePricing", j);
		    }).mouseout(function () {
		        k2();
		    });
		    $("#m-highQuality").mouseover(function () {
		        clearInterval(Timer_ol);
		    }).mouseout(function () {
		        k1();
		    });
		    $("#m-oncePricing").mouseover(function () {
		        clearInterval(Timer_a);
		    }).mouseout(function () {
		        k2();
		    });
		    function chan(obj, n) {
		        $(obj).removeClass("t");
		        $(obj[n]).addClass("t");
		    }
		    function startMove(str, n) {
		  		$("#"+str).stop(true, true).animate({ left: -(711*n)+"px", speed: 500 });
		    }

		});
	 	//议价域名
	 	$(".m-discussList table tr:not(:first)").mouseenter(function(){
	 		$(".m-discussList table tr:not(:first)").removeClass("s-bg-fd");
	 		$(this).find("a").css("color","#FFA200");
	 		$(this).addClass("s-bg-fd");
	 	}).mouseleave(function(){
	 		$(this).find("a").removeAttr("style");
	 		$(".m-discussList table tr:not(:first)").removeClass("s-bg-fd");
	 	});
		// 竞价时间
		if ($("#time_jj").length > 0) {        
		    setInterval(function () {
		        var r = $("#time_jj").html().match(/(\d+?):(\d+?):(\d+)/)
		        var h = Number(r[1]);
		        var m = Number(r[2]);
		        var s = Number(r[3]);
		        if (s == 0) {
		            if (h == 0 && m == 0) {
		                return;
		            }
		            s = 59;
		            if (m == 0) {
		                m = 59;
		                h -= 1;
		            }
		            else
		                m -= 1;

		        } else {
		            s -= 1;
		        }
		        $("#time_jj").html(bl(h) + ":" + bl(m) + ":" + bl(s));
		    }, 1000)
		}
		
$("#selectObjOp").bind("change", function(){
			if($(this).val()=="1") {
				$("#doMainUiId").css("display","");
				$("#webUiId").css("display","none");
			} else {
				$("#webUiId").css("display","");
				$("#doMainUiId").css("display","none");
			}
	 });

$(".faqijiaoyiA").click(function(){
	
	var objType=$("#selectObjOp").val();
	var price =$("#input_price").val();
	if(objType=="1" || objType==1) {//域名
		if($("#doMainUiId").val()=="1") {//中介
			location.href = "index.php?c=Escrow&a=index";
		} else {//代购
			location.href = "index.php?c=Procurement&a=index&d="+price;
		}
	} else {//网站
		if($("#webUiId").val()==3 || $("#webUiId").val()=='3') {//网站中介交易
			location.href = "index.php?c=WebInterOrder&a=editView&price="+price;
		} else {//网站代购，代售
			location.href = "index.php?c=WebEntrusts&a=editView&webtype="+$("#webUiId").val()+"&price="+price;
		}
	}
});