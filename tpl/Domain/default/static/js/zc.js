$(document).ready(function () {
    // 邮箱验证
    var arr_lis = $("#logins").find("input");
    var wrong = "";
    $(arr_lis[0]).blur(function () {
        $(arr_lis[0]).keyup();
        var rex = /^[0-9a-zA-Z\._-]+@[0-9a-zA-Z_-]+\.[a-z]{2,4}$|^1\d{10}$/; //邮箱 or 手机号
        var email = $(this).val() == $(this).attr("ph") ? "" : $(this).val();
        if (rex.test(email)) {
            var t = $(this);
            $.getJSON("/isemail", { "email": email }, function (data, status) {
                if (status = "success" && data.data == "0") {
                    t.parent().next().text("").removeClass("z-txt s-bg-f0").addClass("z-img-right s-bg-white");
                } else {
                    t.parent().next().text(lang.a136).removeClass("z-img-right s-bg-white").addClass("z-txt s-bg-f0");
                }
            });
        } else {
            $(this).parent().next().text(lang.a137).removeClass("z-img-right s-bg-white").addClass("z-txt s-bg-f0");
        }
    })
    // 判断是否是手机
    $(arr_lis[0]).keyup(function () {
        if ($(this).val().length == 11) {
            //如果是手机
            var rex = /^1\d{10}$/;  //11位手机号
            var email = $(this).val() == $(this).attr("ph") ? "" : $(this).val();
            // 是手机
            if (rex.test(email)) {
                $(".tr_yzm").css("display", "none");
                $(".tr_sj_yzm").css("display", "block");
                $(arr_lis[4]).parent().parent().css("display", "none");
            } else {
                $(".tr_sj_yzm").css("display", "none");
                $(".tr_yzm").css("display", "block");
                $(arr_lis[4]).parent().parent().css("display", "inline-block");
            }
        } else {
            $(arr_lis[4]).parent().parent().css("display", "block");
            $(".tr_yzm").css("display", "");
            $("#sj_yanzm").html("");
            $(".tr_sj_yzm").css("display", "none");
        }
    })

    $(arr_lis[1]).keyup(function () {
        var password = $(this).val();
        var rex = /^.{6,16}$/;
        if (rex.test(password)) {
            switch (password.length) {
                case 6:
                case 7:
                case 8://3种混合中，否则弱
                    /[a-z]/i.test(password) && /\d/.test(password) && /[^a-z0-9]/i.test(password) ? strength.z() : strength.r();
                    break;
                case 9:
                case 10:
                case 11:
                case 12://三种强，两种中，一种弱
                    if (/[a-z]/i.test(password) && /\d/.test(password) && /[^a-z0-9]/i.test(password)) {
                        strength.q();
                    } else if ((/[^a-z0-9]{0}/i.test(password) && /[a-z]/i.test(password) && /\d/.test(password)) || (/\d{0}/.test(password) && /[a-z]/i.test(password) && /[^a-z0-9]/i.test(password)) || (/[a-z]{0}/i.test(password) && /[^a-z0-9]/i.test(password) && /\d/.test(password))) {
                        strength.z();
                    } else {
                        strength.r();
                    }
                    break;
                case 13:
                case 14:
                case 15:
                case 16://两种以上强，否则中
                    if ((/[a-z]/i.test(password) && /\d/.test(password) && /[^a-z0-9]/i.test(password)) || (/[^a-z0-9]{0}/i.test(password) && /[a-z]/i.test(password) && /\d/.test(password)) || (/\d{0}/.test(password) && /[a-z]/i.test(password) && /[^a-z0-9]/i.test(password)) || (/[a-z]{0}/i.test(password) && /[^a-z0-9]/i.test(password) && /\d/.test(password))) {
                        strength.q();
                    } else {
                        strength.z();
                    }
            }
        } else {
            return;
        }
    });
    //密码强度
    $(arr_lis[1]).blur(function () {
        password = $(this).val();
        if (password && password.length > 5) {
            $(".u-txq").css("display", "none").parent().removeClass("u_mb0");
            $(this).parent().next().text("").removeClass("z-txt s-bg-f0").addClass("z-img-right s-bg-white");
        } else if (password && password.length < 6) {
            $(this).parent().next().text(lang.a3).removeClass("z-img-right s-bg-white").addClass("z-txt s-bg-f0");
        } else if (!password || password == "") {
            $(this).parent().next().text(lang.a6).removeClass("z-img-right s-bg-white").addClass("z-txt s-bg-f0");
        }
    });
    var strength = {
        q: function () {
            $(arr_lis[1]).parent().next().text("").removeAttr("class");
            $(".u-txq").css("display", "block").parent().addClass("u_mb0");
            $(".u-txq").find("span").removeClass("s-bg-c4").addClass("s-bg-ff6");
        },
        z: function () {
            $(arr_lis[1]).parent().next().text("").removeAttr("class");
            $(".u-txq").css("display", "block").parent().addClass("u_mb0");
            $(".u-txq").find("span").removeClass("s-bg-ff6").addClass("s-bg-c4");
            $(".u-txq").find("span").eq(0).removeClass("s-bg-c4").addClass("s-bg-ff6");
            $(".u-txq").find("span").eq(1).removeClass("s-bg-c4").addClass("s-bg-ff6");
        },
        r: function () {
            $(arr_lis[1]).parent().next().text("").removeAttr("class");
            $(".u-txq").css("display", "block").parent().addClass("u_mb0");
            $(".u-txq").find("span").removeClass("s-bg-ff6").addClass("s-bg-c4");
            $(".u-txq").find("span").eq(0).removeClass("s-bg-c4").addClass("s-bg-ff6");
        }
    }
    $(arr_lis[2]).blur(function () {
        var password = arr_lis[1].value;
        var password2 = $(this).val();
        if (password == password2 && password2 != "") {
            $(this).parent().next().text("").removeClass("z-txt s-bg-f0").addClass("z-img-right s-bg-white");

        } else if (password2 != password && password2 != "") {
            $(this).parent().next().text(lang.a4).removeClass("z-img-right s-bg-white").addClass("z-txt s-bg-f0");

        } else if (!password2 || password2 == "") {
            $(this).parent().next().text(lang.a138).removeClass("z-img-right s-bg-white").addClass("z-txt s-bg-f0");
        }
    });
    //真实姓名
    $(arr_lis[3]).blur(function () {
        var realname = $(this).val() == $(this).attr("ph") ? "" : $(this).val();
        if (realname && realname != "" && realname.length > 1) {
            $(this).parent().next().text("").removeClass("z-txt s-bg-f0").addClass("z-img-right s-bg-white");

        } else if (!realname || realname == "") {
            $(this).parent().next().text(lang.a139).removeClass("z-img-right s-bg-white").addClass("z-txt s-bg-f0");

        } else if (realname && realname.length < 2) {
            $(this).parent().next().text(lang.a140).removeClass("z-img-right s-bg-white").addClass("z-txt s-bg-f0");
        }
    });

    $(arr_lis[4]).blur(function () {
        $("#phone").html("");
        var rex = /^1[\d]{10}$/;
        var phone = $(this).val() == $(this).attr("ph") ? "" : $(this).val();
        if (phone && phone != "") {
            if (rex.test(phone)) {
                $(this).parent().next().text("").removeClass("z-txt s-bg-f0").addClass("z-img-right s-bg-white");
            } else {
                $(this).parent().next().text(lang.a141).removeClass("z-img-right s-bg-white").addClass("z-txt s-bg-f0");
            }
        }
    });

    $(arr_lis[5]).blur(function () {
        $("#ts-qq").html("");
        var rex = /^\d{5,15}$/;
        var qq = $(this).val() == $(this).attr("ph") ? "" : $(this).val();
        if (qq && qq != "") {
            if (rex.test(qq)) {
                $(this).parent().next().text("").removeClass("z-txt s-bg-f0").addClass("z-img-right s-bg-white");
            } else {
                $(this).parent().next().text(lang.a142).removeClass("z-img-right s-bg-white").addClass("z-txt s-bg-f0");
            }
        }
    });
/*
    //验证码
    $(arr_lis[6]).blur(function () {
        var val = $(this).val();
        if (val.length == 4) {
            $.getJSON("/isyzm", { "yzm": val }, function (data, status) {
                if (status == "success" && data.data == "1") {
                    $("#yzm1").text("").removeClass("z-txt s-bg-f0").addClass("z-img-right s-bg-white");
                } else {
                    $("#yzm1").text(lang.a143).removeClass("z-img-right s-bg-white").addClass("z-txt s-bg-f0");
                }
            });
        } else {
            $("#yzm1").text(lang.a143).removeClass("z-img-right s-bg-white").addClass("z-txt s-bg-f0");
        }
    });

    //手机验证码
    $(arr_lis[7]).blur(function () {
        var val = $(this).val();
        if (val.length == 6) {
            console.log('yzm:'+val);
            $.getJSON("/isyzm", { "yzm": val }, function (data, status) {
                if (status == "success" && data.data == "1") {
                    $("#sj_yanzm").text("").removeClass("z-txt s-bg-f0").addClass("z-img-right s-bg-white");
                } else {
                    $("#sj_yanzm").text(lang.a143).removeClass("z-img-right s-bg-white").addClass("z-txt s-bg-f0");
                }
            });
        } else {
            $("#sj_yanzm").text(lang.a143).removeClass("z-img-right s-bg-white").addClass("z-txt s-bg-f0");
        }
    });

    //点击获取验证码
    $(".getAuthCode").click(function () {
        if ($(".getAuthCode").html().match(/\d+/)) {
            showDialog(lang.a145);
        } else {
            $.getJSON("/sendsms/" + $(arr_lis[0]).val() + "/" + $(".hideipt").val(), function (data, status) {
                if (status == "success") {
                    $(".hideipt").val(data.data);
                    getAuthCodeWait(99);
                    showDialog(lang.a14);
                }
            });
        }
    });*/

    $(".u-btn5").click(function () {
        $("input").trigger('blur');
        var numError = $('form .z-txt').length-1;
        if (numError) {
            return false;
        }
        if (!$("#agree").attr("checked")) {
            showDialog(lang.a194);
            return false;
        }
        $("#form").submit();
    });

});
/*
function getAuthCodeWait(i) {
    var t = $(".getAuthCode").html();
    var fun = setInterval(function () {
        var v = $(".getAuthCode").html().match(/(.+?)(\d+)/);
        if (v) {//已进入倒计时
            if (v[2] == "1") {
                $(".getAuthCode").html(t);
                clearInterval(fun);
            } else {
                $(".getAuthCode").html(t + (parseInt(v[2]) - 1));
            }
        } else {//准备进入
            $(".getAuthCode").html(t + i);
        }
    }, 1000);
}*/