﻿$(function () {
    //公共
    $(".money").live("keyup",function () {
        clearNoNum1(this);        
        count_total();        
        count_sfx();
    })

    //中介
    $("#addDomain").click(function () {
        var str = "<p class='u_mt15'>" +
                     "<span class='u-ipt-d u_mr60'><input name='domainName' maxlength='50' class='domainName' type='text'/></span>" +
                     "<span class='pri-txt'>"+lang.a124+"：</span>" +
                     "<span class='u-ipt-d'><input name='money' class='money' maxlength='10' type='text'/></span>" +
                     "<span class='u_del'></span>" +
                    "</p>";
        addElement(str, $("#t-pri"));
        $(".u_del").click(function () {
            addElement(this);
            count_total();
            count_sfx();
        });

        $(".domainName").focusout(function () {
            var v = $(this);
            var t=0;
            if (v.val() != "") {
                $(".domainName").each(function () {
                    if (v.val() == $(this).val()) {
                        t++;
                    }
                })
                if (t>1) {
                    showDialog(lang.a125);
                    v.focus();
                }
            }
        })
    });

    function addElement(str, obj) {
        if (arguments.length > 1) {
            $(arguments[1]).append(arguments[0]);
        } else {
            $(arguments[0]).parent().remove();
        }
    }

    $(".user").keyup(function () {
        clearNoNum1(this);
    }).focus(function () {
        $(".user_tips").css("display", "none");
    }).focusout(function () {
        if ($("#userId")) {
            if ($(this).val() == $("#userId").text().match(/(\d+).*/)[1]) {
                showDialog(lang.a126);
                $(this).val("");
                return;
            }
        }
        $.post("/existsuserid", {"userid": $(this).val()}, function (data, status) {
            if (data.data != 1) {
                $(".user_tips").css("display", "").focus();
            }
        })
    })

    $("input[name='dprice'],input[name='roledprice']").click(function () {
        count_sfx();
    })

    $(".escrow_sub").click(function () {
        if ($("#userId").length<1) {
            showDialog(lang.a66);
            return false;
        }

        //验证域名、价钱
        var k = 0;
        $(".money,.domainName").each(function () {
            if ($(this).val() == "") {
                k = 1;
                return false;
            }
        })

        if (k || $(".user").val() == "")
        {
            showDialog(lang.a127);
            return false;
        }
        //验证会员ID
        if ($(".user_tips").css("display") != "none") {
            showDialog($(".user_tips").html());
            return false;
        }
        //验证角色
        if ($("input[name='dprice']:checked").length < 1) {
            showDialog(lang.a128);
            return false;
        }
        //验证中介费
        if ($("input[name='roledprice']:checked").length < 1) {
            showDialog(lang.a129);
            return false;
        }

//        if (!$("#agree").attr("checked")) {
//            showDialog(lang.a194);
//            return false;
//        }
        
        document.getElementById('form').submit()
    })


    //代购
    $("#btn_pro").click(function () {
//        if ($(".email").length>0) {
//            if ($(".email").val() == "" || $(".email").val() == $(".email").attr("ph") || !/^[0-9a-zA-Z\._-]+@(([0-9a-zA-Z]+)[.])+[a-z]{2,4}$|^1\d{10}$/.test($(".email").val())) {
//                showDialog($(".email").attr("ph"));
//                return false;
//            }
//        }

        if ($(".domainName").val()=="" || $(".domainName").val() == $(".domainName").attr("ph")) {
            showDialog(lang.a130);
            return false;
        }
        if ($("#buyers_price").val()=="" || $("#buyers_price").val() == $("#buyers_price").attr("ph")) {
            showDialog(lang.a131);
            return false;
        }
        if (Number($("#buyers_price").val()) < 5000) {
            showDialog($("#buyers_price").attr("ph"));
            return false;
        }
        
        var domain = $(".domainName").val();
        var buyers_price = $("#buyers_price").val();
        var message = $(".message").val();

        $.post(post_url,{'domain':domain,'buyers_price':buyers_price,'message':message},function(data){
            if(data == 1){
                swal({   title: "增加代购成功!",   text: "增加代购成功.",   timer: 2000,   showConfirmButton: false });
                location.href = "./index.php?c=Account&a=pay";
            }else if(data == 2){
                sweetAlert("异常...", "增加代购异常!", "error");
            }else if(data == 3){
                alert('请输入正确的域名');
                $(".domainName").val("");
                $(".domainName").focus();
            }else if(data == 4){
                swal({   title: "错误",   text: "必须先登陆！",   type: "warning",cancelButtonText: "暂不登陆",
                    showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "马上登陆",   closeOnConfirm: false }, function(){
                    location.href = "./index.php?c=Login&a=index";
                });

            }
        })
        

                
        //是否同意条件  注释
//        if (!$("#agree").attr("checked")) {
//            showDialog(lang.a194);
//            return false;
//        }

//        $("#form").submit();
    })
})

//计算手续费
function count_sfx() {
    var rulo = $("input[name='dprice']:checked").val() || $("input[name='role']:checked").val();
    var rulo_sxf = $("input[name='roledprice']:checked").val() || $("input[name='role1']:checked").val();
    if (!rulo || !rulo_sxf)
        return;

    var sxf_lv = Number($(".money_sxf").attr("sxf")) / 100; //手续费率
    var money_total = Number($(".money_total").html().replace(/[$￥€,.]/g, "")); //交易总额
    var sxf = 0; //手续费
    var result = 0; //您将收到/支出

    switch (rulo_sxf) {
        case "one":
            sxf_lv = rulo == "buy" ? sxf_lv : 0;
            sxf = Math.floor(money_total * sxf_lv);
            result = rulo == "buy" ? (money_total + sxf) : money_total;
            break;
        case "two":
            sxf_lv = rulo == "buy" ? 0 : sxf_lv;
            sxf = Math.floor(money_total * sxf_lv);
            result = rulo == "buy" ? money_total : (money_total - sxf);
            break;
        case "three":
            sxf_lv = sxf_lv / 2;
            sxf = Math.floor(money_total * sxf_lv);
            result = rulo == "buy" ? (money_total + sxf) : (money_total - sxf);
            break;
    }

    $(".money_sxf").html(formatMoney(sxf, 0, "￥"));
    $(".money_result").html(formatMoney(result, 0, "￥"));
    $(".money_result_m").html(rulo == "buy" ? lang.a132 : lang.a133);
}

//计算交易总额
function count_total() {
    var monry_total = 0;
    $(".money").each(function () {
        if ($(this).val() == "")
            return;
        monry_total += Number($(this).val());
    })
    $(".money_total").html(formatMoney(monry_total, 0, "￥"));
}
