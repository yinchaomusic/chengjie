﻿function GetQueryString(name)
{
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if(r!=null)return  unescape(r[2]); return null;
}

$(function () {
    var a;//初始化 当前选中菜单
    var g_c = GetQueryString("c");
    var g_a = GetQueryString("a");
    var g_t = GetQueryString("type");
    var g_p = GetQueryString("page");
    var n; //url
    if(g_c !=null && g_c.toString().length>1)
    {
      var  n_c =g_c;
       // alert(n_c);
        n ="c="+n_c +"&";
    }
    if(g_a !=null && g_a.toString().length>1)
    {
        var n_a = g_a;
        //alert(n_a);
        n +="a="+n_a+"&";
    }
    if(g_t !=null && g_t.toString().length>1)
    {
        var n_t = g_t;
        n +="type="+n_t+"&";
    }
    if(g_p !=null && g_p.toString().length>1)
    {
        var n_p = g_p;
        n +="page="+n_p+"&";
    }

    var li_right = "-999px";
    if ( (n_c == "News") && (n_a =="guide")  ) {//二级菜单
        //if (n == "knowledge" || n == "help" || n == "regulation") {//二级菜单

        li_right = "-919px";
        var sub_a = $(".menu_ul a[href='" + location.href.match(/\/\/.+?(\/.*)/)[1].replace(/#T_a\d+/g,"") + "']");
        $(sub_a).css("background-color", "#FFA200").css("color", "white").css("background-position", "15px -1205px");
        $(sub_a).parent(0).parent(0).css("display","block")
        //a = $(sub_a).parent(0).parent(0).prev();
    } else {//一级菜单

        a = $(".menu_ul a[href*='" + n + "']");
    }

    //切换 li  a背景、图
    if ( (n_c != "News") && (n_a !="guide")) {
        var y = getPos_y(a);
        $(a).parent(0).css("background-position", "200px " + li_right).css("background-color", "#FFA200");
        $(a).css("color", "#fff").css("background-position", "15px " + -(parseInt(y) + 50) + "px");
    }


    //展开区域
    $(".expand").click(function () {
        if ($(this).next().css("display") == "none") {
            $(".expand").next().slideUp("fast");
            $(this).next().slideDown("fast");
            $(this).parent(0).css("background-position", "200px -919px");
        }
        else {
            $(".expand").next().slideUp("fast");
            $(this).next().slideUp("fast");
            $(this).parent(0).css("background-position", "200px -880px");
        }
    })
})