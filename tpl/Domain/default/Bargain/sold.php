<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>最近成交 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bragin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>
<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>优质域名</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="g-content u-cls">
	<div class="m-content">
		<div class='m-ta u_be9 s-bg-fc u_mb6'>
			<div class="title">
                <a class="a2 s-3c" href="{pigcms{:U('Bargain/index')}">
                    <span class="z-img-t18"></span>
                    <span>优质域名</span>
                </a>
                <a class="a1 s-bg-2a" href="javascript:void(0)">
                    <span class="z-img-t16"></span>
                    <span>最近成交</span>
                    <b></b>
                </a>
            </div>
			<div class='u-cls'></div> 
		</div>
		<div class="m-relsult2">
			<div class="g-a-container u_df" id="bidding">
                <table class="u-tab-a">
					<tr>
						<volist name="domains_youzhi_list" id="vo" >
							<td>
								<a href="javascript:void(0)">
									<h4 class="f-p-t1 s-bg-fb" title="{pigcms{$vo.domain}" style="color: rgb(42, 163, 206); background: rgb(251, 251, 251);">{pigcms{$vo.domain}</h4>
									<div class="m-data-DomainList" style="border-color:rgb(255, 255, 255);">
										<p class="f-p6" title="{pigcms{$vo.desc} ">{pigcms{$vo.desc} </p>
										<p class="f-p16"><span>已售</span></p>
									</div>
								</a>
							</td>
							<if condition="$i%4 eq 0"></tr><tr></if>
						</volist>
					</tr>
				</table>
            </div>
			<div class="g-b-container">
				<div class="m-title-b s-bg-55b">
					<span class="recommend">推荐域名</span>
					<span class="refresh">刷新</span>
				</div>
				<table class="tj_domain">
					<volist name="domains_speity_list" id="vo">
						<tr>
							<td width='16px'></td>
							<td width='135px'>
								<div class='u-ina'>
									<a target='_blank' k='{pigcms{$vo.domain_id}' href="{pigcms{:U('Bargain/selling',array('id'=>$vo['domain_id']))}" title='{pigcms{$vo.domain}' class='u-txb s-2a'>{pigcms{$vo.domain}</a>
									<span class='u-txc' title='{pigcms{$vo.desc}'>{pigcms{$vo.desc}</span>
								</div>
							</td>
							<td width='105px'>
								<span class='s-75b u-txd'>￥{pigcms{$vo.money|floor}</span>
							</td>
							<td width='16px'></td>
						</tr>
					</volist>
				</table>
			</div>
			<div class="u-cls"></div>
		</div>
		<div id="paging" class="g-padding">
			<div class='u-fl'><span class='u_fw'>{pigcms{$count_domains_youzhi}</span>条记录</div>
			<div class='u-fr'>{pigcms{$page}</div>
			<div class='u-cls'></div>
		</div>
	</div>
	<div class="u-cls"></div>
</div>
<div class="s-bg-f7"></div>
<include file="Public:footer"/>
<include file="Public:sidebar"/>
 <script type="text/javascript">
	$(document).ready(function () {
		var sUrl = window.location.href;
		//分页按钮
		$("#paging a").live("click", function () {
			if (!$(this).attr("k"))
				return false;
			showLodingImg("0", "50%", "705px", "-150px");
			if (sUrl.indexOf("c=Bargain&a=sold") > 0) {
				yznewdeal($(this).attr("k"));
			} else if (sUrl.indexOf("c=Hotsale&a=sold") > 0) {
				ykjnewdeal($(this).attr("k"));
			}
		})

		function ykjnewdeal(pageindex) {
			$.post('./index.php?c=Bargain&a=sold_ajax', {
				"page": pageindex
			}, function (result) {
				if (result.status == 1) {
					if (result.info.total) {
						var json = result.info.list;
						$(".u-tab-a tr").remove();
						var html = '<tr>';
						if (json.length != 0) {
							$.each(json, function (row, value) { 
								html += "<td"+ (++row%4==0?" class='u_nbr'":"") +"><a href='javascript:void(0)'>"
									 + "<h4 class='f-p-t1 s-bg-fb' title='" + value.domainName + "'>" + value.domainName + "</h4>"
									 + "<div class='m-dataa-DomainList'><p class='f-p6' title='" + value.intro + "'>" + value.intro + "</p>"
									 + "<p class='f-p16'><span>" + lang.a134 + "</span></p></div></a></td>"
								html += row % 4 == 0 ? "</tr><tr>" : "";
							});
						} else {
							html += "<td class='u_norecord' colspan='4'>" + lang.a77 + "</td>";
						}
						html += "</tr>";
						$('#paging').html(nextPage(pageindex, result.info.total, 40, 2));
						$(".u-tab-a").html(html);
						hideLodingImg();
					}
				}
			})
		};

		function yznewdeal(pageindex) {
			$.post('./index.php?c=Hotsale&a=sold_ajax', {
				"page": pageindex
			}, function (data, status) {
				if (status == "success") {
					if (data) {
						var json = jQuery.parseJSON(data);
						$(".u-tab-a tr").remove();
						var html = '<tr>';
						if (json.length != 0) {
							$.each(json, function (row, value) {
								html += "<td" + (++row % 4 == 0 ? " class='u_nbr'" : "") + "><a href='javascript:void(0)'>"
									 + "<h4 class='f-p-t1 s-bg-fb' title='" + value.domainName + "'>" + value.domainName + "</h4>"
									 + "<div class='m-data-DomainList'><p class='f-p6' title='" + value.intro + "'>" + value.intro + "</p>"
									 + "<p class='f-p16'><span>" + lang.a134 + "</span></p></div></a></td>"
								html += row % 4 == 0 ? "</tr><tr>" : "";
							});
						} else {
							html += "<td class='u_norecord' colspan='4'>" + lang.a77 + "</td>";
						}
						html += "</tr>";
						$('#paging').html(nextPage(pageindex, json[0].total, 40, 2));
						$(".u-tab-a").html(html);
						hideLodingImg();
					}
				}
			})
		};

		//推荐 换一批
		$(".refresh").click(function () {
			var type = "3";
			if (sUrl.indexOf("c=Hotsale&a=sold") > -1)
				type = "2";
			showLodingImg("0", "50%", "365px", "350px");
			var id = "";
			$(".m-tj a").each(function () {
				id += $(this).attr("k") + ","
			})
			$.post("./index.php?c=Buydomains&a=recom", { "type": type, "id": id }, function (result) {
				if (result.status == 1) {
					var da = result.info;
					var tb = $(".tj_domain");
					$(".tj_domain tr").remove();
					for (i in da) {
						var url = './index.php?c=Buydomains&a=detail&id=';
						switch(type){
							case "2":
								url = '';
								break;
							case "3":
								url = './index.php?c=Bargain&a=detail&id=';
								break;
						}
						var r = "<tr><td width='16px'></td>"
                        + "<td width='135px'><div class='u-ina'><a target='_blank' k='" + da[i].domain_id + "' href='" + url + da[i].domain_id + "' title='" + da[i].domain + "' class='u-txb s-2a'>" + da[i].domain + "</a><span class='u-txc' title='" + da[i].desc + "'>" + da[i].desc + "</span></div></td>"
                        + "<td width='105px'><span class='s-75b u-txd'>" + (da[i].money == 0 ? lang.a89 : formatMoney(da[i].money, 0, "￥")) + "</span></td>"
                        + "<td width='16px'></td></tr>"
						tb.append(r);
					};
					hideLodingImg();
				} else {
					hideLodingImg();
				}
			})
		})
		$(".tj_domain tr").live("mouseover", function () {
			$(this).css("cursor", "pointer")
		}).live("click", function () {
			window.open($(this).find(".u-ina a").attr("href"));
			return false;
		})
	});
</script>
</body>
</html>