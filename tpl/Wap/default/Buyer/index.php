<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>我是买家</title>
	<else/>
		<title>我是买家-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
</head>

<body class="didding_details">
<header class="header bidding_header">
	<a href="{pigcms{:U('Profile/index')}"><i></i></a>
	<p>我是买家</p>
</header>
<article style="padding-bottom:50px;">
	<section>
                <div class="find_title"><a href="{pigcms{:U('Buyer/deals_complete')}"><span>我是买家</span><span>查看我的交易<i></i></span></a> </div>
		<ul class="find_table clearfix">
                    <li class="clearfix">
                        <a href="{pigcms{:U('Buyer/negotiation')}">
                            <div class="find_img"><img src="{pigcms{$static_path}images/wgcdbj.png"></div>
                            <div class="find_txt"><h3>我给出的报价</h3><p></p></div>
                        </a>
                    </li>
                    <li class="clearfix">
                        <a href="{pigcms{:U('Buyer/partakefast')}">
                            <div class="find_img"><img src="{pigcms{$static_path}images/wcydjj.png"></div>
                            <div class="find_txt"><h3>我参与的竞价</h3><p></p></div>
                        </a>
                    </li>
                    <li class="clearfix">
                        <a href="{pigcms{:U('Buyer/videoauction')}">
                            <div class="find_img"><img src="{pigcms{$static_path}images/wdspjj.png"></div>
                            <div class="find_txt"><h3>我的视频竞价</h3><p></p></div>
                        </a>
                    </li>
                    <li class="clearfix">
                        <a href="{pigcms{:U('Buyer/purchasing')}">
                            <div class="find_img"><img src="{pigcms{$static_path}images/wddgwt.png"/></div>
                            <div class="find_txt"><h3>我的代购委托</h3><p></p></div>
                        </a>
                    </li>
                    <li class="clearfix">
                        <a href="{pigcms{:U('Buyer/deals_complete')}">
                            <div class="find_img"><img src="{pigcms{$static_path}images/wdpljy.png"></div>
                            <div class="find_txt"><h3>我的交易</h3><p></p></div>
                        </a>
                    </li>
		</ul>
	</section>
</article>
<include file="Public:footer"/>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
</body>

</html>