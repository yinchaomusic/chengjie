<html style=" ">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<title>诚介网-详情</title>
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>

<body class="didding_details">
<header class="header bidding_header">
	<a href="{pigcms{:U('Buyer/deals_complete')}"><i></i></a>
	<p>{pigcms{$order_info.domainName} - 详情</p>
<!--        <em class="shear"></em>-->
</header>
<article>
	<section>
		<div class="didding_details_title">
<!--			<h4>{pigcms{$now_domain.domain}</h4><if condition="$collection_show eq 1"><i class="active" id="click_collection"></i><else/><i class="active_click" id="click_collection"></i></if><em></em>-->
			<p>{pigcms{$order_info.domainName}</p>
		</div>
<!--		<ul class="didding_show clearfix">
			<li><span class="didding_show_price">{pigcms{$now_domain.money}元</span>
				<p>当前价格</p>
			</li>
			<li><span>
					<if condition="$now_domain['overplus_time_d']"><strong>{pigcms{$now_domain.overplus_time_d}</strong>天</if><strong>{pigcms{$now_domain.overplus_time_h}</strong>时<strong>{pigcms{$now_domain.overplus_time_m}</strong>分<strong>{pigcms{$now_domain.overplus_time_ms}</strong>秒
				</span>
				<p>剩余时间</p>
			</li>
			<li><span><if condition="$fidList">{pigcms{$_SERVER.REQUEST_TIME|date='Ymd',###}-{pigcms{$fidList.0.uid}<else/>暂无人出价</if></span>
				<p>当前领先</p>
			</li>
		</ul>-->
		<ul class="didding_list">
			<li><span>域名简介</span><span>{pigcms{$order_info.domainName}</span></li>
			<li><span>简介</span><span>{pigcms{$order_info.desc}</span></li>
                        <li><span>报价总额</span><span>￥{pigcms{$order_info.total_price|number_format=###}</span></li>
		</ul>
<!--		<div class="didding_form clearfix">
			<div class="didding_input">
				<input type="text" name="postPrice" value="{pigcms{$now_domain['pleasePrice']}"><i></i>&nbsp;元</div>
			<button>立即出价</button>
		</div>-->
	</section>
</article>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
</body>
<script>
var collectionUrl = "{pigcms{:U('Bargain/collection')}";
</script>
<script>
	var erro_no = {pigcms{$error_no};
	var erro_msg = '{pigcms{$erro_msg}';
	var redirect = '{pigcms{$redirect_url}';
	if(erro_no){
		layer.open({
			content: erro_msg,
			btn: ['OK'],
			shadeClose: true,
			yes: function(){
				window.location.href=redirect;
			}
		});
	}
</script>
</html>