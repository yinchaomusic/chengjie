<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>我的报价</title>
	<else/>
		<title>我的报价-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>
<body class="didding_details">
<header class="header bidding_header">
	<a href="{pigcms{:U('Buyer/index')}" ><i></i></a>
	<p>我的报价</p>
</header>
<article>
	<nav class="auction_title">
		<ul class="clearfix">
			<if condition="$status_type eq 0"><li class="active"><else/><li></if><a href="{pigcms{:U('Buyer/negotiation')}" title="">所有记录</a></li>
			<if condition="$status_type eq 1"><li class="active"><else/><li></if><a href="{pigcms{:U('Buyer/negotiation',array('status'=>1))}" title="">正在记录</a></li>
			<if condition="$status_type eq 2"><li class="active"><else/><li></if><a href="{pigcms{:U('Buyer/negotiation',array('status'=>2))}" title="">议价成功</a></li>
			<if condition="$status_type eq 3"><li class="active"><else/><li></if><a href="{pigcms{:U('Buyer/negotiation',array('status'=>3))}" title="">议价失败</a></li>
		</ul>
	</nav>
	<section>
		<ul class="quote_list">
			<if condition="is_array($domains_quote_list)">
			<volist id="vo" name="domains_quote_list">
				<li>
					<ul>
						<li><span>域名</span><span>{pigcms{$vo.domain}</span></li>
						<li><span>当前状态</span><span><if condition="$vo['status'] eq 0">等待<elseif condition="$vo['status'] eq 1" />成功<elseif condition="$vo['status'] eq 2" />失败</if></span></li>
						<li><span>最新报价</span><span>{pigcms{$vo.new_money|number_format}</span></li>
						<li><span>最后报价时间</span><span>{pigcms{$vo.last_time|date="Y-m-d H:i:s",###}</span></li>
						<li><span>操作</span><span>
								<if condition="$vo['last_offer'] eq 0">
									<if condition="$vo['status'] eq 0">
										<a href="javascript:void();" onclick="oneprice({pigcms{$vo.quote_id},{pigcms{$vo.new_money})"><i>再次出价</i></a><a href="javascript:void();" onclick="cancel({pigcms{$vo.quote_id})"><i>取消议价</i></a>
									<elseif condition="$vo['status'] eq 2"/>
										<i>失败</i>
									</if>
								<elseif condition="$vo['last_offer'] eq 1"/>
									<if condition="$vo['status'] eq 0">
										<a href="javascript:void();" onclick="accept({pigcms{$vo.quote_id})"><i>接受</i></a><a href="javascript:void();" onclick="oneprice({pigcms{$vo.quote_id},{pigcms{$vo.new_money})"><i>回复</i></a><a href="javascript:void();" onclick="cancel({pigcms{$vo.quote_id})"><i>取消</i></a>
									<elseif condition="$vo['status'] eq 1"/>
										<i>成功</i>
									<elseif condition="$vo['status'] eq 2"/>
										<i>失败</i>
									</if>
								</if>
							</span></li>
					</ul>
				</li>
			</volist>
			<else />
					<section style="text-align: center; font-size: larger;color: #a6a6a6;margin-top:30px;">
						<img src="{pigcms{$static_path}images/ui_26.png" style="width:40px;height:40px;margin-bottom:6px;"/><br>
						暂无数据
					</section>
				</if>
		</ul>
	</section>
</article>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script>
function oneprice(obj,new_money){
    var pageii = layer.open({
        type: 1,
        content:'<form method="post" action="index.php?g=Wap&c=Buyer&a=oneprice"><ul class="shopping_list"><li><span>提示</span>您的报价不能小于 <b style="color: red">￥'+new_money+'元</b></li><li><span>价格</span><input type="text" name="money" placeholder="请输入价格"></li></ul><input type="hidden" name="quote_id" value="'+obj+'"/><button class="purchasing_button">确定</button></form><button class="purchasing_button" onclick="layer.closeAll();">关闭</button>',
        style: 'position:fixed; left:0; top:25%; width:100%; height:50%; border:none;'
    });
}


function cancel(quote_id){
    $.post("{pigcms{:U('Buyer/negotiation_cancel')}",{"quote_id":quote_id}, function (result) {
        result = $.parseJSON(result);
        if(result){
            if(result.error == 0){
                layer.open({
                    content: result.msg,
                    btn: ['OK'],
                    shadeClose: true,
                    yes: function(){
                        window.location.href=location.href;
                    }
                });
            }else{
                layer.open({
                    content: result.msg,
                    btn: ['OK']
                });
            }
        }
    });
}

function accept(quote_id){
$.post("{pigcms{:U('Buyer/negotiation_accept')}",{"quote_id":quote_id}, function (result) {
        result = $.parseJSON(result);
        if(result){
            if(result.error == 0){
                layer.open({
                    content: result.msg,
                    btn: ['OK'],
                    shadeClose: true,
                    yes: function(){
                        window.location.href=location.href;
                    }
                });
            }else{
                layer.open({
                    content: result.msg,
                    btn: ['OK']
                });
            }
        }
    });
}
</script>
</body>

</html>