<html style=" ">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<title>诚介网-我委托的代购</title>
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css">
        <link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>

<body class="didding_details">
<header class="header bidding_header">
	<a href="{pigcms{:U('Buyer/index')}" ><i></i></a>
	<p>我委托的代购</p>
</header>
<article>
	<nav class="auction_title">
            <ul class="clearfix">
                <if condition="$status_type eq 0"><li class="active"><else/><li></if><a href="{pigcms{:U('Buyer/purchasing')}" title="">审核状态</a></li>
                <if condition="$status_type eq 1"><li class="active"><else/><li></if><a href="{pigcms{:U('Buyer/purchasing',array('status'=>1))}" title="">代购中</a></li>
                <if condition="$status_type eq 2"><li class="active"><else/><li></if><a href="{pigcms{:U('Buyer/purchasing',array('status'=>2))}" title="">代购成功</a></li>
                <if condition="$status_type eq 3"><li class="active"><else/><li></if><a href="{pigcms{:U('Buyer/purchasing',array('status'=>3))}" title="">代购失败</a></li>
            </ul>
	</nav>
	<section>
            <if condition="is_array($list)">
            <if condition="$status_type eq 1">
                <ul class="quote_list">
                    <volist id="vo" name="list">
                        <li>
                            <ul>
                                <li><span>域名</span><span>{pigcms{$vo.domain}</span></li>
                                <li><span>预算</span><span>￥{pigcms{$vo.buyers_price|number_format}</span></li>
                                <li><span>状态</span><if condition="$vo['status'] eq 0"><span  class="font_default">经纪人未介入</span><elseif condition="$vo['status'] eq 1"/><span  class="font_success">经纪人介入</span><elseif condition="$vo['status'] eq 2"/><span  class="font_default">审核成功等待支付保证金</span><elseif condition="$vo['status'] eq 3"/><span  class="font_warning">审核失败</span><elseif condition="$vo['status'] eq 4"/><span  class="font_default">代购达成</span><elseif condition="$vo['status'] eq 5"/><span  class="font_default">已付款</span><elseif condition="$vo['status'] eq 6"/><span  class="font_default">交易成功</span><elseif condition="$vo['status'] eq 7"/><span  class="font_warning">交易失败</span><elseif condition="$vo['status'] eq 8"/><span  class="font_default">卖家已转移域名</span><elseif condition="$vo['status'] eq 9"/><span  class="font_default">卖家同意</span><elseif condition="$vo['status'] eq 10"/><span  class="font_default">代购达成</span></if></li>
                                <li><span>申请时间</span><span>{pigcms{$vo.add_time|date="Y-m-d H:i:s",###}</span></li>
                                <li><span>操作</span><span><if condition="$vo['status'] eq 5">已付款<elseif condition="$vo['status'] eq 8"/>
                                            <a onclick="confirm({pigcms{$vo.ph_id})"><i>确认收货</i></a> <elseif condition="$vo['status'] eq 9"/>
                                            <a onclick="payment({pigcms{$vo.ph_id})"><i>付款</i></a>
                                            <a onclick="cancel({pigcms{$vo.ph_id})"><i>取消</i></a><elseif condition="$vo['status'] eq 4"/>等待经纪人联系卖家</if></span></li>
                                <input type="hidden" id="ph_id" value="{pigcms{$vo.ph_id}"/>
                            </ul>
			</li>
                    </volist>
		</ul>
            <elseif condition="$status_type eq 2"/>
                <ul class="quote_list">
                    <volist id="vo" name="list">
                        <li>
                            <ul>
                                <li><span>域名</span><span>{pigcms{$vo.domain}</span></li>
                                <li><span>预算</span><span>￥{pigcms{$vo.buyers_price|number_format}</span></li>
                                <li><span>状态</span><if condition="$vo['status'] eq 0"><span  class="font_default">经纪人未介入</span><elseif condition="$vo['status'] eq 1"/><span  class="font_success">经纪人介入</span><elseif condition="$vo['status'] eq 2"/><span  class="font_default">审核成功等待支付保证金</span><elseif condition="$vo['status'] eq 3"/><span  class="font_warning">审核失败</span><elseif condition="$vo['status'] eq 4"/><span  class="font_default">代购达成</span><elseif condition="$vo['status'] eq 5"/><span  class="font_default">已付款</span><elseif condition="$vo['status'] eq 6"/><span  class="font_default">交易成功</span><elseif condition="$vo['status'] eq 7"/><span  class="font_warning">交易失败</span><elseif condition="$vo['status'] eq 8"/><span  class="font_default">卖家已转移域名</span><elseif condition="$vo['status'] eq 9"/><span  class="font_default">卖家同意</span><elseif condition="$vo['status'] eq 10"/><span  class="font_default">代购达成</span></if></li>
                                <li><span>申请时间</span><span>{pigcms{$vo.add_time|date="Y-m-d H:i:s",###}</span></li>
                                <input type="hidden" id="ph_id" value="{pigcms{$vo.ph_id}"/>
                            </ul>
                        </li>
                    </volist>
                </ul>
            <elseif condition="$status_type eq 3"/>
                <ul class="quote_list">
                    <volist id="vo" name="list">
                        <li>
                            <ul>
                                <li><span>域名</span><span>{pigcms{$vo.domain}</span></li>
                                <li><span>预算</span><span>￥{pigcms{$vo.buyers_price|number_format}</span></li>
                                <li><span>状态</span><if condition="$vo['status'] eq 0"><span  class="font_default">经纪人未介入</span><elseif condition="$vo['status'] eq 1"/><span  class="font_success">经纪人介入</span><elseif condition="$vo['status'] eq 2"/><span  class="font_default">审核成功等待支付保证金</span><elseif condition="$vo['status'] eq 3"/><span  class="font_warning">审核失败</span><elseif condition="$vo['status'] eq 4"/><span  class="font_default">代购达成</span><elseif condition="$vo['status'] eq 5"/><span  class="font_default">已付款</span><elseif condition="$vo['status'] eq 6"/><span  class="font_default">交易成功</span><elseif condition="$vo['status'] eq 7"/><span  class="font_warning">交易失败</span><elseif condition="$vo['status'] eq 8"/><span  class="font_default">卖家已转移域名</span><elseif condition="$vo['status'] eq 9"/><span  class="font_default">卖家同意</span><elseif condition="$vo['status'] eq 10"/><span  class="font_default">代购达成</span></if></li>
                                <li><span>申请时间</span><span>{pigcms{$vo.add_time|date="Y-m-d H:i:s",###}</span></li>
                                <input type="hidden" id="ph_id" value="{pigcms{$vo.ph_id}"/>
                            </ul>
                        </li>
                    </volist>
                </ul>
            <else/>
		<ul class="quote_list">
                    
                    <div class="find_title"><span>审核中</span></div>
                    <volist id="vo" name="list">
                        <if condition="$vo['status'] lt  2">
                        <li>
                            <ul>
                                <li><span>域名</span><span>{pigcms{$vo.domain}</span></li>
                                <li><span>预算</span><span>￥{pigcms{$vo.buyers_price|number_format}</span></li>
                                <li><span>状态</span><if condition="$vo['status'] eq 0"><span  class="font_default">经纪人未介入</span><elseif condition="$vo['status'] eq 1"/><span  class="font_success">经纪人介入</span><elseif condition="$vo['status'] eq 2"/><span  class="font_default">审核成功等待支付保证金</span><elseif condition="$vo['status'] eq 3"/><span  class="font_warning">审核失败</span><elseif condition="$vo['status'] eq 4"/><span  class="font_default">代购达成</span><elseif condition="$vo['status'] eq 5"/><span  class="font_default">已付款</span><elseif condition="$vo['status'] eq 6"/><span  class="font_default">交易成功</span><elseif condition="$vo['status'] eq 7"/><span  class="font_warning">交易失败</span><elseif condition="$vo['status'] eq 8"/><span  class="font_default">卖家已转移域名</span><elseif condition="$vo['status'] eq 9"/><span  class="font_default">卖家同意</span><elseif condition="$vo['status'] eq 10"/><span  class="font_default">代购达成</span></if></li>
                                <li><span>申请时间</span><span>{pigcms{$vo.add_time|date="Y-m-d H:i:s",###}</span></li>
                                <li><span>操作</span><span><a href="{pigcms{:U('Buyer/purchasing_save',array('ph_id'=>$vo['ph_id']))}"><i>修改</i></a><a onclick="purchasingDel({pigcms{$vo.ph_id})"><i>删除</i></a></span></li>
                                <input type="hidden" id="ph_id" value="{pigcms{$vo.ph_id}"/>
                            </ul>
			</li>
                        </if>
                    </volist>
                    <div class="find_title"><span>审核成功</span></div>
                    <volist id="vv" name="list">
                        <if condition="$vv['status'] eq  2">
                        <li>
                            <ul>
                                <li><span>域名</span><span>{pigcms{$vv.domain}</span></li>
                                <li><span>预算</span><span>￥{pigcms{$vv.buyers_price|number_format}</span></li>
                                <li><span>状态</span><if condition="$vv['status'] eq 0"><span  class="font_default">经纪人未介入</span><elseif condition="$vv['status'] eq 1"/><span  class="font_success">经纪人介入</span><elseif condition="$vv['status'] eq 2"/><span  class="font_default">审核成功等待支付保证金</span><elseif condition="$vv['status'] eq 3"/><span  class="font_warning">审核失败</span><elseif condition="$vv['status'] eq 4"/><span  class="font_default">代购达成</span><elseif condition="$vv['status'] eq 5"/><span  class="font_default">已付款</span><elseif condition="$vv['status'] eq 6"/><span  class="font_default">交易成功</span><elseif condition="$vv['status'] eq 7"/><span  class="font_warning">交易失败</span><elseif condition="$vv['status'] eq 8"/><span  class="font_default">卖家已转移域名</span><elseif condition="$vv['status'] eq 9"/><span  class="font_default">卖家同意</span><elseif condition="$vv['status'] eq 10"/><span  class="font_default">代购达成</span></if></li>
                                <li><span>申请时间</span><span>{pigcms{$vo.add_time|date="Y-m-d H:i:s",###}</span></li>
                                <li><span>操作</span><span><a onclick="paymargin({pigcms{$vv.ph_id})"><i>支付</i></a></span></li>
                                <input type="hidden" id="ph_id" value="{pigcms{$vv.ph_id}"/>
                            </ul>
			</li>
                        </if>
                    </volist>
                    <div class="find_title"><span>审核失败</span></div>
                    <volist id="voo" name="list">
                        <if condition="$voo['status'] eq 3">
                        <li>
                            <ul>
                                <li><span>域名</span><span>{pigcms{$voo.domain}</span></li>
                                <li><span>预算</span><span>￥{pigcms{$voo.buyers_price|number_format}</span></li>
                                <li><span>状态</span><if condition="$voo['status'] eq 0"><span  class="font_default">经纪人未介入</span><elseif condition="$voo['status'] eq 1"/><span  class="font_success">经纪人介入</span><elseif condition="$voo['status'] eq 2"/><span  class="font_default">审核成功等待支付保证金</span><elseif condition="$voo['status'] eq 3"/><span  class="font_warning">审核失败</span><elseif condition="$voo['status'] eq 4"/><span  class="font_default">代购达成</span><elseif condition="$voo['status'] eq 5"/><span  class="font_default">已付款</span><elseif condition="$voo['status'] eq 6"/><span  class="font_default">交易成功</span><elseif condition="$voo['status'] eq 7"/><span  class="font_warning">交易失败</span><elseif condition="$voo['status'] eq 8"/><span  class="font_default">卖家已转移域名</span><elseif condition="$voo['status'] eq 9"/><span  class="font_default">卖家同意</span><elseif condition="$voo['status'] eq 10"/><span  class="font_default">代购达成</span></if></li>
                                <li><span>申请时间</span><span>{pigcms{$voo.add_time|date="Y-m-d H:i:s",###}</span></li>
                                <li><span>操作</span><span><a onclick="purchasingDel({pigcms{$voo.ph_id})"><i>删除</i></a></span></li>
                                <input type="hidden" id="ph_id" value="{pigcms{$voo.ph_id}"/>
                            </ul>
			</li>
                        </if>
                    </volist>
		</ul>
            </if>
        <else />
            <section style="text-align: center; font-size: larger;color: #a6a6a6;margin-top: 10px;">
                <img src="{pigcms{$static_path}images/ui_26.png" alt=""><br>
                    暂无数据
            </section>
    </if>
	</section>
</article>
    
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>

<script>
    function paymargin(obj){
        layer.open({
            content: '你是想支付保证金吗？',
            btn: ['确认', '取消'],
            shadeClose: true,
            yes: function(){
                $.post("{pigcms{:U('Buyer/freeze_margin')}", {"ph_id":obj}, function(result){
                    result = $.parseJSON(result);
                    if(result){
                        if(result.error == 0){
                            layer.open({
                                content: result.msg,
                                btn: ['OK'],
                                shadeClose: true,
                                yes: function(){
                                    window.location.href=location.href;
                                }
                            });
                        }else{
                            layer.open({
                                content: result.msg,
                                btn: ['OK']
                            });
                        }
                    }
                })
            }, no: function(){
                layer.open({content: '你选择了取消', time: 1});
            }
        });
    }
    function cancel(ph_id){
        var cancel_url = "{pigcms{:U('Buyer/cancel')}"
        layer.open({
            content: '你是想取消吗，如果取消保证金将作为违约金？',
            btn: ['确认', '取消'],
            shadeClose: true,
            yes: function(){
                $.post(cancel_url,{"ph_id":ph_id},function(result){
                    alert(result);
                    result = $.parseJSON(result);
                    if(result){
                        if(result.error == 0){
                            layer.open({
                                content: result.msg,
                                btn: ['OK'],
                                shadeClose: true,
                                yes: function(){
                                    window.location.href=location.href;
                                }
                            });
                        }else{
                            layer.open({
                                content: result.msg,
                                btn: ['OK']
                            });
                        }
                    }
                })
            }, no: function(){
                layer.open({content: '你选择了取消', time: 1});
            }
        });
    }
    //确认收货
    function confirm(obj){
        layer.open({
            content: '你是想确认收货，还是点错了？',
            btn: ['确认', '取消'],
            shadeClose: true,
            yes: function(){
                $.post("{pigcms{:U('Buyer/pay_confirm')}",{"ph_id":obj},function(result){
                    result = $.parseJSON(result);
                    if(result){
                        if(result.error == 0){
                            layer.open({
                                content: result.msg,
                                btn: ['OK'],
                                shadeClose: true,
                                yes: function(){
                                    window.location.href=location.href;
                                }
                            });
                        }else{
                            layer.open({
                                content: result.msg,
                                btn: ['OK']
                            });
                        }
                    }
                })
            }, no: function(){
                layer.open({content: '你选择了取消', time: 1});
            }
        });
    }
//付款
function payment(obj){
    layer.open({
        content: '你是想付款，还是点错了？',
        btn: ['确认', '取消'],
        shadeClose: true,
        yes: function(){
            $.post("{pigcms{:U('Buyer/payment')}",{"ph_id":obj},function(result){
                result = $.parseJSON(result);
                if(result){
                    if(result.error == 0){
                        layer.open({
                            content: result.msg,
                            btn: ['OK'],
                            shadeClose: true,
                            yes: function(){
                                window.location.href=location.href;
                            }
                        });
                    }else{
                        layer.open({
                            content: result.msg,
                            btn: ['OK']
                        });
                    }
                }
            })
        }, no: function(){
            layer.open({content: '你选择了取消', time: 1});
        }
    });
}
</script>
<script>
function purchasingDel(obj){
    layer.open({
        content: '你是想确认呢，还是想取消呢？',
        btn: ['确认', '取消'],
        shadeClose: true,
        yes: function(){
            $.post("{pigcms{:U('Buyer/purchasing_del')}",{"ph_id":obj},function(data){
                var data = $.parseJSON(data);
                if(data){
                    if(data.error == 0){
                        layer.open({
                            content: data.msg,
                            btn: ['OK'],
                            shadeClose: true,
                            yes: function(){
                                window.location.href=location.href;
                            }
                        });
                    }
                }
            })
        }, no: function(){
            layer.open({content: '你选择了取消', time: 1});
        }
    });
}
</script>
</body>

</html>