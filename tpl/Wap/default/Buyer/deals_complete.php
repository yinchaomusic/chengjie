<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>我的所有交易</title>
	<else/>
		<title>我的所有交易-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>

<body class="didding_details">
<header class="header bidding_header">
	<a href="{pigcms{:U('Buyer/index')}" ><i></i></a>
	<p>我的所有交易</p>
</header>
<article>
	<nav class="auction_title">
            <ul class="clearfix">
                <if condition="$status_type eq 0"><li class="active"><else/><li></if><a href="{pigcms{:U('Buyer/deals_complete')}" title="">所有交易</a></li>
                <if condition="$status_type eq 1"><li class="active"><else/><li></if><a href="{pigcms{:U('Buyer/deals_complete',array('status'=>1))}" title="">进行中</a></li>
                <if condition="$status_type eq 2"><li class="active"><else/><li></if><a href="{pigcms{:U('Buyer/deals_complete',array('status'=>2))}" title="">已完成</a></li>
                <if condition="$status_type eq 3"><li class="active"><else/><li></if><a href="{pigcms{:U('Buyer/deals_complete',array('status'=>3))}" title="">已取消</a></li>
            </ul>
	</nav>
	<section>
            
		<ul class="quote_list">
                <if condition="is_array($order_list)">
                    <volist id="vo" name="order_list">
                        <li>
                            <ul>
                                <li><span>域名</span><span>{pigcms{$vo.domainName}</span></li>
                                <li><span>金额</span><span>￥{pigcms{$vo.total_price|number_format}</span></li>
                                <li><span>来源</span><span ><if condition="$vo['trade_type'] eq 0">交易<elseif condition="$vo['trade_type'] eq 1"/>中介<else/>批量</if></span></li>
                                <li><span>状态</span><span class="font_default"><if condition="$vo['yes_no'] eq 1">
                                            等待买家同意条款
                                            <elseif condition="$vo['yes_no'] eq 2" />
                                            等待卖家同意条款
                                            <elseif condition="$vo['yes_no'] eq 3" />
                                            等待买家付钱
                                            <elseif condition="$vo['yes_no'] eq 4" />
                                            等待卖家转移域名
                                            <elseif condition="$vo['yes_no'] eq 5" />
                                            买家拒绝
                                            <elseif condition="$vo['yes_no'] eq 6" />
                                            卖家拒绝
                                            <elseif condition="$vo['yes_no'] eq 7" />
                                            后台关闭
                                            <elseif condition="$vo['yes_no'] eq 8" />
                                            经纪人关闭
                                            <elseif condition="$vo['yes_no'] eq 9" />
                                            等待买家确认收货
                                            <elseif condition="$vo['yes_no'] eq 10" />
                                            交易达成
                                        </if></span></li>
                                <li><span>操作</span><span>
                                        <if condition="$vo['behavior'] eq 1">
                                            <if condition="$vo['yes_no'] eq 1">
                                                <a class="u-btn13" id="save">修改</a>
                                                <a class="u-btn13" id="no">拒绝</a>
                                                <a class="u-btn13" id="yes">同意</a>
                                                <elseif condition="$vo['yes_no'] eq 3 "/>
                                                <a class="u-btn13" onclick="subdeals({pigcms{$vo.order_id})" id="sub"><i>提交付款</i></a> 
                                                <elseif condition="$vo['yes_no'] eq 9 "/>
                                                <a class="u-btn13" onclick="successful({pigcms{$vo.order_id})"><i>确认收货</i></a>
                                            </if>
                                    <else/>
                                        <if condition="$vo['yes_no'] eq 2">
                                            <a class="u-btn13" id="save">修改</a>
                                            <a class="u-btn13" id="no">拒绝</a>
                                            <a class="u-btn13" id="yes">同意</a>
                                                <elseif condition="$vo['yes_no'] eq 4 "/>
                                                   <a class="u-btn13" href="javascript:void(0);" onclick="changedeal({pigcms{$vo.order_id})" ><i>等待您提交域名</i></a>
                                        </if>
                                </if>
<!--                                        <a onclick="detail({pigcms{$vo['order_id']})"><i>详情</i></a></span></li>-->
                                        <a href="{pigcms{:U('Buyer/dealdetails',array('id'=>$vo['order_id']))}"><i>详情</i></a></span></li>
                                
                            </ul>
			</li>
                    </volist>
                <else/>
                    <section style="text-align: center; font-size: larger;color: #a6a6a6;margin-top:20px;">
                            <img src="{pigcms{$static_path}images/ui_26.png" style="width:40px;height:40px;margin-bottom:6px;"/><br>
                            暂无数据
                    </section>
            </if>
		</ul>
                
	</section>
</article>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script>
    function changedeal(order_id){
        var pageii = layer.open({
            type: 1,
            content:'<form method="post" action="index.php?g=Wap&c=Buyer&a=change_dealdetails"><ul class="shopping_list"><li>请填写您当前的注册商和注册商帐号</li><li><span>注册商：</span><input type="text" name="registrar"/></li><li><span>注册商ID：</span><input type="text" name="registrar_id"/></li></ul><input type="hidden" name="order_id" value="'+order_id+'"/> <button class="purchasing_button">提交域名</button></form><button class="purchasing_button" onclick="layer.closeAll();">关闭</button>',
            style: 'position:fixed; left:0; top:0; width:100%; height:100%; border:none;'
        });
    }
    function successful(obj){
        layer.open({
            content: '你是想确认收货吗？',
            btn: ['确认', '取消'],
            shadeClose: true,
            yes: function(){
                $.post("{pigcms{:U('Buyer/successful')}", {"order_id":obj}, function(result){
                    result = $.parseJSON(result);
                    if(result){
                        if(result.error == 0){
                            layer.open({
                                content: result.msg,
                                btn: ['OK'],
                                shadeClose: true,
                                yes: function(){
                                    window.location.href=location.href;
                                }
                            });
                        }else{
                            layer.open({
                                content: result.msg,
                                btn: ['OK']
                            });
                        }
                    }
                })
            }, no: function(){
                layer.open({content: '你选择了取消', time: 1});
            }
        });
    }
    
    
    function subdeals(obj){
        var pageii = layer.open({
            type: 1,
            content:'<form method="post" action="index.php?g=Wap&c=Buyer&a=order_pay"><ul class="shopping_list"><li>转移密码：由卖家提供，您在您的新注册商申请域名转入时提交填写。3~7个工作日即可完成过户。。 </li><li>您提交易域名所在注册商的账号，卖家操作push,您接收即可。1小时内甚至几分钟即可完成过户（要求域名所在注册商支持push过户方式）。。</li><li><span>过户方式：</span></li><li><span>卖家</span><input checked="checked" name="transfer_type" value="1" type="radio"></li><li><span>买家</span><input name="transfer_type"  value="2" type="radio"></li><li><span>注册商：</span><input type="text" name="registrar"/></li><li><span>注册商ID：</span><input type="text" name="registrar_id"/></li></ul><input type="hidden" name="order_id" value="'+obj+'"/> <button class="purchasing_button">付款</button></form><button class="purchasing_button" onclick="layer.closeAll();">关闭</button>',
            style: 'position:fixed; left:0; top:0; width:100%; height:100%; border:none;'
        });
    }
    
    
function detail(obj){
    var pageii = layer.open({
        type: 1,
        content:'<button class="purchasing_button" onclick="layer.closeAll();">关闭</button>',
        style: 'position:fixed; left:0; top:0; width:100%; height:100%; border:none;'
    });
}
</script>
</body>

</html>