<html style=" ">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<title>诚介网-我参与的竞价</title>
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css">
</head>

<body class="didding_details">
<header class="header bidding_header">
	<a href="{pigcms{:U('Buyer/index')}" ><i></i></a>
	<p>我参与的竞价</p>
</header>
<article>
	<nav class="auction_title">
		<ul class="clearfix">
                        <if condition="$status_type eq 0"><li class="active"><else/><li></if><a href="{pigcms{:U('Buyer/videoauction')}" title="">所有</a></li>
                        <if condition="$status_type eq 1"><li class="active"><else/><li></if><a href="{pigcms{:U('Buyer/videoauction',array('status'=>1))}" title="">已得标</a></li>
		</ul>
	</nav>
	<section>
            <ul class="quote_list">
                <if condition="is_array($list)">
                    <volist id="vo" name="list">
                        <li>
                            <ul>
                                <li><span>域名</span><span>{pigcms{$vo.domains}</span></li>
                                <li><span>出价金额</span><span>￥{pigcms{$vo.money|number_format}</span></li>
                                <li><span>出价时间</span><span>{pigcms{$vo.time|date="Y-m-d H:i:s",###}</span></li>
                            </ul>
                        </li>
                    </volist>
                <else />
                <section style="text-align: center; font-size: larger;color: #a6a6a6;margin-top: 10px;">
                        <img src="{pigcms{$static_path}images/ui_26.png" alt=""><br>
                        暂无数据
                </section>
        </if>
            </ul>
	</section>
</article>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
</body>

</html>