<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name='apple-touch-fullscreen' content='yes'>
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
    <title>诚介网-域名代购-修改</title>
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>

<body class=" ">
<header class="header">
    <a href="{pigcms{:U('Buyer/purchasing')}"><i></i></a>
    <p>域名代购-修改</p>
</header>
<article>
    <section>
        <ul class="shopping_list">
            <li><span>购买的域名</span>
                <input type="text" name="domain" class="domainName" value="{pigcms{$show.domain}"  placeholder="请输入域名">
            </li>
            <li><span>心理预算</span>
                <input type="text" name="buyers_price" class="money" value="{pigcms{$show.buyers_price|floor}" placeholder="最低5000元人民币" onkeyup='this.value=this.value.replace(/\D/gi,"")'>
            </li>
            <li><span>附加信息</span>
                <textarea class="message" name="message" placeholder="留言或其它附加信息">{pigcms{$show.message}</textarea>
            </li>

        </ul>
        <input type="hidden" id="ph_id" name="ph_id" value="{pigcms{$show.ph_id}">
        <input type="hidden" id="userId" value="{pigcms{$user_session.uid}">
        <button class="purchasing_button" id="btn_pro">提交申请</button>
    </section>
</article>
</body>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script>
    $("#btn_pro").click(function(){
        
        var post_url = "{pigcms{:U('Buyer/purchasing_save_data')}";
        if ($("#userId").val()<1) {
            layer.open({
                content: "域名代购，需要您先登录",
                btn: ['OK'],
                shadeClose: true,
                yes: function(){
                        window.location.href="{pigcms{:U('Login/login')}";
                }
            });
        }
        if ($(".domainName").val()=="" || $(".domainName").val() == $(".domainName").attr("placeholder")) {
            layer.open({
                content: "域名不可以为空",
                btn: ['OK']
            });
            return false;
        }
        if ($(".money").val()=="" || $(".money").val() == $(".money").attr("placeholder")) {
            layer.open({
                content: "您的心里预算价格不可以为空",
                btn: ['OK']
            });
            return false;
        }
        if (Number($(".money").val()) < 5000) {
            layer.open({
                content: $(".money").attr("placeholder"),
                btn: ['OK']
            });
            return false;
        }
        
        var domain = $(".domainName").val();
        var buyers_price = $(".money").val();
        var message = $(".message").val();
        var ph_id = $("#ph_id").val();
        $.post(post_url,{'ph_id':ph_id,'domain':domain,'buyers_price':buyers_price,'message':message},function(result){
            result = $.parseJSON(result);
            if(result){
                if(result.error == 0){
                    layer.open({
                        content: result.msg,
                        btn: ['OK'],
                        shadeClose: true,
                        yes: function(){
                            window.location.href="/index.php?g=Wap&c=Buyer&a=purchasing";
                        }
                    });
                }else{
                    layer.open({
                        content: result.msg,
                        btn: ['OK']
                    });
                    $(".domainName").focus();
                }
            }
        })
    })
</script>
</html>