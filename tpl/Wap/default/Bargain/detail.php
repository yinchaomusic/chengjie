<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>域名详情</title>
	<else/>
		<title>域名详情-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>

<body class="didding_details">
<header class="header bidding_header">
	<a href="{pigcms{:U('Bargain/index')}"><i></i></a>
	<p>域名详情</p>
</header>
<article>
	<section>
		<div class="didding_details_title">
			<h4>{pigcms{$now_domain.domain}</h4><if condition="$collection_show eq 1"><i class="active" id="click_collection"></i><else/><i class="active_click" id="click_collection"></i></if><em></em>
                        <input type="hidden" value="{pigcms{$user_session.uid}" id="suid"/>
                        <input type="hidden" value="{pigcms{$now_domain.domain_id}" id="domainid" />
                        <input type="hidden" value="{pigcms{$now_domain.domain}" id="domain_name" />
                        <input type="hidden" value="{pigcms{$now_domain.type}" id="domain_type" />
			<p>{pigcms{$now_domain.desc}</p>
		</div>
		<ul class="didding_list mg_top">
			<li><span>当前价</span><span>￥{pigcms{$now_domain.money|number_format}元</span></li>
			<li><span>专属经纪人</span><span><a href="tel:{pigcms{$now_staff.phone}">{pigcms{$now_staff.name}</a> </span></li>
<!--			<li><span>12541</span><span>查看该用户其它域名<i></i></span></li>-->
			<li><span>浏览次数</span><span>{pigcms{$now_domain.hits}</span></li>

		</ul>
		<div class="didding_form clearfix">
			<a href="tel:{pigcms{$now_staff.phone}"><button>拨打电话</button></a> &nbsp;&nbsp; <a href="http://wpa.qq.com/msgrd?v=3&uin={pigcms{$now_staff.qq}&site=qq&menu=yes"><button>QQ联系</button></a>
<!--                                                                                                             <a href="http://wpa.qq.com/msgrd?v=3&uin=502146117&site=qq&menu=yes"></a>-->
                        
<!--                        <a href="tencent://message/?uin={pigcms{$now_staff.qq}&Site=诚介网"><button>QQ联系</button></a>-->
		</div>
	</section>
</article>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
<script src="{pigcms{$static_path}js/rem.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script>
var collectionUrl = "{pigcms{:U('Bargain/collection')}";
</script>
<script>
	var erro_no = "{pigcms{$error_no}";
        var erro_msg = '{pigcms{$erro_msg}';
	var index_url = "{pigcms{:U('Home/index')}";
	if(erro_no){
		layer.open({
			content: erro_msg,
			btn: ['OK'],
			shadeClose: true,
			yes: function(){
				window.location.href=index_url;
			}
		});
	}
</script>
</body>

</html>