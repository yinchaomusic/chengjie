<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name='apple-touch-fullscreen' content='yes'>
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>Whois查询</title>
	<else/>
		<title>Whois查询-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css?t={pigcms{$_SERVER.REQUEST_TIME}">
</head>
<body>
<if condition="!$is_wexin_browser">
<header class="header">
    <a href="#" onclick="window.history.go(-1);"><i></i></a>
    <p>Whois查询</p>
</header>
</if>
<article>
    <section>
        <div class="whois_sec clearfix">
            <form method="post" action="{pigcms{:U('Whois/whois')}" id="whois_form">
				<div class="shopping_search">
					<i></i><input type="text" id="keyword" name="keyword" value="{pigcms{$keyword}" placeholder="whois"/><i class="close"></i>
				</div>
				<button  type="submit">搜索</button>
            </form>
        </div>
        <ul class="shopping_list intermediary_list">
            <li>
				<span>域名</span>
                <span>{pigcms{$whoisResult.domainName}</span>
            </li>
            <li><span>注册商</span>
                <span>{pigcms{$whoisResult.registrar}</span>
            </li>
            <li><span>注册人</span>
                <span>{pigcms{$whoisResult.registrarName}</span>
            </li>
            <li><span>注册人邮箱</span>
                <span>{pigcms{$whoisResult.registrarEmail}</span>
            </li>
            <li><span>注册时间</span>
                <span>{pigcms{$whoisResult.creationDate}</span>
            </li>
            <li>
				<span>更新时间</span>
                <span>{pigcms{$whoisResult.updatedDate}</span>
            </li>
            <li>
				<span>过期时间</span>
                <span>{pigcms{$whoisResult.expirationDate}</span>
            </li>
            <li>
				<span>域名状态</span>
                <span>{pigcms{$whoisResult.domainStatus}</span>
            </li>
            <li>
				<span class="clearfix">DNS服务器</span>
                <span>{pigcms{$whoisResult.nameServer}</span>
            </li>

        </ul>
        <a href="{pigcms{:U('Procurement/index',array('d'=>$whoisResult['domainName']))}"><button class="purchasing_button">联系购买</button></a>
    </section>
</article>
</body>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
<script src="{pigcms{$static_path}js/rem.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
<script>
    var erro_no = {pigcms{$error_no};
    var erro_msg = '{pigcms{$erro_msg}';
    var index_url = "{pigcms{:U('Home/index')}";
    if(erro_no){
        layer.open({
            content: erro_msg,
            btn: ['OK'],
            shadeClose: true,
            yes: function(){
                window.location.href=index_url;
            }
        });
    }

//    var whois_url = '{pigcms{:U('Whois/whois')}';
//    $(function(){
//        $('#whois_form').submit(function(){
//
//            if($('#keyword').val()=='' || $('#keyword').val()==$('#keyword').attr('ph')){
//                layer.open({
//                    content: '请先输入您要搜索Whois的域名',
//                    btn: ['OK'],
//                    shadeClose: true
//
//                });
//                $('#keyword').focus();
//                return false;
//            }else{
//                $.post(whois_url,$("#whois_form").serialize(),function(result){
//                    result = $.parseJSON(result);
//                    if(result){
//                        if(result.error == 0){
//                            //swal('','登录成功','success');
//                            setTimeout(function(){
//                                window.parent.location = user_index;
//                            },1000);
//                        }else{
//                            $('#keyword').focus();
//                           // swal({   title: '登录出现错误' ,   text: result.msg ,type:"error",   timer: 6000,   showConfirmButton: true });
//                        }
//                    }else{
//                        layer.open({
//                            content: '搜索出现异常,请重试.',
//                            btn: ['OK'],
//                            shadeClose: true
//
//                        });
//                    }
//                });
//            }
//            return false;
//        });
//    });


</script>
</html>