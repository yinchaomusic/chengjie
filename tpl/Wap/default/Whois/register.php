<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name='apple-touch-fullscreen' content='yes'>
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<title>诚介网-查询结果</title>
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css">
</head>

<body class="results_content">
<header class="header">
	<a href="#" onclick="javascript:history.go(-1);"><i></i></a>
	<p>查询结果</p>
</header>
<article>
	<section class="results_list">

  <if condition="$is_reg">
		<div class="results_title sure_title">
			<span>可注册</span>
		</div>
		<ul class="sure">
			<if conditioin="$oneresult">
			<li class="clearfix"><span class="label"> <i><input type="radio" id="checked" name="sport" value="nba" checked="checked">
						<label name="nba" for="nba" class=" "> </label></i>1111.com</span><span><em>whois</em></span><span>100元/年<b  class="active_clicks"></b></span></li>
		   </if>

		</ul>
		<button type="text">立即注册</button>

	  </if>

		<div class="results_title cannot_title">
			<span>不可注册</span>
		</div>
		<ul class="cannot">
			<if condition="$oneresult">
			<li class="clearfix"><span>{pigcms{$result['key']}</span><span><em><a href="{pigcms{:U('Whois/whois',array('keyword'=>$result['key']))}">whois</a></em></span><span><a href="{pigcms{:U('Procurement/index',array('id'=>$result['key']))}">联系购买</a></span></li>
			</if>

		</ul>
		<if condition="$likeits">
		<div class="results_title guess_title">
			<span>猜你喜欢</span>
		</div>
		<ul class="guess">
			<volist name="likeits" id="vo">
			<li class="clearfix"><span>{pigcms{$vo.domain}</span><span>
					<if condition="$vo['type'] eq 1">
						一口价
						<elseif condition="$vo['type'] eq 0" />
						议价
						<elseif condition="$vo['type'] eq 2" />
						竞价
						<elseif condition="$vo['type'] eq 3" />
						优质
					</if> {pigcms{$vo.money|number_format}元
				</span><span>
					<a href="
						<if condition="$vo['type'] eq 1">
						{pigcms{:U('Hotsale/selling',array('id'=>$vo['domain_id']))}
						 <elseif condition="$vo['type'] eq 0" />
							{pigcms{:U('Buydomains/detail',array('id'=>$vo['domain_id']))}
						 <elseif condition="$vo['type'] eq 2" />
							{pigcms{:U('Fastbid/detail',array('id'=>$vo['domain_id']))}
						<elseif condition="$vo['type'] eq 3" />
							{pigcms{:U('Bargain/selling',array('id'=>$vo['domain_id']))}
		</if>
		">
						立即出价</a></span></li>
			</volist>
		</ul>
	   </if>
	</section>
</article>
</body>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>

</html>