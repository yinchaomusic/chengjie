<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>购买域名</title>
	<else/>
		<title>购买域名-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css?t={pigcms{$_SERVER.REQUEST_TIME}">
</head>

<body class="">
<if condition="!$is_wexin_browser">
<header class="header">
    <a href="{pigcms{:U('Find/index')}"><i></i></a>
    <p>淘域名</p>
</header>
</if>
<article>
    <section class="bidding">
        <ul class="clearfix mg_top">
        </ul>
    </section>
    <form method="post" id="serach_from">
    <section>
        <div class="shopping_search"><i></i>
            <input type="text"  placeholder="搜索的域名关键词"/>
        </div>
        <div class="shopping_title type_title">
            <i></i>交易类型
        </div>
        <ul class="type_list clearfix">
            <li class="active_click active" data-type="all">全部</li>
            <li class="active_click" data-type="2">竞价</li>
            <li class="active_click" data-type="1">一口价</li>
            <li class="active_click" data-type="0">询价</li>
            <li class="active_click" data-type="3">优质</li>
        </ul>
        <div class="shopping_title postfix_title">
            <i></i>域名后缀
        </div>
        <ul class="clearfix postfix_list suffix">
            <li class="active_click active" data-suffix="all">全部</li>
            <volist name="suffixList" id="vo">
            <li class="active_click" data-suffix="{pigcms{$vo.id}">{pigcms{$vo.suffix}</li>
            </volist>
        </ul>
        <div class="shopping_title postfix_title">
            <i></i>主题属性
        </div>
        <ul class="clearfix postfix_list theme">
            <li class="active_click active" data-theme="all">全部</li>
            <volist name="treasureList" id="vo">
                <li class="active_click" data-theme="{pigcms{$vo.id}">{pigcms{$vo.name}</li>
            </volist>
        </ul>

        <div class="shopping_title classify_title">
            <i></i>域名长度
        </div>
        <ul class="classify_list clearfix domainslength">
            <select  name="domainslength" id="domainslength" class="select_new_style">
                <option value="all" data-dlength="all" >不限</option>
                <option value="1" data-dlength="1">1</option>
                <option value="2" data-dlength="2">2</option>
                <option value="3" data-dlength="3">3</option>
                <option value="4" data-dlength="4">4</option>
                <option value="5" data-dlength="5">5</option>
                <option value="6" data-dlength="6">6</option>
                <option value="7" data-dlength="7">7</option>
                <option value="8" data-dlength="8">8</option>
                <option value="9" data-dlength="9">9</option>
            </select>
        </ul>
        <i></i>


<!--        <div class="shopping_title classify_title">-->
<!--            <i></i>域名长度-->
<!--        </div>-->
<!--        <div class="slide"><i></i>-->
<!--            <ul class="classify_list clearfix domainslength">-->
<!--                <li data-dlength="all" class="list_show">不限</li>-->
<!--                <li data-dlength="1">1</li>-->
<!--                <li data-dlength='2'>2</li>-->
<!--                <li data-dlength="3">3</li>-->
<!--                <li data-dlength="4">4</li>-->
<!--                <li data-dlength="5">5</li>-->
<!--                <li data-dlength="6">6</li>-->
<!--                <li data-dlength="7">7</li>-->
<!--                <li data-dlength="8">8</li>-->
<!--                <li data-dlength="9">9</li>-->
<!--            </ul>-->
<!--        </div>-->

        <div class="shopping_title eliminate_title">
            <i></i>当前价格
        </div>
        <ul class="eliminate_list">
                <ul class="classify_list clearfix nowprice">
                    <select  name="nowprice" id="nowprice" class="select_new_style">
                        <option value="all" data-nowprice="all" >不限</option>
                        <option value="1" data-nowprice="1">0-50</option>
                        <option value="2" data-nowprice="2">500-1000</option>
                        <option value="3" data-nowprice="3">1000-2万</option>
                        <option value="4" data-nowprice="4">2万-10万</option>
                        <option value="5" data-nowprice="5">10万-20万</option>
                        <option value="6" data-nowprice="6">20万-50万</option>
                        <option value="7" data-nowprice="7">50万以上</option>
                    </select>

<!--                    <li data-nowprice="all" class="list_show">不限</li>-->
<!--                    <li data-nowprice="1">00-50</li>-->
<!--                    <li data-nowprice="2">500-1000</li>-->
<!--                    <li data-nowprice="3">1000-2万</li>-->
<!--                    <li data-nowprice="4">2万-10万</li>-->
<!--                    <li data-nowprice="5">10万-20万</li>-->
<!--                    <li data-nowprice="6">20万-50万</li>-->
<!--                    <li data-nowprice="7">50万-</li>-->
                </ul>
        </ul><i></i>


        <div class="check">
				<label for="baohan" class="checked" data-isinclude="1">
					<input type="radio" id="baohan" name="sport" value="1" checked="checked"/>包含
				</label>
				<label for="kaitou" data-isinclude="2">
					<input type="radio" id="kaitou" name="sport" value="2" checked="checked"/>开头
				</label>	
				<label for="weibu" data-isinclude="3">
					<input type="radio" id="weibu" name="sport" value="3" checked="checked"/>尾部
				</label>
            </span>
        </div>

        <button type="submit" class="shooping_button">搜索</button>
    </section>
    </form>
</article>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script src="{pigcms{$static_path}js/index.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
<script src="{pigcms{$static_path}js/rem.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
<script src="{pigcms{$static_path}js/search.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
</body>

</html>