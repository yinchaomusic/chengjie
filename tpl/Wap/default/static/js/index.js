$(function() {
    $(".register_p ,.active_click").click(function() {
        $(this).toggleClass("active");
    });
    
    $("#click_collection").click(function() {
        var sessionUid = $("#suid").val();
        if(!sessionUid){
            layer.open({
                content: "请先登录",
                btn: ['OK'],
                shadeClose: true
            });
			return false;
        }
        var domain_id = $("#domainid").val();
        var domain = $("#domain_name").val();
        var type = $("#domain_type").val();
        $.post(collectionUrl,{'domain_id':domain_id,'domain':domain,'type':type}, function(result){
           var result = $.parseJSON(result);
           if(result){
               if(result.error == 0){
                   layer.open({
                        content: result.msg,
                        btn: ['OK'],
                        yes: function(){
                                window.location.href=location.href;
                        }
                    });
                    $(this).toggleClass("active");
               }else if(result.error == 1){
                   layer.open({
                        content: result.msg,
                        btn: ['OK'],
                        yes: function(){
                                window.location.href=location.href;
                        }
                    });
                    $(this).toggleClass("active");
               }else{
                   layer.open({
                        content: result.msg,
                        btn: ['OK']
                    });
                    $(this).toggleClass("active_click");
               }
           }
        })
        
    });

    $(".index_footer li").click(function() {
        $(this).addClass("active").siblings().removeClass('active');
    });
    $("section .search,.search_layer .search_info span").click(function() {
        $(".search input").val("");
        $(".search_layer").slideToggle("100");
        $(".search_layer input").focus();
    });
    $(".close").click(function() {
        $(".search input").val("");
        $(".search_bottom").show();
        $(".search_list ").hide();

    });

    $(".clearfix > span").click(function(){
        $(".search_layer input").val("");
        $(".search_bottom").show();
        $(".search_list ").hide();
        $(".search_layer").hide();
    }).bind(this);

    $('#home_serach_input').bind('input propertychange', function() {
        //layer.open({type: 2});
        if ($(".search_layer input").val() == "") {};
        $(".search_bottom").hide();
        $(".search_list ").show();
        var strs = new Array();
        var search_key = $(".search_layer input").val();

        var search_key_len = search_key.length;

        var strs = search_key.split('.',search_key_len);
        if( typeof(strs[0]) != 'undefined'){
           // 没有输入后缀
            var domain = strs[0];
            $('.search_list ul').find('li').eq(0).show();
            $('.search_list li').find('a').eq(0).text('查询 '+$(this).val() + ' 的相关交易');
            $('.search_list li').find('a').eq(0).attr('href',trade_url + '&keyword='+$(this).val());
            $('.search_list ul').find('li').eq(1).show();
            var com = $(this).val() +'.com';
            $('.search_list li').find('a').eq(1).text('查询 '+com + ' 的whois信息');
            $('.search_list li').find('a').eq(1).attr('href',whois_url + '&keyword='+com);
            $('.search_list ul').find('li').eq(2).show();
            var cn = $(this).val() +'.cn';
            $('.search_list li').find('a').eq(2).text('查询 '+ cn + ' 的whois信息');
            $('.search_list li').find('a').eq(2).attr('href',whois_url + '&keyword='+cn);
            $('.search_list ul').find('li').eq(3).show();
            var net = $(this).val() +'.net';
            $('.search_list li').find('a').eq(3).text('查询 '+ net + ' 的whois信息');
            $('.search_list li').find('a').eq(3).attr('href',whois_url + '&keyword='+net);

           if(typeof(strs[1]) != 'undefined' ){
               var suffix = strs[1];
               $('.search_list ul').find('li').eq(1).show();
               var com = domain +'.' + suffix;
               $('.search_list li').find('a').eq(1).text('查询 '+com + ' 的whois信息');
               $('.search_list li').find('a').eq(1).attr('href',whois_url + '&keyword='+com);
               $('.search_list ul').find('li').eq(2).hide();
               $('.search_list ul').find('li').eq(3).hide();

           }
        }
        layer.open({type: 2});
    });


    $('label').click(function() {
        var radioId = $(this).attr('name');
		$(this).addClass('checked').find('input[type="radio"]').prop('checked',true);
		$(this).siblings('label').removeClass('checked').find('input[type="radio"]').prop('checked',false);
    });
    var list_height=$(".classify_list li").height();

    /* $(".classify_list").toggle(function (){
     $(".classify_list").css("height",list_height*$(".classify_list li").length);
     },function(){
     $(".classify_list").css("height","1.25rem");
     });
     */
 /*   $(".slide li").click(function() {

        $(".list_show").show();

        if($(this).hasClass("list_show")){
            $(this).siblings().show();        
	}else{  
	 $(this).addClass("list_show").show().siblings().hide().removeClass("list_show");
	 $(this).siblings().hide();
 
	 }
    });
    $(".slide i").click(function() {
        $(this).next().children("li").toggle();
	 $(".list_show").show();
    });
*/
    $(".select_list p").text($(".select_list li.list_show").text());
    $(".bidding_header span em,.select_list p").click(function() {
        $(".select_list ul").slideToggle("100");
        $(".bidding_header span em").toggleClass('active');
    });
    $(".select_list li").click(function() {
        $(this).addClass("list_show").siblings().removeClass("list_show");
        $(".select_list p").text($(".select_list li.list_show").text());
        $(this).parent().slideToggle("100");
        $(".bidding_header span em").toggleClass('active');
    });
    $(".didding_details_title em").toggle(function(){
        $(" .didding_details_title p").css("height","auto");
        //$(".didding_details_title p").css("height","22px");
        $(this).toggleClass("active");
    },function(){
       // $(".didding_details_title p").css("height","22px");
        $(" .didding_details_title p").css("height","auto");
        $(this).toggleClass("active").hide();
    });
    if($(".didding_show_price").size()>0){
        var price = parseInt($(".didding_show_price").text());
        var price_plus = parseInt($(".price_plus").text());
        $(".didding_input input").val(price);
        $(".didding_details .didding_form .didding_input i").click(function() {
            price += price_plus;
            $(".didding_input input").val(price);
        });
    };

    $(".header .shear,.shear_list .cancel").click(function(){
        $(".layer_content").slideToggle("100");
    });

$(".go_top").click(function(){ //当点击标签的时候,使用animate在200毫秒的时间内,滚到顶部

    $("html,body").animate({
    scrollTop:"0px"
    },200);

});

    $(".inte_list b.inte_other").click(function() {
        $(".inte_id .slide").show();
    });
});

$(function() {

    $('.activity_title li').each(function(i){
        $(this).click(function(){
            window.location.href = "./index.php?g=Wap&c=Hotsale&a=index&typeid=" + i;
        });
    })



    $('.activity_title li').each(function(){
        $('.activity_title li').each(function(){
            $(this).removeClass('active');
        });
        $('.activity_title li:eq('+typeid+')').addClass('active');
        $('.acticity_list>li').show();
        //$('.acticity_list>li').eq(typeid).show().siblings().hide();
    });

    $('.activity_title_news li').each(function(){
        $('.activity_title_news li').each(function(){
            $(this).removeClass('active');
        });
        $('.activity_title_news li:eq('+typeid+')').addClass('active');
        $('.acticity_list>li').show();
        //$('.acticity_list>li').eq(typeid).show().siblings().hide();
    });

})


$(function() {

    $('.activity_pl li').each(function(i){
        $(this).click(function(){
            window.location.href = "./index.php?g=Wap&c=Pldomain&a=index&typeid=" + i;
        });
    })



    $('.activity_pl li').each(function(){
        $('.activity_pl li').each(function(){
            $(this).removeClass('active');
        });
        $('.activity_pl li:eq('+typeid+')').addClass('active');
        $('.acticity_list>li').show();
        //$('.acticity_list>li').eq(typeid).show().siblings().hide();
    });

    $('.activity_title_news li').each(function(){
        $('.activity_title_news li').each(function(){
            $(this).removeClass('active');
        });
        $('.activity_title_news li:eq('+typeid+')').addClass('active');
        $('.acticity_list>li').show();
        //$('.acticity_list>li').eq(typeid).show().siblings().hide();
    });

})



//下拉插件
$(function(){
slide(".slide li","list_show",".slide i");
});
function slide(a,b,c){
var down=$(a);

down.bind("click",function(){
      $(c).toggleClass("active");
       $(b).show();
        if ($(this).hasClass(b)) {
            $(this).siblings().show();
        } else {
            $(this).addClass(b).show().siblings().hide().removeClass(b);
			//alert(212)
        };
});
var i_con=$(c)
i_con.bind("click",function(){
        $(this).next().children("li").toggle();
        $(b).show();
        $(this).toggleClass("active");
});
}