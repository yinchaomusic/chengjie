﻿$(function () {
    //倒计时
	if($(".countdown_new strong").size() > 3){
		TimeCountDay(".countdown_new strong");
	}else{
		TimeCount(".countdown_new strong");
	}
})

function TimeCount(selecter) {
    var $timedom = $(selecter);
    var target_time = parseInt($timedom.eq(0).text()) * 3600 + parseInt($timedom.eq(1).text()) * 60 + parseInt($timedom.eq(2).text()); //总秒数
    //加上当前时间戳获得目标时间戳
    target_time += parseInt(new Date().getTime() / 1000);
    var now_time = parseInt(new Date().getTime() / 1000);
    if ((parseInt(target_time) - parseInt(now_time)) > 0) {
        var resTime = new Array(3);
        TimeOut(now_time);
        var timer1 = setInterval(function () {
            now_time = new Date().getTime() / 1000;
            TimeOut(now_time);
        },
        1000);
    }
    function TimeOut(now) {
        var seconds = parseInt(target_time) - parseInt(now);
        if (seconds >= 0) {
            resTime[0] = (seconds % (86400 * 24) - seconds % 86400 % 3600) / 3600;
            resTime[1] = parseInt(seconds % 86400 % 3600 / 60);
            resTime[2] = seconds % 86400 % 3600 % 60;
            for (var i in resTime) {
                if (parseInt(resTime[i]) < 10) {
                    resTime[i] = "0" + resTime[i];
                };
            };
            $timedom.eq(0).text(resTime[0]);
            $timedom.eq(1).text(resTime[1]);
            $timedom.eq(2).text(resTime[2]);
        }
    };
};
function TimeCountDay(selecter) {
    var $timedom = $(selecter);
    var target_time = parseInt($timedom.eq(0).text()) * 86400 + parseInt($timedom.eq(1).text()) * 3600 + parseInt($timedom.eq(2).text()) * 60 + parseInt($timedom.eq(3).text()); //总秒数
    //加上当前时间戳获得目标时间戳
    target_time += parseInt(new Date().getTime() / 1000);
    var now_time = parseInt(new Date().getTime() / 1000);
    if ((parseInt(target_time) - parseInt(now_time)) > 0) {
        var resTime = new Array(3);
        TimeOutDay(now_time);
        var timer1 = setInterval(function () {
            now_time = new Date().getTime() / 1000;
            TimeOutDay(now_time);
        },
        1000);
    }
    function TimeOutDay(now) {
        var seconds = parseInt(target_time) - parseInt(now);
        if (seconds >= 0) {
			// console.log(seconds);
            resTime[0] = parseInt(seconds / 86400);
            resTime[1] = parseInt(seconds % 86400 / 3600);
            resTime[2] = parseInt(seconds % 86400 % 3600 / 60);
            resTime[3] = seconds % 86400 % 3600 % 60;
            for (var i in resTime) {
                if (parseInt(resTime[i]) < 10) {
                    resTime[i] = "0" + resTime[i];
                };
            };
            $timedom.eq(0).text(resTime[0]);
            $timedom.eq(1).text(resTime[1]);
            $timedom.eq(2).text(resTime[2]);
            $timedom.eq(3).text(resTime[3]);
        }
    };
};