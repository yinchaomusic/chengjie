<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name='apple-touch-fullscreen' content='yes'>
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title><if condition="$category['cat_key'] eq 'notice'">公告详情<else/>内容详情</if></title>
	<else/>
		<title><if condition="$category['cat_key'] eq 'notice'">公告详情<else/>内容详情</if>-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css?t={pigcms{$_SERVER.REQUEST_TIME}">
</head>
<body>
<if condition="!$is_wexin_browser">
<header class="header">
	<a href="#" onclick="window.history.go(-1);"><i></i></a>
	<p><if condition="$category['cat_key'] eq 'notice'">公告详情<else/>内容详情</if></p>
</header>
</if>
<article>
	<section>
		<div class="notice_title">
			<h4>{pigcms{$news_show.news_title}</h4>
			<p>{pigcms{$news_show.add_time|date="Y-m-d H:i:s",###} </p>
		</div>
		<div class="notice_txt">
			<php></php>
			{pigcms{$news_show.news_content}
			<!--if condition="$category['cat_key'] eq 'notice'">
				<h6>{pigcms{$now_site_name}</h6>
				<h6>{pigcms{$news_show.add_time|date="Y-m-d",###}</h6>
			</if-->
		</div>
	</section>
</article>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/rem.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
<script src="{pigcms{$static_path}js/index.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
<script>
	var erro_no = {pigcms{$error_no};
	var erro_msg = '{pigcms{$erro_msg}';
	var index_url = "{pigcms{:U('Home/index')}";
	if(erro_no){
		layer.open({
			content: erro_msg,
			btn: ['OK'],
			shadeClose: false,
			yes: function(){
				window.location.href=index_url;
			}
		});
	}
</script>
</body>
</html>