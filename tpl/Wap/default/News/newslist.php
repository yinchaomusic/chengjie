<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name='apple-touch-fullscreen' content='yes'>
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>{pigcms{$cat_title.cat_name}</title>
	<else/>
		<title>{pigcms{$cat_title.cat_name}-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css?t={pigcms{$_SERVER.REQUEST_TIME}">
</head>
<body class="newslist">
	<if condition="!$is_wexin_browser">
		<header class="header">
			<a href="{pigcms{:U('Home/index')}"><i></i></a>
			<p>{pigcms{$cat_title.cat_name}</p>
		</header>
	</if>
	<article>
		<section>
			 <ul class="news_ul">
				 <volist name="list" id="vo">
					<li>
						<a href="{pigcms{:U('News/show',array('news_id'=>$vo['news_id']))}">{pigcms{$vo.news_title}<span>{pigcms{$vo.add_time|date='Y-m-d',###}</span></a>
					</li>
				 </volist>
			 </ul>
		</section>
	</article>
	<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="{pigcms{$static_path}js/rem.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
	<script src="{pigcms{$static_path}js/index.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
	<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
	<script>
		var erro_no = {pigcms{$error_no};
		var erro_msg = '{pigcms{$erro_msg}';
		var index_url = "{pigcms{:U('Home/index')}";
		if(erro_no){
			layer.open({
				content: erro_msg,
				btn: ['OK'],
				shadeClose: false,
				yes: function(){
					window.location.href=index_url;
				}
			});
		}
	</script>
</body>
</html>
