<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name='apple-touch-fullscreen' content='yes'>
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
    <title>{pigcms{$config.site_name}</title>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}"/>
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/swiper.min.css?t={pigcms{$_SERVER.REQUEST_TIME}">
</head>
<body class="index_content">
    <aside>
        <div class="search_layer">
            <div class="clearfix search_info">
                <div class="search"><i></i>
                    <input type="text" id="home_serach_input" placeholder="搜索域名交易信息、查询whois" maxlength="100"/>
                    <!--i class="close"></i-->
                </div>
                <span>取消</span>
            </div>
            <div class="search_bottom">
                <em></em>
                <p>搜索域名的相关交易
                    <br/> 域名的wohis信息
                </p>
            </div>
            <div class="search_list">
                <ul>
                     <li style="display: none;">
                        <a href="">查询 <span> </span> 的相关交易</a>
                    </li>
                    <li style="display: none;">
                        <a href="">查看 <span> </span>.com 的Whois信息</a>
                    </li>

                    <li style="display: none;">
                        <a href="">查看 <span> </span>.cn 的Whois信息</a>
                    </li>
                    <li style="display: none;">
                        <a href="">查看 <span> </span>.net 的Whois信息</a>
                    </li>
                    <li style="display: none;">
                        <a href="">查看 <span> </span>.com.cn 的Whois信息</a>
                    </li>

                </ul>
            </div>
        </div>
    </aside>
	<if condition="!$is_wexin_browser">
		<header class="header">
			<span>
			<!--if condition="$user_session['uid']">
					<a href="{pigcms{:U('Profile/index')}"><i></i>我</a>
					<else/->
					<a href="{pigcms{:U('Login/login')}"><i></i>登录</a>
				</if-->
				<if condition="$user_session['uid']">
					<a href="{pigcms{:U('Profile/index')}"><i></i></a>
				 <else/>
					<a href="{pigcms{:U('Login/login')}"><i></i></a>
				</if>
				</span>
			<p>首页</p>
		</header>
	</if>
    <article>
        <section class="searchBox">
            <div class="search"><i></i>
                <input type="text" placeholder="搜索域名、查询whois">
            </div>
		</section>
		<section>
            <div class="banner">
                <div class="swiper-container s1 swiper-container-horizontal">
                    <div class="swiper-wrapper" style="transition-duration: 0ms; -webkit-transition-duration: 0ms;  ">
                        <pigcms:adver cat_key="wap_index_banner" limit="5" var_name="wap_index_banner">
                            <div class="swiper-slide  swiper-slide-prev">
								<a href="{pigcms{$vo.url}" target="_blank">
									<img src="{pigcms{$vo.pic}"/>
								</a>
                            </div>
                        </pigcms:adver>
                    </div>
                    <div class="swiper-pagination p1 swiper-pagination-clickable"><span class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span><span class="swiper-pagination-bullet"></span></div>
                </div>
            </div>
        </section>
        <section class="officeBox">
            <ul class="office_list">
                <li class="clearfix">
                    <div class="office_list_img"><img src="<if condition="$new_cat['icon'] eq ''">{pigcms{$static_path}images/ui_41.png <else /> /upload/cat/{pigcms{$new_cat.icon} </if>"></div>
                    <div class="office_list_text">
                        <a href="{pigcms{:U('News/newslist',array('cat_id'=>$new_cat['cat_id']))}">
                            <h2>{pigcms{$new_cat.cat_name}</h2>
							<p>{pigcms{$new_cat.desc}&nbsp;</p>
                        </a>
					</div>
                </li>
                <li class="clearfix">
                    <div class="office_list_img"><img src="<if condition="$cat_nocice['icon'] eq ''">{pigcms{$static_path}images/ui_37.png <else /> /upload/cat/{pigcms{$cat_nocice.icon} </if>"></div>
                    <div class="office_list_text">
                        <a href="{pigcms{:U('News/newslist',array('cat_id'=>$cat_nocice['cat_id']))}">
                            <h2>{pigcms{$cat_nocice.cat_name}</h2>
                            <p>{pigcms{$last_news.news_title}&nbsp;</p>
                        </a>
					</div>
                </li>
                <li class="clearfix">
                    <div class="office_list_img"><img src="<if condition="$cat_info['icon'] eq ''">{pigcms{$static_path}images/ui_47.png <else /> /upload/cat/{pigcms{$cat_info.icon} </if>"></div>
                    <div class="office_list_text">
                        <a href="{pigcms{:U('News/newslist',array('cat_id'=>$cat_info['cat_id']))}">
                            <h2>{pigcms{$cat_info.cat_name}</h2>
                            <p>{pigcms{$last_info.news_title}</p>
                        </a>
					</div>
                </li>
                <li class="clearfix">
                    <div class="office_list_img"><img src="{pigcms{$static_path}images/UI-2_14.png"></div>
                    <div class="office_list_text">
                        <a href="{pigcms{:U('Fastbid/index')}">
                            <h2>极速竞价</h2>
                            <p>即拍即卖 价高者得</p><i></i>
                        </a></div>
                </li>
                <li class="clearfix">
                    <div class="office_list_img"><img src="{pigcms{$static_path}images/UI-2_18.png"></div>
                    <div class="office_list_text">
                        <a href="{pigcms{:U('Bargain/index')}">
                            <h2>优质域名</h2>
                            <p>珍惜精品 值得青睐</p><i></i>
                        </a></div>
                </li>
                <li class="clearfix">
                    <div class="office_list_img"><img src="{pigcms{$static_path}images/UI-2_16.png"></div>
                    <div class="office_list_text">
                        <a href="{pigcms{:U('Fastbid/index')}">
                            <h2>一口价</h2>
                            <p>明码标价 即刻购买</p><i></i>
                        </a></div>
                </li>
            </ul>
        </section>
    </article>
    <include file="Public:footer"/>
	<script>
	   // var reg_url = "{pigcms{:U('Whois/register')}";
		var trade_url = "{pigcms{:U('Trade/tradesearch')}";
		var whois_url = "{pigcms{:U('Whois/whois')}";
	</script>
	<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
	<script src="{pigcms{$static_path}js/index.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
	<script src="{pigcms{$static_path}js/rem.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
	<script src="{pigcms{$static_path}js/swiper.min.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
	<script>
	var mySwiper = new Swiper('.s1', {
		loop: false,
		autoplay: 3000,
		pagination: '.p1',
		paginationClickable: true
	});
	</script>
	<script type="text/javascript">
		window.shareData = {  
		            "moduleName":"Home",
		            "moduleID":"0",
		            "imgUrl": "{pigcms{$config.site_logo}", 
		            "sendFriendLink": "{pigcms{$config.site_url}{pigcms{:U('Home/index')}",
		            "tTitle": "{pigcms{$config.site_name}",
		            "tContent": "{pigcms{$config.seo_description}"
		};
		</script>
		{pigcms{$shareScript}
</body>
</html>