<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name='apple-touch-fullscreen' content='yes'>
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>我的米表</title>
		<else/>
		<title>我的米表-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
	<!-- 禁止百度转码 -->
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<!-- UC强制竖屏 -->
	<meta name="screen-orientation" content="portrait" />
	<!-- QQ强制竖屏 -->
	<meta name="x5-orientation" content="portrait" />
	<!-- UC强制全屏 -->
	<meta name="full-scerrn" content="yes" />
	<!-- QQ强制全屏 -->
	<meta name="x5-fullscreen" content="ture" />
	<!-- QQ应用模式 -->
	<meta name="x5-page-mode" content="app" />
	<!-- UC应用模式 -->
	<meta name="browsermode" content="application">
	<!-- window phone 点亮无高光 -->
	<meta name="msapplication-tap-highlight" content="no" />
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
	<script>
		var typeid = '{pigcms{$Think.get.typeid}';
	</script>
</head>

<body class="results_content">
<header class="header bidding_header">
	<a href="{pigcms{:U('Home/index')}"><i></i></a>
	<p>{pigcms{$userinfo.uid}的米表</p>
	<a href="{pigcms{:U('Search/index')}" class="right"><em class="classify_icon"></em></a>

</header>
<article>
	<section class="terminal_tips" style="line-height: 30px;">
		<p> 电话：<a href="tel:{pigcms{$userinfo.phone}"><strong>{pigcms{$userinfo.phone}</strong> </a> &nbsp; QQ：<a href=""><strong>{pigcms{$userinfo.qq}</strong></a></p>
		<p> E-mail：<a href=""><strong>{pigcms{$userinfo.email}</strong></a> &nbsp;   </p>
	</section>
	<div class="trade_title">
		<ul class="activity_title_news clearfix">
			<li <if condition="$typeid eq 0">class="active"</if>><a href="{pigcms{:U('Home/mblist',array('typeid'=>0,'mmid'=>$userinfo['uid']))}">按时间排序</a></li>
			<li <if condition="$typeid eq 1">class="active"</if>><a href="{pigcms{:U('Home/mblist',array('typeid'=>1,'mmid'=>$userinfo['uid']))}">按价格排序</a></li>
			<li <if condition="$typeid eq 2">class="active"</if>><a href="{pigcms{:U('Home/mblist',array('typeid'=>2,'mmid'=>$userinfo['uid']))}">按长度排序</a></li>
		</ul>
	</div>
	<ul class="acticity_list">
		<li>
			<section class="bidding">
				<ul id="domain_list" class="clearfix mg_top">
					<volist id="vo" name="domains_list">
						<li><a href="<if condition="$vo['type'] eq 1">{pigcms{:U('Hotsale/selling',array('id'=>$vo['domain_id']))}<elseif condition="$vo['type'] eq 0" />{pigcms{:U('Buydomains/detail',array('id'=>$vo['domain_id']))}<elseif condition="$vo['type'] eq 2" />{pigcms{:U('Fastbid/detail',array('id'=>$vo['domain_id']))}<elseif condition="$vo['type'] eq 3" />{pigcms{:U('Bargain/detail',array('id'=>$vo['domain_id']))}</if>"><h4>{pigcms{$vo.domain}</h4><p style="color: #ffa200">价格：￥{pigcms{$vo.money|number_format}元</p><p>{pigcms{$vo.desc}&nbsp;</p></a>
						</li>
					</volist>
				</ul>
			</section>
		</li>
	</ul>
</article>
</body>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>

<script>
	var erro_no = {pigcms{$error_no};
	var erro_msg = '{pigcms{$erro_msg}';
	var index_url = "{pigcms{:U('Home/index')}";
	if(erro_no){
		layer.open({
			content: erro_msg,
			btn: ['OK'],
			shadeClose: true,
			yes: function(){
				window.location.href=index_url;
			}
		});
	}
</script>
</html>