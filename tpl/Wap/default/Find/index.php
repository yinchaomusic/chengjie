<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>发现</title>
	<else/>
		<title>发现-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">

</head>

<body class="index_content">
<if condition="!$is_wexin_browser">
<header class="header">
    <p>发现</p>
</header>
</if>
<article>
    <section>
        <div class="find_title">
            <a href="{pigcms{:U('Search/index')}">
              <span>淘域名</span><span>{pigcms{$countdomains}个正在出售的域名<i></i></span>
            </a>
        </div>
        <ul class="find_list clearfix">
            <li>
                <a href="{pigcms{:U('Fastbid/index')}"><img src="{pigcms{$static_path}images/UI-2_14.png">
                    <p>极速竞价</p>
                </a>
            </li>
            <li>
                <a href="{pigcms{:U('Bargain/index')}">
                    <img src="{pigcms{$static_path}images/UI-2_18.png">
                    <p>优质域名</p>
                </a>
            </li>
            <li><a href="{pigcms{:U('Buydomains/index')}">
                <img src="{pigcms{$static_path}images/UI-2_12.png">
                <p>议价域名</p>
                </a>
            </li>

            <li>
                <a href="{pigcms{:U('Hotsale/index')}">
                <img src="{pigcms{$static_path}images/UI-2_16.png">
                <p>一口价</p>
                  </a>

            </li>

        </ul>
        <div class="find_title"><a href="{pigcms{:U('Buyer/index')}"><span>我是买家</span><span>查看我买到的域名<i></i></span></a> </div>
        <ul class="find_table clearfix">
            <li class="clearfix"><div class="find_img">
                    <a href="{pigcms{:U('Pldomain/index')}">
                        <img src="{pigcms{$static_path}images/UI-2_38.png"></div>
                <div class="find_txt">
                    <h3>批量交易</h3>
                    <p>域名打包出售</p>
                </div>
                </a>
            </li>
            <li class="clearfix"><div class="find_img">
                    <a href="{pigcms{:U('Terminal/index')}">
                    <img src="{pigcms{$static_path}images/UI-2_41.png"></div>
                <div class="find_txt">
                    <h3>终端域名</h3>
                    <p>经纪人帮你卖</p>
                </div>
            </a>
            </li>
            <li class="clearfix"><div class="find_img">
                    <a href="{pigcms{:U('Procurement/index')}">
                    <img src="{pigcms{$static_path}images/UI-2_48.png"></div>
                <div class="find_txt">
                    <h3>域名代购</h3>
                    <p>委托经纪人购买</p>
                </div>
            </a>
            </li>
            <li class="clearfix"><div class="find_img">
                    <a href="{pigcms{:U('Escrow/index')}">
                    <img src="{pigcms{$static_path}images/UI-2_49.png"></div>
                <div class="find_txt">
                    <h3>申请中介</h3>
                    <p>双方已谈好价格</p>
                </div>
            </a>
            </li>
        </ul>
        <div class="find_title"><a href="{pigcms{:U('Seller/index')}"><span>我是卖家</span><span>查看我卖出的域名<i></i></span></a> </div>
    </section>
</article>

<include file="Public:footer"/>

<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>

</body>

</html>