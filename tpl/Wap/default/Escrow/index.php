<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name='apple-touch-fullscreen' content='yes'>
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>申请中介</title>
	<else/>
		<title>申请中介-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>

<body class=" ">
<header class="header">
    <a href="{pigcms{:U('Find/index')}"><i></i></a>
    <p>申请中介</p>
</header>
<article>
    <section class="terminal_tips">
        <h3>注意事项：</h3>
        <p> 1.买家向{pigcms{$now_site_name}({pigcms{$now_site_short_url}）支付域名全款后，卖家才开始域名过户。</p>
        <p> 2.买家拿到域名且完成交易后，{pigcms{$now_site_name}（{pigcms{$now_site_short_url}）方解冻款项释放给卖家。</p>
        <p>3.手续费按照支付方会员等级收取，交易未达成不收取手续费。</p>
    </section>
    <section>
        <ul class="shopping_list">
            <li><span>域名</span>
                <input type="text" class="domainName" name="domainName" placeholder="请输入域名">
            </li>
            <li><span>价格</span>
                <input type="number" name="money" class="money" placeholder="请输入价格" onkeyup='this.value=this.value.replace(/\D/gi,"")'>
            </li>
            <li><span>对方ID</span>
                <input type="text" class="user" name="user" placeholder="请输入对方ID">
            </li>
            <li class="clearfix inte_list"><span>角色类型</span>
                    <select class="slide" name="buy_sell" id="buy_sell">
                        <option value="0">请选择</option>
                        <option value="1">买家</option>
                        <option value="2">卖家</option>
                    </select>
            </li>
            <li class="clearfix inte_list"><span>中介费</span>
                    <select class="slide" name="pay_fees" id="pay_fees">
                        <option value="0">请选择谁来支付</option>
                        <option value="1">买家</option>
                        <option value="2">卖家</option>
                        <option value="3">买卖双方各付50%</option>
                    </select>
            </li>
        </ul>
        <input type="hidden" id="userId" value="{pigcms{$user_session.uid}">
        <button class="purchasing_button escrow_sub" id="escrow_sub">提交</button>
    </section>
</article>
</body>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script>
$("#escrow_sub").click(function(){
    if ($("#userId").val()<1) {
        layer.open({
            content: "请登录",
            btn: ['OK'],
            shadeClose: true,
            yes: function(){
                    window.location.href="{pigcms{:U('Login/login')}";
            }
        });
    }else if(!$(".domainName").val()){
        layer.open({
            content: "请填写域名",
            btn: ['OK']
        });
        return false;
    }else if(!$(".money").val()){
        layer.open({
            content: "请填写价格",
            btn: ['OK']
        });
        return false;
    }else if(!$(".user").val()){
        layer.open({
            content: "请填写对方id",
            btn: ['OK']
        });
        return false;
    }else if ($("#buy_sell").val() < 1) {
        layer.open({
            content: "请选择您担当是角色",
            btn: ['OK']
        });
        return false;
    }else if ($("#pay_fees").val() < 1) {
        layer.open({
            content: "请选择谁来支付手续费",
            btn: ['OK']
        });
        return false;
    }else{
        $.post("{pigcms{:U('Escrow/new_deal')}",{"domainName":$(".domainName").val(),"money":$(".money").val(),"user":$(".user").val(),"buy_sell":$("#buy_sell").val(),"pay_fees":$("#pay_fees").val()},function(result){
            result = $.parseJSON(result);
            if(result){
                if(result.error == 0){
                        layer.open({
                            content: result.msg,
                            btn: ['OK'],
                            shadeClose: true,
                            yes: function(){
                                    window.location.href=location.href;
                            }
                        });
                }else{
                    layer.open({
                            content: result.msg,
                            btn: ['OK']
                        });
                        $('#'+result.dom_id).focus();
                }
            }
        })
    }

})
</script>

</html>