<html style=" ">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>{pigcms{$now_term.term_name}</title>
	<else/>
		<title>{pigcms{$now_term.term_name}-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
<link rel="stylesheet" href="http://www.chengjie.com/tpl/Wap/default/static/css/base.css">
	<link rel="stylesheet" type="text/css" href="http://www.chengjie.com/tpl/Wap/default/static/css/index.css">
	<link rel="stylesheet" type="text/css" href="http://www.chengjie.com/static/js/layer.mobile-v1.7/need/layer.css">
	<style type="text/css">
		.video-box {height: 320px;background-color: #f0f0f0;position: relative;}
		.video-box .refresh-btn {position: absolute;margin: 0px auto;display: block;background-color:#333;left: 10px;right: 10px;top: 0px;line-height: 40px;cursor: pointer;color: #fff;font-size: 12px;text-align: center;}
	</style>
</head>

<body class="didding_details">
<!--  
	<div class="video-box">
		<iframe src="http://www.yy.com/90123/2477843798?f=qqzone#0-qzone-1-66520-d020d2d2a4e8d1a374a433f596ad1440" width="100%" height="100%" id="video2" frameborder="0" scrolling="no"></iframe>
		<a class="refresh-btn" onclick="document.getElementById('video2').contentWindow.location.reload();">视频直播有延迟。如无法播放，请尝试点击刷新</a>
	</div> -->

<if condition="!$is_wexin_browser">
<header class="header bidding_header">
	<a href="{pigcms{:U('Home/index')}"><i></i></a>
	<p>{pigcms{$now_term.term_name}</p>
</header>

</if>

<article>
	<nav class="auction_title activity_title_auction">
		<ul class="clearfix">
			<li class="paimaichujiaactive active" id="paimaichujiaactive"><a href="#" title="">拍卖出价</a></li>
			<li class="msgRoom"><a href="#" title="">聊天室</a></li>
			<li><a href="#" title="">待拍列表</a></li>
			<li><a href="#" title="">视频直播</a></li>
		</ul>
	</nav>
    
    
    
	<ul class="acticity_list">
		<li>
			<section>
				<ul class="auction_list" id="bid_container">
<!--					<li class="clearfix"><div class="action_head"><img src="{pigcms{$static_path}images/ui_1_03_03.png"></div><div class="action_txt"><p>出价:<span>39956元</span></p><p>ID:254565</p></div><div class="action_time"><i></i>16:30</div></li>-->

				</ul>
				<div class="auction_product"><!---拍卖域名--><!--后缀--->
					<h2><span class="current-domain"></span></h2>
					<p class="current-domain-description">域名简介</p>
				</div>
				<ul class="auction_price clearfix">
					<li>
						<span id="qipaijia">Loading</span>
						<p>起拍价</p>
					</li>
					<li>
						<span id="jiajiafudu">Loading</span>
						<p>加价幅度</p>
					</li>
					<li>
						<span id="zuigaojia">无</span>
						<p>当前最高价</p>
					</li>
					<li>
						<span id="myzuigaojia">无</span>
						<p>我的出价</p>
					</li>
				</ul>
				<div class="didding_form clearfix">
					<div class="didding_input">
						<input type="text" id="bidding"><i class="btn-add"></i>&nbsp;元</div>
					<button id="sendPrice">立即出价</button>
				</div>
			</section>
		</li>
			<li style="display:none">
			<section>
				<dl class="chartroom_list">
					<dd class="clearfix">
						<div class="  chartroom_head"><img src="{pigcms{$static_path}images/ui_1_03_03.png" alt=""></div>
						<div class="chartroom_txt">
				</dl>
				<div class="chartroom_input clearfix">
					<div class="chart_icon">
						<i class="moji" id="chat-input"></i>
						<input type="text"  name="chatMsg"  placeholder="请输入消息">
					</div>
					<button id="sendMsg">发送</button>
				</div>
			</section>
		</li>
		<li  >
			<section class="bidding">
				<!--- 待拍域名 --->
				<ul class="clearfix mg_top">


				</ul>
			</section></li>
				<li style="display:none">
			<section>
				<div class="seeding active_click"> <!--视频第一张图片-->

					<div id="video" class="video-con" style="height: 280px; text-align: center;">
							 <video id="html5player-video" data-setup="{}" controls="controls" preload="true" width="100%" height="100%" >
							<source src="	{pigcms{$config.video_auction_phone_code}">
							<span>您的手机版本，网页版暂未能支持！</span>
						  </video>
					</div>
					<!-- <i></i> -->
					</div>
				<div class="seeding_title">
					<h3>{pigcms{$now_term.term_name}</h3>
					<p><div id="player" style="width:100%;height:450px;">
    <script type="text/javascript" charset="utf-8" src="http://yuntv.letv.com/player/live/blive.js"></script>
    <script>
        var player = new CloudLivePlayer();
        player.init({activityId:"A201606150000135"});
    </script>
</div>
</p>
				</div>
			</section></li>
	</ul>
</article>
<script>
	var close_error = "{pigcms{$close_error}";
	var jumptoUrl   = "{pigcms{$jumptoUrl}";
	var close_reason= "{pigcms{$close_reason}";
</script>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
<script src="{pigcms{$static_path}js/rem.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
<!--<script src="{pigcms{$static_path}js/videos.js"></script>-->
<script>
	$(function() {
		$(".activity_title_auction li").click(function(){
			$(this).addClass("active").siblings().removeClass("active");
			$(".acticity_list> li").eq($(".activity_title_auction li").index(this)).show().siblings().hide();
			//alert(parseInt($(this).eq(0).index()));

			if($(this).hasClass('msgRoom')){
				setTimeout(function(){
					$(window).scrollTop($('body').height()+10000);
				},50);
			}

			if(parseInt($(this).eq(0).index())  === 0){
				//alert(parseInt($(this).eq(0).index()));
				setTimeout(function(){
					$(window).scrollTop($("html,body").height()+100000);
				},50);
			}


		}).eq(0).trigger("click");
	})


	var pleaseMoney = 0;
	var plusPrice = 0;
	var nowDomainId = 0;
	var term_id = {pigcms{$now_term.term_id};
	var myUid = <if condition="$user_session">{pigcms{$user_session.uid}<else/>0</if>;
	var lastAjaxTime = 0;
	var indexShade = layer.open({type: 2});
	var showSound = true;
	var lockScreen = false;
	var lockScreenIndex = null;
	// alert($(window).height());
	if($(window).height() > 880 && $(window).height() < 930){
		$('.footerBar').remove();
	}
	if($(window).height() < 880){
		$('.s-bg-white').remove();
		$('#miniScreenAccount').show();
	}
	if($(window).height() < 770){
		$('.footerBar').remove();
	}
	if($(window).height() < 720){
		$('.video-wrap .col-title').remove();
	}
	if($(window).height() < 700){
		$('.domain-info .domain-info-item .box2').css({'padding-bottom':'2%','padding-top':'2%'});
	}
	$('.col,.member-list,.slimScrollDiv,.slimscroll').height($(window).height() - $('.s-bg-white').height() - $('.headerBar').height() - $('.footerBar').height());
	$('.auction-list').height($('.auction-wrap').height() - $('.icon_clock').height() - $('.bid-price-wrap').height() - 20);
	$('.chat-list .slimScrollDiv,.chat-list .chat-list-wrap').height($('.chat-list').height()-283);
	$('.video-wrap .tab-content,#milist_container').height($('.video-wrap').height() - $('.video-wrap .col-title').height() - $('.video-wrap .video-box').height() - $('.video-wrap .tab-wrap').height() -26);
	var canShowChujiaRows = parseInt(10);


	$(".f-b-rlt p").click(function(event){
		event.stopPropagation();
	});
	$.post("{pigcms{:U('VideoSale/getFirstDatas')}",{term_id:term_id},function(result){
		//在线会员

		var userHtml = '';
		$.each(result.info.onlineUsers,function(i,item){
			userHtml+= '<div class="member member_'+item.uid+'" data-memberid="'+item.uid+'"><img alt="" class="member-header" data-index="0" src="{pigcms{$config.site_url}/upload/User_level/'+item.pic+'" data-userid="'+item.uid+'"/><div class="member-info"><div class="name">'+item.name+'</div><div class="id">ID：'+item.uid+'</div></div><div class="clr"></div></div>';
		});
		$('#memberDom').html(userHtml);

		//当前拍卖
		var domainList = result.info.domainList;
		if(domainList == 'null'){
			layer.open({
				content: '该期拍卖下没有了域名，系统将跳转首页.',
				btn: ['确认'],
				shadeClose: true,
				yes: function(){
					location.href = "{pigcms{:U('Home/index')}";
				}
			});
			return;
		}
		var retain_money = parseInt(domainList[0].retain_money);
		var NowMoney = domainList[0].now_money.replace(/,/gi,'');
		$('.current-domain').html('<h2><span class="current-domain">当前拍卖域名：<b>'+domainList[0].domains+'</b></span><span><i>'+domainList[0].domains_suffix+'</i><i>'+(retain_money > 1 ? ( NowMoney >= retain_money ? '已过保留价':'有保留价' ) :'无保留价')+'</i></span></h2>');
		$('.current-domain-description').html(domainList[0].domains_desc);
		$('#qipaijia').html(domainList[0].mark_money!='0' ? '￥'+domainList[0].mark_money : '无');
		$('#zuigaojia').html(domainList[0].now_money!='0' ? '￥'+domainList[0].now_money : '无');
		plusPrice = domainList[0].plusPrice;
		$('#jiajiafudu').html('￥'+plusPrice);
		pleasePrice = domainList[0].pleasePrice;
		nowDomainId = domainList[0].domain_id;
		$('#bidding').val(pleasePrice);

		//待拍域名列表
		var domainListHtml = '';
		$.each(result.info.domainList,function(i,item){
			domainListHtml +='<li><h4>'+item.domains+'<i class="video_suffix_i">'+item.domains_suffix+'</i></h4><p>'+item.domains_desc+'</p></li>';
		});
		$('.mg_top').html(domainListHtml);

		//价格历史
		var chujiaHtml = '';
		$.each(result.info.priceHistory,function(i,item){
			chujiaHtml += '<li class="clearfix price-'+item.uninxtime+'-'+item.uid+'"><div class="action_head"><img src="{pigcms{$config.site_url}/upload/User_level/'+item.pic+'"></div><div class="action_txt"><p>出价:￥<span>'+toThousands(item.price)+'元</span></p><p>ID:'+item.uid+'</p></div><div class="action_time"><i></i>'+item.time+  ((item.is_sys_op == 1) ? (' - 同步助手- <font color=green>'+item.chujia_name + '</font>') :'') +'</div></li>';
			if(item.uid == myUid && item.is_sys_op != 1){
				$('#myzuigaojia').html('￥'+toThousands(item.price));
				$('#sendPrice').addClass('disable').data('errTips','您已经出过价，需要等待其他人出价后您才能再次出价。');
			}else{
				$('#sendPrice').removeClass('disable');
			}
		});
		$('#bid_container').html(chujiaHtml);
		if(result.info.priceHistory.length > canShowChujiaRows){
			$('#bid_container >li:lt('+(result.info.priceHistory.length - canShowChujiaRows)+')').remove();
		}
		$('#bid_container >li:last .action_txt').append('&nbsp;&nbsp;<img class="temporary_img" src="{pigcms{$static_path}VideoSale/img/1st.png"/>');

		//聊天历史
		var imHtml = '';
		$.each(result.info.msgHistory,function(i,item){
			imHtml += '<dd class="clearfix '+(item.uid==myUid ? "mine":"")+' msg-'+item.uninxtime+'-'+item.uid+'"><div class="  chartroom_head"><img src="{pigcms{$config.site_url}/upload/User_level/'+item.pic+'" alt=""/></div><div class="chartroom_txt"><div class="chartroom_name"><h3>ID:'+item.uid+'</h3><i></i><em>'+item.time+'</em>'+ ((item.is_sys_op == 1) ? (' - 同步助手- <font color=green>'+item.chujia_name + '</font>') :'') +'</div><p>'+item.msg+'</p></div></dd>';
		});
		if(imHtml != ''){
			$('.chartroom_list').html(imHtml);
		}

		//锁住屏幕
		if(result.info.lockScreen == '1'){
		<if condition="empty($_SESSION['system'])">lockScreenFunction();</if>
			lockScreen = true;
		<if condition="!empty($_SESSION['system'])">$('#lockScreen').html('解锁屏幕');</if>
		}else{
			lockScreen = false;
		<if condition="!empty($_SESSION['system'])">$('#lockScreen').html('锁屏保护');</if>
		}
		layer.close(indexShade);
		lastAjaxTime = result.info.lastAjaxTime;
	});
	<if condition="!empty($_SESSION['system'])">
		function lockScreenFunction(){
			if(lockScreenIndex){
				layer.close(lockScreenIndex);
			}
			lockScreenIndex = layer.open({
				type: 1,
				closeBtn: 0, //不显示关闭按钮
				shift: 2,
				shadeClose: false, //开启遮罩关闭
				content: '<div style="padding:40px;">拍卖主持人关闭了屏幕操作</div>'
			});
		}
		$('#lockScreen').click(function(){
			layer.open({
				content: '您确定要解锁用户的屏幕吗？',
				btn: ['确认', '取消'],
				shadeClose: false,
				yes: function(){
					//indexShade = layer.open({type: 2});
					$.post("{pigcms{:U('VideoSale/lockScreen')}",{lockScreen: lockScreen ? '0' : '1'},function(result){
						if(lockScreen){
							lockScreen = false;
							$('#lockScreen').html('锁屏保护');
						}else{
							lockScreen = true;
							$('#lockScreen').html('解锁屏幕');
						}
						layer.closeAll();
					});
				}, no: function(){
					layer.open({content: '你选择了取消', time: 1});
				}
			});
		});
	$('#finishVideoSale').click(function(){

		layer.open({
			content: '您确定要结束此域名的拍卖吗？',
			btn: ['确认', '取消'],
			shadeClose: false,
			yes: function(){
				$.post("{pigcms{:U('VideoSale/finishDomain')}",{term_id:term_id,domainId:nowDomainId},function(result){
					layer.closeAll();
					if(result.status == 1){
						layer.open({
							content:'结束成功',
							time: 3,
							btn: ['OK']
						});
					}else{
						layer.open({
							content:result.info,
							time: 3,
							btn: ['OK']
						});
					}
				});
			}, no: function(){
				layer.open({content: '你选择了取消', time: 1});
			}
		});
	});
	</if>
	$('.sound_switch,.sound_off').click(function(){
		if(showSound == true){
			showSound = false;
			$(this).addClass('sound_off');
		}else{
			showSound = true;
			$(this).removeClass('sound_off');
		}
	});

	$('.video-wrap .tab-wrap .tab').click(function(){
		$(this).css('color','rgb(255,102,0)').siblings('div').css('color','rgb(102,102,102)');
		if($(this).index() == 1){
			$('.video-wrap .tab-content .domain-list-wrap').show();
			$('.video-wrap .tab-content .domain-info-wrap').hide();
		}else{
			$('.video-wrap .tab-content .domain-info-wrap').show();
			$('.video-wrap .tab-content .domain-list-wrap').hide();
		}
	});

	$('.btn-add').click(function(){
		$('.btn-subtract').removeClass('disable');
		$('#bidding').val(parseInt($('#bidding').val())+plusPrice);
	});
	$('.btn-subtract').click(function(){
		if($('.btn-subtract').hasClass('disable')){
			return false;
		}
		var biddingMoney = parseInt($('#bidding').val())-plusPrice;
		if(biddingMoney == pleasePrice){
			$('.btn-subtract').addClass('disable');
		}
		$('#bidding').val(biddingMoney);
	});
	$('#sendPrice').click(function(){
		if(myUid == 0){
			layer.open({
				content: '请您先进行登录？',
				btn: ['去登录', '取消'],
				shadeClose: false,
				yes: function(){
					window.location.href = "{pigcms{:U('Login/login')}";
				}, no: function(){
					layer.open({content: '你选择了取消', time: 1});
				}
			});
			return false;
		}
		if($('#sendPrice').hasClass('disable')){
			layer.open({
				content: $(this).data('errTips'),
				time: 3,
				btn: ['OK']
			});
			return false;
		}

		layer.open({
			content: '您确定出价 <span style="color:red;font-size:18px;">￥'+toThousands($('#bidding').val())+'</span> 嘛？',
			btn: ['确认', '取消'],
			shadeClose: false,
			yes: function(){
				$.post("{pigcms{:U('VideoSale/sendprice')}",{term_id:term_id,domainId:nowDomainId,offer:$('#bidding').val()},function(result){
					layer.close(indexShade);
					if(result.status == 1){
						$('#myzuigaojia,#zuigaojia').html('￥'+toThousands(result.info));
						$('#sendPrice').addClass('disable').data('errTips','您已经出过价，需要等待其他人出价后您才能再次出价。');
						layer.open({
							content:'出价成功',
							time: 3,
							btn: ['OK']
						});
					}else{
						if(result.info[0] == 1004){
							$('#bidding').val(result.info[1]);
							var errMsg = '您的报价已被超越';
						}else{
							var errMsg = result.info[1];
						}
						layer.open({
							content: errMsg,
							time: 3,
							btn: ['OK']
						});
					}
				});
			}, no: function(){
				layer.open({content: '你选择了取消', time: 1});
			}
		});

	});
	$('.chat-emotion').toggle(function(){
		$('.emotion-wrap').show();
	},function(){
		$('.emotion-wrap').hide();
	});
	$('.emotion-wrap .emotion-img').click(function(){
		var chatMsg = $('input[name=chatMsg]').val();

		var url="<img src=\""+$(this).attr('src')+"\" />";
		chatMsg+=url;
		if('<span class="placeholder">聊天内容</span>' == $(".chat-input[contenteditable=true]").html()){
			$(".chat-input[contenteditable=true]").html('');
		}
		$(".chat-input[contenteditable=true]").html(chatMsg);
		$("input.chat-input[contenteditable=true]").val(chatMsg);
	});
	$('#sendMsg').click(function(){
		sendMsg(0);
	});
	//回到顶
	$('#chat-input').click(function(){
		if(navigator.userAgent.indexOf('Firefox') >= 0){
			//var scrollTop = document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop;
			//alert("访问WAP页面的时候，建议使用Chrome浏览器");
		}else{
			$("html,body").animate({
				scrollTop:"0px"
			},50);
		}

	});

	$('#chat-input').keyup(function(e){
		var chatMsg = $('input[name=chatMsg]').val();
		if (chatMsg && e.keyCode == 13) {
			// $('input[name=chatMsg]').val($("div.chat-input[contenteditable=true]").html());
			sendMsg(1);
			$("div.chat-input[contenteditable=true]").empty();
			$("input.chat-input[contenteditable=true]").val("");
		}

	});
	function toThousands(num) {
		var num = (num || 0).toString(), result = '';
		while (num.length > 3) {
			result = ',' + num.slice(-3) + result;
			num = num.slice(0, num.length - 3);
		}
		if (num) { result = num + result; }
		return result;
	}
	function sendMsg(kind){
		if(myUid == 0){
			layer.open({
				content: '请您先进行登录？',
				btn: ['去登录', '取消'],
				shadeClose: false,
				yes: function(){
					window.location.href = "{pigcms{:U('Login/login')}";
				}, no: function(){
					layer.open({content: '你选择了取消', time: 1});
				}
			});
			return false;
		}
		var chatMsg = $('input[name=chatMsg]').val();
		if(chatMsg.length < 1){
			layer.open({
				content:'随便写点东西呗',
				time: 3,
				btn: ['OK']
			});
			$("div.chat-input[contenteditable=true]").empty();
			$("input.chat-input[contenteditable=true]").val("");
			return;
		}
		if(chatMsg.length>100){
			layer.open({
				content:'发送内容不能超过100个字符，少发点试试',
				time: 3,
				btn: ['OK']
			});
			$('input[name=chatMsg]').val('');
			return;
		}
		$('input[name=chatMsg]').val('');
		$.post("{pigcms{:U('VideoSale/sendmsg')}",{msg:chatMsg,domainId:nowDomainId},function(result){

		});
	}
	var ajaxTimeout = null;
	function ajaxGetData(){
		if(term_id != 0){
			clearTimeout(ajaxTimeout);
			$.post("{pigcms{:U('VideoSale/ajaxCheck')}",{term_id:term_id,domainId:nowDomainId,lastAjaxTime:lastAjaxTime,lockScreen:lockScreen ? '1' : '0'},function(result){
				//在线会员
				if(result.info.onlineUsers){
					var userHtml = '';
					$.each(result.info.onlineUsers,function(i,item){
						if($('.member_'+item.uid).size() == 0){
							userHtml+= '<div class="member member_'+item.uid+'" data-memberid="'+item.uid+'"><img alt="" class="member-header" data-index="0" src="{pigcms{$config.site_url}/upload/User_level/'+item.pic+'" data-userid="'+item.uid+'"/><div class="member-info"><div class="name">'+item.name+'</div><div class="id">ID：'+item.uid+'</div></div><div class="clr"></div></div>';
						}
					});
					$('#memberDom').append(userHtml);
				}
				//下线会员
				if(result.info.unlineUsers){
					$.each(result.info.unlineUsers,function(i,item){
						$('.member_'+item.uid).remove();
					});
				}
				//当前拍卖
				if(result.info.nowDomainInfo){
					var nowDomainInfo = result.info.nowDomainInfo;
					if(nowDomainInfo != 'null'){
						var retain_money = parseInt(nowDomainInfo.retain_money);
						var NowMoney = nowDomainInfo.now_money.replace(/,/gi,'');
						$('.current-domain').html('<h2><span class="current-domain">当前拍卖域名：'+nowDomainInfo.domains+'</span><span><i>'+nowDomainInfo.domains_suffix+'</i> <i> '+(retain_money > 1 ? ( NowMoney >= retain_money ? '已过保留价':'有保留价' ) :'无保留价')+'</i></span></h2>');
						$('.current-domain-description').html(nowDomainInfo.domains_desc);
						$('#qipaijia').html(nowDomainInfo.mark_money!='0' ? nowDomainInfo.mark_money : '无');
						$('#zuigaojia').html(nowDomainInfo.now_money!='0' ? '￥'+nowDomainInfo.now_money : '无');
						plusPrice = nowDomainInfo.plusPrice;
						$('#jiajiafudu').html('￥'+plusPrice);
						pleasePrice = nowDomainInfo.pleasePrice;
						if(nowDomainInfo.domain_id != nowDomainId){
							$('#myzuigaojia').html('无');
							$('#bidding').val(pleasePrice);
							$('#bid_container').empty();
							$('.domain-'+nowDomainId+' .bidding').removeClass('bidding').addClass('passed');
							$('.domain-'+nowDomainId).next().find('.domain-name').removeClass('passed').addClass('bidding');
						}else if(parseInt($('#bidding').val()) < pleasePrice){
							$('#bidding').val(pleasePrice);
							$('.btn-subtract').addClass('disable');
						}
						nowDomainId = nowDomainInfo.domain_id;
						$('#sendPrice').removeClass('disable');
					}else{
						$('.bid-price').html('域名拍卖结束').css('text-align','center');
					}
				}


				//价格历史
				if(result.info.priceHistory){
					var chujiaHtml = '';
					$.each(result.info.priceHistory,function(i,item){
						//bid animated fadeIn price-1453779081-9
						if($('.price-'+item.uninxtime+'-'+item.uid).size() == 0){
							chujiaHtml += '<li class="clearfix price-'+item.uninxtime+'-'+item.uid+'"><div class="action_head"><img src="{pigcms{$config.site_url}/upload/User_level/'+item.pic+'"></div><div class="action_txt"><p>出价:￥<span>'+toThousands(item.price)+'元</span></p><p>ID:'+item.uid+'</p></div><div class="action_time"><i></i>'+item.time+  ((item.is_sys_op == 1) ? (' - 同步助手- <font color=green>'+item.chujia_name + '</font>') :'') +'</div></li>';
						}
						if(item.uid == myUid && item.is_sys_op != 1 ){
							$('#sendPrice').addClass('disable').data('errTips','您已经出过价，需要等待其他人出价后您才能再次出价。');
						}else{
							$('#sendPrice').removeClass('disable');
						}
					});
					if(chujiaHtml != ''){
						$('#bid_container').append(chujiaHtml);
						if($('#bid_container >li').size() > canShowChujiaRows){
							$('#bid_container >li:lt('+($('#bid_container >li').size() - canShowChujiaRows)+')').remove();
						}
						$('.temporary_img').remove();
						$('#bid_container >li:last .action_txt').append('&nbsp;&nbsp;<img class="temporary_img" src="{pigcms{$static_path}VideoSale/img/1st.png"/>');
						if(showSound == true){
							$('#priceMedia').remove();
							/*$('body').append('<video controls="" autoplay="" name="media" id="priceMedia"><source src="{pigcms{$static_path}VideoSale/img/ring.mp3" type="audio/mpeg"></video>');*/
						}
					}
				}
				//聊天历史
				if(result.info.msgHistory){
					var imHtml = '';
					$.each(result.info.msgHistory,function(i,item){
						if($('.msg-'+item.uninxtime+'-'+item.uid).size() == 0){
							imHtml += '<dd class="clearfix '+(item.uid==myUid ? "mine":"")+' msg-'+item.uninxtime+'-'+item.uid+'"><div class="  chartroom_head"><img src="{pigcms{$config.site_url}/upload/User_level/'+item.pic+'" alt=""/></div><div class="chartroom_txt"><div class="chartroom_name"><h3>ID:'+item.uid+'</h3><i></i><em>'+item.time+'</em>'+ ((item.is_sys_op == 1) ? (' - 同步助手- <font color=green>'+item.chujia_name + '</font>') :'') +'</div><p>'+item.msg+'</p></div></dd>';
						}
					});
					if(imHtml != ''){
						$('.chat-chartroom_list-wrap .empty').remove();
						$('.chartroom_list').append(imHtml);
						$(window).scrollTop($('body').height()+10000);
					}
				}

				//锁住屏幕
				if(result.info.lockScreen){
					if(result.info.lockScreen == '1'){
					<if condition="empty($_SESSION['system'])">lockScreenFunction();</if>
						lockScreen = true;
					<if condition="!empty($_SESSION['system'])">$('#lockScreen').html('解锁屏幕');</if>
					}else{
						lockScreen = false;
						layer.close(lockScreenIndex);
					<if condition="!empty($_SESSION['system'])">$('#lockScreen').html('锁屏保护');</if>
					}
				}

				layer.close(indexShade);
				lastAjaxTime = result.info.lastAjaxTime;

				ajaxGetData();
			});
		}
	}
	ajaxTimeout = setInterval(function(){
		ajaxGetData();
	},3000);
</script>
</body>

</html>
