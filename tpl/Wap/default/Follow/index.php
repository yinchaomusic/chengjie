<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>关注</title>
	<else/>
		<title>关注-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/swiper.min.css?t={pigcms{$_SERVER.REQUEST_TIME}">
</head>

<body class="index_content">
<if condition="!$is_wexin_browser">
<header class="header">
    <p>关注</p>
</header>
</if>
<article>
    <section>
        <div class="banner">
            <div class="swiper-container s1 swiper-container-horizontal">
                <div class="swiper-wrapper" style="transition-duration: 0ms; -webkit-transition-duration: 0ms;  ">
                    <pigcms:adver cat_key="wap_index_banner" limit="5" var_name="wap_index_banner">
                        <div class="swiper-slide  swiper-slide-prev">
                            <a href="{pigcms{$vo.url}" target="_blank">
                                <img src="{pigcms{$vo.pic}"/>
                            </a>
                        </div>
                    </pigcms:adver>
                </div>
                <div class="swiper-pagination p1 swiper-pagination-clickable"><span class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span><span class="swiper-pagination-bullet"></span></div>
            </div>
        </div>
    </section>
    <section>

        <ul class="office_list follow_list">
<!--            <li class="clearfix">-->
<!--                <div class="office_list_img"><img src="{pigcms{$static_path}images/ui_37.png"></div>-->
<!--                <div class="office_list_text">-->
<!--                    <h2>距下次拍卖还剩</h2>-->
<!--                    <span class="follow_time"><b>2天</b><b>20时</b><b>20分</b><b>20秒</b></span>-->
<!--                    <p>诚介网视频拍卖规则</p>-->
<!--                    <a href="#"> </a></div>-->
<!--            </li>-->
            <li class="clearfix">
                <a href="{pigcms{:U('VideoSale/index')}">
					<div class="office_list_img"><img src="{pigcms{$static_path}images/UI-2_14.png"></div>
					<div class="office_list_text">
						<h2>视频拍卖</h2>
						<p>每周一,四下午14点 准时开启拍卖！！</p>
						<a href="#"><i></i></a>
					</div>
                </a>
            </li>
<!--            <li class="clearfix">-->
<!--                <div class="office_list_img"><img src="{pigcms{$static_path}images/ui_47.png"></div>-->
<!--                <div class="office_list_text">-->
<!--                    <h2>极速竞价</h2>-->
<!---->
<!--                    <p>竞价时间固定为当天！</p>-->
<!--                    <a href="#"><i></i></a></div>-->
<!--            </li>-->
        </ul>
    </section>
</article>

<include file="Public:footer"/>

<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
<script src="{pigcms{$static_path}js/rem.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
<script src="{pigcms{$static_path}js/swiper.min.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
<script>
    var mySwiper = new Swiper('.s1', {
        loop: false,
        autoplay: 3000,
        pagination: '.p1',
        paginationClickable: true
    });
</script>
</body>
</html>