<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>修改信息</title>
	<else/>
		<title>修改信息-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>

<body class=" ">
<header class="header">
    <a href="{pigcms{:U('Profile/profile')}"><i></i></a>
    <p>修改信息</p>
</header>
<article>
    <section>
        <ul class="shopping_list">
            <li><span>用户昵称</span>
                <input type="text" name="nickname" id="nickname" value="{pigcms{$user_info.nickname}"/>
            </li>
        </ul>
        <button class="purchasing_button" onclick="saveUser('nickname')">保存</button>
    </section>
</article>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script>
function saveUser(obj){
    if(obj == 'nickname'){
        var nickname = $.trim($("#nickname").val());
        if(!nickname){
            layer.open({
                    content: "不可以为空",
                    btn: ['OK'],
                    shadeClose: true,
                    yes: function(){
                            window.location.href=location.href;
                    }
            });
            return false;
        }else{
            var data = {"nickname":nickname};
        }
    }
    $.post("{pigcms{:U('Profile/user_save')}", data, function(data){
//            alert(data);
            data = $.parseJSON(data);
            
            if(data){
                if(data.error == 0){
                    layer.open({
                            content: data.msg,
                            btn: ['OK'],
                            shadeClose: true,
                            yes: function(){
                                    window.location.href="{pigcms{:U('Profile/profile')}";
                            }
                    });
                }else{
                    layer.open({
                            content: data.msg,
                            btn: ['OK'],
                            shadeClose: true,
                            yes: function(){
                                    window.location.href=location.href;
                            }
                    });
                }
            }
        })
}
</script>
{pigcms{$hideScript}
</body>
</html>


