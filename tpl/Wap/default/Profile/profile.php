<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>用户信息</title>
	<else/>
		<title>用户信息-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>

<body class="didding_details">

<header class="header bidding_header">
	<a href="{pigcms{:U('Profile/index')}"><i></i></a>
	<p>用户信息</p>
</header>
<article>
	<section>

		<ul class="user_list mg_top user_info_list">
			<li style="padding-right:0px;"><span>头像</span><span><img src="{pigcms{$static_path}images/ui_1_03_03.png" alt=""></span></li>
            <a href ="{pigcms{:U('Profile/user_show')}"><li><span>昵称</span><span>{pigcms{$user_info.nickname} </span><i> </i></li></a>
			<li><span>UID</span><span>{pigcms{$user_info.uid}</span></li>
			<li><span>我的米表外链</span><span><a <if condition="$mburl eq ''">href="{pigcms{:U('Home/mblist',array('mmid'=>$user_info['uid']))}"<else/>href="{pigcms{$mburl}" </if> >访问</a></span><i> </i></li>

		</ul>
		<ul class="user_list mg_top user_info_list">
<!--			<li><span>操作绑定</span><span>已设置</span><i> </i> </li>-->
                    <li><span>手机绑定</span><span><if condition="$user_info['is_check_phone'] eq 0"><a href ="{pigcms{:U('Profile/is_check_phone')}">未绑定</a><elseif condition="$user_info['is_check_phone'] eq 1" />已绑定</if> </span><i> </i></li>
			<li><span>邮箱认证</span><span class="blue"><if condition="$user_info['is_check_email'] eq 0"><a href ="#">未绑定</a><elseif condition="$user_info['is_check_email'] eq 1" />已绑定</if></span><i></i></li>

		</ul>

		 
                    <p style=" text-align:center;margin-top: 20px;"> <button style="width: 80%;height: 36px;color: #fff;background: red;line-height: 36px;border-radius: 5px;" id="logout">退出登录</button></p>
		 
	</section>
</article>

<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script>
var logout_url = "{pigcms{:U('Profile/logout')}";
$("#logout").click(function(){
    layer.open({
        content: '你是想确认呢，还是想取消呢？',
        btn: ['确认', '取消'],
        shadeClose: true,
        yes: function(){
            $.post(logout_url,{},function(result){
                result = $.parseJSON(result);
                if(result){
                        if(result.error == 0){
                                layer.open({
                                    content: result.msg,
                                    btn: ['OK'],
                                    shadeClose: true,
                                    yes: function(){
                                        window.location.href='index.php?g=Wap&c=Home&a=index';
                                    }
                                });
                        }
                }
            })
        }
    });
})
</script>
</body>

</html>