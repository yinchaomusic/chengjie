<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>冻结明细</title>
	<else/>
		<title>冻结明细-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
</head>

<body class=" ">
<header class="header bidding_header">
    <a href="#" onclick="javascript:history.go(-1);"><i></i></a>
    <p>冻结明细</p><span class="select_list"></span>
</header>
<article>
    <section>
        <table class="explain_list_2">
            <thead>
            <tr>
                <th>冻结时间</th>
                <th>冻结金额</th>
                <th>说明</th>

            </tr>
            </thead>
            <tbody>
            <volist name="user_freeze_list" id="vo">
            <tr>
                <td>{pigcms{$vo.freezetime|date='Y-m-d H:i:s',###}</td>
                <td>￥{pigcms{$vo.freezemoney|number_format}</td>
                <td>{pigcms{$vo.info}</td>
            </tr>
            </volist>

            </tbody>
        </table>
    </section>
</article>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
{pigcms{$hideScript}
</body>
</html>