<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>财务明细</title>
	<else/>
		<title>财务明细-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
</head>

<body class=" ">
<header class="header bidding_header">
	<a href="#" onclick="javascript:history.go(-1);"><i></i></a>
	<p>财务明细</p><!--span class="select_list"><p>全部</p>
		<ul >
			<li class="list_show">所有分类</li>
			<li >充值</li>
			<li>提现</li>
			<li>交易收入</li>
			<li>交易支出</li>
			<li>违约补偿</li>
			<li>违约扣款</li>
			<li>下线提成</li>
			<li>冻结记录</li>
		</ul>
		<em></em></span-->
</header>
<article style="min-height: 300px;">
	<section>
		<table class="explain_list_2">
			<thead>
			<tr>
				<th>时间</th>
				<th>类型</th>
				<th>金额</th>
				<th>说明</th>
			</tr>
			</thead>
			<tbody>
			<volist name="user_money_list" id="vo">
			<tr>
				<td>{pigcms{$vo.time|date='Y-m-d H:i:s',###}</td>
				<td><if condition="$vo['type'] eq 1">充值
						<elseif condition="$vo['type'] eq 2"/>提现
						<elseif condition="$vo['type'] eq 3"/>交易收入
						<elseif condition="$vo['type'] eq 4"/>交易支出
						<elseif condition="$vo['type'] eq 5"/>违约补偿
						<elseif condition="$vo['type'] eq 6"/>违约扣款
						<elseif condition="$vo['type'] eq 7"/>下线提成
						<elseif condition="$vo['type'] eq 12"/>资金冻结
						<elseif condition="$vo['type'] eq 13"/>资金解冻
						<else/>-
					</if></td>
				<td> <if condition="($vo['type'] eq 12) OR ($vo['type'] eq 4) OR ($vo['type'] eq 6) OR ($vo['type'] eq 7) OR
					                ($vo['type'] eq 2)">
						<font color="red">-￥{pigcms{$vo.money|number_format}</font>
						<else/>
						<font color="blue">￥{pigcms{$vo.money|number_format}</font>
					</if></td>
				<td>{pigcms{$vo.desc}</td>

			</tr>
			</volist>
			</tbody>
		</table>
	</section>
</article>
</body>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
{pigcms{$hideScript}
</html>