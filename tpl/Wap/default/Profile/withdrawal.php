<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>账户余额</title>
	<else/>
		<title>账户余额-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
</head>

<body class="didding_details">
<header class="header bidding_header">
	<a href="#" onclick="javascript:history.go(-1);"><i></i></a>
	<p>账户余额</p>
</header>
<article>
	<section>
		<div class="  clearfix balance_banner">

			<div class="user_txt">
				<h2>总金额</h2>
				<p>￥ {pigcms{:number_format($user_info['now_money']+$user_info['freeze_money'])} 元</p>
			</div>

		</div>
		<ul class="user_list  ">
			<li class="clearfix">
				<a href="{pigcms{:U('Profile/record')}">
				<span> 可用余额</span><span class="red">￥{pigcms{:number_format($user_info['now_money'])}元</span><i></i>
				</a>
			</li>

			<li>
				<a href="{pigcms{:U('Profile/user_freeze')}">
				<span> 已冻结金额</span><span class="red">￥{pigcms{:number_format($user_info['freeze_money'])}元</span><i></i>
				</a>
			</li>
<!--			<li><span><img src="{pigcms{$static_path}images/UI-1-20-13_22.png">我的店铺</span><span></span><i></i></li>-->
		</ul>
	</section>
</article>

<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
{pigcms{$hideScript}
</body>
</html>