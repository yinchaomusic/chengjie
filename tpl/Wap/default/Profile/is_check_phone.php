<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>手机绑定</title>
	<else/>
		<title>手机绑定-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>
<body>
<header class="header">
    <a href="#" onclick="javascript:history.go(-1);"><i></i></a>
    <p>手机绑定</p>
</header>
<article>
    <section>
        <ul class="shopping_list">
            <li>
				<span>手机号</span>
                <input type="text" name="phone" id="phone" value="{pigcms{$user_session.phone}"/>
            </li>
			<li>
				<span>验证码</span>
				<input type="text" id="txtcode" name="txtcode" value=""/>
				<button style="width: 80px;height: 22px;color: #fff;border-radius: 5px;background: #16b3ff;    position: absolute;right: 0;top: 10px;" onclick="getCode();return false;">获取验证码</button>
			</li>
        </ul>
        <button class="purchasing_button" id="bind">绑定</button>
    </section>
</article>
</body>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script>
    var get_code_url = "{pigcms{:U('Profile/sendCode')}";
    var sub_url = "{pigcms{:U('Profile/bindphone')}";
    function getCode(obj){
            var check_phone = $("#phone").val();
                    $.post(get_code_url,{phone:check_phone},function(result){
                        result = $.parseJSON(result);
                        if(result){
                                if(result.error == 0){
                                        layer.open({
                                            content: result.msg,
                                            btn: ['OK']
                                        });
//                                                location.href="/index.php?g=Wap&c=Login&a=regcheck";
                                }else{
                                        $('#'+result.dom_id).focus();
                                        layer.open({
                                            content: result.msg,
                                            btn: ['OK']
                                        });
                                }
                        }else{
//                                        swal('','获取验证码出现异常，请重试！','error');
                                layer.open({
                                    content: result.msg,
                                    btn: ['OK']
                                });
                        }
                    });
            //}
    }
    
    
    $("#bind").click(function(){
        var phone = $("#phone").val();
        var txtcode = $("#txtcode").val();
        if(!txtcode){
            
            layer.open({
                content: "请填写验证码",
                btn: ['OK']
            });
            $("#txtcode").focus();
            return false;
        }
        $.post(sub_url,{"txtcode":txtcode,"phone":phone},function(result){
            result = $.parseJSON(result);
            if(result){
                    if(result.error == 0){
                            layer.open({
                                content: result.msg,
                                btn: ['OK'],
                            shadeClose: true,
                            yes: function(){
                                window.location.href='index.php?g=Wap&c=Profile&a=profile';
                            }
                            });
                    }else{
                            $('#'+result.dom_id).focus();
                            layer.open({
                                content: result.msg,
                                btn: ['OK']
                            });
                    }
            }
        })
        

    })
    

</script>
{pigcms{$hideScript}
</html>


