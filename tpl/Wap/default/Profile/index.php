<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>个人中心</title>
	<else/>
		<title>个人中心-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/swiper.min.css">
</head>
<body class="didding_details">
<header class="header bidding_header">
    <a href="#" onclick="javascript:history.go(-1);"></a>
    <p>个人中心</p><span class="scanning"> </span>
</header>
<article>
    <section>
        <a href="{pigcms{:U('Profile/profile')}">
            <div class="user_banner clearfix">
                <!--先判断是否登录-->
				<div class="user_haed"><img src="{pigcms{$static_path}images/ui_1_03_03.png" alt=""></div>
				<div class="user_txt">
					<h2>{pigcms{$user_session.nickname}</h2>
					<p>UID：{pigcms{$user_session.uid}</p>
				</div>
				<i></i>
            </div>
        </a>
        <ul class="user_list  ">
            <a href="{pigcms{:U('Buyer/index')}">
                <li class="clearfix">
                    <span><img src="{pigcms{$static_path}images/ui_1_03_11.png">我是买家</span><i></i>
                </li>
            </a>
           <a href="{pigcms{:U('Seller/index')}"> 
               <li>
                    <span><img src="{pigcms{$static_path}images/ui_1_03_18.png">我是卖家</span><i></i>
                </li>
            </a>
<!--            <li><span><img src="{pigcms{$static_path}images/ui_1_03_20.png">我的店铺</span><span></span><i></i></li>-->
            <a href="{pigcms{:U('Profile/withdrawal')}">
                <li>
                    <span><img src="{pigcms{$static_path}images/ui_1_03_30.png">账户余额</span><span class="yellow">{pigcms{:number_format($user_session['now_money']+$user_session['freeze_money'])}元</span><i></i>
                </li>
            </a>
            <a href="{pigcms{:U('Problem/index')}">
                <li>
                    <span><img src="{pigcms{$static_path}images/ui_1_03_32.png">问题反馈</span><span></span><i></i>
                </li>
            </a>
        </ul>
    </section>
</article>
<include file="Public:footer"/>

<script>
    // var reg_url = "{pigcms{:U('Whois/register')}";
    var trade_url = "{pigcms{:U('Trade/tradesearch')}";
    var whois_url = "{pigcms{:U('Whois/whois')}";
</script>

<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_path}js/swiper.min.js"></script>
<script>
    var mySwiper = new Swiper('.s1', {
        loop: false,
        autoplay: 3000,
        pagination: '.p1',
        paginationClickable: true
    });
</script>
{pigcms{$hideScript}
</body>
</html>