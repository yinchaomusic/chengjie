<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>域名详情</title>
	<else/>
		<title>域名详情-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>
<body class="didding_details">
<header class="header bidding_header">
	<a href="{pigcms{:U('Fastbid/index')}"><i></i></a>
	<p>域名详情</p> 
</header>
<article>
	<section>
		<div class="didding_details_title">
			<h4>{pigcms{$now_domain.domain}</h4><if condition="$collection_show eq 1"><i class="active" id="click_collection"></i><else/><i class="active_click" id="click_collection"></i></if><em></em>
                        <input type="hidden" value="{pigcms{$user_session.uid}" id="suid"/>
                        <input type="hidden" value="{pigcms{$now_domain.domain_id}" id="domainid" />
                        <input type="hidden" value="{pigcms{$now_domain.domain}" id="domain_name" />
                        <input type="hidden" value="{pigcms{$now_domain.type}" id="domain_type" />
			<p>{pigcms{$now_domain.desc}</p>
		</div>
		<ul class="didding_show clearfix">
			<li><span class="didding_show_price">{pigcms{$now_domain.money}元</span>
				<p>当前价格</p>
			</li>
			<li><span id="leftTime" class="c_red countdown_new">
                                    <if condition="$now_domain['overplus_time_d']"><strong>{pigcms{$now_domain.overplus_time_d}</strong>天</if><strong>{pigcms{$now_domain.overplus_time_h}</strong>时<strong>{pigcms{$now_domain.overplus_time_m}</strong>分<strong>{pigcms{$now_domain.overplus_time_ms}</strong>秒
				</span>
				<p>剩余时间</p>
			</li>
			<li><span><if condition="$fidList">{pigcms{$fidList.0.uid}<else/>暂无人出价</if></span>
				<p>当前领先</p>
			</li>
		</ul>
		<ul class="didding_list">
			<li><span>域名简介</span><span>{pigcms{$now_domain.desc}</span></li>
			<li><span>交易类型</span><span>竞价</span></li>
			<li><span>起拍价</span><span>￥{pigcms{$now_domain['mark_money']}元</span></li>
			<li><span>围观次数</span><span>{pigcms{$now_domain['hits']} 次</span></li>
			<li><span>加价幅度</span><span class="price_plus"> <a href="{pigcms{:U('Fastbid/showdetail',array('id'=>$now_domain['domain_id']))}">￥{pigcms{$now_domain.plusPrice.plusPrice}元<!--i></i--> </span></a></li>
			<li>
				<a href="{pigcms{:U('Fastbid/showpricerec',array('id'=>$now_domain['domain_id']))}"><span>出价记录</span><span >查看<!--i></i--></span></a>
			</li>
			<li><a href="{pigcms{:U('Fastbid/showdetail',array('id'=>$now_domain['domain_id']))}"><span>冻结保证金 </span><span>{pigcms{$now_domain.plusPrice.depositPrice}元 <!--i></i--> </span></a> </li>
			<li><span>拍卖周期</span><span><p>{pigcms{$now_domain.startTime|date='Y-m-d H:i',###}</p><p> {pigcms{$now_domain.endTime|date='Y-m-d H:i',###}</p></span></li>
		</ul>
            
            
            
		<div class="didding_form clearfix fastbidBox">
            <div><input id="cph_txt_bj" type="text" name="postPrice" value="{pigcms{$now_domain.pleasePrice}"/>元</div>
			<button id="jsbj">立即出价</button>
		</div>
	</section>
</article>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script src="{pigcms{$static_path}js/shijian.js"></script>
</body>

<script>
    var fastbidmoneyUrl = "{pigcms{:U('Fastbid/fastbidmoney')}";
    $("#jsbj").click(function(){
        var auctionId = $("#domainid").val();
        var offer = $("#cph_txt_bj").val();
        $.post(fastbidmoneyUrl, { auctionId:auctionId,offer:offer}, function (result) {
            result = $.parseJSON(result);
            if(result){
                if(result.error == 0){
                    layer.open({
                        content: result.msg,
                        btn: ['OK'],
                        shadeClose: true,
                        yes: function(){
                            window.location.href=location.href;
                        }
                    });
                }else{
                    layer.open({
                        content: result.msg,
                        btn: ['OK']
                    });
                }
            }
        });
    })
</script>
<script>
var collectionUrl = "{pigcms{:U('Bargain/collection')}";
</script>
<script>
    
	var erro_no = {pigcms{$error_no};
	var erro_msg = '{pigcms{$erro_msg}';
	var redirect = '{pigcms{$redirect_url}';
	if(erro_no){
		layer.open({
			content: erro_msg,
			btn: ['OK'],
			shadeClose: true,
			yes: function(){
				window.location.href=redirect;
			}
		});
	}
</script>

</html>