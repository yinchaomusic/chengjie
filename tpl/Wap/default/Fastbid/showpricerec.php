<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name='apple-touch-fullscreen' content='yes'>
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>出价记录</title>
	<else/>
		<title>出价记录-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css">

</head>
<body class=" ">
<header class="header">
	<a href="#" onclick="history.go(-1);"><i></i></a>
	<p>出价记录</p>
</header>
<article>
	<section>
		<ul class="explain_listp">
			<li><span>出价时间</span><span>竞价人</span><span>价格</span><span>状态</span></li>
			<volist name="fidList" id="vo">
				<li>
					<span>{pigcms{$vo.time|date='Y-m-d H:i:s',###}</span>
					<span>{pigcms{$vo.uid}</span>
					<span>￥{pigcms{$vo.money}</span>
					<span><if condition="$i eq 1">
							<font color="#FFA200">领先</font>
							<else/>
							<font color="#666">出局</font>
						</if></span>
				</li>
			</volist>
		</ul>
	</section>
</article>
</body>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>

</html>