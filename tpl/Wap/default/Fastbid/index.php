<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name='apple-touch-fullscreen' content='yes'>
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>极速竞价</title>
	<else/>
		<title>极速竞价-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>
<body class="">
<header class="header bidding_header">
	<a href="{pigcms{:U('Find/index')}"><i></i></a>
	<p>极速竞价</p>
	<a href="{pigcms{:U('Search/index')}" class="right"><em class="classify_icon"></em></a>
</header>
<article>
        <nav class="trade_title">
            <ul class="clearfix">
                <if condition="$status_type eq 0"><li class="active"><else/><li></if><a href="{pigcms{:U('Fastbid/index')}" title="">按时间排序</a></li>
                <if condition="$status_type eq 1"><li class="active"><else/><li></if><a href="{pigcms{:U('Fastbid/index',array('typeid'=>1))}" title="">按价格排序</a></li>
                <if condition="$status_type eq 2"><li class="active"><else/><li></if><a href="{pigcms{:U('Fastbid/index',array('typeid'=>2))}" title="">按长度排序</a></li>
            </ul>
	</nav>
	<if condition="is_array($fastbidList)">
		<section class="bidding">
			<ul class="clearfix">
               <volist id="vo" name="fastbidList">
                    <li>
					    <a href="{pigcms{$vo['url']}"><h4>{pigcms{$vo.domain|msubstr=###,0,8}</h4>
						<p>{pigcms{$vo.desc}</p>
						<p><span>￥{pigcms{$vo.money|number_format}元</span><span>{pigcms{$vo.over_time}</span></p></a>
					</li>
               </volist>
			</ul>
		</section>
		
		<else/>
		<section style="text-align: center; font-size: larger;color: #a6a6a6;margin-top: 10px;">
			<img src="{pigcms{$static_path}images/ui_26.png" alt=""><br>
			暂无数据
		</section>
	</if>

</article>
</body>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script>
	var erro_no = {pigcms{$error_no};
	var erro_msg = '{pigcms{$erro_msg}';
	var index_url = "{pigcms{:U('Home/index')}";
	if(erro_no){
		layer.open({
			content: erro_msg,
			btn: ['OK'],
			shadeClose: true,
			yes: function(){
				window.location.href=index_url;
			}
		});
	}

</script>
</html>