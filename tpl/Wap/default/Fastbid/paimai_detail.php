<!DOCTYPE html>
<html>
<head>
<title>拍卖详情 - {pigcms{$config.site_name}</title>
<meta charset="utf-8"/>
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<link href="{pigcms{$static_path}paimai/css/common.css" rel="stylesheet">

<link href="{pigcms{$static_path}paimai/css/frozen.css" rel="stylesheet">
<link href="{pigcms{$static_path}paimai/css/iconfont.css" rel="stylesheet">
<link href="{pigcms{$static_path}paimai/css/all.css" rel="stylesheet">
</head>
<body ontouchstart="" class="ms-loader">
	<div class="page-wrapper">
		<div data-title="出价记录" class="page active" id="bidding-list">
			<div class="ui-btn-wrap" style="position: relative;">
				<a href="javascript:window.history.go(-1);" style="position: absolute;z-index: 99;">
					<i class="ui-icon-return" style="color: white;"></i>
				</a>
				<!--ms-if-->
				<button class="ui-btn-lg ui-btn-primary active" style="color: white;">
				拍卖中&nbsp;&nbsp;{pigcms{$now_domain.endTime|date='m月d日 H:i',###}&nbsp;&nbsp;结拍
				</button>
			</div>
			<div class="ui-whitespace"><p class="ui-txt-info">{pigcms{$now_domain.domain} 出价次数 {pigcms{:count($fidList)}</p></div>
			<section class="ui-container">
				<!-- <div ms-widget="overscroll"></div> -->
				<div class="">
					<ul class="ui-list ui-list-text ui-list-cover ui-border-tb">
						<volist name="fidList" id="vo">
							<li class="ui-border-b" style="padding-top:0px;padding-bottom:0px;">
								<div class="ui-list-thumb" style="margin-bottom: 6px;margin-top: 5px; margin-right: 2px;">
									<a href="#"><img width="46px" height="46px" class="auction-domain-size" src="{pigcms{$vo.photo}"/></a>
								</div>
								<div class="ui-list-info">
									<ul class="ui-row">
										<li class="ui-col ui-col-50" style="  padding-bottom: 0px;  padding-top: 0px;">
											<!-- <h4 class="ui-nowrap" style="color: #576b95">{{el.nickname}}</h4> -->
											<div class="ui-txt-warning" style="  margin-top: 9%;">￥{pigcms{$vo.money} 元</div>
										</li>
										<li class="ui-col ui-col-50">
											<div class="ui-flex ui-flex-pack-end">
												<div>
													<if condition="$i eq 1">
														<div class="iconfont icon-top2 center" style="color: #eb4f38; font-size: 23px; height: 23px"></div>
													<else/>
														<div class="iconfont icon-out ui-txt-info center" style="font-size: 23px; height: 23px"></div>
													</if>
													<p><span class="small-font ui-txt-info">{pigcms{$vo.bidTime}</span></p>
												</div>
												<!--ms-if-->
											</div>
										</li>
									</ul>
								</div>
							</li>
						</volist>
					</ul>
				</div>
			</section>
		</div>
	</div>
</body>
</html>