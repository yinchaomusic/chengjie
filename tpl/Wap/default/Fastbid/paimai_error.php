<!DOCTYPE html>
<html>
<head>
	<title>{pigcms{$config.site_name}</title>
	<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<script type="text/javascript">
	var ctx = "/";
</script>

 
 
<link href="{pigcms{$static_path}paimai/css/common.css" rel="stylesheet">

<link href="{pigcms{$static_path}paimai/css/frozen.css" rel="stylesheet">
<link href="{pigcms{$static_path}paimai/css/iconfont.css" rel="stylesheet">
<link href="{pigcms{$static_path}paimai/css/all.css" rel="stylesheet">
	
</head>
<body ontouchstart="" class="ms-loader ms-loading">
	<div class="page-wrapper">
		<div data-title="关注公众号" class="page active ms-controller" ms-controller="error"	id="error">
			<section class="ui-container font-styles">
				<div class="mainContent" style="height: auto;">
					<div class="designerCard">
					<!--
						<div style="margin-top: 7%;width:100%;">
							<img src="" style=" width: 100%;  border-radius: 50%; box-shadow: 0 0 0 4px rgb(205, 218, 162);">
						</div>
					-->
						<div class="text-tip-follow">
							长按二维码关注我们<br>即可参加拍卖
						</div>
					</div>
					<div id="myCode" style="text-align: center;">
						<img src="{pigcms{$config.wechat_qrcode}" style="width: 80%;margin-top: 6%;"></img>
					</div>
					<div class="text-tip">
						<div>---------温馨提示---------</div>
						<div class="grid middle">
							<div  class="ng-binding" style="width: 80%;text-align: center; margin-left: 8%;">
								如果您已关注，仍然看到此页面，请联系经纪人确认是否被封号
							</div>
						</div>
					</div>
				</div>
			</section>
			<script type="text/javascript">
				define( function() {
					var vm = avalon.define({
						$id : "error",
						datas:{
						}
					});
					
					$("#error").on("pageloaded", function(e, d){
					});
					
					return vm;
				});	
			</script>
		</div>
	</div>
	
<script type="text/javascript" src="{pigcms{$static_path}paimai/js/all-common.js" ></script>

<script type="text/javascript" src="{pigcms{$static_path}paimai/vendors/avalon/avalon.mobile.min.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}paimai/js/require.config.js?4"></script>
<script type="text/javascript" src="{pigcms{$static_path}paimai/js/frozen-common.js"></script>

</body>
</html>