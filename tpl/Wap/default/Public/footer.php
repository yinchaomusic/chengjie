<footer class="index_footer">
	<ul>
		<li class="<if condition="$home_active">active</if>">
			<a href="{pigcms{:U('Home/index')}" >
				<i></i>
				<p>首页</p>
			</a>
		</li>
		<li class="<if condition="$follow_active">active</if>">
			<a href="{pigcms{:U('Follow/index')}" >
				<i></i>
				<p>关注</p>
			</a>
		</li>
		<li class="<if condition="$find_active">active</if>">
			<a href="{pigcms{:U('Find/index')}" >
				<i></i>
				<p>发现</p>
			</a>
		</li>
		<li class="<if condition="$profile_active">active</if>">
			<a href="{pigcms{:U('Profile/index')}" >
				<i></i>
				<p>我</p>  
			</a>
		</li>

	</ul>
</footer>