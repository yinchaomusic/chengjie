<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>问题列表</title>
	<else/>
		<title>问题列表-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>
<body class="didding_details">
<header class="header bidding_header">
    <a href="{pigcms{:U('Seller/index')}"><i></i></a>
    <p>问题列表</p>
</header>
<article>
    <nav class="auction_titlemore">
        <ul class="clearfix">
            <li ><a href="{pigcms{:U('Problem/index')}" title="">提交问题</a></li>
            <li class="active"><a href="{pigcms{:U('Problem/plist',array('type'=>1))}" title="">问题列表</a></li>
        </ul>
    </nav>
    <section>
        <ul class="quote_list">
            <if condition="is_array($problem_list)">
            <volist name="problem_list" id="vo">
                <li>
                    <ul>
                        <li><span>问题标题</span><span><strong>{pigcms{$vo.title}</strong></span></li>
                        <li><span>提问时间</span><span>{pigcms{$vo.addtime|date='Y-m-d H:i:s',###}</span></li>
                        <li><span>处理状态</span><span><if condition="$vo['state'] eq 0"><rp class="font_default">未回复</rp><else /><rp class="font_success">已回复</rp></if></span></li>
                        <if condition="$vo['state'] eq 1"><li><span>操作</span><span><a onclick="detail({pigcms{$vo['id']})"><i>查看详细</i></a></span></li>
                        </if>
                    </ul>
                </li>
            </volist>
<else/>
            <li>
                <ul>
                    <li><span></span><span><strong>暂无数据</strong></span></li>
                </ul>
            </li>
</if>
        </ul>
    </section>
</article>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>

<script>
    function detail(obj){
        layer.open({type: 2});
        var post_url = '{pigcms{:U('Problem/detail')}';
        var postdata = {'id': obj}
        $.post(post_url,postdata,function(result){
            result = $.parseJSON(result);
            console.log(result);

            if(result){
                if(result.error == 0){
                    var da =result.resultdata;

                    var pageii = layer.open({
                        type: 1,
                        content: '<section><p style="padding:.65rem;"><strong>查看问题</strong> <br> <b>您的提问:</b> '+da.title+'<br> <b>您的提问内容:</b>'+da.content+'<br/>  <b>提问时间:</b>'+da.addtime+'</p><hr/> <p style="padding:.65rem;"><strong>回复内容</strong><br/> '+da.reply+'<br/> <b> 回复时间:</b>'+da.replytime+' </p></section><button class="purchasing_button" onclick="layer.closeAll();">关闭</button>',
                        style: 'position:fixed; left:0; top:0; width:100%; height:100%; border:none;'
                    });

                }else{
                    layer.open({
                        content: result.msg,
                        btn: ['OK']
                    });

                }
            }else{
                layer.open({
                    content: result.msg,
                    btn: ['OK']
                });
            }
        });
    }

</script>
</body>


</html>