<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name='apple-touch-fullscreen' content='yes'>
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<title>诚介网-我的米表</title>
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>

<body class=" ">
<header class="header">
	<a href="{pigcms{:U('Seller/index')}"><i></i></a>
	<p>米表管理</p>
</header>
<article>
	<section style="margin: 5px;padding: 5px;">
		<h4>注意事项：</h4>
		<p> 1.请输入您的米表关键字，便于别人搜索，如：买域名,卖域名,购买域名,域名出售等。</p>
	</section>
	<form action="{pigcms{:U('Seller/mibiaoupdate')}" method="post" id="adddomains">
            <input type="hidden" name="mid" value="{pigcms{$mibiaoinfo.mid}">
		<section>
			<ul class="shopping_list">
                                <li><span>米表名称：</span>
					<textarea class="txer1" name="title">{pigcms{$mibiaoinfo.title|default='域名交易平台:域名交易,域名出售,域名竞价,域名拍卖,域名经纪,域名中介,中国优质域名交易平台'}</textarea>
				</li>
				<li><span>米表邮箱：</span>
					<input type="text" value="{pigcms{$mibiaoinfo.email}" name="email">
				</li>
				<li><span>米表QQ：</span>
					<input type="text" value="{pigcms{$mibiaoinfo.qq}" name="qq">
				</li>
               <li><span>米表电话：</span>
					<input type="text" value="{pigcms{$mibiaoinfo.phone}" name="phone">
				</li>
				<li><span>米表外链：</span>
					<input type="text" value="{pigcms{$mibiaoinfo.mburl}" name="mburl" placeholder="（选填）如果为空则显示系统默认界面">
				</li>
                 <li><span>关键字：</span>
					<textarea ph="" name="keyword">{pigcms{$mibiaoinfo.keyword|default='域名交易,域名拍卖,域名竞价,域名经纪,域名中介,域名抢注,域名询价,域名买卖,合肥彼岸互联信息技术有限公司,域名中国,域名查询,域名注册,域名工具,域名买卖,域名代购,Whois查询,域名出售页面,域名Whois,域名应用,域名新闻,域名门户,域名资讯,中文域名,cn域名,com域名'}</textarea>
				</li>
			</ul>
			<button type="submit" class="purchasing_button">提交</button>
		</section>
	</form>
</article>
</body>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>

</html>