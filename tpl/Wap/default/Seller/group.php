<html style=" ">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<title>诚介网-我的所有域名</title>
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">

</head>

<body class="didding_details">
<header class="header bidding_header">
	<a href="{pigcms{:U('Seller/index')}"><i></i></a>
	<p>域名分组管理</p>
</header>
<article>
    <nav class="auction_title" style="text-align: center;">
        <p style="padding: .5rem;"><a href="{pigcms{:U('Seller/group_add')}"><button  class="serach_domains_button">添加域名分组</button></a></p>
    </nav>
	<section>
            <ul class="quote_list">
                <volist id="vo" name="group_list">
                    <li>
                        <ul>
                            <li><span>组名</span><span><strong>{pigcms{$vo.group_name}</strong></span></li>
                            <li><span>域名数</span><span><strong>{pigcms{$vo.domain_count}</strong></span></li>
                            <li><span>备注</span><span><strong>{pigcms{$vo.group_desc}</strong></span></li>
                            <li><span>操作</span><span><a onclick="xdel({pigcms{$vo.group_id})"><i>删除</i></a> <a onclick="setprice({pigcms{$vo['group_id']},'{pigcms{$vo.group_name}','{pigcms{$vo['group_desc']}')"><i>设置</i></a></span></li>
                        </ul>
                    </li>
                </volist>
            </ul>
	</section>
</article>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
</body>

<script>
    function xdel(group_id){
        var post_url = "{pigcms{:U('Seller/group_del')}";
        var postdata = {'id': group_id}
        layer.open({
                content: '您确定要删除该域名？',
                btn: ['确认', '取消'],
                shadeClose: true,
                yes: function(){
                    $.post(post_url,postdata,function(result){
                        result = $.parseJSON(result);
                        if(result.error == 0){
                            layer.open({
                                content: result.msg,
                                btn: ['OK'],
                                shadeClose: true,
                                yes: function(){
                                    window.location.href=location.href;
                                }
                            });
                        }else{
                            layer.open({
                                content: result.msg,
                                btn: ['OK']
                            });
                        }
                    });
                }
        });
    }
    
    $("#group_add").click(function(){
        var pageii = layer.open({
            type: 1,
            content:'<form method="post" action="index.php?g=Wap&c=Seller&a=group_add"><header class="header"><p>域名分组管理-添加</p><a id="ceshi">测试</a></header><article><section><ul class="shopping_list"><li><span>组名：</span><input type="text" name="group_name" value="" placeholder="请输入组名"></li><li><span>备注：</span><textarea name="group_desc" placeholder="请输入备注"></textarea></li></ul><button type="submit" class="purchasing_button">提交</button></section></article></form><button class="purchasing_button" onclick="layer.closeAll();">关闭</button>',
            style: 'position:fixed; left:0; top:0; width:100%; height:100%; border:none;'
        });
    })

    function setprice(group_id,group_name,group_desc){
        var pageii = layer.open({
            type: 1,
            content:'<form method="post" action="index.php?g=Wap&c=Seller&a=group_save"><header class="header"><p>域名分组管理-修改</p></header><article><section><ul class="shopping_list"><input type="hidden" name="group_id" value="'+group_id+'"/><li><span>组名：</span><input type="text" name="group_name" value="'+group_name+'" placeholder="请输入组名"></li><li><span>备注：</span><textarea name="group_desc" placeholder="请输入备注">'+group_desc+'</textarea></li></ul><button type="submit" class="purchasing_button">提交</button></section></article></form><button class="purchasing_button" onclick="layer.closeAll();">关闭</button>',
            style: 'position:fixed; left:0; top:0; width:100%; height:100%; border:none;'
        });
    }
    

    
</script>

</html>