<html style=" ">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<title>诚介网-我的终端</title>
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css">
        <link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>

<body class="didding_details">
<header class="header bidding_header">
	<a href="{pigcms{:U('Seller/index')}" ><i></i></a>
	<p>我的终端</p>
</header>
<article>
	<nav class="auction_title">
            <ul class="clearfix">
                <if condition="$status_type eq 0"><li class="active"><else/><li></if><a href="{pigcms{:U('Seller/terminal')}" title="">审核记录</a></li>
                <if condition="$status_type eq 1"><li class="active"><else/><li></if><a href="{pigcms{:U('Seller/terminal',array('status'=>1))}" title="">推荐中</a></li>
                <if condition="$status_type eq 2"><li class="active"><else/><li></if><a href="{pigcms{:U('Seller/terminal',array('status'=>2))}" title="">推荐成功</a></li>
                <if condition="$status_type eq 3"><li class="active"><else/><li></if><a href="{pigcms{:U('Seller/terminal',array('status'=>3))}" title="">推荐失败</a></li>
            </ul>
	</nav>
    <if condition="$status_type eq 0">
        <section>
            <div class="find_title"><span>待审核</span></div>
            <ul class="quote_list">
                <volist id="vo" name="list">
                    <if condition="$vo.status eq 0">
                        <li>
                            <ul>
                                <li><span>域名</span><span>{pigcms{$vo.domain_name}</span></li>
                                <li><span>最低价格</span>{pigcms{$vo.min_price|number_format}</li>
                                <li><span>申请时间</span><span>{pigcms{$vo.add_time|date="Y-m-d H:i:s",###}</span></li>
                                <li><span>操作</span><span><a onclick="terminalDel('{pigcms{$vo.id}')"><i>删除</i></a><a onclick="terminalSave('{pigcms{$vo.id}','{pigcms{$vo.domain_name}','{pigcms{$vo.max_price}','{pigcms{$vo.min_price}','{pigcms{$vo.hope}','{pigcms{$vo.tt_msg}')"><i>修改</i></a></span></li>
                            </ul>
                        </li>
                    </if>
                </volist>
            </ul>
	</section>
        
        <section>
            <div class="find_title"><span>审核失败</span></div>
            <ul class="quote_list">
                <volist id="vo" name="list">
                    <if condition="$vo.status eq 2">
                        <li>
                            <ul>
                                <li><span>域名</span><span>{pigcms{$vo.domain_name}</span></li>
                                <li><span>最低价格</span>{pigcms{$vo.min_price|number_format}</li>
                                <li><span>申请时间</span><span>{pigcms{$vo.add_time|date="Y-m-d H:i:s",###}</span></li>
                                <li><span>操作</span><span><a onclick="terminalDel('{pigcms{$vo.id}')"><i>删除</i></a></span></li>
                            </ul>
                        </li>
                    </if>
                </volist>
            </ul>
        </section>
    <else/>
        <section>
            <ul class="quote_list">
                <volist id="vo" name="list">
                        <li>
                            <ul>
                                <li><span>域名</span><span>{pigcms{$vo.domain_name}</span></li>
                                <li><span>最低价格</span>{pigcms{$vo.min_price|number_format}</li>
                                <li><span>申请时间</span><span>{pigcms{$vo.add_time|date="Y-m-d H:i:s",###}</span></li>
                                <li><span>操作</span><span><a onclick="terminalDel('{pigcms{$vo.id}')"><i>删除</i></a></span></li>
                            </ul>
                        </li>
                </volist>
            </ul>
        </section>
    </if>
	
</article>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script>
    function terminalDel(obj){
        var post_url = "{pigcms{:U('Seller/terminal_del')}";
        var postdata = {'id': obj}
        layer.open({
                content: '您确定要删除该域名？',
                btn: ['确认', '取消'],
                shadeClose: true,
                yes: function(){
                    $.post(post_url,postdata,function(result){
                        result = $.parseJSON(result);
                        if(result.error == 0){
                            layer.open({
                                content: result.msg,
                                btn: ['OK'],
                                shadeClose: true,
                                yes: function(){
                                    window.location.href=location.href;
                                }
                            });
                        }else{
                            layer.open({
                                content: result.msg,
                                btn: ['OK']
                            });
                        }
                    });
                }
        });
    }
    
    function terminalSave(id,domain_name,max_price,min_price,hope,tt_msg){
        var pageii = layer.open({
            type: 1,
            content:'<form method="post" action="index.php?g=Wap&c=Seller&a=terminal_save"><header class="header"><p>我的终端-修改</p></header><ul class="shopping_list"><input type="hidden" name="id" value="'+id+'"/><li><span>域名</span><input type="text" value="'+domain_name+'" name="domain_name"></li><li><span>最低价格</span><input type="text" value="'+min_price+'" name="min_price"></li><li><span>期望价格</span><input type="text" value="'+max_price+'" name="max_price"></li><li><span>希望终端</span><input type="text" name="hope" value="'+hope+'"></li><li><span>留言</span><textarea name="tt_msg" placeholder="请输入简介">'+tt_msg+'</textarea></li></ul><button class="purchasing_button">确定</button></form><button class="purchasing_button" onclick="layer.closeAll();">关闭</button>',
            style: 'position:fixed; left:0; top:0; width:100%; height:100%; border:none;'
        });
    }
</script>
</body>

</html>