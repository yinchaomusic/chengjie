<html style=" ">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<title>诚介网-我的批量交易</title>
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">

</head>

<body class="didding_details">
<header class="header bidding_header">
	<a href="{pigcms{:U('Seller/index')}"><i></i></a>
	<p>我的批量交易</p>
</header>
<article>
<!--	<nav class="auction_title" style="text-align: center;">-->
<!--		<form action="{pigcms{:U('Seller/allsell')}"  method="post">-->
<!--			<p style="padding: .5rem;"><input type="text" class="serach_domains_input" name="search_key" placeholder="输入域名搜索"> <button type="submit" class="serach_domains_button">点我搜索</button></p>-->
<!--		</form>-->
<!--	</nav>-->
	<nav class="auction_titlemore">
		<ul class="clearfix">
			<li   <if condition="$type lt 1">class="active" </if>><a href="{pigcms{:U('Seller/pldomain')}"   title="">所有</a></li>
			<li <if condition="$type eq 1">class="active" </if> ><a href="{pigcms{:U('Seller/pldomain',array('type'=>1))}" title="">待审核</a></li>
			<li <if condition="$type eq 2">class="active" </if>><a href="{pigcms{:U('Seller/pldomain',array('type'=>2))}" title="">审核失败</a></li>
			<li <if condition="$type eq 3">class="active" </if>><a href="{pigcms{:U('Seller/pldomain',array('type'=>3))}" title="">出售中</a></li>
			<li <if condition="$type eq 4">class="active" </if>><a href="{pigcms{:U('Seller/pldomain',array('type'=>4))}" title="">已出售</a></li>
		</ul>
	</nav>
	<section>
		<ul class="quote_list">
			<volist name="bulk_lists" id="vo">
				<li>
					<ul>
						<li><span>标题</span><span><strong>{pigcms{$vo.title}</strong></span></li>
						<li><span>标价</span><span><if condition="$vo['total_price'] gt 0"><font color="#ff8c00">￥{pigcms{$vo.total_price|number_format} 元</font><else/>-</if></span></li>
						<li><span>域名个数</span><span>{pigcms{$vo.amount}</span></li>
						<li><span>提交时间</span><span><strong>{pigcms{$vo.add_time|date='Y-m-d',###}</strong></span></li>
						<li><span>状态</span><span><if condition="$vo['status'] eq 0"><strong class="font_default">未审核</strong><elseif condition="$vo['status'] eq 1"/><strong class="font_info">展示中</strong><elseif condition="$vo['status'] eq 2"/><strong class="font_important">出售中</strong><elseif condition="$vo['status'] eq 3"/><strong class="font_success">交易完成</strong><elseif condition="$vo['status'] eq 4"/><strong class="font_warning">审核失败</strong></if></span></li>
							<li><span>操作</span><span><if condition="$vo['status'] neq 2"><a href="javascript:void();return false;" onclick="xdel({pigcms{$vo['bulk_id']},'{pigcms{$vo.title}')"><i>删除</i></a></if><a href="javascript:void();" onclick="detail({pigcms{$vo['bulk_id']})"><i>查看详细</i></a></span></li>

					</ul>
				</li>
			</volist>

		</ul>
	</section>
</article>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
</body>

<script>
	function xdel(obj,domaintitle){
		var post_url = '{pigcms{:U('Seller/pldel')}';
		var postdata = {'id': obj}
		layer.open({
			content: '您确定要删除【'+domaintitle+'】？',
			btn: ['确认', '取消'],
			shadeClose: true,
			yes: function(){
				$.ajax({
					type: "post",
					url:post_url,
					data:postdata,
					dataType:"json",
					success:function(result){
						layer.open({content: result.msg +' 【' + domaintitle+'】', time: 1});
						window.location.reload();
					}
				});
			}
		});
	}

	function detail(obj){
		var postdata = {'id': obj}
		var post_url = '{pigcms{:U('Seller/getplist')}';
		layer.open({type: 2});
		$.ajax({
			type: "post",
			url:post_url,
			data:postdata,
			dataType:"json",
			success:function(result){
				var ptitle = "<section> <p style=padding:.65rem><strong>注意</strong> <br> 1.未通过域名可能是您未添加该域名的whois邮箱，请添加后重新验证。<br>2.如果添加了不属于您的域名请删除本次提交的批量交易重新添加。 <br>3.请联系经纪人验证域名。能是您未添加该域名的whois<br> </p> </section>";
//				var glist ="<select name=glistid>";
				var plist = '';
				var dlist =result.domain_list;
				var whoischeck = result.ischeck;
				var checkclass= result.checkcalss;
				plist += "<li>标题："+result.title+"</li>";
				plist += "<li>总价： ￥"+result.total_price+" 元</li>";
				plist += "<li>含义："+result.meaning+"</li>";
				plist += "<li class="+result.fcalss+">状态："+result.statustitle+"</li>";
				plist += "<li>域名列表</li>";
				for(i in dlist){
					plist += "<li>"+dlist[i]+"<span class="+checkclass[i]+">"+whoischeck[i]+"</span></li>";
				}
				var pageii = layer.open({
					type: 1,
					content: ''+ptitle+'<section><ul class="news_ul_domains">'+plist+'</li></ul></section><button class="purchasing_button" onclick="layer' +
					'.closeAll();">关闭</button>',
					style: 'position:fixed; left:0; top:0; width:100%; height:100%; border:none;'
				});
			}
		});


	}

</script>

</html>