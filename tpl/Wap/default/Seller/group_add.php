<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>添加域名分组</title>
	<else/>
		<title>添加域名分组-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">

</head>

<body class="didding_details">
<header class="header bidding_header">
	<a href="{pigcms{:U('Seller/group')}"><i></i></a>
	<p>添加域名分组</p>
</header>
<article>
    <section>
            <article>
                <section>
                    <ul class="shopping_list">
                        <li><span>组名：</span><input type="text" name="group_name" id="group_name" value="" placeholder="请输入组名"></li>
                        <li><span>备注：</span><textarea name="group_desc" id="group_desc" placeholder="请输入备注"></textarea></li>
                    </ul>
                    <button id="group_sub" class="purchasing_button">提交</button>
                </section>
            </article>
    </section>
</article>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
</body>

<script>
    $("#group_sub").click(function(){
        var group_name = $("#group_name").val();
        var group_desc = $("#group_desc").val();
        $.post("{pigcms{:U('Seller/group_add')}",{"group_name":group_name,"group_desc":group_desc},function(result){
            result = $.parseJSON(result);
            if(result.error == 0){
                layer.open({
                    content: result.msg,
                    btn: ['OK'],
                    shadeClose: true,
                    yes: function(){
                        window.location.href="index.php?g=Wap&c=Seller&a=group";
                    }
                });
            }else{
                layer.open({
                    content: result.msg,
                    btn: ['OK']
                });
            }
        });
    });
</script>

</html>