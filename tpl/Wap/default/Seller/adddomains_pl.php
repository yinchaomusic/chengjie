<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>添加批量交易</title>
	<else/>
		<title>添加批量交易-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>

<body class=" ">
<header class="header">
	<a href="{pigcms{:U('Seller/index')}"><i></i></a>
	<p>添加批量交易</p>
</header>
<article>
	<section class="terminal_tips">
		<h4>注意事项：</h4>
		<p>1.如果有审核未通过域名可能是您未添加该域名的whois邮箱，请添加后重新验证。</p>
		<p>2.如果添加了不属于您的域名请删除本次提交的批量交易重新添加。</p>
		<p>3.请联系经纪人验证域名。</p>
	</section>
	<section>
		<form id="add_domains_pl" method="post">
		<ul class="shopping_list">
			<li><span>域名列表</span>
				<textarea name="domain_list" id="domain_list" placeholder="请输入顶级域名，请使用英文逗号(,)分隔，如：ab.com,aa.cn"></textarea>
			</li>
			<li><span>标题</span>
				<input type="text" name="title" id="title" placeholder="请输批量交易的标题">
			</li>
			<li><span>总价格</span>
				<input type="text" name="total_price" id="total_price" placeholder="请输入总价格">
			</li>
			<li><span>含义</span>
				<textarea name="meaning" id="meaning" placeholder="请输入含义,可以更好的让买家了解"></textarea>
			</li>

		</ul>
		<button type="submit" class="purchasing_button">提交</button>
		</form>
	</section>
</article>
</body>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>

<script>
	$(function(){

		$('#add_domains_pl').submit(function(){
			//layer.open({type: 2});
			var add_check = '{pigcms{:U('Seller/adddomainss_pl_save')}';
			if($('#domain_list').val()=='' || $('#domain_list').val()==$('#domain_list').attr('placeholder')){
				layer.open({
					content: '必须填写要出售的域名列表',
					style: 'background-color:#ffa200; color:#fff; border:none;',
					time: 2
				});
				$('#domain_list').focus();
				return false;
			}else if($('#title').val()=='' || $('#title').val()==$('#title').attr('placeholder')){
				layer.open({
					content: '必须填写标题',
					style: 'background-color:#ffa200; color:#fff; border:none;',
					time: 2
				});
				$('#title').focus();
				return false;
			}else if($('#total_price').val()=='' || $('#total_price').val()==$('#total_price').attr('placeholder')){
				layer.open({
					content: '必须填写总价格',
					style: 'background-color:#ffa200; color:#fff; border:none;',
					time: 2
				});
				$('#total_price').focus();
				return false;
			}else if($('#meaning').val()=='' || $('#meaning').val()==$('#meaning').attr('placeholder')){
				layer.open({
					content: '填写精确的含义可以更加让买家青睐',
					style: 'background-color:#ffa200; color:#fff; border:none;',
					time: 2
				});
				$('#meaning').focus();
				return false;
			}
			else{
				//layer.open({type: 2});
				$.post(add_check,$("#add_domains_pl").serialize(),function(result){
					result = $.parseJSON(result);
					if(result){
						if(result.error == 0){
							$('#domain_list').val('');
							$('#title').val('');
							$('#total_price').val('');
							$('#meaning').val('');
							var errorArr = result.errorArr;
							var strerrorArr = '';
							if(errorArr){
								strerrorArr += "<p>添加失败的域名：</p>";
								for(i in errorArr){
									strerrorArr += i +"[<strong>"+errorArr[i].domain+"</strong>]--<font color=red>"+errorArr[i].msg+"</font><br>";
								}
							}
							layer.open({
								content: '<span color="font_success">'+result.msg+'</span><p>'+strerrorArr+'</p>',
								style: 'background-color:#fff;',
								time: 3,
								btn: ['OK']
							});
						}else{
							$('#domain_list').focus();
							layer.open({
								content: '['+result.msg+']',
								style: 'background-color:#ffa200; color:#fff; border:none;',
								time: 2
							});
						}
					}else{
						layer.open({
							content: '添加出现异常，请重试',
							style: 'background-color:#ffa200; color:#fff; border:none;',
							time: 2
						});
					}
				});
			}
			return false;
		});
	});

</script>

</html>