<html style=" ">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<title>诚介网-我的所有域名</title>
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">

</head>

<body class="didding_details">
<header class="header bidding_header">
	<a href="{pigcms{:U('Seller/index')}"><i></i></a>
	<p>我的所有域名</p>
</header>
<article>
	<nav class="auction_title" style="text-align: center;">
		<form action="{pigcms{:U('Seller/allsell')}"  method="post">
			<p style="padding: .5rem;"><input type="text" class="serach_domains_input" name="search_key" placeholder="输入域名搜索"> <button type="submit" class="serach_domains_button">点我搜索</button></p>
		</form>
	</nav>	
	<nav class="auction_titlemore">
		<ul class="clearfix">
			<li class="active"><a href="{pigcms{:U('Seller/allsell')}"   title="">所有</a></li>
			<li><a href="{pigcms{:U('Seller/allsell_yj',array('type'=>1))}" title="">议价</a></li>
			<li><a href="{pigcms{:U('Seller/allsell_jj',array('type'=>3))}" title="">极速竞价</a></li>
			<li><a href="{pigcms{:U('Seller/allsell_ykj',array('type'=>2))}" title="">一口价</a></li>
			<li><a href="{pigcms{:U('Seller/allsell_yz',array('type'=>4))}" title="">优质</a></li>
		</ul>
	</nav>
	<section>
		<ul class="quote_list">
			<volist name="mydomains" id="vo">
			<li>
				<ul>
					<li><span>域名</span><span><strong>{pigcms{$vo.domain}</strong></span></li>
					<if condition="$vo['gname'] neq ''"><li><span>分组</span><span>{pigcms{$vo.gname.group_name} </span></li></if>
					<li><span>标价</span><span><if condition="$vo['money'] gt 0"><font color="#ff8c00">￥{pigcms{$vo.money|number_format} 元</font><else/>买家报价
							</if></span></li>
					<if condition="$vo['desc'] neq ''"><li><span>简介</span><span>{pigcms{$vo.desc}</span></li></if>
					<li><span>交易类型</span><span><if condition="$vo['type'] eq 0">议价<elseif condition="$vo['type'] eq 1"/>一口价<elseif condition="$vo['type'] eq 2"/>极速竞价<elseif condition="$vo['type'] eq 3"/>优质</if></span></li>
					<if condition="$vo['status'] neq 0">
<!--					<if condition="$vo['type'] eq 0"><li><span>申请操作</span><span><a href="javascript:void();return false;" onclick="setprice({pigcms{$vo['domain_id']},'{pigcms{$vo.domain}','{pigcms{$vo['type']}',{pigcms{$vo['group_id']})"><i>竞价</i></a><a href="javascript:void();return false;"><i>定价</i></a><a href="javascript:void();return false;"><i>优质</i></a></span></li></if>-->
					<if condition="$vo['type'] eq 0">
						<li><span>操作</span><span><a href="javascript:void();return false;" onclick="xdel({pigcms{$vo['domain_id']},'{pigcms{$vo.domain}')"><i>删除</i></a> <a href="javascript:void();" onclick="setprice({pigcms{$vo['domain_id']},'{pigcms{$vo.domain}','{pigcms{$vo['type']}',{pigcms{$vo['group_id']},'{pigcms{$vo.desc}')"><i>设置</i></a></span></li>
					</if>
				    <else />
						<li><span>审核状态</span><span><strong  class="font_warning">待审核</strong></span></li>
				    </if>
				</ul>
			</li>
			</volist>

		</ul>
	</section>
</article>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
</body>

<script>
	function xdel(obj,domain){
		var post_url = '{pigcms{:U('Seller/xdel')}';
		var postdata = {'id': obj}
		layer.open({
			content: '您确定要删除该域名？',
			btn: ['确认', '取消'],
			shadeClose: true,
			yes: function(){
				$.ajax({
					type: "post",
					url:post_url,
					data:postdata,
					dataType:"json",
					success:function(result){
						// 0议价 yj，1一口价 ykj，2竞价jj，3优质yz
						layer.open({content: result.msg +':' + domain, time: 1});
						window.location.reload();
					}
				});
			}
		});
	}

	function setprice(obj,domain,dtype,gid,desc){

		var post_url = '{pigcms{:U('Seller/getglist')}';
		$.ajax({
			type: "post",
			url:post_url,
			dataType:"json",
			success:function(result){
				var resultdata = result;
				var glist ="<select name=glistid>";
				glist += "<option  value=0>无分组</option>";
				for(i in resultdata){
					glist += "<option value="+resultdata[i].group_id+">"+resultdata[i].group_name+"</option>";
				}
				glist += "</select>";
				var pageii = layer.open({
					type: 1,
					content:'<form method="post" action="index.php?g=Wap&c=Seller&a=allsell"><article><section><ul class="shopping_list"><li><input type="hidden" name="domain_id" value="'+obj+'"><input type="hidden" name="type" value="'+dtype+'"><span>域名</span>'+domain + '</li><li><span>域名分组</span>'+glist+'默认无分组</li><li><span>申请操作</span><select name="settype"><option value=0>议价</option><option value=1>展示一口价</option><option value=2>申请竞价</option><option value=3>申请优质</option></select>默认议价（不用申请）</li><li><span>价格</span><input type="text" name="money" placeholder="请输入价格"></li><li><span>简介</span><textarea name="desc" placeholder="请输入简介">'+desc+'</textarea></li></ul><button type="submit" class="purchasing_button">提交</button></section></article></form><button class="purchasing_button" onclick="layer.closeAll();">关闭</button>',
					style: 'position:fixed; left:0; top:0; width:100%; height:100%; border:none;'
				});
			}
		});


	}
</script>

</html>