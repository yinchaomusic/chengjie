<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>添加域名出售</title>
	<else/>
		<title>添加域名出售-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>

<body class=" ">
<header class="header">
	<a href="{pigcms{:U('Seller/index')}"><i></i></a>
	<p>添加域名出售</p>
</header>
<article>
	<section class="terminal_tips">
		<h4>注意事项：</h4>
		<p> 1.请保证所提交的域名归您自己个人所持有，不存在争议和纠纷</p>
		<p> 2. 默认开通 [<strong> 议价 </strong>]</p>
	</section>
	<form action="" method="post" id="adddomains">
		<section>
			<ul class="shopping_list">
				<li><span>出售域名</span>
					<input type="text" name="domain" id="domain" placeholder="请输入出售的域名">
				</li>
				<li><span>期望价格</span>
					<input type="text" name="price"  id="price"  placeholder="请输入期望价格">
				</li>
				<li class="clearfix"><span>选择分组</span>
					<select class="slide" name="group_id">
						<option value="0">不分组</option>
						<volist name="group_list" id="vo">
							<option value="{pigcms{$vo.group_id}">{pigcms{$vo.group_name}</option>
						</volist>
					</select>
				</li>
				<li><span>域名简介</span>

					<textarea name="intro" id="intro" placeholder="请输入域名简介"></textarea>
				</li>

			</ul>
			<button type="submit" class="purchasing_button">提交</button>
		</section>
	</form>
</article>
</body>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>

<script>
	$(function(){

		$('#adddomains').submit(function(){

			var add_check = '{pigcms{:U('Seller/adddomainss_save')}';
			if($('#domain').val()=='' || $('#domain').val()==$('#domain').attr('placeholder')){
				layer.open({
					content: '必须填写要出售的域名',
					style: 'background-color:#ffa200; color:#fff; border:none;',
					time: 2
				});
				$('#domain').focus();
				return false;
			}else{
				//layer.open({type: 2});
				$.post(add_check,$("#adddomains").serialize(),function(result){
					result = $.parseJSON(result);
					if(result){
						if(result.error == 0){
							$('#domain').val('');
							$('#price').val('');
							$('#intro').val('');
							layer.open({
								content: '<span color="font_success">'+result.msg+'</span>',
								style: 'background-color:#fff;',
								time: 2,
								btn: ['OK']
							});
						}else{
							$('#domain'+result.dom_id).focus();
							layer.open({
								content: '['+result.msg+']',
								style: 'background-color:#ffa200; color:#fff; border:none;',
								time: 2
							});
						}
					}else{
						layer.open({
							content: '添加出现异常，请重试',
							style: 'background-color:#ffa200; color:#fff; border:none;',
							time: 2
						});
					}
				});
			}
			return false;
		});
	});
</script>
</html>