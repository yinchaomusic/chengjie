<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>我是卖家</title>
	<else/>
		<title>我是卖家-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
</head>

<body class="didding_details">
<header class="header bidding_header">
	<a href="{pigcms{:U('Profile/index')}"><i></i></a>
	<p>我是卖家</p>
</header>
<article style="padding-bottom:50px;">
	<section>
		<div class="find_title"><a href="{pigcms{:U('Seller/allsell')}"><span>我是卖家</span><span>查看我代售的域名<i></i></span></a> </div>
		<ul class="find_table clearfix">
			<li class="clearfix"><div class="find_img">
					<a href="{pigcms{:U('Seller/allsell')}">
						<img src="{pigcms{$static_path}images/wdsyym.png"></div>
				<div class="find_txt">
					<h3>我的所有域名</h3>
					<p></p>
				</div>
				</a>
			</li>
			<li class="clearfix"><div class="find_img">
					<a href="{pigcms{:U('Seller/adddomainss')}">
						<img src="{pigcms{$static_path}images/tjymcs.png"></div>
				<div class="find_txt">
					<h3>添加域名出售</h3>
					<p></p>
				</div>
				</a>
			</li>
			<li class="clearfix"><div class="find_img">
					<a href="{pigcms{:U('Seller/pldomain')}">
						<img src="{pigcms{$static_path}images/wdpljy.png"></div>
				<div class="find_txt">
					<h3>我的批量交易</h3>
					<p></p>
				</div>
				</a>
			</li>
			<li class="clearfix"><div class="find_img">
					<a href="{pigcms{:U('Seller/adddomains_pl')}">
						<img src="{pigcms{$static_path}images/tjpljy.png"></div>
				<div class="find_txt">
					<h3>添加批量交易</h3>
					<p></p>
				</div>
				</a>
			</li>
			<li class="clearfix"><div class="find_img">
					<a href="{pigcms{:U('Seller/withcheck')}">
						<img src="{pigcms{$static_path}images/dyzym.png"></div>
				<div class="find_txt">
					<h3>待验证的域名</h3>
					<p></p>
				</div>
				</a>
			</li>
			<li class="clearfix"><div class="find_img">
					<a href="{pigcms{:U('Seller/sellnegotiation')}">
						<img src="{pigcms{$static_path}images/wsddbj.png"></div>
				<div class="find_txt">
					<h3>我收到的报价</h3>
					<p></p>
				</div>
				</a>
			</li>
			<li class="clearfix"><div class="find_img">
					<a href="{pigcms{:U('Seller/allsell_jj',array('type'=>3))}">
						<img src="{pigcms{$static_path}images/jsjjgl.png"></div>
				<div class="find_txt">
					<h3>极速竞价管理</h3>
					<p></p>
				</div>
				</a>
			</li>
			<li class="clearfix"><div class="find_img">
					<a href="{pigcms{:U('Seller/allsell_yz',array('type'=>4))}">
						<img src="{pigcms{$static_path}images/yzymgl.png"></div>
				<div class="find_txt">
					<h3>优质域名管理</h3>
					<p></p>
				</div>
				</a>
			</li>
			<li class="clearfix"><div class="find_img">
					<a href="{pigcms{:U('Seller/allsell_ykj',array('type'=>2))}">
						<img src="{pigcms{$static_path}images/ykjgl.png"></div>
				<div class="find_txt">
					<h3>一口价管理</h3>
					<p></p>
				</div>
				</a>
			</li>
			<li class="clearfix"><div class="find_img">
					<a href="{pigcms{:U('Seller/mibiao')}">
						<img src="{pigcms{$static_path}images/wgcdbj.png"></div>
				<div class="find_txt">
					<h3>我的米表</h3>
					<p></p>
				</div>
				</a>
			</li>
			<li class="clearfix"><div class="find_img">
					<a href="{pigcms{:U('Seller/terminal')}">
						<img src="{pigcms{$static_path}images/wdzdym.png"></div>
				<div class="find_txt">
					<h3>我的终端域名</h3>
					<p></p>
				</div>
				</a>
			</li>
			<li class="clearfix"><div class="find_img">
					<a href="{pigcms{:U('Seller/group')}">
						<img src="{pigcms{$static_path}images/ymfzgl.png"></div>
				<div class="find_txt">
					<h3>域名分组管理</h3>
					<p></p>
				</div>
				</a>
			</li>
		</ul>

	</section>
</article>

<include file="Public:footer"/>

<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>

</body>

</html>