<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>待验证的域名列表</title>
	<else/>
		<title>待验证的域名列表-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>

<body class="didding_details">
<header class="header bidding_header">
	<a href="{pigcms{:U('Seller/index')}"><i></i></a>
	<p>待验证的域名列表</p>
</header>
<article>
	<section class="terminal_tips">
		<h4>注意事项：</h4>
		<p>1.添加出售的域名需要经过验证域名whois信息，以确保交易安全。</p>
		<p>2.{pigcms{$now_site_name}会自动验证域名whois邮箱和用户邮箱（注册邮箱及绑定邮箱）是否匹配。</p>
		<p>3.若您的域名使用多个邮箱注册，请先在账户内 绑定这些邮箱 。</p>
		<p>4.若 {pigcms{$now_site_name} 无法获取域名whois信息、或您的域名启用whois隐私保护，请联系客服进行人工验证。</p>
		<p>5.现在共有<strong>{pigcms{$domians_check.total}</strong>个域名待验证</p>
	</section>
	<section>
		<ul class="quote_list">
			<volist name="domians_check['lists']" id="vo">
			<li>
				<ul>
					<li><span>待验域名</span><span>{pigcms{$vo.domain}</span></li>
					<li><span>当前状态</span><rp id="checkstatus_{pigcms{$vo['domain_id']}">未验证</rp></li>
					<li><span>添加日期</span><span>{pigcms{$vo.add_time}</span></li>
					<li><span>操作</span><span><a href="javascript:void();return false;" onclick="now_check({pigcms{$vo['domain_id']})"><i>立即验证</i></a><a href="javascript:void();return false;" onclick="xddel({pigcms{$vo['domain_id']},'{pigcms{$vo.domain}')"><i>删除</i></a></span></li>
				</ul>
			</li>
			</volist>
		</ul>
	</section>
</article>

</body>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>

<script>
	function xddel(obj,domain){
		//layer.open({type: 2});
		var post_url = '{pigcms{:U('Seller/xddel')}';
		var postdata = {'id': obj}
		layer.open({
			content: '您确定要删除【'+domain+'】？',
			btn: ['确认', '取消'],
			shadeClose: true,
			yes: function(){
				$.ajax({
					type: "post",
					url:post_url,
					data:postdata,
					dataType:"json",
					success:function(result){
						layer.open({content: result.msg +' 【' + domain+'】', time: 1});
						window.location.reload();
					}
				});
			}
		});
	}

	function now_check(obj){
		var post_url = '{pigcms{:U('Seller/checkwithdomain')}';

		$('#checkstatus_'+obj).html('<img style="height: 20px;width: 20px;" src="tpl/Domain/default/static/whois/images/loading.gif"/>');
		var postdata = {'id': obj}
				$.ajax({
					type: "post",
					url:post_url,
					data:postdata,
					dataType:"json",
					success:function(result){

					   console.log(result);
					   if(result.error == 0){
						   $('#checkstatus_'+obj).addClass('font_success').text(result.msg);
						}else{
						   $('#checkstatus_'+obj).addClass('font_warning').text(result.msg);
						}
					}
				});


	}
</script>
</html>