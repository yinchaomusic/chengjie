<html style=" ">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<title>诚介网-我收到的报价</title>
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">

</head>

<body class="didding_details">
<header class="header bidding_header">
	<a href="{pigcms{:U('Seller/index')}"><i></i></a>
	<p>我收到的报价</p>
</header>
<article>
	<nav class="auction_title" style="text-align: center;">
		<form action="{pigcms{:U('Seller/sellnegotiation')}"  method="post">
			<p style="padding: .5rem;"><input type="text" class="serach_domains_input" name="search_key" placeholder="输入域名搜索"> <button type="submit" class="serach_domains_button">点我搜索</button></p>
		</form>
	</nav>
	<nav class="auction_titlemore">
		<ul class="clearfix">
			<li <if condition="$type eq 0"> class="active" </if>><a href="{pigcms{:U('Seller/sellnegotiation')}"   title="">所有记录</a></li>
			<li <if condition="$type eq 1"> class="active" </if>><a href="{pigcms{:U('Seller/sellnegotiation',array('type'=>1))}"title="">正在议价</a></li>
			<li <if condition="$type eq 2"> class="active" </if>><a href="{pigcms{:U('Seller/sellnegotiation',array('type'=>2))}" title="">议价成功</a></li>
			<li <if condition="$type eq 3"> class="active" </if>><a href="{pigcms{:U('Seller/sellnegotiation',array('type'=>3))}" title="">议价失败</a></li>
		</ul>
	</nav>
	<section>
		<ul class="quote_list">
			<volist name="quote" id="vo">
				<li>
					<ul>
						<li><span>域名</span><span><strong>{pigcms{$vo.domain}</strong></span></li>
						<li><span>当前状态</span><span><if condition="$vo['status'] eq 0"><rp class="font_default">议价谈判进行中</rp><elseif condition="$vo['status'] eq 1"/><rp class="font_success">谈判成功</rp><elseif condition="$vo['status'] eq 2"/><rp class="font_warning">谈判失败</rp></if></span></li>
						<li><span>最新报价</span><span class="font_info">￥{pigcms{$vo.new_money|number_format}</span></li>
						<li><span>最后报价时间</span><span>{pigcms{$vo.last_time|date='Y-m-d',###}</span></li>
						<li><span>报价过期时间</span><span>{pigcms{$vo.invalid_time|date='Y-m-d',###}</span></li>
						<li><span>操作</span><span>
                                                    <if condition="$vo['last_offer'] eq 0">
                                                        <if condition="$vo['status'] eq 0">
                                                            <a onclick="setprice({pigcms{$vo['quote_id']},'{pigcms{$vo.domain}',2)"><i>出价</i></a><a href="javascript:void();return false;" onclick="nowcancel({pigcms{$vo['quote_id']},'{pigcms{$vo.domain}',3)"><i>取消</i></a><a onclick="accept({pigcms{$vo['quote_id']},1)"><i>接受</i></a>
                                                        <elseif condition="$vo['status'] eq 2"/>
                                                            <i>失败</i>
                                                        </if>
                                                    <elseif condition="$vo['last_offer'] eq 1"/>
                                                        <if condition="$vo['status'] eq 0">
                                                            <a onclick="setprice({pigcms{$vo['quote_id']},'{pigcms{$vo.domain}',2)"><i>出价</i></a><a href="javascript:void();return false;" onclick="nowcancel({pigcms{$vo['quote_id']},'{pigcms{$vo.domain}',3)"><i>取消</i></a>
                                                        <elseif condition="$vo['status'] eq 1"/>
                                                            <i>成功</i>
                                                        <elseif condition="$vo['status'] eq 2"/>
                                                            <i>失败</i>
                                                        </if>
                                                    </if>
                                                        <!--<if condition="$vo['status'] eq 0">
                                                        <elseif condition="$vo['status'] eq 1"/>
                                                        <i>成功</i>
                                                        <elseif condition="$vo['status'] eq 2"/>
                                                        <i>失败</i>
                                                        </if>-->
                                                        <!--<a onclick="setprice({pigcms{$vo['quote_id']},'{pigcms{$vo.domain}',2)"><i>出价</i></a><a href="javascript:void();return false;" onclick="nowcancel({pigcms{$vo['quote_id']},'{pigcms{$vo.domain}',3)"><i>取消</i></a><a onclick="accept({pigcms{$vo['quote_id']},1)"><i>接受</i></a>-->
                                                    </span></li>
					</ul>
				</li>
			</volist>

		</ul>
	</section>
</article>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
</body>

<script>




	var error = '{pigcms{$error}';
	var msg = '{pigcms{$msg}';
	if(error){
		layer.open({
			content:msg,
			btn: ['OK']
		});

	}

	$(document).on("submit","#layerFormDatas",function(){
		if($('#money').val()=='' || $('#money').val()==$('#money').attr('placeholder') ){

			var moneys = $('#money').val();
			var lastmoney = $('#lastmoney').val();
			if(lastmoney >= moneys){
				layer.open({
					content: '新的报价不能低于老的报价',
					btn: ['OK']
				});
			}
			$('#money').focus();
			return false;
		}else{

			var post_check = "{pigcms{:U('Seller/updatenegotiation')}";
			$.post(post_check,$("#layerFormDatas").serialize(),function(result){
				result = $.parseJSON(result);
				if(result){
						if(result.error == 0){
							layer.open({
								content: result.msg,
								time: 5,
								btn: ['OK']
							});
						}else{
							//$('#money').focus();
							layer.open({
								content: result.msg,
								time: 5,
								btn: ['OK']
							});
						}
				}else{
					layer.open({
						content: result.msg,
						time: 5,
						btn: ['OK']
					});
				}
			});

		}

	});

	function setprice(obj,domain,types){

		var post_url = '{pigcms{:U('Seller/get_negotiation')}';
		var postdata = {'id':obj};
		//alert(obj);
		$.ajax({
			type: "post",
			url:post_url,
			data:postdata,
			dataType:"json",
			success:function(result){

				console.log(result);
				if(result.error){
					layer.open({
						content: result.msg,
						time: 3,
						btn: ['OK']
					});
					return false;
				}
				//报价记录
//console.log(result);
				var da = result.resultdata;
				var mylist = '';
				for(i in da){
					if(da[i].is_sell == 1){
						mylist += "<li><strong class=font_success>我：</strong> ￥"+da[i].money+" <span>"+da[i].time+"</span></li>";
					}else{
						mylist += "<li><strong class=font_info>买家：</strong>￥"+da[i].money+" <span>"+da[i].time+"</span></li>";
					}

				}
				var pageii = layer.open({
					type: 1,
					content: '<form method="post" id="layerFormDatas" autocomplete="off"><article><p>&nbsp;</p><p style="padding: .5rem;">给出一个心动价格,更能促成交易<br> <strong>您的挂牌价格：</strong><rp class="font_important">￥ '+result.first_money+' </rp></p><section><ul class="shopping_list"><li><input type="hidden" name="type" value="'+types+'"><input type="hidden" name="id" value="'+obj+'"><span>域名</span>'+domain +'</li><li><span>您的报价</span><input type="number"autocomplete="off" name="money" id="money" placeholder="请输入价格"></li><li class="shopping_list"><span>报价要大于</span><input type="text"name="lastmoney" readonly=readonly value='+da[0].money+'></li></ul><button type="submit"class="purchasing_button">提交</button></section></article></form><button class="purchasing_button" onclick="layer.closeAll();">关闭</button><ul  class="news_ul"><li><strong>报价记录</strong><span> </span></li>'+mylist+'</ul>',
					style: 'position:fixed; left:0; top:0; width:100%; height:100%; border:none;'
				});
			}
		});

	}

	function nowcancel(obj,domain,types){
		layer.open({
			content: '你确认想取消['+domain+']的报价？',
			btn: ['确认', '取消'],
			shadeClose: false,
			yes: function(){
				//layer.open({content: '你点了确认', time: 1});
				var postdata = {'id':obj,'type':types}
				var post_check = "{pigcms{:U('Seller/updatenegotiation')}";
				$.ajax({
					type: "post",
					url:post_check,
					data:postdata,
					dataType:"json",
					success:function(result){
						layer.open({content: result.msg, time: 2});
					}
				});
			}
		});
	}


        
        
        function accept(quote_id,types){
        var postdata = {'id':quote_id,'type':types}
        var post_check = "{pigcms{:U('Seller/updatenegotiation')}";

        $.post(post_check,postdata, function (result) {
                result = $.parseJSON(result);
                if(result){
                    if(result.error == 0){
                        layer.open({
                            content: result.msg,
                            btn: ['OK'],
                            shadeClose: true,
                            yes: function(){
                                window.location.href=location.href;
                            }
                        });
                    }else{
                        layer.open({
                            content: result.msg,
                            btn: ['OK']
                        });
                    }
                }
            });
        }

</script>

</html>