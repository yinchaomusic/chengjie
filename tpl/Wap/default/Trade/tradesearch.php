<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name='apple-touch-fullscreen' content='yes'>
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<title>诚介网-查询结果</title>
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>

<body class="results_content">
<header class="header bidding_header">
	<a href="#" onclick="javascript:history.go(-1);"><i></i></a>
	<p>淘域名</p>
<!--	<span> <em></em></span>-->
</header>
<article>
	<if condition="is_array($tradelist_count)">
		<volist name="tradelist_count" id="vocount">
			<if condition="$vocount['type'] eq 2">
			<section class="bidding">
				<div class="bidding_title">竞价<i>{pigcms{$vocount.counts}</i><span><a  href="{pigcms{:U('Fastbid/index')}">更多</a></span></div>
				<ul class="clearfix">
				<volist name="domains_ja_list" id="vo">
					<li>
						<a href="{pigcms{:U('Fastbid/detail',array('id'=>$vo['domain_id']))}"><h4>{pigcms{$vo.domain}</h4>
							<p><if condition="$vo['desc']">{pigcms{$vo.desc}<else/> &nbsp;&nbsp;</if></p>
							<p><span>{pigcms{$vo.mark_money|number_format}元</span><span>{pigcms{$vo.over_time}</span></p></a>
					</li>
				</volist>
				</ul>
			</section>
			</if>
			 <if condition="$vocount['type'] eq 3" >
				<section class="bidding">
					<div class="bidding_title">优质<i>{pigcms{$vocount.counts}</i><span><a  href="{pigcms{:U('Bargain/index')}">更多</a></span></div>
					<ul class="clearfix">
						<volist name="domains_yz_list" id="vo">
							<li>
								<a href="{pigcms{:U('Bargain/detail',array('id'=>$vo['domain_id']))}"><h4>{pigcms{$vo.domain}</h4>
									<p><if condition="$vo['desc']">{pigcms{$vo.desc}<else/> &nbsp;&nbsp;</if></p>
									<p><span>{pigcms{$vo.mark_money|number_format}元</span><span></span></p></a>
							</li>
						</volist>
					</ul>
				</section>
			</if>
			 <if condition="$vocount['type'] eq 1" >
				<section class="bidding">
					<div class="bidding_title">一口价<i>{pigcms{$vocount.counts}</i><span><a  href="{pigcms{:U('Hotsale/index')}">更多</a></span></div>
					<ul class="clearfix">
						<volist name="domains_yz_list" id="vo">
							<li>
								<a href="{pigcms{:U('Hotsale/selling',array('id'=>$vo['domain_id']))}"><h4>{pigcms{$vo.domain}</h4>
									<p><if condition="$vo['desc']">{pigcms{$vo.desc}<else/> &nbsp;&nbsp;</if></p>
									<p><span>{pigcms{$vo.mark_money|number_format}元</span><span></span></p></a>
							</li>
						</volist>
					</ul>
				</section>
			  </if>
			 <if condition="$vocount['type'] eq 0" >
				<section class="bidding">
					<div class="bidding_title">议价<i>{pigcms{$vocount.counts}</i><span><a  href="{pigcms{:U('Buydomains/index')}">更多</a></span></div>
					<ul class="clearfix">
						<volist name="domains_yz_list" id="vo">
							<li>
								<a href="{pigcms{:U('Buydomains/detail',array('id'=>$vo['domain_id']))}"><h4>{pigcms{$vo.domain}</h4>
									<p><if condition="$vo['desc']">{pigcms{$vo.desc}<else/> &nbsp;&nbsp;</if></p>
									<p><span>{pigcms{$vo.mark_money|number_format}元</span><span></span></p></a>
							</li>
						</volist>
					</ul>
				</section>
			</if>

		</volist>
	 <else/>
		<section style="text-align: center; font-size: larger;color: #a6a6a6;margin-top: 10px;">
			<img src="{pigcms{$static_path}images/ui_26.png" alt=""><br>
			暂无数据
		 </section>
	</if>

</article>
</body>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script>
	var erro_no = {pigcms{$error_no};
	var erro_msg = '{pigcms{$erro_msg}';
	var index_url = "{pigcms{:U('Home/index')}";
	var no_data =  {pigcms{$no_data};
	if(no_data){
		layer.open({
			content: erro_msg,
			btn: ['OK'],
			time: 2,
			shadeClose: true,
			yes: function(){
				//alert(erro_no);
			}
		});

	}
	if(erro_no){
		layer.open({
			content: erro_msg,
			btn: ['OK'],
			shadeClose: true,
			yes: function(){
				//alert(erro_no);
				if(erro_no != 2){
					window.location.href=index_url;
				}
			}
		});
	}

</script>
</html>