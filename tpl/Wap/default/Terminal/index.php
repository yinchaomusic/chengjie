<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name='apple-touch-fullscreen' content='yes'>
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>终端域名</title>
	<else/>
		<title>终端域名-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>

<body class=" ">
<header class="header">
    <a href="{pigcms{:U('Find/index')}"><i></i></a>
    <p>终端域名</p>
</header>
<article>
    <section class="terminal_tips">
        <h3>注意事项：</h3>
        <p> 1.请保证所提交的域名归您自己个人所持有，不存在争议和纠纷</p>
        <p> 2. 一个有吸引力的报价可能能让终端更有兴趣</p>
        <p> 3. 交易手续费为8%，卖方支付</p>
    </section>
    <section>
        <ul class="shopping_list">
            <li><span>域名</span>
                <input type="text" autocomplete="off" class="domainName" name="domain_name" id="domain_name" value="" placeholder="请输入域名">
            </li>
            <li><span>最低价格</span>
                <input type="number" autocomplete="off"  class="min_price" name="min_price" id="min_price" value="" placeholder="请输入最低价格" onkeyup='this.value=this.value.replace(/\D/gi,"")'>
            </li>
            <li><span>最高价格</span>
                <input type="number" autocomplete="off"  class="max_price" name="max_price" id="max_price" value="{pigcms{$info.max_price}"
                       placeholder="请输入最高价格" onkeyup='this.value=this.value.replace(/\D/gi,"")'>
            </li>
            <li><span>希望的终端</span>
                <input type="text" class="ipt_terminal" name="hope" id="hope" value="{pigcms{$info.hope}" placeholder="可不填">
            </li>
            <li><span>留言</span>
                <textarea id="tt_msg" name="tt_msg" placeholder="请输入留言"></textarea>
                <input type="hidden" id="t_id" name="t_id" value="{pigcms{$info.id}" />
            </li>

        </ul>
        <button class="purchasing_button" id="btn_sub">提交域名</button>
    </section>
</article>
</body>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script>
    $("#btn_sub").click(function(){
        var domain_name = $.trim($("#domain_name").val());
        var max_price = $("#max_price").val();
        var min_price = $("#min_price").val();
        var hope = $("#hope").val();
        var tt_msg = $("#tt_msg").val();

        var m = false
        var f = false;
        $(".domainName,.min_price,.max_price").each(function () {
                if ($(this).val().trim() == "") {
//                        $(this).focus();
                        f = true;
                }
                if (max_price) {
                        if (Number(max_price) < Number(min_price)) {
                                m = true;
                        }
                }
        })
        if (f) {
                layer.open({
                    content: '请填写完整',
                    btn: ['OK']
                });
                return false;
        }
        if (m) {
                layer.open({
                    content: '最低价格不能大于期望价格',
                    btn: ['OK']
                });
                return false;
        }


        if(domain_name == ''){
            alert('域名不可以为空');
        }else if(max_price == ''){
            alert('最大值不可以为空');
        }else{
            $.post("{pigcms{:U('Terminal/terminal_add')}", {"domain_name":domain_name,"max_price":max_price,"min_price":min_price,"hope":hope,"tt_msg":tt_msg}, function(result){
                result = $.parseJSON(result);
                if(result){
                    if(result.error == 0){
                            layer.open({
                                content: result.msg,
                                btn: ['OK']
                            });
                            $("#domain_name").val("");
                            $("#max_price").val("");
                            $("#min_price").val("");
                            $("#hope").val("");
                            $("#tt_msg").val("");
                            $("#t_id").val("");
                    }else{
                        layer.open({
                            content: result.msg,
                            btn: ['OK']
                        });
                    }
                }
            })
        }
    })
    
</script>
</html>