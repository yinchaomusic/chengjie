<html style=" ">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<title>诚介网-一口价域名 - pigcms.com</title>
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>

<body class="didding_details">
<header class="header bidding_header">
	<a href="{pigcms{:U('Hotsale/index')}"><i></i></a>
	<p>一口价域名</p>
</header>
<article>
	<section>
		<div class="didding_details_title">
                        <h4>{pigcms{$now_domain.domain}</h4><if condition="$collection_show eq 1"><i class="active" id="click_collection"></i><else/><i class="active_click" id="click_collection"></i></if><em></em>
                        <input type="hidden" value="{pigcms{$user_session.uid}" id="suid"/>
                        <input type="hidden" value="{pigcms{$now_domain.domain_id}" id="domainid" />
                        <input type="hidden" value="{pigcms{$now_domain.domain}" id="domain_name" />
                        <input type="hidden" value="{pigcms{$now_domain.type}" id="domain_type" />
			<p>{pigcms{$now_domain.desc}</p>
		</div>
		<ul class="didding_list mg_top">
			<li><span>一口价</span><span>￥{pigcms{$now_domain.money|number_format}元</span></li>
			<li><span>浏览次数</span><span>{pigcms{$now_domain.hits}</span></li>
		</ul>
		<div class="didding_form clearfix">
                        <input type="hidden" id="domain_id" value="{pigcms{$now_domain.domain_id}"/>
			<button id="buy">购买</button>
		</div>
	</section>
</article>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script>
var collectionUrl = "{pigcms{:U('Bargain/collection')}";
</script>
<script>
	var erro_no = "{pigcms{$error_no}";
        var erro_msg = '{pigcms{$erro_msg}';
	var url = "{pigcms{:U('Hotsale/index')}";
	if(erro_no >0){
            layer.open({
                    content: erro_msg,
                    btn: ['OK'],
                    shadeClose: true,
                    yes: function(){
                            window.location.href=url;
                    }
            });
	}
</script>
<script>
$("#buy").click(function(){
    layer.open({
        content: '你是想确认呢，还是想取消呢？',
        btn: ['确认', '取消'],
        shadeClose: true,
        yes: function(){
//            layer.open({content: '你点了确认', time: 1});
            var domain_id = $("#domain_id").val();
            $.post("{pigcms{:U('Hotsale/buy')}",{"domain_id":domain_id},function(result){
                result = $.parseJSON(result);
                if(result){
                    if(result.error == 0){
                        layer.open({
                            content: result.msg,
                            btn: ['OK'],
                            shadeClose: true,
                            yes: function(){
                                    window.location.href="{pigcms{:U('Hotsale/index')}";
                            }
                        });
                    }else{
                        layer.open({
                            content: result.msg,
                            btn: ['OK']
                        });
                    }
                    
                }
            })
        }, no: function(){
            layer.open({content: '你选择了取消', time: 1});
        }
    });
})
</script>


</body>

</html>