<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name='apple-touch-fullscreen' content='yes'>
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>议价域名</title>
	<else/>
		<title>议价域名-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
	<script>
		var typeid="{pigcms{$Think.get.typeid}";
	</script>
</head>
<body class="results_content">
<header class="header bidding_header">
	<a href="{pigcms{:U('Find/index')}"><i></i></a>
	<p>议价域名</p> 
	<a href="{pigcms{:U('Search/index')}" class="right"><em class="classify_icon"></em></a>
</header>
<article>
	<div class="trade_title">
		<ul class="activity_title clearfix">
			<li id="f_time"><a href="{pigcms{:U('Buydomains/index',array('typeid'=>0))}">按时间排序</a></li>
			<li id="f_count"><a href="{pigcms{:U('Buydomains/index',array('typeid'=>2))}">按长度排序</a></li>
		</ul>
	</div>
	<ul class="trade_list acticity_list">
		<li>
			<section class="bidding">
				<ul class="clearfix mg_top">
					<if condition="is_array($domainList)">
						<volist id="vo" name="domainList">
							<li>
								<a href="{pigcms{:U('Buydomains/detail',array('id'=>$vo['domain_id']))}"><h4>{pigcms{$vo.domain}</h4>
									<p style="color: #ffa200">价格：买家报价</p>
									<p>{pigcms{$vo.desc}&nbsp;</p>
								</a>
							</li>
						</volist>
						<else />
						<section style="text-align: center; font-size: larger;color: #a6a6a6;margin-top: 10px;">
							<img src="{pigcms{$static_path}images/ui_26.png" alt=""><br>
							暂无数据
						</section>
					</if>
				</ul>
			</section>
		</li>
	</ul>
</article>
</body>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script>
	var erro_no = {pigcms{$error_no};
	var erro_msg = '{pigcms{$erro_msg}';
	var index_url = "{pigcms{:U('Home/index')}";
	if(erro_no){
		layer.open({
			content: erro_msg,
			btn: ['OK'],
			shadeClose: true,
			yes: function(){
				window.location.href=index_url;
			}
		});
	}

	$("#f_time").click(function(){
		//alert(0);
	})

</script>
</html>