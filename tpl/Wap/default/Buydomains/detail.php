<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>域名详情</title>
	<else/>
		<title>域名详情-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
	<link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
        <link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>

<body class="didding_details">
<!--aside class="layer_content">
	<div class="layer"></div>
	<div class="shear_list">
		<ul class="clearfix">
			<li>
				<img src="{pigcms{$static_path}images/UI2-12_03_03.png" alt="">
				<p>whois</p>
			</li>
			<li>
				<img src="{pigcms{$static_path}images/UI2-12_03_06.png" alt="">
				<p>发微信好友</p>
			</li>
			<li>
				<img src="{pigcms{$static_path}images/UI2-12_03_08.png" alt="">
				<p>分享到朋友圈</p>
			</li>
			<li>
				<img src="{pigcms{$static_path}images/UI2-12_03_13.png" alt="">
				<p>分享到微博</p>
			</li>
			<li>
				<img src="{pigcms{$static_path}images/UI2-12_03_15.png" alt="">
				<p>返回首页</p>
			</li>
		</ul>
		<button type="txt" class="cancel">取消</button>
	</div>
</aside-->
<header class="header bidding_header">
	<a href="{pigcms{:U('Buydomains/index')}"><i></i></a>
	<p>域名详情</p> <em class="shear"></em>
</header>
<article>
	<section>
		<div class="didding_details_title">
			<h4>{pigcms{$now_domain.domain}</h4><if condition="$collection_show eq 1"><i class="active" id="click_collection"></i><else/><i class="active_click" id="click_collection"></i></if><em></em>
                        <input type="hidden" value="{pigcms{$user_session.uid}" id="suid"/>
                        <input type="hidden" value="{pigcms{$now_domain.domain_id}" id="domainid" />
                        <input type="hidden" value="{pigcms{$now_domain.domain}" id="domain_name" />
                        <input type="hidden" value="{pigcms{$now_domain.type}" id="domain_type" />
			<p>{pigcms{$now_domain.desc}</p>
		</div>
		<ul class="didding_list mg_top">
<!--			<li><span>注册商</span><span>诚介网</span></li>-->
			<li><span>卖家</span><span>{pigcms{$now_domain.uid}</span></li>
			<li><span>浏览次数</span><span>{pigcms{$now_domain.hits}</span></li>
		</ul>
		<div class="dding_info"><i></i><span>注意：询价出价代表您同意以该价格购买域名，卖家同意出价
后，如果拒绝购买需要支付50元违约金。</span></div>
		<div class="didding_form clearfix">
			<div class="didding_input">
				<input type="text"class="u_prise" value="" placeholder="请输入价格"> &nbsp;元</div>
                                <input type="hidden"  id="domainid" value="{pigcms{$now_domain.domain_id}"/>
			<button id="baojia">立即出价</button>
		</div>
	</section>
</article>

<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script>
var collectionUrl = "{pigcms{:U('Bargain/collection')}";
</script>
<script>
	var erro_no = "{pigcms{$error_no}";
        var erro_msg = '{pigcms{$erro_msg}';
	var index_url = "{pigcms{:U('Home/index')}";
	if(erro_no){
		layer.open({
			content: erro_msg,
			btn: ['OK'],
			shadeClose: true,
			yes: function(){
				window.location.href=index_url;
			}
		});
	}
</script>
<script>
    //报价
    $("#baojia").click(function () {
        var num = $(".u_prise").val();

        if (num == "" || num == "0") {
            layer.open({
                    content: "请输入您的报价",
                    btn: ['OK'],
            });
            return false;
        }
        $.post("{pigcms{:U('Buydomains/quote')}",{"id":$("#domainid").val(),"money":num}, function (result) {
            result = $.parseJSON(result);
                if(result){
                    if(result.error == 0){
                        layer.open({
                            content: result.msg,
                            btn: ['OK'],
                            shadeClose: true,
                            yes: function(){
                                    window.location.href="{pigcms{:U('Buydomains/index')}";
                            }
                        });
                    }else{
                        layer.open({
                            content: result.msg,
                            btn: ['OK']
                        });
                    }
                    
                }
        });
    });
</script>
</body>

</html>