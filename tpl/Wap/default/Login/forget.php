<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name='apple-touch-fullscreen' content='yes'>
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
    <title>诚介网-忘记密码</title>
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css">
</head>

<body class="login_content register_content">
<header class="header">
    <a href="{pigcms{:U('Login/login')}"><i></i></a>
    <p>忘记密码</p>

</header>
<article>
    <section class="login_form">

        <ul>
            <li class=" clearfix"><i></i><div class="login_input"><input type="text" name='phone' value="" id="phone" placeholder="请输入邮箱或手机号"></div></li>
            <li class=" clearfix"><i></i><div class="login_input"><input type="password" name="forget_pwd" value="" id="forget_pwd" placeholder="请输入密码"><span onclick="getCode();return false;">发送验证码</span></div></li>
            <li class=" clearfix"><i></i><div class="login_input"><input  type="text" name="forget_verify" value="" id="forget_verify"  placeholder="请输入验证码"> </div></li>
<!--            <li class=" clearfix"><i></i><div class="login_input"><input placeholder="请输入再次验证码"> </div></li>-->
        </ul>
        <button id="reg_from">提交</button>
    </section>

</article>

</body>

<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script>
        var get_code_url = "{pigcms{:U('Login/sendCode')}";
        var forget_check_url = "{pigcms{:U('Login/forget_check')}";

        function getCode(obj){
                var check_phone = $("#phone").val();
                        $.post(get_code_url,{"phone":check_phone},function(result){
                                result = $.parseJSON(result);
                                if(result){
                                        if(result.error == 0){
                                                layer.open({
                                                    content: result.msg,
                                                    btn: ['OK']
                                                });
//                                                location.href="/index.php?g=Wap&c=Login&a=regcheck";
                                        }else{
                                                $('#'+result.dom_id).focus();
                                                layer.open({
                                                    content: result.msg,
                                                    btn: ['OK']
                                                });
                                        }
                                }else{
//                                        swal('','获取验证码出现异常，请重试！','error');
                                        layer.open({
                                            content: result.msg,
                                            btn: ['OK']
                                        });
                                }
                        });
                //}
        }
        
$("#reg_from").click(function(){
    var phone = $("#phone").val();
    var forget_verify = $("#forget_verify").val();
    var forget_pwd = $("#forget_pwd").val();
    if(phone==''){
            layer.open({
                content: '请输入您的账户',
                btn: ['OK']
            });
//            $('#phone').focus();
            return false;
    }else if(forget_verify==''){
            layer.open({
                content: '请输入验证码',
                btn: ['OK']
            });
//            $('#forget_verify').focus();
            return false;
    }else if(forget_pwd==''){
            layer.open({
                content: '请输入密码',
                btn: ['OK']
            });
//            $('#forget_pwd').focus();
            return false;
    }else{
        $.post(forget_check_url,{"phone":phone,"extra":forget_verify,"forget_pwd":forget_pwd},function(result){
//            alert(result);die;
            result = $.parseJSON(result);
            if(result){
                if(result.error == 0){
                        layer.open({
                            content: '修改成功，请登录',
                            btn: ['OK'],
                            shadeClose: true,
                            yes: function(){
                                window.parent.location = "{pigcms{:U('Login/login')}";
                            }
                        });
                    
                }else{
                    layer.open({
                        content: result.msg,
                        btn: ['OK']
                    });
//                    $('#login_'+result.dom_id).focus();
//                    location.href=location.href;
                }
            }else{
                layer.open({
                    content: '修改密码出现异常，请重试！',
                    btn: ['OK']
                });
            }
        })
    }
})
</script>

</html>