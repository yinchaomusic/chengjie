<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name='apple-touch-fullscreen' content='yes'>
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>微信绑定</title>
	<else/>
		<title>weixin-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css?t={pigcms{$_SERVER.REQUEST_TIME}">
</head>
<body class="login_content">
	<header class="header">
		<a href="{pigcms{:U('Home/index')}"><i></i></a>
		<p>绑定已有账号</p>
		<a href="{pigcms{:U('Login/register')}" class="right yellow">注册</a>
	</header>
    <article>
        <section class="login_form">
			<ul>
				<li class=" clearfix"><i></i><div class="login_input"><input id="login_account" name="login_account" type="text" value="" placeholder="请输入手机号"></div></li>
				<li class=" clearfix"><i></i><div class="login_input"><input id="login_pwd" name="login_pwd" type="password" value="" placeholder="请输入密码"></div></li>
			
			</ul>
			<button id="login_form">绑定微信</button>
			<p style="margin-top:25px;font-size:.8rem;"><!-- a href="{pigcms{:U('Login/weixin')}">绑定账号</a--> &nbsp;&nbsp;&nbsp; <a  id="weixin_nobind" >不想绑定账号,跳过 →</a>&nbsp;&nbsp;&nbsp; <a href="{pigcms{:U('Login/forget')}">忘记密码</a></p>
		</section>

	</article>
<!--	<footer class="login_footer">
		<p><span>第三方账号登陆</span></p>
		<div class="login_img"><a href="#"><img src="{pigcms{$static_path}images/ui_50.png"></a></div>
	</footer>-->
</body>


<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/rem.js?t={pigcms{$_SERVER.REQUEST_TIME}"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script>

$('#weixin_nobind').click(function(){
			layer.open({
				title:['提醒：','background-color:#ffa200;color:#fff;'],
				content:'直接将微信号作为用户登录，以后将无法绑定已有帐号！请确认：',
				btn: ['确认', '取消'],
				shadeClose: false,
				yes: function(){
					layer.open({content: '你点了确认，正在跳转！', time:3});
					window.location.href = "{pigcms{:U('Login/weixin_nobind')}";
				}
			});
			return false;
	});
	
    $("#login_form").click(function(){
        if($('#login_account').val()==''){
                layer.open({
                    content: '请输入您的手机号',
                    btn: ['OK']
                });
//                $('#login_account').focus();
                return false;
        }else if($('#login_pwd').val()==''){
                layer.open({
                    content: '请输入密码',
                    btn: ['OK']
                });
                $('#login_pwd').focus();
                return false;
        }else{
                $.post("{pigcms{:U('Login/weixin_bind')}",{"phone":$('#login_account').val(),"password":$('#login_pwd').val()},function(result){
//                    alert(result);
                        result = $.parseJSON(result);
                        if(result){
                                if(result.error == 0){
                                        layer.open({
                                            content: '绑定成功',
                                            btn: ['OK'],
                                            shadeClose: true,
                                            yes: function(){
                                                window.parent.location = "{pigcms{:U('Profile/index')}";
                                            }
                                        });
                                }else{
                                    layer.open({
                                            content: result.msg,
                                            btn: ['OK'],
                                            shadeClose: true,
                                            yes: function(){
                                                location.href=location.href;
                                            }
                                        });
                                        
                                }
                        }else{
                                layer.open({
                                            content: '绑定微信出现异常，请重试！',
                                            btn: ['OK']
                                        });
                        }
                });
        }
    })


</script>
</html>