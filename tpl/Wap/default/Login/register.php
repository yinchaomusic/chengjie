<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
 
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name='apple-touch-fullscreen' content='yes'>
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
	<meta name="format-detection" name="email=no" />
	<if condition="$is_wexin_browser">
		<title>注册</title>
	<else/>
		<title>注册-{pigcms{$config.site_name}</title>
	</if>
	<meta name="renderer" content="webkit" />
    <!-- 禁止百度转码 -->
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!-- UC强制竖屏 -->
    <meta name="screen-orientation" content="portrait" />
    <!-- QQ强制竖屏 -->
    <meta name="x5-orientation" content="portrait" />
    <!-- UC强制全屏 -->
    <meta name="full-scerrn" content="yes" />
    <!-- QQ强制全屏 -->
    <meta name="x5-fullscreen" content="ture" />
    <!-- QQ应用模式 -->
    <meta name="x5-page-mode" content="app" />
    <!-- UC应用模式 -->
    <meta name="browsermode" content="application">
    <!-- window phone 点亮无高光 -->
    <meta name="msapplication-tap-highlight" content="no" />
    <link rel="stylesheet" href="{pigcms{$static_path}css/base.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/index.css?t={pigcms{$_SERVER.REQUEST_TIME}">
    <link rel="stylesheet" type="text/css" href="{pigcms{$static_public}js/layer.mobile-v1.7/need/layer.css?t={pigcms{$_SERVER.REQUEST_TIME}">
</head>

<body class="login_content register_content">
    <header class="header">
        <a href="#" onclick="window.history.go(-1);"><i></i></a>
        <p>注册</p>
		<!--a href="{pigcms{:U('Login/weixin')}" class="right yellow" style="font-size: .75rem;">微信注册</a-->
    </header>
    <article>
        <section class="login_form">
       
                <ul>
                    <li class=" clearfix"><i></i><div class="login_input">
                            <input type="text" name='phone' value="" id="phone" placeholder="请输入手机号">
                        </div></li>
                    <li class=" clearfix">
						<i></i>
						<div class="login_input"><input type="password" name="reg_pwd" value="" id="reg_pwd" placeholder="请输入密码"></div>
						
					</li>
                    <li class=" clearfix">
						<i></i>
						<div class="login_input"><input type="text" name="reg_verify" value="" id="reg_verify" placeholder="请输入手机验证码"><span  onclick="getCode();return false;">发送验证码</span></div>
					</li>
                </ul>
				<button id="reg_from">注册</button>
<!--		 <p class="register_p"><i></i><span>我已阅读并同意《小猪CMS服务协议》</span></p>-->
	 <p style="margin-top:25px;font-size:1rem;"><a href="{pigcms{:U('Login/weixin_back')}">我已经有账号,绑定账号</a> &nbsp;&nbsp;&nbsp; <!--a href="{pigcms{:U('Login/weixin_nobind')}" id="weixin_nobind" >不想绑定账号,跳过 →</a-->&nbsp;&nbsp;&nbsp; <a href="{pigcms{:U('Login/forget')}">忘记密码</a></p>
		</section>

	</article>
 
</body>

<script src="{pigcms{$static_path}js/rem.js"></script>
<script src="{pigcms{$static_path}js/index.js"></script>
<script src="//apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{pigcms{$static_public}js/layer.mobile-v1.7/layer.js"></script>
<script>
$('#weixin_nobind').click(function(){
			layer.open({
				title:['提醒：','background-color:#ffa200;color:#fff;'],
				content:'直接将微信号作为用户登录，以后将无法绑定已有帐号！请确认：',
				btn: ['确认', '取消'],
				shadeClose: false,
				yes: function(){
					layer.open({content: '你点了确认，正在跳转！', time:3});
					window.location.href = "{pigcms{:U('Login/weixin_nobind')}";
				}
			});
			return false;
	});
	
	
        var get_code_url = "{pigcms{:U('Login/sendCode')}";
        var reg_url = "{pigcms{:U('Login/reg_check')}";

        function getCode(obj){
                var check_phone = $("#phone").val();
                        $.post(get_code_url,{"phone":check_phone},function(result){
                                result = $.parseJSON(result);
                                if(result){
                                        if(result.error == 0){
                                                layer.open({
                                                    content: result.msg,
                                                    btn: ['OK']
                                                });
//                                                location.href="/index.php?g=Wap&c=Login&a=regcheck";
                                        }else{
                                                $('#'+result.dom_id).focus();
                                                layer.open({
                                                    content: result.msg,
                                                    btn: ['OK']
                                                });
                                        }
                                }else{
//                                        swal('','获取验证码出现异常，请重试！','error');
                                        layer.open({
                                            content: result.msg,
                                            btn: ['OK']
                                        });
                                }
                        });
                //}
        }
        
$("#reg_from").click(function(){
    var phone = $("#phone").val();
    var reg_verify = $("#reg_verify").val();
    var reg_pwd = $("#reg_pwd").val();
    
    if(phone==''){
            layer.open({
                content: '请输入您的账户',
                btn: ['OK']
            });
            $('#phone').focus();
            return false;
    }else if(reg_verify==''){
            layer.open({
                content: '请输入验证码',
                btn: ['OK']
            });
            $('#reg_verify').focus();
            return false;
    }else if(reg_pwd==''){
            layer.open({
                content: '请输入密码',
                btn: ['OK']
            });
            $('#reg_pwd').focus();
            return false;
    }else{
        $.post(reg_url,{"phone":phone,"extra":reg_verify,"reg_pwd":reg_pwd},function(result){
            result = $.parseJSON(result);
            if(result){
                if(result.error == 0){
                        layer.open({
                            content: '注册成功，请登录',
                            btn: ['OK']
                        });
                    window.parent.location = "{pigcms{:U('Login/login')}";
                }else{
                    layer.open({
                        content: result.msg,
                        btn: ['OK']
                    });
                    $('#login_'+result.dom_id).focus();
//                    location.href=location.href;
                }
            }else{
                layer.open({
                    content: '注册出现异常，请重试！',
                    btn: ['OK']
                });
            }
        })
    }
})
</script>
</html>
