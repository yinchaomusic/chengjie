<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh_CN" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8">
	<title>首页 | {pigcms{$config.rwseo_title}</title>

	<include file="Public:header_css"  />
	<link href="{pigcms{$static_path}css/todo.min.css" rel="stylesheet" type="text/css" />
	<link href="{pigcms{$static_path}assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
<!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
<body>
<!-- BEGIN HEADER -->
<div class="page-header">
	<!-- BEGIN HEADER TOP -->
	<include file="Public:header_top"/>
	<!-- END HEADER TOP -->
	<!-- BEGIN HEADER MENU -->
	<include file="Public:header_menu"/>
	<!-- END HEADER MENU -->
</div>
<!-- END HEADER -->

<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">


	<!-- BEGIN PAGE CONTAINER -->
	<div class="page-container">
		<!-- BEGIN PAGE HEAD -->

		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE CONTENT -->
		<div class="page-content">
			<div class="container">
				<!-- BEGIN PAGE BREADCRUMB -->

				<div class="row">
					<div class="col-md-8"><!-- 左侧 -->

						<div class="row">
							<div class="col-md-12">
								<div class="portlet light portlet-fit ">
									<div class="portlet-title">
										<div class="caption">
											<i class=" icon-layers font-green"></i>
											<span class="caption-subject font-green bold uppercase">{pigcms{$taskinfo.title}</span>
											<span class="label label-danger"> ￥{pigcms{$taskinfo.pays}</span>
										</div>
									</div>
									<div class="portlet-body">
										<div class="mt-element-step">
											<div class="row step-line">
												<div class="mt-step-desc">
													<div class="font-dark bold uppercase">
														<span class="caption-subject samll  uppercase"> 编号：<span class="label label-default">#{pigcms{$taskinfo.task_id}</span></span>
														<span class="caption-helper">  发布时间： {pigcms{$taskinfo.pub_time|date='Y-m-d H:i:s',###} </span>
													发信息：
														<if condition="$is_seller">
														<a class=" " data-target="#stack_{pigcms{$taskinfo.task_id}" data-toggle="modal">@{pigcms{$taskinfo.chengjieName}</a>

														<else/>
															<a class=" " data-target="#stack_{pigcms{$taskinfo.task_id}" data-toggle="modal">@{pigcms{$taskinfo.nickname}</a>
														</if>

													</div>
													<div class="caption-desc font-grey-cascade"></div>
												</div>



												<div class="col-md-2 mt-step-col first done">
													<div class="mt-step-number bg-white"><i class="fa fa-check"></i></div>
													<div class="mt-step-title uppercase font-grey-cascade"><h5>已承接</h5></div>

												</div>
												<div class="col-md-2 mt-step-col <if condition="$taskinfo['process_status'] egt 2"> done </if> ">
												<div class="mt-step-number bg-white"><if condition="$taskinfo['process_status'] egt 2"><i class="fa fa-check"></i> <else/>2</if></div>
												<div class="mt-step-title uppercase font-grey-cascade"><h5>
														<if condition="$is_seller">协商工期<else/> 提交工期</if>
													</h5></div>

											</div>
											<div class="col-md-2 mt-step-col <if condition="$taskinfo['process_status'] egt 3"> done </if>">
											<div class="mt-step-number bg-white"><if condition="$taskinfo['process_status'] egt 3"><i class="fa fa-check"></i> <else/>3</if></div>
											<div class="mt-step-title uppercase font-grey-cascade"><h5>
													<if condition="$is_seller">对方工作中 <else/> 工作中</if>
												</h5></div>

										</div>
										<div class="col-md-2 mt-step-col <if condition="$taskinfo['process_status'] egt 4"> done </if>">
										<div class="mt-step-number bg-white"><if condition="$taskinfo['process_status'] egt 4"><i class="fa fa-check"></i> <else/>4</if></div>
										<div class="mt-step-title uppercase font-grey-cascade"><h5>
												<if condition="$is_seller">测试期 <else/> 交付中 </if></h5></div>

									</div>
									<div class="col-md-2 mt-step-col <if condition="$taskinfo['process_status'] egt 5"> done </if>">
									<div class="mt-step-number bg-white"><if condition="$taskinfo['process_status'] egt 5"><i class="fa fa-check"></i> <else/>5 </if></div>
									<div class="mt-step-title uppercase font-grey-cascade"><h5>
											<if condition="$is_seller">确认验收 <else/> 交易成功</if></h5></div>

								</div>
								<div class="col-md-2 mt-step-col last <if condition="$taskinfo['process_status'] eq 6"> done </if>">
								<div class="mt-step-number bg-white"><if condition="$taskinfo['process_status'] eq 6"><i class="fa fa-check"></i> <else/>6 </if></div>
								<div class="mt-step-title uppercase font-grey-cascade"><h5>结束</h5></div>

							</div>
							<!--												<i class="fa-li fa fa-spinner fa-spin"></i>-->

							<div class="mt-step-desc">

								<div class="alert alert-danger display-hide">
									<button class="close" data-close="alert"></button><i class="fa fa-close  fa-spin"></i>表单有误，请检查</div>
								<div class="alert alert-success display-hide">
									<button class="close" data-close="alert"></button> <i class="fa fa-circle-o-notch fa-spin"></i>表单验证通过，正在提交中...
								</div>


								<if condition="($taskinfo['process_status'] eq 2)  AND ($is_buyer eq 1)">


									<div class="portlet light ">
										<div class="portlet-title">
											<div class="caption">
												<i class="icon-social-dribbble font-red"></i>
												<span class="caption-subject font-red bold uppercase">进程状态</span>
											</div>

										</div>
										<div class="portlet-body">
											<p class="text-left"> {pigcms{$taskinfo['title']}<br>
												分类：{pigcms{$taskinfo.cate_name}
												<br>

											</p>
											<p class="text-center"><font style="font-size: x-large; color: rgba(58, 127, 230, 0.96);"> <i class="fa fa-spinner fa-spin"></i>正在等待对方确认信息，请耐心等待！ </font></p>
											<p class="text-right"> 下单时间：{pigcms{$taskinfo.buy_time|date='Y-m-d H:i:s',###} </p>
										</div>

										<style>
											.blog-single-img >img{width: 100%;  }
										</style>

										<div class="portlet-body">
											<h4>您的备注：</h4>
											{pigcms{$taskinfo.buydesc}
											<if condition="$taskinfo['meimg']">
												<h4>附件信息：</h4>
												<div class="blog-single-img">
													<img src="{pigcms{$taskinfo.meimg}" alt="">
												</div>

											</if>

										</div>


									</div>

									<elseif condition="($taskinfo['process_status'] eq 2)  AND ($is_seller eq 1)"/>


									<form action="{pigcms{:U('TaskList/ordered_save_setp2')}" role="form" method="POST" id="sellerForm_setp2">

										<input type="hidden" name="task_id" id="task_id" value="{pigcms{$taskinfo.task_id}">
										<input type="hidden" name="optype" id="optype" value="seller_setp2">
										<input type="hidden" name="orderid" id="orderid" value="{pigcms{$taskinfo.orderid}">


										<div class="form-group">
											<label>您的备注(买家可以看到)</label>
											<textarea class="form-control" name="selldesc" rows="3"></textarea>
										</div>


										<br>


										<div class="form-group">
											<div class="col-md-offset-4 col-md-8">
												<button type="button"    class="btn green accept_order">确认接受</button>
												&nbsp;
												<button type="button"   class="btn green  deny_order">拒绝</button>
											</div>
										</div>

									</form>
									<div class="portlet light ">
										<div class="portlet-title">
											<div class="caption">
												<i class="icon-social-dribbble font-red"></i>
												<span class="caption-subject font-red bold uppercase">进程状态</span>
											</div>

										</div>
										<div class="portlet-body">
											<p class="text-left"> {pigcms{$taskinfo['title']}<br>
												分类：{pigcms{$taskinfo.cate_name}
												<br>

											</p>
											<p class="text-center"><font style="font-size: x-large; color: rgba(58, 127, 230, 0.96);"> <i class="fa fa-spinner fa-spin"></i>正在等待您确认信息，请选择你的操作！ </font></p>
											<p class="text-right"> 下单时间：{pigcms{$taskinfo.buy_time|date='Y-m-d H:i:s',###}
												<br>
												购买价格：<span class="label label-danger">￥{pigcms{$taskinfo.price}元 </span>
											</p>
										</div>

										<style>
											.blog-single-img >img{width: 100%;  }
										</style>

										<div class="portlet-body">
											<h4>买家的备注：</h4>
											{pigcms{$taskinfo.buydesc}
											<if condition="$taskinfo['meimg']">
												<h4>附件信息：</h4>
												<div class="blog-single-img">
													<img src="{pigcms{$taskinfo.meimg}" alt="">
												</div>

											</if>

										</div>


									</div>


									<elseif condition="($taskinfo['process_status'] eq 3)  AND ($is_buyer eq 1)"/>

									<form action="{pigcms{:U('TaskList/ordered_save_setp3')}" id="Buyer_set3_Form" role="form"
									      method="POST">

										<input type="hidden" name="task_id" id="task_id" value="{pigcms{$taskinfo.task_id}">
										<input type="hidden" name="optype" id="optype" value="buyer_setp3">


										<div class="portlet light ">
											<div class="portlet-title">
												<div class="caption">
													<i class="icon-social-dribbble font-red"></i>
													<span class="caption-subject font-red bold uppercase">进程状态</span>
												</div>

											</div>
											<div class="portlet-body">
												<p class="text-left"> {pigcms{$taskinfo['title']}<br>
													分类：{pigcms{$taskinfo.cate_name}
													<br>

												</p>
												<p class="text-center"><font style="font-size: x-large; color: rgba(58, 127, 230, 0.96);"> <i
															class="fa fa-check-circle"></i>对方接受您提交的工期，请您及时完成工作！</font></p>
												<p class="text-right"> 下单时间：{pigcms{$taskinfo.buy_time|date='Y-m-d H:i:s',###} </p>
											</div>


											<div class="table-responsive">
												<table class="table table-hover">
													<thead>
													<tr>
														<th>编号</th>
														<!--																		<th>您需要的服务</th>-->
														<th></th>
														<th>类型</th>
														<th>支付金额</th>
													</tr>
													</thead>
													<tbody>
													<tr>
														<td>#{pigcms{$taskinfo.orderid}</td>
														<td> </td>
														<td>购买</td>
														<td><span class="money"><i class="fa fa-cny"></i>{pigcms{$taskinfo.price}</span></td>
													</tr>
													</tbody>
													<tfoot>
													<tr class="warning">
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td class="text-right">合计</td>
														<td><strong class="money"><i class="fa fa-cny"></i>{pigcms{$taskinfo.price}</strong></td>
													</tr>
													<tr>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td class="text-right">账户余额</td>
														<td><strong class="money"><i class="fa fa-cny"></i>{pigcms{$user_session.now_money}</strong>
														</td>
													</tr>
													</tfoot>
												</table>
											</div>

											<div class="text-center">
												<a class="btn btn-primary" href="javascript:void(0);" id="buyerPayNow">立即支付订金</a>
												&nbsp;
												<a class="btn btn-default" href="{pigcms{:U('Task/my_service')}" id="buyerPayLater">稍后支付订金</a>
											</div>

											<div class="portlet-body">


												<if condition="$taskinfo['selldesc']">
													<h4>对方附言：</h4>
													{pigcms{$taskinfo.selldesc}
												</if>

											</div>
										</div>
									</form>

									<elseif condition="($taskinfo['process_status'] eq 3)  AND ($is_seller eq 1)"/>

									<div class="portlet light ">
										<div class="portlet-title">
											<div class="caption">
												<i class="icon-social-dribbble font-red"></i>
												<span class="caption-subject font-red bold uppercase">进程状态</span>
											</div>

										</div>
										<div class="portlet-body">
											<p class="text-left"> {pigcms{$taskinfo['title']}<br>
												分类：{pigcms{$taskinfo.cate_name}
												<br>

											</p>
											<p class="text-center"><font style="font-size: x-large; color: rgba(58, 127, 230, 0.96);"> <i class="fa fa-spinner fa-spin"></i>正在等待对方支付赏金，请耐心等待！ </font></p>
											<p class="text-right"> 下单时间：{pigcms{$taskinfo.buy_time|date='Y-m-d H:i:s',###} </p>
										</div>

										<style>
											.blog-single-img >img{width: 100%;  }
										</style>

										<div class="portlet-body">
											<h4>买家备注：</h4>
											{pigcms{$taskinfo.buydesc}
											<if condition="$taskinfo['meimg']">
												<h4>附件信息：</h4>
												<div class="blog-single-img">
													<img src="{pigcms{$taskinfo.meimg}" alt="">
												</div>

											</if>

										</div>


									</div>
									<elseif condition="($taskinfo['process_status'] eq 4)  AND ($is_buyer eq 1)"/>
									<div class="portlet light ">
										<div class="portlet-title">
											<div class="caption">
												<i class="icon-social-dribbble font-red"></i>
												<span class="caption-subject font-red bold uppercase">进程状态</span>
											</div>

										</div>
										<div class="portlet-body">
											<p class="text-left"> {pigcms{$taskinfo['title']}<br>
												分类：{pigcms{$taskinfo.cate_name}
												<br>

											</p>
											<p class="text-center"><font style="font-size: x-large; color: rgba(58, 127, 230, 0.96);"> <i class="fa fa-spinner fa-spin"></i>您已付款成功，等待对方工作完成！ </font></p>
											<p class="text-right"> 下单时间：{pigcms{$taskinfo.buy_time|date='Y-m-d H:i:s',###} </p>
										</div>

										<div class="text-center">


											<button class="btn yellow-mint uppercase" data-taskgoods-goodid="{pigcms{$taskinfo.task_id}"  data-taskgoods-orderid="{pigcms{$taskinfo.orderid}"  data-taskgoods-optype="buyer" data-taskgoods-type="3" data-title="您是否需要申请仲裁？" data-btn-ok-label="确认申请" data-btn-cancel-label="取消"
											        data-toggle="confirmation" id="confirmation_zhongcai"><i class="fa fa-gavel"></i> 申请仲裁
											</button>
										</div>

										<style>
											.blog-single-img >img{width: 100%;  }
										</style>

										<div class="portlet-body">
											<h4>您的备注：</h4>
											{pigcms{$taskinfo.buydesc}
											<if condition="$taskinfo['meimg']">
												<h4>附件信息：</h4>
												<div class="blog-single-img">
													<img src="{pigcms{$taskinfo.meimg}" alt="">
												</div>

											</if>

											<h4>卖家备注：</h4>
											{pigcms{$taskinfo.selldesc}
										</div>


									</div>
									<elseif condition="($taskinfo['process_status'] eq 4)  AND ($is_seller eq 1)"/>
									<form action="{pigcms{:U('TaskList/ordered_save_setp4')}" id="Seller_setp4_Form" role="form" method="POST">

										<input type="hidden" name="task_id" id="task_id" value="{pigcms{$taskinfo.task_id}">
										<input type="hidden" name="optype" id="optype" value="buyer_setp4">
										<input type="hidden" name="orderid" id="orderid" value="{pigcms{$taskinfo.orderid}">

										<div class="portlet light ">
											<div class="portlet-title">
												<div class="caption">
													<i class="icon-social-dribbble font-red"></i>
													<span class="caption-subject font-red bold uppercase">进程状态</span>
												</div>

											</div>
											<div class="portlet-body">
												<p class="text-left"> {pigcms{$taskinfo['title']}<br>
													分类：{pigcms{$taskinfo.cate_name}
													<br>

												</p>
												<p class="text-center"><font style="font-size: x-large; color: rgba(58, 127, 230, 0.96);"> <i class="fa fa-spinner fa-spin"></i>对方已付款成功，等待您的工作完成！ </font></p>
												<p class="text-right"> 下单时间：{pigcms{$taskinfo.buy_time|date='Y-m-d H:i:s',###} </p>
											</div>

											<div class="text-center">
												<button type="button" id="seller_ok_btn" class="btn blue btn-default">
													<i class="fa fa-paper-plane"></i> 提交，我已经完成
												</button>
											</div>
									</form>
									<style>
										.blog-single-img >img{width: 100%;  }
									</style>

									<div class="portlet-body">
										<h4>买家备注：</h4>
										{pigcms{$taskinfo.buydesc}
										<if condition="$taskinfo['meimg']">
											<h4>附件信息：</h4>
											<div class="blog-single-img">
												<img src="{pigcms{$taskinfo.meimg}" alt="">
											</div>

										</if>

										<h4>您的备注：</h4>
										{pigcms{$taskinfo.selldesc}
									</div>
							</div>

							<elseif condition="($taskinfo['process_status'] eq 5)  AND ($is_seller eq 1)"/>

							<div class="portlet light ">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-social-dribbble font-red"></i>
										<span class="caption-subject font-red bold uppercase">进程状态</span>
									</div>

								</div>
								<div class="portlet-body">
									<p class="text-left"> {pigcms{$taskinfo['title']}<br>
										分类：{pigcms{$taskinfo.cate_name}
										<br>

									</p>
									<p class="text-center"><font style="font-size: x-large; color: rgba(58, 127, 230, 0.96);"> <i class="fa fa-spinner fa-spin"></i>您已付款成功，等待对方工作完成！ </font></p>
									<p class="text-right"> 下单时间：{pigcms{$taskinfo.buy_time|date='Y-m-d H:i:s',###} </p>
								</div>

								<div class="text-center">

									<button class="btn yellow-mint uppercase" data-taskgoods-goodid="{pigcms{$taskinfo.task_id}"  data-taskgoods-orderid="{pigcms{$taskinfo.orderid}"  data-taskgoods-optype="seller" data-taskgoods-type="3" data-title="您是否需要申请仲裁？" data-btn-ok-label="确认申请" data-btn-cancel-label="取消"
									        data-toggle="confirmation" id="confirmation_zhongcai"><i class="fa fa-gavel"></i> 申请仲裁
									</button>
								</div>

								<style>
									.blog-single-img >img{width: 100%;  }
								</style>

								<div class="portlet-body">
									<h4>您的备注：</h4>
									{pigcms{$taskinfo.buydesc}
									<if condition="$taskinfo['meimg']">
										<h4>附件信息：</h4>
										<div class="blog-single-img">
											<img src="{pigcms{$taskinfo.meimg}" alt="">
										</div>

									</if>

									<h4>卖家备注：</h4>
									{pigcms{$taskinfo.selldesc}
								</div>


							</div>

							<elseif condition="($taskinfo['process_status'] eq 5)  AND ($is_buyer eq 1)"/>
							<form action="{pigcms{:U('TaskList/ordered_save_setp5')}" id="Buyer_setp5_Form" role="form" method="POST">
								<input type="hidden" name="task_id" id="task_id" value="{pigcms{$taskinfo.task_id}">
								<input type="hidden" name="optype" id="optype" value="buyer_setp5">
								<input type="hidden" name="orderid" id="orderid" value="{pigcms{$taskinfo.orderid}">

								<div class="portlet light ">
									<div class="portlet-title">
										<div class="caption">
											<i class="icon-social-dribbble font-red"></i>
											<span class="caption-subject font-red bold uppercase">进程状态</span>
										</div>

									</div>
									<div class="portlet-body">
										<p class="text-left"> {pigcms{$taskinfo['title']}<br>
											分类：{pigcms{$taskinfo.cate_name}
											<br>

										</p>
										<p class="text-center"><font style="font-size: x-large; color: rgba(58, 127, 230, 0.96);">  <i class="fa fa-check-circle"></i>对方已经提交完成，正在等待您确认验收！ </font></p>
										<p class="text-right"> 下单时间：{pigcms{$taskinfo.buy_time|date='Y-m-d H:i:s',###} </p>
									</div>

									<div class="text-center">
										<button type="button" id="buyer_ok_btn" class="btn blue btn-default">
											<i class="fa fa-paper-plane"></i> 验 收
										</button>
									</div>

							</form>

							<style>
								.blog-single-img >img{width: 100%;  }
							</style>

							<div class="portlet-body">
								<h4>您的备注：</h4>
								{pigcms{$taskinfo.buydesc}
								<if condition="$taskinfo['meimg']">
									<h4>附件信息：</h4>
									<div class="blog-single-img">
										<img src="{pigcms{$taskinfo.meimg}" alt="">
									</div>

								</if>
								<h4>卖家备注：</h4>
								{pigcms{$taskinfo.selldesc}

							</div>


						</div>
						<elseif condition="($taskinfo['process_status'] eq 6)"/>

						<div class="portlet light ">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-social-dribbble font-red"></i>
									<span class="caption-subject font-red bold uppercase">进程状态</span>
								</div>

							</div>
							<div class="portlet-body">
								<p class="text-left"> {pigcms{$taskinfo['title']}<br>
									分类：{pigcms{$taskinfo.cate_name}
									<br>

								</p>
								<p class="text-center"><font style="font-size: x-large; color: rgba(58, 127, 230, 0.96);">  <i class="fa fa-check-circle"></i>已经结束 </font></p>
								<p class="text-right"> 下单时间：{pigcms{$taskinfo.buy_time|date='Y-m-d H:i:s',###} </p>
							</div>
						</div>

						<elseif condition="($taskinfo['process_status'] eq 7)"/>
						<div class="portlet light ">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-social-dribbble font-red"></i>
									<span class="caption-subject font-red bold uppercase">进程状态</span>
								</div>

							</div>
							<div class="portlet-body">
								<p class="text-left"> {pigcms{$taskinfo['title']}<br>
									分类：{pigcms{$taskinfo.cate_name}
									<br>

								</p>
								<p class="text-center"><font style="font-size: x-large; color: rgba(58, 127, 230, 0.96);">  <i class="fa fa-spinner fa-spin"></i>此单仲裁中...
									</font></p>
								<p class="text-right"> 下单时间：{pigcms{$taskinfo.buy_time|date='Y-m-d H:i:s',###} </p>
							</div>
						</div>
						<else/> <!--- 工期协商 ---->

						<form action="{pigcms{:U('TaskList/ordered_first_save')}" id="First_Setp_Form"   role="form" method="POST"
						      enctype="multipart/form-data">

							<input type="hidden" name="task_id" id="task_id" value="{pigcms{$taskinfo.task_id}">
							<input type="hidden" name="pid" id="pid" value="{pigcms{$taskinfo.pid}">
							<input type="hidden" name="bid" id="bid" value="{pigcms{$taskinfo.bjid}">
							<if condition="$is_seller">
								<input type="hidden" name="type" id="type" value="fabu">
							<else/>
								<input type="hidden" name="type" id="type" value="chengjie">
							</if>

							<div id="stack_{pigcms{$taskinfo.task_id}" class="modal fade" tabindex="-1" data-width="400">
								<div class="modal-dialog">
									<div class="modal-content">
<!--										<form action="{pigcms{:U('Task/send_message')}" id="send_Message_Form" method="POST">-->
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
												<h4 class="modal-title">发送消息</h4>
											</div>
											<input type="hidden" name="good_id" id="good_id" value="{pigcms{$taskinfo.task_id}">
										<if condition="$is_seller">
											<input type="hidden" name="sell_uid" id="sell_uid" value="{pigcms{$taskinfo.carry_uid}">
											<input type="hidden" name="username" id="username" value="{pigcms{$taskinfo.chengjieName}">
											<input type="hidden" name="sentype" id="sentype" value="to_buyer">
										<else/>
											<input type="hidden" name="sell_uid" id="sell_uid" value="{pigcms{$taskinfo.task_uid}">
											<input type="hidden" name="username" id="username" value="{pigcms{$taskinfo.nickname}">
											<input type="hidden" name="sentype" id="sentype" value="to_seller">
										</if>
											<input type="hidden" name="msgtype" id="msgtype" value="task">
											<div class="modal-body">
												<div class="row">
													<div class="form-group ">
														<label class="control-label col-md-3">收件人:</label>
														<div class="col-md-9">
															<if condition="$is_seller">
																<input type="text" class="form-control"  readonly name="username" value="@{pigcms{$taskinfo.chengjieName}">
															<else/>
																<input type="text" class="form-control"  readonly name="username" value="@{pigcms{$taskinfo.nickname}">
															</if>
														</div>
													</div>
													<br><br>
													<div class="form-group ">
														<label class="control-label col-md-3">消息标题:</label>
														<div class="col-md-9">
															<input type="text" class="form-control" id="send_title" name="title">
														</div>
													</div>
													<br><br>
													<div id="show_textarea"
													     class="form-group" >
														<label class="control-label col-md-3">消息内容:</label>
														<div class="col-md-9">
															<textarea name="send_content"  id="send_content" class="form-control" placeholder="请输入您的内容"
															          rows="4"></textarea>
														</div>
														<br>

													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" data-dismiss="modal" class="btn dark btn-outline">关闭</button>
												<button type="button" id="send_Message" class="btn red">发送</button>
											</div>
<!--										</form>-->
									</div>
								</div>
							</div>
							<if condition="$is_seller AND $taskinfo['workday'] gt 0">

								<p>对方提交工期：<span class="label label-danger"> {pigcms{$taskinfo.workday} 天 </span></p>
								<label>如果您不同意，对方协商的工期，请输入您的预算工期（按天算）（请认真计算，确认之后不可修改，可以发短信和
									<a class=" " data-target="#stack_{pigcms{$taskinfo.task_id}" data-toggle="modal"> @{pigcms{$taskinfo.chengjieName}</a>沟通好）</label>

								<div class="input-group margin-top-10">

							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
									<input type="number" name="discussday" id="discussday" value="" class="form-control"  placeholder="请输入您的预算工期 比如：7 "> </div>

								<br>

								<div class="fileinput fileinput-new" data-provides="fileinput" style="display: none">
								<span class="btn green btn-file">
									<span class="fileinput-new"> 添加附件 </span>
									 <span class="fileinput-exists"> 更换 </span>
								  <input type="file" name="meimg" id="meimg"  /> </span>
									<span class="fileinput-filename"> </span> &nbsp; 建议最大上传2M
									<a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
								</div>
								<br>

								<div class="form-group">
									<div class="col-md-offset-4 col-md-8">
										<button type="submit" id="first_post_btn" class="btn blue">确认提交</button>
										&nbsp;
										<button type="button"  id="first_access_btn"  class="btn green">接 受</button>
									</div>
								</div>


							<elseif condition="$is_seller AND $taskinfo['workday'] eq 0"/>
								<div class="portlet light ">
									<div class="portlet-title">
										<div class="caption">
											<i class="icon-social-dribbble font-red"></i>
											<span class="caption-subject font-red bold uppercase">进程状态</span>
										</div>

									</div>
									<div class="portlet-body">
										<p class="text-left">
											分类：{pigcms{$taskinfo.pub_cate}
											<br>
										</p>
										<p class="text-center"><font style="font-size: x-large; color: rgba(58, 127, 230, 0.96);">  <i class="fa fa-spinner fa-spin"></i>等待对方提交工期 </font></p>
										<p class="text-right"> 发标日期：{pigcms{$taskinfo.add_time|date='Y-m-d H:i:s',###} </p>
									</div>
								</div>
							<else/>
							<label>请输入您的预算工期（按天算）（请认真计算，确认之后不可修改，可以发短信和
								<a class=" " data-target="#stack_{pigcms{$taskinfo.task_id}" data-toggle="modal"> @{pigcms{$taskinfo.nickname}</a>
								沟通好）</label>
							</if>
							<if condition="$is_seller AND $taskinfo['workday'] eq 0">
								<!-- --->

							<elseif condition="($is_seller eq 0) AND ($taskinfo['workday'] eq 0)"/>
							<div class="input-group margin-top-10">

							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
								<input type="number" name="workday" id="workday" value="" class="form-control"  placeholder="请输入您的预算工期 比如：7 "> </div>

							<br>

							<div class="fileinput fileinput-new" data-provides="fileinput" style="display: none">
								<span class="btn green btn-file">
									<span class="fileinput-new"> 添加附件 </span>
									 <span class="fileinput-exists"> 更换 </span>
								  <input type="file" name="meimg" id="meimg"  /> </span>
								<span class="fileinput-filename"> </span> &nbsp; 建议最大上传2M
								<a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
							</div>
							<br>

							<div class="form-group">
								<div class="col-md-offset-4 col-md-8">
									<button type="submit" id="first_post_btn" class="btn green">确认提交</button>
								</div>
							</div>

							 <elseif condition="($is_seller eq 0) AND ($taskinfo['workday'] neq 0)"/>
								<div class="portlet light ">
									<div class="portlet-title">
										<div class="caption">
											<i class="icon-social-dribbble font-red"></i>
											<span class="caption-subject font-red bold uppercase">进程状态</span>
										</div>

									</div>
									<div class="portlet-body">
										<p class="text-left">
											分类：{pigcms{$taskinfo.pub_cate}
											<br>
										</p>
										<p class="text-center"><font style="font-size: x-large; color: rgba(58, 127, 230, 0.96);">  <i class="fa fa-spinner
										fa-spin"></i>等待对方接受您提交的工期（{pigcms{$taskinfo.workday} 天）
											</font></p>
										<p class="text-right"> 得标日期：{pigcms{$taskinfo.add_time|date='Y-m-d H:i:s',###} </p>
									</div>
								</div>
							</if>
						</form>

						</if>
					</div>

				</div>
			</div>
		</div>


	</div>
</div>


</div>




</div><!-- 左侧 结束-->

<div class="col-md-4"><!-- 右侧  -->


	<ul class="todo-projects-container">
		<li class="todo-padding-b-0">
			<div class="todo-head">
				<a href="{pigcms{:U('TaskList/index',array('cval'=>4))}" class="btn btn-square btn-sm green todo-bold">更多</a>
				<h3><a href="{pigcms{:U('TaskList/index',array('cval'=>4))}">加急任务</a></h3>
				<p> </p>
			</div>
		</li>
		<volist name="task_list" id="vo">
			<li class="todo-projects-item">
				<h5><a href="{pigcms{:U('TaskList/detail',array('id'=>$vo['task_id']))}">{pigcms{$vo.title}</a></h5>
				<div class="todo-project-item-foot">
					<p class="todo-red todo-inline">{pigcms{$vo.pub_time|date='Y/m/d H:i:s',###}</p>
					<p class="todo-inline todo-float-r">{pigcms{$vo.rolein} 个人参与
						<a class="todo-add-button" href="{pigcms{:U('TaskList/detail',array('id'=>$vo['task_id']))}">+</a>
					</p>
				</div>
			</li>
			<div class="todo-projects-divider"></div>
		</volist>

	</ul>

</div><!-- 右侧 结束 -->

</div>

<!-- END PAGE CONTENT INNER -->
</div>
<!-- BEGIN QUICK SIDEBAR -->
<include file="Public:quick_sidebar" />
<!-- END QUICK SIDEBAR -->
</div>
<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<include file="Public:footer" />

<include file="Public:footer_js" />
<script src="{pigcms{$static_path}assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="{pigcms{$static_path}js/task_process_options.js" type="text/javascript"></script>


<script>
	$(function(){
		$('#first_post_Form #first_post_btn').click(function () {
			$(this).prop('disabled', true).addClass('disabled');
		})

	})

	var options_uri = "{pigcms{:U('TaskList/ordered_save_setp2')}";
	var sendmsg_uri = "{pigcms{:U('Task/send_message')}";
	var first_access_uri = "{pigcms{:U('TaskList/first_access')}";
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>