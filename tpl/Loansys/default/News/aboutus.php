<!doctype html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>{pigcms{$config.seo_title}</title>
	<include file="Public:headersrc"/>

</head>

<body class="">
<!-- 右侧联系我们 -->
<include file="Public:contact"/>

<div class="diyoumask" ></div>
<div class="c_area">


	<include file="Public:nav"/>


	<section>
		<div class="container nrye" style="margin-left: auto;margin-right: auto;">
			<div class="row">

				<include file="Public:news_left"/>

				<div class="col-sm-10">
					<div class="container_page">
						<section class="about">
							<h2>{pigcms{$aoubt_list.title|default='关于我们'}</h2>
							<p>
								 {pigcms{$aoubt_list.content}
							</p>


						</section>
					</div>
				</div>
			</div>
		</div>
	</section>

	<include file="Public:footer"/>
</div>
</body>

</html>