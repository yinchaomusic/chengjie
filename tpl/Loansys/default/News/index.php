<meta charset="utf-8"/>
<title>{pigcms{$config.seo_title}</title>

<include file="Public:headersrc"/>

</head>

<body class="">
<!-- 右侧联系我们 -->
<include file="Public:contact"/>

<div class="diyoumask" ></div>
<div class="c_area">

	<include file="Public:nav"/>

	<section>
		<div class="container nrye" style="margin-left: auto;margin-right: auto;">
			<div class="row">

				<include file="Public:news_left"/>

				<div class="col-sm-10">
					<div class="container_page">
						<section>
							<h2>{pigcms{$cate_list.cat_name}</h2>
							<div class="page_min_h">
								<ul class="news_list">
									<volist name="news_list" id="vo">
									<li>
										<a href="{pigcms{:U('News/show',array('news_id'=>$vo['news_id']))}">
											<span class="iconfont liujiaoxing">&#xe62c;</span>
											{pigcms{$vo.news_title}
											<span class="data">[{pigcms{$vo.add_time|date='Y-m-d',###}]</span>
										</a>
									</li>
									</volist>
								</ul>
							</div>

							<nav>
								<p class="pagination">{pigcms{$pagebar}</p>
							</nav>
						</section>
					</div>
				</div>
			</div>
	</section>

	<include file="Public:footer"/>

</div>
</body>

</html>
