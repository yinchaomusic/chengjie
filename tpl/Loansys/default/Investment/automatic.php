<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">



</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>

<div class="g-content u-cls">
	<div class="m-content">



		<section>
			<div class="container nrye">
				<div class="row">



					<include file="Public:nav_left"/>

					<div class="col-sm-10">
						<div class="container_page padding-sm-none">
							<section>
								<h2>自动投标</h2>
								<div class="tab_table page_min_h zdtb">
									<if condition="$count_user_times lt 5">
									<a href="{pigcms{:U('Investment/createAutomatic')}" class="btn btn-primary3 btn-warning btn-xs-block">+ 添加自动投标</a>
									</if>
									<ul class="nav nav-tabs">
										<li role="presentation" <if condition="$state eq 1"> class="active" </if> >
											<a href="{pigcms{:U('Investment/automatic',array('invalid'=>1))}">有效配置</a>
										</li>
										<li role="presentation" <if condition="$state eq 2"> class="active" </if>>
											<a href="{pigcms{:U('Investment/automatic',array('invalid'=>2))}">失效配置</a>
										</li>
									</ul>
									<div class="table-responsive">
										<table class=" table table-bordered table-striped table-hover ">
											<thead>
											<tr>
												<th>投标总额</th>
												<th>可用金额</th>
												<th>已投资金额</th>
												<th>期数</th>
												<th><if condition="$state eq 1">最低利率<elseif condition="$state eq 2" />利率</if></th>
												<th>类型</th>
												<th>操作</th>
											</tr>
											</thead>
											<tbody class="text-center">
											<if condition="is_array($automaticList)">
												<volist name="automaticList" id="vo">
												<tr>
													<td>￥{pigcms{$vo.total_amount}</td>
													<td>￥{pigcms{$vo.surplus_meoney}</td>
													<td>￥{pigcms{$vo.has_meoney}</td>
													<td>{pigcms{$vo.LoanCycle|default=0} 天</td>
													<td><if condition="$state eq 1">{pigcms{$vo.MinRate}%<elseif condition="$state eq 2" /> {pigcms{$vo.Rate|default=0}%</if> </td>
													<td><if condition="$vo['autotype'] eq 1">自动投标<elseif condition="2"/>回款续投</if></td>
													<td><a href="javascript:void(0);" id="nc_{pigcms{$vo.aid}" onclick="automatic('{pigcms{$vo.aid}',1);return false;">删除</a> </td>
												</tr>
												</volist>
											<else/>
											<!--暂无记录-->
											<tr>
												<td colspan="7">暂无记录</td>
											</tr>
											<!--暂无记录-->
											</if>
											</tbody>
										</table>
									</div>
								</div>

							</section>

						</div>
					</div>
				</div>
			</div>
		</section>



	</div>
</div>
<include file="Public:footer"/>

<include file="Public:footerjs"/>
<script type="text/javascript">

	var submit_check="{pigcms{:U('Investment/automatic')}";
	function automatic(id,type) {
		var title,text,configbutton;
		if(type ==1){
			title = "你确定要删除么？";
			text =  "若删除自动投标配置，您账户内对应的冻结金额将自动转换为可用余额";
			configbutton = "删除";
		}else if(type == 2){
			title = "你确定要发布么？";
			text = "如在设定的借款周期内满标，是无法取消，是否需要发布？"
			configbutton = "我要发布"
		}

		swal({
			title: title,
			text: text,
			type: "info",
			showCancelButton: true,
			cancelButtonText:"取消",
			confirmButtonText:configbutton,
			closeOnConfirm: false,
			showLoaderOnConfirm: true,
		}, function(){
			//setTimeout(function(){
			var nowcheckdomain = $('#nc_'+id);
			nowcheckdomain.html("<img src='{pigcms{$static_path}images/index/loading.gif' width='20' height='20' />");
			$.post(submit_check, {'id': id,'type':type}, function (result) {
				result = $.parseJSON(result);
				if (result) {
					if (result.error == 0) {
						nowcheckdomain.parent().parent().remove();
						swal(result.msg);
					} else {
						swal(result.msg);return false;
					}
				} else {
					swal(result.msg);return false;
				}
			});
			//}, 2000);
		});
	}


	$(function () {
		var $s_area = $("#filter-specs");
		$(".s_bar").click(function () {
			$s_area.addClass("box-filter-expanded");
			$("body").addClass("phone_s");


		});
		$(".close_s").click(function () {
			$s_area.removeClass("box-filter-expanded");
			$("body").removeClass("phone_s");

		});

		if (/*navigator.userAgent.match(/mobile/i) && */$(window).width() < 768) {
			// alert("手机")

			$("#filter-specs dd").height($(window).height() - 100);
			$(window).resize(function () {
				$("#filter-specs dd").height($(window).height() - 100);
			});
			/*选项卡*/
			var $tab_b = $(".tab_s_box dt"), $tab_c = $(".tab_s_box dd");
			$tab_b.click(function () {
				if (!$(this).hasClass("active")) {
					$(this).siblings("dt").removeClass("active");
					$(this).addClass("active");
					$tab_c.removeClass("active");
					$(this).next("dd").addClass("active");
				};
			});
		} else {
			$("#filter-specs dd").height("auto");
		};



	});


</script>


</body>
</html>