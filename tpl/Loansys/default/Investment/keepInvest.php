<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">

</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>

<div class="g-content u-cls">
	<div class="m-content">



		<section>
			<div class="container nrye">
				<div class="row">

					<include file="Public:nav_left"/>

					<form action="{pigcms{:U('Investment/keepInvest')}" autocomplete="off" id="keepInvestForm" method="post">
						<if condition="$automaticList['aid']">
						<input type="hidden" value="{pigcms{$automaticList.aid}" name="aid">
						</if>
					<div class="col-sm-10">
						<div class="container_page  tjzdtb">
							<section>
								<h2>回款续投配置</h2>
								<div class="bg_violet clearfix">

									<div class="form-horizontal  ">
										<div class="form-group">
											<label for="MinRate" class=" control-label col-sm-1">最低利率：</label>
											<div class="col-sm-3">
<input class="form-control Decimal" id="MinRate" name="MinRate" placeholder="" type="number" value="{pigcms{$automaticList.MinRate}" />                                    </div>
											<p class="form-control-static">%，不能超过25%</p>
											<span class="form-control-tips" style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
										</div>
										<div class="form-group">
											<label for="MinRate" class=" control-label col-sm-1">保留金额：</label>
											<div class="col-sm-3">
	<input class="form-control Decimal" id="ReservedAmount" name="ReservedAmount" type="number" value="{pigcms{$automaticList.reserved_meoney|default=100}" />                   </div>
											<p class="form-control-static">元，回款后账户保留金额不会自动续投</p>
										</div>
										<div class="form-group" style="display:none;">
											<label for="MinRate" class=" control-label col-sm-1">起投金额：</label>
											<div class="col-sm-3">
												<input class="form-control Decimal" id="MinAmount" name="MinAmount" type="number" value="100" />                                    </div>
											<p class="form-control-static">元，回款后可用余额 - 保留金额≥起投金额时才会续投</p>
										</div>
										<div class="form-group">
											<label for="input6" class=" control-label col-sm-1 ">贷款周期：</label>
											<div class="col-sm-8 select-inline">
												<span class="a-sm-btn checkall">全部选择</span>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleDay" <if condition="$LoanCycleDay[7] eq 7">checked</if>
													value="7" >
													7天
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleDay"<if condition="$LoanCycleDay[15] eq 15">checked</if>
													value="15" >
													15天
												</label>
												<br />
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth"<if condition="$LoanCycleMonth[1] eq
													1">checked</if> value="1" >
													1个月
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth"<if condition="$LoanCycleMonth[2] eq
													2">checked</if> value="2" >
													2个月
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth"<if condition="$LoanCycleMonth[3] eq
													3">checked</if> value="3" >
													3个月
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth"<if condition="$LoanCycleMonth[4] eq
													4">checked</if> value="4" >
													4个月
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth"<if condition="$LoanCycleMonth[5] eq
													5">checked</if> value="5" >
													5个月
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth" <if condition="$LoanCycleMonth[6] eq
													6">checked</if> value="6" >
													6个月
												</label>
												<br />
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth" <if condition="$LoanCycleMonth[7] eq
													7">checked</if> value="7" >
													7个月
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth" <if condition="$LoanCycleMonth[8] eq
													8">checked</if> value="8" >
													8个月
												</label>
												<label class="checkbox-inline">
												<input type="checkbox" name="LoanCycleMonth"<if condition="$LoanCycleMonth[9] eq 9">checked</if>
													value="9" >
													9个月
												</label>
												<label class="checkbox-inline"><input type="checkbox" name="LoanCycleMonth"<if
														condition="$LoanCycleMonth[10] eq 10">checked</if> value="10" >
													10个月
												</label>
												<label class="checkbox-inline">
												<input type="checkbox" name="LoanCycleMonth" <if condition="$LoanCycleMonth[11] eq 11">checked</if>
													value="11" >
													11个月
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth" <if condition="$LoanCycleMonth[12] eq
													12">checked</if> value="12" >
													12个月
												</label>
											</div>
											<span class="form-control-tips col-sm-2" style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
										</div>
										<div class="form-group">
											<label for="input6" class=" control-label col-sm-1 ">开启设置：</label>
											<div class="col-sm-8 select-inline">
												<label class="checkbox-inline">
													<input type="radio" name="is_open" <if condition="$automaticList['is_open'] eq 1"> checked  </if>
														value="1" >
													开启
												</label>
												<label class="checkbox-inline">
													<input type="radio" name="is_open"  <if condition="$automaticList['is_open'] eq 0"> checked
													</if> value="0" >
													关闭
												</label>

											</div>
										</div>
									</div>
								</div>
								<div class="btn_area">
									<input type="submit" class="btn  btn-warning btn-xs-block" value="保 存">
								</div>
								<br />
								<p class="info-box">
									<span>回款续投功能说明：</span><br />
									目的：回款续投功能主要是为了方便客户因没时间登录帐号导致投资后的资金到期后闲置而设立。<br />
									功能开启：按照投资人要求保留一定的金额后按客户设置的利率将回款后的资金投出，一旦投资满标资金将无法撤回！<br />
									功能关闭：需要投资人手动关闭，关闭后结束回款续投功能。<br />
									特别说明：回款续投金额=下次回款金额+账户余额-保留金额。<br />
									<span style="color:red">特别提醒：回款资金有提现需求的客户请谨慎使用此功能，随时注意开启和关闭。</span>
								</p>
							</section>

						</div>
					</div>
					</form>
				</div>
			</div>
		</section>



	</div>
</div>
<include file="Public:footer"/>

<include file="Public:footerjs"/>

<script type="text/javascript">
	formSubmitSuccess = function () {
		alert("保存成功");
		window.location = "{pigcms{:U('Investment/keepInvest')}";
	}
	$(function () {
		$(".checkall").click(function () {
			$(".checkbox-inline :checkbox").prop('checked', true);
		});
		$("#MinRate").on("keyup", function () {
			if ($(this).val() > 25) {
				$(this).val(25);
			}

		});
		$("#MinAmount").on("blur", function () {
			if ($(this).val() > 100) {
				$(this).val(parseInt($(this).val() / 100) * 100);
			}
			if ($(this).val() < 100) {
				$(this).val(100);
			}
			else if ($(this).val() >1000000) {
				$(this).val(1000000);
			}
		});
		var notNull = "False";
		if (notNull == "True")
		{
			var rule = "";
			$(".checkbox-inline :checkbox").prop('checked', false);
			$(".checkbox-inline :checkbox[name='LoanCycleDay']").each(function () {
				var r = $(this).val() + "d";
				if (rule.indexOf(r) > 0) {
					$(this).prop("checked", true);
				}
			});
			$(".checkbox-inline :checkbox[name='LoanCycleMonth']").each(function () {
				var r = $(this).val()*30 + "d";
				if (rule.indexOf(r) > 0) {
					$(this).prop("checked", true);
				}
			});
		}

		var formSubmitSuccess;
		var formSubmitBefore;
		$('#keepInvestForm').submit(function () {
			var $form = $("form");
			var url = $form.attr("action");
			var submitButtons = $form.find(":submit");
			var texts = $form.find(":text,input[type=date],input[type=number],input[type=tel],input[type=email]");
			var hiddens = $form.find(":hidden");
			var checkboxs = $form.find(":checkbox:checked");
			var radios = $form.find(":radio:checked");
			var data = {};

			//hiddens
			var hiddenItems = {};
			for (var i = 0; i < hiddens.length; i++) {
				var name = hiddens.eq(i).attr("name");
				var values = hiddenItems[name];
				if (!values) {
					values = [];
				}
				values.push(hiddens.eq(i).val());
				hiddenItems[name] = values;
			}
			for (var item in hiddenItems) {
				data[item] = hiddenItems[item].join(',');
			}
			//texts
			var textItems = {};
			for (var i = 0; i < texts.length; i++) {
				var name = texts.eq(i).attr("name");
				var values = textItems[name];
				if (!values) {
					values = [];
				}
				values.push(texts.eq(i).val());
				textItems[name] = values;
			}
			for (var item in textItems) {
				data[item] = textItems[item].join(',');
			}
			//radios
			for (var i = 0; i < radios.length; i++) {
				data[radios.eq(i).attr("name")] = radios.eq(i).val();
			}
			//checkboxs
			var checkboxItems = {};
			for (var i = 0; i < checkboxs.length; i++) {
				var name = checkboxs.eq(i).attr("name");
				var values = checkboxItems[name];
				if (!values) {
					values = [];
				}
				values.push(checkboxs.eq(i).val());
				checkboxItems[name] = values;
			}
			for (var item in checkboxItems) {
				data[item] = checkboxItems[item].join(',');
			}

			if (formSubmitBefore) {
				var isBrack = formSubmitBefore(url, data);
				if (!isBrack)
					return false;
			}


			if(  $("#MinRate").val() == '')
			{
				swal("错误！","回款续投配置最低利率必须设置",'error');
				submitButtons.removeAttr("disabled");
				return false;
			}
			if(($("#ReservedAmount").val() < 100) || $("#ReservedAmount").val() == '')
			{
				swal("保留金额太少了！","保留一定的金额(至少100元)",'error');
				submitButtons.removeAttr("disabled");
				return false;
			}
			if((data.LoanCycleDay == '' || typeof(data.LoanCycleDay) == 'undefined') && (data.LoanCycleMonth == '' || typeof(data.LoanCycleMonth) ==
				'undefined')){
				swal("错误！","贷款周期必须选择一个！",'error');
				submitButtons.removeAttr("disabled");
				return false;
			}
			submitButtons.attr("disabled", "true");
			$.post(url,
				data,
				function (result) {
					result = $.parseJSON(result);
					if (result) {
						if (result.error == -1) {
							swal(result.title,result.msg,'error');
							if(result.reloadurl){
								setTimeout(function(){    window.location = result.reloadurl; }, 3000);
							}
							submitButtons.removeAttr("disabled");
							return;
						}
						if(result.error == 0){
							//swal(result.title,result.msg,'success');
							swal({   title: result.title,   text: result.msg, timer:3000,  imageUrl: "images/thumbs-up.jpg",showCancelButton: false,confirmButtonColor: "#FFA200",confirmButtonText:"OK" }, function (isConfirm) {
								window.location = result.reloadurl;
							});
							submitButtons.removeAttr("disabled");
						}
					}

					if (formSubmitSuccess) {
						formSubmitSuccess($form, result);
					} else {
						//window.location = window.location;
					}
				});
			return false;
		});


	});
</script>

</body>
</html>