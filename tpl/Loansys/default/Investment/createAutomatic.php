<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">

</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>

<div class="g-content u-cls">
	<div class="m-content">



		<section>
			<div class="container nrye">
				<div class="row">


					<include file="Public:nav_left"/>

					<form action="{pigcms{:U('Investment/createAutomatic')}" id="automaticForm" method="post" autocomplete="off">

					<div class="col-sm-10">
						<div class="container_page  tjzdtb">
							<section>
								<h2>自动投标配置</h2>
								<div class="bg_violet clearfix">
<strong>可用余额:<span class="font_c_green" id="autoAquotas"data-amount="{pigcms{$user_session['now_money']}">￥{pigcms{$user_session['now_money']}</span></strong>
									<div class="form-horizontal  ">
										<div class="form-group">
											<label for="autoAmount" class=" control-label col-sm-1">投标总额：</label>
											<div class="col-sm-3">
												<input class="form-control" id="autoAmount" name="Amount" type="number" value="" />
											</div>
											<p class="form-control-static">元，必须为100的整数倍</p>
											<span class="form-control-tips" style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
										</div>
										<div class="form-group">
											<label for="MinRate" class=" control-label col-sm-1">最低利率：</label>
											<div class="col-sm-3">
												<input class="form-control Decimal" id="MinRate" name="MinRate" type="number" value="" />
											</div>
											<p class="form-control-static">%，不能超过25%</p>
											<span class="form-control-tips" style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
										</div>
										<div class="form-group">
											<label for="input6" class=" control-label col-sm-1 ">贷款周期：</label>
											<div class="col-sm-8 select-inline">
												<span class="a-sm-btn checkall">全部选择</span>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleDay" value="7" >7天
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleDay" value="15" >15天
												</label>
												<br />
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth" value="1" >1个月
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth" value="2" >2个月
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth" value="3" >3个月
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth" value="4" >4个月
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth" value="5" >5个月
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth" value="6" >6个月
												</label>
												<br />
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth" value="7" >7个月
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth" value="8" >8个月
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth" value="9" >9个月
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth" value="10" >10个月
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth" value="11" >11个月
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="LoanCycleMonth" value="12" >12个月
												</label>
											</div>
											<span class="form-control-tips col-sm-2" style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
										</div>
									</div>
								</div>
								<div class="btn_area">
									<input type="submit" class="btn  btn-primary3 btn-warning btn-xs-block" value="保存">
								</div>
							</section>
						</div>
					</div>
					</form>
				</div>
			</div>
		</section>



	</div>
</div>
<include file="Public:footer"/>

<include file="Public:footerjs"/>

<script type="text/javascript">
	formSubmitSuccess = function () {
		window.location = "{pigcms{:U('Investment/automatic')}";
	}
	$(function () {
		$(".checkall").click(function () {
			$(".checkbox-inline :checkbox").prop('checked', true);
		});

		var Aquotas = ($("#autoAquotas").attr("data-amount") * 1).toFixed(4).replace(/(.*)\d\d/, "$1") * 1;
		Aquotas = parseInt(Aquotas / 100) * 100;
		$("#autoAmount").on("blur", function () {
			var $this = $(this);
			if ($(this).val() > 100) {
				$(this).val(parseInt($this.val() / 100) * 100);

			} else if ($(this).val() != "") {
				$(this).val(100);
			}
		});
		$("#autoAmount").on("keyup", function () {
			var $this = $(this);
			if ($(this).val() > Aquotas) {
				$(this).val(Aquotas);
			};

		});
		$("#MinRate").on("keyup", function () {
			if ($(this).val() > 25) {
				$(this).val(25);
			}

		});


		var formSubmitSuccess;
		var formSubmitBefore;
		$('#automaticForm').submit(function () {
			var $form = $("form");
			var url = $form.attr("action");
			var submitButtons = $form.find(":submit");
			var texts = $form.find(":text,input[type=date],input[type=number],input[type=tel],input[type=email]");
			var hiddens = $form.find(":hidden");
			var checkboxs = $form.find(":checkbox:checked");
			var radios = $form.find(":radio:checked");
			var data = {};

			//hiddens
			var hiddenItems = {};
			for (var i = 0; i < hiddens.length; i++) {
				var name = hiddens.eq(i).attr("name");
				var values = hiddenItems[name];
				if (!values) {
					values = [];
				}
				values.push(hiddens.eq(i).val());
				hiddenItems[name] = values;
			}
			for (var item in hiddenItems) {
				data[item] = hiddenItems[item].join(',');
			}
			//texts
			var textItems = {};
			for (var i = 0; i < texts.length; i++) {
				var name = texts.eq(i).attr("name");
				var values = textItems[name];
				if (!values) {
					values = [];
				}
				values.push(texts.eq(i).val());
				textItems[name] = values;
			}
			for (var item in textItems) {
				data[item] = textItems[item].join(',');
			}

			//checkboxs
			var checkboxItems = {};
			for (var i = 0; i < checkboxs.length; i++) {
				var name = checkboxs.eq(i).attr("name");
				var values = checkboxItems[name];
				if (!values) {
					values = [];
				}
				values.push(checkboxs.eq(i).val());
				checkboxItems[name] = values;
			}
			for (var item in checkboxItems) {
				data[item] = checkboxItems[item].join(',');
			}

			if (formSubmitBefore) {
				var isBrack = formSubmitBefore(url, data);
				if (!isBrack)
					return false;
			}

			data['Aquotas']    = $("#autoAquotas").text().replace(/￥/ig,'');//可用余额
			//data['Amount']   = $("#Amount").val();
			if(  $("#autoAquotas").text() == ''|| $("#autoAmount").val() =='' || $("#autoAmount").val() =='0'  )
			{
				swal("错误！","请认真填写所需要的信息",'error');
				submitButtons.removeAttr("disabled");
				return false;
			}
			if((data.LoanCycleDay == '' || typeof(data.LoanCycleDay) == 'undefined') && (data.LoanCycleMonth == '' || typeof(data.LoanCycleMonth) ==
				'undefined')){
				swal("错误！","贷款周期必须选择一个！",'error');
				submitButtons.removeAttr("disabled");
				return false;
			}
			submitButtons.attr("disabled", "true");
			$.post(url,
				data,
				function (result) {
					result = $.parseJSON(result);
					if (result) {
						if (result.error == -1) {
							swal(result.title,result.msg,'error');
							if(result.reloadurl){
								setTimeout(function(){    window.location = result.reloadurl; }, 3000);
							}
							submitButtons.removeAttr("disabled");
							return;
						}
						if(result.error == 0){
							//swal(result.title,result.msg,'success');
							swal({   title: result.title,   text: result.msg, timer:3000,  imageUrl: "images/thumbs-up.jpg",showCancelButton: false,confirmButtonColor: "#FFA200",confirmButtonText:"OK" }, function (isConfirm) {
								window.location = result.reloadurl;
							});
							submitButtons.removeAttr("disabled");
						}
					}

					if (formSubmitSuccess) {
						formSubmitSuccess($form, result);
					} else {
						//window.location = window.location;
					}
				});
			return false;
		});



	});
</script>



</body>
</html>