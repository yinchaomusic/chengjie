<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">



</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>

<div class="g-content u-cls">
	<div class="m-content">



		<section>
			<div class="container nrye">
				<div class="row">



					<include file="Public:nav_left"/>

					<div class="col-sm-10">
						<div class="container_page padding-sm-none">
							<section>
								<h2>借出列表</h2>
								<div class="tab_table page_min_h">
									<ul class="nav nav-tabs">
										<li role="presentation" <if condition="$state eq 0"> class="active"</if> ><a href="{pigcms{:U('Investment/lendList')}">全部</a></li>
										<li role="presentation" <if condition="$state eq 1"> class="active"</if>><a href="{pigcms{:U('Investment/lendList',array('state'=>1))}">正在投标</a></li>
										<li role="presentation" <if condition="$state eq 2"> class="active"</if>><a href="{pigcms{:U('Investment/lendList',array('state'=>2))}">等待还款</a></li>
										<li role="presentation" <if condition="$state eq 3"> class="active"</if>><a href="{pigcms{:U('Investment/lendList',array('state'=>3))}">还款结束</a></li>
										<li role="presentation" <if condition="$state eq 4"> class="active"</if>><a href="{pigcms{:U('Investment/lendList',array('state'=>4))}">已过期</a></li>
									</ul>
									<div class="table-responsive">
										<table class=" table table-bordered table-striped table-hover " style="font-size: 13px;">
											<thead>
											<tr>
												<th>借出信息</th>
												<th>状态</th>
												<th>还款日期</th>
												<th>满标日期</th>
												<th>期数</th>
												<th>总收本息</th>
												<th>已收本息</th>
												<th>操作</th>
											</tr>
											</thead>
											<tbody class="text-center">
											<if condition="is_array($borrowsList)">

												 <volist name="borrowsList" id="vo">
													 <tr id ="delbid_{pigcms{$vo.bid}">
														 <td><a href="{pigcms{:U('Investment/loanScheme',array('dkid'=>$vo['bid']))}" target="_blank">
															 总投资  {pigcms{$vo.get_all_Amountdx}元<br />
															￥{pigcms{$vo.get_all_Amount}元<br />
																 {pigcms{$vo.did}
															 </a> <br />
															 贷款人：{pigcms{$vo.dname}<br />
															 年利率:{pigcms{$vo.Rate}% <br/>
															 每期总利息收益:￥{pigcms{$vo.get_all_nsy} <br/>
															 还款方式：每月还息 <br/>
				<a href="javascript:void(0);" id="detail_{pigcms{$vo.bid}" onclick="showdetail('{pigcms{$vo.bid}');return false;">点击我查看详细投资信息</a>
														 </td>
														 <td>
															 <if condition="$vo['status'] eq 1"><span class="font_success">正在投标</span><elseif condition="$vo['status'] eq 2"/><span class="font_default">等待还款</span><elseif condition="$vo['status'] eq 3"/><span class="font_default">还款结束</span><elseif condition="$vo['status'] eq 4"/><span class="font_default">已过期</span><elseif condition="$vo['status'] eq 5"/><span class="font_default">等待审核</span><elseif condition="$vo['status'] eq 6"/><span class="font_success">审核通过</span><elseif condition="$vo['status'] eq 7"/><span class="font_warning"><a href="javascript:void(0);" title="{pigcms{$vo.intro}">申请拒绝</a></span><elseif condition="$vo['status'] eq 9"/><span class="font_success">投资已经完成</span></if>
														 </td>
														 <td><if condition="$vo['repayDate']">{pigcms{$vo.repayDate}</if> </td>
														 <td>{pigcms{$vo.ExpiredAt}</td>
														 <td>{pigcms{$vo.periods}期</td>
														 <td>￥{pigcms{$vo.get_all_menory}</td>
														 <td>￥ {pigcms{$vo.get_all_TR_principal}</td>
														 <td>
															 <if condition="$vo['status'] eq 1">
					<a href="javascript:void(0);" id="nc_{pigcms{$vo.bid}" onclick="cancelthis('{pigcms{$vo.bid}',1);return false;">取消投资</a>
															<elseif condition="$vo['status'] eq 2"/>
																 等待还款
															<elseif condition="$vo['status'] eq 3"/>
																 还款结束
															 <elseif condition="$vo['status'] eq 4"/>
																 已过期
																 <elseif condition="$vo['status'] eq 9"/>
																等待还款
															<else />

															 </if>

														 </td>
													 </tr>
												 </volist>

											<else/>
												<!--暂无记录-->
												<tr>
													<td colspan="8">暂无记录</td>
												</tr>
												<!--暂无记录-->
											</if>

											</tbody>
										</table>
									</div>
								</div>




							</section>
						</div>
					</div>
				</div>
			</div>
		</section>



	</div>
</div>
<include file="Public:footer"/>
<style>
	.customClassset{
		width: 860px;
		text-align: center;
	}
</style>
<include file="Public:footerjs"/>
<script type="text/javascript">

	function showdetail(id){
		//var nowcheckdomain = $('#nc_'+id);
		//nowcheckdomain.html("<img src='{pigcms{$static_path}images/index/loading.gif' width='20' height='20' />");
		swal({   title: "玩命加载中...",   text: "客官，请耐心等待",      showConfirmButton: false });
		var submit_getdata = "{pigcms{:U('Investment/getInvestmentdata')}";
		$.post(submit_getdata, {'id': id}, function (result) {
			//result = $.parseJSON(result);
			if (result) {
				if (result.error == 0) {
					swal({
						title: result.title,
						confirmButtonColor: "#FFA200",
						confirmButtonText: "关闭",
						customClass:"customClassset",
						text: result.htmldata,
						html:true
				});
				} else {
					swal(result.msg);
					return false;
				}
			} else {
				swal(result.msg);
				return false;
			}
		});

	}

	var submit_check="{pigcms{:U('Investment/cancelInvestment')}";
	function cancelthis(id,type) {
		var title,text,configbutton;
		if(type ==1){
			title = "你确定要取消投资？";
			text =  "确认则删除该贷款取消投资资金？";
			configbutton = "是的，我要取消";
		}else if(type == 2){
			title = "你确定要发布么？";
			text = "如在设定的借款周期内满标，是无法取消，是否需要发布？"
			configbutton = "我要发布"
		}

		swal({
			title: title,
			text: text,
			type: "info",
			showCancelButton: true,
			cancelButtonText:"不取消",
			confirmButtonText:configbutton,
			closeOnConfirm: false,
			showLoaderOnConfirm: true,
		}, function(){
			//setTimeout(function(){
			var nowcheckdomain = $('#nc_'+id);
			nowcheckdomain.html("<img src='{pigcms{$static_path}images/index/loading.gif' width='20' height='20' />");
			$.post(submit_check, {'id': id,'type':type}, function (result) {
				//result = $.parseJSON(result);
				if (result) {
					if (result.error == 0) {
						nowcheckdomain.parent().parent().remove();
						swal(result.msg);
					} else {
						swal(result.msg);
						return false;
					}
				} else {
					swal(result.msg);
					return false;
				}
			});
			//}, 2000);
		});
	}

	$(function () {
		var $s_area = $("#filter-specs");
		$(".s_bar").click(function () {
			$s_area.addClass("box-filter-expanded");
			$("body").addClass("phone_s");


		});
		$(".close_s").click(function () {
			$s_area.removeClass("box-filter-expanded");
			$("body").removeClass("phone_s");

		});

		if (/*navigator.userAgent.match(/mobile/i) && */$(window).width() < 768) {
			// alert("手机")

			$("#filter-specs dd").height($(window).height() - 100);
			$(window).resize(function () {
				$("#filter-specs dd").height($(window).height() - 100);
			});
			/*选项卡*/
			var $tab_b = $(".tab_s_box dt"), $tab_c = $(".tab_s_box dd");
			$tab_b.click(function () {
				if (!$(this).hasClass("active")) {
					$(this).siblings("dt").removeClass("active");
					$(this).addClass("active");
					$tab_c.removeClass("active");
					$(this).next("dd").addClass("active");
				};
			});
		} else {
			$("#filter-specs dd").height("auto");
		};

	});
</script>


</body>
</html>