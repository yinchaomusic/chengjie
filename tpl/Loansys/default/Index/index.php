<!doctype html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>诚借-域名融资贷款平台！www.chengjie.com</title>
	<include file="Public:headersrc"/>


</head>

<body class="">
<!-- 右侧联系我们 -->
<include file="Public:contact"/>

<div class="diyoumask" ></div>

<div class="c_area">


	<include file="Public:nav"/>

	<script type="text/javascript">
		$(function () {
			$('#myslides').flexslider({
				animation: "slide",//转换方式 fade淡入淡出 slide滚动
				direction: "horizontal",//滚动方向 horizontal左右 vertical上下
				easing: "swing",
				slideshowSpeed: 4000, //停留时间
				directionNav: false, //是否显示左右控制按钮 true&false
				controlNav: true, //是否显示下方控制按钮 true&false
				mousewheel: false //是否允许鼠标控制滚动 true&fals
			});
		});
	</script>
	<section>
		<div id="myslides" class="flexslider">
			<ul class="slides">
				<pigcms:adver cat_key="Loansys_index_banner" limit="3" var_name="Loansys_index_banner">
					<li   style="background-color:{pigcms{$vo.bg_color};">
							<a  class="img" href="{pigcms{$vo.url}"><img src="{pigcms{$vo.pic}"></a>
					</li>
				</pigcms:adver>

			</ul>
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>

	</section>
	<section class="banner_1">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<a href="{pigcms{:U('Investment/lend')}">
						<dl>
							<dt>
							<div class="icon_title"><span class="iconfont liujiaoxing">&#xe649;</span> <small>理财人</small> </div>
							<h2>有闲钱你就能投资</h2>
							</dt>
							<dd><span class="iconfont dagou">&#xe606;</span>最低100元即可投资</dd>
							<dd><span class="iconfont dagou">&#xe606;</span>高收益超过普通理财产品</dd>
							<dd><span class="iconfont dagou">&#xe606;</span>100%本息保障</dd>
							<dd><span class="iconfont dagou">&#xe606;</span>灵活的投资组合</dd>
						</dl>
						<article><small>理财人</small><span class="dhk"><span class="iconfont iconza hidden-xs">&#xe639;</span>网络理财新模式，高收益，低风险，赶快行动吧！</span></article>
					</a>
				</div>
				<div class="col-sm-6 banner_r">
					<a href="{pigcms{:U('Loan/borrow')}">
						<dl>
							<dt>
							<div class="icon_title"><span class="iconfont liujiaoxing ">&#xe649;</span> <small>借款人</small> </div>
							<h2>有域名你就能借款</h2>
							</dt>
							<dd><span class="iconfont dagou">&#xe606;</span>有域名轻松借款</dd>
							<dd><span class="iconfont dagou">&#xe606;</span>免各种资料审核</dd>
							<dd><span class="iconfont dagou">&#xe606;</span>有效利用闲置资产</dd>
							<dd><span class="iconfont dagou">&#xe606;</span>安全、快捷到账</dd>
						</dl>
						<article><span class="dhk">域名融资新渠道，我们可以做银行做不了的事情<span class="iconfont iconza2 hidden-xs">&#xe636;</span></span><small>借款人</small></article>
					</a>
				</div>
			</div>
		</div>
	</section>
	<section class="help_area text-center">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
                <span><img <if condition="$cate_list['icon']"> style="border-radius: 25%;" src="upload/cat/{pigcms{$cate_list.icon}" <else/>src="{pigcms{$static_path}/images/rongzi/h_1.png"</if> alt=""></span>
					<dl>
						<dt>
						<h3><if condition="$cate_list['cat_name']">{pigcms{$cate_list.cat_name}<else/>我要理财</if> </h3>
						</dt>
						<volist name="licai_list" id="vo">
						<dd><a href="{pigcms{:U('News/show',array('news_id'=>$vo['news_id']))}" target="_blank">{pigcms{$vo.news_title}</a></dd>
						</volist>
						<dd><a href="{pigcms{:U('News/index',array('cat_key'=>'Loansys_help_li'))}" target="_blank" class="more">更多>></a></dd>
					</dl>
				</div>
				<div class="col-sm-4">
					<span><img <if condition="$Xcate_list['icon']"> style="border-radius: 25%;" src="upload/cat/{pigcms{$Xcate_list.icon}" <else/>src="{pigcms{$static_path}/images/rongzi/h_2.png"</if> alt=""></span>
					<dl>
						<dt>
						<h3><if condition="$Xcate_list['cat_name']">{pigcms{$Xcate_list.cat_name}<else/>我要借款</if> </h3>
						</dt>
						<volist name="jiekuanList" id="vo">
						<dd><a href="{pigcms{:U('News/show',array('news_id'=>$vo['news_id']))}" target="_blank">{pigcms{$vo.news_title}</a></dd>
						</volist>
						<dd><a href="{pigcms{:U('News/index',array('cat_key'=>'Loansys_help_ji'))}" target="_blank" class="more">更多>></a></dd>
					</dl>
				</div>
				<div class="col-sm-4">
					<span><img <if condition="$cj_list['icon']"> style="border-radius: 25%;" src="upload/cat/{pigcms{$cj_list.icon}" <else/>src="{pigcms{$static_path}/images/rongzi/h_3.png"</if> alt=""></span>
					<dl>
						<dt>
						<h3><if condition="$cj_list['cat_name']">{pigcms{$cj_list.cat_name}<else/>常见问题</if> </h3>
						</dt>
						<volist name="changjianList" id="vo">
						<dd><a href="{pigcms{:U('News/show',array('news_id'=>$vo['news_id']))}" target="_blank">{pigcms{$vo.news_title} </a></dd>
						</volist>
						<dd><a href="{pigcms{:U('News/index',array('cat_key'=>'Loansys_help_cj'))}" target="_blank" class="more">更多>></a></dd>
					</dl>
				</div>
			</div>
		</div>
	</section>

	<section class="news_area">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="text-center">{pigcms{$notices.cat_name|default='新闻公告'}<span></span></h2>
					<ul class="list-unstyled">
						<volist name="notice_list" id="vo">
						<li>
							<a href="{pigcms{:U('News/show',array('news_id'=>$vo['news_id']))}">
								<span class="iconfont liujiaoxing">&#xe62c;</span>
								{pigcms{$vo.news_title}
								<span class="data">[{pigcms{$vo.add_time|date='Y-m-d',###}]</span>
							</a>
						</li>
						</volist>


					</ul>
					<a href="{pigcms{:U('News/index',array('cat_key'=>'daikuan_notice'))}" class="more text-center">查看更多网站动态</a>
				</div>
			</div>
		</div>
	</section>

	<include file="Public:footer"/>

</div>
</body>

</html>
