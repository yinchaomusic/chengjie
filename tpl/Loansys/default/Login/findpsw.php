<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>注册 - {pigcms{$config.site_name}</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<include file="Public:headersrc"/>
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link type="text/css" href="{pigcms{$static_path}css/register.css" rel="stylesheet"/>

	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<!--	<script type="text/javascript">if(self!=top){window.top.location.href = "{pigcms{:U('Login/index')}";}</script>-->
</head>
<body class="s-bg-global">

<include file="Public:nav"/>




<div class="g-content u-cls">
	<div class="m-content">
		<div class="m-title-a u_be9 s-bg-white u_mb6">
			<a href="javascript:void(0);">
				<span class="u-fl s-05">找回密码</span>
			</a>
		</div>
		<!-- 流程 -->
		<div class="m-process3 s-bg-fc u_be9">
			<span class="f-img-p1 s-2a">输入账户</span>
			<span class="f-img-p5"></span>
			<span class="f-img-p2">验证身份</span>
			<span class="f-img-p5"></span>
			<span class="f-img-p3">重置密码</span>
			<span class="f-img-p5"></span>
			<span class="f-img-p4">完成</span>
			<div class="u-cls"></div>
		</div>
		<!--提示  -->
		<div class="m-insf s-bg-fc" style="width:946px;">
			<p class="u_fw">绑定邮箱后，您可以找回密码和邮箱登录</p>
		</div>
		<!--数据  -->
		<form id="first_step" method="post">
			<div class="m-finddata s-bg-fc u_be9">
				<ul>
					<li class="u_mb30">
						<div class="finddata-txt">账户：</div>
						<div class="finddata-ipt s-bg-white">
							<input type="text" id="email" name="email" ph="请输入您绑定的邮箱" style="color: rgb(216, 216, 216);">
						</div>
						<div class="u-cls"></div>
					</li>
					<li class="u_mb30">
						<div class="finddata-txt">验证码：</div>
						<div class="finddata-ipt1 s-bg-white"><input type="text" id="findpass_verify" name="verify" ph="输入验证码" style="color: rgb(216, 216, 216);"></div>
						<div class="finddata_yzm">

							<img src="{pigcms{:U('Login/verify',array('type'=>'findpass'))}" id="findpass_verifyImg" style="width:95px;height:38px;cursor:pointer;" onclick="reg_fleshVerify('{pigcms{:U('Login/verify',array('type'=>'findpass'))}')" title="刷新验证码" alt="刷新验证码"/>
						</div>
						<div class="u-cls"></div>
					</li>
					<li>
						<div class="u_mall13">
							<input type="submit" value="下一步" class="u-btn12 s-bg-2a">
						</div>
					</li>
				</ul>
			</div>
		</form>
	</div>
</div>



<include file="Public:footer"/>
<script type="text/javascript">
	var static_public="{pigcms{$static_public}",static_path="{pigcms{$static_path}",first_check="{pigcms{:U('Login/first_check')}",secondstep_url="{pigcms{:U('Login/secondset')}";
</script>
<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}login/findpass.js"></script>

</body>
</html>