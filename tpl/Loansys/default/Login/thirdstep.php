<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>注册 - {pigcms{$config.site_name}</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<include file="Public:headersrc"/>
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link type="text/css" href="{pigcms{$static_path}css/register.css" rel="stylesheet"/>

	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<!--	<script type="text/javascript">if(self!=top){window.top.location.href = "{pigcms{:U('Login/index')}";}</script>-->
</head>
<body class="s-bg-global">

<include file="Public:nav"/>



<div class="g-content u-cls">
	<div class="m-content">
		<div class="m-title-a u_be9 s-bg-white u_mb6">
			<a href="javascript:void(0);">
				<span class="u-fl s-05">找回密码</span>
			</a>
		</div>
		<!-- 流程 -->
		<div class="m-process3 s-bg-fc u_be9">
			<span class="f-img-p7 s-2a">输入账户</span>
			<span class="f-img-p6"></span>
			<span class="f-img-p8 s-2a">验证身份</span>
			<span class="f-img-p6"></span>
			<span class="f-img-p9 s-2a">重置密码</span>
			<span class="f-img-p5"></span>
			<span class="f-img-p4">完成</span>
			<div class="u-cls"></div>
		</div>
		<!--提示  -->
		<div class="m-insf s-bg-fc" style="width:946px;">
			<p class="u_fw">绑定邮箱后，您可以找回密码和邮箱登录</p>
		</div>
		<!--数据  -->
		<form id="third_check" method="post">
			<div class="m-finddata s-bg-fc u_be9">
				<ul>
					<li>
						<div class="finddata-txt">设置新密码：</div>
						<div class="finddata-ipt s-bg-white" id="pwd">
							<input type="password" name="txtnewpwd" id="txtnewpwd" maxlength="20">
						</div>
						<div></div>
						<div class="u-cls"></div>
					</li>

					<li class="u_mb30">
						<div class="finddata-txt">确认新密码：</div>
						<div class="finddata-ipt s-bg-white">
							<input type="password"  id="repeatpwd" name="repeatpwd" maxlength="20">
						</div>
						<div></div>
						<div class="u-cls"></div>
					</li>
					<li>
						<div class="u_mall13">
							<input type="submit" value="下一步" class="u-btn12 s-bg-2a">
						</div>
					</li>
				</ul>
			</div>
		</form>
	</div>
</div>


<include file="Public:footer"/>
<script type="text/javascript">
	var static_public="{pigcms{$static_public}",static_path="{pigcms{$static_path}",third_check_post="{pigcms{:U('Login/third_check')}",fourthstep_url="{pigcms{:U('Login/fourthstep')}";
</script>
<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}login/findpass.js"></script>

</body>
</html>