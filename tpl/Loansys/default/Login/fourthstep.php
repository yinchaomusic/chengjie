<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>注册 - {pigcms{$config.site_name}</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<include file="Public:headersrc"/>
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link type="text/css" href="{pigcms{$static_path}css/register.css" rel="stylesheet"/>

	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
</head>
<body class="s-bg-global">

<include file="Public:nav"/>


<div class="g-content u-cls">
	<div class="m-content">
		<div class="m-title-a u_be9 s-bg-white u_mb6">
			<a href="javascript:void(0);">
				<span class="u-fl s-05">找回密码</span>
			</a>
		</div>
		<!-- 流程 -->
		<div class="m-process3 s-bg-fc u_be9">
			<span class="f-img-p7 s-2a">输入账户</span>
			<span class="f-img-p6"></span>
			<span class="f-img-p8">验证身份</span>
			<span class="f-img-p6"></span>
			<span class="f-img-p9">重置密码</span>
			<span class="f-img-p6"></span>
			<span class="f-img-p10">完成</span>
			<div class="u-cls"></div>
		</div>
		<!--提示  -->
		<div class="m-insf s-bg-fc" style="width: 946px;">
			<p class="u_fw">绑定邮箱后，您可以找回密码和邮箱登录</p>
		</div>
		<!--数据  -->
		<div class="m-finddata1 s-bg-fc u_be9">
			<p class="u-setsucess">恭喜您新密码设置成功</p>
			<p class="u-settxt">请牢记您的新设置的密码</p>
			<p class="u-settxt"><a href="/" class="s-2a">返回首页</a>  &nbsp;&nbsp;&nbsp;&nbsp;<a href="{pigcms{:U('Login/index')}" class="s-2a">马上登陆</a>
			</p>
		</div>
	</div>
</div>


<include file="Public:footer"/>

</body>
</html>