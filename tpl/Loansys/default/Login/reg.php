<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>注册 - {pigcms{$config.site_name}</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<include file="Public:headersrc"/>
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link type="text/css" href="{pigcms{$static_path}css/register.css" rel="stylesheet"/>
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />

</head>
<body class="s-bg-global">
	<include file="Public:header"/>
	<include file="Public:nav"/>

	<div class="g-content u-cls u_ptb18">
		<div class="m-content2">
			<div class="m-title2 s-bg-fb">
				<div class="u-fl ut2_l">
					<span class="s-3c">会员注册</span>
				</div>
				<div class="u-fr ut2_r">
					已有账户、现在就<a href="{pigcms{:U('Login/index')}" class="s-2a u-abtn5">登录</a>
				</div>
				<div class="u-cls"></div>
			</div>
			<form id="reg_form" method="post">
				<div class="m-reg s-bg-white" id="reg_form">
					<ul>
						<li>
							<span class="u-txr">邮箱：</span>
							<span id='showemail' style="color:red;">*</span>
							<span class="u-ipt-e">
								<input type="text" ph="请输入正确的邮箱帐号~"  name="email" id="reg_email"  onblur="checkemailValue(this)" />
							</span>
						</li>
						<li>
							<span class="u-txr">密码：</span>
							<span id="showpwd" style="color:red;">*</span>
							<span class="u-ipt-e">
								<input type="password" maxlength="20" name="pwd" id="reg_pwd" onblur="checkpwdValue(this)"/>
							</span>
						</li>
						<li>
							<span class="u-txr">真实姓名：</span>
							<span id="showname" style="color:red;">*</span>
							<span class="u-ipt-e">
								<input type="text" name="nickname" id="reg_name"  onblur="checkNameValue(this)"/>
							</span>
							<span></span>
						</li>
						<li>
							<span class="u-txr">手机号码：</span>
							<span id="showphone" style="color:red;">*</span>
							<span style="color:red;">&nbsp;</span>
							<span class="u-ipt-e">
								<input type="text"  name="phone" id="reg_phone"  onblur="checkPhoneValue(this)"/>
							</span>
							<span></span>
						</li>
						<li>
							<span class="u-txr">QQ：</span>
							<span style="color:red;">&nbsp;</span>
							<span class="u-ipt-e">
								<input type="text"  name="qq"/>
							</span>
							<span></span>
						</li>
						<li class="tr_yzm">
							<span class="u-txr">验证码：</span>
							<span style="color: red;">*</span>
							<span class="u-ipt-f">
								<input type="text" id="reg_verify" style="width:97px;" maxlength="4" name="verify"/>
							</span>
							<span class="u_span2">
								<img src="{pigcms{:U('Login/verify',array('type'=>'reg'))}" id="reg_verifyImg" style="width:95px;height:38px;cursor:pointer;" onclick="reg_fleshVerify('{pigcms{:U('Login/verify',array('type'=>'reg'))}')" title="刷新验证码" alt="刷新验证码"/>
							</span>
							<span id="yzm1"></span>
						</li>

						<li>
							<input type="submit" value="立即注册" style="margin-left: 138px;" class="s-bg-2a u-btn5"/>

						</li>
					</ul>
				</div>
			</form>
		</div>
	</div>
	<include file="Public:footer"/>

<script type="text/javascript">
<!--onblur失去焦点事件-->
function checkemailValue(obj) {
      if (obj.value == "" || obj.value.length < 1 || !/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/.test($('#reg_email').val()) ) {
				var html = '';
				html = "<img src='{pigcms{$static_path}login/img/error.gif' width='20px' height='20px'>";
				html += '<font color=red>'
				+ '请输入正确的邮箱！'
				+ '</font>';
				$("#showemail").empty().append(html);
				$('#reg_email').focus();

      }else {
				var html = '';
					html = "<img src='{pigcms{$static_path}login/img/ok.gif' width='20px' height='20px'>";
				$("#showemail").html(html);
				$('#reg_pwd').focus();
      }
  }

	function checkpwdValue(obj) {
		if (obj.value == "" ) {
			var html = '';
			html = "<img src='{pigcms{$static_path}login/img/error.gif' width='20px' height='20px'>";
			html += '<font color=red>'
			+ '密码不可以为空！'
			+ '</font>';
			$("#showpwd").empty().append(html);
			$('#reg_email').focus();
		}else if ( obj.value.length > 20) {
			var html = '';
			html = "<img src='{pigcms{$static_path}login/img/error.gif' width='20px' height='20px'>";
			html += '<font color=red>'
			+ '密码长度不可以超过20个字符！'
			+ '</font>';
			$("#showpwd").empty().append(html);
			$('#reg_email').focus();
		}else {
			var html = '';
				html = "<img src='{pigcms{$static_path}login/img/ok.gif' width='20px' height='20px'>";
			$("#showpwd").html(html);
			$('#reg_name').focus();
		}
	}

function checkNameValue(obj){
	if (obj.value == "" || obj.value.length < 1 || obj.value.length > 20 ) {
		var html = '';
		html = "<img src='{pigcms{$static_path}login/img/error.gif' width='20px' height='20px'>";
		html += '<font color=red>'
		+ '请输入姓名！'
		+ '</font>';
		$("#showname").empty().append(html);
		$('#reg_name').focus();
	}else {
		var html = '';
			html = "<img src='{pigcms{$static_path}login/img/ok.gif' width='20px' height='20px'>";
		$("#showname").html(html);
		$('#reg_phone').focus();
	}
}

function checkPhoneValue(obj){
	if (obj.value == "" || obj.value.length < 1 ||  !/^[0-9]{11}$/.test($('#reg_phone').val()) ) {
		var html = '';
		html = "<img src='{pigcms{$static_path}login/img/error.gif' width='20px' height='20px'>";
		html += '<font color=red>'
		+ '请输入正确的手机号'
		+ '</font>';
		$("#showphone").empty().append(html);
		$('#reg_phone').focus();
	}else {
		var html = '';
			html = "<img src='{pigcms{$static_path}login/img/ok.gif' width='20px' height='20px'>";
		$("#showphone").html(html);
		$('#reg_qq').focus();
	}
}



</script>

	<script type="text/javascript">
		var static_public="{pigcms{$static_public}",static_path="{pigcms{$static_path}",login_check="{pigcms{:U('Login/check')}",reg_check="{pigcms{:U('Login/reg_check')}",domain_index="{pigcms{$config.site_url}",domain_login="{pigcms{:U('Login/index')}";
	</script>
	<script type="text/javascript" src="{pigcms{$static_path}login/login.js"></script>

</body>
</html>
