﻿var formSubmitSuccess;
var formSubmitBefore;
var sendCommandSuccess;
var sendCommandBefore;
var getSuccess;
var getBefore;
/*
 * 智能机浏览器版本信息:
 *
 */

var $Mbrowser = {
    versions: function () {
        var u = navigator.userAgent, app = navigator.appVersion;
        return {//移动终端浏览器版本信息
            trident: u.indexOf('Trident') > -1, //IE内核
            presto: u.indexOf('Presto') > -1, //opera内核
            webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
            gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
            mobile: !!u.match(/AppleWebKit.*Mobile.*/) || !!u.match(/AppleWebKit/), //是否为移动终端
            ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
            android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
            iPhone: u.indexOf('iPhone') > -1 || u.indexOf('Mac') > -1, //是否为iPhone或者QQHD浏览器
            iPad: u.indexOf('iPad') > -1, //是否iPad
            webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
            Chrome: u.indexOf('Chrome') > -1,
            XiaoMi: u.indexOf('XiaoMi') > -1//小米
        };
    }(),
    language: (navigator.browserLanguage || navigator.language).toLowerCase()
};
function NoToChinese(num) {

    if (!/^\d*(\.\d*)?$/.test(num)) { alert("Number is wrong!"); return "Number is wrong!"; }
    var AA = new Array("零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖");
    var BB = new Array("", "拾", "佰", "仟", "万", "亿", "点", "");
    var a = ("" + num).replace(/(^0*)/g, "").split("."), k = 0, re = "";
    for (var i = a[0].length - 1; i >= 0; i--) {
        switch (k) {
            case 0: re = BB[7] + re; break;
            case 4: if (!new RegExp("0{4}\\d{" + (a[0].length - i - 1) + "}$").test(a[0]))
                re = BB[4] + re; break;
            case 8: re = BB[5] + re; BB[7] = BB[5]; k = 0; break;
        }
        if (k % 4 == 2 && a[0].charAt(i + 2) != 0 && a[0].charAt(i + 1) == 0) re = AA[0] + re;
        if (a[0].charAt(i) != 0) re = AA[a[0].charAt(i)] + BB[k % 4] + re; k++;
    }

    if (a.length > 1) //加上小数部分(如果有小数部分)
    {
        re += BB[6];
        for (var i = 0; i < a[1].length; i++) re += AA[a[1].charAt(i)];
    }
    return re;
}
/*
function formSubmit(form) {
    var $form = $("form");
    var url = $form.attr("action");
    var submitButtons = $form.find(":submit");
    var hiddens = $form.find(":hidden");
    var texts = $form.find(":text,input[type=date],input[type=number],input[type=tel],input[type=email]");
    var textareas = $form.find("textarea");
    var passwords = $form.find(":password");
    var radios = $form.find(":radio:checked");
    var checkboxs = $form.find(":checkbox:checked");
    var selects = $form.find("select");

    var data = {};

    //hiddens
    var hiddenItems = {};
    for (var i = 0; i < hiddens.length; i++) {
        var name = hiddens.eq(i).attr("name");
        var values = hiddenItems[name];
        if (!values) {
            values = [];
        }
        values.push(hiddens.eq(i).val());
        hiddenItems[name] = values;
    }
    for (var item in hiddenItems) {
        data[item] = hiddenItems[item].join(',');
    }
    //texts
    var textItems = {};
    for (var i = 0; i < texts.length; i++) {
        var name = texts.eq(i).attr("name");
        var values = textItems[name];
        if (!values) {
            values = [];
        }
        values.push(texts.eq(i).val());
        textItems[name] = values;
    }
    for (var item in textItems) {
        data[item] = textItems[item].join(',');
    }
    //textareas
    for (var i = 0; i < textareas.length; i++) {
        data[textareas.eq(i).attr("name")] = textareas.eq(i).val();
    }
    //passwords
    for (var i = 0; i < passwords.length; i++) {
        data[passwords.eq(i).attr("name")] = passwords.eq(i).val();
    }
    //radios
    for (var i = 0; i < radios.length; i++) {
        data[radios.eq(i).attr("name")] = radios.eq(i).val();
    }
    //selects
    for (var i = 0; i < selects.length; i++) {
        var selected = selects.eq(i).find(":selected");
        var selectedValue = [];
        for (var j = 0; j < selected.length; j++) {
            selectedValue.push(selected.eq(j).val());
        }
        data[selects.eq(i).attr("name")] = selectedValue.join(",");
    }
    //checkboxs
    var checkboxItems = {};
    for (var i = 0; i < checkboxs.length; i++) {
        var name = checkboxs.eq(i).attr("name");
        var values = checkboxItems[name];
        if (!values) {
            values = [];
        }
        values.push(checkboxs.eq(i).val());
        checkboxItems[name] = values;
    }
    for (var item in checkboxItems) {
        data[item] = checkboxItems[item].join(',');
    }

    if (formSubmitBefore) {
        var isBrack = formSubmitBefore(url, data);
        if (!isBrack)
            return false;
    }

    submitButtons.attr("disabled", "true");
    $.post(url,
        data,
        function (result) {
            if (result) {
                if (result.State == -1) {
                    alert(result.Error);
                    submitButtons.removeAttr("disabled");
                    return;
                }
            }
            if (formSubmitSuccess) {
                formSubmitSuccess($form, result);
            } else {
                window.location = window.location;
            }
        });
    return false;
}
*/
function sendCommand(url, params, tip) {

    if (sendCommandBefore) {
        var isBrack = sendCommandBefore(url, params);
        if (!isBrack)
            return false;
    }

    if (tip) {
        var cf = confirm(tip);
        if (!cf)
            return false;
    }

    $.post(url,
        params,
        function (result) {
            if (result) {
                if (result.State == -1) {
                    alert(result.Error);
                    return;
                }
            }
            if (sendCommandSuccess) {
                sendCommandSuccess(url, params, result);
            } else {
                window.location = window.location;
            }
        });
}

function get(url, params, call) {

    if (getBefore) {
        var isBrack = getBefore(url, params);
        if (!isBrack)
            return false;
    }

    $.get(url,
        params,
        function (result) {
            var called = false;
            if (call) {
                called = true;
                call(result);
            } else if (result.State == -1) {
                called = true;
                alert(result.Error);
                return;
            } else if (getSuccess) {
                called = true;
                getSuccess(url, params, result);
            }

            if (!called) {
                window.location = window.location;
            }
        });
}

/*
function region(container, value, selectedcall) {
    var $container = $("#" + container);

    var change = function (select) {
        var option = select.find("option:selected");
        var isend = option.attr("isend") == "true";
        var tag = option.attr("tag") * 1;
        var id = option.val();
        var name = option.text();
        if (id != -1) {
            if (!isend) {
                getSelect(tag, id);
            } else {
                selectedcall(id, name);
            }
        } else {
            $("#region_p" + tag).remove();
            $("#region_p" + (tag + 1)).remove();
            $("#region_p" + (tag + 2)).remove();
            selectedcall(-1, "");
        }
    }

    var getSelect = function (tag, parent) {
        var select;
        select = $("#region_p" + tag);
        $("#region_p" + (tag + 1)).remove();
        $("#region_p" + (tag + 2)).remove();
        if (select.length <= 0) {
            select = $("<select id='region_p" + tag + "' class='form-control'></select>");
            select.change(function () {
                change($(this));
            });
        }
        select.empty();
        var uu = "{pigcms{:U('Fund/getData')}";
        get(uu, { parent: parent }, function (result) {
            selectedcall(-1, "");
            if (result.State == -1) {
                select = $("<span>获取失败</span>");
            } else {
                var option = $("<option></option>");
                option.val(-1);
                option.text("选择");
                option.attr("tag", tag + 1);
                select.append(option);
                for (var i = 0; i < result.length; i++) {
                    var option = $("<option></option>");
                    option.val(result[i].Id);
                    option.text(result[i].Name);
                    option.attr("isend", result[i].IsEnd);
                    option.attr("tag", tag + 1);

                    if (value.substr(0, 2 * (tag * 1 + 1)) == result[i].Id) {
                        option.attr("selected", true);
                        select.append(option);
                        change(select);
                    } else {
                        select.append(option);
                    }
                }
            }
            $container.append(select);
        });
    }

    getSelect(0, 0);
}
*/

function formatPrice(number, places, symbol, thousand, decimal) {
    number = number || 0;
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    symbol = symbol !== undefined ? symbol : "$";
    thousand = thousand || ",";
    decimal = decimal || ".";
    var negative = number < 0 ? "-" : "",
        i = parseInt(number = (parseInt(Math.abs(+number || 0) * 100) / 100).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
}

function showTips(id,obj,str){
    // console.log("str:"+str +"typeof:"+typeof( str));
    if( typeof(str) == "string" ){
        var htmls,$this=obj,popoverId="popover"+id;
        //console.log(popoverId);
        if($("#"+popoverId).size() > 0){ $("#"+popoverId).show();
        }else{
            var objW=obj.innerWidth(),
                objH=obj.innerHeight();
            htmls='' +
                '<div class="popover fade top in popover-tips " role="tooltip" id="'+popoverId+'" style="display none;  text-align: center; min-width:100px; min-height:30px;">'+/*min-width:270px; min-height:30px;*/
                '<div class="arrow" style="left: 50%;"></div>'+
                    // '<h3 class="popover-title" style="display: none;"></h3>'+
                '<div class="popover-content">'+str+
                '</div>'+
                '</div>';
            $(htmls).appendTo("body");
            var $pop=$("#"+popoverId),
                $popW=$pop.width(),$popH=$pop.height();
            popY= obj.offset().left + parseInt(objW/2)-parseInt($popW/2) -10;
            popX=obj.offset().top - parseInt($popH) -10;
            $("#"+popoverId).css({"top":popX,"left":popY}).show();
        }
    }
}
$(function () {
	 $(".contact_right").hover(function () {
        $("body").addClass("show_contact");
        $(".contact_right").stop().animate({
            right: "0"          
        }, 400);
        $(".diyoumask").stop().animate({
            opacity: "0.3"
        }, 400);
    }, function () {
        $(".contact_right").stop().animate({
            right: "-250"
        }, 400);
        $(".diyoumask").stop().animate({
            opacity: "0"
        }, 400);
        $("body").removeClass("show_contact");

    });
    /* hover 提示*/
    $(".text-tips-af.hover").hover(function () {
        showTips($(".text-tips-af.hover").index(this),$(this),$(this).attr("data-info"))
    },function(){
        var popoverId="#popover"+$(".text-tips-af.hover").index(this);
        $(popoverId).hide();
    });
    // 数字
    $("input[type='number']:not(.Decimal)").on(
        {
            mousewheel: function (event) {
                event.preventDefault();
            }
        ,
            keyup: function () {
                $(this).val($(this).val().replace(/\D|(^0+)/g, ""));
               
            }
        }
    );
    // 数字小数点
    var renumber = /\.\d{2,}$/;
    $("input[type='number'].Decimal").on(
        {
        mousewheel: function (event) {
                        event.preventDefault();
        }
        ,
        keyup: function () {
                $(this).val($(this).val().replace(/[^\d\.]|(\.{2,})|(^\.+)/g, ""));
        }
        ,
        keydown: function (event) {
            if (renumber.test($(this).val())) {
                if (event.keyCode == 8) return true
                return false
            }  
           
        }

        }
    );
}
);

function query(keywordId, url) {
    var keyword = $("#" + keywordId);
    if (url) {
        if (url.indexOf('?') > -1) {
            url += "&query=" + keyword.val();
        } else {
            url += "?query=" + keyword.val();
        }
    } else {
        url = "?query=" + keyword.val();
    }
    window.location = url;
}

