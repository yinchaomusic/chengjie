$(document).ready(function () {
    //搜索提示默认隐藏掉
    $(".serch_ts").css("display", "none");
    back_top_bottom();
    //返回顶部，返回底部
    function back_top_bottom() {
        $(".footer").after(("<div class='wx_tb' id='wx_tb'><ul><li title='返回顶部' class='top'></sli><li class='contact_us' title='联系我们'>" +
        "<a href='/index.php?c=News&a=aboutus&page=contact'  target='_blank'>联系我们</a></li><li class='down' title='去顶部'></li></ul></div><div class='wx_img'></div>"));
        var lis = $("#wx_tb ul li");
        for (var i = 0; i < lis.length; i++) {
            lis[i].index = i;
            $(lis[i]).click(function () {
                if (this.index == 0) {
                    $("html, body").animate({ scrollTop: 0 }, 120);
                } else {
                    var windowHeight = parseInt($("body").css("height"));
                    $("html,body").animate({ "scrollTop": windowHeight }, "slow");
                }
            });
        }
    }
    function showload() {
        var lazyheight = parseFloat($(window).height()) + parseFloat($(window).scrollTop());
        if ($(document).height() - 100 <= lazyheight) {
            $("#wx_tb ul li").eq(0).show("fast");
            $("#wx_tb ul li").eq(2).hide("fast");
        } else {
            $("#wx_tb ul li").eq(2).show("fast");
            $("#wx_tb ul li").eq(0).hide("fast");
        }
    }
    //绑定事件
    $(window).bind("scroll", function () {
        //当滚动条滚动时
        showload();
    });
    //默认选中Whois查询
    $("#ser_tj li").click(function () {
        $("#ser_tj li").find("input").removeClass("t");
        $("input[idx]").attr("flag", false);
        $(this).find("input").addClass("t");
        $(this).find("input").attr("flag", true);
        var num = $(this).find("input").attr("idx");
        if (parseInt(num) == 1) {
            $("#ser_text").attr("placeholder", "请输入域名,邮箱,注册人");
        } else {
            $("#ser_text").attr("placeholder", "请输入域名");
        }
    })
    //提交查询
    var arr_radio = [];
    $(".btn_ser").click(function () {
        tiaozhuan();
    });
    // 搜索框获得
    $(".ser_text").focus(function () {
        var text_val = this.value;
        if (text_val && text_val == "") {
            $(this).val("");
        }
        if ($(".serch_ts>p").length > 0) {
            $(".serch_ts").css("display", "block");
        }
    });
    // 搜索框失去
    $(".ser_text").focusout(function () {
        $(".serch_ts").css("display", "none");
    });
    var searchValue = "";
    function getSearchList(keyword) {
        var text_val = keyword;
        if (text_val == "" || text_val == "填写域名信息,我们帮你评估") return;
        var arr_hz = [".com", ".com.cn", ".net", ".cn"];
        var prefix = text_val; //前缀
        if (text_val.match(/(.+?)\./) != null)
            prefix = text_val.match(/(.+?)\./)[1];
        var suffix = ".";      //后缀
        if (text_val.match(/.+?(\..+)/) != null)
            suffix = text_val.match(/.+?(\..+)/)[1];
        var result = "";
        for (var i = 0; i < arr_hz.length; i++) {
            if (arr_hz[i].indexOf(suffix) > -1)
                result += "<p>" + prefix + arr_hz[i] + "</p>";
        }
        $(".serch_ts").html(result);
        $(".serch_ts").css("display", "block");
    }
    // 提示信息的鼠标悬浮事件
    $(".serch_ts>p").live("mouseover", function () {
        changeClass($(this));
    });
    // 提示信息的鼠标按下事件
    $(".serch_ts>p").live("mousedown", function () {
        $(".ser_text").val($(this).text());
    });
    // 搜索框的keyup事件
    $("#ser_text").keyup(function (event) {
        var sum = $(".serch_ts p").length;
        switch (event.keyCode) {
            case 40: { // ↓
                var idx = $(".serch_ts>p[class='t']").index();
                ++idx;
                if (idx == sum) {
                    $(".ser_text").val((this.value).split(".")[0]);
                    $(".serch_ts>p").eq(idx - 1).removeClass("t");
                    idx = -1; ++idx;
                } else if (idx != sum) {
                    if (idx != sum) {
                        var obj = $(".serch_ts>p").eq(idx);
                        $(".ser_text").val(obj.text());
                        changeClass(obj);
                    }
                }
                break;
            }
            case 38: { // ↑
                var idx = $(".serch_ts>p[class='t']").index();
                if (idx == -1) {
                    idx = sum;
                }
                --idx;
                if (idx == -1) {
                    $(".ser_text").val((this.value).split(".")[0]);
                    $(".serch_ts>p").eq(idx + 1).removeClass("t");
                } else {
                    var obj = $(".serch_ts>p").eq(idx);
                    changeClass(obj);
                    $(".ser_text").val(obj.text());
                }
                break;
            }
            case 13: {// Enter
                tiaozhuan();
                break;
            }
            default: {// 其它按键
                if (this.value != searchValue && this.value != "") {
                    getSearchList(this.value);
                    $("#serch_ts").css("display", "block");
                }
                searchValue = this.value;
            }
        }
    });
    //切换背景色 obj是jquery对象
    function changeClass(obj) {
        $(".serch_ts>p").removeClass("t");
        obj.addClass("t");
    }
    //扩展数组的Array.remove方法根据传回来的下表删除当前数组中的元素
    Array.prototype.xb_remove = function (dx) {
        dx = parseInt(dx);
        if (isNaN(dx) || dx > this.length) { return; }
        this.splice(dx, 1);
    }
    //扩展String的trim方法
    String.prototype.trim = function () {
        return this.replace(/\s+/g, "");
    }

    function tiaozhuan() {
        var ul = $(".btn_radio[flag=true]").attr("ul");
        var ser_text = $(".ser_text").val().trim();
        if (ser_text.charAt(0) == "." || ser_text.charAt(ser_text.length - 1) == ".") {
            var arr_sertext=ser_text.split(".");
            for (i in arr_sertext) {
                if (arr_sertext[i] == "") {
                    arr_sertext.xb_remove(i);
                }
            }
           ser_text=arr_sertext.join();
        }
        //有值没值都可以跳转
        if (ser_text && ser_text != "") {
            if (ul == "whois") {
                if (ser_text.indexOf(".") == "-1") { ser_text = ser_text + ".com" };
                if (ser_text.indexOf("@") != "-1") { $("#ser_text").val(""); return false;};
                location.href = './index.php?c=Whois&a=index&keyword=' + ser_text;
                $("#ser_text").attr("placeholder", "请输入域名");
            } else if (ul == "reverse") {
                location.href = './index.php?c=Whois&a=reverse&type=all&keyword=' + ser_text;
                $("#ser_text").attr("placeholder", "请输入域名,邮箱，注册人");
            } else if (ul == "history") {
                if (ser_text.indexOf(".") == "-1") { ser_text = ser_text + ".com" };
                if (ser_text.indexOf("@") != "-1") { $("#ser_text").val(""); return false; };

                location.href = './index.php?c=Whois&a=history&keyword=' + ser_text;
                $("#ser_text").attr("placeholder", "请输入域名");
            } else if (ul == "0") {
                if (ser_text.indexOf(".") == "-1") { ser_text = ser_text + ".com" };
                if (ser_text.indexOf("@") != "-1") { $("#ser_text").val(""); return false; };
                var a = /^[0-9a-zA-Z\.]+$/;
                if (!a.test(ser_text)) {
                    alert("暂不支持该域名的评估");
                    return;
                } else {
                  alert("暂不开放域名评估，请耐心等待哦。");return ;
                    window.open("./index.php?c=Whois&a=gj&keyword=" + ser_text+"#maodian","_blank");
                }
            } else if (ul == "alexa") {
                if (ser_text.indexOf(".") == "-1") { ser_text = ser_text + ".com" };
                if (ser_text.indexOf("@") != "-1") { $("#ser_text").val(""); return false; };
                location.href = ul + "/" + ser_text;
            }

        } else {
            return false;
        }
    }
});
