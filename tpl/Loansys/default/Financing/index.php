<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">



</head>
<body class="s-bg-global">

<include file="Public:nav"/>

<div class="g-content u-cls">
	<div class="m-content">

		<section>
			<div class="container nrye">
				<div class="row">

					<include file="Public:nav_left"/>

					<div class="col-sm-10">
						<div class="container_page user_page_area">
							<section>
								<h4><span>{pigcms{$user_session.nickname} [ID:{pigcms{$user_session.uid}]</span>，欢迎来到域名融资平台！</h4>
								<div class="user_input_box clearfix">
									<dl>
										<dt>授信总额：</dt>
										<dd>
											<a href="{pigcms{:U('Loan/pledge')}" class="a">￥{pigcms{$pledges.credit_all}</a>
											<if condition="$result_idcard['status'] neq 1">
											<a href="{pigcms{:U('Profile/IdCard')}" class="btn btn-default btn-sm">申请授信</a>
											</if>
										</dd>
										<dt>可用授信：</dt>
										<dd><a href="{pigcms{:U('Loan/borrowList')}" class="a">￥{pigcms{$pledges['credit_now']}</a></dd>
										<dt>借款总额：</dt>
										<dd><a href="{pigcms{:U('Loan/borrowList')}" class="a">￥{pigcms{$pledges['brrowing_total']}</a></dd>
										<dt>待还总额：</dt>
										<dd><a href="{pigcms{:U('Loan/borrowList',array('state'=>2))}" class="a">￥{pigcms{$pledges.stay_total}</a></dd>
										<dt>借款荣誉：</dt>
										<dd><a
												<if condition="$pledges['honor'] eq 2">class="level level_2"
												<elseif condition="$pledges['honor'] eq 3"/>class="level level_3"
												<else/>class="level level_1"
											</if> href="{pigcms{:U('Financing/level')}">

												<if condition="$pledges['honor'] eq 2">钻石会员
												<elseif condition="$pledges['honor'] eq 3"/>皇冠会员
													<else/>普通会员
												</if>
											</a></dd>
									</dl>
									<dl>
										<dt>账户总额：</dt>
										<dd><a href="{pigcms{:U('Domain/Account/record')}" class="a">￥{pigcms{$user_session['now_money']+$user_session['freeze_money']}</a></dd>
										<dt>冻结金额：</dt>
										<dd><a href="{pigcms{:U('Domain/Account/frozendetail')}" class="a">￥{pigcms{:number_format
												($user_session['freeze_money'],2)}</a></dd>
										<dt>可用余额：</dt>
										<dd>
											<span class="span">￥{pigcms{:number_format($user_session['now_money'],2)}</span>
											<a href="{pigcms{:U('Domain/Account/recharge')}" class="btn btn-default btn-sm">充值</a>
											<a href="{pigcms{:U('Domain/Account/withdrawal')}" class="btn btn-default btn-sm">提现</a>
										</dd>
										<dt>借出总额：</dt>
										<dd><a href="{pigcms{:U('Investment/lendList',array('state'=>2))}" class="a">￥{pigcms{$pledges.lend_total}</a></dd>
										<dt>理财荣誉：</dt>
										<dd><a
												<if condition="$pledges['honor'] eq 2">class="level level_2"
												<elseif condition="$pledges['honor'] eq 3"/>class="level level_3"
												<else/>class="level level_1"
											</if> href="{pigcms{:U('Financing/level')}">
												<if condition="$pledges['honor'] eq 2">钻石会员
												<elseif condition="$pledges['honor'] eq 3"/>皇冠会员
												<else/>普通会员
											</if></a></dd>
									</dl>
								</div>
								<div class="btn_area">
									<a href="{pigcms{:U('Investment/lend')}" class="btn btn-primary btn-warning">我要理财</a>
									<a href="{pigcms{:U('Loan/borrow')}" class="btn btn-green">我要借款</a>
									<a href="{pigcms{:U('Investment/lendList')}" class="btn btn-default">借出列表</a>
									<a href="{pigcms{:U('Loan/borrowList')}" class="btn btn-default">借入列表</a>
									<a href="{pigcms{:U('Loan/paymentHistory')}" class="btn btn-danger">最近一个月内还需还款记录</a>
								</div>
								<ul class="list-group security">
									<li class="list-group-item clearfix">
										<span class="iconfont">&#xe653;</span>
										<span class="lt">邮箱认证</span>

										<if condition="$user_session['is_check_email']">
										<span class="info">{pigcms{$user_session.email}</span>
										<span class="state">已经认证</span>
										<else />
											<span class="info">更好地核实您的身份，保障您的账户安全</span>
											<a href="{pigcms{:U('Domain/Account/bindEmail')}" class="state">马上认证</a>
										</if>
									</li>
									<li class="list-group-item clearfix">
										<span class="iconfont">&#xe60a;</span>
										<span class="lt">手机认证</span>
										<if condition="$user_session['is_check_phone']">
											<span class="info">{pigcms{$user_session.phone}</span>
											<span class="state">已经认证</span>
										<else />
											<span class="info">更好地核实您的身份，保障您的账户安全</span>
											<a href="{pigcms{:U('Domain/Account/bind')}" class="state">马上认证</a>
										</if>
									</li>
									<li class="list-group-item clearfix">
										<span class="iconfont">&#xe65c;</span>
										<span class="lt">身份认证</span>
										<if condition="$result_idcard['status'] eq 1">
											<span class="info">恭喜您身份证认证成功 </span>
											<span class="state">已经认证</span>
										<else/>
										<span class="info">更好地核实您的身份，保障您的账户安全</span>
										<a href="{pigcms{:U('Profile/IdCard')}" class="state">马上认证</a>
										</if>
									</li>
									<li class="list-group-item clearfix">
										<span class="iconfont">&#xe601;</span>
										<span class="lt">交易密码</span>
										<if condition="$is_set_trade_pass">
											<span class="info">为了保障您的资金安全，建议定期修改交易密码</span>
											<a href="{pigcms{:U('Profile/transactionPassword')}" class="state">我要修改</a>
										<else/>
											<span class="info">您未设置交易密码</span>
											<a href="{pigcms{:U('Profile/transactionPassword')}" class="state">马上设置</a>
										</if>

									</li>
								</ul>
							</section>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
<include file="Public:footer"/>
<include file="Public:footerjs"/>
</body>
</html>