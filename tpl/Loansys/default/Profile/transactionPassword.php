<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">


</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>

<div class="g-content u-cls">
	<div class="m-content">




			<section>
				<div class="container nrye">
					<div class="row">

						<include file="Public:nav_left"/>

						<form action="{pigcms{:U('Profile/transactionPassword')}" method="post" novalidate="true">
						<div class="col-sm-10 ">
							<div class="container_page">
								<section>
									<h2>交易密码</h2>
									<p class="bg-warning">
										为了保障您的资金安全，建议定期修改交易密码，建议您初始交易密码不要设置为类似123456这样简单的数字！
									</p>
									<div class="form-horizontal form-dlmm">
										<div class="form-group">
											<label for="Password" class=" control-label col-sm-1">登录密码： </label>
											<div class="col-sm-4">
												<input class="form-control" id="Password" name="Password" type="password" />
											</div>
											<span class="form-control-tips col-sm-7" style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
										</div>
										<if condition="$istp eq 1">
										<div class="form-group"  >
											<label for="OldTransactionPassword" class=" control-label col-sm-1">原交易密码： </label>
											<div class="col-sm-4">
												<input class="form-control" id="OldTransactionPassword" name="OldTransactionPassword" type="password" />
											</div>
											<span class="form-control-tips col-sm-7" style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
										</div>
										</if>
										<div class="form-group">
											<label for="NewTransactionPassword" class=" control-label col-sm-1">交易密码： </label>
											<div class="col-sm-4">
												<input class="form-control" id="NewTransactionPassword" name="NewTransactionPassword" type="password" />
											</div>
											<span class="form-control-tips col-sm-7" style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
										</div>
										<div class="form-group">
											<label for="NewTransactionPasswordAgain" class=" control-label col-sm-1">确认密码： </label>
											<div class="col-sm-4">
												<input class="form-control" id="NewTransactionPasswordAgain" name="NewTransactionPasswordAgain" type="password" />
											</div>
											<span class="form-control-tips col-sm-7" style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
										</div>
										<div class="btn_area">
											<input type="submit" class="btn btn-warning btn-xs-block" value="提交">
											<a href="{pigcms{:U('Profile/transactionPassword',array('istp'=>1))}">忘记原交易密码？</a>
										</div>
									</div>
								</section>
							</div>
						</div>
						</form>



					</div>
				</div>
			</section>




	</div>
</div>
<include file="Public:footer"/>

<include file="Public:footerjs"/>

<script type="text/javascript">
	formSubmitSuccess = function () {
		alert("修改成功");
		window.location = window.location;
	}
	formSubmitBefore = function () {
		if ($("#NewTransactionPassword").val() != "" && $("#NewTransactionPasswordAgain").val() != $("#NewTransactionPassword").val()) {
			swal('','密码不一致','error');
			return false;
		};
		return true;
	}
	$(function () {

		$("#NewTransactionPasswordAgain").blur(function () {
			if ($("#NewTransactionPassword").val() != "" && $("#NewTransactionPasswordAgain").val() != $("#NewTransactionPassword").val()) {
				swal('','密码不一致','error');
			};
		});

	});

</script>



</body>
</html>