<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">


</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>

<div class="g-content u-cls">
	<div class="m-content">




		<section>
			<div class="container nrye">
				<div class="row">

					<include file="Public:nav_left"/>

					<form action="{pigcms{:U('Profile/bindphone')}" method="post" novalidate="true">
						<div class="col-sm-10 top_layer">
							<div class="container_page ">
								<section>
									<h2>绑定手机</h2>

							<if condition="is_array($get_user_info)">

								<p class="bg-success">
									您的手机已经验证！<br>
									绑定的手机号：{pigcms{$get_user_info.phone}
								</p>

							<else/>
								<p class="bg-warning">
									请认真填写接收验证码的手机号，手机验证不收取任何费用！
								</p>
								<div class="form-horizontal form-bdsj">
									<div class="form-group">
										<label for="MobliePhoneNumber" class=" control-label col-sm-1">手机号： </label>
										<div class="col-sm-3">
											<input class="form-control" id="MobliePhoneNumber" name="phone" type="tel" value="" />
										</div>
										<a href="javascript:;" class="btn btn-default btn-sm pull-left" id="getCode">获取验证码</a>
                                    <span class="form-control-tips col-sm-8" style="display: none"><i class="iconfont">
		                                    &#xe60a;</i>提示文字</span>
									</div>
									<div class="form-group">
										<label for="Code" class=" control-label col-sm-1">验证码： </label>
										<div class="col-sm-2">
											<input class="form-control" id="Code" name="txtcode" type="text" value="" />
										</div>
                                    <span class="form-control-tips col-sm-8" style="display: none"><i class="iconfont">
		                                    &#xe60a;</i>提示文字</span>
									</div>
									<div class="btn_area">
										<input type="submit" class="btn btn-warning btn-xs-block" value="提交">
									</div>
								</div>
								</section>
							</if>
							</div>
						</div>

					</form>



				</div>
			</div>
		</section>




	</div>
</div>
<include file="Public:footer"/>

<include file="Public:footerjs"/>

<script type="text/javascript">
	var s = 0;
	var time = function () {
		if (s == 0) {
			$("#getCode").removeClass("disabled").text("获取验证码");
		} else {
			$("#getCode").text("(" + s + "s)后再获取");
			s--;
			setTimeout(time, 1000);
		}
	}
	formSubmitBefore = function () {
		return true;
	}
	sendCommandSuccess = function (url) {
		var sendUrl = "{pigcms{:U('Profile/sendCode')}";
		if (url == sendUrl) {
			s = 60;
			time();
		} else {
			window.location = window.location;
		}
	}
	$(function () {
		var getCode = $("#getCode");
			getCode.click(function () {
			if (getCode.hasClass("disabled"))
				return;
			var phone = $("#MobliePhoneNumber").val();
			if (/\d{11}/.test(phone)) {
				var sendUrl = "{pigcms{:U('Profile/sendCode')}";
				sendCommand(sendUrl, { phone: phone });
				getCode.addClass("disabled");
				return;
			}
			swal('','请输入正确的手机号码','error');

		});
	});
</script>



</body>
</html>