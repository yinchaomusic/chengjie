<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">


</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>

<div class="g-content u-cls">
	<div class="m-content">




			<section>
				<div class="container nrye">
					<div class="row">
						<include file="Public:nav_left"/>
						<form id="IDcard" action="{pigcms{:U('Profile/IdCard')}"  method="post" enctype="multipart/form-data">
						<div class="col-sm-10 ">
							<div class="container_page  ">
								<section>
									<h2>身份认证</h2>

									<div class="form-default" <if condition="$result_idcard['card_id']"> <else />style="display: none;"</if> >
										<div class="form-group">
											<if condition="$result_idcard['status'] eq 1">
												恭喜您！实名认证申请审核通过。
											<else />
											您的申请已经提交，工作人员会在1-2个工作日内为您处理
											</if>
										</div>
									</div>

									<div class="form-default" <if condition="$result_idcard['card_id']"> style="display: none;"<else /></if>>
										<div class="form-group">
											<label for="input1" class=" control-label">身份证类型：</label>
											<div class="col-sm-3">
<!--												<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>-->
                                            <span class="font_c_grey" style="display: inline-block; vertical-align: middle;">
                                                <img src="{pigcms{$static_path}images/account/sf.gif"><br />
                                                二代身份证
                                            </span>
											</div>
										</div>
										<div class="form-group">
											<label for="input2" class=" control-label">身份证号码：</label>
											<span class="font_c_grey pull-left">(18位)</span>
											<div class="col-sm-4">
											<input class="form-control" id="Number" minlength="18" maxlength="18" name="id_number" required="required" type="text" value="" placeholder="请输入真实的身份证号" />
											</div>

										</div>
										<div class="form-group">
											<label for="input2" class="control-label">身份证照片：</label>
											<div class="col-sm-8 btn-xs-block">
												<span class="font_c_grey pull-left">(正面)</span>
												<input type="file" name="facadeFile" id="facadeFile" class="pull-left">
												<span class="font_c_grey pull-left" style="clear: both">(背面)</span>
												<input type="file" name="reverseSideFile" id="reverseSideFile" class="pull-left">
												<p class="up_info">
													证件必须是清晰彩色原件电子版，可以是扫描件或者是数码相机拍摄照片，不能使用复印件。<br />
													仅支持.jpg .jpge.bmp.png的图片格式。图片大小不超过2M。
												</p>
											</div>
										</div>
									</div>
									<div class="btn_area"  <if condition="$result_idcard['card_id']"> style="display: none;"<else /></if>>
										<input type="submit" class="btn btn-primary btn-warning btn-xs-block" value="提交">
									</div>
								</section>

							</div>
						</div>
						</form>


					</div>
				</div>
			</section>

	</div>
</div>
<include file="Public:footer"/>

<script src="{pigcms{$static_path}js/unslider.js"></script>
<script src="{pigcms{$static_path}js/bootstrap.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/rz_common.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/matrix.js"></script>



</body>
</html>