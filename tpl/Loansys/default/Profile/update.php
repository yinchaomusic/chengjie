<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">



</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>

<div class="g-content u-cls">
	<div class="m-content">

			<section>
				<div class="container nrye">
					<div class="row">

						<include file="Public:nav_left"/>

						<form action="{pigcms{:U('Profile/update')}" method="post" novalidate="true">
						<div class="col-sm-10 top_layer">
							<div class="container_page">
								<section>
									<h2>个人信息</h2>
									<div class="form-horizontal form-info">
										<div class="form-group">
											<label class="control-label col-sm-1">姓名： </label>
											<div class="col-sm-10">
												<p class="form-control-static">{pigcms{$user_session.nickname}</p>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-1">性别： </label>
											<div class="col-sm-3">

												<label class="radio-inline">
													<input id="sex" name="sex" type="radio" <if condition="$user_session['sex'] eq 1">
														checked="checked"</if>	value="1" />
													男
												</label>
												<label class="radio-inline">
													<input id="sex" name="sex" type="radio" <if condition="$user_session['sex'] eq 2">
														checked="checked"</if> value="2" />
													女
												</label>
											</div>
										</div>
										<div class="form-group">
											<label for="input3" class=" control-label col-sm-1">出生日期： </label>
											<div class="col-sm-4">
												<input class="form-control" id="Birthday" name="birthday" type="date" value="{pigcms{$user_session.birthday}" />                                    </div>
											<span class="form-control-tips col-sm-7" style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
										</div>


										<div class="form-group">
											<label for="input4" class=" control-label col-sm-1">手机：</label>
											<div class="col-sm-10">
												<p class="form-control-static">{pigcms{$user_session.phone}</p>
											</div>
										</div>
										<div class="form-group" style="display: none;">
											<label class=" control-label col-sm-1">地区： </label>
											<div class="col-sm-10 select-inline" id="city">
<!--														<select class="prov"></select>-->
<!--														<select class="city" disabled="disabled"></select>-->
<!--														<select class="dist" disabled="disabled"></select>-->
											</div>
											<span class="form-control-tips col-sm-7" style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
										</div>
										<div class="form-group">
											<label for="input6" class=" control-label col-sm-1">地址：</label>
											<div class="col-sm-4">

												<input class="form-control" id="Address" name="youaddress" type="text" value="{pigcms{$user_session.youaddress}" />
											</div>
											<span class="form-control-tips col-sm-7" style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
										</div>
										<div class="form-group">
											<label for="input4" class=" control-label col-sm-1">QQ： </label>
											<div class="col-sm-4">
												<p class="form-control-static">{pigcms{$user_session.qq}</p>
											</div>
											<span class="form-control-tips col-sm-7" style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
										</div>
										<div class="btn_area">

											<input type="submit" class="btn btn-primary btn-warning btn-xs-block" value="提交">
										</div>
									</div>
								</section>
							</div>
						</div>
						</form>

					</div>
				</div>
			</section>




	</div>
</div>
<include file="Public:footer"/>

<include file="Public:footerjs"/>

<script type="text/javascript">



	formSubmitSuccess = function () {
		alert("修改成功");
		window.location = window.location;
	}
	$(function () {

//		$('#city').citySelect({
//			url:"{pigcms{$static_public}js/jquery-city-select/city.min.js",
//			prov:"安徽", //省份
//			city:"合肥", //城市
//			dist:"蜀山区", //区县
//			nodata:"none" //当子集无数据时，隐藏select
//		});


		if ($Mbrowser.versions.ios || $Mbrowser.versions.XiaoMi || ($Mbrowser.versions.mobile && $Mbrowser.versions.Chrome)) {
			/*alert("支持")  */

		} else {
			/*alert("js日历控件")*/
			$("#Birthday").attr("type", "text");
			laydate({
				elem: '#Birthday'
			});


		};
		region("regionContainer", $("#RegionId").val(), function (id, name) {
			$("#RegionId").val(id);
		});
	});
</script>

</body>
</html>