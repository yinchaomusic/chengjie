<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">


</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>

<div class="g-content u-cls">
	<div class="m-content">
				<div class="container nrye">
					<div class="row">
						<include file="Public:nav_left"/>

						<form action="{pigcms{:U('Profile/avatar')}" method="post"   enctype="multipart/form-data"  >
						<div class="col-sm-10 ">
							<div class="container_page  photo-form">
								<section>
									<h2>上传头像</h2>
									<p>
										上传本人真实的、五官清晰的头像照<br />
										1、您得到的关注度超过随机头像的会员的 <strong class="font_c_blue">50%</strong><br />
										2、您会给予借出者更多信任感<br />
										3、您的借款成功率将提高 <strong class="font_c_blue">20%</strong><br />
									</p>
									<div class="user_photo_area ">
										<div class="user_photo">
											<if condition="$user_session['avatar']">
												<image src="upload/avatar/{pigcms{$user_session.avatar}" />
											 <else />
												<image src="{pigcms{$static_path}images/account/default_avatar.png" />
											  </if>

										</div>
										<input type="file" name="avatar" id="file"  />
									</div>
									<div class="btn_area">
										<input type ="submit" id="upfile"  class="btn  btn-warning" value="更换头像">
									</div>
								</section>
							</div>
						</div>
						</form>


					</div>
				</div>
			</section>


	</div>
</div>
<include file="Public:footer"/>
<include file="Public:footerjs"/>


</body>
</html>