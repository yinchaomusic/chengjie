<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">



</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>

<div class="g-content u-cls">
	<div class="m-content">



		<section>
			<div class="container nrye">
				<div class="row">
					<include file="Public:nav_left"/>

					<div class="col-sm-10">
					<div class="container_page ">
						<section>
							<h2>提现银行</h2>
							<div class="table-responsive">
								<table class=" table table-bordered table-striped table-hover ">
									<thead>
									<th>开户人</th>
									<th>开户行</th>
									<th>账号</th>
									<th>绑定日期</th>
									<th>操作</th>
									</thead>
									<tbody class="text-center">
									<if condition="is_array($bankList)">
										<volist name="bankList" id="vo">
										<tr>
											<td>{pigcms{$vo.nickname}</td>
											<td>{pigcms{$vo.bankName}</td>
											<td>{pigcms{$vo.account}</td>
											<td>{pigcms{$vo.addtime|date='Y-m-d',###}</td>
											<td>
												<a href="javascript:void(0);" onclick="showdetail('{pigcms{$vo.id}');return false;">查看详细</a>|
												<a href="javascript:void(0);" id="nc_{pigcms{$vo.id}" onclick="borrowoption('{pigcms{$vo.id}',1);return false;">删除</a>
											</td>
										</tr>
										</volist>
									<else/>
									<tr>
										<td colspan="5">暂无记录</td>
									</tr>
									</if>
									</tbody>
								</table>
							</div>
							<div class="btn_area">
								<a href="{pigcms{:U('Fund/addBankCard')}" class="btn btn-primary btn-warning btn-xs-block">添加银行卡</a>
							</div>
						</section>
					</div>
				</div>


				</div>
			</div>
		</section>



	</div>
</div>
<include file="Public:footer"/>
<style>
	.customClassset{
		width: 860px;
		text-align: center;
	}
</style>
<include file="Public:footerjs"/>


<script type="text/javascript">


	function showdetail(id){

		swal({   title: "玩命加载中...",   text: "客官，请耐心等待",      showConfirmButton: false });
		var submit_getdata = "{pigcms{:U('Fund/showdetail')}";
		$.post(submit_getdata, {'id': id}, function (result) {
			result = $.parseJSON(result);
			if (result) {
				if (result.error == 0) {
					swal({
						title: result.title,
						confirmButtonColor: "#FFA200",
						confirmButtonText: "关闭",
						customClass:"customClassset",
						text: result.htmldata,
						html:true
					});
				} else {
					swal(result.msg);
					return false;
				}
			} else {
				swal(result.msg);
				return false;
			}
		});

	}



	var submit_check="{pigcms{:U('Fund/bankdel')}";
	function borrowoption(id,type) {
		var title,text,configbutton;
		if(type ==1){
			title = "你确定要删除么？";
			text =  "确认则删除该提现银行信息";
			configbutton = "删除";
		}

		swal({
			title: title,
			text: text,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: configbutton,
			cancelButtonText: "取消",
			closeOnConfirm: false,
			closeOnCancel: false
		}, function(isConfirm){
			if (isConfirm) {
				if(type == 1){
					var nowcheckdomain = $('#nc_'+id);
					nowcheckdomain.html("<img src='{pigcms{$static_public}loading.gif' width='20' height='20' />");
					$.post(submit_check, {'id': id,'type':type}, function (result) {
						result = $.parseJSON(result);
						if (result) {
							if (result.error == 0) {
								nowcheckdomain.parent().parent().remove();
								swal('',result.msg,'success');
								if(result.state == 1){
									window.location = result.reloadurl;
								}
								if(result.isreload){
									window.location = window.location;
								}
							} else {
								swal('',result.msg,'error');
								window.location = window.location;
							}
						} else {
							swal('',result.msg,'error');
							window.location = window.location;
						}
					});
					return false
				}


			} else {
				swal("取消成功", "您点击了取消 :)", "error");window.location = window.location;
			}
		});

	}

</script>

</body>
</html>