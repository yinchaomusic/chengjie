<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">



</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>

<div class="g-content u-cls">
	<div class="m-content">



		<form action="{pigcms{:U('Fund/record')}" method="post" novalidate="true">
			<section>
				<div class="container nrye">
					<div class="row">



						<include file="Public:nav_left"/>

						<div class="col-sm-10">
							<div class="container_page ">
								<section>
									<h2>资金记录</h2>
									<div class="page_min_h">
										<div class="form-horizontal border_box form-zjjl">
											<div class="form-group">
												<label for="input6" class="control-label col-sm-1">时间： </label>
												<div class="col-sm-2">
													<input type="date" class="form-control" id="StartTime" value="">
												</div>
												<span class="col-sm-1 col-sm-_">—</span>
												<div class="col-sm-2">
													<input type="date" class="form-control" id="EndTime" value="">
												</div>
											</div>
											<div class="form-group">
												<label for="input6" class="control-label col-sm-1">资金类型： </label>
												<div class="col-sm-10" id="Purposes">
													<span class="a-sm-btn checkall">全部取消</span>
													<label class="checkbox-inline">
														<input type="checkbox" checked value="1">充值
													</label>
													<label class="checkbox-inline">
														<input type="checkbox"  checked  value="15">本金入款
													</label>
													<label class="checkbox-inline">
														<input type="checkbox"  checked value="16">利息入款
													</label>
													<label class="checkbox-inline">
														<input type="checkbox"   checked value="17">借入入款
													</label>
													<label class="checkbox-inline">
														<input type="checkbox"  checked value="18">逾期入款
													</label>

													<br />
													<label class="checkbox-inline">
														<input type="checkbox"   checked value="2">提现
													</label>
													<label class="checkbox-inline">
														<input type="checkbox"  checked value="19">管理费扣款
													</label>
													<label class="checkbox-inline">
														<input type="checkbox"    checked value="20">本金扣款
													</label>
													<label class="checkbox-inline">
														<input type="checkbox"   checked value="21">利息扣款
													</label>
													<label class="checkbox-inline">
														<input type="checkbox"   checked value="22">借出扣款
													</label>
													<label class="checkbox-inline">
														<input type="checkbox" checked value="23">逾期罚款
													</label>
												</div>
											</div>
											<div class="btn_area">
												<input type="button" id="Query" class="btn btn-primary btn-warning btn-xs-block" value="查询">
											</div>
										</div>
										<div class="table-responsive">
											<table class=" table table-bordered table-striped table-hover ">
												<thead>
												<tr>
													<th>日期/标号</th>
													<th>类型</th>
													<th>收入</th>
													<th>支出</th>
													<th>账户总额</th>
													<th>说明</th>
												</tr>
												</thead>

												<tbody class="text-center">
												<if condition="is_array($user_money_list)">
													<volist name="user_money_list" id="vo">
														<tr>
															<td>{pigcms{$vo['time']|date='Y-m-d',###}</td>
															<td>
																<if condition="$vo['type'] eq 1">
																	充值
																<elseif condition="$vo['type'] eq 2"/>
																	提现
																<elseif condition="$vo['type'] eq 15"/>
																	本金入款
																<elseif condition="$vo['type'] eq 16"/>
																	利息入款
																<elseif condition="$vo['type'] eq 17"/>
																	借入入款
																<elseif condition="$vo['type'] eq 18"/>
																	逾期入款
																<elseif condition="$vo['type'] eq 19"/>
																	管理费扣款
																<elseif condition="$vo['type'] eq 20"/>
																	本金扣款
																<elseif condition="$vo['type'] eq 21"/>
																	利息扣款
																<elseif condition="$vo['type'] eq 22"/>
																	借出扣款
																<elseif condition="$vo['type'] eq 23"/>
																	逾期罚款
																</if>
															</td>
															<td>
<!--																<in name="$vo['type']" value="1,15,16,17,18">￥{pigcms{$vo.money}<else/>-</in>-->
<if condition="($vo['type'] eq 1) OR ($vo['type'] eq 15) OR ($vo['type'] eq 16) OR ($vo['type'] eq 17) OR ($vo['type'] eq 18)"><font color="red">￥{pigcms{$vo.money}</font><else/>-</if>
															</td>
															<td>
<if condition="($vo['type'] eq 2) OR ($vo['type'] eq 19) OR ($vo['type'] eq 20) OR ($vo['type'] eq 21) OR ($vo['type'] eq 22) OR ($vo['type'] eq 23)"><font color="green">￥{pigcms{$vo.money}</font><else/>-</if>
															</td>
															<td>￥{pigcms{$vo.now_money}</td>
															<td>{pigcms{$vo.desc}</td>
														</tr>
													</volist>
												<else/>
													<tr>
														<td colspan="6">暂无数据</td>
													</tr>
												</if>
												</tbody>
											</table>
										</div>
									</div>
								</section>
							</div>
						</div>



					</div>
				</div>
			</section>
		</form>



	</div>
</div>
<include file="Public:footer"/>
<include file="Public:footerjs"/>

<script type="text/javascript">
	$(function () {
		$(".checkall").click(function () {
			$(".checkbox-inline :checkbox").prop('checked', false);
		});
		$("#Query").click(function () {

			var url = "{pigcms{:U('Fund/record')}";
			if ($("#StartTime").val() && $("#EndTime").val()) {
				url += "&start=" + $("#StartTime").val() + "&end=" + $("#EndTime").val() + "&";
			}else{
				url +='&'
			}
			var checkeds = $("#Purposes :checked");
			var purposes = [];
			for (var i = 0; i < checkeds.length; i++) {
				purposes.push(checkeds.eq(i).val());
			}
			if (purposes.length > 0) {
				url += "purpose=" + purposes.join(",");
			}
			window.location = url;
		});


		if ($Mbrowser.versions.ios || $Mbrowser.versions.XiaoMi || ($Mbrowser.versions.mobile && $Mbrowser.versions.Chrome)) {
			/*
			 alert("支持")                     */
		} else {
			/*alert("js日历控件")*/
			$("#StartTime,#EndTime").attr("type", "text");
			laydate({
				elem: '#StartTime'
			});
			laydate({
				elem: '#EndTime'
			});
		};


	});

</script>


</body>
</html>