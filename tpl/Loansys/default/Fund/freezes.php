<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css" />



</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>

<div class="g-content u-cls">
	<div class="m-content">



		<section>
			<div class="container nrye">
				<div class="row">
					<include file="Public:nav_left"/>

					<div class="col-sm-10">
						<div class="container_page ">
							<section>
								<h2>冻结记录</h2>
								<div class="page_min_h">
									<div class="table-responsive">
										<table class=" table table-bordered table-striped table-hover ">
											<thead>
											<tr>
												<th>冻结日期</th>
												<th>冻结金额</th>
												<th>类型</th>
												<th>说明</th>
											</tr>
											</thead>
											<tbody class="text-center">
											<if condition="is_array($user_money_list)">
												<volist name="user_money_list" id="vo">
													<tr>
													<td>{pigcms{$vo.freezetime|date='Y-m-d',###}</td>
													<td>￥{pigcms{$vo.freezemoney}</td>
													<td><if condition="$vo['type'] eq 7">投标成功资金冻结<elseif condition="$vo['type'] eq 8"/>自动投标设置 冻结资金</if> </td>
													<td>{pigcms{$vo.info}</td>
													</tr>
												</volist>
											</if>
											</tbody>
										</table>
									</div>
								</div>

							</section>
						</div>
					</div>


				</div>
			</div>
		</section>



	</div>
</div>
<include file="Public:footer"/>

<include file="Public:footerjs"/>



</body>
</html>