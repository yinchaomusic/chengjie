<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">



</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>
<div class="s-bg-f7">
	<div class="g-position">
		<div class="m-position u-fl">
			<ol>
				<li>当前位置：&nbsp;</li>
				<li><a href="{pigcms{$config.site_url}" class="s-2a">首页</a>&nbsp;&gt;&nbsp;</li>
				<li>域名融资</li>
			</ol>
		</div>
		<div class="m-serverTime u-fr">
			服务器时间：<span id="mytime">{pigcms{$_SERVER.REQUEST_TIME|date='Y-m-d H:i:s',###}</span>&nbsp;(CST&nbsp;+08:00)
		</div>
		<div class="u-cls"></div>
	</div>
</div>
<div class="g-content u-cls">
	<div class="m-content">



		<form action="" method="post" novalidate="true">
			<section>
				<div class="container nrye">
					<div class="row">

						<include file="Public:nav_left"/>

						<div class="col-sm-10 ">
							<div class="container_page blank">
								<section>
									<h2>账户充值</h2>
									<ul id="rechargeTabs" class="nav nav-tabs cz-nav-tabs" >
										<li id="lionline" role="presentation">在线充值</li>
										<li id="lioffline" role="presentation" class="active">线下充值</li>
									</ul>
									<div id="online" style="display:none;">
										<p class="info-box">
											<span class="font_c_orange">在线充值说明：</span><br />
											1、为保障资金安全，充值金额在 200000元 以上的，建议采用银行转账。<br />
											2、用户在线充值不需手续费，第三方支付收取的手续费由平台承担。
										</p>
										<section>
											<div class="bg_violet clearfix form-zxzf">
												<div class="form-horizontal  ">
													<div class="form-group  zfjk-group">
														<label class="control-label col-sm-1">支付接口：</label>
														<ul class="list-zfjk">
															<li>
																<volist name="pay_method" id="vo">
																	<label style="padding:2px 6px;" class="{pigcms{$key} <if condition="$i eq 1">active</if>">
																		<img width="130" src="{pigcms{$static_public}images/pay/{pigcms{$key}.gif" alt="财付通">
																	<input type="radio" name="paytype" value="{pigcms{$key}"  <if condition="$i eq 	1">checked</if>>
																	</label>

																</volist>
															</li>

														</ul>

													</div>
													<div class="form-group">
														<label class=" control-label col-sm-1">充值金额：</label>
														<div class="col-sm-2">
	<input autocomplete="off" class="form-control Decimal" id="rechargeMoney" name="rechargeMoney" placeholder="不能小于100" type="number" value="" />
														</div>
														<p class="form-control-static">元&nbsp;&nbsp;
															<span id="spanRechargeAmount" style="display:none;" data-amount="0">本月已充值￥0.00元</span></p>
													</div>

												</div>
											</div>
						 <input type="button" onclick="postRecharge();" id="btnRecharge" class="btn  btn-primary3 btn-xs-block" value="充 值" style="margin-left: 102px;background-color:#91BE45;">
										</section>

									</div>
									<div id="offline">
										<p class="info-box">
											<span class="font_c_orange">线下充值(汇款人承担手续费)</span><br />
											<span class="font_c_orange">为保障资金安全，充值金额在 200000元 以上的，建议采用银行转账</span><br />
											请联系客服QQ：客服1：<a target="_blank"  href="http://wpa.qq.com/msgrd?v=3&uin={pigcms{$config.donline_serve1}&site={pigcms{$config.dsite_url}&menu=yes">{pigcms{$config.donline_serve1}</a>
											客服2：<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin={pigcms{$config.donline_serve2}&site={pigcms{$config.dsite_url}&menu=yes">{pigcms{$config.donline_serve2}</a><br/>
										</p>
										<dl class="clearfix">
											<dt class="gsyh">中国工商银行</dt>
											<dd>收款人：诚借<br />
												开户行：域名贷款业务暂未开通，敬请期待...<br />
												帐　号：域名贷款业务暂未开通，敬请期待...</dd>
											<dt class="huishang">徽商银行</dt>
											<dd>收款人：诚借<br />
												开户行：域名贷款业务暂未开通，敬请期待... <br />
												帐　号：域名贷款业务暂未开通，敬请期待...</dd>
										</dl>
									</div>
								</section>

							</div>
						</div>

					</div>
				</div>
			</section>
		</form>



	</div>
</div>
<include file="Public:footer"/>
<include file="Public:footerjs"/>
<script type="text/javascript">
	$('.list-zfjk input[type="radio"]').on('click', function () {
		$('.list-zfjk li label').removeClass('active');
		$(this).closest('label').addClass('active');
	})

	function postRecharge() {
		if ($("#rechargeMoney").val() == "") {
			alert('请输入充值金额'); $("#rechargeMoney").focus(); return;
		}
			var pay = $("input[name='paytype']:checked").val();

			var url = "{pigcms{:U('Fund/recharge_save')}&pay_type=" + pay;


			$("#pop1_bg").css({ "display": "block", "height": $(document).height() + "px" });
			$("#msgDiv").css("display", "");
			window.open(url + "&money=" + $("#rechargeMoney").val());


		$(".sp_close").click(function () {
			$("#pop1_bg").css("display", "none");
			$("#msgDiv").css("display", "none");
		})

	}
	$(function () {
		$("#rechargeTabs li").click(function () {
			if (this.id == "lionline") {
				$("#online").show(); $("#lionline").attr("class", "active");
				$("#offline").hide(); $("#lioffline").attr("class", "");
			}
			else {
				$("#offline").show(); $("#lioffline").attr("class", "active");
				$("#online").hide(); $("#lionline").attr("class", "");
			}

		});
		var rechargeAmount = $("#spanRechargeAmount").attr("data-amount");
		var canRechargeAmount = 200000 - rechargeAmount;
		$("#rechargeMoney").on("blur", function () {
			if ($(this).val() > 100) {
				if ($(this).val() > canRechargeAmount) $(this).val(canRechargeAmount);
			} else if ($(this).val() != "") {
				$(this).val(100);
			}
		});

	})
</script>
<div id="msgDiv" style="width: 315px; height: 130px; z-index: 99999; position: absolute; top: 482px; left: 40%; display: none; background: rgb(39, 137, 200);">
	<div style="padding-left: 20px; height: 30px; line-height: 30px; font-size: 15px; font-weight: bold; color: white; text-align: left;">
		<span class="sp_close" style="display: inline-block; width: 30px; height: 30px; line-height: 30px; text-align: center; cursor: pointer; float: right;">×</span>
		提示
	</div>
	<div style="width: 281px; height: 75px; padding: 10px; background: white; margin: 0 auto; text-align: left; line-height: 21px;">
		<div>请您在新打开的网上银行页面进行支付，支付完成前请不要关闭该窗口。</div>
		<a href="{pigcms{:U('Financing/index')}" class="btn_pay">已支付完成</a><a href="{pigcms{:U('Fund/account')}" class="btn_pay">支付遇到问题</a>
	</div>
</div>

</body>
</html>