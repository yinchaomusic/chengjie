<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css" />



</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>

<div class="g-content u-cls">
	<div class="m-content">



		<section>
			<div class="container nrye">
				<div class="row">
					<include file="Public:nav_left"/>

					<div class="col-sm-10">
						<div class="container_page ">
							<section>
								<h2>添加银行卡</h2>
								<div class="form-horizontal form-info form-tjyhk">
									<div class="form-group">
										<label class="control-label col-sm-1">开户姓名： </label>
										<div class="col-sm-10">
											<p class="form-control-static">{pigcms{$user_session.nickname}</p>
											<input type="hidden" id="MyNmae" name="MyNmae" value="{pigcms{$user_session.nickname}"/>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-1">选择银行： </label>
										<div class="col-sm-2">
											<select class="form-control" id="BankName" name="BankName">
												<option value="null">请选择银行</option>
												<option value="工商银行">工商银行</option>
												<option value="招商银行">招商银行</option>
												<option value="农业银行">农业银行</option>
												<option value="建设银行">建设银行</option>
												<option value="中国银行">中国银行</option>
												<option value="民生银行">民生银行</option>
											</select>
										</div>

									</div>
									<div class="form-group">
										<label class="control-label col-sm-1">开户地区： </label>
										<div class="col-sm-8 select-inline">
											<select class="form-control"  id="loc_province" style="width:80px;"></select>
											<select class="form-control" id="loc_city" style="width:100px;"></select>
											<select class="form-control" id="loc_town" style="width:120px;"></select>
											<input type="hidden" name="location_id" />
										</div>
									</div>
									<div class="form-group">
										<label for="BranchName" class=" control-label col-sm-1">开户支行： </label>
										<div class="col-sm-4">
											<input class="form-control" id="BranchName" name="BranchName" type="text" value="" />
										</div>
										<span class="form-control-tips col-sm-7" style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
									</div>
									<div class="form-group">
										<label for="CardNumber" class=" control-label col-sm-1">银行卡号： </label>
										<div class="col-sm-4">
											<input class="form-control" id="CardNumber" maxlength="19" name="CardNumber" type="text" value="" />
										</div>
										<span class="form-control-tips col-sm-7" style="display: none"><i class="iconfont">&#xe60a;</i>提示文字</span>
									</div>

									<div class="btn_area">
										<input type="submit" id="submit" class="btn btn-warning btn-xs-block" value="提交">
									</div>
								</div>
							</section>
						</div>
					</div>


				</div>
			</div>
		</section>



	</div>
</div>
<include file="Public:footer"/>

<include file="Public:footerjs"/>


<script type="text/javascript" src="{pigcms{$static_path}location/area.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}location/location.js"></script>

<script type="text/javascript">

		$(document).ready(function() {
			showLocation();
		});

	$(function () {

		var city,BankName,CardNumber,BranchName,MyNmae;
		$('#BankName').change(function(){
			BankName = $(this).find("option:selected").text();
		});
		$('select').change(function(){
			city=$('#loc_province').find("option:selected").text() + ',' +$('#loc_city').find("option:selected").text() +','+$('#loc_town').find("option:selected").text()
				//console.log(city);
		})

		var post_reset ="{pigcms{:U('Fund/addBankCard')}";
		$('#submit').click(function(){
			CardNumber = $('#CardNumber').val();
			BranchName = $('#BranchName').val();
			MyNmae = $('#MyNmae').val();
			if(typeof(BankName) == 'undefined' || BankName=='null'){
				swal({   title: '错误' ,   text: '请选择银行！' ,type:"warning",   showConfirmButton: true });
				return false;
			}
			if(BranchName == ''){
				swal({   title: '错误' ,   text: '开户支行必须填写！' ,type:"warning",   showConfirmButton: true });
				return false;
			}
			if(CardNumber == ''){
				swal({   title: '错误' ,   text: '银行卡号必须填写！' ,type:"warning",   showConfirmButton: true });
				return false;
			}



			$.post(post_reset,{'city':city,'BankName':BankName,'BranchName':BranchName,'CardNumber':CardNumber,'MyNmae':MyNmae},function(results){

				results = $.parseJSON(results);
				if(results.error == 0){
				swal({   title: results.title ,   text: results.msg  ,type:"success",     showConfirmButton: true, showLoaderOnConfirm: true },
					function(){
						window.location = "{pigcms{:U('Fund/bankcard')}";
					});
				}else{
					swal({   title: '出现错误' ,   text: results.msg ,type:"error",    showConfirmButton: true });
				}
			})
		})

	});
</script>


</body>
</html>