<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css" />



</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>

<div class="g-content u-cls">
	<div class="m-content">

			<section>
				<div class="container nrye">
					<div class="row">

						<include file="Public:nav_left"/>

						<div class="col-sm-10">
							<div class="container_page ">
								<section>
									<h2>账户提现</h2>
									<div class="page_min_h">
										<div class="form-horizontal border_box form-tjyhk">
											<div class="form-group">
												<label for="BankCardId" class=" control-label col-sm-1">提现账户： </label>
												<div class="col-sm-3">
													<select name="BankCardId" id="BankCardId" class="form-control">
														<volist name="bank_card_list" id="vo">
														<option value="{pigcms{$vo.id}">{pigcms{$vo.bankName}</option>
														</volist>
													</select>
												</div>
												<a href="{pigcms{:U('Fund/addBankCard')}">新增银行卡</a>
											</div>
											<div class="form-group">
												<label for="getAmounts" class="control-label col-sm-1">提现金额： </label>
												<div class="col-sm-3">
													<input class="form-control Decimal" id="getAmount" name="getAmount" type="number" value=""  />
												</div>
		 <p class="form-control-static ">元  可提现金额 <span class="font_c_green" id="nowAquotas" data-amount="{pigcms{$user_info.now_money}">￥{pigcms{$user_info.now_money}</span></p>
											</div>
											<div class="form-group">
												<label for="TransactionPassword" class="control-label col-sm-1">交易密码： </label>
												<div class="col-sm-3">
													<input class="form-control" id="TransactionPassword" name="TransactionPassword" type="password" />
												</div>
												<a class=" a-sm-btn" href="{pigcms{:U('Profile/transactionPassword')}">忘记交易密码</a>
											</div>
											<div class="btn_area">
												<input type="button" id="txsubmit" class="btn btn-warning btn-xs-block" value="提现">
											</div>
										</div>
										<h4>提现列表</h4>
										<div class="table-responsive">
											<table class=" table table-bordered table-striped table-hover ">
												<thead>
												<tr>
													<th>申请日期</th>
													<th>申请金额</th>
													<th>手续费</th>
													<th>银行</th>
													<th>卡尾号</th>
													<th>状态</th>
												</tr>
												</thead>
												<tbody class="text-center">
													<if condition="is_array($wr_list)">
														<volist name="wr_list" id="vo">
															<tr>
																<td>{pigcms{$vo.addtime|date='Y-m-d',###}</td>
																<td>￥{pigcms{$vo.txtmoeny}</td>
																<td>￥{pigcms{$vo.decMenory}</td>
																<td>{pigcms{$vo.bankName}</td>
																<td>{pigcms{$vo.account|msubstr=14,4,false}</td>
																<td>
																	<if condition="$vo['state'] eq 0">提现申请中
																	<elseif condition="$vo['state'] eq 1"/><span class="font_success">提现成功</span>
																	<elseif condition="$vo['state'] eq 2"/>提现失败
																	</if>
																</td>
															</tr>
														</volist>
													</if>
												</tbody>
											</table>
										</div>
									</div>
								</section>
							</div>
						</div>


					</div>
				</div>
			</section>




	</div>
</div>
<include file="Public:footer"/>
<include file="Public:footerjs"/>


<script type="text/javascript">
	$(function () {
		var nowAquotas,getAmount,TransactionPassword,bankid;
		nowAquotas = ($("#nowAquotas").attr("data-amount") * 1).toFixed(4).replace(/(.*)\d\d/, "$1") * 1;

		post_reset ="{pigcms{:U('Fund/withdrawal_data')}";
		$('#txsubmit').unbind("click").click(function(){

			  getAmount = $('#getAmount').val();
			  TransactionPassword = $('#TransactionPassword').val();
			  bankid =   $('#BankCardId').find("option:selected").val();


			if(getAmount == ''|| getAmount > nowAquotas ){

				swal({   title: '错误' ,   text: '提现金额必须填写并且不可以大于提现金额！' ,type:"warning",   showConfirmButton: true });

				return false;
			}else if(TransactionPassword == ''){
				swal({   title: '错误' ,   text: '交易密码必须填写！' ,type:"warning",   showConfirmButton: true });

				return false;
			}else{

				$.post(post_reset,{'getAmount':getAmount,'TransactionPassword':TransactionPassword,'bankid':bankid},function(results){

					//	results = $.parseJSON(results);
					if(results.error == 0){
						swal({   title: results.title ,   text: results.msg  ,type:"success",     showConfirmButton: true, showLoaderOnConfirm: true },
							function(){

								window.location = window.location;
								return false;
							});
					}else{
						swal({   title: '出现错误' ,   text: results.msg ,type:"error",    showConfirmButton: true });

						return false;
					}
				})
				return false;

			}
			return false;
		})

	});


</script>



</body>
</html>