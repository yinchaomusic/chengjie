<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">


</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>

<div class="g-content u-cls">
	<div class="m-content">



		    <section>
				<div class="container nrye">
					<div class="row">
						<include file="Public:nav_left"/>
						<form action="{pigcms{:U('Loan/creditApply')}" method="post"  >
						<div class="col-sm-10 top_layer">
							<div class="container_page user_page_area ">
								<section>
									<h2>申请授信</h2>
									<p>质押是指借入者将持有域名过户至域名融资平台，包括域名的管理权和所有权，以获取授信额度申请贷款。</p>
									<if condition="is_array($okArr)">
										<p>

										<div class="m-account-data1 u_be9 s-bg-fc u_hi408" style="width: auto;">
											<h4 class="s-bg-white">
												<a class="s-3c m-ta2">
													<span class="sp1">提交成功的域名</span>
													<span class="sp2"><b></b>{pigcms{:count($okArr)}</span>

												</a>
												<div class="u-cls"></div>
											</h4>
											<table>
												<tbody><tr>
													<th width="16px"></th>
													<th width="240px" class="u_tl">域名</th>
													<th width="160px"></th>
												</tr>
												<volist name="okArr" id="vo">
													<tr>
														<td class="u_nb"></td>
														<td class="u_nb"><a class="u-txa3">{pigcms{$vo}</a></td>
														<td class="u_nb"></td>
													</tr>
												</volist>
												</tbody></table>
										</div>

										</p>
									</if>
									<if condition="is_array($errorArr)">
										<p>

												<div class="m-account-data1 u_be9 s-bg-fc u_hi408" style="width: auto;">
													<h4 class="s-bg-white">
														<a class="s-3c m-ta2">
															<span class="sp1">提交失败的域名</span>
															<span class="sp2"><b></b>{pigcms{:count($errorArr)}</span>

														</a>
														<div class="u-cls"></div>
													</h4>
													<table>
														<tbody><tr>
															<th width="16px"></th>
															<th width="240px" class="u_tl">域名</th>
															<th width="160px"></th>
															<th width="150px" class="u_tl">失败原因</th>
															<th width="16px"></th>
														</tr>
														<volist name="errorArr" id="vo">
														<tr>
															<td class="u_nb"></td>
															<td class="u_nb"><a class="u-txa3">{pigcms{$vo.domain}</a></td>
															<td class="u_nb"></td>
															<td class="u_nb"><span class="u-span12">{pigcms{$vo.msg}</span></td>
															<td class="u_nb"></td>
														</tr>
														</volist>
														</tbody></table>
												</div>

										</p>
									<else/>
										<div class="m-processList2 u_be9" style="width:auto;">
											<div class="txt1">添加域名</div>
											<div class="txt2">平台进行审核</div>
											<div class="txt3">
												<span class="u-span9">进行估价</span>
											</div>
											<div class="txt4">
												<span class="u-span9">域名转移</span>
											</div>
											<div class="txt5">
												<span class="u-span9">质押完成</span>
											</div>
											<div class="txt6">
												<span class="u-span9">授信额度</span>
											</div>
										</div>

									</if>

									<div class="m-addDomains u_be9 s-bg-fc" style="width:auto;">
										<p class="f-p26"></p>
										<p class="f-p-t9">域名列表：</p>
										<p class="f-p27" style="border: 1px solid rgb(235, 235, 235);">
											<textarea class="txta_v" ph="域名一行一个，请输入顶级域名" name="domain" id="domain" style="color: rgb(216, 216, 216);"></textarea>
										</p>
<!--										<p class="f-p-t9">备注信息：</p>-->
<!--										<p   style="border: 1px solid rgb(235, 235, 235);    margin: 0px auto 19px auto;height: 50px;width: 654px;">-->
<!--											<textarea class="txta_v"  ph="这里输入您的备注" name="intro" style="color: rgb(216, 216, 216);height: 50px;width: 654px;padding: 10px;" maxlength="255"></textarea>-->
<!--										</p>-->
										<div class="m-addoper">
											<div class="u-fl addoper-l"> </div>
											<div class="u-fl addoper-r">
												<p class="u_mtb30">
													<input type="submit" id="check_domain" value="提交" class="u-btn9 btn_tj">
												</p>
											</div>
											<div class="u-cls"></div>
										</div>
									</div>

								</section>
							</div>
						</div>
						</form>

					</div>
				</div>
			</section>




	</div>
</div>
<include file="Public:footer"/>
<include file="Public:footerjs"/>

<script>
	$(function () {
		$('#check_domain').click(function(){
			//alert($('#domain').val());
			if($('#domain').val() == '' || $('#domain').val() == $('#domain').attr('ph')){
				swal('','必须填写至少一个域名','error');
				return false;
			}
		})
	})
</script>


</body>
</html>