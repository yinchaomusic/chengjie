<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>域名融资 - {pigcms{$config.site_name}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link type="text/css" href="{pigcms{$static_path}css/common.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/account.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/sell-domain.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/skin.css" />
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}css/style.css">
	<script src="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{pigcms{$static_path}plugs/sweetalert/dist/sweetalert.css">


</head>
<body class="s-bg-global">
<include file="Public:header"/>
<include file="Public:nav"/>

<div class="g-content u-cls">
	<div class="m-content">



		 <section>
				<div class="container nrye">
					<div class="row">

						<include file="Public:nav_left"/>

						<div class="col-sm-10 top_layer">
							<div class="container_page user_page_area ">
								<section>
									<h2>质押管理</h2>
									<p>提醒：如果[授信状态]是失败的，可以把鼠标悬浮在连接上即可看到失败原因</p>
									<form action="{pigcms{:U('Loan/pledge')}" method="post" >
									<div class="form-horizontal" id="searchForm">
										<div class="form-group">
											<div class="col-sm-3">
												<input type="text" class="form-control" id="keyword"  name="keyword"  value="" placeholder="请输入要搜索的域名">
											</div>
											<div class="col-sm-1">
												<div class="btn_area" style="display:inline;">
<!--													<a href="javascript:;" class="btn btn-default">查询</a>-->
<!--													<button class="btn btn-default btn-warning">查询</button>-->
													<input type="submit" class="btn btn-primary btn-warning" value="查询">
												</div>
											</div>
										</div>
									</div>
									</form>



									<p class="info-box"  <if condition="$result_idcard['status'] eq 1"> style="display: none;" </if>>
										申请授信前，请先进行  <a href="{pigcms{:U('Profile/IdCard')}"><strong>身份认证</strong></a>
									</p>
									<div class="page_min_h">
										<div class="table-responsive">
											<table class=" table table-bordered table-striped table-hover ">
												<thead>
												<tr>
													<th>域名</th>
													<th>授信状态</th>
													<th>授信额度</th>
													<th>授信时间</th>
													<th>申请时间</th>
													<th>质押状态</th>
													<th>融资状态</th>
													<th>域名是否验证</th>
												</tr>
												</thead>
												<tbody class="text-center">
<if condition="is_array($pledgelist)">
		<volist name="pledgelist" id="vo">
		<tr <if condition="$vo['credit_status'] eq 1"> class="font_success" </if>>
		<td>{pigcms{$vo.domain}</td>
		<td><if condition="$vo['credit_status'] eq 0">等待审核<elseif condition="$vo['credit_status'] eq 1"/><span class="font_success">已授信</span><elseif condition="$vo['credit_status'] eq 2"/><span class="font_warning"><a href="javascript:void(0);" id="detail_{pigcms{$vo.pid}" onclick="detail('{pigcms{$vo.pid}');return false;" title="{pigcms{$vo
		.intro}">授信失败</a></span></if></td>
		<td><span class="font_success">￥{pigcms{$vo.credit_total}</span></td>
		<td><if condition="$vo['credit_time']">{pigcms{$vo.credit_time|date='Y-m-d',###}<else/>-</if></td>
		<td>{pigcms{$vo.apply_time|date='Y-m-d',###}</td>
		<td><if condition="$vo['pledge_status'] eq 0">-<elseif condition="$vo['pledge_status'] eq 1"/>已转移<elseif condition="$vo['pledge_status'] eq 2"/>未转移<elseif condition="$vo['pledge_status'] eq 3"/><span class="font_warning">转移失败</span></if></td>
		<td><if condition="$vo['rz_status'] eq 0">-<elseif condition="$vo['credit_status'] eq 1"/>已融资<elseif condition="$vo['credit_status'] eq 2"/>正在融资<elseif condition="$vo['credit_status'] eq 3"/>正在还款<elseif condition="$vo['credit_status'] eq 4"/>还款完成</if></td>

		<td><if condition="$vo['is_check']">已验证 <if condition="$vo['credit_status'] eq 2">&nbsp;-&nbsp;<span id="nc_{pigcms{$vo.pid}"><a href="javascript:void(0);" onclick="checkwithdomain('{pigcms{$vo.pid}',1);return false;">删除</a></span> </if> <else/><span id="nc_{pigcms{$vo.pid}"><a href="javascript:void(0);" onclick="checkwithdomain('{pigcms{$vo.pid}',0);return false;">立即验证</a></span>&nbsp;-&nbsp;<span id="nc_{pigcms{$vo.pid}"><a href="javascript:void(0);" onclick="checkwithdomain('{pigcms{$vo.pid}',1);return false;">删除</a></span></if></td>
		</tr>
	</volist>
<else/>
												<tr>
													<td colspan="8">暂无记录</td>
												</tr>
</if>
												</tbody>
											</table>
										</div>
										<div class="bnt_area">
											<if condition="$result_idcard['status'] eq 0">
											<p class="info-box">
											申请授信前，请先进行  <a href="{pigcms{:U('Profile/IdCard')}"><strong>身份认证</strong></a>
											</p>
											<else/>
											<a href="{pigcms{:U('Loan/creditApply')}" class="btn btn-green">申请授信</a>
											</if>
										</div>
									</div>


								</section>
							</div>
						</div>


					</div>
				</div>
			</section>




	</div>
</div>
<include file="Public:footer"/>

<include file="Public:footerjs"/>

<script type="text/javascript">
	var submit_check="{pigcms{:U('Loan/checkwithdomain')}";
	function checkwithdomain(id,type) {
		var nowcheckdomain = $('#nc_'+id);
		nowcheckdomain.html("<img src='{pigcms{$static_public}loading.gif' width='20' height='20' />");
		$.post(submit_check, {'id': id,'type':type}, function (result) {
			result = $.parseJSON(result);
			if (result) {
				if (result.error == 0) {
					nowcheckdomain.html(result.msg);
					if(result.isreload){
						window.location = window.location;
					}
				} else {
					nowcheckdomain.html(result.msg);
				}
			} else {
				nowcheckdomain.html(result.msg);
			}
		});
	}

	function detail(id){
		//alert(1);
	}
</script>
</body>
</html>