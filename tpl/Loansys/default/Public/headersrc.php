<meta name="keywords" content="{pigcms{$config.dseo_keywords}" />
<meta name="description" content="{pigcms{$config.dseo_description}" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<link rel="icon" href="{pigcms{$config.site_url}/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="{pigcms{$config.site_url}/favicon.ico" type="image/x-icon" />

<!-- 新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet" href="{pigcms{$static_path}css/bootstrap.css">
<link rel="stylesheet" href="{pigcms{$static_path}/css/style.css">
<link rel="stylesheet" href="{pigcms{$static_path}/fonts/iconfont.css">
<!--[if lte IE 9]>
<script src="{pigcms{$static_path}js/respond.min.js"></script>
<script src="{pigcms{$static_path}js/html5shiv-printshiv.min.js"></script>
<![endif]-->
<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="{pigcms{$static_path}js/jquery.min.js"></script>
<script src="{pigcms{$static_path}js/unslider.js"></script>
<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
<script src="{pigcms{$static_path}js/bootstrap.js"></script>

<script type="text/javascript" src="{pigcms{$static_path}js/rz_common.js"></script>
<script type="text/javascript" src="{pigcms{$static_path}js/matrix.js"></script>
