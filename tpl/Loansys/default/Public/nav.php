<div class="visible-xs-block text-center" style="display:none !important;">这里是手机广告 </div>
<div class="topbar hidden-xs">
	<div class="container">
		<div class="Login_area">
			<if condition="$user_session">
			<a href="">
				<span class="iconfont">&#xe63c;</span>
			</a>
			<a href="{pigcms{:U('Financing/index')}">欢迎您 {pigcms{$user_session.nickname}  <span>(UID:{pigcms{$user_session.uid}
					会员等级：{pigcms{$user_level_info.lname})</span></a>
			<a href="{pigcms{:U('Login/logout')}">注销</a>
			<else/>
			<a href="{pigcms{:U('Login/index')}"><span class="iconfont denglu">&#xe63c;</span>登录</a> |
			<a href="{pigcms{:U('Login/reg')}">注册</a> |
			<a href="{pigcms{:U('News/index',array('cat_key'=>'Loansys_help_cj'))}">帮助</a>
			</if>
			<a href="{pigcms{$config.site_url}" target="_blank">诚借网</a>
		</div>
	</div>
</div>
<nav class="navbar navbar-default" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle " data-toggle="collapse" data-target="#example-navbar-collapse"><span class="sr-only">切换导航</span> <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
			<a class="logo" href="{pigcms{:U('Index/index')}">
				<img style="height: 45px;width: 165px;margin-top: 18px;" <if condition="$config['dsite_logo']"> src="{pigcms{$config.dsite_logo}" <else/> src="{pigcms{$static_path}/Loansys_logo.png"</if> alt="诚借网" />
			</a>
		</div>
		<div class="collapse navbar-collapse" id="example-navbar-collapse">
			<ul class="nav navbar-nav">
				<li <if condition="$is_active eq 1" >class="active"</if> ><a  <if condition="$is_active eq 1" >style="background-color:#FFA200"</if> href="{pigcms{:U('Index/index')}">首页</a></li>
				<li  <if condition="$is_active eq 2" >class="active"</if>  ><a  <if condition="$is_active eq 2" >style="background-color:#FFA200"</if>href="{pigcms{:U('Investment/lend')}">我要理财</a></li>
				<li  <if condition="$is_active eq 3" >class="active"</if> ><a  <if condition="$is_active eq 3" >style="background-color:#FFA200"</if> href="{pigcms{:U('Loan/borrow')}">我要借款</a></li>
				<li  <if condition="$is_active eq 4" >class="active"</if> ><a <if condition="$is_active eq 4" >style="background-color:#FFA200"</if> href="{pigcms{:U('Financing/index')}">我的账户</a></li>

				<li  <if condition="$is_active eq 5" >class="active"</if> ><a <if condition="$is_active eq 5" >style="background-color:#FFA200"</if> href="{pigcms{:U('News/aboutus')
				}">关于我们</a></li>
			</ul>
			<div class="visible-xs-block mlnav">
				<div class="Login_area">
					<ul>
						<if condition="$user_session">
							<a href="">
								<span class="iconfont">&#xe624;</span>
							</a>
							<a href="{pigcms{:U('Financing/index')}">欢迎您 {pigcms{$user_session.nickname}
								<span>(UID:{pigcms{$user_session.uid} 会员等级：{pigcms{$user_level_info.lname})</span>
							</a>
							<a href="{pigcms{:U('Login/logout')}">注销</a>
							<else/>
							<a href="{pigcms{:U('Login/index')}"><span class="iconfont denglu">&#xe628;</span>登录</a> |
							<a href="{pigcms{:U('Login/reg')}">注册</a> |
							<a href="{pigcms{:U('News/index',array('cat_key'=>'Loansys_help_cj'))}">帮助</a>
						</if>
					</ul>
				</div>
			</div>
		</div>
	</div>
</nav>
