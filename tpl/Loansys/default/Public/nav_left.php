<div class="col-sm-2">
	<!--sidebar-menu-->
	<div id="sidebar">
		<a href="#" class="submenu_bar  visible-xs-block">
			<ol class="breadcrumb">
				<li><i class="iconfont submenu_d"> &#xe622;</i></li>
				<li>首页</li>
				<li class="active">我的账户</li>
			</ol>
		</a>
		<ul style="display: block;"><li class="submenu <if condition="($MODULE_NAME eq 'Financing') AND ($ACTION_NAME eq 'index')"> active</if> " >
			<a href="{pigcms{:U('Financing/index')}"><i class="iconfont">&#xe608;</i><span>我的账户</span></a>	</li>
				<li class="submenu  <if condition="$MODULE_NAME eq 'Investment'"> active open</if>">
				<a href="{pigcms{:U('Investment/lend')}"><i class="iconfont ">&#xe668;</i>
					<span>我要理财</span><span class="label  visible-xs-block">3</span></a>
				<ul>
<li <if condition="($MODULE_NAME eq 'Investment') AND ($ACTION_NAME eq 'lend')"> class="active" </if>><a href="{pigcms{:U('Investment/lend')}">我要借出</a></li>
<li <if condition="($MODULE_NAME eq 'Investment') AND ($ACTION_NAME eq 'lendList')"> class="active" </if>><a href="{pigcms{:U('Investment/lendList')}">借出列表</a></li>
<li <if condition="($MODULE_NAME eq 'Investment') AND ($ACTION_NAME eq 'automatic')"> class="active" </if>><a href="{pigcms{:U('Investment/automatic')}">自动投标</a></li>
<li  <if condition="($MODULE_NAME eq 'Investment') AND ($ACTION_NAME eq 'keepInvest')"> class="active" </if>><a href="{pigcms{:U('Investment/keepInvest')}">回款续投</a></li>
				</ul>
			</li>
			<li class="submenu <if condition="$MODULE_NAME eq 'Loan'"> active open</if>"><a href="{pigcms{:U('Loan/borrow')}"><i class="iconfont ">&#xe668;</i><span>我要借款</span><span class="label  visible-xs-block">3</span></a>
				<ul>
<li <if condition="($MODULE_NAME eq 'Loan') AND ($ACTION_NAME eq 'borrow')"> class="active" </if>><a href="{pigcms{:U('Loan/borrow')}">我要借入</a></li>
<li  <if condition="($MODULE_NAME eq 'Loan') AND ($ACTION_NAME eq 'borrowList')"> class="active" </if>><a href="{pigcms{:U('Loan/borrowList')}">借入列表</a></li>
<li <if condition="($MODULE_NAME eq 'Loan') AND ($ACTION_NAME eq 'pledge')"> class="active" </if>><a href="{pigcms{:U('Loan/pledge')}">质押管理</a></li>
				</ul>
			</li>
			<li class="submenu <if condition="$MODULE_NAME eq 'Fund'"> active open</if>"><a href="#"><i class="iconfont ">&#xe617;</i><span>资金管理</span>
				<span class="label  visible-xs-block">6</span></a>
				<ul>
<li <if condition="($MODULE_NAME eq 'Fund') AND ($ACTION_NAME eq 'account')"> class="active" </if>><a href="{pigcms{:U('Fund/account')}">账户充值</a></li>
<li <if condition="($MODULE_NAME eq 'Fund') AND ($ACTION_NAME eq 'withdrawal')"> class="active" </if>><a href="{pigcms{:U('Fund/withdrawal')}">账户提现</a></li>
<li <if condition="($MODULE_NAME eq 'Fund') AND ($ACTION_NAME eq 'bankcard')"> class="active" </if>><a href="{pigcms{:U('Fund/bankcard')}">提现银行</a></li>
<li <if condition="($MODULE_NAME eq 'Fund') AND ($ACTION_NAME eq 'record')"> class="active" </if>><a href="{pigcms{:U('Fund/record')}">资金记录</a></li>
<li <if condition="($MODULE_NAME eq 'Fund') AND ($ACTION_NAME eq 'freezes')"> class="active" </if>><a href="{pigcms{:U('Fund/freezes')}">冻结记录</a></li>
				</ul>
			</li>
			<li class="submenu <if condition="$MODULE_NAME eq 'Profile'"> active open</if> "><a href="#"><i class="iconfont ">&#xe624;</i><span>账户管理</span><span class="label  visible-xs-block">7</span></a>
				<ul>
<li <if condition="($MODULE_NAME eq 'Profile') AND ($ACTION_NAME eq 'update')"> class="active" </if>><a href="{pigcms{:U('Profile/update')}">个人信息</a></li>
<li <if condition="($MODULE_NAME eq 'Profile') AND ($ACTION_NAME eq 'avatar')"> class="active" </if>><a href="{pigcms{:U('Profile/avatar')}">上传头像</a></li>
<li <if condition="($MODULE_NAME eq 'Profile') AND ($ACTION_NAME eq 'IdCard')"> class="active" </if>><a href="{pigcms{:U('Profile/IdCard')}">身份认证</a></li>
<li <if condition="($MODULE_NAME eq 'Profile') AND ($ACTION_NAME eq 'updatepwd')"> class="active" </if>><a href="{pigcms{:U('Profile/updatepwd')}">登录密码</a></li>
<li <if condition="($MODULE_NAME eq 'Profile') AND ($ACTION_NAME eq 'transactionPassword')"> class="active" </if>><a href="{pigcms{:U('Profile/transactionPassword')}">交易密码</a></li>
<li <if condition="($MODULE_NAME eq 'Profile') AND ($ACTION_NAME eq 'bind')"> class="active" </if>><a href="{pigcms{:U('Profile/bind')}">绑定手机</a></li>
					<!--											<li><a href="/Member/InfoCenter">站内信</a></li>-->
				</ul>
			</li>
		</ul>
	</div>
	<!--sidebar-menu end-->
</div>
