<div class="col-sm-2">
    <!--sidebar-menu-->
    <div id="sidebar">
        <a href="#" class="submenu_bar  visible-xs-block">
            <ol class="breadcrumb">
                <li><i class="iconfont submenu_d">&#xe62c;</i></li>
                <li>首页</li>
                <li class="active">关于我们</li>
            </ol>
        </a>
        <ul>
            <li class="submenu  <if condition="($cat_key eq 'Loansys_help_cj') OR ($cat_key eq 'Loansys_help_ji')  OR ($cat_key eq 'Loansys_help_li')">open active</if>">
                <a href="#">
                    <i class="iconfont">&#xe63d;</i>
                    <span>帮助中心</span>
                    <span class="label visible-xs-block">5</span>
                </a>
                <ul>
                    <li <if condition="$cat_key eq 'Loansys_help_cj'">class="active"</if> ><a href="{pigcms{:U('News/index',array('cat_key'=>'Loansys_help_cj'))}">常见问题</a></li>
                    <li <if condition="$cat_key eq 'Loansys_help_ji'">class="active"</if>><a href="{pigcms{:U('News/index',array('cat_key'=>'Loansys_help_ji'))}">我是借入者</a></li>
                    <li <if condition="$cat_key eq 'Loansys_help_li'">class="active"</if>><a href="{pigcms{:U('News/index',array('cat_key'=>'Loansys_help_li'))}">我是借出者</a></li>
                </ul>
            </li>
            <li class="submenu <if condition="$cat_key eq 'daikuan_notice'">active</if> ">
                <a href="{pigcms{:U('News/index',array('cat_key'=>'daikuan_notice'))}">
                    <i class="iconfont">&#xe645;</i>
                    <span>新闻公告</span>
                </a>
            </li>
            <li  <if condition="$cat_key eq 'aboutus'">class="active"</if>><a href="{pigcms{:U('News/aboutus')}"><i class="iconfont">&#xe658;</i><span>关于我们</span></a></li>
            <li  <if condition="$cat_key eq 'contact'">class="active"</if>><a href="{pigcms{:U('News/contact')}"><i class="iconfont">&#xe769;</i><span>联系我们</span></a></li>
        </ul>
    </div>
    <!--sidebar-menu end-->
</div>
