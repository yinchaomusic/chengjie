<include file="Public:header"/>
<form id="myform" method="post" action="{pigcms{:U('Web/saveWeb')}" >
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%" style="text-align:center">
		<tr>
			<th width="15%">交易类型</th>
			<td width="15%">
				<select name="entr_type">
    				<option value="1" >代购</option>
    				<option value="2" >代售</option>
    			</select>
			</td>
			<th width="15%">用户ID</th>
			<td width="35%">
				<input type="text" name="user_id"  value="">
			</td>
		<tr/>
		<tr>
			<th width="15%">域名</th>
			<td width="35%">
				<input type="text" name="domain" value="" placeholder="请输入你想代购/代售网站的域名"  required=""/>
			</td>
			<th width="15%">简介</th>
			<td width="35%">
				<textarea name="description" placeholder="代售/代购的域名的简要说明,100字以内" required=""></textarea>
			</td>
		<tr/>
		<tr>
			<th width="15%">PR值</th>
			<td width="35%">
				<input type="text" name="pr" value="0" style="width: 150px" onkeyup="this.value=this.value.replace(/\D/g,'')"
                 onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
			</td>
			<th width="15%">报价</th>
			<td width="35%">
				<input type="text" name="price" id="price" value="{pigcms{$price}"  
				onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" /> 0议价
			</td>
			
		</tr>
		
		<tr>
			<th width="15%">网站类型</th>
			<td width="35%">
				<label class="address address2 fl">
					<select name="web_type" style="width: 140px;">
					   <option value="1">行业门户</option>
					   <option value="2">音乐影视</option>
					   <option value="3">游戏小说</option>
					   <option value="4">女性时尚</option>
					   <option value="5">QQ/娱乐</option>
					   <option value="6">商城购物</option>
					   <option value="7">地方网站</option>
					   <option value="8">其他网站</option>
					</select>
				</label>
			</td>
			<th width="15%">月收入元</th>
			<td width="35%">
				<input type="text" name="month_income" value="0" style="width: 150px"
                            onkeyup="this.value=this.value.replace(/\D/g,'')" 
                                onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
			</td>
		</tr>
		<tr>
			<th width="15%">百度权重pc端</th>
			<td width="35%">
    			<input type="text" name="pc_baidu" style="width: 100px" value="0"
    			onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/></td>
			<th width="15%">百度权重移动端</th>
			<td width="35%">
				<input type="text" name="app_baidu" style="width: 100px" value="0"  onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
			</td>
		<tr/>
		<tr>
			<th width="15%">日ip pc端 </th>
			<td width="35%">
				<input type="text" name="pc_ip" style="width: 100px;" value="0"
				onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/></td>
			<th width="15%">日ip 移动端</th>
			<td width="35%">
				<input type="text" name="app_ip" style="width: 100px;" value="0" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
			</td>
		<tr/>
		<tr>
			<th width="15%">源码</th>
			<td width="35%">
				<input type="text" name="souce_code" value="" placeholder="可填写phpcms v9,帝国7.2,织梦5.6,自研等">
			</td>
			<th width="15%">服务器信息</th>
			<td width="35%">
				<input type="text" name="server_info" value="" placeholder="例如:阿里云/香港机房/centos/1g/1cpu/1m">
			</td>
		<tr/>
		
		<tr>
			<th width="15%">广告联盟</th>
			<td width="35%">
				<input type="text" name="advert_aliance" value="" placeholder="例如:百度联盟/360联盟等">
			</td>
			<th width="15%">联盟帐号处理</th>
			<td width="35%">
				<input type="radio" id="radio-ad_union_account-1" name="aliance_acc_handle" value="删除帐号" class="regular-radio big-radio" checked=""/>
				<label for="radio-ad_union_account-1">删除帐号</label> 
				<input type="radio" id="radio-ad_union_account-2" name="aliance_acc_handle" value="交接帐号" class="regular-radio big-radio"/>
				<label for="radio-ad_union_account-2">交接帐号</label>
			</td>
		<tr/>
		<tr>
			<th width="15%">第三方平台名称</th>
			<td width="35%">
				<input type="text" name="third_party" value="" placeholder="例如:微信公众号/微博帐号等等">
			</td>
			<th width="15%">更新方式</th>
			<td width="35%">
				<input type="text" name="update_type" value="" placeholder="例如:编辑更新/自动采集等等">
			</td>
		<tr/>
		<tr>
			<th width="15%">联系邮箱</th>
			<td width="35%">
				<input type="text" name="email" class="email" value="">
			</td>
			<th width="15%">联系电话</th>
			<td width="35%">
				<input type="text" name="phone"  value="">
			</td>
		<tr/>
		<tr>
			<th width="15%">是否同意平台协议</th>
			<td width="35%" colspan="4">
				<label> 
					<input type="checkbox" name="is_agent" id="agreementId" value="checked" required="">
					<p>
						我已经阅读并同意《<a href="/Home/About/fwxy" target="_blank">chengjie.com服务协议</a>》
					</p>
				</label>
			</td>
		</tr>
	</table>
	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>
</form>
<include file="Public:footer"/>