<include file="Public:header"/>
<div class="mainbox">
	<div id="nav" class="mainnav_title">
		<ul>
		</ul>
	</div>
	<table class="search_table" width="100%">
		<tr>
			<td>
				<form action="{pigcms{:U('Web/index')}" method="post">
					筛选:
					<select name="wstatus">
						<option value="" <if condition="$_POST['wstatus'] eq ''">selected="selected"</if>>选择状态</option>
						<option value="1" <if condition="$_POST['wstatus'] eq '1'">selected="selected"</if>>初始状态</option>
						<option value="2" <if condition="$_POST['wstatus'] eq '2'">selected="selected"</if>>审核通过</option>
						<option value="3" <if condition="$_POST['wstatus'] eq '3'">selected="selected"</if>>审核不通过</option>
						<option value="4" <if condition="$_POST['wstatus'] eq '4'">selected="selected"</if>>已关闭</option>
						<option value="5" <if condition="$_POST['wstatus'] eq '5'">selected="selected"</if>>已结束 </option>
					</select> &nbsp;
					<select name="entr_type">
						<option value="" <if condition="$_POST['entr_type'] eq ''">selected="selected"</if>>选择交易类型</option>
						<option value="1" <if condition="$_POST['entr_type'] eq '1'">selected="selected"</if>>代购</option>
						<option value="2" <if condition="$_POST['entr_type'] eq '2'">selected="selected"</if>>代售</option>
					</select> &nbsp;
					<select name="web_type">
						<option value="" <if condition="$_POST['web_type'] eq ''">selected="selected"</if>>选择网站类型</option>
						<option value="1" <if condition="$_POST['web_type'] eq '1'">selected="selected"</if>>行业门户</option>
						<option value="2" <if condition="$_POST['web_type'] eq '2'">selected="selected"</if>>音乐影视</option>
						<option value="3" <if condition="$_POST['web_type'] eq '3'">selected="selected"</if>>游戏小说</option>
						<option value="4" <if condition="$_POST['web_type'] eq '4'">selected="selected"</if>>女性时尚</option>
						<option value="5" <if condition="$_POST['web_type'] eq '5'">selected="selected"</if>>QQ/娱乐</option>
						<option value="6" <if condition="$_POST['web_type'] eq '6'">selected="selected"</if>>商城购物</option>
						<option value="7" <if condition="$_POST['web_type'] eq '7'">selected="selected"</if>>地方网站</option>
						<option value="8" <if condition="$_POST['web_type'] eq '8'">selected="selected"</if>>其他网站</option>
					</select> &nbsp;
					<input type="submit" value="查询" class="button"/>
					<input  style="width:70px;" value="添加网站" class="button" onclick="window.top.artiframe('{pigcms{:U('Web/add')}','添加网站',800,460,true,false,false,addbtn,'add',true);"/>
				</form>
			</td>
		</tr>
	</table>
	<form name="myform" id="myform" action="" method="post">
		<div class="table-list">
			<table width="100%" cellspacing="0" style="text-align:center">
				<colgroup><col><col><col><col><col><col><col><col><col><col><col><col width="200" align="center"> </colgroup>
				<thead>
				<tr>
					<th>编号</th>
					<th>状态</th>
					<th>交易类型</th>
					<th>网站域名</th>
					<th>百度权重(pc)</th>
					<th>百度权重(app)</th>
					<th>经纪人ID</th>
					<th>用户ID</th>
					<th>是否案例</th>
					<th>网站类型</th>
					<th>创建日期</th>
					<th class="textcenter">操作</th>
				</tr>
				</thead>
				<tbody>
				<if condition="is_array($category_list)">
					<volist name="category_list" id="vo">
						<tr>
							<td>{pigcms{$vo.id}</td>
							<td>
								<if condition="$vo['wstatus'] eq 0">初始状态 
    								<elseif  condition="$vo['wstatus'] eq 1"/>审核通过
    								<elseif  condition="$vo['wstatus'] eq 2"/><font color="red">审核不通过</font>
    								<elseif  condition="$vo['wstatus'] eq 3"/>已关闭
    								<elseif  condition="$vo['wstatus'] eq 4"/>已结束
								</if>
							</td>
							<td>
								<if condition="$vo['entr_type'] eq 1"><font color="red">代购 
								<elseif  condition="$vo['entr_type'] eq 2"/><font color="red">代售</font>
								</if>
							</td>
							<td>{pigcms{$vo.domain}</td>
							<td>{pigcms{$vo.pc_baidu}</td>
							<td>{pigcms{$vo.app_baidu}</td>
							<td>
								<if condition="$vo['check_user_id']">
									<font color="green">{pigcms{$vo.check_user_id}</font><else/><font color="red">无</font>
								</if>
							</td>
							<td>{pigcms{$vo.user_id}</td>
							<td>
								<if condition="$vo['is_case'] eq 0">否 
								<elseif  condition="$vo['is_case'] eq 1"/><font color="red">是</font>
								</if>
							</td>
							<td>
								<if condition="$vo['web_type'] eq 1"><font color="red">行业门户 </font>
    								<elseif  condition="$vo['web_type'] eq 2"/>音乐影视
    								<elseif  condition="$vo['web_type'] eq 3"/>游戏小说
    								<elseif  condition="$vo['web_type'] eq 4"/>女性时尚
    								<elseif  condition="$vo['web_type'] eq 5"/>QQ/娱乐
    								<elseif  condition="$vo['web_type'] eq 6"/>商城购物
    								<elseif  condition="$vo['web_type'] eq 7"/>地方网站
    								<elseif  condition="$vo['web_type'] eq 8"/>其他网站
								</if>
							</td>
							<td><div style="height:24px;line-height:24px;">{pigcms{$vo.createdate|date='Y-m-d H:i:s',###}</div></td>
							<td class="textcenter">
							<if condition="is_array($category_high)">
							    <assign name="var" value="1" />
    							<volist name="category_high" id="high">
    								<if condition="$var eq 1">
        								<if condition="$high['web_id'] eq $vo['id']">
    										<a href="javascript:void(0);"  class="web_high" url="{pigcms{:U('Web/set_web_high',array('id'=>$vo['id'],'type'=>0))}">取消优质</a> 
        									<assign name="var" value="2" />
        								</if>
    								</if>
    							</volist>
    							<if condition="$var eq 1">
									<a href="javascript:void(0);" class="web_high" url="{pigcms{:U('Web/set_web_high',array('id'=>$vo['id'],'type'=>1))}">设为优质</a> 
								</if>
						 	<else/>
						        <a href="javascript:void(0);" class="web_high"  url="{pigcms{:U('Web/set_web_high',array('id'=>$vo['id'],'type'=>1))}">设为优质</a> 
						 	</if>
						  | <a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Web/edit',array('id'=>$vo['id']))}','编辑网站',800,460,true,false,false,editbtn,'add',true);">编辑</a> 
						  | <a href="javascript:void(0);" class="delete_row" parameter="id={pigcms{$vo['id']}" url="{pigcms{:U('Web/del')}">删除</a></td>
						</tr>
					</volist>
					<tr><td class="textcenter pagebar" colspan="12">{pigcms{$pagebar}</td></tr>
					<else/>
					<tr><td class="textcenter red" colspan="12">列表为空！</td></tr>
				</if>
				</tbody>
			</table>
		</div>
	</form>
</div>
<include file="Public:footer"/>