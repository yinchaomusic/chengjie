<include file="Public:header"/>
<form id="myform" method="post" action="{pigcms{:U('Web/edit_web')}" >
	<input type="hidden" name="id" value="{pigcms{$now_Web_entrust.id}">
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%" style="text-align:center">
		<tr>
			<th width="15%">编号</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_entrust.id}</div></td>
			<th width="15%">创建时间</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_entrust.createdate|date='Y-m-d H:i:s',###}</div></td>
		<tr/>
		<tr>
			<th width="15%">交易类型</th>
			<td width="15%">
				<if condition="$now_Web_entrust['entr_type'] eq 1">代购</if>
				<if condition="$now_Web_entrust['entr_type'] eq 2">代售</if>
			</td>
			<th width="15%">状态</th>
			<td width="55%">
    			<select name="wstatus">
    				<option value="0" <if condition="$now_Web_entrust['wstatus'] eq 0">selected</if>>初始状态</option>
    				<option value="1" <if condition="$now_Web_entrust['wstatus'] eq 1">selected</if>>审核通过</option>
    				<option value="2" <if condition="$now_Web_entrust['wstatus'] eq 2">selected</if>>审核不通过</option>
    				<option value="3" <if condition="$now_Web_entrust['wstatus'] eq 3">selected</if>>已关闭</option>
    				<option value="4" <if condition="$now_Web_entrust['wstatus'] eq 4">selected</if>>已结束</option>
    			</select>
			</td>
		<tr/>
		<tr>
			<th width="15%">报价</th>
			<td width="35%">
				<if condition="$now_Web_entrust['price'] eq 0">
					<font color="red">议价</font>
					<else /><font color="green" size="5">￥{pigcms{$now_Web_entrust.price|number_format}</font> 
				</if>
			</td>
			<th width="15%">域名</th>
			<td width="35%"><font color="red" size="5">{pigcms{$now_Web_entrust.domain}</font> </td>
		</tr>
		
		<tr>
			<th width="15%">网站类型</th>
			<td width="35%">
				<if condition="$now_Web_entrust['web_type'] eq 1"><font color="red">行业门户 </font>
    				<elseif  condition="$now_Web_entrust['web_type'] eq 2"/>音乐影视
    				<elseif  condition="$now_Web_entrust['web_type'] eq 3"/>游戏小说
    				<elseif  condition="$now_Web_entrust['web_type'] eq 4"/>女性时尚
    				<elseif  condition="$now_Web_entrust['web_type'] eq 5"/>QQ/娱乐
    				<elseif  condition="$now_Web_entrust['web_type'] eq 5"/>商城购物
    				<elseif  condition="$now_Web_entrust['web_type'] eq 5"/>地方网站
    				<elseif  condition="$now_Web_entrust['web_type'] eq 5"/>其他网站
    			</if>
			</td>
			<th width="15%">月收入元</th>
			<td width="35%">
				<if condition="$now_Web_entrust['month_income'] eq 0">
					<font color="red">未公开</font>
					<else /><font color="green" size="5">￥{pigcms{$now_Web_entrust.month_income|number_format}</font> 
				</if>
			</td>
		</tr>
		<tr>
			<th width="15%">百度权重pc</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_entrust.pc_baidu}</div></td>
			<th width="15%">百度权重app</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_entrust.app_baidu}</div></td>
		<tr/>
		<tr>
			<th width="15%">日ip(pc)pc</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_entrust.pc_ip}</div></td>
			<th width="15%">日ip(app)</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_entrust.app_ip}</div></td>
		<tr/>
		<tr>
			<th width="15%">源码</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_entrust.souce_code}</div></td>
			<th width="15%">服务器信息</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_entrust.server_info}</div></td>
		<tr/>
		
		<tr>
			<th width="15%">广告联盟</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_entrust.advert_aliance}</div></td>
			<th width="15%">联盟帐号处理</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_entrust.aliance_acc_handle}</div></td>
		<tr/>
		<tr>
			<th width="15%">第三方平台名称</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_entrust.third_party}</div></td>
			<th width="15%">更新方式</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_entrust.update_type}</div></td>
		<tr/>
		<tr>
			<th width="15%">邮箱</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_entrust.email}</div></td>
			<th width="15%">电话</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_entrust.phone}</div></td>
		<tr/>
		<tr>
			<th width="15%">简介</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_entrust.description}</div></td>
		<tr/>
		<tr>
			<th width="15%">用户ID</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_entrust.user_id}</div></td>
			<th width="15%">中介处理人ID</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_entrust.check_user_id}</div></td>
		<tr/>
		
		<tr>
			<th width="15%">是否案例</th>
			<td width="35%">
				<span class="cb-enable"><label class="cb-enable <if condition="$now_Web_entrust['is_case'] eq 1">selected</if>"><span>是</span><input type="radio" name="is_case" value="1" <if condition="$now_Web_entrust['is_case'] eq 1">checked="checked"</if>/></label></span>
				<span class="cb-disable"><label class="cb-disable <if condition="$now_Web_entrust['is_case'] eq 0">selected</if>"><span>否</span><input type="radio" name="is_case" value="0" <if condition="$now_Web_entrust['is_case'] eq 0">checked="checked"</if>/></label></span>
			</td>
			<th width="15%">是否同意平台协议</th>
			<td width="35%">
				<if condition="$now_Web_entrust['is_agent'] eq 1">是</if>
				<if condition="$now_Web_entrust['is_agent'] eq 0">否</if>
			</td>
		</tr>
		<tr>
			<th width="15%">审核不通过原因描述</th>
			<td width="35%" colspan="4">
				<textarea name="fail_reason"  style="margin: 0px; width: 554px; height: 139px;" placeholder="可以在这里添加审核不通过原因描述">{pigcms{$now_Web_entrust.fail_reason}</textarea>
			</td>
		</tr>
	</table>
	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>
</form>

<include file="Public:footer"/>