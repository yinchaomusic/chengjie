<include file="Public:header"/>
<div class="mainbox">
	<div id="nav" class="mainnav_title">
		<ul>
			<a href="{pigcms{:U('Trade_rule/index')}" class="on">协议/规则列表</a>|
			<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Trade_rule/add')}','添加后缀',800,680,true,false,false,addbtn,'add',true);">添加协议/规则</a>
		</ul>
	</div>
	<form name="myform" id="myform" action="" method="post">
		<div class="table-list">
			<table width="100%" cellspacing="0">
				<colgroup><col> <col> <col> <col><col><col><col> <col width="140" align="center"> </colgroup>
				<thead>
				<tr>
					<th>编号</th>
					<th>名称</th>
					<th>简述</th>
					<th>交易类型</th>
					<th>URL</th>
					<th>状态</th>
					<th>排序</th>
					<th>添加时间</th>
					<th>更新时间</th>
					<th class="textcenter">操作</th>
				</tr>
				</thead>
				<tbody>
				<if condition="is_array($traderule_list)">
					<volist name="traderule_list" id="vo">
						<tr>
							<td>{pigcms{$vo.rid}</td>
							<td>{pigcms{$vo.title}</td>
							<td>{pigcms{$vo.info|msubstr=0,15}</td>
							<td>{pigcms{$vo.models.0.name}</td>
							<td><if condition="$vo['url']"><a href="{pigcms{$vo['url']}" target="_blank">查看</a><else/></if></td>
							<td><if condition="$vo['status']"><font color="green">显示</font><else/><font color="red">隐藏</font></if></td>
							<td>{pigcms{$vo.sorts}</td>
							<td>{pigcms{$vo.add_time|date="Y-m-d H:i:s",###}</td>
							<td>{pigcms{$vo.update_time|date="Y-m-d H:i:s",###}</td>
							<td class="textcenter"> <a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Trade_rule/edit',array('rid'=>$vo['rid']))}','编辑后缀',800,680,true,false,false,editbtn,'add',true);">编辑</a> | <a href="javascript:void(0);" class="delete_row" parameter="rid={pigcms{$vo.rid}" url="{pigcms{:U('Trade_rule/del')}">删除</a></td>
						</tr>
					</volist>
					<tr><td class="textcenter pagebar" colspan="10">{pigcms{$pagebar}</td></tr>
					<else/>
					<tr><td class="textcenter red" colspan="10">列表为空！</td></tr>
				</if>
				</tbody>
			</table>
		</div>
	</form>
</div>
<include file="Public:footer"/>