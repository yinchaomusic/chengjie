<include file="Public:header"/>
<form id="myform" method="post" action="{pigcms{:U('Trade_rule/edit_save')}" >
	<input type="hidden" name="rid" value="{pigcms{$now_traderule.rid}">
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="80">标题</th>
			<td><input type="text" class="input fl" name="title" size="80" value="{pigcms{$now_traderule.title}" placeholder="请输入规则协议标题" validate="maxlength:50,required:true"
			           tips="请输入规则协议标题"/></td>
		</tr>
		<tr>
			<th width="80">简述</th>
			<td>
				<textarea name="info" id="info" placeholder="这里可以填写简单的描述" style="margin-left: 0px; margin-right: 0px; width: 505px;">{pigcms{$now_traderule.info}</textarea>
				<em class="notice_tips" tips="不填写则不显示 "></em>
		</tr>

		<tr>
			<th width="80">交易类型</th>
			<td>
				<select name="tm_id" id="tm_id">
					<volist name="list_trademodel" id="vo">
						<option value="{pigcms{$vo.id}" <if condition="$vo['id'] eq $now_traderule['tm_id']">selected</if>>{pigcms{$vo.name}</option>+

					</volist>
				</select>

			</td>
		</tr>
		<tr>
			<th width="80">自定义链接</th>
			<td><input type="text" class="input fl" name="url" value="{pigcms{$now_traderule.url}" size="80" validate="maxlength:500;url:true" tips="若设置了此项，点击导航将会跳转到设定的链接"/></td>
		</tr>
		<tr>
			<th width="80">状态</th>
			<td>
				<span class="cb-enable"><label class="cb-enable <if condition="$now_traderule['status'] eq 1">selected</if>"><span>显示</span><input type="radio" name="status" value="1" <if condition="$now_traderule['status'] eq 1">checked="checked"</if>/></label></span>
				<span class="cb-disable"><label class="cb-disable <if condition="$now_traderule['status'] eq 0">selected</if>"><span>隐藏</span><input type="radio" name="status" value="0" <if condition="$now_traderule['status'] eq 0">checked="checked"</if>/></label></span>
			</td>
		</tr>
		<tr>
			<th width="80">排序</th>
			<td><input type="text" class="input fl" name="sorts" size="10" value="{pigcms{$now_traderule.sorts}" validate="number:true,maxlength:6" tips="数值越大，排序越前"/></td>
		</tr>

		<tr>
			<th width="80">内容</th>
			<td>
				<textarea name="content" id="content" >{pigcms{$now_traderule.content}</textarea>
			</td>
		</tr>
	</table>
	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>
</form>
<script src="{pigcms{$static_public}kindeditor/kindeditor.js"></script>
<script type="text/javascript">
	KindEditor.ready(function(K){
		kind_editor = K.create("#content",{
			width:'402px',
			height:'370px',
			resizeType : 1,
			allowPreviewEmoticons:false,
			allowImageUpload : true,
			filterMode: true,
			items : [
				'source', 'fullscreen', '|', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
				'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
				'insertunorderedlist', '|', 'emoticons', 'image', 'link'
			],
			emoticonsPath : './static/emoticons/',
			uploadJson : "{pigcms{$config.site_url}/index.php?g=Index&c=Upload&a=editor_ajax_upload&upload_dir=merchant/news"
		});
	});
</script>
<include file="Public:footer"/>