<include file="Public:header"/>
<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
	<tr>
		<th>贷款ID</th>
		<th>违约时间</th>
		<th>违约天数</th>
		<th>违约金额</th>
		<th>违约类型</th>
	</tr>
	<volist name="now_order" id="vo">
		<tr>
			<th>{pigcms{$vo.bid}</th>
			<th>{pigcms{$vo.breachTiem}</th>
			<th>{pigcms{$vo.breachDays}</th>
			<th>￥{pigcms{$vo.breachMoney}</th>
			<th><if condition="$vo['status'] eq 1">借款期内<elseif condition="$vo['status'] eq 2"/> 合同到期后 </if></th>
		</tr>
	</volist>
	<tr><td class="textcenter pagebar" colspan="5" style="border-bottom:1px solid #ccc;">{pigcms{$pagebar}</td></tr>
</table>
<include file="Public:footer"/>