<include file="Public:header"/>
<form id="myform" method="post" action="{pigcms{:U('News/cat_amend')}" frame="true" refresh="true">
	<input type="hidden" name="cat_id" value="{pigcms{$now_category.cat_id}"/>
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="15%">贷款人UID</th>
			<td width="35%"  style="font-size: large;"><div style="height:24px;line-height:24px;">{pigcms{$now_order.uid}</div></td>
			<th width="15%">申请人</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.dname}</div></td>
		<tr/>
		<tr>
			<th width="15%">借款总额</th>
			<td width="35%" ><span class="font_success">￥{pigcms{$now_order.Amount}（{pigcms{$now_order.Amountdx}元）</span> </td>
			<th width="15%">总利息</th>
			<td width="35%"  style="font-size: large;"><div style="height:24px;line-height:24px;">￥{pigcms{$now_order.zlx}</div></td>
		</tr>
		<tr>
			<th width="15%">总管理费</th>
			<td width="35%" style="font-size: large;"> ￥{pigcms{$now_order.zglf} </td>
			<th width="15%" >每月还款</th>
			<td width="35%"  style="font-size: large;"><div style="height:24px;line-height:24px;">￥{pigcms{$now_order.myfk}</div></td>
		</tr>
		<tr>
			<th width="15%">借款期限</th>
			<td width="35%"><if condition="$now_order['LoanCycleDay'] neq 0">{pigcms{$now_order.LoanCycleDay}天<elseif condition="$now_order['LoanCycleMonth'] neq 0"/>{pigcms{$now_order.LoanCycleMonth} 个月</if> </td>
			<th width="15%">期数</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.periods} 期</div></td>
		</tr>
		<tr>
			<th width="15%">违约金</th>
			<td width="35%">￥{pigcms{$now_order.breachMoney} </td>
			<th width="15%">已还贷款本金</th>
			<td width="35%"><div style="height:24px;line-height:24px;">￥{pigcms{$now_order.yh_menory}</div></td>
		</tr>
		<tr>
			<th width="15%">合同到期</th>
			<td width="35%">{pigcms{$now_order.contractExpire}</td>
			<th width="15%">已还总管理费</th>
			<td width="35%"><div style="height:24px;line-height:24px;">￥{pigcms{$now_order.yh_zglfees}</div></td>
		</tr>
		<tr>
			<th width="15%">年利率</th>
			<td width="35%" style="font-size: large;"> {pigcms{$now_order.Rate}%</td>
			<th width="15%">利息</th>
			<td width="35%" style="font-size: large;"><div style="height:24px;line-height:24px;">￥{pigcms{$now_order.lx}</div></td>
		</tr>
		<tr>
			<th width="15%">贷款号</th>
			<td width="35%"  style="font-size: large;"> {pigcms{$now_order.did} </td>
			<th width="15%">状态</th>
			<td width="35%"><div style="height:24px;line-height:24px;"><if condition="$now_order['status'] eq 1"><span class="font_success">正在投标</span><elseif condition="$now_order['status'] eq 2"/><span class="font_default">等待还款</span><elseif condition="$now_order['status'] eq 3"/><span class="font_default">还款结束</span><elseif condition="$now_order['status'] eq 4"/><span class="font_default">已过期</span><elseif condition="$now_order['status'] eq 5"/><span class="font_default">等待审核</span><elseif condition="$now_order['status'] eq 6"/><span class="font_default">审核通过</span><elseif condition="$now_order['status'] eq 7"/><span class="font_default">申请拒绝</span></if></div></td>
		</tr>
		<tr>
			<th width="15%">第一次还款</th>
			<td width="35%">{pigcms{$now_order.repayDate}</td>
			<th width="15%">满标日期</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.ExpiredAt}</div></td>
		</tr>

		<tr>
			<th width="15%">下一次还款</th>
			<td width="35%">{pigcms{$now_order.NextRepayDate}</td>
			<th width="15%">还款方式</th>
			<td width="35%" class="radio_box"> 每月还款 </td>
		</tr>
		<tr>
			<th width="15%">申请时间 </th>
			<td width="35%">{pigcms{$now_order.addTime|date='Y-m-d',###}</td>
			<th width="15%">备注说明</th>
			<td width="35%">  {pigcms{$now_order.intro}  </td>
		</tr>
		<tr>
			<th width="15%" colspan="2">借款描述</th>
			<td width="85%" colspan="2">{pigcms{$now_order.Description}</td>
		</tr>
		<tr>
			<th width="15%">还款、违约记录</th>
			<td width="85%" colspan="3">
				<div style="height:30px;line-height:24px;">
					<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Borrows/repaymentlog',array('bid'=>$now_order['bid']))}','还款记录',700,560,true,false,false,null,'repaymentlog',true);">还款记录</a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Borrows/defaultslog',array('bid'=>$now_order['bid']))}','违约记录',680,560,true,false,false,null,'defaultslog',true);">违约记录</a>
				</div>
			</td>
		</tr>
		<tr>
			<th  colspan="4"><span class="font_warning">温馨提示：请确认好借款的各项信息，一旦审核通过，不可以对已发布的借入列表进行修改 </span></th>
		</tr>


	</table>
</form>
<include file="Public:footer"/>