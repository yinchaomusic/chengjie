<include file="Public:header"/>
<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
	<tr>
		<th>贷款ID</th>
		<th>期数</th>
		<th>还款本息</th>
		<th>应还时间</th>
		<th>还款时间</th>
		<th>逾期金额</th>
		<th>逾期时间</th>
		<th>已还管理费</th>
		<th>状态</th>
	</tr>
	<volist name="now_order" id="vo">
		<tr>
			<th>{pigcms{$vo.bid}</th>
			<th>{pigcms{$vo['LoanCycle']}(天)</th>
			<th>￥{pigcms{$vo.hk_benxi}</th>
			<th>{pigcms{$vo.yh_time}</th>
			<th>{pigcms{$vo.hk_time|date='Y-m-d H:i:s',###}</th>
			<th>{pigcms{$vo.yq_menory}</th>
			<th><if condition="$vo['yq_time']"> {pigcms{$vo.yq_time|date='Y-m-d H:i:s',###}</if></th>
			<th>{pigcms{$vo.yh_glfees}</th>
			<th><if condition="$vo['status'] eq 1">已经还清<elseif condition="$vo['status'] eq 2"/> 逾期还款<else /> 正在还款 </if></th>
		</tr>
	</volist>
	<tr><td class="textcenter pagebar" colspan="9" style="border-bottom:1px solid #ccc;">{pigcms{$pagebar}</td></tr>
</table>
<include file="Public:footer"/>