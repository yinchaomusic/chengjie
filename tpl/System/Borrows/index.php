<include file="Public:header" xmlns="http://www.w3.org/1999/html"/>
<div class="mainbox">
	<div id="nav" class="mainnav_title">
		<ul>
			<a href="{pigcms{:U('Borrows/index')}" class="on">贷款审核列表</a>|

		</ul>
	</div>
	<table class="search_table" width="100%">
		<tr>
			<td>
				<form action="{pigcms{:U('Borrows/index')}" method="get">
					<input type="hidden" name="c" value="Borrows"/>
					<input type="hidden" name="a" value="index"/>

					筛选: <input type="text" name="keyword" class="input-text" value="{pigcms{$_GET['keyword']}"/>
					<select name="searchtype">
						<option value="UID" <if condition="$_GET['searchtype'] eq 'UID'">selected="selected"</if>>UID</option>
						<option value="DID" <if condition="$_GET['searchtype'] eq 'DID'">selected="selected"</if>>合同编号</option>
					</select> &nbsp;
					<input type="submit" value="查询" class="button"/>
				</form>
			</td>
		</tr>
	</table>
	<form name="myform" id="myform" action="" method="post">
		<div class="table-list">
			<table width="100%" cellspacing="0">
				<colgroup><col> <col> <col> <col><col><col><col> <col width="140" align="center"> </colgroup>
				<thead>
				<tr>
					<th>贷款人(UID)</th>
					<th>姓名</th>
					<th>金额</th>
					<th>年利率</th>
					<th>总利息</th>
					<th>总管理费</th>
					<th>每月还款</th>
					<th>借入/借出</th>
					<th>申请时间</th>
					<th>贷款号</th>
					<th>状态</th>
					<th>满标日期</th>
					<th>还款日期</th>
					<th>期数</th>
					<th>借款周期</th>

					<th class="textcenter">操作</th>
				</tr>
				</thead>
				<tbody>
				<if condition="is_array($order_list)">
					<volist name="order_list" id="vo">
						<tr>
							<td>{pigcms{$vo.uid}</td>
							<td>{pigcms{$vo.dname}</td>
							<td>￥{pigcms{$vo.Amount}/{pigcms{$vo.Amountdx}元 </td>
							<td>{pigcms{$vo.Rate}%</td>
							<td>￥{pigcms{$vo.zlx}</td>
							<td>￥{pigcms{$vo.zglf}</td>
							<td>￥{pigcms{$vo.myfk}</td>
							<td><if condition="$vo['borrowstatus'] eq $vo['uid']"><span class="font_warning">借入</span><else/><span class="font_success">借出</span></if></td>
							<td>{pigcms{$vo.addTime|date='Y-m-d',###}</td>
							<td>{pigcms{$vo.did}</td>

							<td><if condition="$vo['status'] eq 1"><span class="font_success">正在投标</span><elseif condition="$vo['status'] eq 2"/><span class="font_default">等待还款</span><elseif condition="$vo['status'] eq 3"/><span class="font_default">还款结束</span><elseif condition="$vo['status'] eq 4"/><span class="font_default">已过期</span><elseif condition="$vo['status'] eq 5"/><span class="font_default">等待审核</span><elseif condition="$vo['status'] eq 6"/><span class="font_success">审核通过</span><elseif condition="$vo['status'] eq 7"/><span class="font_warning">申请拒绝</span></if></td>
							<td>{pigcms{$vo.ExpiredAt}</td>
							<td>{pigcms{$vo.repayDate}</td>
							<td> {pigcms{$vo.periods} </td>
							<td><if condition="$vo['LoanCycleDay'] neq 0">{pigcms{$vo.LoanCycleDay}天<elseif condition="$vo['LoanCycleMonth'] neq 0"/>{pigcms{$vo.LoanCycleMonth} 个月</if> </td>


							<td class="textcenter">
								<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Borrows/show',array('bid'=>$vo['bid'],'frame_show'=>true))}','查看详情',680,560,true,false,false,false,'add',true);">查看详情</a> |
								<if condition="$vo['status'] eq 1 ">
									<span class="font_success">认证通过</span>
								<elseif condition="$vo['status'] eq 2"/>
									<span class="font_default">等待还款</span>
									<elseif condition="$vo['status'] eq 3"/>
									<span class="font_success">还款结束</span>
									<elseif condition="$vo['status'] eq 4"/>
									<span class="font_default">已过期</span>
									<elseif condition="$vo['status'] eq 7"/>
									<span class="font_warning">申请拒绝</span>
									<elseif condition="$vo['status'] eq 8"/>
									<span class="font_default">流标</span>
									<else />
			 <a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Borrows/borrow_edit',array('bid'=>$vo['bid']))}','编辑',680,560,true,false,false,editbtn,'borrow_edit',true);">编辑</a>

								</if>
							</td>
						</tr>
					</volist>
					<tr><td class="textcenter pagebar" colspan="18">{pigcms{$pagebar}</td></tr>
					<else/>
					<tr><td class="textcenter red" colspan="18">列表为空！</td></tr>
				</if>
				</tbody>
			</table>
		</div>
	</form>
</div>
<include file="Public:footer"/>