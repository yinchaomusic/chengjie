<include file="Public:header"/>
<form   method="post" action="{pigcms{:U('Borrows/borrow_edit_save')}" frame="true"  >
	<input type="hidden" name="bid" value="{pigcms{$now_order.bid}"/>
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="15%">贷款人UID</th>
			<td width="35%"  style="font-size: large;"><div style="height:24px;line-height:24px;">{pigcms{$now_order.uid}</div></td>
			<th width="15%">申请人</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.dname}</div></td>
		<tr/>
		<tr>
			<th width="15%">借款总额</th>
			<td width="35%" ><span class="font_success">￥{pigcms{$now_order.Amount}（{pigcms{$now_order.Amountdx}元）</span> </td>
			<th width="15%">总利息</th>
			<td width="35%"  style="font-size: large;"><div style="height:24px;line-height:24px;">￥{pigcms{$now_order.zlx}</div></td>
		</tr>
		<tr>
			<th width="15%">总管理费</th>
			<td width="35%" style="font-size: large;"> ￥{pigcms{$now_order.zglf} </td>
			<th width="15%" >每月还款</th>
			<td width="35%"  style="font-size: large;"><div style="height:24px;line-height:24px;">￥{pigcms{$now_order.myfk}</div></td>
		</tr>
		<tr>
			<th width="15%">借款期限</th>
			<td width="35%"><if condition="$now_order['LoanCycleDay'] neq 0">{pigcms{$now_order.LoanCycleDay}天<elseif condition="$now_order['LoanCycleMonth'] neq 0"/>{pigcms{$now_order.LoanCycleMonth} 个月</if> </td>
			<th width="15%">期数</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.periods} 期</div></td>
		</tr>
		<tr>
			<th width="15%">年利率</th>
			<td width="35%" style="font-size: large;"> {pigcms{$now_order.Rate}%</td>
			<th width="15%">利息</th>
			<td width="35%" style="font-size: large;"><div style="height:24px;line-height:24px;">￥{pigcms{$now_order.lx}</div></td>
		</tr>
		<tr>
			<th width="15%">贷款号</th>
			<td width="35%"  style="font-size: large;"> {pigcms{$now_order.did} </td>
			<th width="15%">状态</th>
			<td width="35%"><div style="height:24px;line-height:24px;"><if condition="$now_order['status'] eq 1"><span class="font_success">正在投标</span><elseif condition="$now_order['status'] eq 2"/><span class="font_default">等待还款</span><elseif condition="$now_order['status'] eq 3"/><span class="font_default">还款结束</span><elseif condition="$now_order['status'] eq 4"/><span class="font_default">已过期</span><elseif condition="$now_order['status'] eq 5"/><span class="font_default">等待审核</span><elseif condition="$now_order['status'] eq 6"/><span class="font_default">审核通过</span><elseif condition="$now_order['status'] eq 7"/><span class="font_default">申请拒绝</span></if></div></td>
		</tr>
		<tr>
			<th width="15%">还款日期</th>
			<td width="35%">{pigcms{$now_order.repayDate}
<!--				<input type="text" class="input fl" name="repayDate" id="d4311"   tips="还款日期，还款日期必须大于满标日期" value="{pigcms{$now_order.repayDate}" onfocus="WdatePicker({isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'d4312\')}'})" />-->

<!--				<input type="text" class="input fl" name="repayDate" style="width:120px;" id="d4311"   tips="还款日期，还款日期必须大于满标日期" value="{pigcms{$now_order.repayDate}" onClick="WdatePicker()"/>-->
			</td>
			<th width="15%">满标日期</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.ExpiredAt}</div></td>
		</tr>

		<tr>
			<th width="15%">审核</th>
			<td width="35%">
				<select name="status" id="status" style="width: 154px;">
					<option <if condition="$now_order['status'] eq 0"> selected="selected" </if> value="0">请选择</option>
					<option <if condition="$now_order['status'] eq 1"> selected="selected" </if>  value="1">通过申请</option>
					<option <if condition="$now_order['status'] eq 7"> selected="selected" </if> value="7">拒绝申请</option>
				</select>
<!--				<span class="notice_tips">请确认收到该域名，包括域名的管理权和所有权</span>-->
			</td>
			<th width="15%">还款方式</th>
			<td width="35%" class="radio_box">
				每月还款
			</td>
		</tr>
		<tr>
			<th width="15%">申请时间 </th>
			<td width="35%">{pigcms{$now_order.addTime|date='Y-m-d',###}</td>
			<th width="15%">备注说明</th>
			<td width="35%">
				<textarea name="intro" id="intro" cols="30"  rows="5">{pigcms{$now_order.intro} </textarea>
			</td>
		</tr>
		<tr>
			<th width="15%" colspan="2">借款描述</th>
			<td width="85%" colspan="2">{pigcms{$now_order.Description}</td>
		</tr>
		<tr>
			<th  colspan="4"><span class="font_warning">温馨提示：请确认好借款的各项信息，一旦审核通过，不可以对已发布的借入列表进行修改 </span></th>
		</tr>
	</table>

	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>

</form>
<include file="Public:footer"/>