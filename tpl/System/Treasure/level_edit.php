<include file="Public:header"/>
<form id="myform" method="post" action="{pigcms{:U('Treasure/level_edit_save')}" >
	<input type="hidden" name="lid" value="{pigcms{$now_Domain_level.lid}">
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="80">品级</th>
			<td><input type="text" class="input fl" name="level_name" id="level_name" value="{pigcms{$now_Domain_level.level_name}" size="25" placeholder="请填写品级，比如：经典级" validate="maxlength:20,required:true" tips=""/></td>
		</tr>
		<tr>
			<th width="80">排序</th>
			<td><input type="text" class="input fl" name="sorts" value="{pigcms{$now_Domain_level.sorts}" size="10" placeholder="分类排序" validate="maxlength:6,number:true" tips="默认添加时间排序！手动排序数值越大，排序越前。"/></td>
		</tr>
		<tr>
			<th width="80">状态</th>
			<td class="radio_box">
				<span class="cb-enable"><label class="cb-enable <if condition="$now_Domain_level['status'] eq 1">selected</if>"><span>显示</span><input type="radio" name="status" value="1" <if condition="$now_Domain_level['status'] eq 1">checked="checked"</if>/></label></span>
				<span class="cb-disable"><label class="cb-disable <if condition="$now_Domain_level['status'] eq 0">selected</if>"><span>隐藏</span><input type="radio" name="status" value="0" <if condition="$now_Domain_level['status'] eq 0">checked="checked"</if>/></label></span>
			</td>
		</tr>
		<tr>
			<th width="80">简介</th>
			<td>
				<textarea name="info" id="info" style="margin: 0px; width: 300px; height: 30px;" placeholder="可以在这里添加简介信息">{pigcms{$now_Domain_level.info}</textarea>
			</td>
		</tr>
	</table>
	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>
</form>
<include file="Public:footer"/>