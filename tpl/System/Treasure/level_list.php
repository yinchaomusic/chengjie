<include file="Public:header"/>
<div class="mainbox">
	<div id="nav" class="mainnav_title">
		<ul>

			<a href="{pigcms{:U('Treasure/index')}" class="on">域名主题列表</a>|
			<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Treasure/add')}','添加域名主题',480,160,true,false,false,addbtn,'add',true);">添加域名主题</a>
			<a href="{pigcms{:U('Treasure/level_list')}" class="on">珍品域名品级列表</a>|
			<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Treasure/level_add')}','添加珍品域名级别',480,260,true,false,false,addbtn,'add',true);">添加珍品域名品级</a>

		</ul>
	</div>

	<form name="myform" id="myform" action="" method="post">
		<div class="table-list">
			<table width="100%" cellspacing="0">
				<colgroup>
					<col/>
					<col/>
					<col/>
					<col/>
					<col/>
					<col width="180" align="center"/>
				</colgroup>
				<thead>
				<tr>
					<th>排序</th>
					<th>品级</th>
					<th>编号</th>
					<th>状态</th>
					<th>简介</th>
					<th class="textcenter">操作</th>
				</tr>
				</thead>
				<tbody>
				<if condition="is_array($levels_list)">
					<volist name="levels_list" id="vo">
						<tr>
							<td>{pigcms{$vo.sorts}</td>
							<td>{pigcms{$vo.level_name}</td>
							<td>{pigcms{$vo.lid}</td>

							<td><if condition="$vo['status'] eq 1"><font color="green">启用</font><elseif condition="$vo['status'] eq 2"/><font color="red">待审核</font><else/><font color="red">关闭</font></if></td>
							<td>{pigcms{$vo.info}</td>
							<td class="textcenter"><a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Treasure/level_edit',array('lid'=>$vo['lid']))}','编辑分类信息',480,260,true,false,false,editbtn,'edit',true);">编辑</a> | <a href="javascript:void(0);" class="delete_row" parameter="lid={pigcms{$vo.lid}" url="{pigcms{:U('Treasure/level_del')}">删除</a></td>
						</tr>
					</volist>
					<tr><td class="textcenter pagebar" colspan="10">{pigcms{$pagebar}</td></tr>
					<else/>
					<tr><td class="textcenter red" colspan="10">列表为空！</td></tr>
				</if>
				</tbody>
			</table>
		</div>
	</form>
</div>
<include file="Public:footer"/>