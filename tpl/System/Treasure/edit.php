<include file="Public:header"/>
<form id="myform" method="post" action="{pigcms{:U('Treasure/edit_save')}" >
	<input type="hidden" name="id" value="{pigcms{$now_Domain_treasure.id}">
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="80">主题</th>
			<td><input type="text" class="input fl" name="name" id="name" size="25" value="{pigcms{$now_Domain_treasure.name}" placeholder="请填写分类，比如：生活，行业" validate="maxlength:20,required:true"
			           tips=""/></td>
		</tr>
		<tr>
			<th width="80">排序</th>
			<td><input type="text" class="input fl" name="sorts" value="{pigcms{$now_Domain_treasure.sorts}" size="10" placeholder="分类排序" validate="maxlength:6,number:true" tips="默认添加时间排序！手动排序数值越大，排序越前。"/></td>
		</tr>
		<tr>
			<th width="80">状态</th>
			<td>
				<span class="cb-enable"><label class="cb-enable <if condition="$now_Domain_treasure['status'] eq 1">selected</if>"><span>显示</span><input type="radio" name="status" value="1" <if condition="$now_Domain_treasure['status'] eq 1">checked="checked"</if>/></label></span>
				<span class="cb-disable"><label class="cb-disable <if condition="$now_Domain_treasure['status'] eq 0">selected</if>"><span>隐藏</span><input type="radio" name="status" value="0" <if condition="$now_Domain_treasure['status'] eq 0">checked="checked"</if>/></label></span>
			</td>
		</tr>
	</table>
	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>
</form>
<include file="Public:footer"/>