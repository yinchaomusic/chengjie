<include file="Public:header"/>
<div class="mainbox">
	<div id="nav" class="mainnav_title">
		<ul>
			<a href="{pigcms{:U('Footer/index')}">导航分类列表</a>|
			<a href="{pigcms{:U('Footer/category',array('cat_id'=>$now_category['cat_id']))}" class="on">{pigcms{$now_category.name} - 导航列表</a>|
			<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Footer/cat_add',array('cat_id'=>$now_category['cat_id']))}','添加导航',800,460,true,false,false,addbtn,'add',true);">添加导航</a>
		</ul>
	</div>
	<form name="myform" id="myform" action="" method="post">
		<div class="table-list">
			<table width="100%" cellspacing="0">
				<colgroup>
					<col/>
					<col/>
					<col/>
					<col/>
					<col />
					<col width="180" align="center"/>
				</colgroup>
				<thead>
				<tr>
					<th>编号</th>
					<th>名称</th>
					<th>标题</th>
					<th>链接地址</th>
					<th>图片(以下为强制小图，点击图片查看大图)</th>
					<th class="textcenter">操作</th>
				</tr>
				</thead>
				<tbody>
				<if condition="is_array($adver_list)">
					<volist name="adver_list" id="vo">
						<tr>
							<td>{pigcms{$vo.id}</td>
							<td>{pigcms{$vo.name}</td>
							<td>{pigcms{$vo.title}</td>
							<td><a href="{pigcms{$vo.url}" target="_blank">访问链接</a></td>
							<td>
								<img src="{pigcms{$config.site_url}/upload/domain/{pigcms{$vo.pic}" style="width:30px;height:30px;" class="view_msg"/>
							</td>

							<td class="textcenter"> <a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Footer/cat_edit',array('id'=>$vo['id']))}','编辑该分类',800,460,true,false,false,editbtn,'add',true);">编辑</a> | <a href="javascript:void(0);" class="delete_row" parameter="id={pigcms{$vo.id}" url="{pigcms{:U('Footer/cate_del')}">删除</a></td>
						</tr>
					</volist>
					<else/>
					<tr><td class="textcenter red" colspan="8">列表为空！</td></tr>
				</if>
				</tbody>
			</table>
		</div>
	</form>
</div>
<include file="Public:footer"/>