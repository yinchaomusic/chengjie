<include file="Public:header"/>
<form id="myform" method="post" action="{pigcms{:U('Footer/footer_modify')}" enctype="multipart/form-data">
	<input type="hidden" name="cat_id" value="{pigcms{$now_category.cat_id}"/>
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="80">名称</th>
			<td><input type="text" class="input fl" name="name" size="20" placeholder="请输入名称" validate="maxlength:20,required:true"/></td>
		</tr>
		<tr>
			<th width="80">标题</th>
			<td><input type="text" class="input fl" name="title" size="20" placeholder="请输入标题" validate="maxlength:20" tips="若设置了此项，将当作小副标来显示"/></td>
		</tr>
		<tr>
			<th width="80">ICON图片</th>
			<td><input type="file" class="input fl" name="pic" style="width:200px;" placeholder="请上传图片" /></td>
		</tr>
<!--		<tr>-->
<!--			<th width="80">背景颜色</th>-->
<!--			<td><input type="text" class="input fl" name="bg_color" id="choose_color" style="width:120px;" placeholder="可不填写" tips="请点击右侧按钮选择颜色，用途为如果图片尺寸小于屏幕时，会被背景颜色扩充，主要为首页使用。"/>&nbsp;&nbsp;<a href="javascript:void(0);" id="choose_color_box" style="line-height:28px;">点击选择颜色</a></td>-->
<!--		</tr>-->
		<tr>
			<th width="80">自定义链接</th>
			<td><input type="text" class="input fl" name="url" value="{pigcms{$now_link.url}" size="60" validate="maxlength:200" tips="若设置了此项，点击导航将会跳转到设定的链接"/></td>
		</tr>
		<tr>
			<th width="80">状态</th>
			<td>
				<span class="cb-enable"><label class="cb-enable selected"><span>启用</span><input type="radio" name="status" value="1" checked="checked" /></label></span>
				<span class="cb-disable"><label class="cb-disable"><span>关闭</span><input type="radio" name="status" value="0" /></label></span>
			</td>
		</tr>
		<tr>

			<th width="80">内容</th>
			<td><textarea name="content" id="content"></textarea></td>
	    </tr>
	</table>
	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>
</form>
	<script src="{pigcms{$static_public}kindeditor/kindeditor.js"></script>
	<script type="text/javascript">
		KindEditor.ready(function(K){
			kind_editor = K.create("#content",{
				width:'402px',
				height:'300px',
				resizeType : 1,
				allowPreviewEmoticons:false,
				allowImageUpload : true,
				filterMode: true,
				items : [
					'source', 'fullscreen', '|', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
					'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
					'insertunorderedlist', '|', 'emoticons', 'image', 'link'
				],
				emoticonsPath : './static/emoticons/',
				uploadJson : "{pigcms{$config.site_url}/index.php?g=Index&c=Upload&a=editor_ajax_upload&upload_dir=merchant/news"
			});
		});
	</script>
<include file="Public:footer"/>