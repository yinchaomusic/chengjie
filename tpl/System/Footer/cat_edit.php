<include file="Public:header"/>
<form id="myform" method="post" action="{pigcms{:U('Footer/amend')}" enctype="multipart/form-data">
	<input type="hidden" name="id" value="{pigcms{$now_adver.id}"/>
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="80">名称</th>
			<td><input type="text" class="input fl" name="name" value="{pigcms{$now_adver.name}" size="20" placeholder="请输入名称" validate="maxlength:20,required:true"/></td>
		</tr>
		<tr>
			<th width="80">标题</th>
			<td><input type="text" class="input fl" name="title" value="{pigcms{$now_adver.title}" size="20" placeholder="请输入标题"
			           validate="maxlength:20"
			           tips="若设置了此项，将当作小副标来显示"/></td>
		</tr>
		<tr>
			<th width="80">ICON图片</th>
			<td><img src="{pigcms{$config.site_url}/upload/domain/{pigcms{$now_adver.pic}" style="width:30px;height:30px;" class="view_msg"/></td>
		</tr>
		<tr>
			<th width="80">图片</th>
			<td><input type="file" class="input fl" name="pic" style="width:200px;" placeholder="请上传图片" tips="不修改请不上传！上传新图片，老图片会被自动删除！"/></td>
		</tr>

			<th width="80">自定义链接</th>
			<td>
				<input type="text" class="input fl" name="url" id="url" value="{pigcms{$now_adver.url}" style="width:200px;" placeholder="可不填写链接地址"tips="若设置了此项，点击导航将会跳转到设定的链接"  validate="maxlength:200,url:true"/>

			</td>
		</tr>
		<tr>
			<th width="80">状态</th>
			<td>
				<span class="cb-enable"><label class="cb-enable <if condition="$now_adver['status'] eq 1">selected</if>"><span>启用</span><input type="radio" name="status" value="1" <if condition="$now_adver['status'] eq 1">checked="checked"</if>/></label></span>
				<span class="cb-disable"><label class="cb-disable <if condition="$now_adver['status'] eq 0">selected</if>"><span>关闭</span><input type="radio" name="status" value="0" <if condition="$now_adver['status'] eq 0">checked="checked"</if>/></label></span>
			</td>
		</tr>
		<tr>

			<th width="80">内容</th>
			<td><textarea name="content" id="content">{pigcms{$now_adver.content}</textarea></td>
		</tr>
	</table>
	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>
</form>
<script src="{pigcms{$static_public}kindeditor/kindeditor.js"></script>
<script type="text/javascript">
	KindEditor.ready(function(K){
		kind_editor = K.create("#content",{
			width:'402px',
			height:'300px',
			resizeType : 1,
			allowPreviewEmoticons:false,
			allowImageUpload : true,
			filterMode: true,
			items : [
				'source', 'fullscreen', '|', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
				'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
				'insertunorderedlist', '|', 'emoticons', 'image', 'link'
			],
			emoticonsPath : './static/emoticons/',
			uploadJson : "{pigcms{$config.site_url}/index.php?g=Index&c=Upload&a=editor_ajax_upload&upload_dir=merchant/news"
		});
	});
</script>
<include file="Public:footer"/>