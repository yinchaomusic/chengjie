<include file="Public:header"/>
<form id="myform" method="post" action="{pigcms{:U('Domains/suffix_edit_save')}" >
	<input type="hidden" name="id" value="{pigcms{$now_Domain_suffix.id}">
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="80">后缀名称</th>
			<td><input type="text" class="input fl" name="suffix" size="20" value="{pigcms{$now_Domain_suffix.suffix}" placeholder="请输入后缀，如 com" validate="maxlength:50,required:true"
			           tips="域名后缀，例如：com 或者 cn"/></td>
		</tr>
		<tr>
			<th width="80">描述</th>
			<td>
				<textarea name="info" id="info" style="margin: 0px; width: 300px; height: 30px;" placeholder="可以在这里添加备注信息">{pigcms{$now_Domain_suffix.info}</textarea>
			</td>
		</tr>
		<tr>
			<th width="80">是否热门</th>
			<td>
				<span class="cb-enable"><label class="cb-enable <if condition="$now_Domain_suffix['is_hots'] eq 1">selected</if>"><span>是</span><input type="radio" name="is_hots" value="1" <if condition="$now_Domain_suffix['is_hot'] eq 1">checked="checked"</if>/></label></span>
				<span class="cb-disable"><label class="cb-disable <if condition="$now_Domain_suffix['is_hots'] eq 0">selected</if>"><span>否</span><input type="radio" name="is_hots" value="0" <if condition="$now_Domain_suffix['is_hots'] eq 0">checked="checked"</if>/></label></span>
				<em class="notice_tips" tips="如果选择热门，颜色会变"></em>
			</td>

		</tr>
		<tr>
			<th width="80">排序</th>
			<td><input type="text" class="input fl" name="sorts" size="10" value="{pigcms{$now_Domain_suffix.sorts}" validate="number:true,maxlength:6" tips="数值越大，排序越前"/></td>
		</tr>
		<tr>
			<th width="80">状态</th>
			<td class="radio_box">
				<span class="cb-enable"><label class="cb-enable <if condition="$now_Domain_suffix['status'] eq 1">selected</if>"><span>显示</span><input type="radio" name="status" value="1" <if condition="$now_Domain_suffix['status'] eq 1">checked="checked"</if>/></label></span>
				<span class="cb-disable"><label class="cb-disable <if condition="$now_Domain_suffix['status'] eq 0">selected</if>"><span>隐藏</span><input type="radio" name="status" value="0" <if condition="$now_Domain_suffix['status'] eq 0">checked="checked"</if>/></label></span>
			</td>
		</tr>
	</table>
	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>
</form>
<include file="Public:footer"/>