<include file="Public:header"/>
<form   method="post" action="{pigcms{:U('Domains/list_edit_save')}" frame="true"  >
	<input type="hidden" name="domain_id" value="{pigcms{$now_order.domain_id}"/>
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="15%">编号</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.domain_id}</div></td>
			<th width="15%">创建时间</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.add_time|date='Y-m-d H:i:s',###}</div></td>
		<tr/>
		<tr>
			<th width="15%">报价</th>
			<td width="35%"><font color="green" size="5">￥{pigcms{$now_order.money|number_format}</font> </td>
			<th width="15%">域名</th>
			<td width="35%"><font color="red" size="5">{pigcms{$now_order.domain}</font> </td>
		</tr>
		<tr>
			<th width="15%">用户ID</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.uid}</div></td>
			<th width="15%">开始时间</th>
			<td width="35%"><div style="height:24px;line-height:24px;"><if condition="$now_order['startTime']">{pigcms{$now_order.startTime|date='Y-m-d H:i:s',###}<else/>-</if></div></td>
		<tr/>
		<tr>
			<th width="15%">标签</th>
			<td width="35%">{pigcms{$now_order.label}</td>
			<th width="15%">长度 </th>
			<td width="35%" class="radio_box">{pigcms{$now_order.length}</td>
		</tr>
		<tr>
			<th width="15%">是否精品</th>
			<td width="35%">
				<span class="cb-enable"><label class="cb-enable <if condition="$now_order['is_high'] eq 1">selected</if>"><span>是</span><input type="radio" name="is_high" value="1" <if condition="$now_order['is_high'] eq 1">checked="checked"</if>/></label></span>
				<span class="cb-disable"><label class="cb-disable <if condition="$now_order['is_high'] eq 0">selected</if>"><span>否</span><input type="radio" name="is_high" value="0" <if condition="$now_order['is_high'] eq 0">checked="checked"</if>/></label></span>
			</td>
			<th width="15%">是否热门</th>
			<td width="35%">
				<span class="cb-enable"><label class="cb-enable <if condition="$now_order['is_hot'] eq 1">selected</if>"><span>是</span><input type="radio" name="is_hot" value="1" <if condition="$now_order['is_hot'] eq 1">checked="checked"</if>/></label></span>
				<span class="cb-disable"><label class="cb-disable <if condition="$now_order['is_hot'] eq 0">selected</if>"><span>否</span><input type="radio" name="is_hot" value="0" <if condition="$now_order['is_hot'] eq 0">checked="checked"</if>/></label></span>

			</td>
		</tr>
		<tr>
			<th width="15%">是否推荐</th>
			<td width="35%">
				<span class="cb-enable"><label class="cb-enable <if condition="$now_order['is_speity'] eq 1">selected</if>"><span>是</span><input type="radio" name="is_speity" value="1" <if condition="$now_order['is_speity'] eq 1">checked="checked"</if>/></label></span>
				<span class="cb-disable"><label class="cb-disable <if condition="$now_order['is_speity'] eq 0">selected</if>"><span>否</span><input type="radio" name="is_speity" value="0" <if condition="$now_order['is_speity'] eq 0">checked="checked"</if>/></label></span>

			</td>
			<th width="15%">状态</th>
			<td width="35%" class="radio_box">

					<if condition="$now_order['status'] eq 0"> 待审核</if>
					<if condition="$now_order['status'] eq 1"> 正常</if>
					<if condition="$now_order['status'] eq 2"> 售出</if>
					<if condition="$now_order['status'] eq 3"> 结束 </if>

			</td>
		</tr>
		<tr>
			<th width="15%">主题类型</th>
			<td width="85%" colspan="3">
				<select name="theme_id" id="theme_id">
					<option value="0">请选择主题类型</option>
					<volist name="treasure" id="vo">
					<option value="{pigcms{$vo.id}" <if condition="$now_order['theme_id'] eq $vo['id']">selected</if> >{pigcms{$vo
						.name}</option>
					</volist>
				</select>
			</td>
			<!--th width="15%">珍品级别</th>
			<td width="35%" class="radio_box">
				<select name="level_id" id="level_id">
					<option value="0">请选择珍品级别</option>
					<volist name="treasure_levels" id="vo">
					<option value="{pigcms{$vo.lid}"<if condition="$now_order['level_id'] eq $vo['lid']">selected</if>>{pigcms{$vo
						.level_name}</option>
					</volist>
				</select>
			</td-->
		</tr>
		<tr>
			<th width="15%">类型</th>
			<td width="35%">
					  <if condition="$now_order['type'] eq 0">议价</if>
					  <if condition="$now_order['type'] eq 1"> 一口价 </if>
					 <if condition="$now_order['type'] eq 2"> 竞价  </if>
				    <if condition="$now_order['type'] eq 3"> 优质 </if>

			</td>
			<th width="15%">查看次数</th>
			<td width="35%">
				 {pigcms{$now_order.hits}
			</td>
		</tr>
<!--		<tr>-->
<!--			<th width="15%">订单状态修改</th>-->
<!--			<td width="35%">-->
<!--				<span class="cb-enable"><label class="cb-enable <if condition="$now_order['yes_no'] neq 7">selected</if>"><span>正常</span><input type="radio" name="yes_no" value="" <if condition="$now_order['yes_no'] neq 7">checked="checked"</if>/></label></span>-->
<!--				<span class="cb-disable"><label class="cb-disable <if condition="$now_order['yes_no'] eq 7">selected</if>"><span>关闭</span><input type="radio" name="yes_no" value="7" <if condition="$now_order['yes_no'] eq 7">checked="checked"</if>/></label></span>-->
<!--				<br /> <br />-->
<!--				注意：一旦订单关闭之后不可以再开启，请谨慎操作！-->
<!--			</td>-->
<!--			<th width="15%">关闭说明</th>-->
<!--			<td width="35%">-->
<!--				<textarea name="info" id="info" cols="30"  rows="5">{pigcms{$order_info.info} </textarea>-->
<!--			</td>-->
<!--		</tr>-->
<!--		<tr>-->
<!--			<th width="15%">记录表</th>-->
<!--			<td width="85%" colspan="3">-->
<!--				<div style="height:30px;line-height:24px;">-->
<!--					<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Trade/operalog',array('order_id'=>$now_order['order_id']))}','订单操作记录',680,560,true,false,false,null,'operalog',true);">订单操作记录</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
<!--				</div>-->
<!--			</td>-->
<!--		</tr>-->
	</table>
	<if condition="$now_order['yes_no'] neq 7">
		<div class="btn hidden">
			<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
			<input type="reset" value="取消" class="button" />
		</div>
		<else/>
		该订单已经关闭
	</if>
</form>
<include file="Public:footer"/>