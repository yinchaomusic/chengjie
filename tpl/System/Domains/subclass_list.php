<include file="Public:header"/>
<div class="mainbox">
	<div id="nav" class="mainnav_title">
		<ul>
			<a href="{pigcms{:U('Domains/index')}" class="on">域名分类列表</a>|
			<a href="{pigcms{:U('Domains/subclass_list',array('id'=>$subclass['id']))}" class="on">{pigcms{$subclass.name} -
				列表</a>|
			<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Domains/subclass_add',array('id'=>$subclass['id']))}','添加子分类',800,460,true,false,false,addbtn,'add',true);">添加子分类</a>
		</ul>
	</div>
	<form name="myform" id="myform" action="" method="post">
		<div class="table-list">
			<table width="100%" cellspacing="0">
				<colgroup><col> <col> <col> <col><col><col><col> <col width="140" align="center"> </colgroup>
				<thead>
				<tr>
					<th>排序</th>
					<th>编号</th>
					<th>名称</th>
					<th>短标记</th>
					<th>查看子分类</th>
					<th>域名数量</th>
					<th>状态</th>
					<th>是否热门</th>
					<th>是否推荐</th>
					<th class="textcenter">操作</th>
				</tr>
				</thead>
				<tbody>
				<if condition="is_array($subclass_list)">
					<volist name="subclass_list" id="vo">
						<tr>
							<td>{pigcms{$vo.sorts}</td>
							<td>{pigcms{$vo.id}</td>
							<td>{pigcms{$vo.name}</td>
							<td>{pigcms{$vo.shortag}</td>
							<td><a href="{pigcms{:U('Domains/subclass_list', array('id'=>$vo['id'],'pid'=>$vo['pid']))}">查看(编辑)</a></td>
							<td>{pigcms{$vo.numbers}</td>
							<td><if condition="$vo['status']"><font color="green">显示</font><else/><font color="red">隐藏</font></if></td>
							<td><if condition="$vo['is_hot']"><font color="#b22222">是</font><else/><font color="#a9a9a9">否</font></if></td>
							<td><if condition="$vo['is_push'] eq 1"><font color="red">首页推荐</font><elseif  condition="$vo['is_push'] eq 2"/>推荐分类页
									<elseif condition="$vo['is_push'] eq 3"/>全站推荐<else/><font
										color="#a9a9a9">默认</font></if></td>
							<td class="textcenter">  <a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Domains/subclass_edit',array('id'=>$vo['id']))}','编辑分类',800,460,true,false,false,editbtn,'add',true);">编辑</a> | <a href="javascript:void(0);" class="delete_row" parameter="id={pigcms{$vo['id']}" url="{pigcms{:U('Domains/del')}">删除</a></td>
						</tr>
					</volist>
					<tr><td class="textcenter pagebar" colspan="12">{pigcms{$pagebar}</td></tr>
					<else/>
					<tr><td class="textcenter red" colspan="12">列表为空！</td></tr>
				</if>
				</tbody>
			</table>
		</div>
	</form>
</div>
<include file="Public:footer"/>