<include file="Public:header"/>
<form id="myform" method="post" action="{pigcms{:U('Domains/edit_modify')}" >
	<input type="hidden" name="id" value="{pigcms{$now_domain_calssify.id}">
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="80">分类名称</th>
			<td><input type="text" class="input fl" name="name" id="name" size="25" value="{pigcms{$now_domain_calssify.name}"  placeholder="请输入分类名称" validate="maxlength:20,required:true"
			           tips=""/></td>
		</tr>
		<tr>
			<th width="80">短标记</th>
			<td><input type="text" class="input fl" name="shortag" id="shortag" size="25" value="{pigcms{$now_domain_calssify.shortag}" placeholder="请输入简单好记的标签" validate="maxlength:20" tips="建议使用英文或数字或者建议使用分类的拼音"/></td>
		</tr>
		<tr>
			<th width="80">是否热门</th>
			<td>
				<span class="cb-enable"><label class="cb-enable <if condition="$now_domain_calssify['is_hot'] eq 1">selected</if>"><span>是</span><input type="radio" name="is_hot" value="1" <if condition="$now_domain_calssify['is_hot'] eq 1">checked="checked"</if>/></label></span>
				<span class="cb-disable"><label class="cb-disable <if condition="$now_domain_calssify['is_hot'] eq 0">selected</if>"><span>否</span><input type="radio" name="is_hot" value="0" <if condition="$now_domain_calssify['is_hot'] eq 0">checked="checked"</if>/></label></span>
				<em class="notice_tips" tips="如果选择热门，颜色会变"></em>
			</td>

		</tr>
		<tr>
			<th width="100">是否推荐</th>
			<td>
				<select name="is_push" id="is_push">
					<option value="0" <if condition="$now_domain_calssify['is_push'] eq 0">selected</if>>默认</option>
					<option value="1" <if condition="$now_domain_calssify['is_push'] eq 1">selected</if>>首页推荐</option>
					<option value="2"<if condition="$now_domain_calssify['is_push'] eq 2">selected</if>>推荐分类页</option>
					<option value="3"<if condition="$now_domain_calssify['is_push'] eq 3">selected</if>>全站推荐</option>
				</select>

				<em class="notice_tips" tips="是否做该分类的主推：默认，在首页推荐，在分类页推荐，全站推荐"></em>
			</td>
		</tr>
		<if condition="$pfcid eq 0">
			<tr>
				<th width="80">分类排序</th>
				<td><input type="text" class="input fl" name="sorts" value="{pigcms{$now_domain_calssify.sorts}" size="10" placeholder="分类排序" validate="maxlength:6,number:true" tips="默认添加时间排序！手动排序数值越大，排序越前。"/></td>
			</tr>

			<tr>
				<th width="80">分类状态</th>
				<td>
					<span class="cb-enable"><label class="cb-enable <if condition="$now_domain_calssify['status'] eq 1">selected</if>"><span>启用</span><input type="radio" name="status" value="1" <if condition="$now_domain_calssify['status'] eq 1">checked="checked"</if>/></label></span>
		<span class="cb-disable"><label class="cb-disable <if condition="$now_domain_calssify['status'] eq 0">selected</if>"><span>关闭</span><input type="radio" name="status" value="0" <if condition="$now_domain_calssify['status'] eq 0">checked="checked"</if>/></label></span>

		</td>
			</tr>
			<tr>
				<th width="80">备注信息</th>
				<td>
					<textarea name="info" id="info" style="margin: 0px; width: 554px; height: 139px;" placeholder="可以在这里添加备注信息">{pigcms{$now_domain_calssify.info}</textarea>
				</td>
			</tr>
		</if>


	</table>
	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>
</form>

<include file="Public:footer"/>