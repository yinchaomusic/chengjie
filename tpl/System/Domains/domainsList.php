<include file="Public:header"/>
<div class="mainbox">
	<div id="nav" class="mainnav_title">
		<ul>
			<a href="{pigcms{:U('Domains/domainsList')}" class="on">域名出售列表</a>|
<!--			<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Domains/add')}','添加域名分类',800,460,true,false,false,addbtn,'add',true);">添加域名分类</a>-->
		</ul>
	</div>
	<table class="search_table" width="100%">
		<tr>
			<td>
				<form action="{pigcms{:U('Domains/domainsList')}" method="get">
					<input type="hidden" name="c" value="Domains"/>
					<input type="hidden" name="a" value="domainsList"/>

					筛选: <input type="text" name="keyword" class="input-text" value="{pigcms{$_GET['keyword']}"/>
					<select name="searchtype">
						<option value="domain_id" <if condition="$_GET['searchtype'] eq 'domain_id'">selected="selected"</if>>编号</option>
						<option value="domain" <if condition="$_GET['searchtype'] eq 'domain'">selected="selected"</if>>域名</option>
						<option value="uid" <if condition="$_GET['searchtype'] eq 'uid'">selected="selected"</if>>用户ID</option>
					</select> &nbsp;
					<input type="submit" value="查询" class="button"/>
				</form>
			</td>
		</tr>
	</table>
	<form name="myform" id="myform" action="" method="post">
		<div class="table-list">
			<table width="100%" cellspacing="0">
				<colgroup><col> <col> <col> <col><col><col><col> <col width="140" align="center"> </colgroup>
				<thead>
				<tr>
					<th>编号</th>
					<th>用户ID</th>
					<th>域名</th>
					<th>买家报价</th>
					<th>添加时间</th>
					<th>开始时间</th>
					<th>标签</th>
					<th>域名长度</th>
					<th>是否精品</th>
					<th>是否热门</th>
					<th>是否推荐</th>
					<th>状态</th>
					<th>类型</th>
					<th>主题</th>
					<!--th>珍品级别</th-->
					<th>查看次数</th>
					<th class="textcenter">操作</th>
				</tr>
				</thead>
				<tbody>
				<if condition="is_array($domains_list)">
					<volist name="domains_list" id="vo">
						<tr>
							<td>{pigcms{$vo.domain_id}</td>
							<td>{pigcms{$vo.uid}</td>
							<td>{pigcms{$vo.domain}</td>
							<td><font color="#228b22">￥{pigcms{$vo.money|number_format}</font> </td>
							<td>{pigcms{$vo.add_time|date='Y-m-d H:i:s',###}</td>
							<td><if condition="$vo['startTime']">{pigcms{$vo.startTime|date='Y-m-d H:i:s',###}<else/>-</if></td>
							<td>{pigcms{$vo.label}</td>
							<td>{pigcms{$vo.length}</td>
							<td><if condition="$vo['is_high'] eq 1"><font color="blue">是</font><else/><font color="#87cefa">否</font></if></td>
							<td><if condition="$vo['is_hot'] eq 1"><font color="#b22222">是</font><else/><font color="#a9a9a9">否</font></if></td>
							<td><if condition="$vo['is_speity'] eq 1"><font color="orangered">是</font><else/><font color="#deb887">否</font></if></td>

							<td><if condition="$vo['status'] eq 1"><font color="green">正常</font><elseif  condition="$vo['status'] eq 2"/>售出<elseif  condition="$vo['status'] eq 3"/>结束<else/>待审核</if></td>

							<td><if condition="$vo['type'] eq 1">一口价<elseif  condition="$vo['type'] eq 2"/>竞价
									<elseif condition="$vo['type'] eq 3"/>优质<else/>议价</if></td>

							<td>{pigcms{$vo.treasure.name}</td>
							<!--td>{pigcms{$vo.treasure_levels.level_name}</td-->
							<td>{pigcms{$vo.hits}</td>
							<td class="textcenter">  <a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Domains/list_edit',array('domain_id'=>$vo['domain_id']))}','编辑域名',800,460,true,false,false,editbtn,'add',true);">编辑</a> | <a href="javascript:void(0);" class="delete_row" parameter="domain_id={pigcms{$vo['domain_id']}" url="{pigcms{:U('Domains/list_del')}">删除</a></td>
						</tr>
					</volist>
					<tr><td class="textcenter pagebar" colspan="16">{pigcms{$pagebar}</td></tr>
					<else/>
					<tr><td class="textcenter red" colspan="16">列表为空！</td></tr>
				</if>
				</tbody>
			</table>
		</div>
	</form>
</div>
<include file="Public:footer"/>