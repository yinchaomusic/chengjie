<include file="Public:header"/>
<div class="mainbox">
	<div id="nav" class="mainnav_title">
		<ul>
			<a href="{pigcms{:U('Domains/suffix')}" class="on">域名后缀列表</a>|
			<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Domains/suffix_add')}','添加后缀',520,250,true,false,false,addbtn,'add',true);">添加后缀</a>
		</ul>
	</div>
	<form name="myform" id="myform" action="" method="post">
		<div class="table-list">
			<table width="100%" cellspacing="0">
				<colgroup><col> <col> <col> <col><col><col><col> <col width="140" align="center"> </colgroup>
				<thead>
				<tr>
					<th>后缀</th>
					<th>描述</th>
					<th>热门</th>
					<th>排序</th>
					<th>编号</th>
					<th>状态</th>
					<th class="textcenter">操作</th>
				</tr>
				</thead>
				<tbody>
				<if condition="is_array($suffix_list)">
					<volist name="suffix_list" id="vo">
						<tr>
							<td>{pigcms{$vo.suffix}</td>
							<td>{pigcms{$vo.info}</td>
							<td><if condition="$vo['is_hots']"><font color="red">是</font><else/>否</if></td>
							<td>{pigcms{$vo.sorts}</td>
							<td>{pigcms{$vo.id}</td>
							<td><if condition="$vo['status']"><font color="green">显示</font><else/><font color="red">隐藏</font></if></td>
							<td class="textcenter"> <a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Domains/suffix_edit',array('id'=>$vo['id']))}','编辑后缀',520,250,true,false,false,editbtn,'add',true);">编辑</a> | <a href="javascript:void(0);" class="delete_row" parameter="id={pigcms{$vo.id}" url="{pigcms{:U('Domains/suffix_del')}">删除</a></td>
						</tr>
					</volist>
					<tr><td class="textcenter pagebar" colspan="7">{pigcms{$pagebar}</td></tr>
					<else/>
					<tr><td class="textcenter red" colspan="7">列表为空！</td></tr>
				</if>
				</tbody>
			</table>
		</div>
	</form>
</div>
<include file="Public:footer"/>