<include file="Public:header"/>
<form   method="post" action="{pigcms{:U('Terminal/edit_save')}" frame="true"  >
	<input type="hidden" name="id" value="{pigcms{$show.id}"/>
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="15%">id</th>
			<td width="35%">{pigcms{$show.id}</td>
			<th width="15%">域名</th>
                        <td width="35%">{pigcms{$show.domain_name}</td>
		</tr>
                <tr>
			<th width="15%">期望价格</th>
			<td width="35%">{pigcms{$show.max_price}</td>
			<th width="15%">最低价格</th>
			<td width="35%">{pigcms{$show.min_price}</td>
		</tr>
                <tr>
			<th width="15%">希望的终端</th>
			<td width="35%">{pigcms{$show.hope}</td>
			<th width="15%">状态</th>
			<td width="35%">
                            <if condition="$show['status'] eq 0">
                                <font style="color:red">待审核</font>
                                <elseif condition="$show['status'] eq 1"/>
                                <font style="color:aqua">推荐中</font>
                                <elseif condition="$show['status'] eq 2"/>
                                <font style="color:#000080">审核失败</font>
                                <elseif condition="$show['status'] eq 3"/>
                                <font style="color:#0f0">推荐成功</font>
                                <elseif condition="$show['status'] eq 4"/>
                                <font style="color:#1c6a9e">推荐失败</font>
                            </if>
                        </td>
		</tr>
                <tr>
			<th width="15%">用户</th>
			<td width="35%">{pigcms{$show.uid}</td>
			<th width="15%">经纪人id</th>
			<td width="35%">{pigcms{$show.staff_id}</td>
		</tr>
                
                <tr>
			<th width="15%">留言</th>
			<td width="35%">{pigcms{$show.tt_msg}</td>
			<th width="15%">添加时间</th>
			<td width="35%">{pigcms{$show.add_time|date="Y-m-d H:i:s",###}</td>
		</tr>
                <tr>
			<th width="15%">状态编辑</th>
			<td width="35%">
                            <div style="height:24px;line-height:24px;">
					<select name="status" id="state">
                                            <option value="0"<if condition="$show['status'] eq 0">selected</if>>待审核</option>
                                            <option value="1"<if condition="$show['status'] eq 1">selected</if>>推荐中</option>
                                            <option value="2"<if condition="$show['status'] eq 2">selected</if>>审核失败</option>
                                            <option value="3"<if condition="$show['status'] eq 3">selected</if>>推荐成功</option>
                                            <option value="4"<if condition="$show['status'] eq 4">selected</if>>推荐失败</option>
					</select>
				</div>
                        </td>
			<th width="15%">原因</th>
			<td width="35%">
                            <textarea name="reason"  cols="30" rows="1">{pigcms{$show['reason']}</textarea> 
                        </td>
		</tr>
	</table>
	<if condition="$now_order['yes_no'] neq 7">
	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>
		<else/>
		该订单已经关闭
	</if>
</form>
<include file="Public:footer"/>