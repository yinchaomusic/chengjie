<include file="Public:header" xmlns="http://www.w3.org/1999/html"/>
<div class="mainbox">
	<table class="search_table" width="100%">
		<tr>
			<td>
				<form action="{pigcms{:U('Terminal/index')}" method="post">
					筛选: <input type="text" name="keyword" class="input-text" />
					<select name="searchtype">
						<option value="domain_name">域名</option>
						<option value="staff_id">经纪人ID</option>
                                                <option value="uid">用户id</option>
					</select> &nbsp;
					交易状态:
					<select name="status">
						<option value="">全部</option>
						<option value="0">待审核</option>
                                                <option value="1">推荐中</option>
                                                <option value="2">审核失败</option>
                                                <option value="3">推荐成功</option>
                                                <option value="4">推荐失败</option>
					</select>&nbsp;
					<input type="submit" value="查询" class="button"/>
				</form>
			</td>
		</tr>
	</table>
	<form name="myform" id="myform" action="" method="post">
		<div class="table-list">
			<table width="100%" cellspacing="0">
				<colgroup><col> <col> <col> <col><col><col><col> <col width="140" align="center"> </colgroup>
				<thead>
				<tr>
					<th>域名</th>
					<th>期望价格(￥)</th>
					<th>最低价格(￥)</th>
					<th>希望的终端</th>
					<th>状态</th>
					<th>用户ID</th>
					<th>经纪人ID</th>
					<th>留言</th>
					<th>添加时间</th>
					<th>原因</th>
					<th class="textcenter">操作</th>
				</tr>
				</thead>
				<tbody>
				<if condition="is_array($list)">
					<volist name="list" id="vo">
						<tr>
                                                        <td>{pigcms{$vo.domain_name}</td>
                                                        <td>￥{pigcms{$vo.max_price|number_format}</td>
                                                        <td>￥{pigcms{$vo.min_price|number_format}</td>
                                                        <td>{pigcms{$vo.hope}</td>
                                                        <td>
                                                            <if condition="$vo['status'] eq 0">
                                                                <font style="color:red">待审核</font>
                                                                <elseif condition="$vo['status'] eq 1"/>
                                                                <font style="color:aqua">推荐中</font>
                                                                <elseif condition="$vo['status'] eq 2"/>
                                                                <font style="color:#000080">审核失败</font>
                                                                <elseif condition="$vo['status'] eq 3"/>
                                                                <font style="color:#0f0">推荐成功</font>
                                                                <elseif condition="$vo['status'] eq 4"/>
                                                                <font style="color:#1c6a9e">推荐失败</font>
                                                            </if>
                                                        </td>
                                                        <td>{pigcms{$vo.uid}</td>
                                                        <td>{pigcms{$vo.staff_id}</td>
                                                        <td>{pigcms{$vo.tt_msg}</td>
							<td>{pigcms{$vo.add_time|date='Y-m-d',###}</td>
							<td>{pigcms{$vo.reason}</td>
							<td class="textcenter">
								 <a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Terminal/edit',array('id'=>$vo['id']))}','编辑',680,560,true,false,false,editbtn,'add',true);">编辑</a> 
							</td>
						</tr>
					</volist>
					<tr><td class="textcenter pagebar" colspan="14">{pigcms{$pagebar}</td></tr>
					<else/>
					<tr><td class="textcenter red" colspan="12">列表为空！</td></tr>
				</if>
				</tbody>
			</table>
		</div>
	</form>
</div>
<include file="Public:footer"/>