<include file="Public:header"/>
	
<form id="myform" method="post" action="{pigcms{:U('About/add')}" enctype="multipart/form-data">
		<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
			<tr>
				<th width="80">文章标题</th>
                                <input type="hidden" name="id" value="{pigcms{$ab_list.id}"/>
				<td><input type="text" class="input fl" name="title" size="20" placeholder="请输入标题" value="{pigcms{$ab_list.title}" validate="maxlength:20,required:true"/></td>
			</tr>
                        <tr>
				<th width="80">调用页面</th>
				<td><input type="text" class="input fl" name="page" size="20" placeholder="请输入页面如：aoubt"   tips="一旦添加则不可以修改,必须是英文" value="{pigcms{$ab_list.page}" validate="maxlength:20,required:true"/>

				</td>
			</tr>
                        <tr>
                                <th width="80">文章状态</th>
                                <td>
                                        <span class="cb-enable"><label class="cb-enable <if condition="$ab_list['state'] eq 0">selected</if>"><span>启用</span><input type="radio" name="state" value="0" <if condition="$ab_list['state'] eq 0">checked="checked"</if>/></label></span>
                                        <span class="cb-disable"><label class="cb-disable <if condition="$ab_list['state'] eq 1">selected</if>"><span>关闭</span><input type="radio" name="state" value="1" <if condition="$ab_list['state'] eq 1">checked="checked"</if>/></label></span>
                                </td>
                        </tr>
                
                        <tr>
                                <th width="80">文章内容</th>
                                <td>
                                        <textarea id="description" name="content"  placeholder="可以在这里添加新闻内容">{pigcms{$ab_list['content']|htmlspecialchars_decode=ENT_QUOTES}</textarea>
                                </td>
                        </tr>
		</table>
		<div class="btn hidden">
			<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
			<input type="reset" value="取消" class="button" />
		</div>
	</form>

<link rel="stylesheet" href="{pigcms{$static_public}kindeditor/themes/default/default.css">
<script src="{pigcms{$static_public}kindeditor/kindeditor.js"></script>
<script src="{pigcms{$static_public}kindeditor/lang/zh_CN.js"></script>
<script type="text/javascript">

KindEditor.ready(function(K){
	var editor = K.editor({
		allowFileManager : true
	});
	 //var islock=false;
	K('.J_selectImage').click(function(){
		var obj=$(this);
		editor.uploadJson = "{pigcms{$config.site_url}/index.php?g=Index&c=Upload&a=editor_ajax_upload&upload_dir=system/image";
		editor.loadPlugin('image', function(){
			editor.plugin.imageDialog({
				showRemote : false,
				imageUrl : K('#course_pic').val(),
				clickFn : function(url, title, width, height, border, align) {
					obj.siblings('input').val(url);
					editor.hideDialog();
					obj.siblings('img').attr('src',url).show();
					//window.location.reload();
				}
			});
		});
	   
	});

	kind_editor = K.create("#description",{
		width:'500px',
		height:'380px',
		minWidth:'480px',
		resizeType : 1,
		allowPreviewEmoticons:false,
		allowImageUpload : true,
		filterMode: true,
		items : [
			'source', '|', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
			'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
			'insertunorderedlist', '|', 'emoticons', 'image', 'link'
		],
		emoticonsPath : './static/emoticons/',
		uploadJson : "{pigcms{$config.site_url}/index.php?g=Index&c=Upload&a=editor_ajax_upload&upload_dir=system/image"
	});
});
</script>
<include file="Public:footer"/>