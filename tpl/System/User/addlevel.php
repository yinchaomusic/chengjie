<include file="Public:header"/>
	<form method="post" action="{pigcms{:U('User/addlevel')}" enctype="multipart/form-data" >
		<input type="hidden" name="lid" value="{pigcms{$leveldata['id']}"/>
		<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
			<tr>
				<td width="80">等级名称：</td>
				<td>
				<input type="text" class="input fl" name="lname" value="{pigcms{$leveldata['lname']}" placeholder="请填写一个等级名" tips="如vip1，vip2等">
				&nbsp;&nbsp;&nbsp;<span class="red">例如：1=>VIP1,2=>VIP2 等</span>
				</td>
			</tr>
			<tr>
				<td width="80">等级级别：</td>
				<td>
				<span class="input fl" style="width: 140px;">{pigcms{$leveldata['level']}</span>
				&nbsp;&nbsp;&nbsp;<span class="red">例如：1=>VIP1,2=>VIP2 等</span>
				</td>
			</tr>
                        <tr>
				<td width="80">等级图标：</td>
				<td><input type="file" class="input fl" name="img" style="width:135px;" placeholder="请上传图片" /></td>
			</tr>
			<tr>
				<td width="80">一口价数量：</td>
				<td>
				<input type="text" class="input fl" name="number" value="{pigcms{$leveldata['number']}" placeholder="请填写一个对应数字" onkeyup="value=value.replace(/[^1234567890]+/g,'')" tips="成为该等级会员所需要的积分数">
				&nbsp;&nbsp;&nbsp;<span class="red">客户达到等级可以发布的一口价交易数量</span>
				</td>
			</tr>
                        <tr>
				<td width="80">等级积分：</td>
				<td>
				<input type="text" class="input fl" name="integral" value="{pigcms{$leveldata['integral']}" placeholder="请填写一个对应数字" onkeyup="value=value.replace(/[^1234567890]+/g,'')" tips="成为该等级会员所需要的积分数">
				&nbsp;&nbsp;&nbsp;<span class="red">客户想成为该等级会员所需要消耗的积分数</span>
				</td>
			</tr>
		   <tr>
				<td width="80">等级福利：</td>
				<td>
				 <input type="text" class="input" name="boon" value="{pigcms{$leveldata['boon']}" placeholder="请填写一个优惠值数字" onkeyup="value=value.replace(/[^1234567890]+/g,'')" >
                                 &nbsp;&nbsp;&nbsp;<span class="red">到达等级所收取的手续费数量</span>
				</td>
			</tr>
		   <tr>
		</table>
		<div class="btn hidden">
			<input type="submit" id="dosubmit" value="提交" class="button" />
			<input type="reset" value="取消" class="button" />
		</div>
	</form>
<include file="Public:footer"/>