<include file="Public:header"/>
<div class="mainbox">


	<div class="table-list">
		<table class="search_table" width="100%">
			<tr>
				<td>
					<form action="{pigcms{:U('Financial/frozen')}" method="get">
						<input type="hidden" name="c" value="Financial"/>
						<input type="hidden" name="a" value="frozen"/>
						筛选:  <input type="text" name="keyword" class="input-text" value="{pigcms{$_GET['keyword']}"/>
						<select name="searchtype">
							<option value="uid" <if condition="$_GET['searchtype'] eq 'uid'">selected="selected"</if>>用户ID</option>
						</select>
						<input type="submit" value="查询" class="button"/>
					</form>
				</td>
			</tr>
		</table>
		<table width="100%" cellspacing="0">
			<colgroup>
				<col/>
				<col/>
				<col/>
				<col/>
				<col/>
				<col/>
				<col width="180" align="center"/>
			</colgroup>
			<thead>
			<tr>
				<th>用户ID</th>
				<th>冻结时间</th>
				<th>冻结金额</th>
				<th>冻结说明</th>
				<th>冻结状态</th>
<!--				<th>解冻时间</th>-->
<!--				<th  >操作</th>-->
			</tr>
			</thead>
			<tbody>
			<if condition="is_array($user_freeze_list)">
				<volist name="user_freeze_list" id="vo">
					<tr>

						<td>{pigcms{$vo.uid}</td>
						<td>{pigcms{$vo.freezetime|date="Y-m-d H:i:s",###}</td>
						<td>￥{pigcms{$vo.freezemoney|number_format}</td>
						<td>{pigcms{$vo.info}</td>
						<td><if condition="$vo['status'] eq 0"><font color="red">冻结</font><elseif condition="$vo['status'] eq 1" /><font
									color="green">
									解冻
								</font><elseif condition="$vo['status'] eq 2" /> <font color="#a52a2a">违约金</font> </if></td>
<!--						<td><if condition="$vo['status'] eq 1"> {pigcms{$vo.thawtime|date="Y-m-d H:i:s",###} <else /> - </if> </td>-->
<!--						<td>-->
<!--<!--							<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Financial/show',array('id'=>$vo['bank_card_id'],'frame_show'=>true))}','查看银行卡详情',400,400,true,false,false,false,'add',true);">查看</a>-->
<!---->
<!--						<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Financial/frozen_edit',array('id'=>$vo['id']))}','编辑',680,250,true,false,false,editbtn,'add',true);">编辑</a> | <a href="javascript:void(0);" class="delete_row" parameter="id={pigcms{$vo.id}" url="{pigcms{:U('Financial/frozen_del')}">删除</a>-->
<!--						</td>-->
					</tr>
				</volist>
				<tr><td class="textcenter pagebar" colspan="11">{pigcms{$pagebar}</td></tr>
				<else/>
				<tr><td class="textcenter red" colspan="11">列表为空！</td></tr>
			</if>
			</tbody>
		</table>
	</div>

</div>
<include file="Public:footer"/>