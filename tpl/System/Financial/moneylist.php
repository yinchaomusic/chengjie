<include file="Public:header"/>
<div class="mainbox">

	<div class="table-list">
		<table class="search_table" width="100%">
			<tr>
				<td>
					<form action="{pigcms{:U('Financial/moneylist')}" method="get">
						<input type="hidden" name="c" value="Financial"/>
						<input type="hidden" name="a" value="moneylist"/>
						筛选: <input type="text" name="keyword" class="input-text" value="{pigcms{$_GET['keyword']}"/>
						<select name="searchtype">
							<option value="uid" <if condition="$_GET['searchtype'] eq 'uid'">selected="selected"</if>>用户ID</option>
						</select>
						<input type="submit" value="查询" class="button"/>
					</form>
				</td>
			</tr>
		</table>
		<table width="100%" cellspacing="0">
			<colgroup>
				<col/>
				<col/>
				<col/>
				<col/>
				<col/>
				<col/>
				<col width="180" align="center"/>
			</colgroup>
			<thead>
			<tr>
				<th>UID</th>
				<th>用户昵称</th>
				<th>操作时间</th>
				<th>操作金额</th>
				<th>原始金额</th>
				<th>流水类型</th>
				<th>操作类型</th>
				<th>说明</th>
				<th class="textcenter">操作</th>
			</tr>
			</thead>
			<tbody>
			<if condition="is_array($datalist)">
				<volist name="datalist" id="vo">
					<tr>

						<td>{pigcms{$vo.uid}</td>
						<td>{pigcms{$vo.username}</td>
						<td>{pigcms{$vo.time|date="Y-m-d H:i:s",###}</td>
						<td>￥{pigcms{$vo.money|number_format}</td>
						<td>￥{pigcms{$vo.now_money|number_format}</td>
						<td>
							<if condition="$vo['type'] eq 1">
								<font color="blue">充值</font>
								<elseif condition="$vo['type'] eq 2"/>
								<font color="green">提现</font>
								<elseif condition="$vo['type'] eq 3"/>
								<font color="red">交易收入</font>
								<elseif condition="$vo['type'] eq 4"/>交易支出
								<elseif condition="$vo['type'] eq 5"/>违约补偿
								<elseif condition="$vo['type'] eq 6"/>违约扣款
								<elseif condition="$vo['type'] eq 7"/>下线提成
								<elseif condition="$vo['type'] eq 12"/>冻结记录
								<elseif condition="$vo['type'] eq 13"/>冻结记录
							</if>
						</td>
						<td>
							<if condition="$vo['typeofs'] eq 0">
								<font color="blue">域名交易</font>
								<elseif condition="$vo['typeofs'] eq 1"/>
								<font color="green">贷款交易</font>
								<elseif condition="$vo['typeofs'] eq 3"/>
								<font color="red">任务交易</font>
							</if>
						</td>
						<td>{pigcms{$vo.desc}</td>
						<td class="textcenter">
							<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Financial/moneylist_edit',array('id'=>$vo['pigcms_id']))}','编辑',680,250,true,false,false,'','add',true);">查看</a>
						</td>


					</tr>
				</volist>
				<tr><td class="textcenter pagebar" colspan="11">{pigcms{$pagebar}</td></tr>
				<else/>
				<tr><td class="textcenter red" colspan="11">列表为空！</td></tr>
			</if>
			</tbody>
		</table>
	</div>

</div>
<include file="Public:footer"/>