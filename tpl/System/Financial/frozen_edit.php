<include file="Public:header"/>
<form   method="post" action="{pigcms{:U('Financial/frozen_edit_save')}" frame="true"  >
	<input type="hidden" name="id" value="{pigcms{$now_user_freeze.id}"/>
	<input type="hidden" name="uid" value="{pigcms{$now_user_freeze.uid}"/>
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="15%">用户ID</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_user_freeze.uid}</div></td>
			<th width="15%">冻结时间</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_user_freeze.freezetime|date='Y-m-d H:i:s',###}</div></td>
		<tr/>
		<tr>
			<th width="15%">冻结金额(￥)</th>
			<td width="35%">￥{pigcms{$now_user_freeze.freezemoney|number_format}</td>
			<th width="15%">关闭说明</th>
			<td width="35%">
				<textarea name="info" id="info" cols="30"  rows="5">{pigcms{$now_user_freeze.info} </textarea>
			</td>
		</tr>
		<tr>
			<th width="15%">冻结状态</th>
			<td width="35%"><div style="height:24px;line-height:24px;">	<span class="cb-enable"><label class="cb-enable <if condition="$now_user_freeze['status'] eq 1">selected</if>"><span>解冻</span><input type="radio" name="status" value="1" <if condition="$now_user_freeze['status'] eq 1">checked="checked"</if>/></label></span>
					<span class="cb-disable"><label class="cb-disable <if condition="$now_user_freeze['status'] eq 0">selected</if>"><span>冻结</span><input type="radio" name="status" value="0" <if condition="$now_user_freeze['status'] eq 0">checked="checked"</if>/></label></span> </div></td>

			<th width="15%">解冻时间 </th>
			<td width="35%"><div style="height:24px;line-height:24px;"><if condition="$now_user_freeze['status'] eq 1"> {pigcms{$now_user_freeze.thawtime|date='Y-m-d H:i:s',
						###} <else /> - </if></div></td>

		<tr/>

	</table>
	<if condition="$now_user_freeze['status'] neq 2">
		<div class="btn hidden">
			<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
			<input type="reset" value="取消" class="button" />
		</div>
	</if>
</form>
<include file="Public:footer"/>