<include file="Public:header"/>
<form   method="post" action="{pigcms{:U('Financial/state_changes_save')}" frame="true"  >
	<input type="hidden" name="id" value="{pigcms{$now_user_record.id}"/>
	<input type="hidden" name="uid" value="{pigcms{$now_user_record.uid}"/>
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="15%">用户ID</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_user_record.uid}</div></td>
			<th width="15%">申请时间</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_user_record.addtime|date='Y-m-d H:i:s',###}</div></td>
		<tr/>
		<tr>
			<th width="15%">提现金额(￥)</th>
			<td width="35%">￥{pigcms{$now_user_record.txtmoeny|number_format}</td>
			<input type="hidden" name="txtmoeny" value="{pigcms{$now_user_record.txtmoeny}">
			<th width="15%">提现账户</th>
			<td width="35%"> {pigcms{$now_bank_card.account}</td>
		</tr>
		<tr>
			<th width="15%">手续费(￥)</th>
			<td width="35%"><input type="number" name="decMenory" class="input-text"  value="{pigcms{$now_user_record.decMenory}"></td>
			<th width="15%"> </th>
			<td width="35%"> </td>
		</tr>
		<tr>
			<th width="15%">支行名称</th>
			<td width="35%">{pigcms{$now_user_record.subBank}</td>
			<th width="15%">提现银行</th>
			<td width="35%"> {pigcms{$now_bank_card.bankName}</td>
		</tr>
		<tr>
			<th width="15%">状态</th>
			<td width="35%"><div style="height:24px;line-height:24px;">
					<select name="state" id="state">
						<option value="0" <if condition="$now_user_record['state'] eq 0">selected</if>>提现申请中</option>
						<option value="1"<if condition="$now_user_record['state'] eq 1">selected</if>>汇款</option>
						<option value="2"<if condition="$now_user_record['state'] eq 2">selected</if>>拒绝</option>
					</select>
				</div></td>

			<th width="15%">说明 </th>
			<td width="35%"><div style="height:24px;line-height:24px;"><textarea name="info"  cols="30" rows="1">{pigcms{$now_user_record['info']}</textarea> </div></td>

		<tr/>

	</table>

	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>

</form>
<include file="Public:footer"/>