<include file="Public:header" xmlns="http://www.w3.org/1999/html"/>
<div class="mainbox">
	<div id="nav" class="mainnav_title">
		<ul>
			<a href="{pigcms{:U('Pledge/index')}" class="on">域名质押列表</a>|

		</ul>
	</div>
	<table class="search_table" width="100%">
		<tr>
			<td>
				<form action="{pigcms{:U('Pledge/index')}" method="get">
					<input type="hidden" name="c" value="Pledge"/>
					<input type="hidden" name="a" value="index"/>

					筛选: <input type="text" name="keyword" class="input-text" value="{pigcms{$_GET['keyword']}"/>
					<select name="searchtype">
						<option value="domainName" <if condition="$_GET['searchtype'] eq 'domainName'">selected="selected"</if>>域名</option>
					</select> &nbsp;
					<input type="submit" value="查询" class="button"/>
				</form>
			</td>
		</tr>
	</table>
	<form name="myform" id="myform" action="" method="post">
		<div class="table-list">
			<table width="100%" cellspacing="0">
				<colgroup><col> <col> <col> <col><col><col><col> <col width="140" align="center"> </colgroup>
				<thead>
				<tr>
					<th>域名</th>
					<th>申请人(UID)</th>
					<th>授信状态</th>
					<th>授信额度</th>
					<th>授信时间</th>
					<th>申请时间</th>
					<th>质押状态</th>
					<th>融资状态</th>
					<th>备注</th>
					<th class="textcenter">操作</th>
				</tr>
				</thead>
				<tbody>
				<if condition="is_array($order_list)">
					<volist name="order_list" id="vo">
						<tr>
							<td>{pigcms{$vo.domain}</td>
							<td>{pigcms{$vo.uid}</td>
							<td><if condition="$vo['credit_status'] eq 0">等待审核<elseif condition="$vo['credit_status'] eq 1"/><span class="font_success">已授信</span><elseif condition="$vo['credit_status'] eq 2"/><span class="font_warning">授信失败</span></if></td>
							<td>￥{pigcms{$vo.credit_total}</td>
							<td><if condition="$vo['credit_time']">{pigcms{$vo.credit_time|date='Y-m-d',###}<else/>-</if></td>
							<td>{pigcms{$vo.apply_time|date='Y-m-d',###}</td>
							<td><if condition="$vo['pledge_status'] eq 0">-<elseif condition="$vo['pledge_status'] eq 1"/><span class="font_success">已转移</span><elseif condition="$vo['pledge_status'] eq 2"/>未转移<elseif condition="$vo['pledge_status'] eq 3"/><span class="font_warning">转移失败</span></if></td>
							<td><if condition="$vo['rz_status'] eq 0">-<elseif condition="$vo['credit_status'] eq 1"/>已融资<elseif condition="$vo['credit_status'] eq 2"/>正在融资<elseif condition="$vo['credit_status'] eq 3"/>正在还款<elseif condition="$vo['credit_status'] eq 4"/>还款完成</if></td>
							<td>{pigcms{$vo.intro}</td>
							<td class="textcenter">

<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Pledge/edit',array('pid'=>$vo['pid']))}','编辑',680,560,true,false,false,editbtn,'add',true);">编辑</a> | <a href="javascript:void(0);" class="delete_row" parameter="pid={pigcms{$vo.pid}" url="{pigcms{:U('Pledge/del')}">删除</a>

							</td>

						</tr>
					</volist>
					<tr><td class="textcenter pagebar" colspan="14">{pigcms{$pagebar}</td></tr>
					<else/>
					<tr><td class="textcenter red" colspan="12">列表为空！</td></tr>
				</if>
				</tbody>
			</table>
		</div>
	</form>
</div>
<include file="Public:footer"/>