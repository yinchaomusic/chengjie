<include file="Public:header"/>
<form   method="post" action="{pigcms{:U('Pledge/edit_save')}" frame="true"  >
	<input type="hidden" name="pid" value="{pigcms{$now_order.pid}"/>
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="15%">域名</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.domain}</div></td>
			<th width="15%">申请人</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.uid}</div></td>
		<tr/>
		<tr>
			<th width="15%">授信时间</th>
			<td width="35%"><if condition="$now_order['credit_time']">{pigcms{$now_order.credit_time|date='Y-m-d',###}<else/>-</if></td>
			<th width="15%">申请时间</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.apply_time|date='Y-m-d',###}</div></td>
		</tr>
		<tr>
			<th width="15%">授信状态</th>
			<td width="35%">
				<select name="credit_status" id="credit_status">
					<option <if condition="$now_order['credit_status'] eq 0"> selected="selected" </if> value="0">请选择</option>
					<option <if condition="$now_order['credit_status'] eq 1"> selected="selected" </if>  value="1">确认授信</option>
					<option <if condition="$now_order['credit_status'] eq 2"> selected="selected" </if> value="2">拒绝授信</option>
				</select>
				<span class="notice_tips">请确认收到该域名，包括域名的管理权和所有权</span>
			</td>
			<th width="15%">￥授信额度</th>
			<td width="35%" class="radio_box">
				<input type="text" class="input fl" name="credit_total" value="{pigcms{$now_order.credit_total}"  placeholder="请输入该域名的可授信额度" />
			</td>
		</tr>
		<tr>
			<th width="15%">质押状态</th>
			<td width="35%">
				<select name="pledge_status" id="pledge_status">
					<option <if condition="$now_order['pledge_status'] eq 0"> selected="selected" </if>  value="0">请选择</option>
					<option <if condition="$now_order['pledge_status'] eq 1"> selected="selected" </if> value="1">已转移</option>
					<option <if condition="$now_order['pledge_status'] eq 2"> selected="selected" </if> value="2">未转移</option>
					<option <if condition="$now_order['pledge_status'] eq 3"> selected="selected" </if> value="3">转移失败</option>
				</select>
			</td>
			<th width="15%">融资状态</th>
			<td width="35%">
				<if condition="$now_order['rz_status'] eq 0">-<elseif condition="$now_order['credit_status'] eq 1"/>已融资<elseif condition="$now_order['credit_status'] eq 2"/>正在融资<elseif condition="$now_order['credit_status'] eq 3"/>正在还款<elseif condition="$now_order['credit_status'] eq 4"/>还款完成</if>
			</td>
		</tr>

		<tr>
			<th width="15%"> </th>
			<td width="35%">
			</td>
			<th width="15%">备注说明</th>
			<td width="35%">
				<textarea name="intro" id="intro" cols="30"  rows="5">{pigcms{$order_info.intro} </textarea>
			</td>
		</tr>

	</table>

		<div class="btn hidden">
			<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
			<input type="reset" value="取消" class="button" />
		</div>
		 
</form>
<include file="Public:footer"/>