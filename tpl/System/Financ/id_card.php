<include file="Public:header" xmlns="http://www.w3.org/1999/html"/>
<div class="mainbox">
	<div id="nav" class="mainnav_title">
		<ul>
			<a href="{pigcms{:U('Financ/id_card')}" class="on">身份认证审核列表</a>|

		</ul>
	</div>
	<table class="search_table" width="100%">
		<tr>
			<td>
				<form action="{pigcms{:U('Financ/id_card')}" method="get">
					<input type="hidden" name="c" value="Financ"/>
					<input type="hidden" name="a" value="id_card"/>

					筛选: <input type="text" name="keyword" class="input-text" value="{pigcms{$_GET['keyword']}"/>
					<select name="searchtype">
						<option value="UID" <if condition="$_GET['searchtype'] eq 'UID'">selected="selected"</if>>UID</option>
						<option value="IDNUMBER" <if condition="$_GET['searchtype'] eq 'IDNUMBER'">selected="selected"</if>>身份证号码</option>
					</select> &nbsp;
					<input type="submit" value="查询" class="button"/>
				</form>
			</td>
		</tr>
	</table>
	<form name="myform" id="myform" action="" method="post">
		<div class="table-list">
			<table width="100%" cellspacing="0">
				<colgroup><col> <col> <col> <col><col><col><col> <col width="140" align="center"> </colgroup>
				<thead>
				<tr>
					<th>申请人(UID)</th>
					<th>姓名</th>
					<th>身份证类型</th>
					<th>申请时间</th>
					<th>认证时间</th>
					<th>状态</th>
					<th>身份证号</th>
					<th>身份证正面(强制小图，点击图片查看大图)</th>
					<th>身份证背面</th>
					<th class="textcenter">操作</th>
				</tr>
				</thead>
				<tbody>
				<if condition="is_array($order_list)">
					<volist name="order_list" id="vo">
						<tr>
							<td>{pigcms{$vo.uid}</td>
							<td>{pigcms{$vo.nickname}</td>
							<td>二代身份证</td>
							<td>{pigcms{$vo.pull_time|date='Y-m-d',###}</td>
							<td><if condition="$vo['update_time']">{pigcms{$vo.update_time|date='Y-m-d',###}<else/>-</if></td>
							<td><if condition="$vo['status'] eq 1"><span class="font_success">认证通过</span><else/><span class="font_default">未审核</span></if></td>
							<td>{pigcms{$vo.id_number}</td>
							<td><img src="{pigcms{$config.site_url}/upload/idcard/{pigcms{$vo.facadeFile}" style="width:300px;height:80px;" class="view_msg"/</td>
							<td><img src="{pigcms{$config.site_url}/upload/idcard/{pigcms{$vo.reverseSideFile}" style="width:300px;height:80px;" class="view_msg"/</td>

							<td class="textcenter">
								<if condition="$vo['status'] eq 1">
									<span class="font_success">认证通过</span>
								<else />
								<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Financ/id_card_edit',array('card_id'=>$vo['card_id']))}','编辑',680,260,true,false,false,editbtn,'id_card_edit',true);">编辑</a> | <a href="javascript:void(0);" class="delete_row" parameter="id={pigcms{$vo.card_id}&type=idcard" url="{pigcms{:U('Financ/del')}">拒绝申请</a>
								</if>
							</td>
						</tr>
					</volist>
					<tr><td class="textcenter pagebar" colspan="14">{pigcms{$pagebar}</td></tr>
					<else/>
					<tr><td class="textcenter red" colspan="12">列表为空！</td></tr>
				</if>
				</tbody>
			</table>
		</div>
	</form>
</div>
<include file="Public:footer"/>