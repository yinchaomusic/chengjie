<include file="Public:header"/>
<form   method="post" action="{pigcms{:U('Financ/id_card_edit_save')}" frame="true"  >
	<input type="hidden" name="card_id" value="{pigcms{$now_order.card_id}"/>
	<input type="hidden" name="uid" value="{pigcms{$now_order.uid}"/>
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="15%">姓名</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.nickname}</div></td>
			<th width="15%">申请人UID</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.uid}</div></td>
		<tr/>
		<tr>
			<th width="15%">认证时间</th>
			<td width="35%"><if condition="$now_order['update_time']">{pigcms{$now_order.update_time|date='Y-m-d',###}<else/>-</if></td>
			<th width="15%">申请时间</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.pull_time|date='Y-m-d',###}</div></td>
		</tr>
		<tr>
		 <th width="15%">是否通过</th>
		<td  width="35%">
			<span class="cb-enable"><label class="cb-enable <if condition="$now_order['status'] eq 1">selected</if>"><span>通过</span><input type="radio" name="status" value="1" <if condition="$now_order['status'] eq 1">checked="checked"</if>/></label></span>
			<span class="cb-disable"><label class="cb-disable <if condition="$now_order['status'] eq 0">selected</if>"><span>未审核</span><input type="radio" name="status" value="0" <if condition="$now_order['status'] eq 0">checked="checked"</if>/></label></span>
		</td>
		 <th width="15%"> </th>
		<td width="35%"></td>
		</tr>
	</table>

	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>

</form>
<include file="Public:footer"/>