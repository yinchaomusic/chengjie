<include file="Public:header"/>
<form id="myform" method="post" action="{pigcms{:U('Trade_mode/edit_save')}" >
	<input type="hidden" name="id" value="{pigcms{$now_Domain_trademodel.id}">
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="80">交易类型</th>
			<td><input type="text" class="input fl" name="name" size="20" value="{pigcms{$now_Domain_trademodel.name}" placeholder="请输入交易类型，如 一元竞拍" validate="maxlength:50,required:true"
			           tips="交易类型，例如：一元竞拍"/></td>
		</tr>

		<tr>
			<th width="80">排序</th>
			<td><input type="text" class="input fl" name="sorts" size="10" value="{pigcms{$now_Domain_trademodel.value}" validate="number:true,maxlength:6" tips="数值越大，排序越前"/></td>
		</tr>
		<tr>
			<th width="80">状态</th>
			<td class="radio_box">
				<span class="cb-enable"><label class="cb-enable <if condition="$now_Domain_trademodel['status'] eq 1">selected</if>"><span>显示</span><input type="radio" name="status" value="1" <if condition="$now_Domain_trademodel['status'] eq 1">checked="checked"</if>/></label></span>
				<span class="cb-disable"><label class="cb-disable <if condition="$now_Domain_trademodel['status'] eq 0">selected</if>"><span>隐藏</span><input type="radio" name="status" value="0" <if condition="$now_Domain_trademodel['status'] eq 0">checked="checked"</if>/></label></span>
			</td>
		</tr>
	</table>
	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>
</form>
<include file="Public:footer"/>