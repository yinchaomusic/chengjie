<include file="Public:header"/>
<form id="myform" method="post" action="{pigcms{:U('Trade_mode/add_save')}" >
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="80">交易类型</th>
			<td><input type="text" class="input fl" name="name" size="20" placeholder="请输入交易类型，如 一元竞拍" validate="maxlength:50,required:true"
			           tips="交易类型，例如：一元竞拍"/></td>
		</tr>

		<tr>
			<th width="80">排序</th>
			<td><input type="text" class="input fl" name="sorts" size="10" value="0" validate="number:true,maxlength:6" tips="数值越大，排序越前"/></td>
		</tr>
		<tr>
			<th width="80">状态</th>
			<td class="radio_box">
				<label style="float:left;width:60px" class="checkbox_status"><input type="radio" class="input_radio" name="status" checked="checked" value="1" validate=" maxlength:1" /> 显示</label>
				<label style="float:left;width:60px" class="checkbox_status"><input type="radio" class="input_radio" name="status" value="0" /> 隐藏</label>
			</td>
		</tr>
	</table>
	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>
</form>
<include file="Public:footer"/>