<include file="Public:header"/>
<form id="myform" method="post" action="{pigcms{:U('Video_auction/edit_number_save')}" >
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<input type="hidden" name="term_id" value="{pigcms{$term_info.term_id}">
		<tr>
			<th width="80">本期名称</th>
			<td><input type="text" class="input fl" value="{pigcms{$term_info.term_name}" name="term_name" id="term_name" size="25" placeholder="请填写本期拍卖名称"
			           validate="maxlength:50,required:true"
			           tips=""/></td>
		</tr>

		<tr>
			<th width="80">状态</th>
			<td>
				<span class="cb-enable"><label class="cb-enable <if condition="$term_info['is_open'] eq 1">selected</if>"><span>开启</span><input type="radio" name="is_open" value="1" <if condition="$term_info['status'] eq 1">checked="checked"</if>/></label></span>
				<span class="cb-disable"><label class="cb-disable <if condition="$term_info['is_open'] eq 0">selected</if>"><span>关闭</span><input type="radio" name="is_open" value="0" <if condition="$term_info['status'] eq 0">checked="checked"</if>/></label></span>
			</td>
		</tr>
		<tr>
			<th width="80">本期公告</th>
			<td>
				<textarea name="term_notice" id="term_notice"  style="margin: 0px; width: 500px; height: 178px;"  validate="required:true">{pigcms{$term_info.term_notice}</textarea>
			</td>
		</tr>
	</table>
	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>
</form>
<include file="Public:footer"/>