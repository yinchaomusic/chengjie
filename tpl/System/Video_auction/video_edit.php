<include file="Public:header"/>
<form id="myform" method="post" action="{pigcms{:U('Video_auction/video_edit_save')}" >
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<input type="hidden" name="domain_id" value="{pigcms{$video_info.domain_id}">
		<tr>
			<th width="80">域名</th>
			<td>{pigcms{$video_info.term_name}</td>
		</tr>
		<tr>
			<th width="80">域名后缀</th>
			<td> <input type="text" class="input fl" value="{pigcms{$video_info.domains_suffix}" name="domains_suffix" id="domains_suffix"  /></td>
		</tr>
		<tr>
			<th width="80">域名备注</th>
			<td>{pigcms{$video_info.domains_desc}</td>
		</tr>
		<tr>
			<th width="80">起拍价</th>
			<td><input type="text" class="input fl" value="{pigcms{$video_info.mark_money}" name="mark_money" id="mark_money"  /></td>
		</tr>
		<tr>
			<th width="80">最终价</th>
			<td>￥{pigcms{$video_info.now_money}</td>
		</tr>
		<tr>
			<th width="80">保留价</th>
			<td><input type="text" class="input fl" value="{pigcms{$video_info.retain_money}" name="retain_money" id="retain_money"  /></td>
		</tr>
		<tr>
			<th width="80">期数</th>
			<td>{pigcms{$video_info.term_name}</td>
		</tr>
		<tr>
			<th width="80">排序</th>
			<td><input type="text" class="input fl" value="{pigcms{$video_info.sort}" name="sort" id="sort"  /></td>
		</tr>

		<tr>
			<th width="80">状态</th>
			<td>
				<span class="cb-enable"><label class="cb-enable <if condition="$video_info['domain_status'] eq 1">selected</if>"><span>开启</span><input type="radio" name="domain_status" value="1" <if condition="$video_info['domain_status'] eq 1">checked="checked"</if>/></label></span>
				<span class="cb-disable"><label class="cb-disable <if condition="$video_info['domain_status'] eq 0">selected</if>"><span>关闭</span><input type="radio" name="domain_status" value="0" <if condition="$video_info['domain_status'] eq 0">checked="checked"</if>/></label></span>
			</td>
		</tr>

	</table>
	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>
</form>
<include file="Public:footer"/>