<include file="Public:header"/>
<div class="mainbox">
	<div id="nav" class="mainnav_title">
		<ul>
			<a href="{pigcms{:U('Video_auction/index')}" class="on">视频拍卖期数</a>|
			<font color="blue">当前操作的期数：</font><span style="font-size: larger;background-color:red">
				{pigcms{$count_videos_list[0]['term']['term_name']} </span>
			<if condition="$term_id eq ''">
				<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Video_auction/video_domains',array('term_id'=>$count_videos_list[0]['term_id']))}','添加拍卖域名',520,350,true,false,false,editbtn,'add',true);">添加拍卖域名</a>
				<else />
				<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Video_auction/video_domains',array('term_id'=>$term_id))}','添加拍卖域名',520,350,true,false,false,editbtn,'add',true);">添加拍卖域名</a>
			</if>

		</ul>
	</div>
	<table class="search_table" width="100%">
		<tr>
			<td>
				<form action="{pigcms{:U('Video_auction/video_list')}" method="get">
					<input type="hidden" name="c" value="Video_auction"/>
					<input type="hidden" name="a" value="video_list"/>
					筛选: <input type="text" name="keyword" class="input-text" value="{pigcms{$_GET['keyword']}"/>
					<select name="searchtype">
						<option value="domains" <if condition="$_GET['searchtype'] eq 'domains'">selected="selected"</if>>域名</option>

					</select>
					<input type="submit" value="查询" class="button"/>
				</form>
			</td>
		</tr>
	</table>
	<form name="myform" id="myform" action="" method="post">
		<div class="table-list">
			<table width="100%" cellspacing="0">
				<colgroup><col> <col> <col> <col><col><col><col> <col width="140" align="center"> </colgroup>
				<thead>
				<tr>
					<th>编号ID</th>
					<th>域名</th>
					<th>域名后缀</th>
					<th>域名备注</th>
					<th>起拍价</th>
					<th>最终拍价</th>
					<th>保留价</th>
					<th>得标人</th>
					<th>出价记录</th>
					<th>排序</th>
					<th>状态</th>
					<th class="textcenter">操作</th>
				</tr>
				</thead>
				<tbody>
				<if condition="is_array($count_videos_list)">
					<volist name="count_videos_list" id="vo">
						<tr>
							<td>{pigcms{$vo.domain_id}</td>
							<td>{pigcms{$vo.domains}</td>
							<td>{pigcms{$vo.domains_suffix}</td>
							<td>{pigcms{$vo.domains_desc}</td>
							<td>￥{pigcms{$vo.mark_money|number_format}</td>
							<td>￥{pigcms{$vo.now_money|number_format}</td>
							<td>￥{pigcms{$vo.retain_money|number_format}</td>
							<td>{pigcms{$vo.Uname}</td>
							<td> <a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Video_auction/bid_record',array('domain_id'=>$vo['domain_id']))}','出价记录',680,560,true,false,false,null,'bid_record',true);">出价记录</a> </td>
							<td>{pigcms{$vo.sort}</td>
							<td><if condition="$vo['domain_status']"><font color="green">开启</font><else/><font color="red">关闭</font></if></td>
							<td class="textcenter"> <a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Video_auction/video_edit',array('domain_id'=>$vo['domain_id']))}','编辑后缀',520,350,true,false,false,editbtn,'add',true);">编辑</a>
								| <a href="javascript:void(0);" class="delete_row" parameter="domain_id={pigcms{$vo.domain_id}" url="{pigcms{:U('Video_auction/del_video')}">删除</a>
							</td>
						</tr>
					</volist>
					<tr><td class="textcenter pagebar" colspan="16">{pigcms{$pagebar}</td></tr>
					<else/>
					<tr><td class="textcenter red" colspan="16">列表为空！</td></tr>
				</if>
				</tbody>
			</table>
		</div>
	</form>
</div>
<include file="Public:footer"/>