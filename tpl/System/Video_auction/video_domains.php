<include file="Public:header"/>
<form   method="post" action="{pigcms{:U('Video_auction/video_domains')}" >
	<input type="hidden" name="term_id" value="{pigcms{$term_id}">

	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="80">拍卖域名</th>
			<td><input type="text" class="input fl" name="domains" id="domains" size="25" placeholder="请填写拍卖域名" validate="maxlength:50,required:true"
			           tips="您也可以从右侧菜单选择"  autocomplete="off"/>


			</td>
		</tr>

		<tr>
			<th width="80">域名描述</th>
			<td><input type="text" class="input fl" name="domains_desc" id="domains_desc" validate="maxlength:50,required:true"  placeholder="请填写本期域名描述"  /></td>
		</tr>
		<tr>
			<th width="80">域名后缀</th>
			<td><input type="text" class="input fl" name="domains_suffix" id="domains_suffix" validate="maxlength:50,required:true" size="25" placeholder="请填写拍卖域名后缀" /></td>
		</tr>

		<tr>
			<th width="80">状态</th>
			<td>
				<span class="cb-enable"><label class="cb-enable selected"><span>开启</span><input type="radio" name="domain_status" value="1" checked="checked" /></label></span>
				<span class="cb-disable"><label class="cb-disable"><span>关闭</span><input type="radio" name="domain_status" value="0" /></label></span>
			</td>
		</tr>

		<tr>
			<th width="80">起拍价</th>
			<td><input type="text" class="input fl" name="mark_money" validate="maxlength:50,required:true" id="mark_money" size="25" /></td>
		</tr>
		<tr>
			<th width="80">保留价</th>
			<td><input type="text" class="input fl" name="retain_money" validate="maxlength:50,required:true" id="retain_money" size="25" />
				如果竞拍价格未过保留价，则流拍。</td>
		</tr>
		<tr>
			<th width="80">排序</th>
			<td>
				<input type="text" class="input fl" name="sort" id="sort" size="25" />

			</td>
		</tr>
	</table>
	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>
</form>
<!--<br>-->
<!--<h2>已经添加拍卖的域名</h2>-->
<!--<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">-->
<!--	<tr>-->
<!--		<th>编号ID</th>-->
<!--		<th>域名</th>-->
<!--		<th>域名描述</th>-->
<!--		<th>域名后缀</th>-->
<!--		<th>起拍价</th>-->
<!--		<th>排序</th>-->
<!---->
<!---->
<!--	</tr>-->
<!---->
<!--	<volist name="videosale_domains_list" id="vo">-->
<!--		<tr>-->
<!--			<td>{pigcms{$vo.domain_id}</td>-->
<!--			<td>{pigcms{$vo.domains}</td>-->
<!--			<td>{pigcms{$vo.domains_desc}</td>-->
<!--			<td>{pigcms{$vo.domains_suffix}</td>-->
<!--			<td>￥{pigcms{$vo.mark_money|number_format}</td>-->
<!--			<td>{pigcms{$vo.sort}</td>-->
<!--		</tr>-->
<!--	</volist>-->
<!--	<tr><td class="textcenter pagebar" colspan="9" style="border-bottom:1px solid #ccc;">{pigcms{$pagebar}</td></tr>-->
<!--</table>-->
<script type="text/javascript" src="./static/js/artdialog/jquery.artDialog.js"></script>
<script type="text/javascript" src="./static/js/artdialog/iframeTools.js"></script>
<script>
	function checkDomains(domid, iskeyword, type){
		art.dialog.data('domid', domid);
		if (type == 1) {
			art.dialog.open('?g=System&c=Video_auction&a=getCheckDomains&iskeyword='+iskeyword,{lock:true,title:'插入域名',width:600,height:400,yesText:'关闭',background: '#000',opacity: 0.45});
		} else {
			art.dialog.open('?g=System&c=Video_auction&a=getCheckDomains&iskeyword='+iskeyword,{lock:true,title:'插入域名',width:600,height:400,yesText:'关闭',background: '#000',opacity: 0.45});
		}
	}
</script>
<include file="Public:footer"/>