<include file="Public:header"/>
<div class="mainbox">
	<div id="nav" class="mainnav_title">
		<ul>
			<a href="{pigcms{:U('Video_auction/index')}" class="on">视频拍卖期数</a>|
			<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Video_auction/add_number')}','视频拍卖期数添加',860,350,true,false,false,addbtn,'add',true);">视频拍卖期数添加</a>|
<!--			<a href="{pigcms{:U('Video_auction/video_list')}" class="on">视频拍卖域名列表</a>|-->

		</ul>
	</div>
	<form name="myform" id="myform" action="" method="post">
		<div class="table-list">
			<table width="100%" cellspacing="0">
				<colgroup><col> <col> <col> <col><col><col><col> <col width="140" align="center"> </colgroup>
				<thead>
				<tr>
					<th>编号ID</th>
					<th>本期名称</th>
					<th>本期公告</th>
<!--					<th>添加本期拍卖域名</th>-->
					<th>状态</th>
					<th>视频拍卖域名列表</th>
					<th class="textcenter">操作</th>
				</tr>
				</thead>
				<tbody>
				<if condition="is_array($term_term)">
					<volist name="term_term" id="vo">
						<tr>
							<td>{pigcms{$vo.term_id}</td>
							<td>{pigcms{$vo.term_name}</td>
							<td>{pigcms{$vo.term_notice|msubstr=###,0,50}</td>
<!--							<td> <a href="{pigcms{:U('Video_auction/video_domains',array('term_id'=>$vo['term_id']))}">添加本期拍卖域名</a> </td>-->
							<td><if condition="$vo['is_open']"><font color="green">开启</font><else/><font color="red">关闭</font></if></td>
							<td> <a href="{pigcms{:U('Video_auction/video_list',array('term_id'=>$vo['term_id']))}">视频拍卖域名列表</a> </td>

							<td class="textcenter"> <a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Video_auction/edit_number',array('term_id'=>$vo['term_id']))}','编辑后缀',860,350,true,false,false,editbtn,'add',true);">编辑</a> |

								<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Video_auction/showinfo',array('term_id'=>$vo['term_id'],'frame_show'=>true))}','查看',860,380,true,false,false,false,'add',true);">查看</a> </td>
						</tr>
					</volist>
					<tr><td class="textcenter pagebar" colspan="16">{pigcms{$pagebar}</td></tr>
					<else/>
					<tr><td class="textcenter red" colspan="16">列表为空！</td></tr>
				</if>
				</tbody>
			</table>
		</div>
	</form>
</div>
<include file="Public:footer"/>