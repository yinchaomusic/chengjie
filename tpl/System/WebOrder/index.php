<include file="Public:header"/>
<div class="mainbox">
	<div id="nav" class="mainnav_title">
		<ul>
		</ul>
	</div>
	<table class="search_table" width="100%">
		<tr>
			<td>
				<form action="{pigcms{:U('WebOrder/index')}" method="post">
					筛选:
					<select name="status">
						<option value="" <if condition="$_POST['status'] eq ''">selected="selected"</if>>选择状态</option>
						<option value="1" <if condition="$_POST['status'] eq '1'">selected="selected"</if>>初始状态</option>
						<option value="2" <if condition="$_POST['status'] eq '2'">selected="selected"</if>>付款状态</option>
						<option value="3" <if condition="$_POST['status'] eq '3'">selected="selected"</if>>收发货</option>
						<option value="4" <if condition="$_POST['status'] eq '4'">selected="selected"</if>>中介确认</option>
						<option value="5" <if condition="$_POST['status'] eq '5'">selected="selected"</if>>交易结束</option>
					</select> &nbsp;
					<select name="inter_obj">
						<option value="" <if condition="$_POST['inter_obj'] eq ''">selected="selected"</if>>选择发起人</option>
						<option value="1" <if condition="$_POST['inter_obj'] eq '1'">selected="selected"</if>>买家</option>
						<option value="2" <if condition="$_POST['inter_obj'] eq '2'">selected="selected"</if>>卖家</option>
					</select> &nbsp;
					<select name="web_type">
						<option value="" <if condition="$_POST['web_type'] eq ''">selected="selected"</if>>选择网站类型</option>
						<option value="1" <if condition="$_POST['web_type'] eq '1'">selected="selected"</if>>行业门户</option>
						<option value="2" <if condition="$_POST['web_type'] eq '2'">selected="selected"</if>>音乐影视</option>
						<option value="3" <if condition="$_POST['web_type'] eq '3'">selected="selected"</if>>游戏小说</option>
						<option value="4" <if condition="$_POST['web_type'] eq '4'">selected="selected"</if>>女性时尚</option>
						<option value="5" <if condition="$_POST['web_type'] eq '5'">selected="selected"</if>>QQ/娱乐</option>
						<option value="6" <if condition="$_POST['web_type'] eq '6'">selected="selected"</if>>商城购物</option>
						<option value="7" <if condition="$_POST['web_type'] eq '7'">selected="selected"</if>>地方网站</option>
						<option value="8" <if condition="$_POST['web_type'] eq '8'">selected="selected"</if>>其他网站</option>
					</select> &nbsp;
					<input type="submit" value="查询" class="button"/>
				</form>
			</td>
		</tr>
	</table>
	<form name="myform" id="myform" action="" method="post">
		<div class="table-list">
			<table width="100%" cellspacing="0">
				<colgroup><col> <col> <col> <col><col><col><col><col><col><col><col><col><col><col width="140" align="center"> </colgroup>
				<thead>
				<tr>
					<th>编号</th>
					<th>状态</th>
					<th>发起人</th>
					<th>买家ID</th>
					<th>卖家ID</th>
					<th>网站域名</th>
					<th>价格</th>
					<th>平台手续费</th>
					<th>总额</th>
					<th>经纪人ID</th>
					<th>是否为案例</th>
					<th>网站类型</th>
					<th>订单号</th>
					<th>创建日期</th>
					<th class="textcenter">操作</th>
				</tr>
				</thead>
				<tbody>
				<if condition="is_array($category_list)">
					<volist name="category_list" id="vo">
						<tr>
							<td>{pigcms{$vo.id}</td>
							<td>
								<if condition="$vo['status'] eq 0">初始状态 
    								<elseif  condition="$vo['status'] eq 1"/><font color="red">待付款</font>
    								<elseif  condition="$vo['status'] eq 2"/><font color="red">收发货</font>
    								<elseif  condition="$vo['status'] eq 3"/><font color="red">中介确认</font>
    								<elseif  condition="$vo['status'] eq 4"/><font color="red">交易结束</font>
								</if>
							</td>
							<td>
								<if condition="$vo['inter_obj'] eq 1">买家
									<elseif  condition="$vo['inter_obj'] eq 2"/>卖家
								</if>
							</td>
							<td>
    							<if condition="$vo['inter_obj'] eq 1">{pigcms{$vo.user_id}
    								<elseif  condition="$vo['inter_obj'] eq 2"/>{pigcms{$vo.other_user_id}
    							</if>
							</td>
							<td>
								<if condition="$vo['inter_obj'] eq 1">{pigcms{$vo.other_user_id}
    								<elseif  condition="$vo['inter_obj'] eq 2"/>{pigcms{$vo.user_id}
    							</if>
							</td>
							<td>{pigcms{$vo.domain}</td>
							<td>
    							<if condition="$vo['price'] eq 0">
                					<font color="red">议价</font>
                					<else /><font color="green" size="5">￥{pigcms{$vo.price|number_format}</font> 
                				</if>
							</td>
							<td>
    							<font color="green" size="5">￥{pigcms{$vo.platform_fee|number_format}</font> 
							</td>
							<td>
    							<font color="green" size="5">￥{pigcms{$vo.all_price|number_format}</font> 
							</td>
							<td>
								<if condition="$vo['check_user_id']">
									<font color="green">{pigcms{$vo.check_user_id}</font><else/><font color="red">无</font>
								</if>
							</td>
							<td>
								<if condition="$vo['is_case'] eq 0">否 
								<elseif  condition="$vo['is_case'] eq 1"/><font color="red">是</font>
								</if>
							</td>
							<td>
								<if condition="$vo['web_type'] eq 1"><font color="red">行业门户 </font>
    								<elseif  condition="$vo['web_type'] eq 2"/>音乐影视
    								<elseif  condition="$vo['web_type'] eq 3"/>游戏小说
    								<elseif  condition="$vo['web_type'] eq 4"/>女性时尚
    								<elseif  condition="$vo['web_type'] eq 5"/>QQ/娱乐
    								<elseif  condition="$vo['web_type'] eq 6"/>商城购物
    								<elseif  condition="$vo['web_type'] eq 7"/>地方网站
    								<elseif  condition="$vo['web_type'] eq 8"/>其他网站
								</if>
							</td>
							<td>{pigcms{$vo.order_flow}</td>
							<td><div style="height:24px;line-height:24px;">{pigcms{$vo.createdate|date='Y-m-d H:i:s',###}</div></td>
							<td class="textcenter">  
								<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('WebOrder/edit',array('id'=>$vo['id']))}','编辑交易',800,460,true,false,false,editbtn,'add',true);">编辑</a> 
						  		| 
						  		<if condition="$vo['status'] eq 0">
  								  	<a href="javascript:void(0);" class="delete_row" parameter="id={pigcms{$vo['id']}" url="{pigcms{:U('WebOrder/del')}">删除</a></td>
								<elseif  condition="$vo['status'] neq 0"/>
									<a href="javascript:void(0);" class="not_del">删除</a>  								  	
								</if>
							</td>
						</tr>
					</volist>
					<tr><td class="textcenter pagebar" colspan="15">{pigcms{$pagebar}</td></tr>
					<else/>
					<tr><td class="textcenter red" colspan="15">列表为空！</td></tr>
				</if>
				</tbody>
			</table>
		</div>
	</form>
</div>
<include file="Public:footer"/>