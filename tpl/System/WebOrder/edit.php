<include file="Public:header"/>
<form id="myform" method="post" action="{pigcms{:U('WebOrder/edit_web_order')}" >
	<input type="hidden" name="id" value="{pigcms{$now_Web_inter_order.id}">
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%" style="text-align:center">
		<tr>
			<th width="15%">编号</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_inter_order.id}</div></td>
			<th width="15%">创建时间</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_inter_order.createdate|date='Y-m-d H:i:s',###}</div></td>
		<tr/>
		<tr>
			<th width="15%">状态</th>
			<td width="15%">
				<if condition="$now_Web_inter_order['status'] eq 0">初始状态</if>
				<if condition="$now_Web_inter_order['status'] eq 1">中介已介</if>
			</td>
			<th width="15%">发起人</th>
			<td width="35%">
    			<if condition="$now_Web_inter_order['inter_obj'] eq 1">买家</if>
				<if condition="$now_Web_inter_order['inter_obj'] eq 2">卖家</if>
			</td>
		<tr/>
		<tr>
			<th width="15%">买家ID</th>
			<td width="15%">
				<div style="height:24px;line-height:24px;">
    				<if condition="$now_Web_inter_order['inter_obj'] eq 1">{pigcms{$now_Web_inter_order.user_id}
    					<elseif  condition="$vo['inter_obj'] eq 2"/>{pigcms{$now_Web_inter_order.other_user_id}
    				</if>
				</div>
			</td>
			<th width="15%">卖家ID</th>
			<td width="35%">
				<div style="height:24px;line-height:24px;">
					<if condition="$now_Web_inter_order['inter_obj'] eq 1">{pigcms{$now_Web_inter_order.other_user_id}
    					<elseif  condition="$vo['inter_obj'] eq 2"/>{pigcms{$now_Web_inter_order.user_id}
    				</if>
				</div>
			</td>
		<tr/>
		<tr>
			<th width="15%">域名</th>
			<td width="35%">
				<div style="height:24px;line-height:24px;">{pigcms{$now_Web_inter_order.domain}</div>
			</td>
			<th width="15%">pr值</th>
			<td width="15%">
				<div style="height:24px;line-height:24px;">{pigcms{$now_Web_inter_order.pr}</div>
			</td>
		<tr/>
		<tr>
			<th width="15%">报价</th>
			<td width="35%">
				<if condition="$now_Web_inter_order['price'] eq 0">
					<font color="red">议价</font>
					<else /><font color="green" size="5">￥{pigcms{$now_Web_inter_order.price|number_format}</font> 
				</if>
			</td>
			<th width="15%">平台手续费</th>
			<td width="35%">
				<if condition="$now_Web_inter_order['platform_fee'] eq 0">
					<font color="red">待定</font>
					<else /><font color="green" size="5">￥{pigcms{$now_Web_inter_order.platform_fee|number_format}</font> 
				</if>
			</td>
		</tr>
		
		<tr>
			<th width="15%">总额</th>
			<td width="35%">
				<if condition="$now_Web_inter_order['all_price'] eq 0">
					<font color="red">议价</font>
					<else /><font color="green" size="5">￥{pigcms{$now_Web_inter_order.all_price|number_format}</font> 
				</if>
			</td>
			<th width="15%">手续费分摊方式</th>
			<td width="35%">
				<if condition="$now_Web_inter_order['plat_fee_obj'] eq 1">买家付
					<elseif  condition="$now_Web_inter_order['web_type'] eq 2"/>卖家付
					<elseif  condition="$now_Web_inter_order['web_type'] eq 3"/>各一半
				</if>
			</td>
		</tr>
		<tr>
			<th width="15%">网站类型</th>
			<td width="35%">
				<if condition="$now_Web_inter_order['web_type'] eq 1"><font color="red">行业门户 </font>
    				<elseif  condition="$now_Web_inter_order['web_type'] eq 2"/>音乐影视
    				<elseif  condition="$now_Web_inter_order['web_type'] eq 3"/>游戏小说
    				<elseif  condition="$now_Web_inter_order['web_type'] eq 4"/>女性时尚
    				<elseif  condition="$now_Web_inter_order['web_type'] eq 5"/>QQ/娱乐
    				<elseif  condition="$now_Web_inter_order['web_type'] eq 5"/>商城购物
    				<elseif  condition="$now_Web_inter_order['web_type'] eq 5"/>地方网站
    				<elseif  condition="$now_Web_inter_order['web_type'] eq 5"/>其他网站
    			</if>
			</td>
			<th width="15%">月收入元</th>
			<td width="35%">
				<if condition="$now_Web_inter_order['month_income'] eq 0">
					<font color="red">未公开</font>
					<else /><font color="green" size="5">￥{pigcms{$now_Web_inter_order.month_income|number_format}</font> 
				</if>
			</td>
		</tr>
		<tr>
			<th width="15%">日ip(pc)pc</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_inter_order.ip_pc}</div></td>
			<th width="15%">日ip(app)</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_inter_order.ip_app}</div></td>
		<tr/>
		<tr>
			<th width="15%">源码</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_inter_order.souce_code}</div></td>
			<th width="15%">服务器信息</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_inter_order.server_info}</div></td>
		<tr/>
		
		<tr>
			<th width="15%">广告联盟</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_inter_order.advert_aliance}</div></td>
			<th width="15%">联盟帐号处理</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_inter_order.aliance_acc_handle}</div></td>
		<tr/>
		
		
		<tr>
			<th width="15%">第三方平台名称</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_inter_order.third_party}</div></td>
			<th width="15%">更新方式</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_inter_order.update_type}</div></td>
		<tr/>
		<tr>
			<th width="15%">是否需要搬迁</th>
			<td width="35%">
				<if condition="$now_Web_inter_order['is_help_move'] eq 1"><font color="red">不需要 </font>
    				<elseif  condition="$now_Web_inter_order['is_help_move'] eq 2"/>需要
    			</if>
			</td>
			<th width="15%">联系方式</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_inter_order.phone}</div></td>
		<tr/>
		<tr>
			<th width="15%">简介</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_inter_order.description}</div></td>
		<tr/>
		<tr>
			<th width="15%">用户ID</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_inter_order.user_id}</div></td>
			<th width="15%">中介处理人ID</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_Web_inter_order.check_user_id}</div></td>
		<tr/>
		
		<tr>
			<th width="15%">是否案例</th>
			<td width="35%">
				<span class="cb-enable"><label class="cb-enable <if condition="$now_Web_inter_order['is_case'] eq 1">selected</if>"><span>是</span><input type="radio" name="is_case" value="1" <if condition="$now_Web_inter_order['is_case'] eq 1">checked="checked"</if>/></label></span>
				<span class="cb-disable"><label class="cb-disable <if condition="$now_Web_inter_order['is_case'] eq 0">selected</if>"><span>否</span><input type="radio" name="is_case" value="0" <if condition="$now_Web_inter_order['is_case'] eq 0">checked="checked"</if>/></label></span>
			</td>
			<th width="15%">是否同意平台协议</th>
			<td width="35%">
				<if condition="$now_Web_inter_order['is_agent'] eq 1">是</if>
				<if condition="$now_Web_inter_order['is_agent'] eq 0">否</if>
			</td>
		</tr>
	</table>
	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>
</form>

<include file="Public:footer"/>