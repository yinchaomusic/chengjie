<include file="Public:header"/>
<div id="nav" class="mainnav_title">
	<ul>
		<a href="{pigcms{:U('Trade/procurement')}" class="on">域名代购列表</a>|
		<a href="{pigcms{:U('Trade/procure_site')}" class="on">域名代购设置</a>|

	</ul>
</div>
<form method="post" action="{pigcms{:U('Trade/procure_site')}">
	<input type="hidden" name="name" value="{pigcms{$config_data['name']}">
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="80">是否开启审核</th>
			<td class="radio_box">
				<span class="cb-enable"><label class="cb-enable <if condition="$config_data['type'] eq 1">selected</if>"><span>开启</span><input type="radio" name="type" value="1" <if condition="$config_data['type'] eq 1">checked="checked"</if>/></label></span>
				<span class="cb-disable"><label class="cb-disable <if condition="$config_data['type'] eq 0">selected</if>"><span>关闭</span><input type="radio" name="type" value="0" <if condition="$config_data['type'] eq 0">checked="checked"</if>/></label></span>
				<em class="notice_tips" tips="默认开启"></em>
			</td>
		</tr>

		<tr>
			<th width="80">代购有效期</th>
			<td class="radio_box">

				<textarea name="value" id="value" style="margin: 0px; width: 415px; height: 51px;">{pigcms{$config_data.value|default='永久&#124;三天&#124;五天&#124;七天&#124;一个月&#124;三个月&#124;半年&#124;一年'}</textarea>
				<em class="notice_tips" tips="代购有效期，过期自动失效，默认永久，请用英文 | 竖线来分割，比如 永久|三天|五天"></em>
			</td>
		</tr>
	</table>
	<div class="btn">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>
</form>
<include file="Public:footer"/>