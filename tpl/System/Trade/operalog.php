<include file="Public:header"/>
<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
	<tr>
		<th width="180">操作时间</th>
		<th>操作详情</th>
		<th>操作人</th>
	</tr>
	<volist name="order_info_list" id="vo">
		<tr>
			<th width="180">{pigcms{$vo.time|date='Y-m-d H:i:s',###}</th>
			<th>{pigcms{$vo['info']}</th>
			<th><if condition="$vo['operator'] neq ''">{pigcms{$vo['operator']}<else /> - </if></th>
		</tr>
	</volist>
	<tr><td class="textcenter pagebar" colspan="3" style="border-bottom:1px solid #ccc;">{pigcms{$pagebar}</td></tr>
</table>
<include file="Public:footer"/>