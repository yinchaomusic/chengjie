<include file="Public:header" xmlns="http://www.w3.org/1999/html"/>
<div class="mainbox">
	<div id="nav" class="mainnav_title">
		<ul>
			<a href="{pigcms{:U('Trade/index')}" class="on">域名交易列表</a>|

		</ul>
	</div>
	<table class="search_table" width="100%">
		<tr>
			<td>
				<form action="{pigcms{:U('Trade/index')}" method="get">
					<input type="hidden" name="c" value="Trade"/>
					<input type="hidden" name="a" value="index"/>

					筛选: <input type="text" name="keyword" class="input-text" value="{pigcms{$_GET['keyword']}"/>
					<select name="searchtype">
						<option value="order_id" <if condition="$_GET['searchtype'] eq 'order_id'">selected="selected"</if>>订单ID</option>
						<option value="domainName" <if condition="$_GET['searchtype'] eq 'domainName'">selected="selected"</if>>域名</option>
						<option value="staff_id" <if condition="$_GET['searchtype'] eq 'staff_id'">selected="selected"</if>>经纪人ID</option>
					</select> &nbsp;
					<input type="submit" value="查询" class="button"/>
				</form>
			</td>
		</tr>
	</table>
	<form name="myform" id="myform" action="" method="post">
		<div class="table-list">
			<table width="100%" cellspacing="0">
				<colgroup><col> <col> <col> <col><col><col><col> <col width="140" align="center"> </colgroup>
				<thead>
				<tr>
					<th>订单ID</th>
					<th>订单创建时间</th>
					<th>订单关闭时间</th>
					<th>订单总价(￥)</th>
					<th>域名</th>
					<th>交易类型</th>
					<th><font color="fuchsia"> 买家 </font>ID</th>
					<th><font color="#ff7f50">卖家</font>ID</th>
					<th>经纪人ID</th>
					<th>转移类型</th>
					<th>中介费支付方式</th>
					<th>订单进程</th>
					<th>交易状态</th>
					<th class="textcenter">操作</th>
				</tr>
				</thead>
				<tbody>
				<if condition="is_array($order_list)">
					<volist name="order_list" id="vo">
						<tr>
							<td>{pigcms{$vo.order_id}</td>
							<td>{pigcms{$vo.addTime|date='Y-m-d H:i:s',###}</td>
							<td><if condition="$vo['up_time'] eq '' ">-<else />{pigcms{$vo.up_time|date='Y-m-d H:i:s',###}</if></td>
							<td>￥{pigcms{$vo.total_price|number_format}</td>
							<td>{pigcms{$vo.domainName}</td>
							<td>
								<if condition="$vo['trade_type'] eq 1">中介交易<elseif condition="$vo['trade_type'] eq 0"/> 会员域名交易<elseif condition="$vo['trade_type'] eq 2"/>  <a href="javascript:void(0);" title="查看域名" onclick="window.top.artiframe('{pigcms{:U('Trade/lottrading',array('domain_id'=>$vo['domain_id']))}','批量交易列表',680,560,true,false,false,editbtn,'add',true);"> <font color="blue"> 批量交易</font> </a></if>
							</td>
							<td>{pigcms{$vo.buy_id}</td>
							<td>{pigcms{$vo.sell_id}</td>
							<td><if condition="$vo['staff_id'] eq '' ">-<else /> {pigcms{$vo.staff_id}</if></td>
							<td><if condition="$vo['transfer_type'] eq 1 ">卖家提供域名转移密码<elseif condition="$vo['transfer_type'] eq 2" /> {pigcms{$vo.staff_id}</if></td>
							<td>
								<if condition="$vo['payTye'] eq 1"><font color="fuchsia"> 买家 </font><elseif condition="$vo['payTye'] eq 2"/><font color="#ff7f50">卖家</font> <elseif condition="$vo['payTye'] eq 3"/> 双方各付一半  </if>
							</td>
							<td>
								<if condition="$vo['yes_no'] eq 1">等待<font color="fuchsia"> 买家 </font>同意条款 <elseif condition="$vo['yes_no'] eq 2"/> 等待<font
										color="#ff7f50">卖家</font>同意条款 <elseif condition="$vo['yes_no'] eq 3"/> 等待<font color="fuchsia"> 买家 </font>付钱<elseif condition="$vo['yes_no'] eq 4"/> 等待<font color="fuchsia"> 买家 </font>转移域名<elseif condition="$vo['yes_no'] eq 5"/><font color="fuchsia"> 买家 </font>拒绝<elseif condition="$vo['yes_no'] eq 6"/> <font color="#ff7f50">卖家</font>拒绝<elseif condition="$vo['yes_no'] eq 7"/> 后台关闭交易<elseif condition="$vo['yes_no'] eq 8"/> 经纪人关闭<elseif condition="$vo['yes_no'] eq 9"/> 等待确认收货 <elseif condition="$vo['yes_no'] eq 10"/> 确认此次交易达成</if>
							</td>
							<td>
								<if condition="$vo['status'] eq 1">经纪人已经介入 <elseif condition="$vo['status'] eq 2"/>交易完成<elseif condition="$vo['status'] eq 3"/>交易失败   <else/> 待经纪人介入 </if>
							</td>
							<td class="textcenter">
								 <a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Trade/edit',array('order_id'=>$vo['order_id']))}','编辑',680,560,true,false,false,editbtn,'add',true);">编辑</a> | <a href="javascript:void(0);" class="delete_row" parameter="order_id={pigcms{$vo.order_id}" url="{pigcms{:U('Trade/del')}">删除</a>
							</td>
						</tr>
					</volist>
					<tr><td class="textcenter pagebar" colspan="14">{pigcms{$pagebar}</td></tr>
					<else/>
					<tr><td class="textcenter red" colspan="12">列表为空！</td></tr>
				</if>
				</tbody>
			</table>
		</div>
	</form>
</div>
<include file="Public:footer"/>