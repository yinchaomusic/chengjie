<include file="Public:header"/>
<div class="mainbox">
	<div id="nav" class="mainnav_title">
		<ul>
			<a href="{pigcms{:U('Trade/procurement')}" class="on">域名代购列表</a>|
			<a href="{pigcms{:U('Trade/procure_site')}" class="on">域名代购设置</a>|

		</ul>
	</div>
	<table class="search_table" width="100%">
		<tr>
			<td>
				<form action="{pigcms{:U('Trade/procurement')}" method="get">
					<input type="hidden" name="c" value="Trade"/>
					<input type="hidden" name="a" value="procurement"/>
					筛选: <input type="text" name="keyword" class="input-text" value="{pigcms{$_GET['keyword']}"/>
					<select name="searchtype">
						<option value="ph_id" <if condition="$_GET['searchtype'] eq 'ph_id'">selected="selected"</if>>代购ID</option>
						<option value="domain" <if condition="$_GET['searchtype'] eq 'domain'">selected="selected"</if>>域名</option>
						<option value="uid" <if condition="$_GET['searchtype'] eq 'uid'">selected="selected"</if>>用户ID</option>
						<option value="staff_id" <if condition="$_GET['searchtype'] eq 'staff_id'">selected="selected"</if>>经纪人ID</option>
					</select>
					<input type="submit" value="查询" class="button"/>
				</form>
			</td>
		</tr>
	</table>
	<form name="myform" id="myform" action="" method="post">
		<div class="table-list">
			<table width="100%" cellspacing="0">
				<colgroup><col> <col> <col> <col><col><col><col> <col width="140" align="center"> </colgroup>
				<thead>
				<tr>
					<th>代购ID</th>
					<th>代购域名</th>
					<th>心里预算</th>
					<th>成交价格</th>
					<th>用户ID</th>
					<th>用户名称</th>
					<th>经纪人ID</th>
					<th>经纪人名称</th>
					<th>添加时间</th>
					<th>状态</th>
					<th>是否显示案例</th>
					<th>留言</th>
					<th>原因</th>
					<th>结束时间</th>
					<th class="textcenter">操作</th>
				</tr>
				</thead>
				<tbody>
				<if condition="is_array($purchase)">
					<volist name="purchase" id="vo">
						<tr>
							<td>{pigcms{$vo.ph_id}</td>
							<td><strong> {pigcms{$vo.domain}</strong></td>
							<td>￥{pigcms{$vo.buyers_price|number_format}</td>
							<td>￥{pigcms{$vo.succeed_price|number_format}</td>
							<td>{pigcms{$vo.uid}</td>
							<td>{pigcms{$vo.uname}</td>
							<td>{pigcms{$vo.staff_id}</td>
							<td>{pigcms{$vo.staff_name}</td>
							<td>{pigcms{$vo.add_time|date='Y-m-d H:i:s',###}</td>
							<td><if condition="$vo['status'] eq 0">经纪人未介入
									<elseif condition="$vo['status'] eq 1"/> 经纪人介入
									<elseif condition="$vo['status'] eq 2"/> 审核成功等待支付保证金
									<elseif condition="$vo['status'] eq 3"/> 审核失败
									<elseif condition="$vo['status'] eq 4"/> 代购达成
									<elseif condition="$vo['status'] eq 5"/> 已付款
									<elseif condition="$vo['status'] eq 6"/> 交易成功
									<elseif condition="$vo['status'] eq 7"/> 交易失败
									<elseif condition="$vo['status'] eq 8"/> 卖家已转移域名
									<elseif condition="$vo['status'] eq 9"/> 卖家同意

								</if></td>
							<td><if condition="$vo['is_show'] eq 1">显示
									<elseif condition="$vo['is_show'] eq 0"/> 隐藏
								</if></td>
							<td>{pigcms{$vo.message}</td>
							<td>{pigcms{$vo.why}</td>
							<td>
								<if condition="$vo['over_time'] neq ''">{pigcms{$vo.over_time|date='Y-m-d H:i:s',###}
									<else />-
								</if></td>


							<td class="textcenter">
								<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Trade/procurement_edit',array('ph_id'=>$vo['ph_id']))}','编辑后缀',520,460,true,false,false,editbtn,'add',true);">编辑</a>
<!--								| <a href="javascript:void(0);" class="delete_row" parameter="ph_id={pigcms{$vo.ph_id}" url="{pigcms{:U('Trade_mode/del')}">删除</a>-->
							</td>
						</tr>
					</volist>
					<tr><td class="textcenter pagebar" colspan="16">{pigcms{$pagebar}</td></tr>
					<else/>
					<tr><td class="textcenter red" colspan="16">列表为空！</td></tr>
				</if>
				</tbody>
			</table>
		</div>
	</form>
</div>
<include file="Public:footer"/>