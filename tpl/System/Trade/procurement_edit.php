<include file="Public:header"/>
<form   method="post" action="{pigcms{:U('Trade/procurement_edit_save')}" frame="true"  >
	<input type="hidden" name="ph_id" value="{pigcms{$now_order.ph_id}"/>
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="15%">代购ID</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.ph_id}</div></td>
			<th width="15%">代购域名</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.domain}</div></td>
		<tr/>
		<tr>
			<th width="15%">心里预算</th>
			<td width="35%">￥{pigcms{$now_order.buyers_price|number_format}</td>
			<th width="15%">成交价格</th>
			<td width="35%">￥{pigcms{$now_order.succeed_price|number_format}</td>
		</tr>
		<tr>
			<th width="15%">用户ID</th>
			<td width="35%">{pigcms{$now_order.uid}</td>
			<th width="15%">用户名称 </th>
			<td width="35%" class="radio_box">{pigcms{$now_order.uname}</td>
		</tr>
		<tr>
			<th width="15%"><font color="fuchsia"> 经纪人ID </font>ID</th>
			<td width="35%">{pigcms{$now_order.staff_id}</td>
			<th width="15%"><font color="#ff7f50">经纪人名称</font>ID</th>
			<td width="35%">{pigcms{$now_order.staff_name}</td>
		</tr>
		<tr>
			<th width="15%">添加时间</th>
			<td width="35%"> {pigcms{$now_order.add_time|date='Y-m-d H:i:s',###} </td>
			<th width="15%">状态</th>
			<td width="35%" class="radio_box">
				<if condition="$now_order['status'] eq 0">经纪人未介入
					<elseif condition="$now_order['status'] eq 1"/> 经纪人介入
					<elseif condition="$now_order['status'] eq 2"/> 审核成功等待支付保证金
					<elseif condition="$now_order['status'] eq 3"/> 审核失败
					<elseif condition="$now_order['status'] eq 4"/> 代购达成
					<elseif condition="$now_order['status'] eq 5"/> 已付款
					<elseif condition="$now_order['status'] eq 6"/> 交易成功
					<elseif condition="$now_order['status'] eq 7"/> 交易失败
					<elseif condition="$now_order['status'] eq 8"/> 卖家已转移域名
					<elseif condition="$now_order['status'] eq 9"/> 卖家同意

				</if>
			</td>
		</tr>
		<tr>
			<th width="15%">是否显示案例</th>
			<td class="radio_box">
				<span class="cb-enable"><label class="cb-enable <if condition="$now_order['is_show'] eq 1">selected</if>"><span>显示</span><input type="radio" name="is_show" value="1" <if condition="$now_order['is_show'] eq 1">checked="checked"</if>/></label></span>
				<span class="cb-disable"><label class="cb-disable <if condition="$now_order['is_show'] eq 0">selected</if>"><span>隐藏</span><input type="radio" name="is_show" value="0" <if condition="$now_order['is_show'] eq 0">checked="checked"</if>/></label></span>
			</td>
			<th width="15%">留言</th>
			<td width="35%" class="radio_box">
				{pigcms{$now_order.message}
			</td>
		</tr>
		<tr>
			<th width="15%">原因</th>
			<td width="35%"> {pigcms{$now_order.why}</td>
			<th width="15%">结束时间</th>
			<td width="35%">
				<if condition="$now_order['over_time'] neq ''">{pigcms{$now_order.over_time|date='Y-m-d H:i:s',###}
					<else />-
				</if>
			</td>
		</tr>

	</table>

		<div class="btn hidden">
			<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
			<input type="reset" value="取消" class="button" />
		</div>

</form>
<include file="Public:footer"/>