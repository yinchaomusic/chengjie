<include file="Public:header"/>
<form   method="post" action="{pigcms{:U('Trade/edit_save')}" frame="true"  >
	<input type="hidden" name="order_id" value="{pigcms{$now_order.order_id}"/>
	<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
		<tr>
			<th width="15%">订单ID</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.order_id}</div></td>
			<th width="15%">订单创建时间</th>
			<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$now_order.addTime|date='Y-m-d H:i:s',###}</div></td>
		<tr/>
		<tr>
			<th width="15%">订单总价(￥)</th>
			<td width="35%">￥{pigcms{$now_order.total_price}</td>
			<th width="15%">域名</th>
			<td width="35%">{pigcms{$now_order.domainName}</td>
		</tr>
		<tr>
			<th width="15%">交易类型</th>
			<td width="35%"><if condition="$now_order['trade_type'] eq 1">中介交易</if></td>
			<th width="15%">订单关闭时间 </th>
			<td width="35%" class="radio_box">
				<if condition="$vo['up_time'] eq '' ">-<else />{pigcms{$vo.up_time|date='Y-m-d H:i:s',###}</if>
			</td>
		</tr>
		<tr>
			<th width="15%"><font color="fuchsia"> 买家 </font>ID</th>
			<td width="35%">{pigcms{$now_order.buy_id}</td>
			<th width="15%"><font color="#ff7f50">卖家</font>ID</th>
			<td width="35%">{pigcms{$now_order.sell_id}</td>
		</tr>
		<tr>
			<th width="15%">经纪人ID</th>
			<td width="35%"><if condition="$now_order['staff_id'] eq '' ">-<else /> {pigcms{$now_order.staff_id}</if></td>
			<th width="15%">中介付费方式</th>
			<td width="35%" class="radio_box">
				<if condition="$now_order['payTye'] eq 1"><font color="fuchsia"> 买家 </font><elseif condition="$now_order['payTye'] eq 2"/><fontcolor="#ff7f50">卖家</font> <else/> 双方各付一半 </if>
			</td>
		</tr>
		<tr>
			<th width="15%">注册商/注册商ID</th>
			<td width="35%">{pigcms{$now_order.registrar} / {pigcms{$now_order.registrar_id}</td></td>
			<th width="15%">转移类型</th>
			<td width="35%" class="radio_box">
				<if condition="$now_order['transfer_type'] eq 1 ">卖家提供域名转移密码<elseif condition="$now_order['transfer_type'] eq 2" />当前注册商内转移(PUSH)</if>
			</td>
		</tr>
		<tr>
			<th width="15%">订单进程</th>
			<td width="35%">
				<if condition="$now_order['yes_no'] eq 1">等待<font color="fuchsia"> 买家 </font>同意条款 <elseif condition="$now_order['yes_no'] eq 2"/> 等待<font
						color="#ff7f50">卖家</font>同意条款 <elseif condition="$vo['yes_no'] eq 3"/> 等待<font color="fuchsia"> 买家 </font>付钱<elseif condition="$now_order['yes_no'] eq 4"/> 等待<font color="fuchsia"> 买家 </font>转移域名<elseif condition="$now_order['yes_no'] eq 5"/><font color="fuchsia"> 买家 </font>拒绝<elseif condition="$now_order['yes_no'] eq 6"/> <font
						color="#ff7f50">卖家</font>拒绝 <elseif condition="$now_order['yes_no'] eq 7"/>后台关闭 </if></td>
			<th width="15%">交易状态</th>
			<td width="35%">
				&nbsp;
				<if condition="$now_order['status'] eq 1"> 经纪人已经介入 <elseif condition="$now_order['status'] eq 2"/>交易完成<elseif condition="$now_order['status'] eq 3"/>交易失败 <elseif condition="$now_order['status'] eq 3"/> 后台关闭交易<else />待经纪人介入 </if>
			</td>
		</tr>
		<tr>
			<th width="15%">订单状态修改</th>
			<td width="35%">
				<span class="cb-enable"><label class="cb-enable <if condition="$now_order['yes_no'] neq 7">selected</if>"><span>正常</span><input type="radio" name="yes_no" value="" <if condition="$now_order['yes_no'] neq 7">checked="checked"</if>/></label></span>
				<span class="cb-disable"><label class="cb-disable <if condition="$now_order['yes_no'] eq 7">selected</if>"><span>关闭</span><input type="radio" name="yes_no" value="7" <if condition="$now_order['yes_no'] eq 7">checked="checked"</if>/></label></span>
				 <br /> <br />
				注意：一旦订单关闭之后不可以再开启，请谨慎操作！
			</td>
			<th width="15%">关闭说明</th>
			<td width="35%">
				<textarea name="info" id="info" cols="30"  rows="5">{pigcms{$order_info.info} </textarea>
			</td>
		</tr>
		<tr>
			<th width="15%">记录表</th>
			<td width="85%" colspan="3">
				<div style="height:30px;line-height:24px;">
					<a href="javascript:void(0);" onclick="window.top.artiframe('{pigcms{:U('Trade/operalog',array('order_id'=>$now_order['order_id']))}','订单操作记录',680,560,true,false,false,null,'operalog',true);">订单操作记录</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</div>
			</td>
		</tr>
	</table>
	<if condition="($now_order['yes_no'] neq 7) OR ($now_order['yes_no'] neq 8)">
	<div class="btn hidden">
		<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
		<input type="reset" value="取消" class="button" />
	</div>
		<else/>
		该订单已经关闭
	</if>
</form>
<include file="Public:footer"/>