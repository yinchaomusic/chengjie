<include file="Public:header" xmlns="http://www.w3.org/1999/html"/>
<div class="mainbox">
<!--	<table class="search_table" width="100%">
		<tr>
			<td>
				<form action="{pigcms{:U('Terminal/index')}" method="post">
					筛选: <input type="text" name="keyword" class="input-text" />
					<select name="searchtype">
						<option value="domain_name">域名</option>
						<option value="staff_id">经纪人ID</option>
                                                <option value="uid">用户id</option>
					</select> &nbsp;
					交易状态:
					<select name="status">
						<option value="">全部</option>
						<option value="0">待审核</option>
                                                <option value="1">推荐中</option>
                                                <option value="2">审核失败</option>
                                                <option value="3">推荐成功</option>
                                                <option value="4">推荐失败</option>
					</select>&nbsp;
					<input type="submit" value="查询" class="button"/>
				</form>
			</td>
		</tr>
	</table>-->
	<form name="myform" id="myform" action="" method="post">
		<div class="table-list">
			<table width="100%" cellspacing="0">
				<colgroup><col> <col> <col> <col><col><col><col> <col width="140" align="center"> </colgroup>
				<thead>
				<tr>
					<th>买家</th>
					<th>卖家</th>
                                        <th>经纪人</th>
					<th>起因</th>
                                        <th>结果</th>
                                        <th>添加时间</th>
                                        <th></th>
					<th class="textcenter">操作</th>
				</tr>
				</thead>
				<tbody>
				<if condition="is_array($list)">
					<volist name="list" id="vo">
						<tr>
                                                        <td>{pigcms{$vo.buy_uid}</td>
                                                        <td>{pigcms{$vo.sell_uid}</td>
                                                        <td>{pigcms{$vo.staff_id}</td>
                                                        <td>
                                                            <div style="width:200px;height:20px;text-overflow:ellipsis; white-space:nowrap; overflow:hidden; ">{pigcms{$vo.origin}</div>
<!--                                                            <span style="width: 120px;text-overflow:ellipsis;">{pigcms{$vo.origin}</span>-->
                                                        </td>
                                                        <td>
                                                            <div style="width:200px;height:20px;text-overflow:ellipsis; white-space:nowrap; overflow:hidden; ">{pigcms{$vo.result}</div>
<!--                                                            <span style="width: 120px;text-overflow:ellipsis;">{pigcms{$vo.result}</span>-->
                                                        </td>
<!--                                                        <td>{pigcms{$vo.result}</td>-->
							<td>{pigcms{$vo.add_time|date='Y-m-d',###}</td>
							<td></td>
							<td class="textcenter">
                                                            <a href="{pigcms{:U('Dispute/Dispute_show',array('id'=>$vo['id']))}">查看</a>
							</td>
						</tr>
					</volist>
					<tr><td class="textcenter pagebar" colspan="14">{pigcms{$pagebar}</td></tr>
					<else/>
					<tr><td class="textcenter red" colspan="12">列表为空！</td></tr>
				</if>
				</tbody>
			</table>
		</div>
	</form>
</div>
<include file="Public:footer"/>