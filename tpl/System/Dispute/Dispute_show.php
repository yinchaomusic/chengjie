<include file="Public:header"/>
<div class="mainnav_title" id="nav">
		<ul>
			<a class="on" href="{pigcms{:U('Dispute/index')}">返回纠纷列表</a>
		</ul>
	</div>
		<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
			<tr>
                            <th width="80">买家id</th>
                            <td><input type="text" class="input fl"size="20"value="{pigcms{$show.buy_uid}" readonly="readonly"  /></td>
			</tr>
                        <tr>
                            <th width="80">卖家id</th>
                            <td><input type="text" class="input fl"size="20"value="{pigcms{$show.sell_uid}" readonly="readonly"  /></td>
			</tr>
                        <tr>
                            <th width="80">经纪人</th>
                            <td><input type="text" class="input fl"size="20"value="{pigcms{$show.staff_id}" readonly="readonly"  /></td>
			</tr>
                        <tr>
                            <th width="80">添加时间</th>
                            <td>{pigcms{$show.add_time|date="Y-m-d H:i:s",###}</td>
			</tr>
                        
                        <tr>
                                <th width="80">起因</th>
                                <td>
                                        <textarea id="origin" name="origin"  placeholder="可以在这里添加起因">{pigcms{$show['origin']|htmlspecialchars_decode=ENT_QUOTES}</textarea>
                                </td>
                        </tr>
                        <tr>
                                <th width="80">结果</th>
                                <td>
                                        <textarea id="result" name="result"  placeholder="可以在这里添加结果">{pigcms{$show['result']|htmlspecialchars_decode=ENT_QUOTES}</textarea>
                                </td>
                        </tr>
                        
		</table>

<link rel="stylesheet" href="{pigcms{$static_public}kindeditor/themes/default/default.css">
<script src="{pigcms{$static_public}kindeditor/kindeditor.js"></script>
<script src="{pigcms{$static_public}kindeditor/lang/zh_CN.js"></script>
<script type="text/javascript">

KindEditor.ready(function(K){
	var editor = K.editor({
		allowFileManager : true
	});
	 //var islock=false;
	K('.J_selectImage').click(function(){
		var obj=$(this);
		editor.uploadJson = "{pigcms{$config.site_url}/index.php?g=Index&c=Upload&a=editor_ajax_upload&upload_dir=system/image";
		editor.loadPlugin('image', function(){
			editor.plugin.imageDialog({
				showRemote : false,
				imageUrl : K('#course_pic').val(),
				clickFn : function(url, title, width, height, border, align) {
					obj.siblings('input').val(url);
					editor.hideDialog();
					obj.siblings('img').attr('src',url).show();
					//window.location.reload();
				}
			});
		});
	   
	});

	kind_editor = K.create("#origin",{
		width:'420px',
		height:'380px',
		minWidth:'480px',
		resizeType : 1,
		allowPreviewEmoticons:false,
		allowImageUpload : true,
		filterMode: true,
		items : [
			'source'
		],
		emoticonsPath : './static/emoticons/',
		uploadJson : "{pigcms{$config.site_url}/index.php?g=Index&c=Upload&a=editor_ajax_upload&upload_dir=system/image"
	});
        
        kind_editor = K.create("#result",{
		width:'420px',
		height:'380px',
		minWidth:'480px',
		resizeType : 1,
		allowPreviewEmoticons:false,
		allowImageUpload : true,
		filterMode: true,
		items : [
			'source'
		],
		emoticonsPath : './static/emoticons/',
		uploadJson : "{pigcms{$config.site_url}/index.php?g=Index&c=Upload&a=editor_ajax_upload&upload_dir=system/image"
	});
});
</script>
<include file="Public:footer"/>