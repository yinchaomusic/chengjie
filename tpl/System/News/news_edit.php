<include file="Public:header"/>
	
<form id="myform" method="post" action="{pigcms{:U('News/news_edit_data')}" enctype="multipart/form-data">
		<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
			<tr>
				<th width="80">新闻标题</th>
                        <input type="hidden" name="news_id" value="{pigcms{$now_news.news_id}"/>
				<td><input type="text" class="input fl" name="news_title" size="50" placeholder="请输入标题" value="{pigcms{$now_news.news_title}" validate="maxlength:50,required:true"/></td>
			</tr>
			<tr>
				<th width="80">新闻图片</th>
				<td><input type="file" class="input fl" name="pic" style="width:200px;" placeholder="请上传图片" /></td>
			</tr>
                        <tr>
                                <th width="80">新闻分类</th>
                                <td>
                                        <select name="cat_id" id="tm_id">
                                                <volist name="news_cat_list" id="vo">
                                                        <option value="{pigcms{$vo.cat_id}" <if condition="$vo['cat_id'] eq $now_news['cat_id']">selected</if>>{pigcms{$vo.cat_name}</option>+

                                                </volist>
                                        </select>

                                </td>
                        </tr>
                        
                        <tr>
                                <th width="80">新闻状态</th>
                                <td>
                                        <span class="cb-enable"><label class="cb-enable <if condition="$now_news['state'] eq 1">selected</if>"><span>显示</span><input type="radio" name="state" value="1" <if condition="$now_news['state'] eq 1">checked="checked"</if>/></label></span>
                                        <span class="cb-disable"><label class="cb-disable <if condition="$now_news['state'] eq 0">selected</if>"><span>隐藏</span><input type="radio" name="state" value="0" <if condition="$now_news['state'] eq
                                        0">checked="checked"</if>/></label></span>
                                </td>
                        </tr>
                
                        <tr>
                                <th width="80">新闻内容</th>
                                <td>
                                        <textarea id="description" name="news_content"  placeholder="可以在这里添加新闻内容">{pigcms{$now_news['news_content']|htmlspecialchars_decode=ENT_QUOTES}</textarea>
                                </td>
                        </tr>
		</table>
		<div class="btn hidden">
			<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
			<input type="reset" value="取消" class="button" />
		</div>
	</form>

<link rel="stylesheet" href="{pigcms{$static_public}kindeditor/themes/default/default.css">
<script src="{pigcms{$static_public}kindeditor/kindeditor.js"></script>
<script src="{pigcms{$static_public}kindeditor/lang/zh_CN.js"></script>
<script type="text/javascript">

KindEditor.ready(function(K){
	var editor = K.editor({
		allowFileManager : true
	});
	 //var islock=false;
	K('.J_selectImage').click(function(){
		var obj=$(this);
		editor.uploadJson = "{pigcms{$config.site_url}/index.php?g=Index&c=Upload&a=editor_ajax_upload&upload_dir=system/image";
		editor.loadPlugin('image', function(){
			editor.plugin.imageDialog({
				showRemote : false,
				imageUrl : K('#course_pic').val(),
				clickFn : function(url, title, width, height, border, align) {
					obj.siblings('input').val(url);
					editor.hideDialog();
					obj.siblings('img').attr('src',url).show();
					//window.location.reload();
				}
			});
		});
	   
	});

	kind_editor = K.create("#description",{
		width:'500px',
		height:'380px',
		minWidth:'480px',
		resizeType : 1,
		allowPreviewEmoticons:false,
		allowImageUpload : true,
		filterMode: true,
		items : [
			'source', '|', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
			'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
			'insertunorderedlist', '|', 'emoticons', 'image', 'link'
		],
		emoticonsPath : './static/emoticons/',
		uploadJson : "{pigcms{$config.site_url}/index.php?g=Index&c=Upload&a=editor_ajax_upload&upload_dir=system/image"
	});
});
</script>
<include file="Public:footer"/>