<include file="Public:header"/>
	<form  method="post" action="{pigcms{:U('News/cat_modify')}" frame="true" refresh="true" enctype="multipart/form-data">
		<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
			<tr>
				<th width="80">分类名称</th>
				<td><input type="text" class="input fl" name="cat_name" size="10" placeholder="请输入名称" validate="maxlength:20,required:true"/></td>
			</tr>
                        <tr>
				<th width="80">分类图片</th>
				<td><input type="file" class="input fl" name="icon" style="width:200px;" placeholder="请上传图片" /></td>
			</tr>
			<tr>
				<th width="80">分类标识</th>
				<td><input type="text" class="input fl" name="cat_key" size="10" placeholder="分类标识如：index_top" validate="maxlength:20,required:true" tips="一旦填写则不可以修改"/></td>
			</tr>
			<tr>
				<th width="80">排序</th>
				<td><input type="text" class="input fl" name="sort" size="10" value="" validate="number:true,maxlength:6" tips="数值越大，排序越前"/></td>
			</tr>
			<tr>
				<th width="80">是否显示</th>
				<td class="radio_box">
					<span class="cb-enable"><label class="cb-enable selected"><span>显示</span><input type="radio" selected="selected" name="state"value="1"/>
						</label></span>
					<span class="cb-disable"><label class="cb-disable"><span>隐藏</span><input type="radio" name="state" value="0" /></label></span>
				</td>
			</tr>
			<tr>
				<th width="80">备注</th>
				<td>
					<textarea name="desc" id="desc" ></textarea>
				</td>
			</tr>
		</table>
		<div class="btn hidden">
			<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
			<input type="reset" value="取消" class="button" />
		</div>
	</form>
<include file="Public:footer"/>