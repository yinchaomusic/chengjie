<include file="Public:header"/>
	<form id="myform" method="post" action="{pigcms{:U('News/news_add')}" enctype="multipart/form-data">
		<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
			<tr>
				<th width="80">新闻标题</th>
				<td><input type="text" class="input fl" name="news_title" size="50" placeholder="请输入标题" validate="maxlength:50,required:true"/></td>
			</tr>
			<tr>
				<th width="80">新闻图片</th>
				<td><input type="file" class="input fl" name="pic" style="width:200px;" placeholder="请上传图片" /></td>
			</tr>
<!--			<tr>
				<th width="80">背景颜色</th>
				<td><input type="text" class="input fl" name="bg_color" id="choose_color" style="width:120px;" placeholder="可不填写" tips="请点击右侧按钮选择颜色，用途为如果图片尺寸小于屏幕时，会被背景颜色扩充，主要为首页使用。"/>&nbsp;&nbsp;<a href="javascript:void(0);" id="choose_color_box" style="line-height:28px;">点击选择颜色</a></td>
			</tr>-->
			<tr>
                            <th width="100">新闻分类</th>
                            <td>
                                <select name="cat_id" id="is_push" validate="required:true">
                                        <option value="" selected="selected">请选择分类</option>
                                        <volist name="news_cat_list" id="vo">
                                            <option value="{pigcms{$vo['cat_id']}">{pigcms{$vo.cat_name}</option>
                                        </volist>
                                </select>

                                <em class="notice_tips" tips="分类"></em>
                            </td>
                        </tr>
			<tr>
				<th width="80">新闻状态</th>
				<td>
					<span class="cb-enable"><label class="cb-enable selected"><span>显示</span><input type="radio" name="state" value="1" checked="checked" /></label></span>
					<span class="cb-disable"><label class="cb-disable"><span>隐藏</span><input type="radio" name="state" value="0" /></label></span>
				</td>
			</tr>
                        <tr>
                                <th width="80">新闻内容</th>
<!--                                <td>
                                    <textarea name="news_content" id="info" style="margin: 0px; width: 450px; height: 250px;" placeholder="可以在这里添加新闻内容" validate="required:true"></textarea>
                                </td>-->
                                <td>
                                    <textarea id="description" name="news_content"  placeholder="可以在这里添加新闻内容">{pigcms{$leveldata['description']|htmlspecialchars_decode=ENT_QUOTES}</textarea>
                                </td>
                        </tr>
		</table>
		<div class="btn hidden">
			<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
			<input type="reset" value="取消" class="button" />
		</div>
	</form>
<script type="text/javascript" src="./static/js/artdialog/jquery.artDialog.js"></script>
<script type="text/javascript" src="./static/js/artdialog/iframeTools.js"></script>

<link rel="stylesheet" href="{pigcms{$static_public}kindeditor/themes/default/default.css">
<script src="{pigcms{$static_public}kindeditor/kindeditor.js"></script>
<script src="{pigcms{$static_public}kindeditor/lang/zh_CN.js"></script>
<script type="text/javascript">

KindEditor.ready(function(K){
	var editor = K.editor({
		allowFileManager : true
	});
	 //var islock=false;
	K('.J_selectImage').click(function(){
		var obj=$(this);
		editor.uploadJson = "{pigcms{$config.site_url}/index.php?g=Index&c=Upload&a=editor_ajax_upload&upload_dir=system/image";
		editor.loadPlugin('image', function(){
			editor.plugin.imageDialog({
				showRemote : false,
				imageUrl : K('#course_pic').val(),
				clickFn : function(url, title, width, height, border, align) {
					obj.siblings('input').val(url);
					editor.hideDialog();
					obj.siblings('img').attr('src',url).show();
					//window.location.reload();
				}
			});
		});
	   
	});

	kind_editor = K.create("#description",{
		width:'500px',
		height:'380px',
		minWidth:'480px',
		resizeType : 1,
		allowPreviewEmoticons:false,
		allowImageUpload : true,
		filterMode: true,
		items : [
			'source', '|', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
			'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
			'insertunorderedlist', '|', 'emoticons', 'image', 'link'
		],
		emoticonsPath : './static/emoticons/',
		uploadJson : "{pigcms{$config.site_url}/index.php?g=Index&c=Upload&a=editor_ajax_upload&upload_dir=system/image"
	});
});
</script>

<include file="Public:footer"/>