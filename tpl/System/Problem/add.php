<include file="Public:header"/>
<script>
$(function(){ 
$(".selectdomain").toggle(function(){ 
$(this).next("domain").hide(1000); 
},function(){ 
$(this).next("domain").show(1000); 
}); 
}); 
</script>
	<form id="myform" method="post" action="{pigcms{:U('Escrow/modify')}" enctype="multipart/form-data">
		<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
			<tr>
				<th width="80">需求类型</th>
				<td>
					<span class="cb-enable">
						<label class="cb-enable">
							<span>指定域名</span>
							<input type="radio" class='selectdomain' name="escrowType" value="1" checked="checked">
						</label>
					</span>
					<span class="cb-disable">
						<label class="cb-disable  selected">
							<span>无指定域名</span>
							<input type="radio" class='selectdomain' name="escrowType" value="0">
						</label>
					</span>
					<img src="./tpl/System/Static/images/help.gif" class="tips_img" title="无指定域名”提交经纪交易：用户在没有指定域名的情况下通过域名经纪帮助用户完成购买域名" style="margin-top:1px;">
				</td>
			</tr>
			<tr id='domain'>
				<th width="80">域名</th>
				<td><input type="text" class="input fl" name="domainName" size="20" validate="required:true" tips="请输入活动的期数，活动属于第几期！应该接上之前的期数"/></td>
			</tr>
			<tr>
				<th width="80">用户ID</th>
				<td><input type="text" class="input fl" validate="number:true,required:true" name="uid" /></td>
			</tr>
			<tr>
				<th width="80">经济人ID</th>
				<td><input type="text" class="input fl" validate="number:true,required:true" name="escrowId" /></td>
			</tr>
			<tr>
				<th width="80">报价</th>
				<td><input type="text" class="input fl" name="price" id="" style="width:120px;"  validate="number:true,required:true"/>&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<th width="80">中介费支付方式</th>
				<td>
					<select name="payTye" class="valid">
						<option value="1"  selected="selected">买方支付</option>
						<option value="2"> 卖方支付</option>
						<option value="3">双方各付一半</option>
						
					</select></td>
			</tr>
			<tr>
				<th width="80">联系人</th>
				<td><input type="text" class="input fl" name="linkman" style="width:120px;" id="" validate="required:true"/></td>
			</tr>
			<tr>
				<th width="80">手机号码</th>
				<td><input type="text" class="input fl" name="phone" style="width:120px;" id="" validate="mobile:true,required:true" /></td>
			</tr>
			<tr>
				<th width="80">QQ号码</th>
				<td><input type="text" class="input fl" name="qq" style="width:120px;" id="" validate="number:true,required:true" /></td>
			</tr>
			<tr>
				<th width="80">邮箱</th>
				<td><input type="text" class="input fl" name="email" style="width:120px;" id="" validate="required:true" /></td>
			</tr>
			<tr>
				<th width="80">留言</th>
				<td><textarea  class="input fl" name="msg" style="width:320px;height:80px" id=""></textarea> </td>
			</tr>
		</table>
		<div class="btn hidden">
			<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
			<input type="reset" value="取消" class="button" />
		</div>
	</form>
<include file="Public:footer"/>