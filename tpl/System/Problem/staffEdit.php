<include file="Public:header"/>
	<form id="myform" method="post" action="{pigcms{:U('Escrow/staffEdit',array('id'=>$_GET['id']))}" enctype="multipart/form-data">
		<input type="hidden" name="id" value="{pigcms{$staff.id}"/>
		<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
			<tr>
				<th width="15%">ID</th>
				<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$staff.id}</div></td>
				<th width="15%">昵称</th>
				<td width="35%"><input type="text" class="input fl" name="userName" size="20" validate="maxlength:50,required:true" value="{pigcms{$staff.userName}"/></td>
			<tr/>
			<tr>
				<th width="15%">姓名</th>
				<td width="35%"><input type="text" class="input fl" name="name" size="20" validate="maxlength:50,required:true" value="{pigcms{$staff.name}"/></td>
				<th width="15%">手机号</th>
				<td width="35%"><input type="text" class="input fl" name="phone" size="20" validate="mobile:true" value="{pigcms{$staff.phone}" autocomplete="off"/></td>
			</tr>
         
			<tr>
				<th width="15%">密码</th>
				<td width="35%"><input type="password" class="input fl" name="pwd" size="20" value="" tips="不修改则不填写" autocomplete="off"/></td>
                                <th width="15%">QQ号</th>
				<td width="35%"><input type="text" class="input fl" name="qq" size="20" value="{pigcms{$staff.qq}"/></td>
			</tr>
			<tr>
                            <th width="15%">邮箱</th>
				<td width="35%"><input type="text" class="input fl" name="email" size="20" value="{pigcms{$staff.email}"/></td>
				<th width="15%">状态</th>
				<td width="35%" class="radio_box">
					<span class="cb-enable"><label class="cb-enable <if condition="$staff['state'] eq 1">selected</if>"><span>正常</span><input type="radio" name="state" value="1"  <if condition="$staff['state'] eq 1">checked="checked"</if>/></label></span>
					<span class="cb-disable"><label class="cb-disable <if condition="$staff['state'] eq 0">selected</if>"><span>禁止</span><input type="radio" name="state" value="0"  <if condition="$staff['state'] eq 0">checked="checked"</if>/></label></span>
				</td>
			</tr>
                        <tr>
				<th width="15%">域名交易总数量</th>
				<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$staff.domainSum}</div></td>
				<th width="15%">域名交易总额</th>
				<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$staff.GMV}</div></td>
			<tr/>
                        <tr>
				<th width="15%">名下会员数量</th>
				<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$staff.userSum}</div></td>
				<th width="15%">交易成功数量</th>
				<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$staff.successSum}</div></td>
			<tr/>
                        <tr>
				<th width="15%">交易失败数量</th>
				<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$staff.errorSum}</div></td>

			<tr/>
			<tr>
				<th width="15%">最后访问时间</th>
				<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$staff.lastTime|date='Y-m-d H:i:s',###}</div></td>
				<th width="15%">最后访问IP</th>
				<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$staff.lastIp|long2ip=###}</div></td>
			</tr>
                        <tr>
				<th width="15%">注册时间</th>
				<td width="35%"><div style="height:24px;line-height:24px;">{pigcms{$staff.addTime|date='Y-m-d H:i:s',###}</div></td>
			</tr>

		</table>
		<div class="btn hidden">
			<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
			<input type="reset" value="取消" class="button" />
		</div>
	</form>
<include file="Public:footer"/>