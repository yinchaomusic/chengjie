<include file="Public:header"/>
<script>
$(function(){ 
$(".selectdomain").toggle(function(){ 
$(this).next("domain").hide(1000); 
},function(){ 
$(this).next("domain").show(1000); 
}); 
}); 
</script>
	<form id="myform" method="post" action="{pigcms{:U('Escrow/staffadd')}" enctype="multipart/form-data">
		<table cellpadding="0" cellspacing="0" class="frame_form" width="100%">
			<tr>
				<th width="80">状态</th>
				<td>
					<span class="cb-enable">
						<label class="cb-enable">
							<span>正常</span>
							<input type="radio" class='selectdomain' name="state" value="1" checked="checked">
						</label>
					</span>
					<span class="cb-disable">
						<label class="cb-disable  selected">
							<span>异常</span>
							<input type="radio" class='selectdomain' name="state" value="0">
						</label>
					</span>
					<img src="./tpl/System/Static/images/help.gif" class="tips_img" title="异常状态下，将无法接待新的客户，以及不会在平台中介列表中出现" style="margin-top:1px;">
				</td>
			</tr>
			
			<tr>
				<th width="80">姓名</th>
				<td><input type="text" class="input fl" name="name" style="width:120px;" id="" validate="required:true"/></td>
			</tr>
			<tr>
				<th width="80">用户名：</th>
				<td>
					<input type="text" class="input fl" name="userName" style="width:120px;" id="" validate="required:true"/>
					<img src="./tpl/System/Static/images/help.gif" class="tips_img" title="用于登陆经济人管理中心的帐号" style="margin-top:1px;">
				</td>
			</tr>
			<tr>
				<th width="80">密码：</th>
				<td><input type="password" class="input fl" name="pwd" style="width:120px;" id="" validate="required:true"/><img src="./tpl/System/Static/images/help.gif" class="tips_img" title="用于登陆经济人管理中心的密码" style="margin-top:1px;"></td>
			</tr>
			<tr>
				<th width="80">手机号码</th>
				<td><input type="text" class="input fl" name="phone" style="width:120px;" id="" validate="mobile:true,required:true" /></td>
			</tr>
			<tr>
				<th width="80">QQ号码</th>
				<td><input type="text" class="input fl" name="qq" style="width:120px;" id="" validate="number:true,required:true" /></td>
			</tr>
			<tr>
				<th width="80">邮箱</th>
				<td><input type="text" class="input fl" name="email" style="width:120px;" id="" validate="required:true" /></td>
			</tr>
			
		</table>
		<div class="btn hidden">
			<input type="submit" name="dosubmit" id="dosubmit" value="提交" class="button" />
			<input type="reset" value="取消" class="button" />
		</div>
	</form>
<include file="Public:footer"/>