<?php
/*
 * 若需要关闭 Memcache，采用文件缓存，将下个三行配置进行缓存
 */
if(extension_loaded('memcache')){
	return array(
		'DATA_CACHE_TYPE' => 'Memcache',		//缓存类型
		'MEMCACHE_HOST'   => '127.0.0.1',	// Memcache主机
		'MEMCACHE_PORT' => 11211,				// Memcache端口
	);
}else{
	return array();
}