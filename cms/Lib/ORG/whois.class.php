<?php
class whois {
    public function getWhois($domain){
		$WhoisAPI = 'http://whois.263.tw/weixinindex.php?domain='.$domain;	//定义whois api的url


		import('ORG.Net.Http');
		$http = new Http();
		$WhoisData = Http::curlGet($WhoisAPI);

		if(!empty($WhoisData)){
			$WhoisDataRows = explode("\n",$WhoisData);
			$WhoisDataArray = array();
			foreach($WhoisDataRows as $value){
				$tmpValue = $this->changeValue(explode(':',trim($value)));
				if(count($tmpValue) == 2){
					$tmpValue[1] = trim($tmpValue[1]);
					$tmpValue[0] = trim($tmpValue[0]);
					if($tmpValue[0] == 'Domain Name' || $tmpValue[0] =='﻿Domain Name'){
						$WhoisDataArray['domainName'] = strtolower($tmpValue[1]);
					}
					if($tmpValue[0] == 'Registrar' || $tmpValue[0] == 'Sponsoring Registrar'){
						$WhoisDataArray['registrar'] = $tmpValue[1];
					}
					if($tmpValue[0] == 'Registrant Name' || $tmpValue[0] == 'Registrant'){
						$WhoisDataArray['registrarName'] = $tmpValue[1];
					}
					if($tmpValue[0] == 'Registrant Email'){
						$WhoisDataArray['registrarEmail'] = strtolower($tmpValue[1]);
					}
					if($tmpValue[0] == 'Registrant Contact Email'){
						$WhoisDataArray['registrarEmail'] = $tmpValue[1];
					}
					if($tmpValue[0] == 'Creation Date' || $tmpValue[0] == 'Registration Time'){
						$WhoisDataArray['creationDate'] = $tmpValue[1];
					}
					if($tmpValue[0] == 'Updated Date' || $tmpValue[0] == 'Registration Time'){
						$WhoisDataArray['updatedDate'] = $tmpValue[1];
					}
					if($tmpValue[0] == 'Registrar Registration Expiration Date' ||$tmpValue[0] == 'Registry Expiry Date'){
						$WhoisDataArray['expirationDate'] = $tmpValue[1];
					}
					if($tmpValue[0] == 'Domain Status'){
						$tmpValue[1] = str_replace('clientUpdateProhibited','clientUpdateProhibited[运营商设置了禁止修改]',$tmpValue[1]);
						$tmpValue[1] = str_replace('clientTransferProhibited','clientTransferProhibited[运营商设置了禁止转移]',$tmpValue[1]);
						$tmpValue[1] = str_replace('clientDeleteProhibited','clientDeleteProhibited[运营商设置了禁止删除]',$tmpValue[1]);
						$tmpValue[1] = str_replace('serverUpdateProhibited','serverUpdateProhibited[域名服务器设置了禁止修改]',$tmpValue[1]);
						$tmpValue[1] = str_replace('serverTransferProhibited','serverTransferProhibited[域名服务器设置了禁止转移]',$tmpValue[1]);
						$tmpValue[1] = str_replace('serverDeleteProhibited','serverDeleteProhibited[域名服务器设置了禁止删除]',$tmpValue[1]);
						$WhoisDataArray['domainStatus'] = $WhoisDataArray['domainStatus'] ? $WhoisDataArray['domainStatus'].PHP_EOL.$tmpValue[1] : $tmpValue[1];
					}
					if($tmpValue[0] == 'Name Server'){
						$WhoisDataArray['nameServer'] = $WhoisDataArray['nameServer'] ? $WhoisDataArray['nameServer'].PHP_EOL.$tmpValue[1] : $tmpValue[1];
					}
				}
			}
			$WhoisDataArray['all'] = $WhoisData;
//dump($WhoisDataArray);
//			die;
			return $WhoisDataArray;
		}else{
			return array();
		}
	}

	private function changeValue($array){
		$return[0] = $array[0];
		unset($array[0]);
		$return[1] = implode(':',$array);
		return $return;
	}
}
?>