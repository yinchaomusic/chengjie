<?php
class Page{
	// 起始行数
    public $firstRow;
	//现在页数
	public $nowPage;
	//总页数
	public $totalPage;
	//总行数
	public $totalRows;
	//分页的条数
	public $page_rows;
	//架构函数
	public function __construct($totalRows,$listRows){
		$this->totalRows = $totalRows;
		$this->nowPage  = !empty($_GET['page']) ? intval($_GET['page']) : 1;
		$this->listRows = $listRows;
		$this->totalPage = ceil($totalRows/$listRows);
		if($this->nowPage > $this->totalPage && $this->totalPage>0){
			$this->nowPage = $this->totalPage;
		}
		$this->firstRow = $listRows*($this->nowPage-1);
	}
    public function show(){
		if($this->totalRows == 0) return false;
		$now = $this->nowPage;
		$total = $this->totalPage;
		
		$str = '';
		if($now > 1){
			$str .= '<a class="pa_disabled" title="上一页" k="'.($now-1).'" href="javascript:void(0)"></a>';
		}else{
			$str .= '<a class="pa_disabled" title="上一页" href="javascript:void(0)"></a>';
		}
		if($now > 5){
			$str .= '<a href="javascript:void(0)" k="1" class="s-def">1</a>';
		}
		for($i=1;$i<=5;$i++){
			if($now <= 1){
				$page = $i;
			}elseif($now > $total-1){
				$page = $total-5+$i;
			}else{
				$page = $now-3+$i;
			}
			if($page != $now  && $page>0){
				if($page<=$total){
					$str .= '<a href="javascript:void(0)" k="'.$page.'" class="s-def">'.$page.'</a>';
				}else{
					break;
				}
			}else{
				if($page == $now) $str .= '<a href="javascript:void(0)" class="s-def t">'.$page.'</a>';
			}
		}
		
		if ($now != $total){
			$str .= '<a class="pg_enabled" title="下一页" href="javascript:void(0)" k="'.($now+1).'"></a>';
		}else{
			$str .= '<a class="pg_enabled" title="下一页" href="javascript:void(0)"></a>';
		}
		return $str;
    }
}
?>