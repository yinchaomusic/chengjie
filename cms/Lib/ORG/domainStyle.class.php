<?php
/* 
 * 分析域名
 * 
 */
class domainStyle{
	protected $area_code = array(0310,0311,0312,0313,0314,0315,0316,0317,0318,0319,0335,0570,0571,0572,0573,0574,0575,0576,0577,0578,0579,0580,024,0410,0411,0412,0413,0414,0415,0416,0417,0418,0419,0421,0427,0429,027,0710,0711,0712,0713,0714,0715,0716,0717,0718,0719,0722,0724,0728,025,0510,0511,0512,0513,0514,0515,0516,0517,0517,0518,0519,0523,0470,0471,0472,0473,0474,0475,0476,0477,0478,0479,0482,0483,0790,0791,0792,0793,0794,0795,0796,0797,0798,0799,0701,0350,0351,0352,0353,0354,0355,0356,0357,0358,0359,0930,0931,0932,0933,0934,0935,0936,0937,0938,0941,0943,0530,0531,0532,0533,0534,0535,0536,0537,0538,0539,0450,0451,0452,0453,0454,0455,0456,0457,0458,0459,0591,0592,0593,0594,0595,0595,0596,0597,0598,0599,020,0751,0752,0753,0754,0755,0756,0757,0758,0759,0760,0762,0763,0765,0766,0768,0769,0660,0661,0662,0663,028,0810,0811,0812,0813,0814,0816,0817,0818,0819,0825,0826,0827,0830,0831,0832,0833,0834,0835,0836,0837,0838,0839,0840,0730,0731,0732,0733,0734,0735,0736,0737,0738,0739,0743,0744,0745,0746,0370,0371,0372,0373,0374,0375,0376,0377,0378,0379,0391,0392,0393,0394,0395,0396,0398,0870,0871,0872,0873,0874,0875,0876,0877,0878,0879,0691,0692,0881,0883,0886,0887,0888,0550,0551,0552,0553,0554,0555,0556,0557,0558,0559,0561,0562,0563,0564,0565,0566,0951,0952,0953,0954,0431,0432,0433,0434,0435,0436,0437,0438,0439,0440,0770,0771,0772,0773,0774,0775,0776,0777,0778,0779,0851,0852,0853,0854,0855,0856,0857,0858,0859,029,0910,0911,0912,0913,0914,0915,0916,0917,0919,0971,0972,0973,0974,0975,0976,0977,0890,0898,0899,0891,0892,0893,010,021,022,023,852,853);
	protected $single_pinyin = array('zhuang','chuang','shuang','chuai','chong','jiong','cheng','shang','sheng','chang','zhang','shuai','xiong','qiang','xiang','guang','qiong','jiang','shuan','huang','zheng','chuan','zhuai','kuang','zhuan','liang','niang','zhong','heng','quan','guan','hong','guai','tiao','tian','hang','qing','tang','qiao','teng','liao','tong','meng','gang','lian','wang','mian','ping','qian','leng','gong','suan','geng','mang','ruan','tuan','ting','lang','seng','juan','shou','luan','kuan','kuai','rang','shen','shan','keng','shai','shao','shei','kang','jing','shua','shun','shui','shuo','kong','huai','huan','reng','ling','long','feng','jiao','sang','jian','song','xian','chao','chen','zhan','chan','chai','nuan','ceng','zhao','chou','zhai','cong','zeng','weng','chuo','chun','chua','chui','zhei','zhen','zhuo','bang','zhun','zong','niao','ning','nong','zuan','zhui','beng','zhou','nian','cang','bing','zhua','bian','biao','cuan','neng','peng','pian','duan','dong','yang','dian','ding','piao','xuan','fang','miao','rong','xiao','ming','xing','ying','diao','yuan','yong','pang','nang','zang','deng','dang','pou','qia','ren','pin','pan','pai','nuo','qie','nou','pie','rao','qin','qiu','pao','nun','ran','qun','que','nve','pei','pen','tao','yin','yao','you','yue','yun','yan','xun','xie','xia','xin','xiu','xue','zai','zan','zou','zhu','zui','zun','zuo','zhi','zhe','zao','zei','zen','zha','wen','wei','she','sha','shi','shu','sou','sen','sao','run','rui','ruo','sai','san','sui','sun','tun','tui','tuo','wai','wan','tou','tie','suo','tai','tan','niu','rou','miu','gua','gou','gen','gui','gun','hai','guo','gei','gao','fei','fan','fen','fou','gan','gai','han','nin','jiu','jin','jie','jue','jun','kan','kai','jia','huo','hen','hei','hou','hua','hun','hui','eng','duo','cao','can','cai','cen','cha','chi','che','bin','bie','bai','ang','ban','bao','ben','bei','chu','cou','die','dia','diu','dou','dun','dui','dei','den','cun','cui','cuo','dai','dao','dan','kao','hao','lie','lin','luo','min','mie','lei','nei','lia','liu','lou','lve','lun','mou','lue','nai','nao','nan','lao','nen','kua','lai','mao','mai','kuo','kun','nie','kui','mei','men','ken','kou','lan','man','mo','ze','yi','di','yu','pi','po','de','mi','da','yo','za','pu','ce','zi','nv','ni','ng','nu','ba','ai','an','ao','zu','bi','bo','ye','na','mu','ci','ne','pa','bu','ca','ou','cu','ei','ha','li','ru','ta','te','ri','tu','gu','re','ti','he','le','se','ju','ku','ka','ji','la','su','hu','si','sa','qi','lu','en','ma','er','fa','ke','xu','me','ya','du','ge','xi','fu','ga','wa','lv','qu','fo','wu','wo','m','e','o','a',);
	protected $shengmu = array('b','p','m','f','d','t','n','l','g','k','h','j','q','x','r','z','c','s','y','w');
	protected $yunmu = array('a','o','e','i','u','v');
    protected $returnArr = array();
	/*
	 * return array
	 */
	public function getStyle($domain){
		if(preg_match('/^\d+$/',$domain)){
			$this->matchNumber($domain);
		}else if(preg_match('/^[a-z]+$/',$domain)){
			$this->matchWord($domain);
		}else{
			$this->matchMix($domain);
		}
		
		$style = $this->returnArr['style'];
		$newStyle = array();
		foreach($style as $key=>$value){
			if($value){
				$newStyle[] = $key;
			}
		}
		$this->returnArr['style'] = $newStyle;
		return $this->returnArr;
	}
	protected function matchNumber($domain){
		$strlen = strlen($domain);
		$this->returnArr['styleType'] = 'a';
		if($strlen > 6){
			$this->returnArr['label'] = '多数字';
		}else{
			$this->returnArr['label'] = $strlen.'数字';
		}
		//5个相同数字
		if($strlen >= 5){
			for($i=0;$i<10;$i++){
				if(substr_count($domain,$i) == 5){
					$this->returnArr['style']['a1'] = true;
					$this->returnArr['style']['a2'] = true;
					$this->returnArr['style']['a3'] = true;
					$this->returnArr['style']['a4'] = true;
					break;
				}
			}
		}
		//4个相同数字
		if(empty($this->returnArr['style']['a2']) && $strlen >= 4){
			for($i=0;$i<10;$i++){
				if(substr_count($domain,$i) == 4){
					$this->returnArr['style']['a2'] = true;
					$this->returnArr['style']['a3'] = true;
					$this->returnArr['style']['a4'] = true;
					break;
				}
			}
		}
		//3个相同数字
		if(empty($this->returnArr['style']['a3']) && $strlen == 3){
			for($i=0;$i<10;$i++){
				if(substr_count($domain,$i) == 3){
					$this->returnArr['style']['a3'] = true;
					$this->returnArr['style']['a4'] = true;
					break;
				}
			}
		}
		//2个相同数字
		if(empty($this->returnArr['style']['a4']) && $strlen == 2){
			for($i=0;$i<10;$i++){
				if(str_replace($i.$i,'',$domain) != $domain){
					$this->returnArr['style'][] = 'a4';
					break;
				}
			}
		}
		//顺子
		if($strlen >= 3){
			//升序顺子
			$this->returnArr['style']['a5'] = true;
			$lastI = - 1;
			for($i=0;$i<$strlen;$i++){
				if($lastI != -1 && $domain[$lastI]+1 != $domain[$i]){
					$this->returnArr['style']['a5'] = false;
					break;
				}
				$lastI++;
			}
			//升级顺子成立，则升序也成立
			if($this->returnArr['style']['a5'] == true){
				$this->returnArr['style']['a7'] = true;
			}
			//倒序顺子
			if($this->returnArr['style']['a5'] == false){
				$lastI = $strlen;
				for($i=$strlen-1;$i>=0;$i--){
					if($lastI != $strlen && $domain[$lastI] - 1 != $domain[$i]){
						$this->returnArr['style']['a5'] = false;
						break;
					}
					$lastI--;
				}
				//倒序顺子成立，则倒序也成立
				if($this->returnArr['style']['a5'] == true){
					$this->returnArr['style']['a8'] = true;
				}
			}
		}
		//区号
		if(str_replace($this->area_code,'',$domain) != $domain){
			$this->returnArr['style']['a6'] == true;
		}
		//域名长度大于等于2位，则判断升序降序
		if($strlen >= 2 && $this->returnArr['style']['a7'] == false){
			$this->returnArr['style']['a7'] = true;
			$lastI = - 1;
			for($i=0;$i<$strlen;$i++){
				if($lastI != -1 && $domain[$lastI] > $domain[$i]){
					$this->returnArr['style']['a7'] = false;
					break;
				}
				$lastI++;
			}
		}
		//域名长度大于等于2位，则判断降序降序
		if($strlen >= 2 && $this->returnArr['style']['a8'] == false){
			$this->returnArr['style']['a8'] = true;
			$lastI = $strlen;
			for($i=$strlen-1;$i>=0;$i--){
				if($lastI != $strlen && $domain[$lastI] > $domain[$i]){
					$this->returnArr['style']['a8'] = false;
					break;
				}
				$lastI--;
			}
		}
		//不带4
		if(false === strpos($domain,'4')){
			$this->returnArr['style']['a9'] = true;
		}
		//不带0
		if(false === strpos($domain,'0')){
			$this->returnArr['style']['a0'] = true;
		}
	}
	protected function matchWord($domain){
		$strlen = strlen($domain);
		$this->returnArr['styleType'] = 'b';
		if($strlen > 3){
			$this->returnArr['label'] = '多字母';
		}else{
			$this->returnArr['label'] = $strlen.'字母';
		}
		//单拼
		foreach($this->single_pinyin as $key=>$value){
			$single_pinyin[$key] = '/'.$value.'/';
 		}
		foreach($single_pinyin as $value){
			$danpinhou = preg_replace($value,'',$domain,1);
			if($danpinhou != $domain){
				break;
			}
 		}
		if($danpinhou == ''){
			$this->returnArr['style']['b1'] = true;
			$this->returnArr['label'] = '单拼';
			$firstPinyin = $domain;
		}else if($danpinhou != $domain){	//双拼
			$firstPinyin = $danpinhou;
			foreach($single_pinyin as $value){
				$shuangpinhou = preg_replace($value,'',$danpinhou,1);
				if($shuangpinhou != $danpinhou){
					break;
				}
			}
			if($shuangpinhou == ''){
				$this->returnArr['style']['b2'] = true;
				$this->returnArr['label'] = '双拼';
				$twoPinyin = $danpinhou;
				if($firstPinyin == $twoPinyin){
					$this->returnArr['style']['b4'] = true;	//叠拼
				}
			}else if($shuangpinhou != $danpinhou){	//三拼
				foreach($single_pinyin as $value){
					$sanpinhou = preg_replace($value,'',$shuangpinhou,1);
					if($sanpinhou != $shuangpinhou){
						break;
					}
				}
				if($sanpinhou == ''){
					$this->returnArr['label'] = '三拼';
					$this->returnArr['style']['b3'] = true;
				}
			}
		}
		//CVCV
		if($strlen == 4 && in_array($domain[0],$this->shengmu) && in_array($domain[1],$this->yunmu) && in_array($domain[2],$this->shengmu) && in_array($domain[3],$this->yunmu)){
			$this->returnArr['style']['b5'] = true;
		}
	}
	protected function matchMix($domain){
		$strlen = strlen($domain);
		$this->returnArr['styleType'] = 'c';
		if($strlen > 4){
			$this->returnArr['label'] = '多杂';
		}else{
			$this->returnArr['label'] = $strlen.'杂';
		}
		
		//数字在前
		if(preg_match('/^\d$/',$domain[0])){
			$this->returnArr['style']['c1'] = true;
		}
		//字母在前
		if(preg_match('/^[a-z]$/',$domain[0])){
			$this->returnArr['style']['c2'] = true;
		}
		//NNL 
		if(preg_match('/^\d$/',$domain[0]) && preg_match('/^\d$/',$domain[1]) && preg_match('/^[a-z]$/',$domain[2])){
			$this->returnArr['style']['c3'] = true;
		}
		//NLN 
		if(preg_match('/^\d$/',$domain[0]) && preg_match('/^[a-z]$/',$domain[1]) && preg_match('/^\d$/',$domain[2])){
			$this->returnArr['style']['c4'] = true;
		}
		//LNN 
		if(preg_match('/^[a-z]$/',$domain[0]) && preg_match('/^\d$/',$domain[1]) && preg_match('/^\d$/',$domain[2])){
			$this->returnArr['style']['c5'] = true;
		}
		//LLN 
		if(preg_match('/^[a-z]$/',$domain[0]) && preg_match('/^[a-z]$/',$domain[1]) && preg_match('/^\d$/',$domain[2])){
			$this->returnArr['style']['c6'] = true;
		}
		//LNL 
		if(preg_match('/^[a-z]$/',$domain[0]) && preg_match('/^\d$/',$domain[1]) && preg_match('/^[a-z]$/',$domain[2])){
			$this->returnArr['style']['c7'] = true;
		}
		//NLL 
		if(preg_match('/^\d$/',$domain[0]) && preg_match('/^[a-z]$/',$domain[1]) && preg_match('/^[a-z]$/',$domain[2])){
			$this->returnArr['style']['c8'] = true;
		}
	}
}
?>