<?php
class PayAction extends BaseAction{
	public function go_pay(){
		if(empty($this->user_session)){
			$this->error_tips('请先进行登录！',U('Login/index'));
		}
		if(!in_array($_GET['order_type'],array('recharge'))){
			$this->error_tips('订单来源无法识别，请重试。');
		}
		if($_GET['order_type'] == 'recharge'){
			$now_order = D('User_recharge_order')->get_pay_order($this->user_session['uid'],intval($_GET['order_id']),true);
		}else{
			$this->error_tips('非法的订单');
		}
		if($now_order['error'] == 1){
			if($now_order['url']){
				$this->error_tips($now_order['msg'],$now_order['url']);
			}else{
				$this->error_tips($now_order['msg']);
			}
		}
		$order_info = $now_order['order_info'];
		
		//用户信息
		$now_user = D('User')->get_user($this->user_session['uid']);
		if(empty($now_user)){
			$this->error_tips('未获取到您的帐号信息，请重试！');
		}

		//需要支付的钱
		$pay_money = $order_info['order_total_money'];
		$pay_method = D('Config')->get_pay_method($notOnline,$notOffline,true);
		if(empty($pay_method)){
			$this->error_tips('系统管理员没开启任一一种支付方式！');
		}
		if(empty($pay_method[$_GET['pay_type']])){
			$this->error_tips('您选择的支付方式不存在，请更新支付方式！');
		}
		$pay_class_name = ucfirst($_GET['pay_type']);
		$import_result = import('@.ORG.pay.'.$pay_class_name);
		if(empty($import_result)){
			$this->error_tips('系统管理员暂未开启该支付方式，请更换其他的支付方式');
		}
		$pay_class = new $pay_class_name($order_info,$pay_money,$_GET['pay_type'],$pay_method[$_GET['pay_type']]['config'],$this->user_session,0);
		$go_pay_param = $pay_class->pay();
		
		if(empty($go_pay_param['error'])){
			if($pay_class_name == 'Weixin'){
				$this->success($go_pay_param['qrcode']);
				exit;
			}else if(!empty($go_pay_param['url'])){
				$this->assign('url',$go_pay_param['url']);
			}else if(!empty($go_pay_param['form'])){
				$this->assign('form',$go_pay_param['form']);
			}else{
				$this->error_tips('调用支付发生错误，请重试。');
			}
		}else{
			$this->error_tips($go_pay_param['msg']);
		}
		
		$this->display();
	}
	
	//异步通知
	public function notify_url(){
		$pay_method = D('Config')->get_pay_method();
		if(empty($pay_method)){
			$this->error_tips('系统管理员没开启任一一种支付方式！');
		}
		if(empty($pay_method[$_GET['pay_type']])){
			$this->error_tips('您选择的支付方式不存在，请更新支付方式！');
		}
		
		$pay_class_name = ucfirst($_GET['pay_type']);
		$import_result = import('@.ORG.pay.'.$pay_class_name);
		if(empty($import_result)){
			$this->error_tips('系统管理员暂未开启该支付方式，请更换其他的支付方式');
		}
		
		$pay_class = new $pay_class_name('','',$_GET['pay_type'],$pay_method[$_GET['pay_type']]['config'],$this->user_session,0);
		$notify_return = $pay_class->notice_url();
		
		if(empty($notify_return['error'])){

		}else{
			$this->error_tips($notify_return['msg']);
		}
	}
	
	//跳转通知
	public function return_url(){
		$pay_type = $_GET['pay_type'];
		$this->config['merchant_ownpay'] = intval($this->config['merchant_ownpay']);
		switch($this->config['merchant_ownpay']){
			case '0':
				$pay_method = D('Config')->get_pay_method();
				break;
			case '1':
				$pay_method = D('Config')->get_pay_method();
				if($_GET['own_mer_id']){
					$mer_id = $_GET['own_mer_id'];
				}else if($_SESSION['own_mer_id']){
					$mer_id = $_SESSION['own_mer_id'];
					unset($_SESSION['own_mer_id']);
				}
				if($mer_id){
					$merchant_ownpay = D('Merchant_ownpay')->field('mer_id',true)->where(array('mer_id'=>$mer_id))->find();
					foreach($merchant_ownpay as $ownKey=>$ownValue){
						$ownValueArr = unserialize($ownValue);
						if($ownValueArr['open']){
							$ownValueArr['is_own'] = true;
							$pay_method[$ownKey] = array('name'=>$this->getPayName($ownKey),'config'=>$ownValueArr);
						}
					}
				}				
				break;
			case '2':
				$pay_method = array();
				if($_GET['own_mer_id']){
					$mer_id = $_GET['own_mer_id'];
				}else if($_SESSION['own_mer_id']){
					$mer_id = $_SESSION['own_mer_id'];
					unset($_SESSION['own_mer_id']);
				}
				if($mer_id){
					$merchant_ownpay = D('Merchant_ownpay')->field('mer_id',true)->where(array('mer_id'=>$mer_id))->find();
					foreach($merchant_ownpay as $ownKey=>$ownValue){
						$ownValueArr = unserialize($ownValue);
						if($ownValueArr['open']){
							$ownValueArr['is_own'] = true;
							$pay_method[$ownKey] = array('name'=>$this->getPayName($ownKey),'config'=>$ownValueArr);
						}
					}
					unset($pay_method['weixin']);
					if(empty($pay_method)){
						$pay_method = D('Config')->get_pay_method();
					}
				}
				break;			
		}
		if(empty($pay_method)){
			$this->error_tips('系统管理员没开启任一一种支付方式！');
		}
		if(empty($pay_method[$pay_type])){
			$this->error_tips('您选择的支付方式不存在，请更新支付方式！');
		}

		$pay_class_name = ucfirst($pay_type);
		$import_result = import('@.ORG.pay.'.$pay_class_name);
		if(empty($import_result)){
			$this->error_tips('系统管理员暂未开启该支付方式，请更换其他的支付方式');
		}
	
		$pay_class = new $pay_class_name('','',$pay_type,$pay_method[$pay_type]['config'],$this->user_session,0);
		$get_pay_param = $pay_class->return_url();
		if(empty($get_pay_param['error'])){
			if($get_pay_param['order_param']['order_type'] == 'group'){
				$pay_info = D('Group_order')->after_pay($get_pay_param['order_param']);			
			}else if($get_pay_param['order_param']['order_type'] == 'meal' || $get_pay_param['order_param']['order_type'] == 'takeout' || $get_pay_param['order_param']['order_type'] == 'food'){
				$pay_info = D('Meal_order')->after_pay($get_pay_param['order_param']);			
			}else if($get_pay_param['order_param']['order_type'] == 'recharge'){
				$pay_info = D('User_recharge_order')->after_pay($get_pay_param['order_param']);			
			}else if($get_pay_param['order_param']['order_type'] == 'appoint'){
				$pay_info = D('Appoint_order')->after_pay($get_pay_param['order_param']);			
			}else if($get_pay_param['order_param']['order_type'] == 'waimai'){
				$pay_info = D('Waimai_order')->after_pay($get_pay_param['order_param']);			
			}else if($get_pay_param['order_param']['order_type'] == 'waimai-recharge'){
				$pay_info = D('User_recharge_order')->after_pay($get_pay_param['order_param']);			
			}else{
				$this->error_tips('订单类型非法！请重新下单。');
			}
			// if($get_pay_param['order_param']['pay_type'] == 'yeepay' && $pay_info['url']){
				// $pay_info['url'] = str_replace('/source/web_yeepay.php','/index.php',$pay_info['url']);
			// }
			if($pay_info['url']){
				$pay_info['url'] = preg_replace('#/source/(\w+).php#','/index.php',$pay_info['url']);
			} 
			if(empty($pay_info['error'])){
				if($get_pay_param['order_param']['pay_type'] == 'weixin'){
					exit('<xml><return_code><![CDATA[SUCCESS]]></return_code></xml>');
				}
				if(!empty($pay_info['url'])){
					$this->assign('jumpUrl',$pay_info['url']);
					$this->success('订单付款成功！现在跳转.');
					exit();
				}
			}
			if(empty($pay_info['url'])){
				$this->error_tips($pay_info['msg']);
			}else{
				$this->error_tips($pay_info['msg'],$pay_info['url']);
			}
		}else{
			$this->error_tips($get_pay_param['msg']);
		}
	}
	//微信同步回调页面
	public function weixin_back(){
		switch($_GET['order_type']){
			case 'group':
				$now_order = D('Group_order')->get_order_by_id($this->user_session['uid'],intval($_GET['order_id']));
				break;
			case 'meal':
			case 'takeout':
			case 'food':
				$now_order = D('Meal_order')->get_order_by_id($this->user_session['uid'],intval($_GET['order_id']));
				break;
			case 'recharge':
				$now_order = D('User_recharge_order')->get_order_by_id($this->user_session['uid'],intval($_GET['order_id']));
				break;
			case 'task_pay':
				$now_order = D('User_recharge_order')->get_order_by_id($this->user_session['uid'],intval($_GET['order_id']));
				break;
			case 'appoint':
				$now_order = D('Appoint_order')->get_order_by_id($this->user_session['uid'],intval($_GET['order_id']));
				break;
			case 'waimai':
				$now_order = D('Waimai_order')->get_order_by_id($this->user_session['uid'],intval($_GET['order_id']));
			default:
				$this->error_tips('非法的订单');
		}
		$now_order['order_type'] = $_GET['order_type'];
		if(empty($now_order)){
			$this->error_tips('该订单不存在');
		}
		if($now_order['paid']){
			switch($_GET['order_type']){
				case 'group':
					$redirctUrl = C('config.site_url').'/index.php?g=User&c=Index&a=group_order_view&order_id='.$now_order['order_id'];
					break;
				case 'meal':
				case 'takeout':
				case 'food':
					$redirctUrl = C('config.site_url').'/index.php?g=User&c=Index&a=meal_order_view&order_id='.$now_order['order_id'];
					break;
				case 'recharge':
					//$redirctUrl = C('config.site_url').'/index.php?g=User&c=Credit&a=index';
					$redirctUrl = C('config.site_url').'/index.php?g=Domain&c=Account&a=index';
					break;
				case 'task_pay':
					$redirctUrl = C('config.site_url').'/index.php?g=Task&c=Profile&a=index';
					break;
				case 'appoint':
					$redirctUrl = C('config.site_url').'/index.php?g=User&c=Index&a=appoint_order_view&order_id='.$now_order['order_id'];
					break;
				case 'waimai':
					$redirctUrl = C('config.site_url').'/index.php?g=Waimai&c=Order&a=detail&order_id='.$now_order['order_id'];
					break;
			}
			redirect($redirctUrl);exit;
		}
		$import_result = import('@.ORG.pay.Weixin');
		$pay_method = D('Config')->get_pay_method();
		if(empty($pay_method)){
			$this->error_tips('系统管理员没开启任一一种支付方式！');
		}
		$pay_class = new Weixin($now_order,0,'weixin',$pay_method['weixin']['config'],$this->user_session,1);
		$go_query_param = $pay_class->query_order();
		if($go_query_param['error'] === 0){
			switch($_GET['order_type']){
				case 'group':
					D('Group_order')->after_pay($go_query_param['order_param']);
					break;
				case 'meal':
				case 'takeout':
				case 'food':
					D('Meal_order')->after_pay($go_query_param['order_param']);
					break;
				case 'recharge':
					D('User_recharge_order')->after_pay($go_query_param['order_param']);
					break;
				case 'appoint':
					D('Appoint_order')->after_pay($go_query_param['order_param']);
					break;
				case 'waimai':
					D('Appoint_order')->after_pay($go_query_param['order_param']);
					break;
			}
		}
		switch($_GET['order_type']){
			case 'group':
				$redirctUrl = C('config.site_url').'/index.php?g=User&c=Index&a=group_order_view&order_id='.$now_order['order_id'];
				break;
			case 'meal':
				$redirctUrl = C('config.site_url').'/index.php?g=User&c=Index&a=meal_order_view&order_id='.$now_order['order_id'];
				break;
			case 'recharge':
				$redirctUrl = C('config.site_url').'/index.php?g=Domain&c=Account&a=index';
				break;
			case 'appoint':
				$redirctUrl = C('config.site_url').'/index.php?g=User&c=Index&a=appoint_order_view&order_id='.$now_order['order_id'];
				break;
			case 'waimai':
				$redirctUrl = C('config.site_url').'/index.php?g=Waimai&c=Order&a=detail&order_id='.$now_order['order_id'];
				break;
		}
		redirect($redirctUrl);
	}
	//支付宝支付同步回调
	public function alipay_return(){
		$order_id_arr = explode('_',$_GET['out_trade_no']);				
		$order_type = $order_id_arr[0];
		$order_id = $order_id_arr[1];
		switch($order_type){
			case 'group':
				$now_order = D('Group_order')->where(array('order_id'=>$order_id))->find();
				break;
			case 'meal':
			case 'takeout':
			case 'food':
				$now_order = D('Meal_order')->where(array('order_id'=>$order_id))->find();
				break;
			case 'recharge':
				$now_order = D('User_recharge_order')->where(array('order_id'=>$order_id))->find();
				break;
			case 'appoint':
				$now_order = D('Appoint_order')->where(array('order_id'=>$order_id))->find();
				break;
			case 'waimai':
				$now_order = D('Waimai_order')->where(array('order_id'=>$order_id))->find();
				break;
			default:
				$this->error('非法的订单');
		}
		if($now_order['paid']){
			switch($order_type){
				case 'group':
					$redirctUrl = C('config.site_url').'/index.php?g=User&c=Index&a=group_order_view&order_id='.$now_order['order_id'];
					break;
				case 'meal':
				case 'takeout':
				case 'food':
					$redirctUrl = C('config.site_url').'/index.php?g=User&c=Index&a=meal_order_view&order_id='.$now_order['order_id'];
					break;
				case 'recharge':
					$redirctUrl = C('config.site_url').'/index.php?g=Domain&c=Account&a=index';
					break;
				case 'appoint':
					$redirctUrl = C('config.site_url').'/index.php?g=User&c=Index&a=appoint_order_view&order_id='.$now_order['order_id'];
					break;
				case 'waimai':
					$redirctUrl = C('config.site_url').'/index.php?g=Waimai&c=Order&a=detail&order_id='.$now_order['order_id'];
					break;
			}
			redirect($redirctUrl);exit;
		}
		$pay_method = D('Config')->get_pay_method();
		if(empty($pay_method)){
			$this->error_tips('系统管理员没开启任一一种支付方式！');
		}
		$import_result = import('@.ORG.pay.Alipay');
		$pay_class = new Alipay('','',$pay_type,$pay_method['alipay']['config'],$this->user_session,0);
		$go_query_param = $pay_class->query_order();
		if($go_query_param['error'] === 0){
			switch($order_type){
				case 'group':
					D('Group_order')->after_pay($go_query_param['order_param']);
					break;
				case 'meal':
				case 'takeout':
				case 'food':
					D('Meal_order')->after_pay($go_query_param['order_param']);
					break;
				case 'recharge':
					D('User_recharge_order')->after_pay($go_query_param['order_param']);
					break;
				case 'appoint':
					D('Appoint_order')->after_pay($go_query_param['order_param']);
					break;
				case 'waimai':
					D('Waimai_order')->after_pay($go_query_param['order_param']);
					break;
			}
		}
		switch($order_type){
			case 'group':
				$redirctUrl = C('config.site_url').'/index.php?g=User&c=Index&a=group_order_view&order_id='.$now_order['order_id'];
				break;
			case 'meal':
			case 'takeout':
			case 'food':
				$redirctUrl = C('config.site_url').'/index.php?g=User&c=Index&a=meal_order_view&order_id='.$now_order['order_id'];
				break;
			case 'recharge':
				//$redirctUrl = C('config.site_url').'/index.php?g=User&c=Credit&a=index&order_id='.$now_order['order_id'];
				$redirctUrl = C('config.site_url').'/index.php?g=Domain&c=Account&a=index';
				break;
			case 'appoint':
				$redirctUrl = C('config.site_url').'/index.php?g=User&c=Index&a=appoint_order_view&order_id='.$now_order['order_id'];
				break;
			case 'waimai':
				$redirctUrl = C('config.site_url').'/index.php?g=Waimai&c=Order&a=detail&order_id='.$now_order['order_id'];
				break;
		}
		redirect($redirctUrl);
	}
}
?>