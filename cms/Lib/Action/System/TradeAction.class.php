<?php

/**
 * 域名 交易记录
 * 包含各个交易类型的交易记录
 * 一元起拍 - 竞价 - 一口价 - 询价 - 易拍易卖 - 专题拍卖
 */

class TradeAction extends BaseAction{

	//域名代购列表
	public function index(){
		$Db_order = M('Order');
		$condition = array();
		//搜索条件组合
		if (!empty($_GET['keyword'])) {
			if ($_GET['searchtype'] == 'order_id') {
				$condition['order_id'] = trim($_GET['keyword']);
			} else if ($_GET['searchtype'] == 'domainName') {
				$condition['domainName'] = array('like', '%' . $_GET['keyword'] . '%');
			} else if ($_GET['searchtype'] == 'staff_id') {
				$condition['staff_id'] = intval($_GET['keyword']) ? intval($_GET['keyword']) : 0;
			}
		}


		$order_list_count = $Db_order->where($condition)->count();
		import('@.ORG.system_page');
		$p = new Page($order_list_count, 15);
		$order_list = $Db_order->field(true)->order('`addTime` DESC,`order_id` ASC')->where($condition)->limit($p->firstRow . ',' .
			$p->listRows)->select();

		$this->assign('order_list',$order_list);
		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);
		$this->display();
	}

	//订单编辑
	public function edit(){
		$condition['order_id'] = intval($_GET['order_id']);
		$Db_order = M('Order');
		$Db_order_info  = M('Order_info');
		$now_order = $Db_order->field(true)->where($condition)->find();
		$order_info =$Db_order_info->field('`id`,`info`')->where("`order_id` = ".$condition['order_id'] ." AND `operator` != ''")->order('`id` DESC')
			->find();
		//查最新一条日志

		if(empty($now_order)){
			$this->frame_error_tips('数据库中没有查询到该内容！',3);
		}

		$this->assign('now_order',$now_order);
		$this->assign('order_info',$order_info);
		$this->display();
	}

	//订单保存编辑
	public function edit_save(){
		$Db_order       = M('Order');
		$Db_order_info  = M('Order_info');
		$order_info['info']    = fulltext_filter($_POST['info']);
		$order_info['time']    = time();
		$order_info['operator']    = $_SESSION['system']['account'];
		$order_info['order_id']    = $_POST['order_id'];

		$oder_data['yes_no'] = intval($_POST['yes_no']);
		$oder_data['order_id']     = $order_info['order_id'];
		if($oder_data['yes_no'] === 7){ //关闭状态
			$oder_data['up_time'] = time();
			$oder_data['status'] = 3;
		}

		if(empty($oder_data['yes_no'])){
			$this->frame_submit_tips(0,'编辑失败！');exit;
		}
		if($Db_order->data($oder_data)->save()){
			$Db_order_info->data($order_info)->add();
			$this->frame_submit_tips(1,'编辑成功！');
		}else{

			$this->frame_submit_tips(0,'编辑失败！');
		}
	}


	//订单后台操作日志记录列表
	public function operalog(){
		$condition['order_id'] = intval($_GET['order_id']);
		$Db_order_info  = M('Order_info');
		$count_user_info = $Db_order_info->where($condition)->count();

		import('@.ORG.system_page');
		$p = new Page($count_user_info, 10);
		$order_info_list = $Db_order_info->field(true)->where($condition)->order('`time` DESC,`id` ASC')->limit($p->firstRow . ',' .
			$p->listRows)->select();

		$this->assign('order_info_list', $order_info_list);
		$this->display();
	}

	public function del(){
		$order_id = isset($_POST['order_id']) ? intval($_POST['order_id']) : 0;
		if($order_id>0){
			$Db_order       = M('Order');
			if($Db_order->where(array('order_id'=>$order_id))->delete()){
				$this->success('删除成功！');
				exit();
			}else{
				$this->error('删除失败！请重试~');
				exit();
			}
		}
		$this->error('操作失败！请重试~');

	}

	//域名代购列表
	public function procurement(){
		$Db_purchase = M('Purchase');
		import('@.ORG.system_page');
		$condition = array();
		$contthis = $Db_purchase->where($condition)->count();
		$p = new Page($contthis , 15);
		$purchase = $Db_purchase->field(true)->where($condition)->order('`add_time` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
		//dump($purchase);
		$this->assign('purchase',$purchase);
		$this->display();
	}

	public function procurement_edit(){
		$condition['ph_id'] = intval($_GET['ph_id']);
		$Db_purchase = M('Purchase');
		$now_order = $Db_purchase->field(true)->where($condition)->find();
		if(empty($now_order)){
			$this->frame_error_tips('数据库中没有查询到该内容！',3);
		}
		$this->assign('now_order',$now_order);
		$this->display();
	}


	public function procurement_edit_save(){
		$Db_purchase = M('Purchase');

		$oder_data['ph_id'] = intval($_POST['ph_id']);
		$oder_data['is_show'] = intval($_POST['is_show']);

		if(empty($oder_data['ph_id'])){
			$this->frame_submit_tips(0,'编辑失败！');exit;
		}
		if($Db_purchase->data($oder_data)->save()){

			$this->frame_submit_tips(1,'编辑成功！');
		}else{

			$this->frame_submit_tips(0,'编辑失败！');
		}

	}
	//域名代购设置
	public function procure_site(){
		$db_config = D('Config');
		$conditioin_name['name'] = 'procure_set';
		if(IS_POST){
			$data['value'] = trim(trim($_POST['value'],'|'));
			$data['type']  = intval($_POST['type']);
			if($db_config->where($conditioin_name)->save($data)){
				$this->success('设置成功！');exit;
			}else{
				$this->error('非法提交,请重新提交~');exit;
			}
		}
		$get_config = $db_config->field('`name`,`type`,`value`')->where($conditioin_name)->find();
		$this->assign('config_data',$get_config);


		$this->display();
	}

	public function lottrading(){
		$bulk_id = intval($_GET['domain_id']);
		$Db_bulk_trading = M('Bulk_trading');
		$lottrad = $Db_bulk_trading->field('`domain_list`')->where(' `bulk_id` = '. $bulk_id)->find();
		$lottrad['domain_list'] = unserialize($lottrad['domain_list']);
		$this->assign('lottrad',$lottrad);
		$this->display();

	}
}