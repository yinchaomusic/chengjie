<?php

/**
 * Class FinancAction
 */

class FinancAction extends BaseAction{

	public function index(){
		$this->display();
	}

	public function id_card(){

		$condition = array();
		//搜索条件组合
		if (!empty($_GET['keyword'])) {
			if ($_GET['searchtype'] == 'IDNUMBER') {
				$condition['id_number'] = array('like', '%' . $_GET['keyword'] . '%');
			}
			if ($_GET['searchtype'] == 'UID') {
				$condition['uid'] = array('like', '%' . $_GET['keyword'] . '%');
			}
		}
		$Db_id_card = D('Id_card');

		//$condition['is_check'] = 1;
		$order_list_count = $Db_id_card->where($condition)->count();
		import('@.ORG.system_page');
		$p = new Page($order_list_count, 15);
		$order_list = $Db_id_card->field(true)->order('`pull_time` DESC')->where($condition)->limit($p->firstRow . ',' .$p->listRows)->select();
		//获取真实姓名
		if($order_list){
			$Db_user = D('User');
			foreach($order_list as &$val){
				$val['nickname'] = $Db_user->where('`uid` = '.$val['uid'])->getField('`nickname`');
			}
		}
		$this->assign('order_list',$order_list);
		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);

		$this->display();
	}

	public function id_card_edit(){
		$condition['card_id'] = intval($_GET['card_id']);
		$Db_id_card = D('Id_card');
		$now_order = $Db_id_card->field(true)->where($condition)->find();
		if(empty($now_order)){
			$this->frame_error_tips('数据库中没有查询到该内容！',3);
		}
		$Db_user = D('User');
		$now_order['nickname'] = $Db_user->where('`uid` = '.$now_order['uid'])->getField('`nickname`');
		$this->assign('now_order',$now_order);

		$this->display();
	}

	public function id_card_edit_save(){
		$Db_id_card = D('Id_card');
		$oder_data['card_id'] = intval($_POST['card_id']);
		$oder_data['status'] = intval($_POST['status']);
		$oder_data['update_time'] = time();
		if($Db_id_card->data($oder_data)->save()){
			$this->frame_submit_tips(1,'审核通过！');
		}else{
			$this->frame_submit_tips(0,'编辑失败！');
		}
	}

	public function del(){
		 if(IS_POST){
			 $type = trim($_POST['type']);
			 $id  = intval($_POST['id']);
			 switch($type){
				 case 'idcard':
					 $Mode = D('Id_card');
					 $condition_del['card_id'] = $id;
					 $ok_msg = "删除成功";
					 $error_msg = "删除失败，请重试";
					 break;
			 }
			 if($Mode->where($condition_del)->delete()){
				 //unlink('./upload/idcard/'.$now_adver['facadeFile']);
				 //unlink('./upload/idcard/'.$now_adver['reverseSideFile']);
				 //S('adver_list_'.$now_adver['cat_id'],NULL);
				 $this->success($ok_msg);
			 }else{
				 $this->error($error_msg);
			 }
		 }
	}
}