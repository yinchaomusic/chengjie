<?php

class FinancialAction extends BaseAction{
    public function cash(){
        $user_deal_record = M('user_deal_record');
        //搜索
        if (!empty($_GET['keyword'])) {
            if ($_GET['searchtype'] == 'uid') {
                $condition['uid'] = intval($_GET['keyword']);
                $search_where  =" AND ud.`uid`=".$condition['uid'];
            }
        }
        $count = $user_deal_record->where($condition)->count();
        import('@.ORG.system_page');
        $p = new Page($count, 15);
        $user_deal_record_list = M()->table(array( C('DB_PREFIX').'user_deal_record'=>'ud',C('DB_PREFIX').'bank_card'=>'bc'))
                            ->field('ud.txtmoeny,ud.id,ud.uid,ud.info,ud.operatetime,ud.addtime,ud.state,ud.bank_card_id,bc.bankName,bc.account')
                            ->where('ud.bank_card_id = bc.id ' . $search_where )->limit($p->firstRow . ',' . $p->listRows)->order('ud.id desc' )
            ->select();
       // echo M()->getLastSql();
        $this->assign('user_deal_record_list', $user_deal_record_list);
        $pagebar = $p->show();
        $this->assign('pagebar', $pagebar);
        $this->display();
            
    }
    
    public function show(){
        $databases_bank_card = M('bank_card');
        $where['id']  = intval($_GET['id']);
        $bc_show = $databases_bank_card->where($where)->find();
        $this->assign('bc_show',$bc_show);
        $this->display();
    }

    //提现修改
    public function state_changes(){
        $Db_user_deal_record = M('user_deal_record');
        $condition['id'] = intval($_GET['id']);
        $now_user_record = $Db_user_deal_record->field(true)->where($condition)->find();
        if(empty($now_user_record)){
            $this->frame_error_tips('数据库中没有查询到该内容！',3);
        }
        $Db_bank_card =  M('Bank_card');
        $bank_condition['id'] = $now_user_record['bank_card_id'];
        $now_bank_card = $Db_bank_card->field(true)->where($bank_condition)->find();
        $this->assign('now_bank_card',$now_bank_card);
        $this->assign('now_user_record',$now_user_record);
        $this->display();

    }

    public function state_changes_save(){
        $Db_user_deal_record = M('user_deal_record');
        $user_record['info']    = fulltext_filter($_POST['info']);
        $user_record['operatetime']= time();
        $user_record['state']  = intval($_POST['state']);
        if(intval($_POST['state']) == 2){ //提现失败，应该退回提现的钱
            D('User')->where('`uid`='.intval($_POST['uid']))->setInc('now_money',$_POST['txtmoeny']);
        }
        $user_record['id']      = intval($_POST['id']);
        $user_record['uid']     = intval($_POST['uid']);
        $user_record['decMenory']     = $_POST['decMenory'];
        if($Db_user_deal_record->data($user_record)->save()){
            $this->frame_submit_tips(1,'编辑成功！');
        }else{

            $this->frame_submit_tips(0,'编辑失败！');
        }
    }

    //资金冻结列表
    public function frozen(){
        $Db_user_freeze = M('user_freeze');
        //搜索
        if (!empty($_GET['keyword'])) {
            if ($_GET['searchtype'] == 'uid') {
                $condition['uid'] = intval($_GET['keyword']);
            }
        }
        $count = $Db_user_freeze->where($condition)->count();
        import('@.ORG.system_page');
        $p = new Page($count, 15);
        $user_freeze_list = $Db_user_freeze->field(true)->where($condition)->order('`freezetime` DESC,`id` ASC')->limit($p->firstRow . ',' .
            $p->listRows)->select();
        $this->assign('user_freeze_list', $user_freeze_list);
        $pagebar = $p->show();
        $this->assign('pagebar', $pagebar);
        $this->display();
    }

    public function frozen_edit(){
        $condition['id'] = intval($_GET['id']);
        $Db_user_freeze = M('user_freeze');
        $now_user_freeze = $Db_user_freeze->field(true)->where($condition)->find();
        if(empty($now_user_freeze)){
            $this->frame_error_tips('数据库中没有查询到该内容！',3);
        }

        $this->assign('now_user_freeze',$now_user_freeze);
        $this->display();
    }

    public function frozen_edit_save(){
        $Db_user_freeze = M('user_freeze');
        $user_freeze['info']    = fulltext_filter($_POST['info']);
        $user_freeze['thawtime']= time();
        $user_freeze['status']  = intval($_POST['status']);
        $user_freeze['id']      = $_POST['id'];
        $user_freeze['uid']     = $_POST['uid'];
        //解冻的时候要把资金加回

        if($Db_user_freeze->data($user_freeze)->save()){
            $this->frame_submit_tips(1,'编辑成功！');
        }else{

            $this->frame_submit_tips(0,'编辑失败！');
        }
    }

    public function moneylist (){

        $Db_user_money_list = D('User_money_list');

        //搜索
        if (!empty($_GET['keyword'])) {
            if ($_GET['searchtype'] == 'uid') {
                $condition['uid'] = intval($_GET['keyword']);

            }
        }

        $count = $Db_user_money_list->where($condition)->count();
        import('@.ORG.system_page');
        $p = new Page($count, 15);
        $list = $Db_user_money_list->field(true)->where($condition)->order('`time` DESC')->limit($p->firstRow . ',' .
            $p->listRows)->select();

        $pagebar = $p->show();
        $this->assign('pagebar', $pagebar);
        $Db_user = D("User");
        foreach($list as $k=>$val){
            $list[$k]['username'] =$Db_user->where(array('uid'=>$val['uid']))->getField('nickname');
        }


        $this->assign('datalist',$list);
        $this->display();
    }

    public function moneylist_edit(){
        $condition['pigcms_id'] = intval($_GET['id']);
        $Db_user_money_list = D('User_money_list');
        $now_user_freeze = $Db_user_money_list->field(true)->where($condition)->find();
        if(empty($now_user_freeze)){
            $this->frame_error_tips('数据库中没有查询到该内容！',3);
        }

        $this->assign('now_user_freeze',$now_user_freeze);
        $this->display();
    }

    public function moneylist_save(){

    }
    
}