<?php
/*
 * 底部导航
 *
 */

class FooterAction extends BaseAction{
	public function index(){
		//$keyword = isset($_GET['keyword'])? trim($_GET['keyword']): '';
		//$fstatus = isset($_GET['fstatus'])? trim($_GET['fstatus']): 1;

		//$condition = array();
		//$condition = "`fstatus`=1";
//		if ($keyword) {
//			$condition .= " AND `name like` %$keyword%";
//		}
		$count = D('Footer_link')->count();
		import('@.ORG.system_page');

		$p = new Page($count, 20);
		$list = D('Footer_link')->field(true)->order('`fsort` DESC,cat_id DESC')->limit($p->firstRow.','.$p->listRows)->select();

		$pagebar = $p->show();
		$this->assign('link_list',$list);
		//$this->assign('keyWord', $keyWord);
		//$this->assign('fid', $fid);
		$this->assign('pagebar',$pagebar);
		$this->display();

	}
	public function add(){
		$this->assign('bg_color','#F3F3F3');
		//var_dump($_POST);die;
		
		$this->display();
	}
	public function uploadFile(){
		$rand_num = date('Y/m',$_SERVER['REQUEST_TIME']);
		$upload_dir = './upload/domain/'.$rand_num.'/';
		if(!is_dir($upload_dir)){
			mkdir($upload_dir,0777,true);
		}
		import('ORG.Net.UploadFile');
		$upload = new UploadFile();
		$upload->maxSize = 10*1024*1024;
		$upload->allowExts = array('jpg','jpeg','png','gif');
		$upload->allowTypes = array('image/png','image/jpg','image/jpeg','image/gif');
		$upload->savePath = $upload_dir;
		$upload->saveRule = 'uniqid';
		if($upload->upload()){
			$uploadList = $upload->getUploadFileInfo();

			return $rand_num.'/'.$uploadList[0]['savename'];
		}else{
			$this->frame_submit_tips(0,$upload->getErrorMsg());
		}
	}

	public function modify(){
		if (IS_POST) {
			$category = array();
			if ($_FILES) {
				$ficon = $this->uploadFile();
				$category['ficon'] = $ficon;
			}
			//dump($category);die;
			$category['name']    = trim($_POST['name']);
			$category['fkey']    = trim($_POST['fkey']);
			$category['url']     = htmlspecialchars_decode($_POST['url']);
			//$category['content'] = fulltext_filter($_POST['content']);
			$category['fsort']   = trim(intval($_POST['fsort']));
			$category['fstatus'] = trim(intval($_POST['fstatus']));
			//$category['fid']     = empty($_POST['fid'])? 0: intval($_POST['fid']);

			$result = D("Footer_link")->add($category);
			if (false === $result) {
				$this->frame_submit_tips(0,'添加失败！');
			}else{
				$this->frame_submit_tips(1,'添加成功！');
			}
		}

	}
	public function edit(){
		$database_footer_link = D('Footer_link');
		$condition_footer_link['id'] = $_GET['id'];
		$now_link = $database_footer_link->field(true)->where($condition_footer_link)->find();
		//dump($database_footer_link);
		if(empty($now_link)){
			$this->frame_error_tips('数据库中没有查询到该内容！',5);
		}

		$this->assign('now_link',$now_link);
		
		$this->assign('bg_color','#F3F3F3');
		$this->display();




	}

	//查看分类
	public  function category(){
		$now_category = $this->check_get_category($_GET['cat_id']);
		$this->assign('now_category',$now_category);

		$database_footer_cates = D('Footer_cates');
		$condition_footer['cat_id'] = $now_category['cat_id'];
		$adver_list = $database_footer_cates->field(true)->where($condition_footer)->order('`id` DESC')->select();
		$this->assign('adver_list',$adver_list);

		$this->display();
	}

	//检查分类是否存在
	protected function check_get_category($cat_id){
		$now_category = $this->get_category($cat_id);
		if(empty($now_category)){
			$this->error_tips('分类不存在！');
		}else{
			return $now_category;
		}
	}

	protected function get_category($cat_id){
		$database_footer_category  = D('Footer_link');
		$condition_footer_category['cat_id'] = $cat_id;
		$now_category = $database_footer_category->field(true)->where($condition_footer_category)->find();
		return $now_category;
	}


	//编辑子分类
	public function amend(){
		$database_footer_link = D('Footer_cates');
		$_POST['content'] = fulltext_filter($_POST['content']);
		$_POST['url']     = htmlspecialchars_decode($_POST['url']);
		//$_POST['pic'] = $_SERVER['REQUEST_TIME'];
		//dump($_POST);die;
		if($database_footer_link->data($_POST)->save()){

			$this->frame_submit_tips(1,'编辑成功！');
		}else{

			$this->frame_submit_tips(0,'编辑失败！');
		}
	}

	//添加子分类
	public function cat_add(){
		$this->assign('bg_color','#F3F3F3');
		$now_category = $this->frame_check_get_category($_GET['cat_id']);
		$this->assign('now_category',$now_category);
		$this->display();
	}


	protected function frame_check_get_category($cat_id){
		$now_category = $this->get_category($cat_id);
		if(empty($now_category)){
			$this->frame_error_tips('分类不存在！');
		}else{
			return $now_category;
		}
	}

	public function news_del(){
		if(IS_POST){
			$database_merchant_news = D('Merchant_news');
			$condition_merchant_news['id'] = $_POST['id'];
			if($database_merchant_news->where($condition_merchant_news)->delete()){
				$this->success('删除成功！');
			}else{
				$this->error('删除失败！请重试~');
			}
		}else{
			$this->error('非法提交,请重新提交~');
		}
	}
	
	public function order(){
		
		$percent = 0;
		$mer_id = isset($_GET['mer_id']) ? intval($_GET['mer_id']) : 0;
		$merchant = D('Merchant')->field(true)->where('mer_id=' . $mer_id)->find();
		if ($merchant['percent']) {
			$percent = $merchant['percent'];
		} elseif ($this->config['platform_get_merchant_percent']) {
			$percent = $this->config['platform_get_merchant_percent'];
		}
		$this->assign('percent', $percent);

		$result = D("Meal_order")->get_order_by_mer_id($mer_id, 1);
		$this->assign($result);
		$this->assign('total_percent', $result['total'] * $percent * 0.01);
		$this->assign('all_total_percent', ($result['alltotal']+$result['alltotalfinsh']) * $percent * 0.01);
		
// 		$this->assign(D("Meal_order")->get_order_by_mer_id($mer_id, 1));
		$this->assign('now_merchant', $merchant);
		$this->assign('mer_id', $mer_id);
		$this->display();
	}
	
	public function change(){
		$mer_id = isset($_GET['mer_id']) ? intval($_GET['mer_id']) : 0;
		$strids = isset($_GET['strids']) ? htmlspecialchars($_GET['strids']) : '';
		if ($strids) {
			$array = explode(',', $strids);
			$mealids = $groupids = array();
			foreach ($array as $val) {
				$t = explode('_', $val);
				if ($t[0] == 1) {
					$mealids[] = $t[1];
				} else {
					$groupids[] = $t[1];
				}
			}
			$mealids && D('Meal_order')->where(array('mer_id' => $mer_id, 'order_id' => array('in', $mealids)))->save(array('is_pay_bill' => 1));
			$groupids && D('Group_order')->where(array('mer_id' => $mer_id, 'order_id' => array('in', $groupids)))->save(array('is_pay_bill' => 1));
		}
		exit(json_encode(array('error_code' => 0)));
	}
	
	public function menu()
	{
		$this->assign('bg_color','#F3F3F3');
		
		$database_merchant = D('Merchant');
		$condition_merchant['mer_id'] = intval($_GET['mer_id']);
		$merchant = $database_merchant->field(true)->where($condition_merchant)->find();
		if(empty($merchant)){
			$this->frame_error_tips('数据库中没有查询到该商户的信息！');
		}
		$merchant['menus'] = explode(',', $merchant['menus']);
		$this->assign('merchant',$merchant);
		
		$menus = D('Merchant_menu')->where(array('status' => 1, 'show' => 1))->select();
		
		$list = array();
		
		foreach ($menus as $menu) {
			if (empty($menu['fid'])) {
				if (isset($list[$menu['id']])) {
					$list[$menu['id']] = array_merge($list[$menu['id']], $menu);
				} else {
					$list[$menu['id']] = $menu;
				}
			} else {
				if (isset($list[$menu['fid']])) {
					$list[$menu['fid']]['lists'][] = $menu;
				} else {
					$list[$menu['fid']]['lists'] = array($menu);
				}
			}
		}
		$this->assign('menus', $list);
		$this->display();
	}

	//编辑子分类
	public function cat_edit(){
		//$this->assign('bg_color','#F3F3F3');

		$database_adver = D('Footer_cates');
		$condition_adver['id'] = $_GET['id'];
		$now_adver = $database_adver->field(true)->where($condition_adver)->find();
		if(empty($now_adver)){
			$this->frame_error_tips('该子分类不存在！');
		}
		$this->assign('now_adver',$now_adver);

		$now_category = $this->frame_check_get_category($now_adver['cat_id']);
		$this->assign('now_category',$now_category);

		$this->display();
	}

	public function footer_modify(){

//		$image = D('Image')->handle($this->system_session['id'], 'adver', 0, array('size' => 10), false);
//		if (!$image['error']) {
//			$_POST = array_merge($_POST, str_replace('/upload/adver/', '', $image['url']));
//		} else {
//			$this->frame_submit_tips(0, $image['msg']);
//		}
//		$_POST['last_time'] = $_SERVER['REQUEST_TIME'];
		if ($_FILES) {
			$ficon = $this->uploadFile();
			$_POST['pic'] = $ficon;
		}
		$_POST['url'] = htmlspecialchars_decode($_POST['url']);
		$database_Footer_cates = D('Footer_cates');
		//dump($_POST);die;
		if($id = $database_Footer_cates->data($_POST)->add()){
			//D('Image')->update_table_id('/upload/addver/' . $_POST['pic'], $id, 'adver');
			S('domain_list_'.$_POST['cat_id'],NULL);
			$this->frame_submit_tips(1,'添加成功！');
		}else{
			$this->frame_submit_tips(0,'添加失败！请重试~');
		}
		//$this->display();
	}
	
	public function savemenu()
	{
		if (IS_POST) {
			$mer_id = isset($_POST['mer_id']) ? intval($_POST['mer_id']) : 0;
			$menus = isset($_POST['menus']) ? $_POST['menus'] : '';
			$menus = implode(',', $menus);
			$database_merchant = D('Merchant');
			$database_merchant->where(array('mer_id' => $mer_id))->save(array('menus' => $menus));
			$this->success('权限设置成功！');
		} else {
			$this->error('非法提交,请重新提交~');
		}
	}

   /*******删除*********/
  public function del(){
		$id = isset($_POST['cat_id']) ? intval($_POST['cat_id']) : 0;

		if($id>0){
		 $footer_linkDb = D('Footer_link');
		 if($footer_linkDb->where(array('cat_id'=>$id))->delete()){
		    $this->success('删除成功！');
			exit();
		 }else{
		    $this->error('删除失败！请重试~');
			exit();
		 }
		}
		 $this->error('操作失败！~');

	}

	public function cate_del(){
		$id = isset($_POST['id']) ? intval($_POST['id']) : 0;

		if($id>0){
			$footer_linkDb = D('Footer_cates');
			if($footer_linkDb->where(array('id'=>$id))->delete()){
				$this->success('删除成功！');
				exit();
			}else{
				$this->error('删除失败！请重试~');
				exit();
			}
		}
		$this->error('操作失败！~');
	}
}