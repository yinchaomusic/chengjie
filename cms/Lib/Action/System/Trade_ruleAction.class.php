<?php
/*
 * 域名交易 -
 * 交易规则
 *
 * @  Writers    weslee
 * @  BuildTime  2015年12月10日 11:51:16
 *
 */
class Trade_ruleAction extends BaseAction
{
	public function index(){
		//先保证域名交易类型已经添加
		$database_Domain_trademodel = D('Domain_trademodel');
		$count_Domain_trademodel = $database_Domain_trademodel->count();
		if(empty($count_Domain_trademodel)){
				$this->error_tips('至少要有一个交易类型可选择！马上跳转到【交易类型】请稍候 ',3,U('Trade_mode/index'));
		}

		$database_Domain_traderule = D('Domain_traderule');
		$count_Domain_traderule = $database_Domain_traderule->count();
		import('@.ORG.system_page');
		$p = new Page($count_Domain_traderule, 30);
		$traderule_list = $database_Domain_traderule->field(true)->order('`sorts` DESC,`rid` ASC')->limit($p->firstRow . ',' . $p->listRows)
			->select();

		foreach($traderule_list as $k=>$val){
			$traderule_list[$k]['models'] = $database_Domain_trademodel->field('id,name')->where("id={$val['tm_id']}")->select();
		}
		$this->assign('traderule_list',$traderule_list);
		//dump($traderule_list);

		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);

		$this->display();
	}


	//协议规则添加
	public function add(){
		//查找已经创建的交易类型
		$database_Domain_trademodel = D('Domain_trademodel');
		$list_trademodel = $database_Domain_trademodel->field('`id`,`name`')->select();
		$this->assign('list_trademodel',$list_trademodel);

		$this->display();
	}

	//协议规则入库
	public function add_save(){
		$database_Domain_traderule = D('Domain_traderule');
		$_POST['content']    = fulltext_filter($_POST['content']);
		$_POST['url']        = htmlspecialchars_decode($_POST['url']);
		$_POST['add_time']   = time();
		$_POST['update_time'] = time();
		//dump($_POST);die;

		if($database_Domain_traderule->data($_POST)->add()){
			$this->frame_submit_tips(1,'添加成功！');
		}else{

			$this->frame_submit_tips(0,'添加失败！');
		}
	}

	//协议规则编辑
	public function edit(){
		$database_Domain_traderule = D('Domain_traderule');
		$condition_traderule['rid'] = intval($_GET['rid']);
		$now_traderule = $database_Domain_traderule->field(true)->where($condition_traderule)->find();
		if(empty($now_traderule)){
			$this->frame_error_tips('数据库中没有查询到该内容！',3);
		}
		$database_Domain_trademodel = D('Domain_trademodel');
		$list_trademodel  = $database_Domain_trademodel->field('`id`,`name`')->select();
		$this->assign('list_trademodel',$list_trademodel);
		$this->assign('now_traderule',$now_traderule);
		$this->display();
	}

	//协议规则保存编辑
	public function edit_save(){
		$database_Domain_traderule = D('Domain_traderule');
		$_POST['content']    = fulltext_filter($_POST['content']);
		$_POST['url']        = htmlspecialchars_decode($_POST['url']);
		$_POST['update_time'] = time();
		if($database_Domain_traderule->data($_POST)->save()){
			$this->frame_submit_tips(1,'编辑成功！');
		}else{

			$this->frame_submit_tips(0,'编辑失败！');
		}
	}

	//协议规则删除
	public function del(){
		$rid = isset($_POST['rid']) ? intval($_POST['rid']) : 0;
		if($rid>0){
			$database_Domain_traderule = D('Domain_traderule');
			if($database_Domain_traderule->where(array('rid'=>$rid))->delete()){
				$this->success('删除成功！');
				exit();
			}else{
				$this->error('删除失败！请重试~');
				exit();
			}
		}
		$this->error('操作失败！请重试~');
	}

}