<?php
/*
 * 珍品域名
 *
 * @  Writers    weslee
 * @  BuildTime  2015年12月10日 11:51:16
 *
 */
class TreasureAction extends BaseAction
{
	public function index(){
		$database_Domain_treasure = D('Domain_treasure');
		$count_Domain_treasure = $database_Domain_treasure->count();
		import('@.ORG.system_page');
		$p = new Page($count_Domain_treasure, 30);
		$treasure_list = $database_Domain_treasure->field(true)->order('`sorts` DESC,`id` ASC')->limit($p->firstRow . ',' . $p->listRows)->select();

		$this->assign('treasure_list',$treasure_list);
		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);
		$this->display();
	}


	//添加珍品域名类别
	public function add(){
		$this->display();
	}

	//保存添加珍品域名类别
	public function add_save(){
		$database_Domain_suffix = D('Domain_treasure');
		if($database_Domain_suffix->data($this->Removalquotes($_POST))->add()){
			$this->frame_submit_tips(1,'添加成功！');
		}else{
			$this->frame_submit_tips(0,'添加失败~');
		}
	}

	//珍品域名类别编辑
	public function edit(){
		$database_Domain_treasure = D('Domain_treasure');
		$condition_suffix['id'] = intval($_GET['id']);
		$now_Domain_treasure = $database_Domain_treasure->field(true)->where($condition_suffix)->find();
		if(empty($now_Domain_treasure)){
			$this->frame_error_tips('数据库中没有查询到该内容！',3);
		}

		$this->assign('now_Domain_treasure',$now_Domain_treasure);

		$this->display();
	}

	//珍品域名类别编辑保存
	public function edit_save(){
		$database_Domain_treasure = D('Domain_treasure');
		if($database_Domain_treasure->data($this->Removalquotes($_POST))->save()){
			$this->frame_submit_tips(1,'编辑成功！');
		}else{

			$this->frame_submit_tips(0,'编辑失败！');
		}
	}



	//删除珍品域名类别
	public function del(){
		$id = isset($_POST['id']) ? intval($_POST['id']) : 0;
		if($id>0){
			$database_Domain_suffix = D('Domain_treasure');
			if($database_Domain_suffix->where(array('id'=>$id))->delete()){
				$this->success('删除成功！');
				exit();
			}else{
				$this->error('删除失败！请重试~');
				exit();
			}
		}
		$this->error('操作失败！请重试~');
	}

	//珍品域名级别列表
	public function level_list(){
		$database_treasure_levels = D('Domain_treasure_levels');
		$count_treasure_levels = $database_treasure_levels->count();
		import('@.ORG.system_page');
		$p = new Page($count_treasure_levels, 30);
		$levels_list = $database_treasure_levels->field(true)->order('`sorts` DESC,`lid` ASC')->limit($p->firstRow . ',' . $p->listRows)->select();
		$this->assign('levels_list',$levels_list);
		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);
		$this->display();
	}

	//添加珍品域名级别
	public function level_add(){
		$this->display();
	}

	//保存添加珍品域级别
	public function level_add_save(){
		$database_Domain_suffix = D('Domain_treasure_levels');
		if($database_Domain_suffix->data($this->Removalquotes($_POST))->add()){
			$this->frame_submit_tips(1,'添加成功！');
		}else{
			$this->frame_submit_tips(0,'添加失败~');
		}
	}

	//珍品域名级别编辑
	public function level_edit(){
		$database_treasure_levels = D('Domain_treasure_levels');
		$condition_level['lid'] = intval($_GET['lid']);
		$now_Domain_level = $database_treasure_levels->field(true)->where($condition_level)->find();
		if(empty($now_Domain_level)){
			$this->frame_error_tips('数据库中没有查询到该内容！',3);
		}

		$this->assign('now_Domain_level',$now_Domain_level);

		$this->display();
	}

	//珍品域名级别编辑保存
	public function level_edit_save(){
		$database_Domain_treasure = D('Domain_treasure_levels');
		if($database_Domain_treasure->data($this->Removalquotes($_POST))->save()){
			$this->frame_submit_tips(1,'编辑成功！');
		}else{

			$this->frame_submit_tips(0,'编辑失败！');
		}
	}

	//删除珍品域名类别
	public function level_del(){
		$id = isset($_POST['lid']) ? intval($_POST['lid']) : 0;
		if($id>0){
			$database_Domain_suffix = D('Domain_treasure_levels');
			if($database_Domain_suffix->where(array('lid'=>$id))->delete()){
				$this->success('删除成功！');
				exit();
			}else{
				$this->error('删除失败！请重试~');
				exit();
			}
		}
		$this->error('操作失败！请重试~');
	}

	/*     * *去除单双引号*** */
	private function Removalquotes($array) {
		$regex = "/\'|\"|\\\|\<script|\<\/script/";
		if (is_array($array)) {
			foreach ($array as $key => $value) {
				if (is_array($value)) {
					$array[$key] = $this->Removalquotes($value);
				} else {
					$value = strip_tags(trim($value));
					$array[$key] = preg_replace($regex, '', $value);
				}
			}
			return $array;
		} else {
			$array = strip_tags(trim($array));
			$array = preg_replace($regex, '', $array);
			return $array;
		}
	}
}


