<?php 
class EscrowAction  extends BaseAction{
	
	public function index(){
		 if (!empty($_GET['keyword'])) {
            if ($_GET['searchtype'] == 'uid') {
                $condition['uid'] = $_GET['keyword'];
            }else if ($_GET['searchtype'] == 'phone') {
                $condition['phone'] = array('like', '%' . $_GET['keyword'] . '%');
            }else if ($_GET['searchtype'] == 'domainName') {
                $condition['domainName'] = array('like', '%' . $_GET['keyword'] . '%');
            }
        }
		
		$escrow_list_data1 = D('Escrow_list');
			
        $count = $escrow_list_data1->where($condition)->count();
        import('@.ORG.system_page');
        $p = new Page($count, 15);
        $escrow_list_data = $escrow_list_data1->where($condition)->order('`id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();

    
        $this->assign('escrow_list_data', $escrow_list_data);
        $pagebar = $p->show();
        $this->assign('pagebar', $pagebar);
		$this->assign('escrow_list_data',$escrow_list_data);
		$this->display();
	}
	public function add(){
		$this->display();
	}
	public function staff(){
            $staff_list_data1 = D('Escrow_staff');
            $count = $staff_list_data1->count();
            import('@.ORG.system_page');
            $p = new Page($count, 15);
            $staff_list_data = $staff_list_data1->order('`id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
            $this->assign('staff_list_data', $staff_list_data);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
	}
	public function staffAdd(){
		if(IS_POST){
			$userName=M('Escrow_staff')->where('`userName`='.$_POST['userName'])->find();
			if($userName!=false){
				$this->frame_submit_tips(0,'用户名已经存在');
			}
			$_POST['pwd']=md5($_POST['pwd']);
			$database= D('Escrow_staff');
			if($activity_id = $database->data($_POST)->add()){
				
				$this->frame_submit_tips(1,'添加成功！');
			}else{
				$this->frame_submit_tips(0,'添加失败！请重试~');
			}
		}else{
			$this->display();
		}
	}
	public function staffEdit(){
		$database= D('Escrow_staff');
		$staff=$database->where('`id`='.$_GET['id'])->find();
		$this->assign('staff',$staff);
		if(IS_POST){
			$_POST['id']=$staff['id'];
			if($_POST['pwd']){
				$_POST['pwd']=md5($_POST['pwd']);
			}else{
				$_POST['pwd']=$staff['pwd'];
			}
			$activity_id = $database->data($_POST)->save();
			if($activity_id){
				
				$this->frame_submit_tips(1,'编辑成功！');
			}else{
				//dump($database->getLastSql());exit;
				$this->frame_submit_tips(0,'编辑失败！请重试~');
			}
			
		}else{
			if($staff['id']==false){
			$this->frame_submit_tips(0,'不存在的经济人');
		}
			$this->display();
		}
	}
	public function modify(){
		if(IS_POST){
			$_POST['price']=$this->_post('price');
			$database= D('Escrow_list');
			if($activity_id = $database->data($_POST)->add()){
				$this->frame_submit_tips(1,'添加成功！');
			}else{
				$this->frame_submit_tips(0,'添加失败！请重试~');
			}
		}else{
			$this->frame_submit_tips(0,'非法提交,请重新提交~');
		}
	}
        
        
//        更换经纪人记录
        public function replace(){
            if($_POST){
                $where = "`uid` = ".$_POST['uid'];
            }
            $staff_replace = M('Staff_replace');
            $count = $staff_replace->count();
            import('@.ORG.system_page');
            $p = new Page($count, 15);
            $staff_replace_list = $staff_replace->where($where)->order('`id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
            $this->assign('staff_replace_list', $staff_replace_list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }
        
        public function replace_confirm(){
            $staff_replace = M('Staff_replace');
            $databases_user = M('User');
            if($_GET['state'] == 1){
                $databases_user->where(array('uid'=>  intval($_GET['uid'])))->setField('staff_uid',$_GET['new']);
            }
            $row = $staff_replace->save($_GET);
            if($row){
                $this->success('提交成功',U('Escrow/replace'));
            }  else {
                $this->success('提交失败',U('Escrow/replace'));
            }
        }

//        呼叫经纪人
        public function call(){
            $call_staff = M('Call_staff');
            if($_POST){
                $where = "`uid` = ".$_POST['uid'];
            }
            $count = $call_staff->count();
            import('@.ORG.system_page');
            $p = new Page($count, 15);
            $call_staff_list = $call_staff->where($where)->order('`id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
            $this->assign('call_staff_list', $call_staff_list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }
        
        public function call_confirm(){
            $call_staff = M('Call_staff');
            $row = $call_staff->save($_GET);
//            echo $call_staff->getLastSql();
            if($row){
                $this->success('提交成功',U('Escrow/call'));
            }  else {
                $this->success('提交失败',U('Escrow/call'));
            }
        }
        
        
}

?>