<?php
/*
 * 域名分类管理
 *
 * @  Writers    weslee
 * @  BuildTime  2015年12月10日 11:51:16
 *
 */
class DomainsAction extends BaseAction
{

	public function index()
	{
		$database_Domain_classify = D('Domain_classify');
		$condtion['pid'] = 0; //只显示顶级 分类
		$count_Domain_classify = $database_Domain_classify->where($condtion)->count();
		import('@.ORG.system_page');
		$p = new Page($count_Domain_classify, 15);
		$category_list = $database_Domain_classify->field(true)->where($condtion)->order('`sorts` DESC,`id` ASC')->limit($p->firstRow . ',' . $p->listRows)->select();

		$this->assign('category_list', $category_list);
		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);

		$this->display();
	}

	//添加域名分类列表
	public function add(){

		$this->display();
	}

	//分类入库
	public function cat_modify(){
		if (IS_POST) {

			if(empty($_POST['shortag']))  $_POST['shortag'] = $_POST['name'];

			$datas = $this->Removalquotes($_POST);
			$database_Domain_classify = D('Domain_classify');
			//TODO
			//验证短标签是否有重复的

			if ($database_Domain_classify->data($datas)->add()) {
				$this->frame_submit_tips(1,'添加成功！');
			} else {
				$this->frame_submit_tips(0,'添加失败！请重试~');
			}
		} else {
			$this->frame_submit_tips(0,'非法提交,请重新提交~');
		}

	}

	//编辑分类
	public function edit(){
		$database_Domain_classify = D('Domain_classify');
		$condition_Domain_classify['id'] = intval($_GET['id']);
		$now_domain_calssify = $database_Domain_classify->field(true)->where($condition_Domain_classify)->find();
		if(empty($now_domain_calssify)){
			$this->frame_error_tips('数据库中没有查询到该内容！',5);
		}

		$this->assign('now_domain_calssify',$now_domain_calssify);

		$this->display();
	}

	//保存分类修改编辑
	public function edit_modify(){
		$database_Domain_classify = D('Domain_classify');
		$_POST['info'] = fulltext_filter($_POST['info']);
			//dump($_POST);die;
		if($database_Domain_classify->data($this->Removalquotes($_POST))->save()){

		$this->frame_submit_tips(1,'编辑成功！');
		}else{

			$this->frame_submit_tips(0,'编辑失败！');
		}
}

	/*******删除*********/
	public function del(){
		$id = isset($_POST['id']) ? intval($_POST['id']) : 0;

		if($id>0){
			$footer_linkDb = D('Domain_classify');
			if($footer_linkDb->where(array('id'=>$id))->delete()){
				$this->success('删除成功！');
				exit();
			}else{
				$this->error('删除失败！请重试~');
				exit();
			}
		}
		$this->error('操作失败！请重试~');

	}

	//子分类列表
	public function subclass_list(){
		$condition_sub['pid'] = $_GET['id'];
		$database_Domain_classify = D('Domain_classify');
		$count_Domain_classify = $database_Domain_classify->where($condition_sub)->count();
		import('@.ORG.system_page');
		$p = new Page($count_Domain_classify, 30);
		$subclass_list = $database_Domain_classify->field(true)->where($condition_sub)->order('`sorts` DESC,`id` ASC')->limit($p->firstRow . ',' . $p->listRows)
			->select();
		$getPname =   $database_Domain_classify->field('`name`')->where(array('id'=>$_GET['id']))->find();

		$subclass['name'] =$getPname['name'];
		$subclass['id'] =$_GET['id'];

		$this->assign('subclass', $subclass);
		$this->assign('subclass_list', $subclass_list);

		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);
		$this->display();


	}

	//添加子分类列表
	public function subclass_add(){
		$pid = intval($_GET['id']);
		if( $pid ===''){
			$this->error('非法添加,请重新返回~');
		}
		$this->assign('pid',$pid);
		$this->display();
	}

	//编辑子分类列表
	public function subclass_edit(){
		$database_Domain_classify = D('Domain_classify');
		$condition_Domain_classify['id'] = intval($_GET['id']);
		$now_domain_calssify = $database_Domain_classify->field(true)->where($condition_Domain_classify)->find();
		if(empty($now_domain_calssify)){
			$this->frame_error_tips('数据库中没有查询到该内容！',5);
		}

		$this->assign('now_domain_calssify',$now_domain_calssify);
		$this->display();
	}

	//域名后缀管理
	public function suffix(){
		$database_Domain_suffix = D('Domain_suffix');
		$count_Domain_suffix = $database_Domain_suffix->count();
		import('@.ORG.system_page');
		$p = new Page($count_Domain_suffix, 15);
		$suffix_list = $database_Domain_suffix->field(true)->order('`sorts` DESC,`id` ASC')->limit($p->firstRow . ',' . $p->listRows)->select();

		$this->assign('suffix_list', $suffix_list);
		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);
		$this->display();
	}

	//域名后缀添加
	public function suffix_add(){
		$this->display();
	}

	//域名后缀添加保存
	public function suffix_modify(){
		$database_Domain_suffix = D('Domain_suffix');
		$_POST['info'] = fulltext_filter($_POST['info']);
		$_POST['suffix'] = '.'.trim($_POST['suffix'],'.');
		$condition_suffix['suffix'] = $_POST['suffix'];
		$now_Domain_suffix = $database_Domain_suffix->field('`suffix`')->where($condition_suffix)->find();
		if(!empty($now_Domain_suffix)){
			$this->frame_error_tips("数据库中已经有了<font color='red'>{$_POST['suffix']} </font>后缀！",3);
		}
		if($database_Domain_suffix->data($this->Removalquotes($_POST))->add()){
			$this->frame_submit_tips(1,'添加成功！');
		}else{
			$this->frame_submit_tips(0,'添加失败~');
		}
	}

	//域名后缀编辑
	public function suffix_edit(){
		$database_Domain_suffix = D('Domain_suffix');
		$condition_suffix['id'] = intval($_GET['id']);
		$now_Domain_suffix = $database_Domain_suffix->field(true)->where($condition_suffix)->find();
		if(empty($now_Domain_suffix)){
			$this->frame_error_tips('数据库中没有查询到该内容！',3);
		}

		$this->assign('now_Domain_suffix',$now_Domain_suffix);

		$this->display();
	}

	//域名后缀编辑保存
	public function suffix_edit_save(){
		$database_Domain_suffix = D('Domain_suffix');
		$_POST['info'] = fulltext_filter($_POST['info']);
		$_POST['suffix'] = '.'.trim($_POST['suffix'],'.');
		$condition_suffix['suffix'] = $_POST['suffix'];

		if($database_Domain_suffix->data($this->Removalquotes($_POST))->save()){

			$this->frame_submit_tips(1,'编辑成功！');
		}else{

			$this->frame_submit_tips(0,'编辑失败！');
		}
	}

	//域名后缀删除
	public function suffix_del(){
		$id = isset($_POST['id']) ? intval($_POST['id']) : 0;

		if($id>0){
			$database_Domain_suffix = D('Domain_suffix');
			if($database_Domain_suffix->where(array('id'=>$id))->delete()){
				$this->success('删除成功！');
				exit();
			}else{
				$this->error('删除失败！请重试~');
				exit();
			}
		}
		$this->error('操作失败！请重试~');
	}

	//用户提交的域名列表
	public function domainsList(){
		$Db_Domains = D('Domains');
		//$condtion = '';
		$condition = array();
		//搜索条件组合
		if (!empty($_GET['keyword'])) {
			if ($_GET['searchtype'] == 'domain_id') {
				$condition['domain_id'] = trim($_GET['keyword']);
			} else if ($_GET['searchtype'] == 'domain') {
				$condition['domain'] = array('like', '%' . $_GET['keyword'] . '%');
			} else if ($_GET['searchtype'] == 'uid') {
				$condition['uid'] = intval($_GET['keyword']) ? intval($_GET['keyword']) : 0;
			}
			$this->assign('searchtype',$_GET['searchtype']);
		}
		$count_Domains = $Db_Domains->where($condition)->count();
		import('@.ORG.system_page');
		$p = new Page($count_Domains, 20);
		$domains_list = $Db_Domains->field(true)->where($condition)->order('`add_time` DESC,`domain_id` ASC')->limit($p->firstRow . ',' . $p->listRows)
			->select();

		$Db_Domain_treasure = D('Domain_treasure');
		$Db_treasure_levels = D('Domain_treasure_levels');
		foreach($domains_list as $key=>$value){
			$domains_list[$key]['treasure']         = $Db_Domain_treasure->field('`name`')->where('`id`='.$value['theme_id'])->find();
			$domains_list[$key]['treasure_levels'] = $Db_treasure_levels->field('`level_name`')->where('`lid`='.$value['level_id'])->find();
		}

		$this->assign('domains_list', $domains_list);
		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);
		$this->display();
	}

	public function list_edit(){
		$Db_Domains = D('Domains');
		$Db_Domain_treasure = D('Domain_treasure');
		$Db_treasure_levels = D('Domain_treasure_levels');
		$condition_domains_list['domain_id'] = intval($_GET['domain_id']);
		$now_domains_list = $Db_Domains->field(true)->where($condition_domains_list)->find();
		if(empty($now_domains_list)){
			$this->frame_error_tips('数据库中没有查询到该内容！',3);
		}
		$treasure =  $Db_Domain_treasure->field(true)->where('`status`=1')->order('`sorts` DESC')->select();
		$treasure_levels =  $Db_treasure_levels->field(true)->where('`status`=1')->order('`sorts` DESC')->select();
		$this->assign('treasure',$treasure);
		$this->assign('treasure_levels',$treasure_levels);
		$this->assign('now_order',$now_domains_list);
		$this->display();
	}

	public function list_edit_save(){
		$Db_Domains = D('Domains');
		if($Db_Domains->data($_POST)->save()){

			$this->frame_submit_tips(1,'编辑成功！');
		}else{

			$this->frame_submit_tips(0,'编辑失败！');
		}
	}

	public function list_del(){
		$domain_id = isset($_POST['domain_id']) ? intval($_POST['domain_id']) : 0;
		if($domain_id>0){
			$Db_Domains = D('Domains');
			if($Db_Domains->where(array('domain_id'=>$domain_id))->delete()){
				$this->success('删除成功！');
				exit();
			}else{
				$this->error('删除失败！请重试~');
				exit();
			}
		}
		$this->error('操作失败！请重试~');
	}

	/*     * *去除单双引号*** */
	private function Removalquotes($array) {
		$regex = "/\'|\"|\\\|\<script|\<\/script/";
		if (is_array($array)) {
			foreach ($array as $key => $value) {
				if (is_array($value)) {
					$array[$key] = $this->Removalquotes($value);
				} else {
					$value = strip_tags(trim($value));
					$array[$key] = preg_replace($regex, '', $value);
				}
			}
			return $array;
		} else {
			$array = strip_tags(trim($array));
			$array = preg_replace($regex, '', $array);
			return $array;
		}
	}
}