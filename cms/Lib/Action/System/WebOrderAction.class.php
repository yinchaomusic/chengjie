<?php
/**
 * 网站交易
 * @author gcyit
 *
 */
class WebOrderAction extends BaseAction
{

	public function index()
	{
		$database_Web_inter_order = D('Web_inter_order');
		$condition = array();
		if(is_array($_POST)&&count($_POST)>0){
		    if(isset($_POST["status"])){
		        if (!empty($_POST['status'])) {
		            $condition['status'] = intval($_POST['status'])-1;
		            $this->assign('status',$_POST['status']);
		        }
		    };
		    if(isset($_POST["inter_obj"])){
		        if (!empty($_POST['inter_obj'])) {
		            $condition['inter_obj'] = $_POST['inter_obj'];
		            $this->assign('inter_obj',$_POST['inter_obj']);
		        }
		    };
		    if(isset($_POST["web_type"])){
		        if (!empty($_POST['web_type'])) {
		            $condition['web_type'] = $_POST['web_type'];
		            $this->assign('web_type',$_POST['web_type']);
		        }
		    };
		};
		if(count($condition)>0){
		    $count_Web_inter_order = $database_Web_inter_order->where($condition)->count();
		    import('@.ORG.system_page');
		    $p = new Page($count_Web_entrust, 15);
		    $category_list = $database_Web_inter_order->where($condition)->order('`id` ASC')->limit($p->firstRow . ',' . $p->listRows)->select();
		}else {
            $count_Web_inter_order = $database_Web_inter_order->count();
    		import('@.ORG.system_page');
    		$p = new Page($count_Web_inter_order, 15);
    		$category_list = $database_Web_inter_order->order('`id` ASC')->limit($p->firstRow . ',' . $p->listRows)->select();
		};
		
		$this->assign('category_list', $category_list);
		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);

		$this->display();
	}



	//编辑网站
	public function edit(){
		$database_Web_inter_order = D('Web_inter_order');
		$condition_Web_inter_order['id'] = intval($_GET['id']);
		$now_Web_inter_order = $database_Web_inter_order->field(true)->where($condition_Web_inter_order)->find();
		if(empty($now_Web_inter_order)){
			$this->frame_error_tips('数据库中没有查询到该内容！',5);
		}
		$this->assign('now_Web_inter_order',$now_Web_inter_order);
        echo $now_Web_inter_order[0];
		$this->display();
	}

	//保存网站修改编辑
	public function edit_web_order(){
		$database_Web_inter_order = D('Web_inter_order');
		$_POST['info'] = fulltext_filter($_POST['info']);
		$_POST['updatedate'] = date('Y-m-d H:i:s');
			//dump($_POST);die;
		if($database_Web_inter_order->data($this->Removalquotes($_POST))->save()){
		  $this->frame_submit_tips(1,'编辑成功！');
		}else{

			$this->frame_submit_tips(0,'编辑失败！');
		}
}

	/*******删除*********/
	public function del(){
		$id = isset($_POST['id']) ? intval($_POST['id']) : 0;

		if($id>0){
			$footer_linkDb = D('Web_inter_order');
			if($footer_linkDb->where(array('id'=>$id))->delete()){
				$this->success('删除成功！');
				exit();
			}else{
				$this->error('删除失败！请重试~');
				exit();
			}
		}
		$this->error('操作失败！请重试~');

	}

	/*     * *去除单双引号*** */
	private function Removalquotes($array) {
		$regex = "/\'|\"|\\\|\<script|\<\/script/";
		if (is_array($array)) {
			foreach ($array as $key => $value) {
				if (is_array($value)) {
					$array[$key] = $this->Removalquotes($value);
				} else {
					$value = strip_tags(trim($value));
					$array[$key] = preg_replace($regex, '', $value);
				}
			}
			return $array;
		} else {
			$array = strip_tags(trim($array));
			$array = preg_replace($regex, '', $array);
			return $array;
		}
	}
}