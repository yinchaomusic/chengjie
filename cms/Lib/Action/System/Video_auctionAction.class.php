<?php
/**
 * 视频拍卖
 *
 */

class Video_auctionAction extends BaseAction{

	//域名代购列表
	public function index(){
		$Db_videosale_term  = M('Videosale_term');
		$condition = array();
		//搜索条件组合
		if (!empty($_GET['keyword'])) {
			if ($_GET['searchtype'] == 'order_id') {
				$condition['order_id'] = trim($_GET['keyword']);
			} else if ($_GET['searchtype'] == 'domainName') {
				$condition['domainName'] = array('like', '%' . $_GET['keyword'] . '%');
			} else if ($_GET['searchtype'] == 'staff_id') {
				$condition['staff_id'] = intval($_GET['keyword']) ? intval($_GET['keyword']) : 0;
			}
		}

		$order_list_count = $Db_videosale_term->where($condition)->count();
		import('@.ORG.system_page');
		$p = new Page($order_list_count, 15);
		$term_term = $Db_videosale_term->field(true)->order('`term_id` DESC')->where($condition)->limit($p->firstRow . ',' .
			$p->listRows)->select();

		$this->assign('term_term',$term_term);
		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);
		$this->display();
	}

	public function add_number(){

		$this->display();
	}

	public function add_number_save(){
		$Db_videosale_term  = M('Videosale_term');
		$_POST['term_notice']    = fulltext_filter($_POST['term_notice']);
		if($Db_videosale_term->add($_POST)){

			$this->frame_submit_tips(1,'编辑成功！');
		}else{

			$this->frame_submit_tips(0,'编辑失败！');
		}
	}


	public function edit_number(){
		$condition['term_id'] = intval($_GET['term_id']);
		$Db_videosale_term = M('Videosale_term');
		$term_info =$Db_videosale_term->where("`term_id` = ".$condition['term_id'])->find();

		if(empty($term_info)){
			$this->frame_error_tips('数据库中没有查询到该内容！',3);
		}
		$this->assign('term_info',$term_info);
		$this->display();
	}


	public function edit_number_save(){
		$Db_videosale_term = M('Videosale_term');
		$_POST['term_notice']    = fulltext_filter($_POST['term_notice']);
		if($Db_videosale_term->data($_POST)->save()){
			$this->frame_submit_tips(1,'编辑成功！');
		}else{

			$this->frame_submit_tips(0,'编辑失败！');
		}
	}

	public function showinfo(){
		$condition['term_id'] = intval($_GET['term_id']);
		$Db_videosale_term = M('Videosale_term');
		$term_info =$Db_videosale_term->where("`term_id` = ".$condition['term_id'])->find();

		if(empty($term_info)){
			$this->frame_error_tips('数据库中没有查询到该内容！',3);
		}
		$this->assign('term_info',$term_info);
		$this->display();
	}



	public function video_list(){
		$condition = array();
		if (!empty($_GET['keyword'])) {
			 if ($_GET['searchtype'] == 'domains') {
				$condition['domains'] = array('like', '%' . trim($_GET['keyword']) . '%');
			}
		}
		$this->assign('term_id',intval($_GET['term_id']));
		$condition['term_id'] = intval($_GET['term_id']);
		$Db_videosale_domains  = M('Videosale_domains');
		$Db_videosale_term  = M('Videosale_term');
		$Db_user  = M('User');
		$count_videos = $Db_videosale_domains->where($condition)->count();
		//echo $Db_videosale_domains->getLastSql();
		import('@.ORG.system_page');
		$p = new Page($count_videos, 10);
		$count_videos_list = $Db_videosale_domains->field(true)->where($condition)->order('`sort` DESC,`domain_id` ASC')->limit($p->firstRow . ',' .
			$p->listRows)->select();
		if(!empty($count_videos_list)){
			foreach($count_videos_list as $k => $v){
				$count_videos_list[$k]['term'] =$Db_videosale_term->field('`term_name`')->where('`term_id` = ' .$v['term_id'])->find();
				if($v['lastFidId'] > 0){
					$get_last_uid = D('Videosale_domains_list')->where(array('pigcms_id'=>$v['lastFidId']))->getField('uid');
					$count_videos_list[$k]['Uname'] =   $Db_user->where(array('uid'=>$get_last_uid))->getField('`nickname`');
				}
				if( $v['domain_status'] == 3){
					$count_videos_list[$k]['Uname'] = "<font color='red'>无人得标 流拍</font>";
				}
			}
		}else{
			$count_videos_list[0]['term']  = $Db_videosale_term->field('`term_name`')->where('`term_id` = ' .$condition['term_id'])->find();
		}

		//dump($count_videos_list);
		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);
		$this->assign('count_videos_list', $count_videos_list);
		$this->display();
	}

	public function video_edit(){
		$condition['domain_id'] = intval($_GET['domain_id']);
		$Db_videosale_domains= M('Videosale_domains');
		$Db_videosale_term  = M('Videosale_term');
		$video_info =$Db_videosale_domains->where("`domain_id` = ".$condition['domain_id'])->find();
		//echo $Db_videosale_domains->getLastSql();

		if(empty($video_info)){
			$this->frame_error_tips('数据库中没有查询到该内容！',3);
		}
		if($video_info['lastFidId'] > 0){
			$this->frame_error_tips('已经有人出价，不可以编辑！',3);exit;
		}
		$term_info =$Db_videosale_term->field('`term_name`')->where('`term_id` = ' .$video_info['term_id'])->find();

		$video_info['term_name'] = $term_info['term_name'];
		$this->assign('video_info',$video_info);
		$this->display();
	}

	public function video_edit_save(){
		$Db_videosale_domains= M('Videosale_domains');
		$_POST['now_money'] = $_POST['mark_money'];
		if($Db_videosale_domains->data($_POST)->save()){
			$this->frame_submit_tips(1,'编辑成功！');
		}else{

			$this->frame_submit_tips(0,'编辑失败！');
		}
	}

	public function video_domains(){
		$term_id = intval($_GET['term_id']);
		if($term_id < 0){
			$this->frame_submit_tips(0,'请重新进去该页面！');exit;
		}
		$Db_videosale_domains = M('Videosale_domains');
		$this->assign('term_id',$term_id);
		if(IS_POST){
			//$_POST['domain_id']  = intval($_POST['domain_id']);
			$_POST['term_id']  = intval($_POST['term_id']);
			$_POST['now_money']  = intval($_POST['mark_money']);

			$Db_domains = M('Domains');
			$domains_condition['type'] = 2;
		    $domains_condition['status'] = 1;
		    $domains_condition['domain'] = trim($_POST['domains']);
			$check_domains = $Db_domains->field('`domain_id`,`domain`,`uid`')->where($domains_condition)->find();

			if(empty($_POST['domains'])){
				$this->frame_submit_tips(0,'域名必须填写！');exit;
			}
			if(empty($_POST['domains_desc'])){
				$this->frame_submit_tips(0,'域名描述必须填写！');exit;
			}
			if(empty($_POST['domains_suffix'])){
				$this->frame_submit_tips(0,'域名后缀必须填写！');exit;
			}
			if(empty($_POST['domains_suffix']) ){
				$this->frame_submit_tips(0,'域名后缀必须填写！');exit;
			}

//			if(empty($_POST['mark_money']) ||intval($_POST['mark_money']) < 1 ){
//				$this->frame_submit_tips(0,'起拍价必须填写或者必须大于等于1块！');exit;
//			}

			if(!empty($check_domains)){
				$_POST['uid'] = $check_domains['uid'];

			}

				if($Db_videosale_domains->add($_POST)){
					$this->frame_submit_tips(1,'添加成功！');exit;
				}else{
					$this->frame_submit_tips(0,'添加失败！');exit;
				}
		}

		$domains_list = $Db_videosale_domains->field(true)->where('`term_id` = ' .$term_id)->order('`sort` DESC')->select();
		$this->assign('videosale_domains_list',$domains_list);
		//dump($domains_list);

		$this->display();

	}

	public function del_video(){
		$domain_id = isset($_POST['domain_id']) ? intval($_POST['domain_id']) : 0;
		if($domain_id>0){
			$Db_videosale_domains= M('Videosale_domains');
			$islastFidId = $Db_videosale_domains->field('`lastFidId`')->where(array('domain_id'=>$domain_id))->find();

			if($islastFidId['lastFidId'] > 0){
				$this->error('已经有人出价，不可以删除！');
				exit();
			}
			if($Db_videosale_domains->where(array('domain_id'=>$domain_id))->delete()){
				$this->success('删除成功！');
				exit();
			}else{
				$this->error('删除失败！请重试~');
				exit();
			}
		}
		$this->error('操作失败！请重试~');
	}

	public function bid_record(){

		$condition = array();
		$condition['domain_id']  =  intval($_GET['domain_id']);
		$Db_videosale_domains_list = M('Videosale_domains_list');
		$Db_videosale_domains      = M('Videosale_domains');
		$Db_videosale_term         = M('Videosale_term');
		$videosale_domains_count = $Db_videosale_domains_list->where($condition)->count();
		import('@.ORG.system_page');
		$p = new Page($videosale_domains_count, 15);
		$videosale_domains_list = $Db_videosale_domains_list->field(true)->where($condition)->order('`time` DESC')->limit($p->firstRow . ',' .
			$p->listRows)->select();
		//dump($videosale_domains_list);
		foreach($videosale_domains_list as $k => $val){
				$videosale_domains_list[$k]['domainsName'] = $Db_videosale_domains->field('`domains`,`term_id`')
																->where('`domain_id` =' .$val['domain_id'])
																->find();
			 $videosale_domains_list[$k]['termName'] =$Db_videosale_term->field('`term_name`')->where('`term_id` = '.$videosale_domains_list[$k]['domainsName']['term_id'])->find();
		}


		$this->assign('videosale_domains_list',$videosale_domains_list);
		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);

		$this->display();
	}

//	public function getCheckDomains(){
//		$Db_domains = M('Domains');
//		$domains_condition['type'] = 2;
//		$domains_condition['status'] = 1;
//		import('@.ORG.system_page');
//		$countDomains = $Db_domains->where($domains_condition)->count();
//		$p            = new Page($countDomains, 15);
//		$getDomains  = $Db_domains->field('`domain_id`,`domain`,`uid`')->where($domains_condition)->order('`domain_id` DESC')->limit($p->firstRow . ',' .
//			$p->listRows)->select();
//		//dump($getDomains);
//		/***
//		 * [0] => array(3) {
//		["domain_id"] => string(2) "22"
//		["domain"] => string(8) "2wgf.com"
//		["uid"] => string(1) "4"
//		}
//		 */
//		$this->assign('getDomains', $getDomains);
//		$pagebar = $p->show();
//		$this->assign('pagebar', $pagebar);
//
//		$this->display();
//	}


}