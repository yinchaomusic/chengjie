<?php
class LinkAction extends BaseAction
{
	public $modules;
	
	public function _initialize() 
	{
		parent::_initialize();
		
		$this->modules = array(
			'Home' => '首页',
			'Profile' => '会员中心',
			'Fastbid' => '极速竞价',
			'Fastbidp' => '拍卖列表',
			'Bargain' => '优质域名',
			'Buydomains' => '议价域名',
			'Hotsale' => '一口价',
			'Procurement' => '域名代购',
			'Terminal' => '终端域名',
			'Pldomain' => '批量交易',
			'VideoSale' => '视频拍卖',
		);
	}
	public function insert()
	{
		$modules = $this->modules();
		$this->assign('modules', $modules);
		$this->display();
	}
	public function modules()
	{
		$t = array(
		array('module' => 'Home', 'linkcode' => str_replace('admin.php', 'wap.php', U('Wap/Home/index', '', true, false, true)), 'name'=>'微站首页','sub'=>0,'canselected'=>1,'linkurl'=>'','keyword'=>$this->modules['Home'],'askeyword'=>1),

		array('module' => 'Profile', 'linkcode' => str_replace('admin.php', 'wap.php', U('Wap/Profile/index', '', true, false, true)),'name'=>$this->modules['Profile'],'sub'=>0,'canselected'=>1,'linkurl'=>'','keyword'=>'','askeyword'=>1),
		array('module' => 'Fastbid', 'linkcode' => str_replace('admin.php', 'wap.php', U('Wap/Fastbid/index', '', true, false, true)),'name'=>$this->modules['Fastbid'],'sub'=>0,'canselected'=>1,'linkurl'=>'','keyword'=>'','askeyword'=>1),
		array('module' => 'Fastbid', 'linkcode' => str_replace('admin.php', 'wap.php', U('Wap/Fastbid/lists', '', true, false, true)),'name'=>$this->modules['Fastbidp'],'sub'=>0,'canselected'=>1,'linkurl'=>'','keyword'=>'','askeyword'=>1),
		array('module' => 'Bargain', 'linkcode' => str_replace('admin.php', 'wap.php', U('Wap/Bargain/index', '', true, false, true)),'name'=>$this->modules['Bargain'],'sub'=>0,'canselected'=>1,'linkurl'=>'','keyword'=>'','askeyword'=>1),
		array('module' => 'Buydomains', 'linkcode' => str_replace('admin.php', 'wap.php', U('Wap/Buydomains/index', '', true, false, true)),'name'=>$this->modules['Buydomains'],'sub'=>0,'canselected'=>1,'linkurl'=>'','keyword'=>'','askeyword'=>1),
		array('module' => 'Hotsale', 'linkcode' => str_replace('admin.php', 'wap.php', U('Wap/Hotsale/index', '', true, false, true)),'name'=>$this->modules['Hotsale'],'sub'=>0,'canselected'=>1,'linkurl'=>'','keyword'=>'','askeyword'=>1),
		array('module' => 'Procurement', 'linkcode' => str_replace('admin.php', 'wap.php', U('Wap/Procurement/index', '', true, false, true)),'name'=>$this->modules['Procurement'],'sub'=>0,'canselected'=>1,'linkurl'=>'','keyword'=>'','askeyword'=>1),
		array('module' => 'Terminal', 'linkcode' => str_replace('admin.php', 'wap.php', U('Wap/Terminal/index', '', true, false, true)),'name'=>$this->modules['Terminal'],'sub'=>0,'canselected'=>1,'linkurl'=>'','keyword'=>'','askeyword'=>1),
		array('module' => 'Pldomain', 'linkcode' => str_replace('admin.php', 'wap.php', U('Wap/Pldomain/index', '', true, false, true)),'name'=>$this->modules['Pldomain'],'sub'=>0,'canselected'=>1,'linkurl'=>'','keyword'=>'','askeyword'=>1),
		array('module' => 'VideoSale', 'linkcode' => str_replace('admin.php', 'wap.php', U('Wap/VideoSale/index', '', true, false, true)),'name'=>$this->modules['VideoSale'],'sub'=>0,'canselected'=>1,'linkurl'=>'','keyword'=>'','askeyword'=>1),
		);
		
		return $t;
	}
	
	public function Group()
	{
		$this->assign('moduleName', $this->modules['Group']);
		$cat_fid = isset($_GET['cat_fid']) ? intval($_GET['cat_fid']) : 0;
		$where = array('cat_fid' => $cat_fid);
		$db = D('Group_category');
		$count      = $db->where($where)->count();
		$Page       = new Page($count, 5);
		$show       = $Page->show();
		
		$list = $db->where($where)->limit($Page->firstRow.','.$Page->listRows)->select();
		$items = array();
		foreach ($list as $item){
			if ($db->where(array('cat_fid' => $item['cat_id']))->find()) {
				array_push($items, array('id' => $item['cat_id'], 'sub' => 1, 'name' => $item['cat_name'], 'linkcode'=> str_replace('admin.php', 'wap.php', U('Wap/Group/index', array('cat_url' => $item['cat_url']), true, false, true)),'sublink' => U('Link/group', array('cat_fid' => $item['cat_id']), true, false, true),'keyword' => $item['cat_name']));
			} else {
				array_push($items, array('id' => $item['cat_id'], 'sub' => 0, 'name' => $item['cat_name'], 'linkcode'=> str_replace('admin.php', 'wap.php', U('Wap/Group/index', array('cat_url' => $item['cat_url']), true, false, true)),'sublink' => '','keyword' => $item['cat_name']));
			}
		}
		$this->assign('list', $items);
		$this->assign('page', $show);
		$this->display('detail');
	}
}
?>