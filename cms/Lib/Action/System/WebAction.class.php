<?php
/**
 * 
 * @author gcyit
 *
 */
class WebAction extends BaseAction
{

	public function index()
	{
		$database_Web_entrust = D('Web_entrust');
		$condition = array();
		 if(is_array($_POST)&&count($_POST)>0){
    		 if(isset($_POST["wstatus"])){
        		 if (!empty($_POST['wstatus'])) {
            		 $condition['wstatus'] = intval($_POST['wstatus'])-1;
            		 $this->assign('wstatus',$_POST['wstatus']);
        		 }
    		 };
    		 if(isset($_POST["entr_type"])){
        		 if (!empty($_POST['entr_type'])) {
            		 $condition['entr_type'] = $_POST['entr_type'];
            		 $this->assign('entr_type',$_POST['entr_type']);
        		 }
    		 };
    		 if(isset($_POST["web_type"])){
        		 if (!empty($_POST['web_type'])) {
            		 $condition['web_type'] = $_POST['web_type'];
            		 $this->assign('web_type',$_POST['web_type']);
        		 }
    		 };
		 };
		 if(count($condition)>0){
    		 $count_Web_entrust = $database_Web_entrust->where($condition)->count();
    		 import('@.ORG.system_page');
    		 $p = new Page($count_Web_entrust, 15);
    		 $category_list = $database_Web_entrust->where($condition)->order('`id` ASC')->limit($p->firstRow . ',' . $p->listRows)->select();
		 }else {
    		 $count_Web_entrust = $database_Web_entrust->count();
    		 import('@.ORG.system_page');
    		 $p = new Page($count_Web_entrust, 15);
    		 $category_list = $database_Web_entrust->order('`id` ASC')->limit($p->firstRow . ',' . $p->listRows)->select();
		 };
		 
		 $database_Web_high = D('Web_high');
		 $category_high = $database_Web_high->select();
		 
		 $this->assign('category_list', $category_list);
		 $this->assign('category_high', $category_high);
		 
		 $pagebar = $p->show();
		 $this->assign('pagebar', $pagebar);
		 $this->display();
	}

	//添加网站
	public function add(){
	    $this->display();
	}
	
	//保存网站
	public function saveWeb(){
	    $WebEntrust = D('Web_entrust');
	    $_POST['info'] = fulltext_filter($_POST['info']);
	    $_POST['createdate'] = date('Y-m-d H:i:s');
	    
	    if ($WebEntrust->create($data)) {
	        if (false !== $WebEntrust->add()) {
	            $this->frame_submit_tips(1,'增加成功！');
	        } else {
	            $this->frame_submit_tips(0,'增加失败！');
	        }
	    } else {
	        $this->frame_submit_tips(0,'增加失败！');
	    }
	}
	
	

	//编辑网站
	public function edit(){
		$database_Web_entrust = D('Web_entrust');
		$condition_Web_entrust['id'] = intval($_GET['id']);
		$now_Web_entrust = $database_Web_entrust->field(true)->where($condition_Web_entrust)->find();
		if(empty($now_Web_entrust)){
			$this->frame_error_tips('数据库中没有查询到该内容！',5);
		}
		$this->assign('now_Web_entrust',$now_Web_entrust);
		$this->display();
	}

	//保存网站修改编辑
	public function edit_web(){
		$database_Web_entrust = D('Web_entrust');
		$_POST['info'] = fulltext_filter($_POST['info']);
		$_POST['updatedate'] = date('Y-m-d H:i:s');
		if($database_Web_entrust->data($this->Removalquotes($_POST))->save()){
	      $this->frame_submit_tips(1,'编辑成功！');
		}else{
			$this->frame_submit_tips(0,'编辑失败！');
		}
    }

	/*******删除*********/
	public function del(){
		$id = isset($_POST['id']) ? intval($_POST['id']) : 0;
		if($id>0){
			$footer_linkDb = D('Web_entrust');
			if($footer_linkDb->where(array('id'=>$id))->delete()){
				$this->success('删除成功！');
				exit();
			}else{
				$this->error('删除失败！请重试~');
				exit();
			}
		}
		$this->error('操作失败！请重试~');

	}

	//修改网站是否设置为优质网站
	public function set_web_high(){
	    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
	    $type = isset($_GET['type']) ? intval($_GET['type']) : 0;
	    if($id>0){
	        $database_Web_high = D('Web_high');
	        $data['web_id'] = $id;
	        $now_Web_high = $database_Web_high->where($data)->find();
	        if(empty($now_Web_high)){
	            if($type == 1){
	                $data['createdate'] = date('Y-m-d H:i:s');
	                $database_Web_high->add($data);
	            }
	        }else{
	            if($type == 0){
	                $database_Web_high->where($data)->delete();
	            }
	        };
            $this->success('设置成功！');
            exit();
	    }
	    $this->error('操作失败！请重试~');
	}
	
	
	/*     * *去除单双引号*** */
	private function Removalquotes($array) {
		$regex = "/\'|\"|\\\|\<script|\<\/script/";
		if (is_array($array)) {
			foreach ($array as $key => $value) {
				if (is_array($value)) {
					$array[$key] = $this->Removalquotes($value);
				} else {
					$value = strip_tags(trim($value));
					$array[$key] = preg_replace($regex, '', $value);
				}
			}
			return $array;
		} else {
			$array = strip_tags(trim($array));
			$array = preg_replace($regex, '', $array);
			return $array;
		}
	}
}