<?php
/*
 * 帮助中心
 * 关于我们
 * 
 */
class AboutAction extends BaseAction{
	public function index(){
            $about = M('about');
            $about_list = $about->select();
            $this->assign('about_list',$about_list);
            $this->display();
	}
        
        public function add(){
            if($_POST){
                $about = M('about');
                unset ($_POST['dosubmit']);
                $_POST['lasttime'] = time();
                $_POST['content']    = fulltext_filter($_POST['content']);
                $row = $about->add($_POST);
                if($row){
                            $this->frame_submit_tips(1,'添加成功！');
                        }  else {
                            $this->frame_submit_tips(0,'添加失败！请重试~');
                        }
            }
            $this->display();
        }
        
        public function edit(){
            
            if($_POST){
                $about = M('about');
                unset ($_POST['dosubmit']);
                $_POST['lasttime'] = time();
                $_POST['content']    = fulltext_filter($_POST['content']);
                $row = $about->data($_POST)->save();
//                dump($_POST);
//                echo $about->getLastSql();
//                die;
                if($row){
                            $this->frame_submit_tips(1,'修改成功！');
                        }  else {
                            $this->frame_submit_tips(0,'修改失败！请重试~');
                        }
            }
            $id = intval($_GET['id']);
            $about = M('about');
            $ab_list = $about->where('`id`='.$id)->find();
            $this->assign('ab_list',$ab_list);
            $this->display();
        }
        
//        public function del(){
//            $about = M('about');
//            $id = intval($_GET['id']);
//            $about->where('`id`='.$id)->delete();
//            $this->success('删除成功！');
//        }
}