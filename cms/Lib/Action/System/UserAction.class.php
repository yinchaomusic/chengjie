<?php

/*
 * 用户中心
 *
 */

class UserAction extends BaseAction {
    public function index() {
        //搜索
        if (!empty($_GET['keyword'])) {
            if ($_GET['searchtype'] == 'uid') {
                $condition_user['uid'] = $_GET['keyword'];
            } else if ($_GET['searchtype'] == 'nickname') {
                $condition_user['nickname'] = array('like', '%' . $_GET['keyword'] . '%');
            } else if ($_GET['searchtype'] == 'phone') {
                $condition_user['phone'] = array('like', '%' . $_GET['keyword'] . '%');
            } elseif ($_GET['searchtype'] == 'email') {
                $condition_user['email'] = array('like', '%' . $_GET['keyword'] . '%');
            }
        }

        $database_user = D('User');

        $count_user = $database_user->where($condition_user)->count();
        import('@.ORG.system_page');
        $p = new Page($count_user, 15);
        $user_list = $database_user->field(true)->where($condition_user)->order('`uid` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();

        if (!empty($user_list)) {
            import('ORG.Net.IpLocation');
            $IpLocation = new IpLocation();
            foreach ($user_list as &$value) {
                $last_location = $IpLocation->getlocation(long2ip($value['last_ip']));
                $value['last_ip_txt'] = iconv('GBK', 'UTF-8', $last_location['country']);
            }
        }
        $this->assign('user_list', $user_list);
        $pagebar = $p->show();
        $this->assign('pagebar', $pagebar);

        $this->display();
    }

    public function edit() {
        $this->assign('bg_color', '#F3F3F3');

        $database_user = D('User');
        $condition_user['uid'] = intval($_GET['uid']);
        $now_user = $database_user->field(true)->where($condition_user)->find();
        if (empty($now_user)) {
            $this->frame_error_tips('没有找到该用户信息！');
        }

        $levelDb = M('User_level');
        $tmparr = $levelDb->field(true)->order('id ASC')->select();
        $levelarr = array();
        if ($tmparr) {
            foreach ($tmparr as $vv) {
                $levelarr[$vv['level']] = $vv;
            }
        }
        $databases_user_group = M('User_group');
        $user_group = $databases_user_group->select();
        $this->assign('user_group',$user_group);
        $this->assign('levelarr', $levelarr);
        $this->assign('now_user', $now_user);

        $this->display();
    }

    public function amend() {
        if (IS_POST) {
            $database_user = D('User');
            $condition_user['uid'] = intval($_POST['uid']);
            $now_user = $database_user->field(true)->where($condition_user)->find();
            if (empty($now_user)) {
                $this->error('没有找到该用户信息！');
            }
            $condition_user['uid'] = $now_user['uid'];
            $data_user['nickname'] = $_POST['nickname'];
            $data_user['phone'] = $_POST['phone'];
            $data_user['email'] = $_POST['email'];
            $data_user['group_id'] = $_POST['group_id'];
            if ($_POST['pwd']) {
                $data_user['pwd'] = md5($_POST['pwd']);
            }
            $data_user['sex'] = $_POST['sex'];
            $data_user['province'] = $_POST['province'];
            $data_user['city'] = $_POST['city'];
            $data_user['qq'] = $_POST['qq'];
            $data_user['yy'] = $_POST['yy'];
            $data_user['weixin'] = $_POST['weixin'];
            $data_user['status'] = $_POST['status'];
            $data_user['alias_name'] = trim($_POST['alias_name']);
			$data_user['youaddress'] = trim($_POST['youaddress']);
			$data_user['truename'] = trim($_POST['truename']);
			$data_user['is_sys_op'] = trim($_POST['is_sys_op']);
			$data_user['chujia_name'] = trim($_POST['chujia_name']);

            if($data_user['is_sys_op'] == 1){
                if(empty($data_user['chujia_name'])){
                    $this->error('你设置该用户为系统出价员，请必须设置出价员名称');
                }
            }

            $_POST['set_money'] = floatval($_POST['set_money']);
            if (!empty($_POST['set_money'])) {
                if ($_POST['set_money_type'] == 1) {
                    $data_user['now_money'] = $now_user['now_money'] + $_POST['set_money'];
                } else {
                    $data_user['now_money'] = $now_user['now_money'] - $_POST['set_money'];
                }
                if ($data_user['now_money'] < 0) {
                    $this->error('修改后，余额不能小于0');
                }
            }

            $_POST['set_score'] = intval($_POST['set_score']);
            if (!empty($_POST['set_score'])) {
                if ($_POST['set_score_type'] == 1) {
                    $data_user['score_count'] = $now_user['score_count'] + $_POST['set_score'];
                } else {
                    $data_user['score_count'] = $now_user['score_count'] - $_POST['set_score'];
                }
                if ($data_user['score_count'] < 0) {
                    $this->error('修改后，积分不能小于0');
                }
            }

            $data_user['level'] = intval($_POST['level']);

            if ($database_user->where($condition_user)->data($data_user)->save()) {
                if (!empty($_POST['set_money'])) {
                    D('User_money_list')->add_row($now_user['uid'], $_POST['set_money_type'], $_POST['set_money'], $data_user['now_money'], '管理员后台操作', false);
                }
                if (!empty($_POST['set_score'])) {
                    D('User_score_list')->add_row($now_user['uid'], $_POST['set_score_type'], $_POST['set_score'], '管理员后台操作', false);
                }
                $this->success('修改成功！');
            } else {
                $this->error('修改失败！请重试。');
            }
        } else {
            $this->error('非法访问！');
        }
    }

    public function money_list() {
        $this->assign('bg_color', '#F3F3F3');


        $database_user_money_list = D('User_money_list');
        $condition_user_money_list['uid'] = intval($_GET['uid']);

        $count = $database_user_money_list->where($condition_user_money_list)->count();
        import('@.ORG.system_page');
        $p = new Page($count, 15);

        $money_list = $database_user_money_list->field(true)->where($condition_user_money_list)->order('`time` DESC')->select();

        $this->assign('pagebar', $p->show());
        $this->assign('money_list', $money_list);
        $this->display();
    }

    public function score_list() {
        $this->assign('bg_color', '#F3F3F3');


        $database_user_score_list = D('User_score_list');
        $condition_user_score_list['uid'] = intval($_GET['uid']);

        $count = $database_user_score_list->where($condition_user_score_list)->count();
        import('@.ORG.system_page');
        $p = new Page($count, 15);

        $score_list = $database_user_score_list->field(true)->where($condition_user_score_list)->order('`time` DESC')->select();

        $this->assign('pagebar', $p->show());
        $this->assign('score_list', $score_list);
        $this->display();
    }

    /*     * *导入客户页**** */

    public function import() {

        $this->display();
    }

    /*     * *导入客户页**** */

    public function execimport() {
        if ($_FILES['file']['error'] != 4) {

            $getupload_dir = "/upload/excel/user/" . date('Ymd') . '/';
            $upload_dir = "." . $getupload_dir;
            if (!is_dir($upload_dir)) {
                mkdir($upload_dir, 0777, true);
            }
            import('ORG.Net.UploadFile');
            $upload = new UploadFile();
            $upload->maxSize = 10 * 1024 * 1024;
            $upload->allowExts = array('xls', 'xlsx');
            $upload->allowTypes = array(); // 允许上传的文件类型 留空不做检查
            $upload->savePath = $upload_dir;
            $upload->thumb = false;
            $upload->thumbType = 0;
            $upload->imageClassPath = '';
            $upload->thumbPrefix = '';
            $upload->saveRule = 'uniqid';
            if ($upload->upload()) {
                $uploadList = $upload->getUploadFileInfo();
                require_once APP_PATH . 'Lib/ORG/phpexcel/PHPExcel/IOFactory.php';
                $path = $uploadList['0']['savepath'] . $uploadList['0']['savename'];
                //$reader = PHPExcel_IOFactory::createReader('Excel5');
                $fileType = PHPExcel_IOFactory::identify($path); //文件名自动判断文件类型
                $objReader = PHPExcel_IOFactory::createReader($fileType);
                $excelObj = $objReader->load($path);
                $result = $excelObj->getActiveSheet()->toArray(null, true, true, true);
                if (!empty($result) && is_array($result)) {
                    unset($result[1]);
                    $user_importDb = D('User_import');
                    foreach ($result as $kk => $vv) {
                        if (empty($vv['A']) || empty($vv['B']) || empty($vv['C']))
                            continue;
                        $tmpdata = array();
                        $tmpdata['ppname'] = htmlspecialchars(trim($vv['A']), ENT_QUOTES);
                        $tmpdata['telphone'] = htmlspecialchars(trim($vv['B']), ENT_QUOTES);
                        $tmpdata['address'] = htmlspecialchars(trim($vv['C']), ENT_QUOTES);
                        !empty($vv['D']) && $tmpdata['mer_id'] = intval(trim($vv['D']));
                        !empty($vv['E']) && $tmpdata['memberid'] = htmlspecialchars(trim($vv['E']), ENT_QUOTES);
                        !empty($vv['F']) && $tmpdata['level'] = intval(trim($vv['F']));
                        !empty($vv['G']) && $tmpdata['qq'] = htmlspecialchars(trim($vv['G']), ENT_QUOTES);
                        !empty($vv['H']) && $tmpdata['email'] = htmlspecialchars(trim($vv['H']), ENT_QUOTES);
                        !empty($vv['I']) && $tmpdata['money'] = intval(trim($vv['I']));
                        !empty($vv['J']) && $tmpdata['integral'] = htmlspecialchars(trim($vv['J']), ENT_QUOTES);
                        !empty($vv['K']) && $tmpdata['useraccount'] = htmlspecialchars(trim($vv['K']), ENT_QUOTES);
                        if (!empty($vv['L'])) {
                            $tmpdata['pwdmw'] = trim($vv['L']);
                            $tmpdata['pwd'] = md5($tmpdata['pwdmw']);
                        }
                        $tmpdata['isuse'] = 0;
                        $tmpdata['addtime'] = time();
                        $user_importDb->add($tmpdata);
                    }
                    if (!empty($tmpdata)) {
                        $this->dexit(array('error' => 0));
                    } else {
                        $this->dexit(array('error' => 1, 'msg' => '导入失败！'));
                    }
                }
            } else {
                $this->dexit(array('error' => 1, 'msg' => $upload->getErrorMsg()));
            }
        }
        $this->dexit(array('error' => 1, 'msg' => '文件上传失败！'));
    }

    /*     * *导入客户的列表页**** */

    public function importlist() {
        $user_importDb = D('User_import');
        $count_userimportDb = $user_importDb->where('22=22')->count();
        import('@.ORG.system_page');
        $p = new Page($count_userimportDb, 20);
        $pagebar = $p->show();
        $this->assign('pagebar', $pagebar);
        $tmpdatas = $user_importDb->where('22=22')->order('id ASC')->limit($p->firstRow . ',' . $p->listRows)->select();
        $this->assign('userimport', $tmpdatas);
        $this->display();
    }

    /*     * *导入客户的列表页**** */

    
//    积分列表
    public function levellist() {
        $user_levelDb = D('User_level');
        $count_userlevelDb = $user_levelDb->count();
        import('@.ORG.system_page');
        $p = new Page($count_userlevelDb, 20);
        $pagebar = $p->show();
        $this->assign('pagebar', $pagebar);
        $tmpdatas = $user_levelDb->order('id ASC')->limit($p->firstRow . ',' . $p->listRows)->select();
        $this->assign('userlevel', $tmpdatas);
        $this->display();
    }
    
    public function grouplist() {
        $databases_user_group = M('User_group');
        $user_group_list = $databases_user_group->select();
        $this->assign('user_group_list', $user_group_list);
        $this->display();
    }
    
    public function addgroup(){
        
//        $_POST['permissions_id'] = explode(',',$_POST['permissions_id']);
        $databases_user_group = M('User_group');
        if($_POST){
            $_POST['permissions_id'] = implode(',',$_POST['permissions_id']);
            if($_POST['group_id']>0){
                $info = $databases_user_group->save($_POST);
            }  else {
                $info = $databases_user_group->add($_POST);
            }
            if($info){
                $this->frame_submit_tips(1,'保存成功！');
            }  else {
                $this->frame_submit_tips(0,'保存失败！请重试~');
            }
        }

        $group_info = $databases_user_group->where(array('group_id'=>  intval($_GET['id'])))->find();
        $group_info['permissions_id'] = explode(',',$group_info['permissions_id']);
        $this->assign('group_info',$group_info);

        $group_permissions_data = array(
            '0'=>array('id'=>1,'name'=>"拍卖免保证金"),
            );

        foreach ($group_permissions_data as $k => $v){
            $checked = "";
            foreach ($group_info['permissions_id'] as $kk=>$vv){
                if($v['id'] == $vv){
                    $checked = 'checked="checked"';
                }
            }
            $check_data[] = '<tr><td width="80">用户组权限</td><td><ladel><input type="checkbox" name="permissions_id[]" value="'.$v['id'].'" '.$checked.' />
'.$v['name'].'</ladel></td><td>提醒：在视频拍卖的时候无需缴纳保证金</td></tr>';
        }
        $this->assign('check_data',$check_data);
        $this->display();
    }
    
//    public function group_permissions(){
//        $databases_user_group_permissions = M('User_group_permissions');
//        $count = $databases_user_group_permissions->count();
//        import('@.ORG.system_page');
//        $p = new Page($count, 20);
//        $group_permissions_data = $databases_user_group_permissions->order('id ASC')->limit($p->firstRow . ',' . $p->listRows)->select();
//        $pagebar = $p->show();
//        $this->assign('pagebar', $pagebar);
//        $this->assign('group_permissions_data', $group_permissions_data);
//        $this->display();
//    }
//    public function add_group_permissions(){
//        $databases_user_group_permissions = M('User_group_permissions');
//        $id = intval($_GET['id']);
//        $group_permissions_data = $databases_user_group_permissions->where(array('id'=>$id))->find();
//        $this->assign('group_permissions_data',$group_permissions_data);
//        
//        if($_POST){
//            if($_POST['id'] > 0){
//                $info = $databases_user_group_permissions->save($_POST);
//            }  else {
//                $info = $databases_user_group_permissions->add($_POST);
//            }
//            if($info){
//                $this->frame_submit_tips(1,'保存成功！');
//            }  else {
//                $this->frame_submit_tips(0,'保存失败！请重试~');
//            }
//        }
//        $this->display();
//    }
//    public function del_group_permissions(){
//        $databases_user_group_permissions = M('User_group_permissions');
//        $databases_user_group_permissions->where(array('id'=>  intval($_GET['id'])))->delete();
//        $this->success('删除成功');
//    }

    /*     * *添加等级**** */

    public function addlevel() {
        
        $levelDb = M('User_level');
        $tmparr = $levelDb->order('level DESC')->find();
        $level = 0;
        if (!empty($tmparr)) {
            $level = $tmparr['level'];
        }
        $level = $level + 1;

        
        if ($_POST) {
            
            $lid = intval($_POST['lid']);
            if (!($lid > 0)) {
                $newdata = array('level' => $level);
            }
            if($_FILES['img']['error'] == 0){
                $image = D('Image')->handle($this->system_session['id'], 'User_level', 0, array('size' => 10), false);
                if (!$image['error']) {
                        $_POST = array_merge($_POST, str_replace('/upload/User_level/', '', $image['url']));
                } else {
                        $this->frame_submit_tips(0, $image['msg']);
                }
                $newdata['img'] = $_POST['img'];
            }
            
            $lname = trim($_POST['lname']);
            if (empty($lname))
                $this->error('等级名称没有填写！');
            $newdata['lname'] = $lname;
            $integral = intval($_POST['integral']);
//            if (!($integral > 0))
//                $this->error('等级积分没有填写！');
            $newdata['integral'] = $integral;
            $newdata['boon'] = trim($_POST['boon']);
            $newdata['number'] = intval($_POST['number']);
            if ($lid > 0) {
                $inser_id = $levelDb->where(array('id' => $lid))->save($newdata);
            } else {
                $inser_id = $levelDb->add($newdata);
            }
            if ($inser_id) {
                $this->frame_submit_tips(1,'保存成功！');
            } else {
                $this->frame_submit_tips(0,'保存失败！请重试~');
            }
        } else {
            $lid = intval($_GET['lid']);
            $tmpdata = $levelDb->where(array('id' => $lid))->find();
            $this->assign('leveldata', $tmpdata);
            $this->display();
        }
    }

    /*     * **删除一条导入的记录**** */

    function delimportuser() {
        $idx = (int) trim($_POST['id']);
        $user_importDb = D('User_import');
        if ($user_importDb->where(array('id' => $idx))->delete()) {
            $this->dexit(array('error' => 0));
        } else {
            $this->dexit(array('error' => 1));
        }
    }

    /*     * json 格式封装函数* */

    private function dexit($data = '') {
        if (is_array($data)) {
            echo json_encode($data);
        } else {
            echo $data;
        }
        exit();
    }

}
