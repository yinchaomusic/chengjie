<?php
/*
 * 新闻管理
 */
class NewsAction extends BaseAction{
	public function index(){
//		$database_News_category  = D('News_category');
//		$category_list = $database_News_category->field(true)->order('`cat_id` ASC')->select();
//		$this->assign('category_list',$category_list);
//		$this->display();

		$count = D('News_category')->count();
		import('@.ORG.system_page');

		$p = new Page($count, 20);
		$category_list = D('News_category')->field(true)->order('`sort` DESC,`cat_id` DESC')->limit($p->firstRow.','.$p->listRows)->select();

		$pagebar = $p->show();
		$this->assign('category_list',$category_list);

		$this->assign('pagebar',$pagebar);
		$this->display();

	}

	public function cat_add(){
		$this->assign('bg_color','#F3F3F3');
		$this->display();
	}
	public function cat_modify(){
		if(IS_POST){
			$database_News_category  = D('News_category');
                        $image = D('Image')->handle($this->system_session['id'], 'cat', 0, array('size' => 10), false);
                        if (!$image['error']) {
                                $_POST = array_merge($_POST, str_replace('/upload/cat/', '', $image['url']));
                        } else {
                                $this->frame_submit_tips(0, $image['msg']);
                        }
			if($database_News_category->data($_POST)->add()){
                                $this->frame_submit_tips(1,'添加成功！');
			}else{
                                $this->frame_submit_tips(0, '添加失败！请重试~');
			}
		}else{
			$this->error('非法提交,请重新提交~');
		}
	}
//        新闻分类修改
	public function cat_edit(){
		$this->assign('bg_color','#F3F3F3');
                $news_category = M('news_category');
                $cat_id = intval($_GET['cat_id']);
                $now_category = $news_category->where('`cat_id` = '.$cat_id)->find();
		$this->assign('now_category',$now_category);
		$this->display();
	}
        
	public function cat_amend(){
		if(IS_POST){
//                    print_r($_POST);exit;
			$database_news_category  = D('News_category');
                        if($_FILES['icon']['error'] == 0){
                            $image = D('Image')->handle($this->system_session['id'], 'cat', 0, array('size' => 10), false);
                            if (!$image['error']) {
                                    $_POST = array_merge($_POST, str_replace('/upload/cat/', '', $image['url']));
                            } else {
                                    $this->frame_submit_tips(0, $image['msg']);
                            }
                        }
			if($database_news_category->data($_POST)->save()){
                                $this->frame_submit_tips(1,'编辑成功！');
			}else{
                                $this->frame_submit_tips(0, '编辑失败！请重试~');
			}
		}else{
			$this->error('非法提交,请重新提交~');
		}
	}
        
//	public function cat_del(){
//            $database_news_category  = D('Dews_category');
//            $where['cat_id'] = $_GET['cat_id'];
//            if($database_news_category->where($where)->delete()){
//                    $this->success('删除成功！');
//            }else{
//                    $this->error('删除失败！请重试~');
//            }
//	}
        
	public function news_list(){
		$Db_news = D('News');
		$Db_News_category = D('News_category');
		$count = $Db_news->count();
		import('@.ORG.system_page');
		$p = new Page($count, 15);
		$news_list = $Db_news->field(true)->order('news_id DESC')->limit($p->firstRow.','.$p->listRows)->select();
		foreach($news_list as  $k=>$va){
			$news_list[$k]['cates']= $Db_News_category->field('`cat_name`,`cat_id`,`cat_key`')->where('`cat_id`='.$va['cat_id'])->find();
		}
		$pagebar = $p->show();
		$this->assign('news_list',$news_list);
		$this->assign('pagebar',$pagebar);

		$this->display();
	}


        
	public function news_add(){
		$this->assign('bg_color','#F3F3F3');
                $database_news = M('news');
                $database_news_category = M('News_category');
                if($_POST){
	                $_POST['news_content']  = fulltext_filter($_POST['news_content']);
                    $image = D('Image')->handle($this->system_session['id'], 'news', 0, array('size' => 10), false);
                    if (!$image['error']) {
                            $_POST = array_merge($_POST, str_replace('/upload/news/', '', $image['url']));
                    } else {
                            $this->frame_submit_tips(0, $image['msg']);
                    }
                    $_POST['add_time'] = time();
                    $row = $database_news->add($_POST);
                        if($row){
                            $this->frame_submit_tips(1,'添加成功！');
                        }  else {
                            $this->frame_submit_tips(0,'添加失败！请重试~');
                        }
                }
                $news_cat_list = $database_news_category->select();
                $this->assign('news_cat_list',$news_cat_list);
		$this->display();
	}
        
        public function news_edit(){
		$this->assign('bg_color','#F3F3F3');
		$database_news = M('news');
                $database_news_category  = D('News_category');
                
		$condition_news['news_id'] = $_GET['id'];
		$now_news = $database_news->field(true)->where($condition_news)->find();
//                echo $database_news->getLastSql();
		if(empty($now_news)){
			$this->frame_error_tips('该新闻不存在！');
		}
		$this->assign('now_news',$now_news);
//                dump($now_news['cat_id']);
//                die;
         $news_cat_list = $database_news_category->select();
                
		$this->assign('news_cat_list',$news_cat_list);
//                dump($news_cat_list);
		$this->display();
	}
        
        public function news_edit_data(){
            if($_POST){
                $database_news = M('news');
                unset ($_POST['dosubmit']);
	            $_POST['news_content']  = fulltext_filter($_POST['news_content']);
                $row = $database_news->data($_POST)->save();
//                echo $database_news->getLastSql();
//                dump($_POST);
//                die;
                if($row){
                            $this->frame_submit_tips(1,'修改成功！');
                        }  else {
                            $this->frame_submit_tips(0,'修改失败！请重试~');
                        }
            }
        }




	public function news_del(){
		$database_adver = D('News');
		$condition_adver['news_id'] = intval($_POST['id']);
		$now_adver = $database_adver->field(true)->where($condition_adver)->find();
		if($database_adver->where($condition_adver)->delete()){
			unlink('./upload/adver/'.$now_adver['pic']); 
			S('adver_list_'.$now_adver['cat_id'],NULL);
			$this->success('删除成功');
		}else{
			$this->error('删除失败！请重试~');
		}
	}

}