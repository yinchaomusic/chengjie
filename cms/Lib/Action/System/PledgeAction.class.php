<?php

class PledgeAction extends BaseAction{

	public function index(){
		$Db_pledge_domain = D('Pledge_domain');
		$condition = array();
		
		//只需要显示已经通过域名验证的，才进行审核等操作
		$condition['is_check'] = 1;
		//搜索条件组合
		if (!empty($_GET['keyword'])) {
			if ($_GET['searchtype'] == 'domainName') {
			
				$condition['_string'] =  " `domain` LIKE  '%".strval($_GET['keyword'])."%'";
				 
			}
		}
		$order_list_count = $Db_pledge_domain->where($condition)->count();
		import('@.ORG.system_page');
		$p = new Page($order_list_count, 15);
		$order_list = $Db_pledge_domain->field(true)->order('`apply_time` DESC,`pid` ASC')->where($condition)->limit($p->firstRow . ',' . $p->listRows)->select();
		$this->assign('order_list',$order_list);

		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);
		$this->display();
	}

	public function edit(){
		$condition['pid'] = intval($_GET['pid']);
		$Db_pledge_domain = D('Pledge_domain');
		$now_order = $Db_pledge_domain->field(true)->where($condition)->find();
		if(empty($now_order)){
			$this->frame_error_tips('数据库中没有查询到该内容！',3);
		}
		$this->assign('now_order',$now_order);
		$this->display();
	}

	public function edit_save(){
		$Db_pledge_domain           = D('Pledge_domain');
		$oder_data['intro']        = fulltext_filter($_POST['intro']);
		$oder_data['pledge_status'] = intval($_POST['pledge_status']);
		$oder_data['credit_status'] = intval($_POST['credit_status']);
		$oder_data['pid']           = intval($_POST['pid']);
		if($oder_data['credit_status'] == 1 && $oder_data['pledge_status'] == 1){
		//确认授信,并且已经是转移了域名,才更新时间和授信额度
			$oder_data['credit_time'] = time();
			$oder_data['credit_total']  =trim($_POST['credit_total']);
		}
		if($Db_pledge_domain->data($oder_data)->save()){
			//累计个人授信信息[质押域名个数   授信总额]
			$getdata = $Db_pledge_domain->where('`pid`='.intval($_POST['pid']))->Field('`uid`,`credit_total`')->find();

			$Db_pledges  = M('Pledges');
			if($Db_pledges->where('`uid`='.$getdata['uid'])->find()){ //累加值
				$Db_pledges->where('`uid`='.$getdata['uid'])->setInc('pledge_number',1);
				$Db_pledges->where('`uid`='.$getdata['uid'])->setInc('credit_all',$getdata['credit_total']);
				$Db_pledges->where('`uid`='.$getdata['uid'])->setInc('credit_now',$getdata['credit_total']);
			}else{ //初始化(授信总额 和 可用授信 )是一样的
				$setIncdata['pledge_number'] = 1;
				$setIncdata['credit_all'] = $getdata['credit_total'];
				$setIncdata['credit_now'] = $getdata['credit_total'];
				$setIncdata['uid']       = $getdata['uid'];
				$Db_pledges->data($setIncdata)->add();
			}
			$this->frame_submit_tips(1,'编辑成功！');
		}else{

			$this->frame_submit_tips(0,'编辑失败！');
		}
	}

}