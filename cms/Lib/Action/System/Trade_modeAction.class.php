<?php
/*
 * 域名交易
 * @  交易类型
 * @  Writers     weslee
 * @  BuildTime  2015年12月10日 11:51:16
 *
 */
class Trade_modeAction extends BaseAction
{
	// 域名交易类型列表
	public function index(){
		$database_Domain_trademodel = D('Domain_trademodel');
		$count_Domain_trademodel = $database_Domain_trademodel->count();
		import('@.ORG.system_page');
		$p = new Page($count_Domain_trademodel, 30);
		$treasure_trademodel = $database_Domain_trademodel->field(true)->order('`sorts` DESC,`id` ASC')->limit($p->firstRow . ',' . $p->listRows)->select();

		$this->assign('treasure_trademodel',$treasure_trademodel);
		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);
		$this->display();
	}

	public function add(){
		$this->display();
	}

	//域名交易类型入库
	public function add_save(){
		$database_Domain_suffix = D('Domain_trademodel');
		if($database_Domain_suffix->data($_POST)->add()){
			$this->frame_submit_tips(1,'添加成功！');
		}else{
			$this->frame_submit_tips(0,'添加失败~');
		}
	}

	//域名交易类型编辑
	public function edit(){
		$database_treasure_levels = D('Domain_trademodel');
		$condition_level['id'] = intval($_GET['id']);
		$now_Domain_level = $database_treasure_levels->field(true)->where($condition_level)->find();
		if(empty($now_Domain_level)){
			$this->frame_error_tips('数据库中没有查询到该内容！',3);
		}

		$this->assign('now_Domain_trademodel',$now_Domain_level);
		$this->display();
	}

	//域名交易类型编辑保存
	public function edit_save(){
		$database_Domain_treasure = D('Domain_trademodel');
		if($database_Domain_treasure->data($_POST)->save()){
			$this->frame_submit_tips(1,'编辑成功！');
		}else{

			$this->frame_submit_tips(0,'编辑失败！');
		}
	}

	//域名交易类型删除
	public function del(){
		$id = isset($_POST['id']) ? intval($_POST['id']) : 0;
		if($id>0){
			$database_Domain_suffix = D('Domain_trademodel');
			if($database_Domain_suffix->where(array('id'=>$id))->delete()){
				$this->success('删除成功！');
				exit();
			}else{
				$this->error('删除失败！请重试~');
				exit();
			}
		}
		$this->error('操作失败！请重试~');
	}
}