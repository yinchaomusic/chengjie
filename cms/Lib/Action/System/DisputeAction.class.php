<?php
/*
 * 中介纠纷
 */
class DisputeAction extends BaseAction{
	public function index(){
            $databases_dispute = M('Dispute');
            $count = $databases_dispute->count();
            import('@.ORG.system_page');
            $p = new Page($count, 15);
            $list = $databases_dispute->order('`add_time` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
            $this->assign('list', $list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
	}
        
        public function Dispute_show(){
            $databases_dispute = M('Dispute');
            $show = $databases_dispute->where(array("id"=>  intval($_GET['id'])))->find();
            $this->assign('show',$show);
//            dump($show);
            $this->display();
        }
}