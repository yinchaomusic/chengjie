<?php
/*
 * 优质域名
 *
 */
class BargainAction extends BaseAction{
	public function index(){
		$Db_domains = D("Domains");
		//type =>3 -优质域名
		$domains_condition_youzhi['type'] = '3';
		$domains_condition_youzhi['status'] = '1';
		$count_domains_youzhi = $Db_domains->where($domains_condition_youzhi)->count();

	    import('@.ORG.domain_page');
	    $p = new Page($count_domains_youzhi, 24);
	    $domains_youzhi_list = $Db_domains->field(true)->where($domains_condition_youzhi)->order('`add_time` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
		
		$page = $p->show();
		$this->assign('page',$page);
		$this->assign("count_domains_youzhi",$count_domains_youzhi);
		$this->assign('domains_youzhi_list', $domains_youzhi_list);

		//推荐域名
		$domains_condition_speity['type'] = '3';
		$domains_condition_speity['status']    = '1';
		$domains_condition_speity['domain_id']  = array('egt','(SELECT FLOOR(MAX(`domain_id`)*RAND()) FROM `pigcms_domains`)');
		$domains_speity_list = $Db_domains->field(true)->where($domains_condition_speity)->order('`add_time` DESC, `domain_id` DESC')->limit(10)->select();
		$this->assign('domains_speity_list',$domains_speity_list);
		$this->display();
	}
	public function selling(){
		$domain_id = $_GET['id'];
                $collection_show = $this->is_collection($domain_id);
                $this->assign('collection_show',$collection_show);
		if(empty($domain_id)){
			redirect(U('Buydomains/index'));
		}
		$databaseDomains = D('Domains');
		$now_domain = $databaseDomains->field(true)->where(array('domain_id'=>$domain_id))->find();
		if(empty($now_domain)){
			$this->assign('jumpUrl',U('Buydomains/index'));
			$this->error('当前域名不存在或已被删除');
		}
		if($now_domain['status'] != 1){
			$this->assign('jumpUrl',U('Buydomains/index'));
			$this->error($now_domain['status'] == 0 ? '当前域名正在审核中' : '当前域名已经出售');
		}
		D('Domains')->where(array('domain_id'=>$domain_id))->setInc('hits');
		
		//找到域名的经纪人
		$now_domain_user = D('User')->field('`staff_uid`')->where(array('uid'=>$now_domain['uid']))->find();
		if($now_domain_user['staff_uid']){
			$now_staff = D('Escrow_staff')->field('`qq`,`phone`')->where(array('id'=>$now_domain_user['staff_uid']))->find();
			$this->assign('now_staff',$now_staff);
		}
		
		
		$now_domain['hits'] += 1;
		$now_domain['money'] = rtrim(rtrim($now_domain['money'],'0'),'.');
		$this->assign('now_domain',$now_domain);
		
		
		//感兴趣的域名
		$conditionFavList['styleType'] = $now_domain['styleType'];
		$conditionFavList['status'] = '1';
		$favList = $databaseDomains->field('`domain_id`,`domain`')->where($conditionFavList)->order('`domain_id` DESC')->limit(16)->select();
		$this->assign('favList',$favList);
		
		$this->display();
	}
	public function sold(){
		$Db_domains = D("Domains");
		//type =>3 -优质域名
		$domains_condition_youzhi['type'] = '3';
		$domains_condition_youzhi['status'] = '2';
		$count_domains_youzhi = $Db_domains->where($domains_condition_youzhi)->count();

	    import('@.ORG.domain_page');
	    $p = new Page($count_domains_youzhi, 24);
	    $domains_youzhi_list = $Db_domains->field(true)->where($domains_condition_youzhi)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit($p->firstRow . ',' . $p->listRows)->select();
		
		$page = $p->show();
		$this->assign('page',$page);
		$this->assign("count_domains_youzhi",$count_domains_youzhi);
		$this->assign('domains_youzhi_list', $domains_youzhi_list);

		//推荐域名
		$domains_condition_speity['type'] = '3';
		$domains_condition_speity['status']    = '1';
		$domains_condition_speity['domain_id']  = array('egt','(SELECT FLOOR(MAX(`domain_id`)*RAND()) FROM `pigcms_domains`)');
		$domains_speity_list = $Db_domains->field(true)->where($domains_condition_speity)->order('`add_time` DESC, `domain_id` DESC')->limit(10)->select();
		$this->assign('domains_speity_list',$domains_speity_list);
		
		$this->display();
	}
}