<?php
/*
 * 提交问题
 *
 */
class ProblemAction extends BaseAction{
    
    
    
    protected function _initialize(){
		parent::_initialize();
		if(empty($this->user_session)){
			redirect(U('Login/index'));
		}
                if($this->user_session['staff_uid']){
			$escrow_staff  = M('Escrow_staff');
			$this->staff_show = $escrow_staff->where(array('id'=>$this->user_session['staff_uid']))->find();
			$this->assign('staff_show',$this->staff_show);
			if(IS_GET){
				$staff_list = $escrow_staff->field('`id`,`name`,`qq`')->select();
				$this->assign('staff_list',$staff_list);
			}
		}
	}
        
        
        
	public function index(){
            $problem = M('Problem');
            $where="uid = ".$_SESSION['user']['uid'];
            $count_problem = $problem->where($where)->count();
            import('@.ORG.system_page');
            $p = new Page($count_problem, 10);
            $problem_list = $problem->field(true)->where($where)->order('`id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->assign('problem_list',$problem_list);
            $this->display();
            
	}
        
        public function pb_show(){
            $problem = M('Problem');
            $id = intval($_GET['id']);
            $pbshow = $problem->where('`id` = '.$id)->find();
            $this->assign('pbshow',$pbshow);
            $this->display();
	}
        public function problem_add(){
            $problem = M('Problem');
            if($_POST){
                if($_POST['title']=='请填写您的问题标题' || $_POST['title']==' ')$this->error('请填写您的问题标题',U('Problem/problem_add'));
                $_POST['uid'] = $_SESSION['user']['uid'];
                $_POST['addtime'] = time();
                if($problem->add($_POST)){
                    $this->success('添加成功',U('Problem/index'));exit;
                }  else {
                    $this->error('问题提交失败',U('Problem/problem_add'));exit;
                }
            }
            
            $this->display();
        }
}