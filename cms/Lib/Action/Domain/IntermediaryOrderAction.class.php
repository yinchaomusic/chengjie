<?php

class IntermediaryOrderAction extends BaseAction{
    protected function _initialize(){
        parent::_initialize();
        session_start();
        $static_path   = './tpl/Domain/'.C('DEFAULT_THEME').'/static/';
        $static_public = './static/';
        $this->assign('static_path',$static_path);
        $this->assign('static_public',$static_public);
        $aba = $this->staff_session = session('staff');
        $this->assign('staff_session',$this->staff_session);
        if(ACTION_NAME != "login" && ACTION_NAME != "checkLogin"){
            if($_SESSION['staff']==false){
                $this->error('请先登录',U('Staff/login'));
            }
        }
    }
    
    
    //中介交易
    public  function  listing_buyers(){
        $Web_inter_order = M('Web_inter_order');
        //查询类型  
        $searchtype = $_POST['search_type'];
        $keyword = trim($_POST['keyword']);
        
        if(!empty($keyword)){
            if($searchtype == 'phone'){
                $where = "AND `phone` LIKE '%$keyword%' ";
            }
            if($searchtype == 'uid'){
                 $where .= "AND `user_id` LIKE '%$keyword%' ";
            }
            if($searchtype == 'order_flow'){
                 $where .= "AND `order_flow` LIKE '%$keyword%' ";
            }
        }
        ////买家发起
        $where .= "AND `inter_obj` = '1' ";
        $where .= "AND (`status`= 0 OR `status` = '3')";
        if($where) $where = substr($where, 3);
        $count = $Web_inter_order->where($where)->count();
        import('@.ORG.system_page');
        $p = new Page($count, 15);
        $web_order_list = $Web_inter_order->where($where)->order('`createdate` DESC')->limit($p->firstRow . ',' .
            $p->listRows)->select();
       
        //dump($Web_inter_order->getLastSql());exit;
        $pagebar = $p->show();
        $this->assign('pagebar', $pagebar);
        //dump($not_check_list);
    
        $this->assign('bt_list',$web_order_list);
        $this->display();
    }
    
    
    //网站审核 卖家
    public  function  listing_vendor(){
        $Web_inter_order = M('Web_inter_order');
        //查询类型  
        $searchtype = $_POST['search_type'];
        $keyword = trim($_POST['keyword']);
        
       if(!empty($keyword)){
            if($searchtype == 'phone'){
                $where = "AND `phone` LIKE '%$keyword%' ";
            }
            if($searchtype == 'uid'){
                 $where .= "AND `user_id` LIKE '%$keyword%' ";
            }
            if($searchtype == 'order_flow'){
                 $where .= "AND `order_flow` LIKE '%$keyword%' ";
            }
        }
        ////卖家发起
        $where .= "AND `inter_obj` = 2 ";
        $where .= "AND (`status`= 0 OR `status` = '3')";
        if($where) $where = substr($where, 3);
        $count = $Web_inter_order->where($where)->count();
        import('@.ORG.system_page');
        $p = new Page($count, 15);
        $web_order_list = $Web_inter_order->where($where)->order('`createdate` DESC')->limit($p->firstRow . ',' .
            $p->listRows)->select();
       
        //dump($Web_inter_order->getLastSql());exit;
        $pagebar = $p->show();
        $this->assign('pagebar', $pagebar);
        //dump($not_check_list);
    
        $this->assign('bt_list',$web_order_list);
        $this->display();
    }
    
    
    
    
    //详情
    public function agency_details(){
        $Web_inter_order = M('Web_inter_order');
        $id = intval($_GET['id']);
        $inter_order_info = $Web_inter_order->where(array('id'=>$id))->find();
        $this->assign('info',$inter_order_info);
        $this->display();
    }
    
    
    //介入交易
    public function intervene(){
         $Web_inter_order = M('Web_inter_order');
         $where['id'] = $_POST['id'] ;
         
         $check_list = $Web_inter_order->where( $where)->find();
         if(empty($check_list)){
             echo 2 ; exit ;
         }
         if($check_list['wstatus'] != 0){
             echo 2 ; exit ;
         }
         $data['status'] = 1;  //成功介入
         $data['updatedate'] = date("Y-m-d H:i:s" ,time());
         $id = $_SESSION['staff']['staffUid'];
         $data['check_user_id'] =  $id;
         $result =  $Web_inter_order->where($where)->data($data)->save();
         echo $result ;
    }
    
    //关闭交易
    public function close(){
        $Web_inter_order = M('Web_inter_order');
        $where['id'] = $_POST['id'] ;
        $check_list = $Web_inter_order->where($where)->find();
        if(empty($check_list)){
            echo 2 ; exit ;
        }
        if($check_list['status'] != 0){
            echo 2 ; exit ;
        }
        $data['status'] = 5;  //关闭
        $data['updatedate'] = date("Y-m-d H:i:s" ,time());
        $data['fail_reason'] = $_POST['fail_reason'];
        $result =  $Web_inter_order->where($where)->data($data)->save();
        echo $result ;
    }
    
    
    //划拨资金 
    public function transfer(){
        //order_id
        $Web_inter_order = M('Web_inter_order');
        $databases_user = M('User');
        $where['id'] = intval($_POST['id']);
        $check_list = $Web_inter_order->where($where)->find();
        if($check_list){
            //如果不是已发货状态 退出
            if($check_list['status'] != 3){
                echo 2 ; exit;
            }
            $inter_obj = $check_list['inter_obj'];
            if($inter_obj == 1){
                $buy_id = $check_list['user_id'];
                $sell_id = $check_list['other_user_id'];
            }else{
                $buy_id = $check_list['other_user_id'];
                $sell_id = $check_list['user_id'];
            }
            //买家账户
           $user_buy_info =$databases_user->where(array("uid"=>$buy_id))->find();
           if(!$user_buy_info){
               echo 5 ; exit;
           }
           //卖家用户
           $user_sell_info =$databases_user->where(array("uid"=>$sell_id))->find();
           if($user_sell_info){
               //冻结金额 
               $tradingAmount =sprintf("%.2f", $check_list['price']/100);
               if( $user_sell_info['freeze_money'] <  $tradingAmount){
                   echo 4 ; exit("卖家冻结金额小于交易金");
               }
               $data_user_info['freeze_money']= $user_sell_info['freeze_money'] - $tradingAmount;
               $data_user_info['now_money'] = $user_sell_info['now_money'] + sprintf("%.2f", $check_list['price']/100) ;  
               $result =  $databases_user->where(array("uid"=>$sell_id))->data($data_user_info)->save();
           }else{
               echo 6 ; exit;
           }
        }else{
             echo 7 ; exit;
        }
        $data['status'] = 4;  //已完成
        $data['updatedate'] = date("Y-m-d H:i:s" ,time());
        $result =  $Web_inter_order->where($where)->data($data)->save();
        echo $result ;
    }
    
}

?>