<?php
/**
 *+--------------------------------------------------------------------------------------------------------------
 * | 小猪cms原创开发
 * | 合肥彼岸互联信息技术有限公司 版权所有
 * | 未经商业授权严禁拷贝 派生其它版本进行商业目的
 *+-------------------------------------------------------------------------------------------------------------
  * 经纪人交易控制器
 */
 class StaffdealAction extends BaseAction{
	    protected function _initialize(){
                parent::_initialize();
		 session_start();
		$static_path   = './tpl/Domain/'.C('DEFAULT_THEME').'/static/';
		$static_public = './static/';
		$this->assign('static_path',$static_path);
		$this->assign('static_public',$static_public);
                $aba = $this->staff_session = session('staff');
		$this->assign('staff_session',$this->staff_session);
                if(ACTION_NAME != "login" && ACTION_NAME != "checkLogin"){
                          if($_SESSION['staff']==false){
                                $this->error('请先登录',U('Staff/login'));
                         }
                        }
                  }


                  
                  
//        呼叫经纪人
        public function call(){
            $call_staff = M('Call_staff');
            $count = $call_staff->count();
            import('@.ORG.system_page');
            $p = new Page($count, 15);
//            dump($_SESSION['staff']);
            $call_staff_list = $call_staff->where(array('staff_id'=>$_SESSION['staff']['staffUid']))->order('`id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
//            echo $call_staff->getLastSql();
            $this->assign('call_staff_list', $call_staff_list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }
        
        public function call_confirm(){
            $call_staff = M('call_staff');
            $row = $call_staff->save($_GET);
//            echo $call_staff->getLastSql();
            if($row){
                $this->success('提交成功',U('Staffdeal/call'));
            }  else {
                $this->success('提交失败',U('Staffdeal/call'));
            }
        }

    public function whoischeck(){
        $Db_domains_check = M('Domains_check');
        $Db_user = M('User');
        //$notcheck_con['is_check'] = array('neq',);
       // $not_check_list = $Db_domains_check->where(array('is_check'=>0))->select();

        $count = $Db_domains_check->where(array('is_check'=>0,'check_fail'=>1))->count();
        import('@.ORG.system_page');
        $p = new Page($count, 15);
        $not_check_list = $Db_domains_check->where(array('is_check'=>0,'check_fail'=>1))->order('`add_time` DESC')->limit($p->firstRow . ',' .
            $p->listRows)->select();



        $pagebar = $p->show();
        $this->assign('pagebar', $pagebar);

        //得到为验证域名，如果有这个用户有选择了 经纪人
        $id = $_SESSION['staff']['staffUid'];
        foreach($not_check_list as $k=>&$val){
            $val['nickname'] = $Db_user->where(array('uid'=>$val['uid'],'staff_uid'=>$id))->getField('nickname');
        }

//dump($not_check_list);

        $this->assign('bt_list',$not_check_list);

        $this->display();
    }

    public function domain_ok_check(){
        $Db_domains_check = M('Domains_check');
        $save['is_check'] = intval($_POST['is_check']);
        $save['domain_id'] = intval($_POST['domain_id']);
        $save['check_fail'] = '0';
        $check_list = $Db_domains_check->where(array('domain_id'=>$save['domain_id']))->find();
        if($check_list){

            //添加到domains域名出售列表，议价
            $dataDomains['uid'] = $check_list['uid'];
            $dataDomains['domain'] = $check_list['domain'];
            $dataDomains['group_id'] = $check_list['group_id'];
            $dataDomains['desc'] = $check_list['intro'];
            $dataDomains['money'] = $check_list['price'];
            $dataDomains['add_time'] = $_SERVER['REQUEST_TIME'];

            //找出系统支持的后缀列表
            $suffix_list = D('Domain_suffix')->field('`id`,`suffix`')->where(array('status'=>'1'))->select();
            $suffixArr = array();
            $tmpSuffix = array();
            foreach($suffix_list as $value){
                if(str_replace($value['suffix'],'',$check_list['domain']) != $check_list['domain']){
                    if(empty($tmpSuffix) || strlen($value['suffix']) > strlen($tmpSuffix['suffix'])){
                        $tmpSuffix = $value;
                    }
                }
                $suffixArr[$value['id']] = $value['suffix'];
            }


            $tmpDomain = str_replace($suffixArr,'',$check_list['domain']);
            $dataDomains['main_domain'] = $tmpDomain;

            //通过文件匹配域名类型
            import('@.ORG.domainStyle');
            $domainStyle = new domainStyle();
            $styleResult = $domainStyle->getStyle($tmpDomain);
            $dataDomains['styleType'] = $styleResult['styleType'];
            $dataDomains['label'] = $styleResult['label'];
            $dataDomains['style'] = implode(',',$styleResult['style']);


            $dataDomains['suffix'] = $tmpSuffix['id'];
            $dataDomains['length'] = strlen($tmpDomain);
            $dataDomains['status'] = '1';
            if(D('Domains')->data($dataDomains)->add()){
                $Db_domains_check->where(array('domain_id'=>$save['domain_id']))->delete();
                echo 1;
            }else{
//                $databaseDomainCheck->where(array('domain_id'=$save['domain_id']))->data(array('is_check'=>'0'))->save();
//                $this->error('验证时发生错误');
                echo 2;
            }

        }  else {
            echo 2;
        }
    }


                  //优质域名
                  public function bargain(){
                    $id = $_SESSION['staff']['staffUid'];
                    $where = 'us.staff_uid='.$id.' AND us.uid = do.uid AND do.`type` = 3 AND do.`status` = 0 ';
                    if($_POST){
                       $where .= "AND do.`domain` LIKE '%{$_POST['domain']}%' ";
                    }
//                    dump($_POST);
                    $count =  M()->table(array(C('DB_PREFIX').'user'=>'us',C('DB_PREFIX').'domains'=>'do'))->where($where)->count();

                    import('@.ORG.system_page');
                    $p = new Page($count, 15);
                    $bargain_list = M()->table(array(C('DB_PREFIX').'user'=>'us',C('DB_PREFIX').'domains'=>'do'))->where($where)->order('do.`domain_id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
//                    echo M()->getLastSql();
                    $pagebar = $p->show();
                    $this->assign('pagebar', $pagebar);
                    $this->assign('bargain_list',$bargain_list);
                    $this->display();
                }

                public function bargain_save(){
                    $databases_domains = M('Domains');
                    $info = $databases_domains->save($_GET);
                    if($info){
                        $this->success('审核成功');
                    }  else {
                        $this->error('审核异常，请重试');
                    }
                }




//                  未接待客户
		public function new_client(){
                    $database_user = D('User');

            $searchtype = $_POST['search_type'];
            $keyword = trim($_POST['keyword']);
            if($searchtype == 'name'){

                $where['nickname'] = array('like',"%$keyword%");
            }

            if($searchtype == 'uid'){

                $where['uid'] = array('like',"%$keyword%");
            }


            if($searchtype == 'phone'){

                $where['phone'] = array('like',"%$keyword%");
            }

            if($searchtype == 'email'){

                $where['email'] = array('like',"%$keyword%");
            }


           // $staff_uid = $_SESSION['staff']['staffUid'];

            $where['staff_uid'] = '-1';
            $where['status'] = 1;

                    //$name = $_POST['name'];
                   // if($name)$where = "AND `nickname` LIKE '%$name%' ";
                   // $where .="AND `staff_uid`= -1 AND `status` = 1";
                    //if($where) $where = substr($where, 3);
                    $count_user = $database_user->where($where)->count();
                    import('@.ORG.system_page');
                    $p = new Page($count_user, 15);
                    $user_list = $database_user->field(true)->where($where)->order('`uid` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();

                    $this->assign('user_list', $user_list);
                    $pagebar = $p->show();
                    $this->assign('pagebar', $pagebar);
                    $this->display();
		}
//                我的客户
                public function my_client(){
                    $database_user = D('User');

                    $searchtype = $_POST['search_type'];
                    $keyword = trim($_POST['keyword']);
                    if($searchtype == 'name'){

                        $where['nickname'] = array('like',"%$keyword%");
                    }

                    if($searchtype == 'uid'){

                        $where['uid'] = array('like',"%$keyword%");
                    }


                    if($searchtype == 'phone'){

                        $where['phone'] = array('like',"%$keyword%");
                    }

                    if($searchtype == 'email'){

                        $where['email'] = array('like',"%$keyword%");
                    }


                    $staff_uid = $_SESSION['staff']['staffUid'];
                   // $where .="AND `staff_uid`= ".$staff_uid." AND `status` = 1";
                    $where['staff_uid'] = array('eq',$staff_uid);
                    $where['status'] = 1;
                    //$where['_string'] = " AND `staff_uid`= ".$staff_uid." AND `status` = 1";
                    //if($where) $where = substr($where, 3);
                    $count_user = $database_user->where($where)->count();
                    import('@.ORG.system_page');
                    $p = new Page($count_user, 15);
                    $user_list = $database_user->field(true)->where($where)->order('`uid` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
                    //echo $database_user->getLastSql();

                    $this->assign('user_list', $user_list);
                    $pagebar = $p->show();
                    $this->assign('pagebar', $pagebar);
                    $this->display();
                }

//                我的客户详情
                public function show_user(){
                    if($_POST){
                        $databases_user = M('User');
                        $info = $databases_user->save($_POST);
                        if($info){
                            $this->success('修改成功');
                        }  else {
                            $this->error('修改失败');
                        }
                    }  else {
                        $uid = intval($_GET['uid']);
                        $user = M('User');
                        $show_user = $user->where('`uid` = '.$uid)->find();
                        $this->assign('show_user',$show_user);
                        $this->display();
                    }
                }

//                我的客户的待验证域名列表
                public function dmains_list(){
                    $uid = intval($_GET['uid']);
                    $domains_check = M('Domains_check');
                    $where = '';
                    $where .="AND `uid`= ".$uid;
                    if($where) $where = substr($where, 3);
                    $count_user = $domains_check->where($where)->count();
                    import('@.ORG.system_page');
                    $p = new Page($count_user, 15);
                    $list = $domains_check->field(true)->where($where)->order('`uid` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();

                    $pagebar = $p->show();
                    $this->assign('pagebar', $pagebar);
                    $this->assign('list',$list);
                    $this->display();
                }
//                帮助我的客户删除待验证的域名
                public function dmains_del(){
                    $domain_id = intval($_GET['domain_id']);
                    $domains_check = M('Domains_check');
                    $domains_check->delete($domain_id);
                    $this->success('删除成功',U('Staffdeal/my_client'));
                }

                public function dmains_check_save(){
                    $databaseDomainCheck = D('Domains_check');
                    $conditionDomainCheck['uid'] = $_GET['uid'];
                    $conditionDomainCheck['domain_id'] = $_GET['domain_id'];
                    $nowDomain = $databaseDomainCheck->field(true)->where($conditionDomainCheck)->find();

                    if(empty($nowDomain)){
                            $this->error('当前域名不存在');
                    }
                    if($databaseDomainCheck->where(array('domain_id'=>$_GET['domain_id']))->data(array('is_check'=>'1'))->save()){
					//添加到domains域名出售列表，议价
					$dataDomains['uid'] = $_GET['uid'];
					$dataDomains['domain'] = $nowDomain['domain'];
					$dataDomains['group_id'] = $nowDomain['group_id'];
					$dataDomains['desc'] = $nowDomain['intro'];
					$dataDomains['money'] = $nowDomain['price'];
					$dataDomains['add_time'] = $_SERVER['REQUEST_TIME'];

					//找出系统支持的后缀列表
					$suffix_list = D('Domain_suffix')->field('`id`,`suffix`')->where(array('status'=>'1'))->select();
					$suffixArr = array();
					$tmpSuffix = array();
					foreach($suffix_list as $value){
						if(str_replace($value['suffix'],'',$nowDomain['domain']) != $nowDomain['domain']){
							if(empty($tmpSuffix) || strlen($value['suffix']) > strlen($tmpSuffix['suffix'])){
								$tmpSuffix = $value;
							}
						}
						$suffixArr[$value['id']] = $value['suffix'];
					}


					$tmpDomain = str_replace($suffixArr,'',$nowDomain['domain']);
					$dataDomains['main_domain'] = $tmpDomain;

					//通过文件匹配域名类型
					import('@.ORG.domainStyle');
					$domainStyle = new domainStyle();
					$styleResult = $domainStyle->getStyle($tmpDomain);
					$dataDomains['styleType'] = $styleResult['styleType'];
					$dataDomains['label'] = $styleResult['label'];
					$dataDomains['style'] = implode(',',$styleResult['style']);

					//停止找字典库
					// $nowDomainDict = D('Domain_dicts')->field(true)->where(array('name'=>$tmpDomain))->find();
					// if(!empty($nowDomainDict)){
						// $dataDomains['cat_id'] = $nowDomainDict['tindex'];
						// $dataDomains['cat_fid'] = $nowDomainDict['sindex'];
						// $dataDomains['cat_pid'] = $nowDomainDict['mindex'];
						// $dataDomains['label'] = $nowDomainDict['name'];
					// }else{
						// if(preg_match('/^\d+$/',$tmpDomain)){
							// $dataDomains['cat_pid'] = '1';
							// $dataDomains['label'] = strlen($tmpDomain).'数字';
						// }else if(preg_match('/^[a-z]+$/',$tmpDomain)){
							// $dataDomains['cat_pid'] = '2';
							// $dataDomains['label'] = strlen($tmpDomain).'字母';
						// }else{
							// $dataDomains['cat_pid'] = '3';
							// $dataDomains['label'] = strlen($tmpDomain).'杂';
						// }
					// }
					$dataDomains['suffix'] = $tmpSuffix['id'];
					$dataDomains['length'] = strlen($tmpDomain);
					$dataDomains['status'] = '1';

					if(D('Domains')->data($dataDomains)->add()){
						$databaseDomainCheck->where(array('domain_id'=>$_GET['domain_id']))->delete();
						$this->success('验证成功');
					}else{
						$databaseDomainCheck->where(array('domain_id'=>$_GET['domain_id']))->data(array('is_check'=>'0'))->save();
						$this->error('验证时发生错误');
					}
				}

                }



//                发起新交易
                public function new_deal(){
                    $databases_order = M('Order');
                    if($_POST){
                        $_POST['status']= 1;
                        $_POST['addTime']= time();
                        $_POST['staff_id']= $_SESSION['staff']['staffUid'];
                        $_POST['trade_type']= 1;
                        $row = $databases_order->add($_POST);
                        if($row){
                            $this->success('添加成功',U('Staffdeal/ongoing_deal'));
                        }
                    }  else {
                        $this->display();
                    }
                }

//                
//                Public function mediation_deal(){
////                    $databases_order = M('Order');
////                    $list = $databases_order->where(array('staff_id'=>$_SESSION['staff']['staffUid'],'trade_type'=>1))->select();
//                    
//                    $order = D('Order');
//                    $where = '';
////                    $domainName = $_POST['domainName'];
////                    if($domainName)$where = "AND `domainName` LIKE '%$domainName%' ";
//                    $staff_id = $_SESSION['staff']['staffUid'];
//                    $where .="AND `staff_id`= ".$staff_id." AND `trade_type` = 1";
//                    if($where) $where = substr($where, 3);
//                    $count_order = $order->where($where)->count();
//                    import('@.ORG.system_page');
//                    $p = new Page($count_order, 15);
//                    $order_list = $order->where($where)->order('`order_id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
//                    $this->assign('order_list', $order_list);
//                    dump($order_list);
//                    $pagebar = $p->show();
//                    $this->assign('pagebar', $pagebar);
//                    $this->display();
//                    
//                }
                
                
                
                
//                进行中的交易
                public function ongoing_deal(){
                    $order = D('Order');
                    $where = '';
                    $domainName = $_POST['domainName'];
                    if($domainName)$where = "AND `domainName` LIKE '%$domainName%' ";
                    $staff_id = $_SESSION['staff']['staffUid'];
                    $where .="AND `staff_id`= ".$staff_id." AND `status` = 1";
                    if($where) $where = substr($where, 3);
                    $count_order = $order->where($where)->count();
                    import('@.ORG.system_page');
                    $p = new Page($count_order, 15);
                    $order_list = $order->where($where)->order('`order_id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();

                    $this->assign('order_list', $order_list);
                    $pagebar = $p->show();
                    $this->assign('pagebar', $pagebar);
                    $this->display();
                }

                public function operate(){
                    $databases_order = M('Order');
                    $databases_order_info = M('Order_info');


                    $order_info['order_id'] = $_GET['order_id'];
                    $order_info['info'] = "经济人确认卖家以转移域名";
                    $order_info['time'] = time();


                    $info = $databases_order->save($_GET);
                    if($info){
                        $databases_order_info->add($order_info);
                        $this->success('状态变更成功');
                    }  else {
                        $this->error('状态变更失败');
                    }
                }


                //经纪人关闭正在进行中的交易
                public function ongoing_voer(){
                    $databases_order = M('Order');
                    $databases_order_info = M('Order_info');
                    $data['order_id'] = $_POST['order_id'];
                    $data['status'] = 3;
                    $data['yes_no'] = 8;
                    $order_info['order_id'] = $_POST['order_id'];
                    $order_info['info'] = "经济人关闭此次交易";
                    $order_info['time'] = time();
                    $over_id = $databases_order->save($data);
                    if($over_id){
                        $databases_order_info->add($order_info);
                        echo 1;
                    }  else {
                        echo 2;
                    }
                }

                public function dealdetails(){
                    $order_id = intval($_GET['order_id']);
                    $databases_order = M('Order');
                    $databases_order_info = M('Order_info');
                    $order_info = $databases_order->where(array('order_id'=>$order_id))->find();
                    $order_info_list = $databases_order_info->where(array('order_id'=>$order_id))->order('id DESC')->select();

                    $this->assign('order_info',$order_info);
                    $this->assign('order_info_list',$order_info_list);

                    $this->display();
                }

//                已完成的交易
                public function over_deal(){
                    $databases_order = D('Order');
                    $where = '';
                    $domainName = $_POST['domainName'];
                    if($domainName)$where = "AND `domainName` LIKE '%$domainName%' ";
                    $staff_uid = $_SESSION['staff']['staffUid'];
                    $where .="AND `staff_id`= ".$staff_uid." AND `status` = 2";
                    if($where) $where = substr($where, 3);
                    $count_order = $databases_order->where($where)->count();

                    import('@.ORG.system_page');
                    $p = new Page($count_order, 15);
                    $order_list = $databases_order->field(true)->where($where)->order('`order_id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
//                    echo $databases_order->getLastSql();
                    $this->assign('order_list', $order_list);
                    $pagebar = $p->show();
                    $this->assign('pagebar', $pagebar);
                    $this->display();
                }

//                代购列表
                public function purchase_list(){
                    $purchase = M('Purchase');
                    $staff_uid = $_SESSION['staff'][staffUid];
                    $where .="AND `staff_id`= ".$staff_uid.' OR staff_id = -1';
                    if($where) $where = substr($where, 3);
                    $purchase_list = $purchase->where($where)->order('ph_id DESC')->select();
                    $this->assign('purchase_list',$purchase_list);
//                    dump($purchase_list);
                    $this->display();
                }
                //代购审核
                public function purchase_audit(){
                    $databases_purchase = M('Purchase');
                    $info = $databases_purchase->save($_POST);
                    if($info){
                        echo 1;
                    }else{
                        echo 2;
                    }
                }
                //代购记录详情
                public function purchase_show(){
                    $databases_purchase = M('Purchase');
                    $info = $databases_purchase->where(array('ph_id'=>$_GET['ph_id']))->find();
                    $this->assign('info',$info);
//                    dump($info);
                    $this->display();
                }

                //卖家同意出售域名
                public function purchase_sell_agree(){
                    $databases_purchase = M('Purchase');
                    $info = $databases_purchase->save($_POST);
                    if($info){
                        echo 1;
                    }  else {
                        echo 2;
                    }
                }

                //卖家不同意出售域名
                public function purchase_sell_refused(){
                    $databases_purchase = M('Purchase');
                    $databases_user = M('User');
                    $databases_user_freeze = M('User_freeze');
                    $info = $databases_purchase->save($_POST);
                    if($info){

                        $purchase_info = $databases_purchase->where(array('ph_id'=>  intval($_POST['ph_id'])))->find();
                        $user_freeze_info = $databases_user_freeze->where(array('id'=>$purchase_info['freeze_id']))->find();
                        $user_info = $databases_user->where(array('uid'=>$purchase_info['uid']))->setInc('now_money',$user_freeze_info['freezemoney']);
                        if($user_info){
                            $databases_user_freeze->setField('status',1);
                            echo 1;die;
                        }  else {
                            echo 2;die;
                        }
                    }  else {
                        echo 2;die;
                    }
                }


                //经纪人确定卖家以发货
                public function purchase_sell_delivery(){
                    $databases_purchase = M('Purchase');
                    $info = $databases_purchase->save($_POST);
                    if($info){
                        echo 1;
                    }  else {
                        echo 2;
                    }
                }


                //通过代购订单接入未知客户
                public function reception_purchase(){
                    $databases_purchase = M('Purchase');
                    $databases_user = M('User');
                    $_GET['staff_id'] = $_SESSION['staff']['staffUid'];
                    $data['uid'] = $_GET['uid'];
                    $data['staff_uid'] = $_SESSION['staff']['staffUid'];
                    $info = $databases_user->save($data);
//                    echo $databases_user->getLastSql();
//                    die;
                    if($info){
                        $row = $databases_purchase->save($_GET);
                        if($row){
                            $this->success('接入成功');
                        }else{
                            $this->error('失败');
                        }
                    }  else {
                        $this->error('失败');
                    }
                }



//                接入新客户
                public function intervene(){
                    $database_user = D('User');
                    $data_user['uid'] = $_GET['uid'];
                    $data_user['staff_uid'] = $_SESSION['staff']['staffUid'];
                    if($database_user->data($data_user)->save()){
                        $this->success('接入成功',U('Staffdeal/my_client'));
                    }else{
                        $this->error('接入失败');
                    }


                }






                public function bulk_trading(){
                    $bulk_trading = M('Bulk_trading');
                    $user = M('User');
                    $id = $_SESSION['staff']['staffUid'];
                    $bt_list = M()->table(array(C('DB_PREFIX').'user'=>'us',C('DB_PREFIX').'bulk_trading'=>'bt'))
                            ->field('us.nickname,us.uid,bt.bulk_id,bt.domain_list,bt.title,bt.total_price,bt.amount,bt.meaning,bt.add_time,bt.status')
                            ->where('us.staff_uid='.$id.' AND us.uid = bt.uid')->order('`bulk_id` DESC')->select();
//                    echo M()->getLastSql();
                    $this->assign('bt_list',$bt_list);
                    $this->display();

                }

//                客户批量交易列表
                public function bt_list(){
                    $bulk_id = intval($_GET['bulk_id']);
                    $bulk_trading = M('Bulk_trading');
                    $domain_list = $bulk_trading->where('`bulk_id` = '.$bulk_id)->find();
                    $domain_list['domain_list'] = unserialize($domain_list['domain_list']);
                  // dump($domain_list['domain_list']);
 $jsondata =  json_encode($domain_list['domain_list']);
 $check_uid = json_encode($domain_list['uid']);
 $this->assign('jsondata',$jsondata);
  $this->assign('jsonwhoid',$check_uid);

                  //  dump($domain_list['uid']);
                    //验证域名是否是本人的 whois
                  // 先去查询 cache_search_domains 表里是否有这个域名，并且whois邮箱是对的
                    $this->assign('domain_list',$domain_list);
//                    dump($domain_list);
                    $this->display();

                }

                public function whois(){
                  if(IS_POST){
                   // file_put_contents('00000000000000000000000000000000000.log',print_r($_POST,true));
                    $check_domains  = $_POST['arr_yms'];
              			$count = count($check_domains);
                    $uid = intval($_POST['whoid']);
              			$result_list = array();
                   // import("@.Action.WhoisAction");
                    $Db_cache_search_domains = D('Cache_search_domains');
                    $Db_domain_whoisemail = D('Domain_whoisemail');
              			for($i = 0; $i < $count ;$i++){
                    // echo $i.'==>'.$check_domains[$i].'---';
                     $keyword = $check_domains[$i];
                     $get_reg_email = $Db_cache_search_domains->field('`cache_id`,`domainName`,`registrarEmail`')->where("`domainName`='{$keyword}'")->find();
                     if($get_reg_email['registrarEmail']){ //系统已经获取信息
                          $lowemail = strtolower($get_reg_email['registrarEmail']);
                          $is_uid_email =   $Db_domain_whoisemail->field('`is_check`')->where("`uid` = $uid AND `email` = '{$lowemail}'   AND `is_check` = 1")->find();
                          if($is_uid_email){ //whois 邮箱已经验证是本人
                                $result_list[$i]  = 1;
                          }else{
                               $result_list[$i]  = 0;
                          }
                          //echo  $i.'==>'.$result_list[$i].'---';
                     }else{
                        $result_list[$i]  = 2;

                     } // TODO 需要重新获取whois信息
                    //  WhoisAction::index('True');
                      //dump($result_list);
              			}

              			echo json_encode($result_list);die;

                  }else{
                    echo "";
                  }
                }

                public function bt_check(){
                    $bulk_trading = M('Bulk_trading');
                    if($bulk_trading->save($_POST)){
                        echo 1;
                    }  else {
                        echo 2;
                    }
                }




//                终端域名审核 - 审核列表页面
                public function terminal(){
                    $databases_terminal = M('Terminal');
                    $list = $databases_terminal->where(array('staff_id'=>$_SESSION['staff']['staffUid'],"status"=>0))->order('id DESC')->select();
//                    echo $databases_terminal->getLastSql();
                    $this->assign('list',$list);
//                    dump($list);
                    $this->display();
                }

//                终端域名审核 - 审核详情页面
                public function terminal_show(){
                    $id = $_GET['id'];
                    $databases_terminal = M('Terminal');
                    $show = $databases_terminal->where(array('id'=>$id))->find();
                    $this->assign('show',$show);
                    $this->display();
                }
//                终端域名审核 - 通过审核
                public function pass(){
                    $databases_terminal = M('Terminal');
                    $info  = $databases_terminal->where(array("id"=>$_POST['id']))->setField("status",1);
                    if($info){
                        echo 1;
                    }else{
                        echo 2;
                    }
                }
//                终端域名审核 - 拒绝审核
                public function refuse(){
                    $databases_terminal = M('Terminal');
                    $data['id'] = intval($_POST['id']);
                    $data['status'] = 2;
                    $data['reason'] = $_POST['reason'];
                    $info  = $databases_terminal->save($data);
                    if($info){
                        echo 1;
                    }else{
                        echo 2;
                    }
                }

//        推荐中
        public function ongoing(){
            $databases_terminal = M('Terminal');
            $list = $databases_terminal->where(array('staff_id'=>$_SESSION['staff']['staffUid'],"status"=>1))->select();
            $this->assign('list',$list);
            $this->display();
        }
//        推荐成功
        public function succeed(){
            $databases_terminal = M('Terminal');
            $list = $databases_terminal->where(array('staff_id'=>$_SESSION['staff']['staffUid'],"status"=>3))->select();
//            echo $databases_terminal->getLastSql();
            $this->assign('list',$list);
//            dump($list);
            $this->display();
        }
//        推荐失败
        public function failure(){
            $databases_terminal = M('Terminal');
            $list = $databases_terminal->where(array('staff_id'=>$_SESSION['staff']['staffUid'],"status"=>4))->select();
            $this->assign('list',$list);
//            dump($list);
            $this->display();
        }

        //交易纠纷

        public function disputes_deal(){
            $databases_dispute = M('Dispute');
            $count = $databases_dispute->where(array("staff_id"=>$_SESSION['staff']['staffUid']))->count();
            import('@.ORG.system_page');
            $p = new Page($count, 15);
            $list = $databases_dispute->where(array("staff_id"=>$_SESSION['staff']['staffUid']))->order('`id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
            $this->assign('list', $list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }

         public function add_dispute(){
            $databases_dispute = M('Dispute');
            if($_POST){
                if($_POST['id'] < 1){
                    $_POST['staff_id'] = $_SESSION['staff']['staffUid'];
                    $_POST['add_time'] = time();
                    unset ($_POST['id']);
                    $info = $databases_dispute->add($_POST);
                    if($info){
                        $this->success('添加成功');die;
                    }else{
                        $this->error('失败请重试');die;
                    }

                }  else {
                    $info = $databases_dispute->save($_POST);
                    if($info){
                        $this->success('修改成功',U('Staffdeal/disputes_deal'));die;
                    }else{
                        $this->error('失败请重试');die;
                    }
                }
            }  else {
                $id = $_GET['id'];
                $save_show = $databases_dispute->where(array("id"=>  intval($_GET['id'])))->find();
                $this->assign('save_show',$save_show);
                $this->display();
            }
        }

        public function dispute_delete(){
            $databases_dispute = M('Dispute');
            if($databases_dispute->where(array("id"=>  intval($_GET['id']),"staff_id"=>$_SESSION['staff']['staffUid']))->delete()){
                $this->success('删除成功');
            }  else {
                $this->error('删除失败请重试');
            }
        }




	}


?>
