<?php
/*
 * 米表
 *
 */
class MibiaoAction extends BaseAction{
	public function index(){
		$uid = intval($_GET['mmid']);
		if($uid < 1){
			redirect(U('Index/index'));
		}
		
		//域名后缀
		$databaseDomainSuffix = D('Domain_suffix');
		$conditionDomainSuffix['is_hots'] = '1';
		$suffixList = $databaseDomainSuffix->field('`id`,`suffix`')->where($conditionDomainSuffix)->order('`sorts` DESC,`id` ASC')->select();
		$this->assign('suffixList',$suffixList);
		
		//主题属性
		$databaseDomainTreasure = D('Domain_treasure');
		$conditionDomainTreasure['status'] = '1';
		$treasureList = $databaseDomainTreasure->field(true)->where($conditionDomainTreasure)->order('`sorts` DESC,`id` ASC')->select();
		$this->assign('treasureList',$treasureList);
		
		$Db_domains = D("Domains");
		$userinfo = D('Mibiao')->where(array('uid'=>$uid))->find();
		if(empty($userinfo)){
			$this->assign('jumpUrl',$_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : U('Index/index'));
			$this->error('该用户没有开通米表功能');
		}
		$domains_condition['status'] = '1';
		$domains_condition['uid'] = $uid;
		$count_domains = $Db_domains->where($domains_condition)->count();
		// dump($Db_domains);
		import('@.ORG.domain_page');
		$p = new Page($count_domains, 24);
		$domains_list = $Db_domains->field(true)->where($domains_condition)->order('`add_time` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();

		$page = $p->show();
		$this->assign('page',$page);
		$this->assign("count_domains",$count_domains);
		$this->assign('domains_list', $domains_list);
		$this->assign('userinfo', $userinfo);

		$this->display();
	}

}