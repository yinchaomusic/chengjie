<?php
/*
 * 首页
 *
 */
class IndexAction extends BaseAction {
    public function index(){
        
	    $database_news = D('news');
	    $news_list = $database_news->where('`cat_id`=1')->order('news_id DESC')->limit('0,10')->select();
	    $this->assign('cat_id',1);
                
	    $this->assign('news_list',$news_list);

		$flink_list = D('Flink')->get_flink_list();
		$this->assign('flink_list',$flink_list);

	    //首页自动收缩图，按照图片添加的先后时间来排序，最先添加的是小图
	    $Db_adver_category = D('adver_category');
	    $adver_cagegory =  $Db_adver_category->where("`cat_key`= 'domain_index_top'")->find();
	    $adverList = D('Adver')->where(array('cat_id'=>$adver_cagegory['cat_id'],'status'=>1))->order('`id` ASC')->select();
        $this->assign('adverList',$adverList);

		$Db_domains = D("Domains");
		//首页只显示【type】 3 -优质域名 ，1 - 一口价域名 ，0 -议价域名
	    //优质域名
	    $domains_condition_youzhi['type'] = 3;
	    $domains_condition_youzhi['status'] = 1;

	   // $count_domains_youzhi = $Db_domains->where($domains_condition_youzhi)->count();

	    //import('@.ORG.system_page');
	   // $p = new Page($count_domains_youzhi, 24);
	    //$domains_youzhi_list = $Db_domains->field(true)->where($domains_condition_youzhi)->order('`add_time` DESC,`is_hot` DESC,`domain_id` //ASC')->limi  ($p->firstRow . ',' . $p->listRows)->select();t

 $domains_youzhi_list = $Db_domains->field(true)->where($domains_condition_youzhi)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')
		    ->limit(24)->select();
$this->assign('domains_youzhi_list', $domains_youzhi_list);

	    $YouWeb = M();
	    $web_youzhi_list = $YouWeb->query("SELECT a.id,a.description,a.price,a.entr_type FROM pigcms_web_entrust a
	        ,pigcms_web_high b WHERE a.id=b.web_id ORDER by b.createdate DESC limit 0,24");
	    $this->assign('web_youzhi_list', $web_youzhi_list);

        
		//一口价域名
	    $domains_condition_ykj['type'] = 1;
	    $domains_condition_ykj['status'] = 1;
	   // $count_domains_ykj = $Db_domains->where($domains_condition_ykj)->count();
	   // $p = new Page($count_domains_ykj, 24);
	    //$domains_ykj_list = $Db_domains->field(true)->where($domains_condition_ykj)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit($p->firstRow . ',' . $p->listRows)->select();
	    $domains_ykj_list = $Db_domains->field(true)->where($domains_condition_ykj)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit
	    (24)->select();
	    $this->assign('domains_ykj_list', $domains_ykj_list);


		//批量域名
	    $domains_condition_Pldomain['type'] = 2;
	    $domains_condition_Pldomain['status'] = 1;
	   // $count_domains_ykj = $Db_domains->where($domains_condition_ykj)->count();
	   // $p = new Page($count_domains_ykj, 24);
	    //$domains_ykj_list = $Db_domains->field(true)->where($domains_condition_ykj)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit($p->firstRow . ',' . $p->listRows)->select();
	    $domains_Pldomain_list = $Db_domains->field(true)->where($domains_condition_Pldomain)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit
	    (24)->select();
	    $this->assign('domains_Pldomain_list', $domains_Pldomain_list);


		//0 -议价域名
	    $domains_condition_yj['type'] = 0;
	    $domains_condition_yj['status'] = 1;
	    $domains_yj_list = $Db_domains->field(true)->where($domains_condition_yj)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit
	    (12)->select();
	    $this->assign('domains_yj_list', $domains_yj_list);

	    //最近成交 6
	   // $domains_condition_yj['type'] = 0;
	    $domains_condition_news['status'] = 2;
	    $domains_news_list = $Db_domains->field('`domain`,`desc`')->where($domains_condition_news)->order('`is_high` DESC,`is_hot` DESC,`is_speity`
	    DESC,
	    `domain_id`
	    ASC')->limit
	    (6)->select();
	    $this->assign('domains_news_list', $domains_news_list);

	    //右侧上top新闻 pigcms_news_category ==》cat_key ---  index_news_top1
		$Db_News          = M('News');
	    $Db_news_category = M('News_category');
		$news_cate_condition['cat_key'] = 'index_news_top1';
		$cate_list = $Db_news_category->field('`cat_id`,`cat_key`,`cat_name`')->where($news_cate_condition)->find();

		$news_list_condition['cat_id'] = $cate_list['cat_id'];
	    $news_list_condition['state'] = 1;
	    $news_list = $Db_News->field('`news_id`,`news_title`,`cat_id`')->where($news_list_condition)->order('`add_time` DESC,`news_id` ASC ')
		    ->limit(5)
			    ->select();
		//dump($news_list);
	    $this->assign('cate_list',$cate_list);
		$this->assign('news_list',$news_list);

	    //右侧下top新闻 pigcms_news_category ==》cat_key ---  notice
	    $Xnews_cate_condition['cat_key'] = 'notice';
	    $Xcate_list = $Db_news_category->field('`cat_id`,`cat_key`,`cat_name`')->where($Xnews_cate_condition)->find();

	    $Xnews_list_condition['cat_id'] = $Xcate_list['cat_id'];
	    $Xnews_list_condition['state'] = 1;
	    $Xnews_list = $Db_News->field('`news_id`,`news_title`,`cat_id`')->where($Xnews_list_condition)->order('`add_time` DESC,`news_id` ASC ')
		    ->limit(8)
		    ->select();
	    //dump($news_list);
	    $this->assign('Xcate_list',$Xcate_list);
	    $this->assign('Xnews_list',$Xnews_list);

	    //经典案例
	    $case_condition['cat_key'] = 'index_cases';
	    $Db_adver_category = M('Adver_category');
		$get_cat_key = $Db_adver_category->field('`cat_id`,`cat_key`')->where($case_condition)->find();
	    $Db_adver  = M('Adver');
	    $adver_condition['cat_id'] =  $get_cat_key['cat_id'];
	    $adver_condition['status'] =  1;
	    $adver_list = $Db_adver->field(true)->where($adver_condition)->order('`last_time` DESC,`id` ASC')->select();
	    $this->assign('adver_list',$adver_list);


        if(empty($web_youzhi_list) || $web_youzhi_list=='') {
		   $this->display();
		} else {
		   $this->display('index-web');
		}
	    
    }
    
    
    
	
	public function group_index_sort(){
		$group_id = $_POST['id'];
		$database_index_group_hits = D('Index_group_hits_'.substr(dechex($group_id),-1));
		$data_index_group_hits['group_id'] = $group_id;
		$data_index_group_hits['ip']		= get_client_ip();
		if(!$database_index_group_hits->field('`group_id`')->where($data_index_group_hits)->find()){
			$condition_group['group_id'] = $group_id;
			if(M('Group')->where($condition_group)->setDec('index_sort')){
				$data_index_group_hits['time'] = $_SERVER['REQUEST_TIME'];
				$database_index_group_hits->data($data_index_group_hits)->add();
			}
		}
	}
}