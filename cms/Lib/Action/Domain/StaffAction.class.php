<?php
/**
 *+--------------------------------------------------------------------------------------------------------------
 * | 小猪cms原创开发
 * | 合肥彼岸互联信息技术有限公司 版权所有
 * | 未经商业授权严禁拷贝 派生其它版本进行商业目的
 *+--------------------------------------------------------------------------------------------------------------
 * | Author : 猪哥
 * | E-mail : cmsno1@163.com
 * | web-QQ : 800022936
 * | Website: www.webcms.cn
 *+--------------------------------------------------------------------------------------------------------------
  * 登陆控制器
 */
 class StaffAction extends BaseAction{
	    protected function _initialize(){
                parent::_initialize();
		 session_start();
		$static_path   = './tpl/Domain/'.C('DEFAULT_THEME').'/static/';
		$static_public = './static/';
		$this->assign('static_path',$static_path);
		$this->assign('static_public',$static_public);
     
    $aba = $this->staff_session = session('staff');
		$this->assign('staff_session',$this->staff_session);
                if(ACTION_NAME != "login" && ACTION_NAME != "checkLogin"){
                          if($_SESSION['staff']==false){
                                $this->error('请先登录',U('Staff/login'));
                         }
                        }
                  }
		public function index(){
                    $databases_user = M('User');
                    $user_count = $databases_user->where(array('staff_id'=>$_SESSION['staff']['staffUid']))->count();
                    $this->assign('user_count',$user_count);

                    $databases_order  = M('Order');
                    $order_sum['total_sum']  = $databases_order->where(array('staff_id'=>$_SESSION['staff']['staffUid']))->count();
                    $order_sum['succeed_sum']  = $databases_order->where(array('staff_id'=>$_SESSION['staff']['staffUid'],'status'=>2))->count();
                    $order_sum['failure_sum']  = $databases_order->where(array('staff_id'=>$_SESSION['staff']['staffUid'],'status'=>3))->count();
                    $this->assign('order_sum',$order_sum);



                        $Escrow_staff =  M('Escrow_staff');
			$staff=$Escrow_staff->where(array('id'=>$_SESSION['staff']['staffUid']))->find();
//                        echo $Escrow_staff->getLastSql();
//                        dump($staff);
			import('ORG.Net.IpLocation');// 导入IpLocation类
			$Ip = new IpLocation('UTFWry.dat'); // 实例化类 参数表示IP地址库文件
			// 获取某个IP地址所在的位置
			$city=$Ip->getlocation($staff['lastIp']);;
			$staff['lastCity']=iconv("GBK", "UTF-8", $city['area']);
			$this->assign('staff',$staff);
		//	dump($staff);

                        $order = D('Order');
                        $staff_uid = $_SESSION['staff']['staffUid'];
                        $where ="`staff_id`= ".$staff_uid." AND `status` = 1";
                        $domains_list = $order->where($where)->order('`order_id` DESC')->limit('0,5')->select();
//                        dump($domains_list);
//                        echo $order->getLastSql();
                        $this->assign('domains_list', $domains_list);
			$this->display();
		}
		public function login(){
                    if($_SESSION['staff']!=false){
                            $this->error('以登录',U('Staff/index'));
                     }
                    $this->display();
		}

		public function checkLogin(){
			if(IS_POST){
				$data['userName']=$this->_post('uname','htmlspecialchars');
				$data['pwd']=md5($this->_post('password','htmlspecialchars'));

				$adminDB=M('Escrow_staff');
				$adminInfo=$adminDB->where($data)->find();
                                if($adminInfo['state'] == "0")$this->error('此帐号异常暂不能登录');
				//dump($adminDB->getLastSql());exit;
				if($adminInfo!=false){
					$where['nowTime']=time();
					$where['nowIp']=get_client_ip();
					$where['lastIp']=$adminInfo['nowIp'];
					$where['lastTime']=$adminInfo['nowTime'];
					$where['id']=$adminInfo['id'];
					$adminDb2=$adminDB->data($where)->save();
					if($adminDb2!=false){
                                            $_SESSION['staff']['phone'] = $adminInfo['phone'];
                                            $_SESSION['staff']['email'] = $adminInfo['email'];
                                            $_SESSION['staff']['staffUid'] = $adminInfo['id'];
                                            $_SESSION['staff']['userName'] = $adminInfo['userName'];
                                            $_SESSION['staff']['name'] = $adminInfo['name'];
                                            $_SESSION['staff']['qq'] = $adminInfo['qq'];
                                                $this->success('登陆成功',U('Staff/index'));
					}
				}else{
					$this->error('帐号或者密码不正确');
				}
			}else{
				//dump( $_SERVER["HTTP_REFERER"]);
				$this->display();
			}
		}
		//交易信息
		public function escrow($state=1){

			$dataBase=D('Escrow_lis');
			$data=$dataBase->where(array('state'=>$state,'staffId'=>session('staffUid')))->select();
			$this->assign($data);

		}
		public function logout() {
                    session('staff',null);
                    header('Location: '.U('Staff/login'));
		}
		public function verify(){
			Image::buildImageVerify(4,1,'gif',80,36,'checkcode');
		}




	}


?>
