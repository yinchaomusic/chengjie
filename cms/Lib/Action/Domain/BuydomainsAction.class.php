<?php
/*
 * 购买 域名
 *
 */
class BuydomainsAction extends BaseAction{
	public function index(){
		//域名后缀
		$databaseDomainSuffix = D('Domain_suffix');
		$conditionDomainSuffix['is_hots'] = '1';
		$suffixList = $databaseDomainSuffix->field('`id`,`suffix`')->where($conditionDomainSuffix)->order('`sorts` DESC,`id` ASC')->select();
		$this->assign('suffixList',$suffixList);
		
		//主题属性
		$databaseDomainTreasure = D('Domain_treasure');
		$conditionDomainTreasure['status'] = '1';
		$treasureList = $databaseDomainTreasure->field(true)->where($conditionDomainTreasure)->order('`sorts` DESC,`id` ASC')->select();
		$this->assign('treasureList',$treasureList);
		
		$databaseDomains = D('Domains');
		$conditionDomains['status'] = '1';
		if($_GET['keyword']){
			if(strpos($_GET['keyword'],'.')){
				$conditionDomains['domain'] = array('LIKE','%'.$_GET['keyword'].'%');
			}else{
				$conditionDomains['main_domain'] = array('LIKE','%'.$_GET['keyword'].'%');
			}
		}
		$allCount = $databaseDomains->where($conditionDomains)->count();
		$allPage = ceil($allCount/20);
		$domainList = $databaseDomains->field('`domain_id`,`domain`,`label`,`money`,`type`')->where($conditionDomains)->order('`add_time` DESC')->limit(20)->select();
		foreach($domainList as &$domainValue){
			$domainValue['money'] = floatval($domainValue['money']);
			switch($domainValue['type']){
				case '1':
					$domainValue['url'] = U('Hotsale/selling',array('id'=>$domainValue['domain_id']));
					break;
				case '2':
					$domainValue['url'] = U('Fastbid/detail',array('id'=>$domainValue['domain_id']));
					break;
				default:
					$domainValue['url'] = U('Buydomains/detail',array('id'=>$domainValue['domain_id']));
			}
		}
		$this->assign('allCount',$allCount);
		$this->assign('allPage',$allPage);
		$this->assign('domainList',$domainList);
		import('@.ORG.domain_page');
		$pageClass = new Page($allCount,20);
		$page = $pageClass->show();
		$this->assign('page',$page);
		
		//推荐域名
		$domains_condition_speity['type'] = '0';
		$domains_condition_speity['status']    = '1';
		$domains_condition_speity['domain_id']  = array('egt','(SELECT FLOOR(MAX(`domain_id`)*RAND()) FROM `pigcms_domains`)');
		$domains_speity_list = $databaseDomains->field(true)->where($domains_condition_speity)->order('`add_time` DESC, `domain_id` DESC')->limit(10)->select();
		$this->assign('domains_speity_list',$domains_speity_list);		
		
		$this->display();
	}
	public function pagedomins(){
		$databaseDomains = D('Domains');
		$conditionDomains['status'] = '1';
		
		//域名类别
		$pageRows = 20;
		if($_POST['type']){
			if($_POST['type'] != 4){
				$conditionDomains['type'] = $_POST['type'];
			}
			switch($_POST['type']){
				case '3':
				case '1':
					$pageRows = 24;
					break;
			}
		}
		//域名关键词
		if($_POST['input_val_one'] != ''){
			switch($_POST['select_one']){
				case '0':
					$main_domain_one = array('LIKE','%'.$_POST['input_val_one'].'%');
					break;
				case '1':
					$main_domain_one = array('LIKE',$_POST['input_val_one'].'%');
					break;
				case '2':
					$main_domain_one = array('LIKE','%'.$_POST['input_val_one']);
					break;
			}
		}
		
		//域名关键词
		if($_POST['input_val_two'] != ''){
			switch($_POST['select_two']){
				case '0':
					$main_domain_two = array('NOTLIKE','%'.$_POST['input_val_two'].'%');
					break;
				case '1':
					$main_domain_two = array('NOTLIKE',$_POST['input_val_two'].'%');
					break;
				case '2':
					$main_domain_two = array('NOTLIKE','%'.$_POST['input_val_two']);
					break;
			}
		}
		
		if($main_domain_one && $main_domain_two){
			$conditionDomains['main_domain'] = array($main_domain_one,$main_domain_two);
		}else if($main_domain_one){
			$conditionDomains['main_domain'] = $main_domain_one;
		}else if($main_domain_two){
			$conditionDomains['main_domain'] = $main_domain_two;
		}
		//后缀
		if($_POST['hznum'] !== ''){
			if(strpos($_POST['hznum'],'other') !== false){
				$_POST['hznum'] = str_replace(array(',other','other'),'',$_POST['hznum']);
				$conditionDomains['suffix'] = array('not in',$_POST['hznum']);
				// dump($_POST['hznum']);
			}else{
				$conditionDomains['suffix'] = array('IN',$_POST['hznum']);
			}
		}
		//主题
		if($_POST['oper'] !== ''){
			$conditionDomains['theme_id'] = $_POST['oper'];
		}
		//域名所有人
//		if($_POST['userid'] !== ''){
//			$conditionDomains['uid'] = $_POST['userid'];
//		}
		//判断域名长度
		if($_POST['ym_length_one'] !== '' && $_POST['ym_length_two'] !== ''){
			if($_POST['ym_length_one'] == $_POST['ym_length_two']){
				$conditionDomains['length'] = $_POST['ym_length_one'];
			}else if($_POST['ym_length_one'] < $_POST['ym_length_two']){
				$conditionDomains['length'] = array(array('egt',$_POST['ym_length_one']),array('elt',$_POST['ym_length_two']));
			}else{
				$conditionDomains['length'] = array('egt',$_POST['ym_length_one']);
			}
		}else if($_POST['ym_length_one'] !== ''){
			$conditionDomains['length'] = array('egt',$_POST['ym_length_one']);
		}else if($_POST['ym_length_two'] !== ''){
			$conditionDomains['length'] = array('elt',$_POST['ym_length_two']);
		}
		
		//判断域名价格
		if(!empty($_POST['earge_parse_one']) && !empty($_POST['earge_parse_two'])){
			if($_POST['earge_parse_one'] == $_POST['earge_parse_two']){
				$conditionDomains['money'] = $_POST['earge_parse_one'];
			}else if($_POST['earge_parse_one'] < $_POST['earge_parse_two']){
				$conditionDomains['money'] = array(array('egt',$_POST['earge_parse_one']),array('elt',$_POST['earge_parse_two']));
			}else{
				$conditionDomains['money'] = array('egt',$_POST['earge_parse_one']);
			}
		}else if(!empty($_POST['earge_parse_one'])){
			$conditionDomains['money'] = array('egt',$_POST['earge_parse_one']);
		}else if(!empty($_POST['earge_parse_two'])){
			$conditionDomains['money'] = array('elt',$_POST['earge_parse_two']);
		}
		
		//判断域名类型
		if(in_array($_POST['style_ym'],array('a','b','c'))){
			$conditionDomains['styleType'] = $_POST['style_ym'];
		}else if(!empty($_POST['style_ym'])){
			$styleYmArr = explode(',',$_POST['style_ym']);
			if(count($styleYmArr) == 1){
				$conditionDomains['_string'] = "FIND_IN_SET('{$styleYmArr[0]}',`style`)";
			}else{
				foreach($styleYmArr as $key=>$value){
					$conditionDomains['_string'] .= ($key == 0 ? '' : ' ')."FIND_IN_SET('{$value}',`style`) OR";
				}
				$conditionDomains['_string'] = rtrim($conditionDomains['_string'],'OR');
			}
		}
                
		$allCount = $databaseDomains->where($conditionDomains)->count();
//                echo $databaseDomains->getLastSql();
//                die;
		// dump($databaseDomains);
		$allPage = ceil($allCount/$pageRows);
		
		$pageFirst = ($_POST['pageIndex'] - 1)*$pageRows;
		
		switch($_POST['order']){
			case '5':
				$orderStr = '`add_time` DESC';
				break;
			case '4':
				$orderStr = '`add_time` ASC';
				break;
			case '3':
				$orderStr = '`money` DESC';
				break;
			case '2':
				$orderStr = '`money` ASC';
				break;
			case '1':
				$orderStr = '`length` DESC';
				break;
			case '0':
				$orderStr = '`length` ASC';
				break;
		}
		$domainList = $databaseDomains->field('`domain_id`,`domain`,`label`,`money`,`type`,`desc`,`startTime`,`endTime`')->where($conditionDomains)->order($orderStr)->limit($pageFirst.','.$pageRows)->select();
		// dump($databaseDomains);
		if(!empty($domainList)){
			foreach($domainList as &$domainValue){
				$domainValue['money'] = floatval($domainValue['money']);
				$domainValue['cls'] = '其它';
				if($domainValue['type'] == 2){
					$domainValue['over_time'] = getOverTime($domainValue['endTime']);
				}
			}
		}
		$resultInfo['rows'] = $domainList ? $domainList : array();
		$resultInfo['paging'][0] = array(
			'pageIndex' => $_POST['pageIndex'],
			'total'	=> $allCount,
			'pageSize' => $pageRows,
		);
		$this->success($resultInfo);
	}
	public function recom(){
		$Db_domains = D("Domains");
		$domains_condition_speity['type'] = $_POST['type'];
		$domains_condition_speity['status']    = '1';
		$domains_condition_speity['domain_id']  = array('egt','(SELECT FLOOR(MAX(`domain_id`)*RAND()) FROM `pigcms_domains` )');
		$count_domians_speity = $Db_domains->where($domains_condition_speity)->count();
		$domains_speity_list = $Db_domains->field('`domain_id`,`domain`,`label`,`money`,`type`,`desc`')->where($domains_condition_speity)->order('`add_time` DESC')->limit(10)->select();
		$this->success($domains_speity_list);
	}
	public function detail(){
		$domain_id = $_GET['id'];
                $collection_show = $this->is_collection($domain_id);
                $this->assign('collection_show',$collection_show);
		if(empty($domain_id)){
			redirect(U('Buydomains/index'));
		}
		$databaseDomains = D('Domains');
		$now_domain = $databaseDomains->field(true)->where(array('domain_id'=>$domain_id))->find();
		if(empty($now_domain)){
			$this->assign('jumpUrl',U('Buydomains/index'));
			$this->error('当前域名不存在或已被删除');
		}
		if($now_domain['status'] != 1){
			$this->assign('jumpUrl',U('Buydomains/index'));
			$this->error($now_domain['status'] == 0 ? '当前域名正在审核中' : '当前域名已经出售');
		}
		D('Domains')->where(array('domain_id'=>$domain_id))->setInc('hits');
		
		$now_domain['hits'] += 1;
		$now_domain['money'] = rtrim(rtrim($now_domain['money'],'0'),'.');
		$this->assign('now_domain',$now_domain);
		
		
		//感兴趣的域名
		$conditionFavList['styleType'] = $now_domain['styleType'];
		$conditionFavList['status'] = '1';
		$favList = $databaseDomains->field('`domain_id`,`domain`')->where($conditionFavList)->order('`domain_id` DESC')->limit(16)->select();
		$this->assign('favList',$favList);
		
		$this->display();
	}
	public function quote(){
		$databaseDomains = D('Domains');
		if(empty($this->user_session)){
			$this->error('1');
			$databaseDomains = D('Domains');
		}
		$domain_id = $_GET['id'];
		if(empty($domain_id)){
			$this->error('请携带域名ID');
		}
		$now_domain = $databaseDomains->field(true)->where(array('domain_id'=>$domain_id))->find();
		if(empty($now_domain)){
			$this->error('当前域名不存在或已被删除');
		}
		if($now_domain['status'] != 1){
			$this->error('当前域名状态不正常');
		}
		if($this->user_session['uid'] == $now_domain['uid']){
			$this->error('您不能对自己的域名进行报价');
		}
		$money = intval($_GET['money']);
		if(empty($money)){
			$this->error('请携带您报价的价格');
		}
		if(D('Domains_quote')->where(array('buy_uid'=>$this->user_session['uid'],'domain_id'=>$domain_id,'status'=>array('neq','2')))->find()){
			$this->success('1');
			exit();
		}
		
		$dataQuote = array(
			'buy_uid'=>$this->user_session['uid'],
			'sell_uid'=>$now_domain['uid'],
			'domain_id'=>$domain_id,
			'domain'=>$now_domain['domain'],
			'new_money'=>$money,
			'last_time'=>$_SERVER['REQUEST_TIME'],
			'invalid_time'=>$_SERVER['REQUEST_TIME'] + 604800,
		);
		$quote_id = D('Domains_quote')->data($dataQuote)->add();
		if($quote_id){
			$dataQuoteList['quote_id'] = $quote_id;
			$dataQuoteList['is_sell'] = '0';
			$dataQuoteList['money'] = $money;
			$dataQuoteList['time'] = $_SERVER['REQUEST_TIME'];
			D('Domains_quote_list')->data($dataQuoteList)->add();
				//通知卖方
				$phone =  D('User')->where(array('uid'=>$now_domain['uid']))->getField('phone');
				$send_uid = $now_domain['uid'];

			if(!empty($phone)){
				$text = '您好，【' .$now_domain['domain'] .'】有新的报价【￥'.$money.' 元】，请您及时登录系统查看！';
				$send_time = time();
				Sms::sendSms(array( 'content' => $text, 'mobile' => $phone, 'uid' =>$send_uid, 'type' => 'domains_user', 'send_time'=>$send_time,'expire_time'=>'','extra'=>''));
			}



			$this->success($quote_id);
		}else{
			$this->error('报价失败，请重试');
		}
	}
}