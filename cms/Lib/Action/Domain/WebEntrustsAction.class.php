<?php
/*
 * web列表
 *
 */
class WebEntrustsAction extends BaseAction{
    
    
	  
	public function index1(){
		$this->assign('entr_type',1);
		$this->display('index');
	}
	
	public function index2(){
	
		$this->assign('entr_type',2);
		$this->display('index');
	}
	
	
	
	public function pagewebs(){
	    $WebEntrust = D('WebEntrust');
	    
	    $where['wstatus'] = '1';
	    //销售类型
	    if($_POST['entr_type']){
	        $where['entr_type']=$_POST['entr_type'];
	    }
	    $keybaohan =  intval($_POST['keybaohan']);
	    $description = trim($_POST['description']);
	    //关键字	    if($keybaohan && $description){
	        $description = $_POST['description'];
	        switch($keybaohan){
	            case 1:
	                $where['description']=array('LIKE','%'.$description.'%');
	                break;
	            case 2:
	                $where['description']=array('LIKE','%'.$description);
	                break;
	            case 3:
	                $where['description']=array('LIKE',$description.'%');
	                break;
	            default:
	                $where['description']=array('LIKE',$description.'%');
	                break;
	        }
	    }
	    //网站类型
        if($_POST['web_type'] !== ''){
                $_POST['web_type'] = str_replace(array(',',''),',',$_POST['web_type']);
                if($_POST['web_type']!=',') {
                   $where['web_type'] = array('in',$_POST['web_type']);
                }
        }
        
        //pr
        if($_POST['pr_begin'] !== '' && $_POST['pr_end'] !== ''){
            if($_POST['pr_begin'] == $_POST['pr_end']){
                $where['pr'] = $_POST['pr_begin'];
            }else if($_POST['pr_begin'] < $_POST['pr_end']){
                $where['pr'] = array(array('egt',$_POST['pr_begin']),array('elt',$_POST['pr_end']));
            }else{
                $where['pr'] = array('egt',$_POST['pr_begin']);
            }
        }else if($_POST['pr_begin'] !== ''){
            $where['pr'] = array('egt',$_POST['pr_begin']);
        }else if($_POST['pr_end'] !== ''){
            $where['pr'] = array('elt',$_POST['pr_end']);
        }
        
        //价格
        if($_POST['flowWebBegin'] !== '' && $_POST['flowWebEnd'] !== ''){
            if($_POST['flowWebBegin'] == $_POST['flowWebEnd']){
                $where['price'] = $_POST['flowWebBegin'];
            }else if($_POST['flowWebBegin'] < $_POST['flowWebEnd']){
                $where['price'] = array(array('egt',$_POST['flowWebBegin']),array('elt',$_POST['flowWebEnd']));
            }else{
                $where['price'] = array('egt',$_POST['flowWebBegin']);
            }
        }else if($_POST['flowWebBegin'] !== ''){
            $where['price'] = array('egt',$_POST['flowWebBegin']);
        }else if($_POST['flowWebEnd'] !== ''){
            $where['price'] = array('elt',$_POST['flowWebEnd']);
        }
        
        if($_POST['baiduBegin'] !== ''){
            $where['pc_baidu'] = array(array('egt',$_POST['baiduBegin']));
            $where['app_baidu'] = array(array('egt',$_POST['baiduBegin']));
        }else if($_POST['baiduEnd'] !== ''){
            $where['pc_baidu'] = array('elt',$_POST['baiduEnd']);
            $where['app_baidu'] = array('elt',$_POST['baiduEnd']);
        }
        
	    $pageRows = 15;
	   
	    $allCount = $WebEntrust->where($where)->count();
	    
	    $allPage = ceil($allCount/$pageRows);
	   
	    $pageFirst = (I('post.pageIndex',1)- 1)*$pageRows;
	
	    if($_POST['order']){
    	    switch($_POST['order']){
    	        case '1':
    	            $orderStr = ' `price` DESC';
    	            break;
    	        case '2':
    	            $orderStr = ' `createdate` DESC';
    	            break;
    	        default:
    	            $orderStr = ' `price` ASC';
    	            break;
    	    }
	    }else {
	        $orderStr = ' `price` ASC';
	    }
	    $domainList = $WebEntrust->field('id,pr,price,description,entr_type,pc_baidu,app_baidu')->where($where)->
	    order($orderStr)->limit($pageFirst.','.$pageRows)->select();
	    $resultInfo['rows'] = $domainList;
	    $resultInfo['paging'][0] = array(
	        'pageIndex' => I('post.pageIndex',1),
	        'total'	=> $allCount,
	        'pageSize' => $pageRows,
	    );
	    $resultInfo['where'] = json_encode($where);
	    $this->ajaxReturn($resultInfo);
	}
	
	public function detail() {
	    $id=$_GET['id'];
	    $WebEntrust = D('WebEntrust');
	    $web = $WebEntrust->where('id='.$id)->find();
	    $WebEntrust->where(array('id' => $id))->setInc('look_num',1);
	    $list = $WebEntrust->where('wstatus=1 and entr_type=2')->order('`createdate` DESC')->limit(0,30)->select();
	    $this->assign('web_list',$list);
	    $this->assign('ar',$web);
	    $this->display();
	}
	
	
	public function editView(){
	      $price=$_GET['price'];
	      $webtype=$_GET['webtype'];
	      $this->assign('webtype',$webtype);
	      if(empty($price) || $price=='') {
	          $price=0;
	      }
	      $this->assign('price',$price);
	      $WebEntrust = M('WebEntrust');
	      $list = $WebEntrust->where('wstatus=1 and is_case=1 and entr_type='.$webtype)->order('`createdate` DESC')->limit(0,30)->select();;
	      $this->assign('alList',$list);
	      $this->display();
	}
	public function add(){
	    parent::_initialize();
	    if(empty($this->user_session)){
	        $this->error('请您先登录！',U('Login/index'));
	    }
	    $data['domain'] = I('post.domain');
	    $data['entr_type'] =I('post.entr_type',1);
	    $data['web_type'] = I('post.web_type',1);
	    $data['description'] =I('post.description');
	    $data['pc_baidu'] = I('post.pc_baidu',0);
	    $data['app_baidu'] =I('post.app_baidu',0);
	    $data['pc_ip'] = I('post.pc_ip',0);
	    $data['app_ip'] =I('post.app_ip',0);
	    $data['souce_code'] =I('post.souce_code');
	    $data['pr'] = I('post.pr',0);
	    
	    $data['month_income'] =I('post.month_income',0);
	    $data['server_info'] =I('post.server_info');
	    $data['third_party'] =I('post.third_party');
	    $data['update_type'] =I('post.update_type');
	    $data['advert_aliance'] =I('post.advert_aliance');
	    $data['aliance_acc_handle'] =I('post.aliance_acc_handle',1);
	    $data['price'] =I('post.price',0);
	    $data['email'] =I('post.email');
	    $data['phone'] =I('post.phone');
	    $data['createdate'] = date("Y-m-d H:i:s",time());
	    $data['user_id'] = $this->user_session['uid'];
	    $WebEntrust = D("WebEntrust");
	    if ($WebEntrust->create($data)) {
	        if (false !== $WebEntrust->add()) {
	           // $this->success($data['domain'],U('WebEntrusts/index'));
	            $this->success($data['domain']);
	        } else {
	            $this->error('添加失败,请检查您的输入是否有误!');
	        }
	    } else {
	        // 字段验证错误
	        $this->error($WebEntrust->getError());
	    }
	}
	
	/* 网站查询 1 代购 2 代售*/
	public function webList(){
	    $database_Web_entrust = D('Web_entrust');
	     
	    $condition = array();
	    $type = isset($_GET['type']) ? intval($_GET['type']) : 0;
	     
	    $condition['user_id'] = $this->user_session['uid'];
	    $condition['entr_type'] = intval($_GET['entr_type']);
	    echo $type;
	    if ($type>0) {
	        $condition['wstatus'] = intval($_GET['type'])-1;
	    };
	    $this->assign('type',$_GET['type']);
	    $this->assign('entr_type',$_GET['entr_type']);
	    $count_Web_entrust = $database_Web_entrust->where($condition)->count();
	    import('@.ORG.system_page');
	    $p = new Page($count_Web_entrust, 5);
	    $group_list = $database_Web_entrust->field(true)->where($condition)->order('`id` ASC')->limit($p->firstRow . ',' . $p->listRows)->select();
	    $this->assign('group_list',$group_list);
	    $pagebar = $p->show();
	    $this->assign('pagebar', $pagebar);
	    $this->display();
	}
	
	
	//编辑网站
	public function edit(){
	    $database_Web_entrust = D('Web_entrust');
	    $condition_Web_entrust['id'] = intval($_GET['id']);
	    $now_Web_entrust = $database_Web_entrust->field(true)->where($condition_Web_entrust)->find();
	    if(empty($now_Web_entrust)){
	        $this->frame_error_tips('数据库中没有查询到该内容！',5);
	    }
	    $this->assign('now_Web_entrust',$now_Web_entrust);
	    $this->display();
	}
	
	//保存网站修改编辑
	public function edit_web(){
	    $database_Web_entrust = D('Web_entrust');
	    $_POST['info'] = fulltext_filter($_POST['info']);
	    $_POST['updatedate'] = date('Y-m-d H:i:s');
	    if($database_Web_entrust->data($this->Removalquotes($_POST))->save()){
	        $this->frame_submit_tips(1,'编辑成功！');
	    }else{
	        $this->frame_submit_tips(0,'编辑失败！');
	    }
	}
	
	/*******删除*********/
	public function del(){
	    $id = isset($_POST['id']) ? intval($_POST['id']) : 0;
	    D('Web_entrust')->where(array('id'=>$id))->delete();
	    echo 1;die;
	}
	
	
}
