<?php
/*
 * 拍卖域名
 *
 */
class FastbidAction extends BaseAction{
	public function index(){
		//域名后缀
		$databaseDomainSuffix = D('Domain_suffix');
		$conditionDomainSuffix['is_hots'] = '1';
		$suffixList = $databaseDomainSuffix->field('`id`,`suffix`')->where($conditionDomainSuffix)->order('`sorts` DESC,`id` ASC')->select();
		$this->assign('suffixList',$suffixList);
		
		//主题属性
		$databaseDomainTreasure = D('Domain_treasure');
		$conditionDomainTreasure['status'] = '1';
		$treasureList = $databaseDomainTreasure->field(true)->where($conditionDomainTreasure)->order('`sorts` DESC,`id` ASC')->select();
		$this->assign('treasureList',$treasureList);
		
		$databaseDomains = D('Domains');
		$conditionDomains['status'] = '1';
		$conditionDomains['type'] = '2';
		$allCount = $databaseDomains->where($conditionDomains)->count();
		$allPage = ceil($allCount/20);
		$domainList = $databaseDomains->field('`domain_id`,`domain`,`label`,`money`,`type`,`startTime`,`endTime`')->where($conditionDomains)->order('`add_time` DESC')->limit(20)->select();
		foreach($domainList as &$domainValue){
			$domainValue['money'] = floatval($domainValue['money']);
			$domainValue['over_time'] = getOverTime($domainValue['endTime']);
			switch($domainValue['type']){
				case '1':
					$domainValue['url'] = U('Hotsale/selling',array('id'=>$domainValue['domain_id']));
					break;
				case '2':
					$domainValue['url'] = U('Fastbid/detail',array('id'=>$domainValue['domain_id']));
					break;
				default:
					$domainValue['url'] = U('Buydomains/detail',array('id'=>$domainValue['domain_id']));
			}
		}
		$this->assign('allCount',$allCount);
		$this->assign('allPage',$allPage);
		$this->assign('domainList',$domainList);
		import('@.ORG.domain_page');
		$pageClass = new Page($allCount,20);
		$page = $pageClass->show();
		$this->assign('page',$page);
		
		$this->display();
	}
	public function detail(){
		$domain_id = $_GET['id'];
                $collection_show = $this->is_collection($domain_id);
                $this->assign('collection_show',$collection_show);
		if(empty($domain_id)){
			redirect(U('Fastbid/index'));
		}
		$databaseDomains = D('Domains');
		$now_domain = $databaseDomains->field(true)->where(array('domain_id'=>$domain_id))->find();
		if(empty($now_domain)){
			$this->assign('jumpUrl',U('Fastbid/index'));
			$this->error('当前域名不存在或已被删除');
		}
		if($now_domain['status'] != 1){
			$this->assign('jumpUrl',U('Fastbid/index'));
			$this->error($now_domain['status'] == 0 ? '当前域名正在审核中' : '当前域名已经出售');
		}
		if($now_domain['endTime'] < $_SERVER['REQUEST_TIME']){
			$this->endFastBid($now_domain);
			$this->error('当前竞价已结束');
		}
		D('Domains')->where(array('domain_id'=>$domain_id))->setInc('hits');
		
		$now_domain['overplus_time']    = $now_domain['endTime'] - $_SERVER['REQUEST_TIME'];
		$now_domain['overplus_time_d']  = floor($now_domain['overplus_time']/86400);
		$now_domain['overplus_time_dh'] = $now_domain['overplus_time']%86400;
		// echo $now_domain['overplus_time_d'].'<br/>';
		// echo $now_domain['overplus_time_dh'].'<br/><br/><br/>';
		$now_domain['overplus_time_h']  = floor($now_domain['overplus_time_dh']/3600);	
		$now_domain['overplus_time_hm']  = $now_domain['overplus_time_dh']%3600;
		// echo $now_domain['overplus_time_h'].'<br/>';
		// echo $now_domain['overplus_time_hm'].'<br/><br/><br/>';
		$now_domain['overplus_time_m']  = floor($now_domain['overplus_time_hm']/60);
		$now_domain['overplus_time_ms']  = $now_domain['overplus_time_hm']%60;
		// echo $now_domain['overplus_time_m'].'<br/>';
		// echo $now_domain['overplus_time_ms'].'<br/>';
		$now_domain['overplus_time_s']  = floor($now_domain['overplus_time_ms']/60);
		$now_domain['hits'] += 1;
		if($now_domain['lastFidId'] && $now_domain['money'] >= $now_domain['mark_money']){
			$now_domain['plusPrice'] = getFastbidPriceList($now_domain['money']);
			$now_domain['pleasePrice'] = $now_domain['plusPrice']['plusPrice'] + $now_domain['money'];
		}else{
			$now_domain['plusPrice'] = getFastbidPriceList($now_domain['mark_money']);
			$now_domain['pleasePrice'] = $now_domain['plusPrice']['plusPrice'] + $now_domain['mark_money'];
		}
		$now_domain['money'] = getFriendMoney($now_domain['money'],true);
		$now_domain['mark_money'] = getFriendMoney($now_domain['mark_money'],true);
		$this->assign('now_domain',$now_domain);
		
		if($now_domain['lastFidId']){
			$fidList = D('Domains_fastbid_list')->where(array('domain_id'=>$now_domain['domain_id']))->order('`pigcms_id` DESC')->select();
			foreach($fidList as $key=>$value){
				$fidList[$key]['money'] = getFriendMoney($value['money'],true);
			}
			$this->assign('fidList',$fidList);
		}
		
		$allPlusPrice = getFastbidPriceList(-1);
		$this->assign('allPlusPrice',$allPlusPrice);
//                dump($now_domain);
		
		$this->display();
	}
	public function fastbidmoney(){
		if(empty($this->user_session)){
			$this->success('5');exit();
		}
		$this->user_session = D('User')->get_user($this->user_session['uid']);
		$domain_id = $_POST['auctionId'];
		if(empty($domain_id)){
			$this->error(U('Buydomains/index'));
		}
		$databaseDomains = D('Domains');
		$now_domain = $databaseDomains->field(true)->where(array('domain_id'=>$domain_id))->find();
		if(empty($now_domain)){
			$this->error('当前域名不存在或已被删除');
		}
		if($now_domain['type'] != 2){
			$this->error('当前域名不属于竞价');
		}
		if($this->user_session['uid'] == $now_domain['uid']){
			$this->success('4');exit();
		}
		if($now_domain['status'] != '1' || $now_domain['endTime'] < $_SERVER['REQUEST_TIME']){
			$this->endFastBid($now_domain);
			$this->success('0');exit();
		}
		if($now_domain['lastFidId'] && $now_domain['money'] >= $now_domain['mark_money']){
			$now_domain['plusPrice'] = getFastbidPriceList($now_domain['money']);
			$now_domain['pleasePrice'] = $now_domain['plusPrice']['plusPrice'] + $now_domain['money'];
		}else{
			$now_domain['plusPrice'] = getFastbidPriceList($now_domain['mark_money']);
			$now_domain['pleasePrice'] = $now_domain['plusPrice']['plusPrice'] + $now_domain['mark_money'];
		}
		if($_POST['offer'] < $now_domain['pleasePrice']){
			$this->error('您的价格必须大于或等于 ￥'.$now_domain['pleasePrice']);exit();
		}
		if($this->user_session['now_money'] < $now_domain['plusPrice']['depositPrice']){
			$this->success('1');exit();
		}
		if(in_array(1,$this->user_session['permissions'])){
			$freeze = 0;
		}else{
			$freeze = $now_domain['plusPrice']['depositPrice'];
		}
		$databaseUser = D('User');
		$conditionUser['uid'] = $this->user_session['uid'];
		$dataUser['now_money'] = $this->user_session['now_money'] - $freeze;
		$dataUser['freeze_money'] = $this->user_session['freeze_money'] + $freeze;
		if($databaseUser->where($conditionUser)->data($dataUser)->save()){
			//添加到冻结记录表
			$data_user_freeze['uid'] = $this->user_session['uid'];
			$data_user_freeze['freezetime'] = $_SERVER['REQUEST_TIME'];
			$data_user_freeze['freezemoney'] = $freeze;
			$data_user_freeze['info'] = '竞价域名 '.$now_domain['domain'].'冻结保证金';
			$data_user_freeze['type'] = '6';
			$freeze_id = D('User_freeze')->data($data_user_freeze)->add();
			
			//添加到资金记录表
			D('User_money_list')->add_row($this->user_session['uid'],12,$freeze,$dataUser['now_money'],'竞价域名 '.$now_domain['domain'].' 冻结保证金');
			
			//添加到竞价记录表
			$dataDomainsFastbidList['time'] = time();
			$dataDomainsFastbidList['freeze_id'] = $freeze_id;
			$dataDomainsFastbidList['domain_id'] = $now_domain['domain_id'];
			$dataDomainsFastbidList['money'] = $_POST['offer'];
			$dataDomainsFastbidList['uid'] = $this->user_session['uid'];
			$lastFidId = D('Domains_fastbid_list')->data($dataDomainsFastbidList)->add();
			
			//修改域名状态
			$dataDomain = array('money'=>$_POST['offer'],'lastFidId'=>$lastFidId);
			if(time()-300 >= $now_domain['endTime']){
				$dataDomain['endTime'] = $now_domain['endTime']+300;
			}
			D('Domains')->where(array('domain_id'=>$now_domain['domain_id']))->data($dataDomain)->save();
			
			
			//将上一位用户的保证金还回去
			if($now_domain['lastFidId']){
				$lastFid = D('Domains_fastbid_list')->field(true)->where(array('pigcms_id'=>$now_domain['lastFidId']))->find();
				
				$condition_last_user_freeze['id'] = $lastFid['freeze_id'];
				$now_freeze = D('User_freeze')->where($condition_last_user_freeze)->find();
				
				$databaseUser = D('User');
				$lastFidUser = $databaseUser->get_user($lastFid['uid']);
				$conditionLastFidUser['uid'] = $lastFidUser['uid'];
				$dataLastFidUser['now_money'] = $lastFidUser['now_money'] + $now_freeze['freezemoney'];
				$dataLastFidUser['freeze_money'] = $lastFidUser['freeze_money'] - $now_freeze['freezemoney'];
				if($databaseUser->where($conditionLastFidUser)->data($dataLastFidUser)->save()){
					D('User_freeze')->where($condition_last_user_freeze)->delete();
					// dump(D('User_freeze')->getLastSql());
					D('User_money_list')->add_row($lastFidUser['uid'],13,$now_freeze['freezemoney'],$dataLastFidUser['now_money'],'竞价域名 '.$now_domain['domain'].' 解冻保证金');
				}
			}
			
			$this->success('10');
		}else{
			// dump($databaseUser);
			$this->success('7');
		}
	}
	public function sold(){
		$Db_domains = D("Domains");
		//type =>3 -优质域名
		$domains_condition_youzhi['type'] = '3';
		$domains_condition_youzhi['status'] = '2';
		$count_domains_youzhi = $Db_domains->where($domains_condition_youzhi)->count();

	    import('@.ORG.domain_page');
	    $p = new Page($count_domains_youzhi, 24);
	    $domains_youzhi_list = $Db_domains->field(true)->where($domains_condition_youzhi)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit($p->firstRow . ',' . $p->listRows)->select();
		
		$page = $p->show();
		$this->assign('page',$page);
		$this->assign("count_domains_youzhi",$count_domains_youzhi);
		$this->assign('domains_youzhi_list', $domains_youzhi_list);

		//推荐域名
		$domains_condition_speity['type'] = '3';
		$domains_condition_speity['status']    = '1';
		$domains_condition_speity['domain_id']  = array('egt','(SELECT FLOOR(MAX(`domain_id`)*RAND()) FROM `pigcms_domains`)');
		$domains_speity_list = $Db_domains->field(true)->where($domains_condition_speity)->order('`add_time` DESC, `domain_id` DESC')->limit(10)->select();
		$this->assign('domains_speity_list',$domains_speity_list);
		
		$this->display();
	}
}