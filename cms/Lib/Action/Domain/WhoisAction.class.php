<?php
/*
 * whois 查询
 *
 *
 */
class WhoisAction extends BaseAction{
	public function index($call = ''){
		$this->assign('select_class_t','whois');
		$keyword = strtolower(trim($_GET['keyword']));
		$get_str =  array();
		$get_str = explode('.',$keyword);
		if(count($get_str) < 0){
			$this->redirect('/','',0,'您进来的方式不对。');exit;
		}
		if(empty($get_str[1])){
			$keyword = $keyword .'.com';
			$this->assign('keyword',$keyword);
		}

		if($this->topdomain == 'zhu.cn' || $this->topdomain == 'dev.com'){
			//if($this->topdomain == 'zhu.cn'){
			//Self
			$Db_cache_search_domains = D('Cache_search_domains');
			if(isset($_GET['type']) && trim($_GET['type']) =='now'){
				$check_whoisResult = $Db_cache_search_domains->field('`cache_id`,`domainName`')->where("`domainName`='{$keyword}'")->find();
				//更新数据
				if(!empty($check_whoisResult)){
					import('@.ORG.NewWhois');
					$newInfo = new NewWhois();
					$whoisResult = $newInfo->getNewWhois($keyword);
					if(!empty($whoisResult['domainName'])){
						$whoisResult['update_time'] = time();
						$whoisResult['cache_id'] = $check_whoisResult['cache_id'];
						$Db_cache_search_domains->data($whoisResult)->save();
						$this->redirect('Whois/index',array('keyword'=>$whoisResult['domainName']));
					}else{
						import('@.ORG.whois');
						$whoisClass = new whois();
						$whoisResult = $whoisClass->getWhois($keyword);
						if(!empty($whoisResult['domainName'])){
							$whoisResult['update_time'] = time();
							$whoisResult['cache_id'] = $check_whoisResult['cache_id'];
							$Db_cache_search_domains->data($whoisResult)->save();
							$this->redirect('Whois/index',array('keyword'=>$whoisResult['domainName']));
						}
					}
				}
			}
			$whoisResult = $Db_cache_search_domains->field(true)->where("`domainName`='{$keyword}'")->find();
			if(!empty($whoisResult)){
				$this->assign('whoisResult',$whoisResult);
			}else{

				import('@.ORG.NewWhois');
				$newInfo = new NewWhois();
				$whoisResult = $newInfo->getNewWhois($keyword);
				if(!empty($whoisResult['domainName'])){
					$whoisResult['update_time'] = time();
					$Db_cache_search_domains->data($whoisResult)->add();
				}else{
					import('@.ORG.whois');
					$whoisClass = new whois();
					$whoisResult = $whoisClass->getWhois($keyword);
					if(!empty($whoisResult['domainName'])){
						$whoisResult['update_time'] = time();
						$Db_cache_search_domains->data($whoisResult)->add();
					}
					$whoisResult['domainName'] = $keyword;
					$whoisResult['update_time'] = time();
				}
			}



		}else{
			//Other
			if(trim($_GET['type']) == 'now' && isset($_GET['type'])) $type = '&type=now';
			$WhoisAPI = 'http://demo.zhu.cn/index.php?c=Whois&a=whoisinfo&keyword='.$keyword.$type;
			//$WhoisAPI = 'http://www.dev.com/index.php?c=Whois&a=whoisinfo&keyword='.$keyword.$type;
			import('ORG.Net.Http');
			$http = new Http();
			$whoisResult = Http::curlGet($WhoisAPI);
			$whoisResult =json_decode($whoisResult,true);
		}

		$this->assign('whoisResult',$whoisResult);
		if($call =='True'){}else{
			$this->display();
		}

	}

	//api
	public function whoisinfo($keyword = ''){
		$keyword = strtolower(trim($keyword));
		$get_str =  array();
		$get_str = explode('.',$keyword);
		if(count($get_str) < 0) exit(json_encode(array('status'=>-1,'result'=>'')));
		$whoisResult = array();
		if(empty($get_str[1])){
			$keyword = $keyword .'.com';
			$this->assign('keyword',$keyword);
		}

		$Db_cache_search_domains = D('Cache_search_domains');

		if(isset($_GET['type']) && trim($_GET['type']) =='now'){
			$check_whoisResult = $Db_cache_search_domains->field('`cache_id`,`domainName`')->where("`domainName`='{$keyword}'")->find();
			//更新数据
			if(!empty($check_whoisResult)){
				import('@.ORG.NewWhois');
				$newInfo = new NewWhois();
				$whoisResult = $newInfo->getNewWhois($keyword);
				if(!empty($whoisResult['domainName'])){
					$whoisResult['update_time'] = time();
					$whoisResult['cache_id'] = $check_whoisResult['cache_id'];
					$Db_cache_search_domains->data($whoisResult)->save();
				}else{
					import('@.ORG.whois');
					$whoisClass = new whois();
					$whoisResult = $whoisClass->getWhois($keyword);
					if(!empty($whoisResult['domainName'])){
						$whoisResult['update_time'] = time();
						$whoisResult['cache_id'] = $check_whoisResult['cache_id'];
						$Db_cache_search_domains->data($whoisResult)->save();
					}
				}
			}
		}

		$whoisResult = $Db_cache_search_domains->field(true)->where("`domainName`='{$keyword}'")->find();
		if(!empty($whoisResult)){
			print json_encode($whoisResult); exit;
		}else{
			import('@.ORG.NewWhois');
			$newInfo = new NewWhois();
			$whoisResult = $newInfo->getNewWhois($keyword);
			if(!empty($whoisResult['domainName'])){
				$whoisResult['update_time'] = time();
				$Db_cache_search_domains->data($whoisResult)->add();
			}else{
				import('@.ORG.whois');
				$whoisClass = new whois();
				$whoisResult = $whoisClass->getWhois($keyword);
				if(!empty($whoisResult['domainName'])){
					$whoisResult['update_time'] = time();
					$Db_cache_search_domains->data($whoisResult)->add();
				}
				$whoisResult['domainName'] = $keyword;
				$whoisResult['update_time'] = time();
			}

		}
		print json_encode($whoisResult); exit;
	}


	//ajax 请求域名是否注册
	public function isreg(){
		if(IS_POST){

			/***
			 *
			 *
			 * 万网提供了域名查询接口，接口采用HTTP协议：
			接口URL：http://panda.www.net.cn/cgi-bin/check.cgi
			接口参数：area_domain，接口参数值为标准域名，例：52bong.com
			调用举例：
			http://panda.www.net.cn/cgi-bin/check.cgi?area_domain=52bong.com
			 * 返回结果说明：

			200 返回码，200表示返回成功
			52bong.com  表示当前查询的域名
			211 : Domain exists 返回结果的原始信息，主要有以下几种
			 *
			original=210 : Domain name is available     表示域名可以注册
			original=211 : Domain exists    表示域名已经注册
			original=212 : Domain name is invalid       表示查询的域名无效
			original=213 : Time out 查询超时
			 *
			 *
			 *
			 */

			//域名是否注册
			$server_url = 'http://panda.www.net.cn/cgi-bin/check.cgi?area_domain=';
 			$get_domains = $_POST['arr_yms'];
			$put_url =$server_url . $get_domains;
			import('ORG.Net.Http');
			$http = new Http();
			$get_str = Http::curlGet($put_url);
			//$lstr = ltrim($get_info,'("');
			$get_info = str_replace('")','',ltrim($get_str,'("'));

			//file_put_contents('00get_status.log', str_replace('")','',ltrim($get_info,'("')));
			$str_explode = explode('#',$get_info);
			$count = count($str_explode);
			$lstr_explode = array();
			for($i = 0; $i< $count ;$i++){
				$lstr_explode[$i] = explode('|',$str_explode[$i]);
			}
			//file_put_contents('is_ok.log',print_r($lstr_explode,true));
			echo json_encode($lstr_explode);die;

		}
	}

public function reverse(){
		//域名  注册人  注册人邮箱
		$Db_cache_search_domains = D('Cache_search_domains');
		$keyword = strtolower(trim($_GET['keyword']));
		$get_str = explode('.',$keyword);
		if(empty($get_str[1])){
			$keyword = $keyword .'.com';
		}
		$this->assign('keyword',$keyword);

		if($this->topdomain == 'zhu.cn' ) {
			$get_reverse = $Db_cache_search_domains->field('`cache_id`,`domainName`,`registrarName`,`registrarEmail`')->where("`domainName`='{$keyword}'")->find();

			if(!$get_reverse){
				self::index('True');

				$get_reverse = $Db_cache_search_domains->field('`cache_id`,`domainName`,`registrarName`,`registrarEmail`')->where("`domainName`='{$keyword}'")->find();
			}

		}else{
			//Other

			$Whois_history = 'http://demo.zhu.cn/index.php?c=Whois&a=get_reverse&keyword='.$keyword;
			import('ORG.Net.Http');
			$http = new Http();
			$whoisHistoryResult = Http::curlGet($Whois_history);
			$get_reverse =json_decode($whoisHistoryResult,true);

		}

		$this->assign('reverseinfo',$get_reverse);
		$this->assign('select_class_t','reverse');
		$this->display();
	}

	//域名估价
	public function gj(){
		$this->assign('select_class_t','gj');
		$this->redirect('/','',3,'暂时未开放，请耐心等待。');exit;
		$this->display();

	}

	//whois 历史
	public function history(){
		$keyword = strtolower(trim($_GET['keyword']));
		$get_str = explode('.', $keyword);
		if (empty($get_str[1])) {
			$keyword = $keyword . '.com';
		}
		$this->assign('keyword', $keyword);

		if($this->topdomain == 'zhu.cn' ) {
			$Db_cache_search_domains = D('Cache_search_domains');


			//dump($keyword);
			$from_updatedDate = $Db_cache_search_domains->field(true)->where("`domainName`='{$keyword}'")->order('`update_time` DESC')->select();
			//echo $Db_cache_search_domains->getLastSql();
			//注册商	注册人	注册人邮箱	注册日期	过期日期	记录时间	对比	    查看
			//dump($from_updatedDate);
			if (!$from_updatedDate) {
				self::index('True');
				$from_updatedDate = $Db_cache_search_domains->field(true)->where("`domainName`='{$keyword}'")->order('`update_time` DESC')->select();
			}

		}else{
			//Other
			//if(trim($_GET['type']) == 'now' && isset($_GET['type'])) $type = '&type=now';
			$Whois_history = 'http://demo.zhu.cn/index.php?c=Whois&a=gethistory&keyword='.$keyword;
			//echo $Whois_history;
			import('ORG.Net.Http');
			$http = new Http();
			$whoisHistoryResult = Http::curlGet($Whois_history);
			$from_updatedDate =json_decode($whoisHistoryResult,true);
		}
		$this->assign('from_updatedDate', $from_updatedDate);
		$this->assign('select_class_t', 'history');
		$this->display();
	}
	
	public function get_history(){
				
				if($this->topdomain == 'zhu.cn' ) {
					if (IS_POST) {
						
						$cache_id = intval($_POST['id']);
						$Db_cache_search_domains = D('Cache_search_domains');
						$history_data = $Db_cache_search_domains->field(true)->where("`cache_id`='{$cache_id}'")->find();

						print json_encode($history_data);
					}
				}else{
					//Other
					$cache_id = intval($_GET['id']);
					$Whois_history = 'http://demo.zhu.cn/index.php?c=Whois&a=noajax_get_history&id='.$cache_id;
					import('ORG.Net.Http');
					$http = new Http();
					$whoisHistoryResult = Http::curlGet($Whois_history);
					echo $whoisHistoryResult;
				}


	}



}