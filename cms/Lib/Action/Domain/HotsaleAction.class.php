<?php
/*
 * 一口价域名
 *
 */
class HotsaleAction extends BaseAction{
	public function index(){
		//域名后缀
		$databaseDomainSuffix = D('Domain_suffix');
		$conditionDomainSuffix['is_hots'] = '1';
		$suffixList = $databaseDomainSuffix->field('`id`,`suffix`')->where($conditionDomainSuffix)->order('`sorts` DESC,`id` ASC')->select();
		$this->assign('suffixList',$suffixList);
		
		//主题属性
		$databaseDomainTreasure = D('Domain_treasure');
		$conditionDomainTreasure['status'] = '1';
		$treasureList = $databaseDomainTreasure->field(true)->where($conditionDomainTreasure)->order('`sorts` DESC,`id` ASC')->select();
		$this->assign('treasureList',$treasureList);
		
		$Db_domains = D("Domains");
		//type =>3 -优质域名
		$domains_condition_youzhi['type'] = '1';
		$domains_condition_youzhi['status'] = '1';
		$count_domains_youzhi = $Db_domains->where($domains_condition_youzhi)->count();

	    import('@.ORG.domain_page');
	    $p = new Page($count_domains_youzhi, 24);
	    $domains_youzhi_list = $Db_domains->field(true)->where($domains_condition_youzhi)->order('`add_time` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
		
		$page = $p->show();
		$this->assign('page',$page);
		$this->assign("count_domains_youzhi",$count_domains_youzhi);
		$this->assign('domains_youzhi_list', $domains_youzhi_list);

		//推荐域名
		$domains_condition_speity['type'] = '1';
		$domains_condition_speity['status']    = '1';
		$domains_condition_speity['domain_id']  = array('egt','(SELECT FLOOR(MAX(`domain_id`)*RAND()) FROM `pigcms_domains`)');
		$domains_speity_list = $Db_domains->field(true)->where($domains_condition_speity)->order('`add_time` DESC, `domain_id` DESC')->limit(10)->select();
		$this->assign('domains_speity_list',$domains_speity_list);
		$this->display();
	}
        
//        域名收藏
        public function collection(){
            $_POST['uid'] = $this->user_session['uid'];
            $databases_domain_collection = M('Domain_collection');
            $row = $databases_domain_collection->where(array('uid'=>$_POST['uid'],'domain_id'=>$_POST['domain_id']))->select();
            if($row){
                echo 3;die;
            }
            $info = $databases_domain_collection->add($_POST);
            if($info){
                echo 1;die;
            }else{
                echo 2;die;
            }
        }
        
	public function selling(){
//            dump($this->user_session);
            
		$domain_id = $_GET['id'];
                $collection_show = $this->is_collection($domain_id);
                $this->assign('collection_show',$collection_show);
		if(empty($domain_id)){
			redirect(U('Buydomains/index'));
		}
		$databaseDomains = D('Domains');
		$now_domain = $databaseDomains->field(true)->where(array('domain_id'=>$domain_id))->find();
		if(empty($now_domain)){
			$this->assign('jumpUrl',U('Buydomains/index'));
			$this->error('当前域名不存在或已被删除');
		}
		if($now_domain['status'] != 1){
			$this->assign('jumpUrl',U('Buydomains/index'));
			$this->error($now_domain['status'] == 0 ? '当前域名正在审核中' : '当前域名已经出售');
		}
		D('Domains')->where(array('domain_id'=>$domain_id))->setInc('hits');
		
		$now_domain['hits'] += 1;
		$now_domain['money'] = rtrim(rtrim($now_domain['money'],'0'),'.');
		$this->assign('now_domain',$now_domain);
		
		
		//感兴趣的域名
		$conditionFavList['styleType'] = $now_domain['styleType'];
		$conditionFavList['status'] = '1';
		$favList = $databaseDomains->field('`domain_id`,`domain`')->where($conditionFavList)->order('`domain_id` DESC')->limit(16)->select();
		$this->assign('favList',$favList);
		
		$this->display();
	}
	public function buy(){
                $databaseUser = D('User');
                $data_info = $databaseUser->where(array('uid'=>  $this->user_session['uid']))->find();
                
		if(empty($this->user_session)){
			$this->error('1');
		}
		$domain_id = $_GET['id'];
		if(empty($domain_id)){
			$this->error('请携带域名ID');
		}
		$databaseDomains = D('Domains');
		$now_domain = $databaseDomains->field(true)->where(array('domain_id'=>$domain_id))->find();
		if(empty($now_domain)){
			$this->error('当前域名不存在或已被删除');
		}
		if($now_domain['status'] != 1){
			$this->error('当前域名状态不正常');
		}
		if($this->user_session['uid'] == $now_domain['uid']){
			$this->error('您不能购买自己的域名');
		}
		$freeze = ceil($now_domain['money']/10);
		if($data_info['now_money'] < $freeze){
			$this->error('账户余额不足！');
		}
		
		
		$conditionUser['uid'] = $this->user_session['uid'];
                $this->user_session['now_money'] -=$freeze;
                $this->user_session['freeze_money'] +=$freeze;
		$dataUser['now_money'] = $data_info['now_money'] - $freeze;
		$dataUser['freeze_money'] = $data_info['freeze_money'] + $freeze;
		if($databaseUser->where($conditionUser)->data($dataUser)->save()){
			//修改域名状态
			D('Domains')->where(array('domain_id'=>$now_domain['domain_id']))->data(array('status'=>'2'))->save();
			
			//添加到冻结记录表
			$data_user_freeze['uid'] = $this->user_session['uid'];
			$data_user_freeze['freezetime'] = $_SERVER['REQUEST_TIME'];
			$data_user_freeze['freezemoney'] = $freeze;
			$data_user_freeze['info'] = '购买一口价域名 '.$now_domain['domain'].'冻结资金';
			$data_user_freeze['type'] = '4';
			$freeze_id = D('User_freeze')->data($data_user_freeze)->add();
			
			//添加到资金记录表
			D('User_money_list')->add_row($this->user_session['uid'],12,$freeze,$dataUser['now_money'],'购买一口价域名 '.$now_domain['domain'].' 冻结资金');
			
			//添加到订单表
			$dataOrder['addTime'] = $_SERVER['REQUEST_TIME'];
			$dataOrder['total_price'] = $now_domain['money'];
			$dataOrder['domain_id'] = $now_domain['domain_id'];
			$dataOrder['domainName'] = $now_domain['domain'];
			$dataOrder['trade_type'] = '0';
			$dataOrder['status'] = '0';
			$dataOrder['buy_id'] = $this->user_session['uid'];
			$dataOrder['sell_id'] = $now_domain['uid'];
			$dataOrder['staff_id'] = '0';
			$dataOrder['payTye'] = '1';
			$dataOrder['yes_no'] = '3';
			$dataOrder['freeze_id'] = $freeze_id;
			$order_id = D('Order')->data($dataOrder)->add();
			// dump($dataOrder);exit;
			//添加到订单详细信息表
			$dataOrderInfo['order_id'] = $order_id;
			$dataOrderInfo['info'] =  '买家接受了交易，等待买家付款中 买家已冻结资金 '.$freeze .'元';
			$dataOrderInfo['time'] = $_SERVER['REQUEST_TIME'];
			D('Order_info')->data($dataOrderInfo)->add();
			$this->success('操作成功');
		}else{
			$this->error('开通失败，请重试');
		}
	}
	public function sold(){
		$Db_domains = D("Domains");
		//type =>3 -优质域名
		$domains_condition_youzhi['type'] = '3';
		$domains_condition_youzhi['status'] = '2';
		$count_domains_youzhi = $Db_domains->where($domains_condition_youzhi)->count();

	    import('@.ORG.domain_page');
	    $p = new Page($count_domains_youzhi, 24);
	    $domains_youzhi_list = $Db_domains->field(true)->where($domains_condition_youzhi)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit($p->firstRow . ',' . $p->listRows)->select();
		
		$page = $p->show();
		$this->assign('page',$page);
		$this->assign("count_domains_youzhi",$count_domains_youzhi);
		$this->assign('domains_youzhi_list', $domains_youzhi_list);

		//推荐域名
		$domains_condition_speity['type'] = '3';
		$domains_condition_speity['status']    = '1';
		$domains_condition_speity['domain_id']  = array('egt','(SELECT FLOOR(MAX(`domain_id`)*RAND()) FROM `pigcms_domains`)');
		$domains_speity_list = $Db_domains->field(true)->where($domains_condition_speity)->order('`add_time` DESC, `domain_id` DESC')->limit(10)->select();
		$this->assign('domains_speity_list',$domains_speity_list);
		
		$this->display();
	}
}