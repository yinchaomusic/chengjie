<?php
/*
 * 终端域名
 *
 */
class TerminalAction extends BaseAction{
    protected function _initialize(){
            parent::_initialize();
            if(empty($this->user_session)){
                    redirect(U('Login/index'));
            }
            if($this->user_session['staff_uid']){
                    $escrow_staff  = M('Escrow_staff');
                    $this->staff_show = $escrow_staff->where(array('id'=>$this->user_session['staff_uid']))->find();
                    $this->assign('staff_show',$this->staff_show);
                    if(IS_GET){
                            $staff_list = $escrow_staff->field('`id`,`name`,`qq`')->select();
                            $this->assign('staff_list',$staff_list);
                    }
            }
	}
        
	public function index(){
            $databases_terminal = M('Terminal');
            $id = intval($_GET['id']);
            $info = $databases_terminal->where(array('id'=>$id,'uid'=>  $this->user_session['uid']))->find();
//            echo $databases_terminal->getLastSql();
            $this->assign('info',$info);
//            dump($info);

        //经典案例
        $case_condition['cat_key'] = 'index_cases';
        $Db_adver_category = M('Adver_category');
        $get_cat_key = $Db_adver_category->field('`cat_id`,`cat_key`')->where($case_condition)->find();
        $Db_adver  = M('Adver');
        $adver_condition['cat_id'] =  $get_cat_key['cat_id'];
        $adver_condition['status'] =  1;
        $adver_list = $Db_adver->field(true)->where($adver_condition)->order('`last_time` DESC,`id` ASC')->select();
        $this->assign('adver_list',$adver_list);

        $this->display();
	}
        public function add(){
            $databases_terminal = M('Terminal');
        
            if($_POST['t_id'] > 0){
                $save_info = $databases_terminal->where(array("id"=>  intval($_POST['t_id']),"uid"=>  $this->user_session['uid']))->save($_POST);
                if($save_info){
                    echo 1;die;
                }  else {
                    echo 2;die;
                }
            }else{
                $row = $databases_terminal->where(array('domain_name'=>$_POST['domain_name'],'uid'=>  $this->user_session['uid']))->find();
                if($row){
                    echo 3;die;
                }
//                dump($row);
//                echo $databases_terminal->getLastSql();die;
                $_POST['staff_id'] = $this->user_session['staff_uid'];
                $_POST['uid'] = $this->user_session['uid'];
                $_POST['add_time'] = time();
                $info = $databases_terminal->add($_POST);
                if($info){
                    echo 1;die;
                }  else {
                    echo 2;die;
                }
            }
            

            
        }
        
        
        //审核
        public function terminal(){
            $databases_terminal = M('Terminal');
            $list = $databases_terminal->where(array('uid'=>  $this->user_session['uid']))->select();
            $this->assign('list',$list);
            $this->display();
        }
//        推荐中
        public function ongoing(){
            $databases_terminal = M('Terminal');
            $list = $databases_terminal->where(array('uid'=>  $this->user_session['uid'],'status'=>1))->select();
            $this->assign('list',$list);
            $this->display();
        }
//        推荐成功
        public function succeed(){
            $databases_terminal = M('Terminal');
            $list = $databases_terminal->where(array('uid'=>  $this->user_session['uid'],'status'=>3))->select();
//            echo $databases_terminal->getLastSql();
            $this->assign('list',$list);
//            dump($list);
            $this->display();
        }
//        推荐失败
        public function failure(){
            $databases_terminal = M('Terminal');
            $list = $databases_terminal->where(array('uid'=>  $this->user_session['uid'],'status'=>4))->select();
            $this->assign('list',$list);
//            dump($list);
            $this->display();
        }
        
//        删除
        public function delete(){
            $databases_terminal = M('Terminal');
            if($databases_terminal->where(array("id"=>  intval($_POST['id']),"uid"=>  $this->user_session['uid']))->delete()){
                echo 1;
            }  else {
                echo 2;
            }
        }
        
//        取消
        public function cancel(){
            $databases_terminal = M('Terminal');
            $data['id'] = intval($_POST['id']);
            $data['uid'] = $this->user_session['uid'];
            $data['status'] = 2;
            $data['reason'] = "您取消了本次推荐";
            $info = $databases_terminal->save($data);
            if($info){
                echo 1;
            }  else {
                echo 2;
            }
        }
        
}