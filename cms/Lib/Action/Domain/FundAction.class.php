<?php

class FundAction extends BaseAction{
	public function recharge(){
		$this->display();
	}

	//体现
	public function withdrawal(){
		$this->display();
	}

	//提现银行
	public function bankCard(){
		$this->display();
	}

	//资金记录
	public function invoice(){
		$this->display();
	}

	//冻结记录
	public function freezes(){
		$this->display();
	}
}