<?php
/*
 * web列表
 *
 */
class WebInterOrderAction extends BaseAction{
    
    
	public function editView(){
	      $price=$_GET['price'];
	      if(empty($price) || $price=='') {
	          $price=0;
	      }
	      $this->assign('price',$price);
	      
	      $WebEntrust = D('WebEntrust');
	      $list = $WebEntrust->where('wstatus=1')->order('`createdate` DESC')->limit(0,30)->select();
	      $this->assign('alList',$list);
	      $this->display();
	}
	
	
	public function add(){
	    parent::_initialize();
	    if(empty($this->user_session)){
	        $this->error('请您先登录！',U('Login/index'));
	    }
	    $data['other_user_id'] =I('post.other_user_id',0);
	    $data['inter_obj'] =I('post.inter_obj',1);//记录发起方 1=买家 2=卖家
	    $User = M('User');
	    $userCount=$User->where("uid=".$data['other_user_id'])->count();
	    if($userCount<1) {
	        $objType='';
	        if($data['inter_obj']==1){
	            $objType='卖家';
	        } else {
	            $objType='买家';
	        }
	        $this->error('您输入的'.$objType.'UID暂未在平台注册过，请先联系其来chengjie.com网注册！');
	    }
	    
	    $data['domain'] = I('post.domain');
	    $data['web_type'] = I('post.web_type',1);
	    $data['description'] =I('post.description');
	    $data['ip_app'] = I('post.ip_app',0);
	    $data['ip_pc'] =I('post.ip_pc',0);
	    $data['souce_code'] =I('post.souce_code');
	    $data['pr'] = I('post.pr',0);
	     
	    $data['month_income'] =I('post.month_income',0);
	    $data['server_info'] =I('post.server_info');
	    $data['third_party'] =I('post.third_party');
	    $data['update_type'] =I('post.update_type');
	    $data['advert_aliance'] =I('post.advert_aliance');
	    $data['aliance_acc_handle'] =I('post.aliance_acc_handle',1);
	    $data['is_help_move'] =I('post.is_help_move',1);
	    $data['price'] =I('post.price',0);
	    $data['plat_fee_obj'] =I('post.plat_fee_obj',1);
	    $data['phone'] =I('post.phone');
	    $data['createdate'] = date("Y-m-d H:i:s",time());
	    $data['user_id'] = $this->user_session['uid'];
	    $data['is_agent']=1;
	    $arr=$this->updateTotalMoney($data);
	    $data['all_price']=$arr['all_price'];
	    $data['buy_poundage']=$arr['buy_poundage'];
	    $data['sell_poundage']=$arr['sell_poundage'];
	    $WebInterOrder = D("WebInterOrder");
	    if ($WebInterOrder->create($data)) {
	        if (false !== $WebInterOrder->add()) {
	            $this->success($data['domain']);
	        } else {
	            $this->error('添加失败,请检查您的输入是否有误!');
	        }
	    } else {
	        // 字段验证错误
	        $this->error($WebInterOrder->getError());
	    }
	}
	
	/* 所有网站交易*/
	public function orderList(){
	    $database_Web_entrust = D('Web_inter_order');
	
	    $role = intval($_GET['role']);
	     
	    if ($role ==1) {
	        $where_90 = array();
	        $where_90['inter_obj'] = 1;
	        $where_90['user_id'] = $this->user_session['uid'];
	        $where_90 = M('back_order')->where($where_90)->buildSql();
	        $where_90 = strstr($where_90,'WHERE');
	        $preg_match = preg_match('/WHERE(.*)/', $where_90, $matches);
	        if($preg_match){
	            $where_90 = $matches[1];
	            $where_90 = '('.$where_90;
	        }
	        $where = array();
	        $where['inter_obj'] = 2;
	        $where['other_user_id'] = $this->user_session['uid'];
	        $map['_complex'] = $where;
	        $map['_string'] = $where_90;
	        $map['_logic'] = 'OR';
	
	    }else {
	        $where_90 = array();
	        $where_90['inter_obj'] = 2;
	        $where_90['user_id'] = $this->user_session['uid'];
	        $where_90 = M('back_order')->where($where_90)->buildSql();
	        $where_90 = strstr($where_90,'WHERE');
	        $preg_match = preg_match('/WHERE(.*)/', $where_90, $matches);
	        if($preg_match){
	            $where_90 = $matches[1];
	            $where_90 = '('.$where_90;
	        }
	        $where = array();
	        $where['inter_obj'] = 1;
	        $where['other_user_id'] = $this->user_session['uid'];
	        $map['_complex'] = $where;
	        $map['_string'] = $where_90;
	        $map['_logic'] = 'OR';
	    };
	     
	    $count_Web_entrust = $database_Web_entrust->where($map)->count();
	    import('@.ORG.system_page');
	    $p = new Page($count_Web_entrust, 1);
	    $group_list = $database_Web_entrust->field(true)->where($map)->order('`id` ASC')->limit($p->firstRow . ',' . $p->listRows)->select();
	     
	    $this->assign('role',$_GET['role']);
	    $this->assign('group_list',$group_list);
	    $pagebar = $p->show();
	    $this->assign('pagebar', $pagebar);
	    $this->display();
	     
	}
	
	//获取支付信息
	public function order_pay(){
	
	    $databases_order = M('Web_inter_order');
	    $databases_user = M('User');
	    $databases_user_freeze = M('User_freeze');
	
	    $user_info = $databases_user->where(array('uid'=>  $this->user_session['uid']))->find();
	
	    $order_info = $databases_order->where(array('id'=>  intval($_POST['order_id'])))->find();
	
	
	    $freeze_info = $databases_user_freeze->where(array('id'=>$order_info['freeze_id']))->find();
	
	    //  `plat_fee_obj '中介费支付方 1买家 2卖家 3双方各一半',
	    if(intval($_POST['role']) == 1){
	        if($order_info['plat_fee_obj']  == 1){
	            $data['poundage'] =     number_format($order_info['buy_poundage']);
	        }else if($order_info['plat_fee_obj']  == 3){
	            $data['poundage'] =   '双方各一半：' . number_format($order_info['buy_poundage']);
	        }else{
	            $data['poundage'] = '卖方支付';
	        }
	    }else {
	        if($order_info['plat_fee_obj']  == 2){
	            $data['poundage'] =     number_format($order_info['sell_poundage']);
	        }else if($order_info['plat_fee_obj']  == 3){
	            $data['poundage'] =   '双方各一半：' . number_format($order_info['sell_poundage']);
	        }else{
	            $data['poundage'] = '买方支付';
	        }
	    }
	
	    $data['now_money'] = getFriendMoney($user_info['now_money'],true);
	
	    $data['total_price'] = getFriendMoney($order_info['price'],true);
	
	    echo json_encode($data);
	}
	
	public function order_pay_data(){
	
	    $databases_order = M('Web_inter_order');
	
	    $databases_user = M('User');
	
	    $databases_user_freeze = M('User_freeze');
	
	    $databases_user_money_list = M('User_money_list');
	
	    $order_info = $databases_order->where(array('id'=>  intval($_POST['order_id'])))->find();
	
	    $user_info = $databases_user->where(array('uid'=> intval($this->user_session['uid'])))->find();
	
	    if($user_info['now_money']< $order_info['all_price']+$poundage){
	        echo 3;die;
	    }
	
	    $now_money_total = $user_info['now_money'];
	    $freezemoney_total = $user_info['freeze_money'];
	
	    if(intval($_POST['role']) == 1){
	        $minus_total = $order_info['price'] + $order_info['buy_poundage'];
	
	    }else {
	        $minus_total =  $order_info['sell_poundage'];
	    }
	
	    $money['now_money'] = $now_money_total-$minus_total;
	
	    $money['freeze_money'] = $freezemoney_total+$minus_total;
	
	    $user_save_info = $databases_user->where(array('uid'=>  $this->user_session['uid']))->save($money);
	    $user_money_list_data_two['type'] = 4;
	    $user_money_list_data_two['uid'] = $this->user_session['uid'];
	    $user_money_list_data_two['money'] = $minus_total;
	    $user_money_list_data_two['now_money'] = $now_money;
	    $user_money_list_data_two['desc'] = "购买网站支付所用";
	    $user_money_list_data_two['time'] = time();
	    $databases_user_money_list->add($user_money_list_data_two);
	    /*买方*/
	    if(intval($_POST['role']) == 1){
	        if( $order_info['plat_fee_obj'] != 1){
	            if( $order_info['sell_poun_pay'] == 1){
	                $data['status'] = 2;
	                $data['buy_price_pay'] = 1;
	                $data['buy_poun_pay'] = 1;
	                $databases_order->where(array('id'=>  intval($_POST['order_id'])))->save($data);
	                echo 1;die;
	            }else {
	                $data['buy_price_pay'] = 1;
	                $data['buy_poun_pay'] = 1;
	                $databases_order->where(array('id'=>  intval($_POST['order_id'])))->save($data);
	                echo 1;die;
	            }
	        }else {
	            $data['status'] = 2;
	            $data['buy_price_pay'] = 1;
	            $data['buy_poun_pay'] = 1;
	            $databases_order->where(array('id'=>  intval($_POST['order_id'])))->save($data);
	            echo 1;die;
	        }
	
	    }else {
	        if( $order_info['buy_price_pay'] == 1){
	            $data['status'] = 2;
	            $data['sell_poun_pay'] = 1;
	            $databases_order->where(array('id'=>  intval($_POST['order_id'])))->save($data);
	            echo 1;die;
	        }else {
	            $data['sell_poun_pay'] = 1;
	            $databases_order->where(array('id'=>  intval($_POST['order_id'])))->save($data);
	            echo 1;die;
	        }
	    }
	}
	
	/*确认收货*/
	public function sureGet(){
	    $databases_order = M('Web_inter_order');
	    $data['status'] = 3;
	    $data['buy_rece_status'] = 1;
	    $databases_order->where(array('id'=>  intval($_POST['order_id'])))->save($data);
	    echo 1;die;
	}
	
	/*确认发货*/
	public function sureSend(){
	    $databases_order = M('Web_inter_order');
	    $data['sell_rece_status'] = 1;
	    $databases_order->where(array('id'=>  intval($_POST['order_id'])))->save($data);
	    echo 1;die;
	}
	/**
	 * 计算手续费，金额
	 * @param unknown $data
	 */
	private function updateTotalMoney($data) {
	    $interObj=$data['inter_obj'];
	    $price=$data['price'];
	    $platFeeObj=$data['plat_fee_obj'];
        $fee = 0;
        if($price>0 && $price<1000) {
            $fee= price/50;
            if($fee<1){
                $fee=1;
            }
        } else {
            $fee= $price/40;
            if($fee<1){
                $fee=1;
            }
        }
        $all_price=$price+$fee;
        $buy_poundage=0;
        $sell_poundage=0;
		if($platFeeObj==1 && $interObj==1) {//买家付全部手续费
		   $buy_poundage=$fee;
			$sell_poundage=0;
		}
		else if($platFeeObj==2 && $interObj==1) {//卖家付全部手续费
			$buy_poundage=0;
			$sell_poundage=$fee;
		}
		else if($platFeeObj==3 && $interObj==1) {//买家卖家各付一半手续费
			$buy_poundage=round($fee/2,1);
			$sell_poundage=round($fee/2,1);
		}
		else if($platFeeObj==1 && $interObj==2) {//卖家发起，买家付全部手续费
			$buy_poundage=$fee;
			$sell_poundage=0;
		}
		else if($platFeeObj==2 && $interObj==2) {//卖家发起，卖家付全部手续费
			$buy_poundage=0;
			$sell_poundage=$fee;
		}
		else if($platFeeObj==3 && $interObj==2) {//卖家发起，买家卖家各付一半手续费
			$buy_poundage=round($fee/2,1);
			$sell_poundage=round($fee/2,1);
        }
       return array('buy_poundage'=>$buy_poundage,'sell_poundage'=>$sell_poundage,'all_price'=>$all_price);
    }
}