<?php
/*
 * 用户中心
 *
 */

class AccountAction extends BaseAction{
	protected $staff_show;
	protected function _initialize(){
		parent::_initialize();
		if(empty($this->user_session)){
			redirect(U('Login/index'));
		}
		$user_session = D('User')->get_user($this->user_session['uid']);
		$user_session['last'] = $this->user_session['last'];
		$this->user_session = $user_session;
		$this->assign('user_session',$this->user_session);

		if($this->user_session['staff_uid']){
			$escrow_staff  = M('Escrow_staff');
			$this->staff_show = $escrow_staff->where(array('id'=>$this->user_session['staff_uid']))->find();
			$this->assign('staff_show',$this->staff_show);
			if(IS_GET){
				$staff_list = $escrow_staff->field('`id`,`name`,`qq`')->select();
				$this->assign('staff_list',$staff_list);
			}
		}

	}
	public function _empty(){
		$this->display();
	}
        
//        我的收藏
        public function collection(){
            $databases_domain_collection = M('Domain_collection');
            $count = $databases_domain_collection->where(array('uid'=>  $this->user_session['uid']))->count();
            import('@.ORG.system_page');
            $p = new Page($count, 10);
            $list = $databases_domain_collection->where(array('uid'=>  $this->user_session['uid']))->limit($p->firstRow . ',' . $p->listRows)->select();
            $this->assign('list',$list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }
        
        public function del_collection(){
            $databases_domain_collection = M('Domain_collection');
            $databases_domain_collection->where(array('id'=>$_POST['id']))->delete();
            echo 1;
            
        }

        public function partakefast(){
            $uid =  $this->user_session['uid'];
            $count =  M()->table(array(C('DB_PREFIX').'domains_fastbid_list'=>'dolt',C('DB_PREFIX').'domains'=>'do'))
                            ->where('dolt.uid='.$uid .' AND dolt.domain_id = do.domain_id')->group('dolt.domain_id')->order('dolt.pigcms_id desc')->select();
            $count = count($count);
            import('@.ORG.system_page');
            $p = new Page($count, 10);
            $list =  M()->table(array(C('DB_PREFIX').'domains_fastbid_list'=>'dolt',C('DB_PREFIX').'domains'=>'do'))
                            ->where('dolt.uid='.$uid .' AND dolt.domain_id = do.domain_id')->limit($p->firstRow . ',' . $p->listRows)->group('dolt.domain_id')->order('dolt.pigcms_id desc')->select();
//            echo M()->getLastSql(); 
            $this->assign('list',$list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }
        
        public function partakefast_bidding(){
            $uid =  $this->user_session['uid'];
            $list =  M()->table(array(C('DB_PREFIX').'domains_fastbid_list'=>'dolt',C('DB_PREFIX').'domains'=>'doo'))
                            ->group('domain_id')->where('dolt.uid='.$uid .' AND dolt.domain_id = doo.domain_id AND doo.status =1 ')->group('dolt.domain_id')->order('dolt.pigcms_id desc')->select();
            $count = count($list);
            import('@.ORG.system_page');
            $p = new Page($count, 10);
            $list =  M()->table(array(C('DB_PREFIX').'domains_fastbid_list'=>'dolt',C('DB_PREFIX').'domains'=>'doo'))
                            ->group('domain_id')->where('dolt.uid='.$uid .' AND dolt.domain_id = doo.domain_id AND doo.status =1 ')->limit($p->firstRow . ',' . $p->listRows)->group('dolt.domain_id')->order('dolt.pigcms_id desc')->select();
//            echo M()->getLastSql();
            $this->assign('list',$list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }
        
        public function partakefast_success(){
            $uid =  $this->user_session['uid'];
            $count =  M()->table(array(C('DB_PREFIX').'domains_fastbid_list'=>'dolt',C('DB_PREFIX').'domains'=>'do'))
                            ->group('domain_id')->where('dolt.uid='.$uid .' AND dolt.domain_id = do.domain_id AND do.status = 2 AND do.lastFidId = '.$uid)->limit($p->firstRow . ',' . $p->listRows)->group('dolt.domain_id')->order('dolt.pigcms_id desc')->select();
            $count = count($count);
            import('@.ORG.system_page');
            $p = new Page($count, 10);
            $list =  M()->table(array(C('DB_PREFIX').'domains_fastbid_list'=>'dolt',C('DB_PREFIX').'domains'=>'do'))
                            ->group('domain_id')->where('dolt.uid='.$uid .' AND dolt.domain_id = do.domain_id AND do.status = 2 AND do.lastFidId = '.$uid)->limit($p->firstRow . ',' . $p->listRows)->group('dolt.domain_id')->order('dolt.pigcms_id desc')->select();
//            echo M()->getLastSql(); 
            $this->assign('list',$list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }
        
        public function partakefast_defalt(){
            $uid =  $this->user_session['uid'];
            $count = M()->table(array(C('DB_PREFIX').'domains_fastbid_list'=>'dolt',C('DB_PREFIX').'domains'=>'do'))
                            ->group('domain_id')->where('dolt.uid='.$uid .' AND dolt.domain_id = do.domain_id AND do.status = 2 AND do.lastFidId != '.$uid)->limit($p->firstRow . ',' . $p->listRows)->group('dolt.domain_id')->order('dolt.pigcms_id desc')->select();
            $count = count($count);
            import('@.ORG.system_page');
            $p = new Page($count, 10);
            $list =  M()->table(array(C('DB_PREFIX').'domains_fastbid_list'=>'dolt',C('DB_PREFIX').'domains'=>'do'))
                            ->group('domain_id')->where('dolt.uid='.$uid .' AND dolt.domain_id = do.domain_id AND do.status = 2 AND do.lastFidId != '.$uid)->limit($p->firstRow . ',' . $p->listRows)->group('dolt.domain_id')->order('dolt.pigcms_id desc')->select();
//            echo M()->getLastSql(); 
            $this->assign('list',$list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }


        public function deals(){
            $id = $this->user_session['uid'];
            if($_POST){
                if (!empty($_POST['domainName']) && $_POST['domainName'] != "请输入要查询的域名") {
                    $domainName = $_POST['domainName'];
                        $where = "AND `domainName` LIKE '%$domainName%' ";
                }
                if (!empty($_POST['startTime'])) {
                        $where    .= "AND `addTime` >= ".strtotime($_POST['startTime'])." ";
                }
                if (!empty($_POST['endTime'])) {
                    $endtime = strtotime($_POST['endTime'])+86399;
                        $where .= "AND `addTime` <= ".$endtime." ";
                }
            }
            $this->assign("state",1);
            $databases_order = M('Order');
            $where .= 'AND (`buy_id`='.$id.' OR sell_id = '.$id.')';
            if($where) $where = substr($where, 3);
            $count_order = $databases_order->where($where)->count();
            import('@.ORG.system_page');
            $p = new Page($count_order, 18);
            $order_list = $databases_order->where($where)->order('`order_id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
            $this->assign("order_list",$order_list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }
        
        public function deals_dealing(){
            $this->assign("state",2);
            $id = $this->user_session['uid'];
            if($_POST){
//                echo $_POST['domainName'];
                if (!empty($_POST['domainName']) && $_POST['domainName'] != "请输入要查询的域名") {
                    $domainName = $_POST['domainName'];
                        $where = "AND `domainName` LIKE '%$domainName%' ";
                }
                if (!empty($_POST['startTime'])) {
                        $where    .= "AND `addTime` >= ".strtotime($_POST['startTime'])." ";
                }
                if (!empty($_POST['endTime'])) {
                    $endtime = strtotime($_POST['endTime'])+86399;
                        $where .= "AND `addTime` <= ".$endtime." ";
                }
            }
            $databases_order = M('Order');
            $where .= 'AND (`buy_id`='.$id.' OR sell_id = '.$id.') AND (`status` = 0 OR `status` = 1)';
            if($where) $where = substr($where, 3);
            $count_order = $databases_order->where($where)->count();
            import('@.ORG.system_page');
            $p = new Page($count_order, 15);
            $order_list = $databases_order->where($where)->order('`order_id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
            $this->assign("order_list",$order_list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }
        //完成
        public function deals_complete(){
            $this->assign("state",3);
            $id = $this->user_session['uid'];
           if($_POST){
//                echo $_POST['domainName'];
                if (!empty($_POST['domainName']) && $_POST['domainName'] != "请输入要查询的域名") {
                    $domainName = $_POST['domainName'];
                        $where = "AND `domainName` LIKE '%$domainName%' ";
                }
                if (!empty($_POST['startTime'])) {
                        $where    .= "AND `addTime` >= ".strtotime($_POST['startTime'])." ";
                }
                if (!empty($_POST['endTime'])) {
                    $endtime = strtotime($_POST['endTime'])+86399;
                        $where .= "AND `addTime` <= ".$endtime." ";
                }
            }
            $where .= 'AND (`buy_id`='.$id.' OR sell_id = '.$id.' )  AND `status` = 2 ';
            if($where) $where = substr($where, 3);
            $databases_order = M('Order');
            $count_order = $databases_order->where($where)->count();
            import('@.ORG.system_page');
            $p = new Page($count_order, 15);
            $order_list = $databases_order->where($where)->order('`order_id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
            $this->assign("order_list",$order_list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }
        //取消失败的
        public function deals_cancel(){
            $this->assign("state",4);
            $id = $this->user_session['uid'];
            if($_POST){
//                echo $_POST['domainName'];
                if (!empty($_POST['domainName']) && $_POST['domainName'] != "请输入要查询的域名") {
                    $domainName = $_POST['domainName'];
                        $where = "AND `domainName` LIKE '%$domainName%' ";
                }
                if (!empty($_POST['startTime'])) {
                        $where    .= "AND `addTime` >= ".strtotime($_POST['startTime'])." ";
                }
                if (!empty($_POST['endTime'])) {
                    $endtime = strtotime($_POST['endTime'])+86399;
                        $where .= "AND `addTime` <= ".$endtime." ";
                }
            }
            $where .= 'AND (`buy_id`='.$id.' OR sell_id = '.$id.' )  AND `status` = 3 ';
            if($where) $where = substr($where, 3);
//            echo $where;
            $databases_order = M('Order');
            $count_order = $databases_order->where($where)->count();
            import('@.ORG.system_page');
            $p = new Page($count_order, 15);
            $order_list = $databases_order->where($where)->order('`order_id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
            $this->assign("order_list",$order_list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }



        //获取支付信息
        public function order_pay(){
            $databases_order = M('Order');
            $databases_user = M('User');
            $databases_user_freeze = M('User_freeze');
            $user_info = $databases_user->where(array('uid'=>  $this->user_session['uid']))->find();
            $order_info = $databases_order->where(array('order_id'=>  intval($_POST['order_id'])))->find();

            $get_buy_uid = $databases_user->where(array('uid'=>$order_info['buy_id']))->getField('uid');

            $freeze_info = $databases_user_freeze->where(array('id'=>$order_info['freeze_id']))->find();

            //  `payTye` tinyint(2) NOT NULL COMMENT '中介费支付方 1买家 2卖家 3双方各一半',
            if($order_info['payTye']  == 1){
                $user_level_info = $this->user_Level($user_info['score_count']);
                $boon = $user_level_info['boon']/100;
                $poundage = $order_info['total_price']*$boon;
                $data['poundage'] =     number_format($poundage);
            }else if($order_info['payTye']  == 3){
                $user_level_info = $this->user_Level($user_info['score_count']);
                $boon = $user_level_info['boon']/100;
                $poundage = ($order_info['total_price'] *$boon) /2 ;
                $data['poundage'] =   '双方各一半：' . number_format($poundage);
            }else{
                $data['poundage'] = '卖方支付';
            }



            $data['now_money'] = getFriendMoney($user_info['now_money'],true);
            $data['total_price'] = getFriendMoney($order_info['total_price'],true);

            $data['freezemoney'] = getFriendMoney($freeze_info['freezemoney'],true);
            echo json_encode($data);
        }

    public function order_pay_data(){
        $databases_order = M('Order');
        $databases_order_info = M('Order_info');
        $databases_user = M('User');
        $databases_user_freeze = M('User_freeze');
        $databases_user_money_list = M('User_money_list');

        $order_info = $databases_order->where(array('order_id'=>  intval($_POST['order_id'])))->find();
        //$freeze_info = $databases_user_freeze->where(array('id'=>$order_info['freeze_id']))->find();
        $user_info = $databases_user->where(array('uid'=> intval($this->user_session['uid'])))->find();

        $user_level_info = $this->user_Level($user_info['score_count']);
        $boon = $user_level_info['boon']/100;
        $poundage = $order_info['total_price']*$boon;

        if($user_info['now_money']< $order_info['total_price']+$poundage){
            //  echo "钱不够：".__LINE__ ."<br/>";
            echo 3;die;
        }





            $now_money_total = $user_info['now_money'];
            $minus_total = $order_info['total_price'] + $poundage;
            $now_money = $now_money_total-$minus_total;
            $user_save_info = $databases_user->where(array('uid'=>  $this->user_session['uid']))->setField('now_money',$now_money);

            if($user_save_info){
                $user_money_list_data_two['type'] = 4;
                $user_money_list_data_two['uid'] = $this->user_session['uid'];
                $user_money_list_data_two['money'] = $minus_total;
                $user_money_list_data_two['now_money'] = $now_money;
                $user_money_list_data_two['desc'] = "购买支付所用";
                $user_money_list_data_two['time'] = time();
                $databases_user_money_list->add($user_money_list_data_two);

                $order_data['yes_no'] = 4;
                $order_data['transfer_type'] = $_POST['transfer_type'];
                $order_data['registrar'] = $_POST['registrar'];
                $order_data['registrar_id'] = $_POST['registrar_id'];
                $order_data['order_id'] = $_POST['order_id'];

                $order_save_info = $databases_order->save($order_data);
                if($order_save_info){
                    $order_info_data['order_id'] = intval($_POST['order_id']);
                    $order_info_data['info'] = "买家已付款，等待卖家提供域名";
                    $order_info_data['time'] = time();
                    $databases_order_info->add($order_info_data);
                    echo 1;die;
                }  else {
                    //echo "保存失败". __LINE__ ;
                    echo 2;die;
                }
            }  else {
                // echo "用户保存失败".__LINE__ ;
                echo 2;die;
            }


    }
    /*
        //订单支付
        public function order_pay_data(){
            $databases_order = M('Order');
            $databases_order_info = M('Order_info');
            $databases_user = M('User');
            $databases_user_freeze = M('User_freeze');
            $databases_user_money_list = M('User_money_list');

            $order_info = $databases_order->where(array('order_id'=>  intval($_POST['order_id'])))->find();
            $freeze_info = $databases_user_freeze->where(array('id'=>$order_info['freeze_id']))->find();
            $user_info = $databases_user->where(array('uid'=> intval($this->user_session['uid'])))->find();

            $user_level_info = $this->user_Level($user_info['score_count']);
            $boon = $user_level_info['boon']/100;
            $poundage = $order_info['total_price']*$boon;

            if($user_info['now_money']+$freeze_info['freezemoney'] < $order_info['total_price']+$poundage){
             //  echo "钱不够：".__LINE__ ."<br/>";
                echo 3;die;
            }

            $user_freeze_data['thawtime'] = time();
            $user_freeze_data['status'] = 1;
            //dump($order_info);
            $order_freeze_info = $databases_user_freeze->where(array('id'=>$order_info['freeze_id']))->save($user_freeze_data);

            if($order_freeze_info){
                $user_money_list_data['type'] = 3;
                $user_money_list_data['uid'] = $this->user_session['uid'];
                $user_money_list_data['desc'] = "冻结资金释放";
                $user_money_list_data['money'] = $freeze_info['freezemoney'];
                $user_money_list_data['now_money'] = $freeze_info['freezemoney']+$user_info['now_money'];
                $user_money_list_data['time'] = time();
                $databases_user_money_list->add($user_money_list_data);


                $now_money_total = $freeze_info['freezemoney']+$user_info['now_money'];
                $minus_total = $order_info['total_price']+$poundage;
                $now_money = $now_money_total-$minus_total;
                $user_save_info = $databases_user->where(array('uid'=>  $this->user_session['uid']))->setField('now_money',$now_money);

                if($user_save_info){
                    $user_money_list_data_two['type'] = 4;
                    $user_money_list_data_two['uid'] = $this->user_session['uid'];
                    $user_money_list_data_two['money'] = $minus_total;
                    $user_money_list_data_two['now_money'] = $now_money;
                    $user_money_list_data_two['desc'] = "购买支付所用";
                    $user_money_list_data_two['time'] = time();
                    $databases_user_money_list->add($user_money_list_data_two);

                    $order_data['yes_no'] = 4;
                    $order_data['transfer_type'] = $_POST['transfer_type'];
                    $order_data['registrar'] = $_POST['registrar'];
                    $order_data['registrar_id'] = $_POST['registrar_id'];
                    $order_data['order_id'] = $_POST['order_id'];

                    $order_save_info = $databases_order->save($order_data);
                    if($order_save_info){
                        $order_info_data['order_id'] = intval($_POST['order_id']);
                        $order_info_data['info'] = "买家已付款，等待卖家提供域名";
                        $order_info_data['time'] = time();
                        $databases_order_info->add($order_info_data);
                        echo 1;die;
                    }  else {
                        //echo "保存失败". __LINE__ ;
                        echo 2;die;
                    }
                }  else {
                   // echo "用户保存失败".__LINE__ ;
                    echo 2;die;
                }

            }  else {
               // echo "冻结保存失败".__LINE__ ;
                echo 2;die;
            }
        }

*/

//        确认收货
        public function successful (){
            $databases_order = M('Order');
            $databases_order_info = M('Order_info');
            $databases_user = M('User');
            $order_info = $databases_order->where(array('order_id'=>$_GET['order_id']))->find();

            $order_data['order_id'] = intval($_GET['order_id']);
            $order_data['yes_no'] = 10;
            $order_data['status'] = 2;
            $order_data['up_time'] = time();

            $score_count = $order_info['total_price']*$this->config['user_score_get'];


            $order_back = $databases_order->save($order_data);

            if($order_back){
                $order_info_data['order_id'] = intval($_GET['order_id']);
                $order_info_data['info'] = "买家确认收货，交易达成";
                $order_info_data['time'] = time();
                $databases_order_info->add($order_info_data);

                if($order_info['buy_id']){
                    $databases_user->where(array('uid'=>$order_info['buy_id']))->setInc('score_count',$score_count);
                }

                if ($order_info['sell_id']) {
                    $databases_user->where(array('uid'=>$order_info['sell_id']))->setInc('score_count',$score_count);
                }

                if($order_info['trade_type'] == 0){
                    $databases_domains = M('Domains');
                    $databases_domains->where(array('domain_id'=>$order_info['domain_id']))->setField('status',3);
                }  elseif ($order_info['trade_type'] == 2) {
                    $databases_bulk_trading = M('Bulk_trading');
                    $databases_bulk_trading->where(array('bulk_id'=>$order_info['domain_id']))->setField('status',3);
                }  elseif ($order_info['trade_type'] == 3) {

                }
                $this->success('交易达成');
            }

        }
        public function pay(){
            $databases_purchase = M('Purchase');
            $list = $databases_purchase->where(array('uid'=>  $this->user_session['uid']))->select();
            $this->assign('list',$list);
//            dump($list);
//            dump($list);
            $this->display();
        }

        public function pay_edit(){
            $databases_purchase = M('Purchase');
            if($_POST){
                if($_POST['domain'] == "" || $_POST['buyers_price'] == ""){
                    $this->error('信息不可以为空');
                }
                $row = $databases_purchase->save($_POST);
                if($row){
                    $this->success('修改成功');die;
                }  else {
                    $this->error('支付失败，异常');die;
                }

            }

                $ph_info = $databases_purchase->where(array('ph_id'=>$_GET['ph_id']))->find();
                $this->assign('ph_info',$ph_info);
                $this->display();

        }
        //缴纳保证金
        public function pay_margin(){
            $databases_purchase = M('Purchase');
            $databases_user = M('User');
            $user_info = $databases_user->where(array('uid'=> intval($this->user_session['uid'])))->find();
            getFriendMoney($money);
            $ph_info = $databases_purchase->where(array('ph_id'=>  intval($_POST['ph_id'])))->find();
            $ph_info['now_money'] =number_format($user_info['now_money']);
            $ph_info['uid'] = $user_info['uid'];
            $ph_info['margin'] = number_format($ph_info['buyers_price']*0.08);
            echo $json = json_encode($ph_info);
        }

        //委托代购保证金
        public function freeze_margin(){

            $databases_purchase = M('Purchase');
            $databases_user = M('User');
            $databases_user_freeze = M('User_freeze');
            $databases_user_money_list = M('User_money_list');
            $user_info = $databases_user->where(array('uid'=>  $this->user_session['uid']))->find();

//            echo $user_info['now_money'];
//            die;
            $data['info'] = "委托代购保证金冻结资金";
            $data['type'] = 3;
            $data['freezetime'] = time();
            $data['uid'] = $_POST['uid'];
            $data['freezemoney'] = $_POST['margin'];
            $user_money_list_data['type'] = 13;
            $user_money_list_data['uid'] = $_POST['uid'];
            $user_money_list_data['money'] = $_POST['margin'];
            $user_money_list_data['desc'] = "委托代购保证金冻结";
            $user_money_list_data['now_money'] = $user_info['now_money']-$_POST['margin'];
            $user_money_list_data['time'] = time();

            $u_info = $databases_user->where(array('uid'=>$_POST['uid']))->setDec('now_money',$_POST['margin']); // 用户的金额减少
            $databases_user->where(array('uid'=>$_POST['uid']))->setInc('freeze_money',$_POST['margin']); // 用户的冻结金额增加

            if($u_info){
                $f_info = $databases_user_freeze->add($data);
                if($f_info){
                    $p_info = $databases_purchase-> where(array("ph_id"=>$_POST['ph_id']))->setField(array('status'=>4,'freeze_id'=>$f_info));
//                    echo $databases_purchase->getLastSql();
                    if($p_info){
                        $databases_user_money_list->add($user_money_list_data);
                        echo 1;
                    }  else {
                        echo 2;
                    }
                }  else {
                    echo 2;
                }
            }  else {
                echo 2;
            }
        }

        //删除代购
        public function pay_del(){
            $databases_purchase = M('Purchase');
            $databases_purchase->where(array('ph_id'=>$_GET['ph_id'],'uid'=>  $this->user_session['uid']))->delete();
            $this->success('删除成功');
        }
        //代购中
        public function paying(){
            $databases_purchase = M('Purchase');
            $where = "`uid` = ".$this->user_session['uid']." AND status = 4 OR status = 5 OR status = 8 OR status = 9";
            $count_paying = $databases_purchase->where($where)->count();
            import('@.ORG.system_page');
            $p = new Page($count_paying, 15);
            $paying_list = $databases_purchase->where($where)->order('`ph_id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
//            echo $databases_purchase->getLastSql();
            $this->assign('paying_list',$paying_list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }
        //代购付款
        public function payment(){
            $databases_purchase = M('Purchase');
            $databases_user = M('User');
            $databases_user_freeze = M('User_freeze');
            $databases_user_money_list = M('User_money_list');
            $ph_info = $databases_purchase->where(array('ph_id'=>  intval($_POST['ph_id'])))->find();
            $user_info = $databases_user->where(array('uid'=> intval($this->user_session['uid'])))->find();
            $ph_info['margin'] = $ph_info['buyers_price']*0.08;
            $ph_info['poundage'] = $ph_info['buyers_price']*0.08;
            $ph_info['again_pay'] = $ph_info['buyers_price']+$ph_info['poundage'];
            $user_info['now_money'] = $user_info['now_money']+$ph_info['margin'];
            if($user_info['now_money'] < $ph_info['again_pay']){
                echo 3;//资金不足
                die;
            }
            $user_money_list_data['type'] = 13;
            $user_money_list_data['uid'] = $this->user_session['uid'];
            $user_money_list_data['money'] = $ph_info['margin'];
            $user_money_list_data['desc'] = "委托代购保证金解冻";
            $user_money_list_data['now_money'] = $user_info['now_money'];
            $user_money_list_data['time'] = time();
            $u_m_l_d = $databases_user_money_list->add($user_money_list_data);

            if($u_m_l_d){
            }  else {
                echo 2;
            }
            $user_money_list_info['type'] = 4;
            $user_money_list_info['uid'] = $this->user_session['uid'];
            $user_money_list_info['money'] = $ph_info['again_pay'];
            $user_money_list_info['desc'] = "委托代购买家付款记录";
            $user_money_list_info['now_money'] = $user_info['now_money']-$ph_info['again_pay'];
            $user_money_list_info['time'] = time();
            $u_m_l_i = $databases_user_money_list->add($user_money_list_info);
            if($u_m_l_i){
            }  else {
                echo 2;
            }
            $user_data['uid'] = $this->user_session['uid'];
            $user_data['now_money'] = $user_info['now_money']-$ph_info['again_pay'];
            $user_data['freeze_money'] = $user_info['freeze_money']-$ph_info['margin'];

            $data['id'] = $ph_info['freeze_id'];
            $data['thawtime'] = time();
            $data['status'] = 1;
            $user_row = $databases_user->save($user_data);
            if($user_row){
                $freeze_row = $databases_user_freeze->save($data);
                if($freeze_row){
                    $ph_row = $databases_purchase->where(array('ph_id'=>  intval($_POST['ph_id'])))->setField('status',5);
                    if($ph_row){
                        echo 1;die;
                    }else{
                        echo 2;die;
                    }
                }  else {
                    echo 2;die;
                }
            }  else {
                echo 2;die;
            }
        }
        //代购付款信息
        public function payment_data(){
            $databases_purchase = M('Purchase');
            $databases_user = M('User');
            $databases_user_freeze = M('User_freeze');
            $user_info = $databases_user->where(array('uid'=> intval($this->user_session['uid'])))->find();
            $ph_info = $databases_purchase->where(array('ph_id'=>  intval($_POST['ph_id'])))->find();
            $ph_info['now_money'] = $user_info['now_money'];
            $ph_info['uid'] = $user_info['uid'];
            $ph_info['margin'] = $ph_info['buyers_price']*0.08;
            $ph_info['again_pay'] = $ph_info['buyers_price'];
            $ph_info['poundage'] = $ph_info['buyers_price']*0.08;
            echo $json = json_encode($ph_info);
        }

        //代购取消
        public function pay_undo(){
            $ph_id = $_GET['ph_id'];
            $databases_user_freeze = M('User_freeze');
            $databases_purchase = M('Purchase');
            $freeze_info = $databases_user_freeze->where(array('id'=>  intval($_GET['freeze_id'])))->setField('status',2);
//            echo $databases_user_freeze->getLastSql();
//            dump($freeze_info);die;

//            $user_money_list_data['type'] = 6;
//            $user_money_list_data['uid'] = $this->user_session['uid'];
//            $user_money_list_data['money'] = $ph_info['margin'];
//            $user_money_list_data['desc'] = "委托代购保证金因用户取消变更为违约金";
//            $user_money_list_data['now_money'] = $user_info['now_money'];
//            $user_money_list_data['time'] = time();
//            $u_m_l_d = $databases_user_money_list->add($user_money_list_data);
//
//
            if($freeze_info){

                $ph_info = $databases_purchase->save($_GET);
                $_GET['status'] = 7;
                $_GET['why'] = "用户取消了委托代购";
                $ph_info = $databases_purchase->where(array('ph_id'=>  intval($_GET['ph_id'])))->setField('status',7);
                if($ph_info){
                    $this->success('取消成功，冻结资金已作为违约金');
                }  else {
                    $this->error('取消异常');
                }
            }

        }


        //代购域名确认收到域名更改状态
        public function pay_confirm(){
            $databases_purchase = M('Purchase');
            $databases_user = M('User');
            $ph_info = $databases_purchase->where(array('ph_id'=>  intval($_POST['ph_id'])))->find();
            $user_info = $databases_user->where(array('uid'=> intval($this->user_session['uid'])))->find();
            $ph_info['poundage'] = $ph_info['buyers_price']*0.08;
            $ph_info['again_pay'] = $ph_info['buyers_price']+$ph_info['poundage'];
            $score_count = $ph_info['again_pay']*$this->config['meal_score'];

            $databases_user->where(array('uid'=>  $this->user_session['uid']))->setInc('score_count',$score_count);
            $databases_purchase = M('Purchase');
            $info  = $databases_purchase->where(array('ph_id'=>  intval($_POST['ph_id'])))->setField("status",6);
            if($info){
                echo 1;
            }  else {
                echo 2;
            }
        }

        //paysuccess 代购成功
        public function paysuccess(){
            $databases_purchase = M('Purchase');
            $count_paysuccess = $databases_purchase->where(array('uid'=>$this->user_session['uid'],'status'=>6))->count();
            import('@.ORG.system_page');
            $p = new Page($count_paysuccess, 15);
            $paysuccess_list = $databases_purchase->where(array('uid'=>$this->user_session['uid'],'status'=>6))->order('`ph_id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
            $this->assign('paysuccess_list',$paysuccess_list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }
        //payend 代购失败
        public function payend(){
            $databases_purchase = M('Purchase');
            $count_payend = $databases_purchase->where(array('uid'=>$this->user_session['uid'],'status'=>7))->count();
            import('@.ORG.system_page');
            $p = new Page($count_payend, 15);
            $payend_list = $databases_purchase->where(array('uid'=>$this->user_session['uid'],'status'=>7))->order('`ph_id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
            $this->assign('payend_list',$payend_list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }

    public function index(){
//        dump($_SESSION);
		$databases_order = M('Order');
		$id = $this->user_session['uid'];
		$where = '(`buy_id`='.$id.' OR sell_id = '.$id.') AND (`status` = 0 OR `status` = 1)';
		$order_list = $databases_order->field(true)->where($where)->order("order_id DESC")->select();
//                echo $databases_order->getLastSql();
//		dump($order_list);
		$count = count($order_list);
		$this->assign('count',$count);
		foreach ($order_list as $k=>$v){
			if($v['buy_id'] == $id){
				$order_list[$k]['behavior'] = "1";
			}  elseif ($v['sell_id'] == $id) {
				$order_list[$k]['behavior'] = "2";
			}
		}
		$this->assign('order_list',$order_list);

		//我的出价
		$databaseQuote = D('Domains_quote');
		$conditionBuyQuote['buy_uid'] = $this->user_session['uid'];
		$conditionBuyQuote['status'] = '0';
		$buyQuoteList = $databaseQuote->field(true)->where($conditionBuyQuote)->order('`last_time` DESC')->limit(5)->select();
		$buyQuoteCount = count($buyQuoteList);
		if($buyQuoteCount == 5){
			$buyQuoteCount = $databaseQuote->where($conditionBuyQuote)->count();
		}
		// dump($buyQuoteList);
		$this->assign('buyQuoteList',$buyQuoteList);
		$this->assign('buyQuoteCount',$buyQuoteCount);

		//收到的报价
		$databaseQuote = D('Domains_quote');
		$conditionQuote['sell_uid'] = $this->user_session['uid'];
		$conditionQuote['status'] = '0';
		$sellQuoteList = $databaseQuote->field(true)->where($conditionQuote)->order('`last_time` DESC')->limit(5)->select();
		$sellQuoteCount = count($sellQuoteList);
		if($sellQuoteCount == 5){
			$sellQuoteCount = $databaseQuote->where($conditionQuote)->count();
		}
		$this->assign('sellQuoteCount',$sellQuoteCount);
		$this->assign('sellQuoteList',$sellQuoteList);


	     // 登录ip，地理位置，时间
	     $Db_loginlog = M('Loginlog');
	     $log_condition['uid'] = $this->user_session['uid'];
	     $login_info = $Db_loginlog->field(true)->where($log_condition)->order('`log_time` DESC ,`log_id` DESC')->find();
	     $this->assign('login_info',$login_info);
		 $this->display();
	}
	/*我收到的报价*/
	public function mysell(){
		$databaseQuote = D('Domains_quote');
		$conditionQuote['sell_uid'] = $this->user_session['uid'];
		if($_POST['type'] !== ''){
			$conditionQuote['status'] = $_POST['type'];
		}
		if($_POST['domain']){
			$conditionQuote['domain'] = array('LIKE','%'.$_POST['domain'].'%');
		}
		if($_POST['t1'] && $_POST['t2']){
			$conditionQuote['last_time'] = array(array('egt',strtotime($_POST['t1'].' 0:00:00')),array('elt',strtotime($_POST['t2'].' 23:59:59')));
		}else if($_POST['t1']){
			$conditionQuote['last_time'] = array('egt',strtotime($_POST['t1'].' 0:00:00'));
		}else if($_POST['t2']){
			$conditionQuote['last_time'] = array('elt',strtotime($_POST['t2'].' 23:59:59'));
		}
		$return['list'] = $databaseQuote->field(true)->where($conditionQuote)->order('`last_time` DESC')->limit((($_POST['page']-1)*20).',20')->select();
		// dump($databaseQuote);
		if(!empty($return['list'])){
			foreach($return['list'] as &$quoteValue){
				$quoteValue['last_time'] = date('Y-m-d H:i:s',$quoteValue['last_time']);
			}
		}else{
			$return['list'] = array();
		}
		$return['total'] = $databaseQuote->where($conditionQuote)->count();
		$this->success($return);
	}
    /*我给出的报价*/
	public function mybuy(){
		$databaseQuote = D('Domains_quote');
		$conditionQuote['buy_uid'] = $this->user_session['uid'];
		if($_POST['type'] !== ''){
			$conditionQuote['status'] = $_POST['type'];
		}
		if($_POST['domain']){
			$conditionQuote['domain'] = array('LIKE','%'.$_POST['domain'].'%');
		}
		if($_POST['t1'] && $_POST['t2']){
			$conditionQuote['last_time'] = array(array('egt',strtotime($_POST['t1'].' 0:00:00')),array('elt',strtotime($_POST['t2'].' 23:59:59')));
		}else if($_POST['t1']){
			$conditionQuote['last_time'] = array('egt',strtotime($_POST['t1'].' 0:00:00'));
		}else if($_POST['t2']){
			$conditionQuote['last_time'] = array('elt',strtotime($_POST['t2'].' 23:59:59'));
		}
		$return['list'] = $databaseQuote->field(true)->where($conditionQuote)->order('`last_time` DESC')->limit((($_POST['page']-1)*20).',20')->select();
		// dump($databaseQuote);
		if(!empty($return['list'])){
			foreach($return['list'] as &$quoteValue){
				$quoteValue['last_time'] = date('Y-m-d H:i:s',$quoteValue['last_time']);
			}
		}else{
			$return['list'] = array();
		}
		$return['total'] = $databaseQuote->where($conditionQuote)->count();
		$this->success($return);
	}
	public function sellnegotiation_detail(){
		$quote_id = $_GET['id'];
		$condition_quote['quote_id'] = $quote_id;
		$condition_quote['sell_uid'] = $this->user_session['uid'];
		$now_quote = D('Domains_quote')->field(true)->where($condition_quote)->find();
		if(empty($now_quote)){
			$this->error('当前报价域名不存在');
		}
		$now_quote['ok_shouxufei'] = round($now_quote['new_money']*$this->user_level_info['boon']/100);
		$now_quote['ok_money'] = $now_quote['new_money'] - $now_quote['ok_shouxufei'];
		$this->assign('now_quote',$now_quote);

		//得到当前域名的信息
		$conditionDomains['domain_id'] = $now_quote['domain_id'];
		// $conditionDomains['status'] = '1';
		$nowDomain = D('Domains')->field('`money`')->where($conditionDomains)->find();
		if(empty($nowDomain)){
			$this->error('当前域名不存在或已被删除');
		}
		$nowDomain['money'] = getFriendMoney($nowDomain['money']);
		$this->assign('nowDomain',$nowDomain);


		$quote_list = D('Domains_quote_list')->field(true)->where($condition_quote)->order('`list_id` DESC')->select();
		$this->assign('quote_list',$quote_list);

		$this->display();
	}
	public function updatenegotiation(){
		$quote_id = $_POST['id'];
		$condition_quote['quote_id'] = $quote_id;
		$now_quote = D('Domains_quote')->field(true)->where($condition_quote)->find();
		if(empty($now_quote)){
			$this->error('当前报价域名不存在');
		}
		if($this->user_session['uid'] != $now_quote['buy_uid'] && $this->user_session['uid'] != $now_quote['sell_uid']){
			$this->error('您没有权限操作此报价单');
		}

		switch($_POST['type']){
			case '1':
				if(($this->user_session['uid'] == $now_quote['buy_uid'] && $now_quote['last_offer'] == 1) || ($this->user_session['uid'] == $now_quote['sell_uid'] && $now_quote['last_offer'] == 0)){
					$dataQuote = array(
						'last_time'=>$_SERVER['REQUEST_TIME'],
						'last_offer' => $this->user_session['uid'] == $now_quote['buy_uid'] ? '0' : '1',
						'status' => '1',
					);
                   // file_put_contents('typessss.log',print_r($_POST,true));die;
					if(D('Domains_quote')->where($condition_quote)->data($dataQuote)->save()){

						D('Domains')->where(array('domain_id'=>$now_quote['domain_id']))->data(array('status'=>'2'))->save();

						$dataOrder['addTime'] = $_SERVER['REQUEST_TIME'];
						$dataOrder['total_price'] = $now_quote['new_money'];
						$dataOrder['domain_id'] = $now_quote['domain_id'];
						$dataOrder['domainName'] = $now_quote['domain'];
						$dataOrder['trade_type'] = '0';
						$dataOrder['status'] = '0';
						$dataOrder['buy_id'] = $now_quote['buy_uid'];
						$dataOrder['sell_id'] = $now_quote['sell_uid'];
						$dataOrder['staff_id'] = '0';
						$dataOrder['payTye'] = '2';
						$dataOrder['yes_no'] = '3';
						$order_id = D('Order')->data($dataOrder)->add();

						$dataOrderInfo['order_id'] = $order_id;
						$dataOrderInfo['info'] = $this->user_session['uid'] == $now_quote['buy_uid'] ? '买家接受了交易，等待买家付款中' : '卖家接受了交易，等待买家付款中';
						$dataOrderInfo['time'] = $_SERVER['REQUEST_TIME'];
						D('Order_info')->data($dataOrderInfo)->add();
                        echo json_encode(array('status'=>1,'info'=>'操作成功'));
						//$this->success('操作成功');
					}else{
                        echo json_encode(array('status'=>-1,'info'=>'操作失败'));
                       // $this->error('操作失败');
					}
				}else{
                    echo json_encode(array('status'=>-1,'info'=>'您没有权限操作此报价单'));
                    //$this->error('您没有权限操作此报价单');
				}
				break;
			case '2':
				$money = intval($_POST['money']);
				if($money <= $now_quote['new_money']){
					$this->error('新的报价不能低于老的报价');
				}
				$dataQuote = array(
					'new_money'=>$money,
					'last_time'=>$_SERVER['REQUEST_TIME'],
					'invalid_time'=>$_SERVER['REQUEST_TIME'] + 604800,
					'last_offer' => $this->user_session['uid'] == $now_quote['buy_uid'] ? '0' : '1',
				);
				if(D('Domains_quote')->where($condition_quote)->data($dataQuote)->save()){
					$dataQuoteList['quote_id'] = $quote_id;
					$dataQuoteList['is_sell'] = $this->user_session['uid'] == $now_quote['buy_uid'] ? '0' : '1';
					$dataQuoteList['money'] = $money;
					$dataQuoteList['time'] = $_SERVER['REQUEST_TIME'];
					D('Domains_quote_list')->data($dataQuoteList)->add();


                    if($this->user_session['uid'] == $now_quote['buy_uid']){
                        //通知卖方
                        $phone =  D('User')->where(array('uid'=>$now_quote['sell_uid']))->getField('phone');
                        $send_uid = $now_quote['sell_uid'];
                    }else{
                        //通知买方
                        $phone =  D('User')->where(array('uid'=>$now_quote['buy_uid']))->getField('phone');
                        $send_uid =$now_quote['buy_uid'];
                    }

                    if(!empty($phone)){
                        $text = '您好，【' .$now_quote['domain'] .'】有新的报价【￥'.$money.' 元】，请您及时登录系统查看！';
                        $send_time = time();
                        Sms::sendSms(array( 'content' => $text, 'mobile' => $phone, 'uid' =>$send_uid, 'type' => 'domains_user', 'send_time'=>$send_time,'expire_time'=>'','extra'=>''));
                    }


					$this->success('报价成功');
				}else{
					$this->error('报价失败');
				}
				break;
			case '3':
				$dataQuote = array(
					'status' => '2',
					'last_offer' => '1',
					'last_time'=>$_SERVER['REQUEST_TIME'],
				);
				if(D('Domains_quote')->where($condition_quote)->data($dataQuote)->save()){
					$this->success('取消成功');
				}else{
					$this->error('取消失败');
				}
				break;
		}
	}
	public function negotiation_detail(){
		$quote_id = $_GET['id'];
		$condition_quote['quote_id'] = $quote_id;
		$condition_quote['buy_uid'] = $this->user_session['uid'];
		$now_quote = D('Domains_quote')->field(true)->where($condition_quote)->find();
		if(empty($now_quote)){
			$this->error('当前报价域名不存在');
		}
		$this->assign('now_quote',$now_quote);

		//得到当前域名的信息
		$conditionDomains['domain_id'] = $now_quote['domain_id'];
		// $conditionDomains['status'] = '1';
		$nowDomain = D('Domains')->field('`money`')->where($conditionDomains)->find();
		if(empty($nowDomain)){
			$this->error('当前域名不存在或已被删除');
		}
		$nowDomain['money'] = getFriendMoney($nowDomain['money']);
		$this->assign('nowDomain',$nowDomain);


		$quote_list = D('Domains_quote_list')->field(true)->where($condition_quote)->order('`list_id` DESC')->select();
		$this->assign('quote_list',$quote_list);

		$this->display();
	}
        public function dealdetails(){
            $id = $this->user_session['uid'];
            $deal_id = intval($_GET['id']);
            $databases_order = M('Order');
            $databases_order_info = M('Order_info');
            $order_info = $databases_order->where(array('order_id'=>$deal_id))->find();
            $order_info_list = $databases_order_info->where(array('order_id'=>$deal_id))->order('id DESC')->select();
            if($order_info['buy_id'] == $id){
                    $order_info['behavior'] = "1";
                    if($order_info['payTye'] == 1){
                        $order_info['my_payTye'] = "1";//为1 是我自己支付  为2是各付50
                    }  elseif ($order_info['payTye'] == 3) {
                        $order_info['my_payTye'] = "2";
                    }
            }else if ($order_info['sell_id'] == $id) {
                    $order_info['behavior'] = "2";
                    if($order_info['payTye'] == 2){
                        $order_info['my_payTye'] = "1";//为1 是我自己支付  为2是各付50
                    }  elseif ($order_info['payTye'] == 3) {
                        $order_info['my_payTye'] = "2";
                    }
            }
//                dump($_SESSION['user']);
            //dump($order_info);
            $this->assign('order_info',$order_info);
            $this->assign('order_info_list',$order_info_list);
           //dump($order_info_list);
		$this->display();
	}

    public function change_dealdetails(){
        if(IS_POST){
            //1卖家提供域名转移密码，2当前注册商内转移(PUSH)',
          //   file_put_contents('xxxxxxxxxxxxxxxxxxxxxxxxxxxx',print_r($_POST,true));
            $Db_order = M('Order');
            $_POST['order_id'] =  intval($_POST['order_id']);
            $_POST['yes_no'] =  9;
            if($_POST['transfer_type'] == 1){
                $_POST['registrar'] =  strval($_POST['registrar']);
                $_POST['registrar_id'] =  strval($_POST['registrar_id']);
            }

                if($Db_order->data($_POST)->save()){
                    $Db_order_info = M('Order_info');
                    $info['order_id'] =$_POST['order_id'];
                    $info['info'] =  '卖家已提交域名等待买家确认';
                    $info['time'] = time();
                    $Db_order_info->data($info)->add();
                    exit(json_encode(array('error'=>'0','msg'=>'域名提交成功。')));
                }else{
                    exit(json_encode(array('error'=>'5','msg'=>'操作失败，请重新操作。')));
                }

        }
    }

        public function el_yes_no(){
            $databases_order = M('Order');
            $databases_order_info = M('Order_info');
            $info['order_id'] = intval($_POST['order_id']);
            $info['time'] = time();
            $info['info'] = $_POST['info'];
            $databases_order_info->add($info);
            $order_save['yes_no'] = intval($_POST['yes_no']);

            $info = $databases_order->where(array('order_id'=>intval($_POST['order_id'])))->save($order_save);
//            echo $databases_order->getLastSql();
//            die;
            if($info){
                echo 1;
            }  else {
                echo 2;
            }
        }

//        买卖双方修改信息
        public function dealdetailsInfo(){
            $databases_order = M('Order');
            $databases_order_info = M('Order_info');
            $data['order_id'] = $_POST['id'];
            $data['payTye'] = $_POST['payTye'];
            if($_POST['up'] == 'buy'){
                $data['yes_no'] = 2;
                $info['info'] = "买家修改了交易条款，等待卖家同意条款";
            }  else {
                $data['yes_no'] = 1;
                $info['info'] = "卖家修改了交易条款，等待买家同意条款";
            }
            $info['order_id'] = intval($_POST['id']);
            $info['time'] = time();
            $databases_order_info->add($info);
            $row = $databases_order->save($data);
//            echo $databases_escrow_list->getLastSql();
//            die;
            if($row){
                $this->success('成功');
            }  else {
                $this->success('失败');
            }
        }


	public function ktOperation(){
		if(IS_POST){
			if($_POST['action'] == 'open'){
				if($this->user_session['now_money'] < 1000){
					$this->error('开通失败，请查看您的余额是否充足。');
				}
				if($_POST['type'] == '0'){
					if($this->user_session['ykjfw'] == '1'){
						$this->error('您已经开通了一口价服务');
					}
					$dataUser['ykjfw'] = '1';
					$desc = '开通一口价服务冻结资金';
					$freeze_type = '1';
				}else{
					if($this->user_session['yzfw'] == '1'){
						$this->error('您已经开通了一口价服务');
					}
					$dataUser['yzfw'] = '1';
					$desc = '开通优质服务冻结资金';
					$freeze_type = '2';
				}
				$databaseUser = D('User');
				$conditionUser['uid'] = $this->user_session['uid'];
				$dataUser['now_money'] = $this->user_session['now_money'] - 1000;
				$dataUser['freeze_money'] = $this->user_session['freeze_money'] + 1000;
				if($databaseUser->where($conditionUser)->data($dataUser)->save()){
					$data_user_freeze['uid'] = $this->user_session['uid'];
					$data_user_freeze['freezetime'] = $_SERVER['REQUEST_TIME'];
					$data_user_freeze['freezemoney'] = '1000';
					$data_user_freeze['info'] = $desc;
					$data_user_freeze['type'] = $freeze_type;
					D('User_freeze')->data($data_user_freeze)->add();
					D('User_money_list')->add_row($this->user_session['uid'],12,1000,$dataUser['now_money'],$desc);
					$this->success('开通成功');
				}else{
					$this->error('开通失败，请重试');
				}
			}else{
				if($_POST['type'] == '0'){
					if($this->user_session['ykjfw'] == '0'){
						$this->error('您已经关闭了一口价服务');
					}
					$dataUser['ykjfw'] = '0';
					$desc = '关闭一口价服务资金解冻';
					$freeze_type = '1';
				}else{
					if($this->user_session['yzfw'] == '0'){
						$this->error('您已经关闭了优质服务');
					}
					$dataUser['yzfw'] = '0';
					$desc = '关闭优质服务资金解冻';
					$freeze_type = '2';
				}
				$databaseUser = D('User');
				$conditionUser['uid'] = $this->user_session['uid'];
				$dataUser['now_money'] = $this->user_session['now_money'] + 1000;
				$dataUser['freeze_money'] = $this->user_session['freeze_money'] - 1000;
				if($databaseUser->where($conditionUser)->data($dataUser)->save()){
					$condition_user_freeze['uid'] = $this->user_session['uid'];
					$condition_user_freeze['type'] = $freeze_type;
					D('User_freeze')->where($condition_user_freeze)->delete();
					D('User_money_list')->add_row($this->user_session['uid'],13,1000,$dataUser['now_money'],$desc);
					$this->success('开通成功');
				}else{
					$this->error('开通失败，请重试');
				}
			}
		}
		// $this->success('开通成功');
	}

        public function replace(){
            $staff_replace = M('Staff_replace');
            $data['uid'] = $this->user_session['uid'];
            $data['uname'] = $this->user_session['nickname'];
            $data['staff_id'] = $this->user_session['staff_uid'];
            $data['reason'] = $_POST['reason'];
            $data['new_staff_id'] = $_POST['new'];
            $call_data = $staff_replace->where(array('uid'=>$data['uid'],'state'=>'0'))->find();
            if($call_data){
				if($call_data['new_staff_id'] == $_POST['new']){
					echo "3";
				}else{
					if($staff_replace->where(array('id'=>$call_data['id']))->data(array('new_staff_id'=>$_POST['new']))->save()){
						echo "1";
					}else{
						echo "2";
					}
				}
            }else{

                if($staff_replace->data($data)->add()){
                    echo "1";
                }else{
                    echo "2";
                }
            }

        }


        public function call(){
            $call_staff = M('Call_staff');

            $data['uid'] = $_SESSION['user']['uid'];
            $data['uname'] = $_SESSION['user']['nickname'];
            $data['staff_id'] = $_SESSION['user']['staff_uid'];
            $data['phone'] = $_SESSION['user']['phone'];
            $where='`uid` = '.$data['uid'].' AND `state` = 0';
            $call_data = $call_staff->where($where)->find();
            if($call_data){
                echo "3";
            }  else {
                $row = $call_staff->add($data);
                if($row){
                    echo "1";
                }  else {
                    echo "2";
                }
            }
        }

	/* 批量交易 操作*/
	public function pldomain(){
		$this->display();
	}
	public function pldomainInf(){
		$databaseBulkTrading = D('Bulk_trading');
		$conditionBulkTrading['uid'] = $this->user_session['uid'];
		$status = intval($_POST['type']);
		switch($status){
			case 1:
				$conditionBulkTrading['status'] = '0';
				break;
			case 2:
				$conditionBulkTrading['status'] = '1';
				break;
			case 3:
				$conditionBulkTrading['status'] = '2';
				break;
			case 4:
				$conditionBulkTrading['status'] = '3';
				break;
		}
		$tradingList = $databaseBulkTrading->where($conditionBulkTrading)->select();

		$return['total'] = $databaseBulkTrading->where($conditionBulkTrading)->count();
		$return['lists'] = $databaseBulkTrading->field(true)->where($conditionBulkTrading)->order('`bulk_id` DESC')->select();
		if($return['lists']){
			foreach($return['lists'] as $key=>$value){
				$return['lists'][$key]['add_time'] = date('Y-m-d H:i:s',$value['add_time']);
				$return['lists'][$key]['status'] = intval($value['status']);
			}
		}else{
			$return['lists'] = array();
		}
		$this->success($return);
	}
	public function pldomaindetail(){
		if(empty($_GET['id'])){
			redirect(U('Account/pldomain'));
		}
		$databaseBulkTrading = D('Bulk_trading');
		$conditionBulkTrading['uid'] = $this->user_session['uid'];
		$conditionBulkTrading['bulk_id'] = $_GET['id'];
		$trading = $databaseBulkTrading->field(true)->where($conditionBulkTrading)->find();
		if(empty($trading)){
			$this->error('当前交易不存在');
		}
		$trading['domain_list'] = unserialize($trading['domain_list']);
        $jsondata =  json_encode($trading['domain_list']);
        $check_uid = json_encode($trading['uid']);
        $this->assign('jsondata',$jsondata);
        $this->assign('jsonwhoid',$check_uid);
		$this->assign('trading',$trading);

		$this->display();
	}
	public function pldomainOperation(){
		if(IS_POST){
			if(empty($_POST['id'])){
				$this->error('请携带欲删除的ID');
			}
			$databaseBulkTrading = D('Bulk_trading');
			$conditionBulkTrading['uid'] = $this->user_session['uid'];
			$conditionBulkTrading['bulk_id'] = $_POST['id'];
			if($databaseBulkTrading->where($conditionBulkTrading)->delete()){
				$this->success('删除成功');
			}else{
				$this->error('删除失败，请重试');
			}
		}else{
			$this->error('非法访问');
		}
	}
	public function adddomains_pl(){
        $bulk_id = intval($_GET['bulk_id']);

        if(!empty($bulk_id)){
            $this->assign('bulk_id',$bulk_id);
            $databaseBulkTrading = D('Bulk_trading');
            $bulks =  $databaseBulkTrading->where(array('bulk_id'=>$bulk_id))->find();

            $bulks['domain_list'] = unserialize($bulks['domain_list']);
            $str_list = '';
            foreach($bulks['domain_list']  as $v){
                $str_list .= "$v".PHP_EOL;
            }

            $this->assign('str_list',$str_list);
            $this->assign('bulks',$bulks);
        }
		$this->display();
	}
	public function addDomainCheck_pl(){

		if(IS_POST){
			$okArr = array();
			$errorArr = array();
			if(empty($_POST['domain'])) $this->error('请填写域名或上传文件');
			if(empty($_POST['title'])) $this->error('请填写标题');
			if(empty($_POST['allprice'])) $this->error('请填写总价格');
			$postDomainTmpArr = explode(PHP_EOL,$_POST['domain']);
			if(count($postDomainTmpArr) < 2){
				$this->error('添加批量出售域名最少2个');
			}
			$postDomainArr = array();
			foreach($postDomainTmpArr as $value){
				$value = trim($value);
				if(!empty($value)){
					$postDomainArr[] = trim($value);
				}
			}
			//找出系统支持的后缀列表
			$suffix_list = D('Domain_suffix')->field('`suffix`')->where(array('status'=>'1'))->select();
			$suffixArr = array();
			foreach($suffix_list as $value){
				$suffixArr[] = $value['suffix'];
			}
			foreach($postDomainArr as $key=>$value){
				if(substr_count($value,'.') == 0){
					$errorArr[] = array('domain'=>$value,'msg'=>'域名不合法');
					continue;
				}
				$tmpDomain = str_replace($suffixArr,'',$value);
				if(empty($tmpDomain)){
					$errorArr[] = array('domain'=>$value,'msg'=>'域名不合法');
					continue;
				}
				if($tmpDomain == $value){
					$errorArr[] = array('domain'=>$value,'msg'=>'域名后缀不合法');
					continue;
				}
				if(substr_count($tmpDomain,'.') > 0){
					$errorArr[] = array('domain'=>$value,'msg'=>'请填写顶级域名');
					continue;
				}
				if(!preg_match('/^\w+$/',$tmpDomain)){
					$errorArr[] = array('domain'=>$value,'msg'=>'目前只支持英文域名');
					continue;
				}
				$okArr[] = $value;
			}
			if($okArr){
                $databaseBulkTrading = D('Bulk_trading');
				$dataBulkTrading['uid'] = $this->user_session['uid'];
				$dataBulkTrading['domain_list'] = serialize($okArr);
				$dataBulkTrading['title'] = $_POST['title'];
				$dataBulkTrading['total_price'] = $_POST['allprice'];
				$dataBulkTrading['meaning'] = $_POST['intro'];
				$dataBulkTrading['amount'] = count($okArr);
				$dataBulkTrading['group_id'] = $group_id;
				$dataBulkTrading['add_time'] = $_SERVER['REQUEST_TIME'];
				$dataBulkTrading['status'] = '0';
                $has_bulk_id = intval($_POST['bulk_id']);
                if($has_bulk_id >0 ){
                    if($databaseBulkTrading->where(array('bulk_id'=>$has_bulk_id))->data($dataBulkTrading)->save()){
                        // dump($databaseBulkTrading);exit;
                        $this->success('成功修改批量域名',U('Account/pldomain'));exit;
                    }else{
                        $this->error('修改失败，请重试');
                    }
                }else{
                    if(!$databaseBulkTrading->data($dataBulkTrading)->add()){
                        // dump($databaseBulkTrading);exit;
                        $this->error('添加失败，请重试');
                    }
                }

			}
			$this->assign('okArr',$okArr);
			$this->assign('errorArr',$errorArr);
                        $this->success('成功添加批量域名');
//			$this->display();
		}else{
            $bulk_id = intval($_GET['id']);

            if(!empty($bulk_id))
			    redirect(U('Account/adddomains_pl',array('bulk_id'=>$bulk_id)));
            else
                redirect(U('Account/adddomains_pl'));
		}
	}
	/* 我的所有域名 操作*/
	public function allsell(){
		if($_GET['type'] == 0){
			$group_list = D('Domains_group')->field(true)->where(array('uid'=>$this->user_session['uid']))->order('`group_id` ASC')->select();
			$this->assign('group_list',$group_list);
			$this->display();
		}else if($_GET['type'] == 3){
			$this->display('allsell_fastbid');
		}else if($_GET['type'] == 4){
			$this->display('allsell_yz');
		}else if($_GET['type'] == 1){
			$this->display('allsell_yj');
		}else{
			$this->display('allsell_yikoujia');
		}
	}
	public function allsellinf(){
		if(IS_POST){
			$databaseDomain = D('Domains');
			$conditionDomain['uid'] = $this->user_session['uid'];
			$conditionDomain['status'] = '1';
			if($_POST['haveprice'] == 1){
				$conditionDomain['money'] = array('neq','0.00');
			}else if($_POST['haveprice'] == 2){
				$conditionDomain['money'] = array('eq','0.00');
			}
			if(isset($_POST['groupid']) && $_POST['groupid'] !== ''){
				$conditionDomain['group_id'] = $_POST['groupid'];
			}
			if($_POST['domainName'] !== ''){
				$conditionDomain['domain'] = array('like','%'.$_POST['domainName'].'%');
			}
			if($_POST['type'] != 0){
				$conditionDomain['type'] = $_POST['type'] - 1;
                if($conditionDomain['type'] == 3){
                    $conditionDomain['status'] = array('in','0,1');
                }
			}
			$return['total'] = $databaseDomain->where($conditionDomain)->count();
            import('@.ORG.reply_ajax_page');
            $p = new Page($return['total'], 20);
			$return['lists'] = $databaseDomain->field(true)->where($conditionDomain)->order('`domain_id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();

			if($return['lists']){
				foreach($return['lists'] as $key=>$value){
					if($value['startTime']){
						$return['lists'][$key]['startTime'] = date('Y-m-d H:i:s',$value['startTime']);
					}
				}
				$group_list = D('Domains_group')->field('`group_id`,`group_name`')->where(array('uid'=>$this->user_session['uid']))->order('`group_id` ASC')->select();
				foreach($group_list as $value){
					$groupArr[$value['group_id']] = $value['group_name'];
				}
				foreach($return['lists'] as $key=>$value){
					if($value['group_id']){
						$return['lists'][$key]['group_name'] = $groupArr[$value['group_id']];
					}
					$return['lists'][$key]['add_time'] = date('Y-m-d H:i:s',$value['add_time']);
				}
			}else{
				$return['lists'] = array();
			}
			$this->success($return);
		}
	}
	public function apriceinf(){
		if(IS_POST){
			$databaseDomain = D('Domains');
			$conditionDomain['uid'] = $this->user_session['uid'];
			$conditionDomain['type'] = '1';
			if($_POST['haveprice'] == 1){
				$conditionDomain['money'] = array('neq','0.00');
			}else if($_POST['haveprice'] == 2){
				$conditionDomain['money'] = array('eq','0.00');
			}
			if(isset($_POST['groupid']) && $_POST['groupid'] !== ''){
				$conditionDomain['group_id'] = $_POST['groupid'];
			}
			if($_POST['domainName'] !== ''){
				$conditionDomain['domain'] = array('like','%'.$_POST['domainName'].'%');
			}
			if($_POST['type'] != 0){
				$conditionDomain['type'] = $_POST['type'] - 1;
			}
			$return['total'] = $databaseDomain->where($conditionDomain)->count();
			// dump($databaseDomain);
			$return['lists'] = $databaseDomain->field(true)->where($conditionDomain)->order('`domain_id` DESC')->select();
			if($return['lists']){
				foreach($return['lists'] as $key=>$value){
					if($value['startTime']){
						$return['lists'][$key]['startTime'] = date('Y-m-d H:i:s',$value['startTime']);
					}
				}
				$group_list = D('Domains_group')->field('`group_id`,`group_name`')->where(array('uid'=>$this->user_session['uid']))->order('`group_id` ASC')->select();
				foreach($group_list as $value){
					$groupArr[$value['group_id']] = $value['group_name'];
				}
				foreach($return['lists'] as $key=>$value){
					if($value['group_id']){
						$return['lists'][$key]['group_name'] = $groupArr[$value['group_id']];
					}
					$return['lists'][$key]['add_time'] = date('Y-m-d H:i:s',$value['add_time']);
				}
			}else{
				$return['lists'] = array();
			}
			$this->success($return);
		}
	}
	public function pl_yj_set(){
		if(empty($_COOKIE['selectAll'])){
			redirect(U('Account/index'));
		}
		$allArr = explode('&&',$_COOKIE['selectAll']);
		if($allArr[1] == 'all'){
			$selectArr = array();
		}else{
			$selectArr = explode(',',array_pop($allArr));
			foreach($selectArr as $key=>$value){
				if(empty($value)){
					unset($selectArr[$key]);
				}
			}
		}
		if(empty($selectArr)){
			$this->error('您没有选中任何数据');
		}
		$group_list = D('Domains_group')->field(true)->where(array('uid'=>$this->user_session['uid']))->order('`group_id` ASC')->select();
		$this->assign('group_list',$group_list);

		$databaseDomain = D('Domains');
		$conditionDomain['uid'] = $this->user_session['uid'];
		$conditionDomain['type'] = '0';
		if($selectArr){
			$conditionDomain['domain_id'] = array('in',$selectArr);
		}
		$domainList = $databaseDomain->field(true)->where($conditionDomain)->order('`domain_id` DESC')->select();
		$this->assign('domainList',$domainList);

		$this->display();
	}
	public function pl_operation(){
		if(IS_POST){
			$inputTmp = explode('&',file_get_contents('php://input'));
           // dump($inputTmp);die;
			$inputArr = array();
			foreach($inputTmp as $value){
				$tmpValue = explode('=',$value);
				$tmpValue[1] = urldecode($tmpValue[1]);
				if(!empty($inputArr[$tmpValue[0]])){
					if(is_array($inputArr[$tmpValue[0]])){
						$inputArr[$tmpValue[0]][] = $tmpValue[1];
					}else{
						$tmpInputValue = $inputArr[$tmpValue[0]];
						$inputArr[$tmpValue[0]] = array();
						$inputArr[$tmpValue[0]][] = $tmpInputValue;
						$inputArr[$tmpValue[0]][] = $tmpValue[1];
					}
				}else{
					$inputArr[$tmpValue[0]] = $tmpValue[1];
				}
			}
            //dump($inputArr);
			// foreach($inputArr as $key=>$value){
				// if(!is_array($value)){
					// unset($inputArr[$key]);
				// }
			// }
			$databaseDomain = D('Domains');
			$conditionDomains['uid'] = $this->user_session['uid'];
			if(!is_array($inputArr['id'])){
				$newInputArr['id'][0] = $inputArr['id'];
				$newInputArr['group_id'][0] = $inputArr['group_id'];
				$newInputArr['money'][0] = $inputArr['money'];
				$newInputArr['blj'][0] = $inputArr['blj'];
				$newInputArr['desc'][0] = $inputArr['desc'];
				$inputArr = $newInputArr;
			}
          //    dump($inputArr);
			foreach($inputArr['id'] as $key=>$value){

				$conditionDomains['domain_id'] = $value;
				$dataDomains['group_id'] = $inputArr['group_id'];
				$dataDomains['money'] = $inputArr['money'];
				$dataDomains['blj'] = $inputArr['blj'];
				$dataDomains['desc'] = $inputArr['desc'][$key];

				//$databaseDomain->where($conditionDomains)->data($dataDomains)->save();
			}
          //  die;
			redirect(U('Account/allsell'));
		}else{
			if(empty($_COOKIE['selectAll'])){
				redirect(U('Account/allsell'));
			}
			$allArr = explode('&&',$_COOKIE['selectAll']);
			if($allArr[1] == 'all'){
				$selectArr = array();
			}else{
				$selectArr = explode(',',array_pop($allArr));
				foreach($selectArr as $key=>$value){
					if(empty($value)){
						unset($selectArr[$key]);
					}
				}
			}
			if(empty($selectArr)){
				$this->error('您没有选中任何数据');
			}
			$group_list = D('Domains_group')->field(true)->where(array('uid'=>$this->user_session['uid']))->order('`group_id` ASC')->select();
			$this->assign('group_list',$group_list);

			$databaseDomain = D('Domains');
			$conditionDomain['uid'] = $this->user_session['uid'];
			$conditionDomain['domain_id'] = array('in',$selectArr);
			if($_GET['type'] == 'del'){
				$databaseDomain->where($conditionDomain)->delete();
			}else{
				$databaseDomain->where($conditionDomain)->data(array('type'=>'0'))->save();
			}
			$this->success('操作成功');
		}
	}
	public function allsellOperation(){
		$databaseDomain = D('Domains');
		$conditionDomain['uid'] = $this->user_session['uid'];
		$conditionDomain['domain_id'] = $_POST['id'];
		$databaseDomain->where($conditionDomain)->delete();
		$this->success('设置成功');
	}
	public function highgradeOperation(){
		$databaseDomain = D('Domains');
		$conditionDomain['uid'] = $this->user_session['uid'];
		$conditionDomain['domain_id'] = $_POST['id'];
		$databaseDomain->where($conditionDomain)->data(array('type'=>'0'))->save();
		$this->success('设置成功');
	}
	public function pl_ykj(){
		if(empty($this->user_session['ykjfw'])){
			$this->assign('jumpUrl',U('Account/ykjfw'));
			$this->error('请先开通一口价');
		}
		if(empty($_COOKIE['selectAll'])){
			redirect(U('Account/index'));
		}
		// dump($_COOKIE['selectAll']);
		$allArr = explode('&&',$_COOKIE['selectAll']);
		if($allArr[1] == 'all'){
			$selectArr = array();
		}else{
			$selectArr = explode(',',array_pop($allArr));
			foreach($selectArr as $key=>$value){
				if(empty($value)){
					unset($selectArr[$key]);
				}
			}
		}
		if(empty($selectArr)){
			$this->error('您没有选中任何数据');
		}
		$databaseDomain = D('Domains');
		$conditionDomain['uid'] = $this->user_session['uid'];
                $conditionDomain['status'] = 1;
		if($selectArr){
			$conditionDomain['domain_id'] = array('in',$selectArr);
		}
		$domainList = $databaseDomain->field(true)->where($conditionDomain)->order('`domain_id` DESC')->select();
		$this->assign('domainList',$domainList);

		$this->display();
	}
	public function pl_ykj_operation(){
		if(IS_POST){
			$inputTmp = explode('&',file_get_contents('php://input'));
			$inputArr = array();
			foreach($inputTmp as $value){
				$tmpValue = explode('=',$value);
				$tmpValue[1] = urldecode($tmpValue[1]);
				if(isset($inputArr[$tmpValue[0]])){
					if(is_array($inputArr[$tmpValue[0]])){
						$inputArr[$tmpValue[0]][] = $tmpValue[1];
					}else{
						$tmpInputValue = $inputArr[$tmpValue[0]];
						$inputArr[$tmpValue[0]] = array();
						$inputArr[$tmpValue[0]][] = $tmpInputValue;
						$inputArr[$tmpValue[0]][] = $tmpValue[1];
					}
				}else{
					$inputArr[$tmpValue[0]] = $tmpValue[1];
				}
			}
			$databaseDomain = D('Domains');
			$conditionDomains['uid'] = $this->user_session['uid'];
			if(!is_array($inputArr['id'])){
				$newInputArr['id'][0] = $inputArr['id'];
				$newInputArr['money'][0] = $inputArr['money'];
				$newInputArr['desc'][0] = $inputArr['desc'];
				$inputArr = $newInputArr;
			}
			foreach($inputArr['id'] as $key=>$value){
				if(empty($inputArr['money'][$key])){
					continue;
				}
				$conditionDomains['domain_id'] = $value;
				$dataDomains['money'] = $inputArr['money'][$key];
				$dataDomains['desc'] = $inputArr['desc'][$key];
				$dataDomains['type'] = '1';
				$dataDomains['startTime'] = $_SERVER['REQUEST_TIME'];
				$databaseDomain->where($conditionDomains)->data($dataDomains)->save();
			}
			redirect(U('Account/allsell'));
		}
	}
	public function apriceOperation(){
		if(IS_POST){
			$conditionDomain['uid'] = $this->user_session['uid'];
			$conditionDomain['domain_id'] = $_POST['id'];
			if(D('Domains')->where($conditionDomain)->data(array('type'=>'0'))->save()){
				$this->success('下架成功');
			}else{
				$this->error('下架失败，请重试');
			}
		}
	}
	public function pl_yz(){
		if(empty($this->user_session['ykjfw'])){
			$this->assign('jumpUrl',U('Account/yzfw'));
			$this->error('请先开通优质');
		}

		if(empty($_COOKIE['selectAll'])){
			redirect(U('Account/index'));
		}
		$allArr = explode('&&',$_COOKIE['selectAll']);
		if($allArr[1] == 'all'){
			$selectArr = array();
		}else{
			$selectArr = explode(',',array_pop($allArr));
			foreach($selectArr as $key=>$value){
				if(empty($value)){
					unset($selectArr[$key]);
				}
			}
		}
		if(empty($selectArr)){
			$this->error('您没有选中任何数据');
		}
		$databaseDomain = D('Domains');
		$conditionDomain['uid'] = $this->user_session['uid'];
		if($selectArr){
			$conditionDomain['domain_id'] = array('in',$selectArr);
		}
		$domainList = $databaseDomain->field(true)->where($conditionDomain)->order('`domain_id` DESC')->select();
		$this->assign('domainList',$domainList);

		$this->display();
	}
	public function pl_yz_operation(){
		if(IS_POST){
			$inputTmp = explode('&',file_get_contents('php://input'));
			$inputArr = array();
			foreach($inputTmp as $value){
				$tmpValue = explode('=',$value);
				$tmpValue[1] = urldecode($tmpValue[1]);
				if(isset($inputArr[$tmpValue[0]])){
					if(is_array($inputArr[$tmpValue[0]])){
						$inputArr[$tmpValue[0]][] = $tmpValue[1];
					}else{
						$tmpInputValue = $inputArr[$tmpValue[0]];
						$inputArr[$tmpValue[0]] = array();
						$inputArr[$tmpValue[0]][] = $tmpInputValue;
						$inputArr[$tmpValue[0]][] = $tmpValue[1];
					}
				}else{
					$inputArr[$tmpValue[0]] = $tmpValue[1];
				}
			}
			$databaseDomain = D('Domains');
			$conditionDomains['uid'] = $this->user_session['uid'];
			if(!is_array($inputArr['id'])){
				$newInputArr['id'][0] = $inputArr['id'];
				$newInputArr['money'][0] = $inputArr['money'];
				$newInputArr['desc'][0] = $inputArr['desc'];
				$inputArr = $newInputArr;
			}
			foreach($inputArr['id'] as $key=>$value){
				$conditionDomains['domain_id'] = $value;
				$dataDomains['money'] = $inputArr['money'][$key];
				$dataDomains['desc'] = $inputArr['desc'][$key];
				$dataDomains['status'] = '0';
				$dataDomains['type'] = '3';
				$dataDomains['startTime'] = $_SERVER['REQUEST_TIME'];
				$databaseDomain->where($conditionDomains)->data($dataDomains)->save();
			}
			redirect(U('Account/allsell'));
		}
	}
	public function pl_fastbid(){
		if(empty($_COOKIE['selectAll'])){
			redirect(U('Account/index'));
		}
		// dump($_COOKIE['selectAll']);
		$allArr = explode('&&',$_COOKIE['selectAll']);
		if($allArr[1] == 'all'){
			$selectArr = array();
		}else{
			$selectArr = explode(',',array_pop($allArr));
			foreach($selectArr as $key=>$value){
				if(empty($value)){
					unset($selectArr[$key]);
				}
			}
		}
		if(empty($selectArr)){
			$this->error('您没有选中任何数据');
		}
		$databaseDomain = D('Domains');
		$conditionDomain['uid'] = $this->user_session['uid'];
		if($selectArr){
			$conditionDomain['domain_id'] = array('in',$selectArr);
		}
		$domainList = $databaseDomain->field(true)->where($conditionDomain)->order('`domain_id` DESC')->select();
		$this->assign('domainList',$domainList);

		$this->display();
	}
	public function pl_fastbid_operation(){
		if(IS_POST){
			$inputTmp = explode('&',file_get_contents('php://input'));
			$inputArr = array();
			foreach($inputTmp as $value){
				$tmpValue = explode('=',$value);
				$tmpValue[1] = urldecode($tmpValue[1]);
				if(isset($inputArr[$tmpValue[0]])){
					if(is_array($inputArr[$tmpValue[0]])){
						$inputArr[$tmpValue[0]][] = $tmpValue[1];
					}else{
						$tmpInputValue = $inputArr[$tmpValue[0]];
						$inputArr[$tmpValue[0]] = array();
						$inputArr[$tmpValue[0]][] = $tmpInputValue;
						$inputArr[$tmpValue[0]][] = $tmpValue[1];
					}
				}else{
					$inputArr[$tmpValue[0]] = $tmpValue[1];
				}
			}
			$databaseDomain = D('Domains');
			$conditionDomains['uid'] = $this->user_session['uid'];
			if(!is_array($inputArr['id'])){
				$newInputArr['id'][0] = $inputArr['id'];
				$newInputArr['money'][0] = $inputArr['money'];
				$newInputArr['desc'][0] = $inputArr['desc'];
				$inputArr = $newInputArr;
			}
			foreach($inputArr['id'] as $key=>$value){
				if(!empty($inputArr['money'][$key])){
					$conditionDomains['domain_id'] = $value;
					$nowDomain = $databaseDomain->field(true)->where($conditionDomains)->find();
					if($nowDomain['lastFidId']){
						$dataDomains['mark_money'] = $inputArr['money'][$key];
					}else{
						$dataDomains['money'] = $dataDomains['mark_money'] = $inputArr['money'][$key];
					}
					$dataDomains['desc'] = $inputArr['desc'][$key];
					$dataDomains['status'] = '1';
					$dataDomains['type'] = '2';
					$dataDomains['startTime'] = $dataDomains['apply_time'] = $_SERVER['REQUEST_TIME'];
					$dataDomains['endTime'] = $_SERVER['REQUEST_TIME'] + $this->config['fastbid_term']*86400;
					$databaseDomain->where($conditionDomains)->data($dataDomains)->save();
				}
			}
			redirect(U('Account/fastbid'));
		}
	}
	/* 我提交的优质域名 */
	public function bargain(){
		redirect(U('Account/allsell',array('type'=>'4')));
	}
	/* 我提交的一口价 */
	public function aprice(){
		redirect(U('Account/allsell',array('type'=>'2')));
	}
	/* 我提交的竞价 */
	public function fastbid(){
		redirect(U('Account/allsell',array('type'=>'3')));
		//待审核
		$databaseDomains = D('Domains');
		$conditionDomains['type'] = '2';
		$conditionDomains['status'] = '0';
		$applyDomains = $databaseDomains->field(true)->where($conditionDomains)->order('`apply_time` DESC')->select();
		$this->assign('applyDomains',$applyDomains);

		$this->display();
	}
	public function fastbidinf(){
		//待审核
		$databaseDomains = D('Domains');
		$conditionDomains['type'] = '2';
		$conditionDomains['status'] = '2';
		$applyDomains = $databaseDomains->field(true)->where($conditionDomains)->order('`apply_time` DESC')->limit(20)->select();
		if($applyDomains){
			foreach($applyDomains as &$applyDomainsValue){
				$applyDomainsValue['apply_time'] = date('Y-m-d H:i:s',$applyDomainsValue['apply_time']);
			}
		}
		$result['total'] = $databaseDomains->where($conditionDomains)->count();
		$result['list'] = $applyDomains;
		$this->success($result);
	}
	public function fastbid_add(){
		$group_list = D('Domains_group')->field(true)->where(array('uid'=>$this->user_session['uid']))->order('`group_id` ASC')->select();
		$this->assign('group_list',$group_list);

		$this->display();
	}
	/* 添加域名出售 操作*/
	public function adddomainss(){
		$group_list = D('Domains_group')->field(true)->where(array('uid'=>$this->user_session['uid']))->order('`group_id` ASC')->select();
		$this->assign('group_list',$group_list);
		$this->display();
	}

	//添加域名出售
	public function addDomainCheck(){
		if(IS_POST){
			$group_id = intval($_POST['group']);
			if($group_id){
				$databaseDomainsGroup = D('Domains_group');
				$nowGroup = $databaseDomainsGroup->field('`group_id`')->where(array('group_id'=>$group_id,'uid'=>$this->user_session['uid']))->find();
				if(empty($nowGroup)){
					$this->error('您选择的分组不存在！');
				}
			}

			$okArr = array();
			$errorArr = array();
			if(empty($_POST['domain'])) $this->error('请填写域名或上传文件');
			$postDomainTmpArr = explode(PHP_EOL,$_POST['domain']);
			$postDomainArr = array();
			foreach($postDomainTmpArr as $value){
				$value = trim($value);
				if(!empty($value)){
					$postDomainArr[] = trim($value);
				}
			}
			//找出系统支持的后缀列表
			$suffix_list = D('Domain_suffix')->field('`suffix`')->where(array('status'=>'1'))->select();
			$suffixArr = array();
			foreach($suffix_list as $value){
				$suffixArr[] = $value['suffix'];
			}
			foreach($postDomainArr as $key=>$value){
				if(substr_count($value,'.') == 0){
					$errorArr[] = array('domain'=>$value,'msg'=>'域名不合法');
					continue;
				}
				$tmpDomain = str_replace($suffixArr,'',$value);
				if(empty($tmpDomain)){
					$errorArr[] = array('domain'=>$value,'msg'=>'域名不合法');
					continue;
				}
				if($tmpDomain == $value){
					$errorArr[] = array('domain'=>$value,'msg'=>'域名后缀不合法');
					continue;
				}
				if(substr_count($tmpDomain,'.') > 0){
					$errorArr[] = array('domain'=>$value,'msg'=>'请填写顶级域名');
					continue;
				}
				if(!preg_match('/^\w+$/',$tmpDomain)){
					$errorArr[] = array('domain'=>$value,'msg'=>'目前只支持英文域名');
					continue;
				}
				$okArr[] = strtolower($value);
			}

			$databaseDomainCheck = D('Domains_check');
			$dataDomainCheck['uid'] = $this->user_session['uid'];
			$dataDomainCheck['group_id'] = $group_id;
			$dataDomainCheck['add_time'] = $_SERVER['REQUEST_TIME'];
			$dataDomainCheck['is_check'] = '0';
			foreach($okArr as $key=>$value){
				$dataDomainCheck['domain'] = $value;
                $domains_isset = D('Domains')->where(array('domain'=>$value,'uid'=>$this->user_session['uid']))->find();
				if($databaseDomainCheck->where(array('domain'=>$value,'uid'=>$this->user_session['uid']))->find() ||$domains_isset){
					unset($okArr[$key]);
					$errorArr[] = array('domain'=>$value,'msg'=>'已添加过该域名');
					continue;
				}
				if($databaseDomainCheck->data($dataDomainCheck)->add()){
					if($group_id){
						$databaseDomainsGroup->where(array('group_id'=>$group_id))->setInc('domain_count');
					}
				}else{
					unset($okArr[$key]);
					$errorArr[] = array('domain'=>$value,'msg'=>'添加失败，请重试');
					continue;
				}
			}
			$this->assign('okArr',$okArr);
			$this->assign('errorArr',$errorArr);
			$this->display();
		}else{
			redirect(U('Account/adddomainss'));
		}
	}

	/* 域名分组 操作*/
	public function group(){
		$group_list = D('Domains_group')->field(true)->where(array('uid'=>$this->user_session['uid']))->order('`group_id` ASC')->select();
		$this->assign('group_list',$group_list);
		$this->display();
	}
	public function group_add(){
		if(IS_POST){
			if(empty($_POST['groupName'])) $this->error('域名分组名称不能为空');
			if(D('Domains_group')->data(array('uid'=>$this->user_session['uid'],'group_name'=>$_POST['groupName'],'group_desc'=>$_POST['intro']))->add()){
				$this->success('添加分组成功');
			}else{
				$this->error('添加失败，请重试');
			}
		}else{
			$this->error('非法访问');
		}
	}
	public function group_update(){
		if(IS_POST){
			if(empty($_POST['id'])) $this->error('请携带修改的ID');
			if(empty($_POST['groupName'])) $this->error('域名分组名称不能为空');
			if(D('Domains_group')->where(array('group_id'=>$_POST['id'],'uid'=>$this->user_session['uid']))->data(array('group_name'=>$_POST['groupName'],'group_desc'=>$_POST['intro']))->save()){
				$this->success('编辑分组成功');
			}else{
				$this->error('编辑失败，请检查您有没有变更内容后再重试');
			}
		}else{
			$this->error('非法访问');
		}
	}
	public function group_del(){
		if(IS_POST){
			if(empty($_POST['id'])) $this->error('请携带删除的ID');
			if(D('Domains_group')->where(array('group_id'=>$_POST['id'],'uid'=>$this->user_session['uid']))->delete()){
				$this->success('删除分组成功');
			}else{
				$this->error('删除失败，请重试');
			}
		}else{
			$this->error('非法访问');
		}
	}
	/* 待验证的域名 操作*/
	public function withcheck(){
		if(IS_POST){
			$databaseDomainCheck = D('Domains_check');
			$conditionDomainCheck['uid'] = $this->user_session['uid'];
			$conditionDomainCheck['is_check'] = '0';
			$return['total'] = $databaseDomainCheck->where($conditionDomainCheck)->count();
			$return['lists'] = $databaseDomainCheck->field(true)->where($conditionDomainCheck)->order('`domain_id` DESC')->select();
			if($return['lists']){
				foreach($return['lists'] as $key=>$value){
					$return['lists'][$key]['add_time'] = date('Y-m-d H:i:s',$value['add_time']);
				}
			}else{
				$return['lists'] = array();
			}
			$this->success($return);
		}
		$this->display();
	}
	public function checkwithdomain(){
		$databaseDomainCheck = D('Domains_check');
		$conditionDomainCheck['uid'] = $this->user_session['uid'];
		$conditionDomainCheck['domain_id'] = $_POST['id'];
		$nowDomain = $databaseDomainCheck->field(true)->where($conditionDomainCheck)->find();

		if(empty($nowDomain)){
			$this->error('当前域名不存在');
		}
		if($_POST['type'] == 0){
			import('@.ORG.whois');
			$whoisClass = new whois();
			$whoisResult = $whoisClass->getWhois($nowDomain['domain']);
			//file_put_contents('whois.log',serialize($whoisResult));
			if(empty($whoisResult) || empty($whoisResult['registrarEmail'])){
                $databaseDomainCheck->where(array('domain_id'=>$_POST['id']))->data(array('is_check'=>'0','check_fail'=>'1'))->save();
				$this->error('当前域名没找到whois信息,请联系经纪人');
			}
			// dump($whoisResult);
			if(D('Domain_whoisemail')->where(array('uid'=>$this->user_session['uid'],'email'=>$whoisResult['registrarEmail'],'is_check'=>'1'))->find()){
				if($databaseDomainCheck->where(array('uid'=>$this->user_session['uid'],'domain_id'=>$_POST['id']))->data(array('is_check'=>'1'))->save()){
					//添加到domains域名出售列表，议价
					$dataDomains['uid'] = $this->user_session['uid'];
					$dataDomains['domain'] = $nowDomain['domain'];
					$dataDomains['group_id'] = $nowDomain['group_id'];
					$dataDomains['desc'] = $nowDomain['intro'];
					$dataDomains['money'] = $nowDomain['price'];
					$dataDomains['add_time'] = $_SERVER['REQUEST_TIME'];

					//找出系统支持的后缀列表
					$suffix_list = D('Domain_suffix')->field('`id`,`suffix`')->where(array('status'=>'1'))->select();
					$suffixArr = array();
					$tmpSuffix = array();
					foreach($suffix_list as $value){
						if(str_replace($value['suffix'],'',$nowDomain['domain']) != $nowDomain['domain']){
							if(empty($tmpSuffix) || strlen($value['suffix']) > strlen($tmpSuffix['suffix'])){
								$tmpSuffix = $value;
							}
						}
						$suffixArr[$value['id']] = $value['suffix'];
					}


					$tmpDomain = str_replace($suffixArr,'',$nowDomain['domain']);
					$dataDomains['main_domain'] = $tmpDomain;

					//通过文件匹配域名类型
					import('@.ORG.domainStyle');
					$domainStyle = new domainStyle();
					$styleResult = $domainStyle->getStyle($tmpDomain);
					$dataDomains['styleType'] = $styleResult['styleType'];
					$dataDomains['label'] = $styleResult['label'];
					$dataDomains['style'] = implode(',',$styleResult['style']);

					//停止找字典库
					// $nowDomainDict = D('Domain_dicts')->field(true)->where(array('name'=>$tmpDomain))->find();
					// if(!empty($nowDomainDict)){
						// $dataDomains['cat_id'] = $nowDomainDict['tindex'];
						// $dataDomains['cat_fid'] = $nowDomainDict['sindex'];
						// $dataDomains['cat_pid'] = $nowDomainDict['mindex'];
						// $dataDomains['label'] = $nowDomainDict['name'];
					// }else{
						// if(preg_match('/^\d+$/',$tmpDomain)){
							// $dataDomains['cat_pid'] = '1';
							// $dataDomains['label'] = strlen($tmpDomain).'数字';
						// }else if(preg_match('/^[a-z]+$/',$tmpDomain)){
							// $dataDomains['cat_pid'] = '2';
							// $dataDomains['label'] = strlen($tmpDomain).'字母';
						// }else{
							// $dataDomains['cat_pid'] = '3';
							// $dataDomains['label'] = strlen($tmpDomain).'杂';
						// }
					// }
					$dataDomains['suffix'] = $tmpSuffix['id'];
					$dataDomains['length'] = strlen($tmpDomain);
					$dataDomains['status'] = '1';
					if(D('Domains')->data($dataDomains)->add()){
						$databaseDomainCheck->where(array('domain_id'=>$_POST['id']))->delete();
						$this->success('验证成功');
					}else{
						$databaseDomainCheck->where(array('domain_id'=>$_POST['id']))->data(array('is_check'=>'0','check_fail'=>'1'))->save();
						$this->error('验证时发生错误');
					}
				}else{
					$this->error('验证失败，请重试');
				}
			}else{
                $databaseDomainCheck->where(array('domain_id'=>$_POST['id']))->data(array('is_check'=>'0','check_fail'=>'1'))->save();
				$this->error('该whois对应的邮箱不是您的邮箱，请添加邮箱');
			}
		}  else if($_POST['type'] == 1){
                            if($databaseDomainCheck->where(array('domain_id'=>  intval($_POST['id']),'uid'=>  $this->user_session['uid']))->delete()){
                                echo 1;
                            }  else {
                                echo 2;
                            }
                }
	}
	/* Whois邮箱 操作*/
	public function whoisemail(){
//       if(intval($_GET['check']) === 1){
//          echo '验证成功';
//       }elseif(intval($_GET['check']) === 0){
//           echo '验证信息已经失效';
//       }
		$whoisemail_list = D('Domain_whoisemail')->field(true)->where(array('uid'=>$this->user_session['uid']))->order('`mail_id` ASC')->select();
		$this->assign('whoisemail_list',$whoisemail_list);
		$this->display();
	}
	public function bindwhoisemail(){
		if(IS_POST){
			if(empty($_POST['txtemail'])) $this->error('请填写绑定的邮箱');
			$_POST['txtemail'] = strtolower($_POST['txtemail']);
			if(D('Domain_whoisemail')->where(array('uid'=>$this->user_session['uid'],'email'=>$_POST['txtemail']))->find()){
				$this->error('邮箱已经存在！');
			}
			if(D('Domain_whoisemail')->data(array('uid'=>$this->user_session['uid'],'email'=>$_POST['txtemail']))->add()){
				redirect(U('Account/whoisemail'));
			}else{
				$this->error('添加失败，请重试');
			}
		}else{
			$this->error('非法访问');
		}
	}

	//手机绑定
	public function bind(){
		$this->assign('status',1);
		$Db_user = M('User');
		$get_user_info = $Db_user->field('`uid`,`phone`,`is_check_phone`')->where('uid = '.$_SESSION['user']['uid'])->find();
		$this->assign('get_user_info',$get_user_info);
		$this->display();
	}

	public function bindphone(){
		$this->assign('status',1);
        if(IS_POST){
            $Db_Sms_record = M('Sms_record');
            $check_condition['uid'] = $_SESSION['user']['uid'];
            $check_condition['type'] = 'domains_user';
            $check_condition['extra'] = trim($_POST['txtcode']);
            $check_condition['phone'] = trim($_POST['phone']);
            $get_check = $Db_Sms_record->field('`uid`,`extra`,`expire_time`')->order('`pigcms_id` DESC')->where($check_condition)->find();
            if($get_check['expire_time'] > time() && !empty($get_check['extra'])){
                $check_data['is_check_phone'] = 1;
                $check_data['phone'] = trim($_POST['phone']);
                $check_data['uid'] = $_SESSION['user']['uid'];
               // $check_data['extra'] = trim($_POST['txtcode']);
                $Db_user = M('User');
                if($Db_user->data($check_data)->save()){
                    $this->success('手机验证成功。');
                }else{
                    $this->error('校验出现异常，请稍候再试。');exit;
                }

            }else{
                $this->error('校验码已超时，或者手机号不存在请重新获取');exit;
            }

        }


	}

	public  function sendCode(){
		$phone = I('phone', 0);
        $phone = trim($_POST['phone']);
        if (preg_match("/^1[0-9]{10}$/", $phone)) {
            if (! $phone) {
               // $this->returnCode('20042001');
                exit(json_encode(array('error'=>'4','msg'=>'手机号有误，请重新输入')));
            }
            $code = mt_rand(1000, 9999);
            $text = '您的验证码是：' . $code . '。此验证码20分钟内有效，请不要把验证码泄露给其他人。如非本人操作，可不用理会！';

//              $_SESSION['sys-send-time']['now'] = time();
//              $expire_time = $_SESSION['sys-send-time']['now'] + 1200;
                $send_time = time();
                $expire_time  = time() + 1200;

            $return = Sms::sendSms(array( 'content' => $text, 'mobile' => $phone, 'uid' => $_SESSION['user']['uid'], 'type' => 'domains_user',
                'send_time'=>$send_time,'expire_time'=>$expire_time,'extra'=>$code));
            exit(json_encode(array('error'=>'0','msg'=>'获取验证码成功，请及时查看~','expire_time'=>$expire_time)));
        } else {
            exit(json_encode(array('error'=>'4','msg'=>'手机号有误，请重新输入')));
        }

		//file_put_contents('x222.log',$phone);die;
		if (! $phone) {
			$this->returnCode('20042001');
		}

	}

    //错误代码
    static private function ConverSmsCode($smscode) {
        $errCode = array(
            '2'    => '20060001',
            '400'  => '20060002',
            '401'  => '20060003',
            '402'  => '20060004',
            '403'  => '20060005',
            '4030' => '20060006',
            '404'  => '20060007',
            '405'  => '20060008',
            '4050' => '20060009',
            '4051' => '20060010',
            '4052' => '20060011',
            '406'  => '20060012',
            '407'  => '20060013',
            '4070' => '20060014',
            '4071' => '20060015',
            '4072' => '20060016',
            '4073' => '20060017',
            '408'  => '20060018',
            '4085' => '20060019',
            '4084' => '20060020',
        );

        return $errCode[$smscode];
    }

	public function bindEmail(){
		$this->assign('status',2);
        $Db_user = M('User');
        $user_condition['uid'] = $_SESSION['user']['uid'];
        $get_user_info = $Db_user->field('`email`,`uid`,`is_check_email`')->where($user_condition)->find();
        //$get_user_info = $Db_user->field('`email`,`uid`,`is_check_email`')->where($user_condition)->find();
        $this->assign('get_user_info',$get_user_info);
		$this->display();
	}

    Public function go_bind_email(){

        import('@.ORG.smtp');
        $mail = new smtp();
        $Db_user = M('User');
        $email =  trim($_POST['email']);
        $pattern = "/^([0-9A-Za-z\\-_\\.]+)@([0-9a-z]+\\.[a-z]{2,3}(\\.[a-z]{2})?)$/i";
        if(!preg_match($pattern,$email)){
            $this->error('您输入的电子邮件地址不合法！');exit;
        }
        $whoisemail_info['email'] =$email;
        $mail->setServer($this->config['mail_config_smtp_host'],
            $this->config['mail_config_smtp_user'],
            $this->config['mail_config_smtp_pwd'],
            $this->config['mail_config_smtp_port'],
            $this->config['mail_config_smtp_auth']);
        $mail->setFrom($this->config['mail_config_sender']); //设置发件人
        $mail->setReceiver($whoisemail_info['email']); //设置收件人，多个收件人，调用多次
        $hosts = $this->top_domain($this->config['site_url']);
        $mail_title = "{$this->config['site_name']}({$hosts})：会员邮箱绑定确认信";
        $data['email_code'] =  sha1(md5($this->user_session['uid'].$whoisemail_info['email']));
        $data['uid'] = $this->user_session['uid'];
        $data['email'] = $email;
        $Db_user->data($data)->save();
       // $send_url =  U('Account/yzemail',array('email_code'=> $data['email_code'] ),true,false,true);
        $send_url =  $this->config['site_url'].'/yzregemail/'. $data['email_code'] . '/'.$this->user_session['uid'];
        $mail_body = <<<Eof

尊敬的用户: <br/>
您好, <br/>
欢迎使用邮箱绑定{$this->config['site_name']}({$hosts})！<br/>
绑定的邮箱：{$whoisemail_info['email']} <br/>
为了保证每个会员的电子邮件真实性，您还需要最后一个步骤才能完成绑定，请点击下面的链接地址进行确认：<br/>
 <a href="{$send_url}"> <font color='red' size='2'>{$send_url}</font> </a> <br/>
(如果链接地址无法直接点击，请将上面的链接地址拷贝到浏览器的地址栏进行激活)<br/>
请注意：会员帐户只有激活后才能使用！<br/>
如果您没有注册成为我们会员，那可能是输入错误，该邮件为系统自动发送邮件，请删除该邮件，为您带来的不便表示歉意。<br/>


如果您有什么疑问或建议，欢迎给我们提供，客服邮箱：{$this->config['site_email']}<br/>

感谢您对{$this->config['site_name']}({$hosts})的支持!<br/>

{$this->config['site_name']}({$hosts})客服部<br/>
{$this->config['site_url']}<br/>
联系电话：+86  {$this->config['site_phone']}<br/>
客服邮箱: {$this->config['site_email']}

Eof;
        $mail->setMail($mail_title , $mail_body); //设置邮件主题、内容
        $mail->sendMail(); //发送
        $this->success('邮件发送成功。');


    }
//
//    Public function yzemail(){
//        dump($_GET);
//    }

	public function logprotect(){
		$this->assign('status',3);
		$Db_Logprotect = M('Logprotect');
		$condition['uid'] =$_SESSION['user']['uid'] ;
		$count_Logprotect = $Db_Logprotect->where($condition)->count();
		if($_POST){
			if($count_Logprotect > 5){
				exit(json_encode(array('error'=>'5','msg'=>'您的安全问题已经超出5个了！')));
			}

			$Db_user = M('User');
			$user_condition['uid'] = $_SESSION['user']['uid'];
			$user_condition['pwd'] = md5(trim($_POST['pwd']));
			$check_pwd = $Db_user->field('`pwd`,`uid`')->where($user_condition)->find();
			if(empty($check_pwd)){
				exit(json_encode(array('error'=>'4','msg'=>'您输入的密码不正确！')));
			}
			$data['question'] = trim($_POST['question']);
			$data['answer'] = md5(trim($_POST['answer']));
			$data['uid'] =  $_SESSION['user']['uid'];
			if($Db_Logprotect->data($data)->add()){
				exit(json_encode(array('error'=>'0','msg'=>'添加成功')));
			}else{
				exit(json_encode(array('error'=>'3','msg'=>'添加异常请重新提交！')));
			}
		}
		$protect_list = $Db_Logprotect->field(true)->where($condition)->limit(5)->select();
		$this->assign('protect_list',$protect_list);


		$this->display();
	}

	public function logprotect_check(){
		$this->assign('status',3);
		unset($_SESSION['pawd_status']);
		$Db_Logprotect = M('Logprotect');
		$pro_condition['uid']=$_SESSION['user']['uid'];
		$count = $Db_Logprotect->where($pro_condition)->count();
		$rand = mt_rand(0,$count -1); //产生随机
		$limit = $rand .','. 1;
		$pro_check = $Db_Logprotect->field('`id`,`question`')->where($pro_condition)->limit($limit)->select();
		$this->assign('pro_check',$pro_check);
		//dump($pro_check);
		if($_POST){
			$Db_user = M('User');
			$user_condition['uid'] = $_SESSION['user']['uid'];
			$user_condition['pwd'] = md5(trim($_POST['pwd']));
			$check_pwd = $Db_user->field('`pwd`,`uid`')->where($user_condition)->find();
			if(empty($check_pwd)){
				$this->error('您输入的密码不正确！');exit;
			}

			$check_data['id'] = intval($_POST['id']);
			$check_data['uid'] = $_SESSION['user']['uid'];
			$check_data['answer'] = md5(trim($_POST['answer']));
			$check_result = $Db_Logprotect->field('`id`,`uid`,`answer`')->where($check_data)->find();
			//echo $Db_Logprotect->getLastSql();exit;
			if($check_result){
				$_SESSION['pawd_status'] = 1;
				$this->redirect('Account/updatepwd', array('check' => 1), 0);exit;
			}else{
				$_SESSION['pawd_status'] = 0;
				$this->error('您输入的答案不正确！');exit;
			}
		}
		$this->display();
	}

	public function logprotect_del(){
		if($_POST){
			$id = isset($_POST['id']) ? intval($_POST['id']) : 0;
			if($id>0){
				$Db_Logprotect = M('Logprotect');
				if($Db_Logprotect->where(array('id'=>$id))->delete()){
					exit(json_encode(array('error'=>'0','msg'=>'删除成功！')));
				}else{
					exit(json_encode(array('error'=>'2','msg'=>'删除失败！')));
				}
			}
			exit(json_encode(array('error'=>'3','msg'=>'删除异常请重新提交！')));
		}
	}



	public function loginlog(){
		$this->assign('status',4);
		$Db_loginlog = M('Loginlog');
		$condition['uid'] =$_SESSION['user']['uid'] ;
		$count_loglist = $Db_loginlog->where($condition)->count();
		import('@.ORG.system_page');
		$p = new Page($count_loglist, 15);
		$log_list = $Db_loginlog->field(true)->where($condition)->order('`log_time` DESC,`log_id` ASC')->limit($p->firstRow . ',' . $p->listRows)->select();
		//echo $Db_loginlog->getLastSql();
		$this->assign('log_list', $log_list);
		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);

		$this->display();
	}

	public function revalidationemail(){
		import('@.ORG.smtp');
		$mail = new smtp();
        $condition['mail_id'] = intval($_POST['id']);
        $condition['uid'] = $this->user_session['uid'];
        $condition['is_check'] = 0;
        $whoisemail_info = D('Domain_whoisemail')->field(true)->where($condition)->find();
        if(empty($whoisemail_info)){
            $this->error('非法访问');exit;
        }
		//$mail->setServer("smtp.qq.com", "2880922995@qq.com", "kbfoahcicpsiddaa", 465, true);
		$mail->setServer($this->config['mail_config_smtp_host'],
            $this->config['mail_config_smtp_user'],
            $this->config['mail_config_smtp_pwd'],
            $this->config['mail_config_smtp_port'],
            $this->config['mail_config_smtp_auth']);
		$mail->setFrom($this->config['mail_config_sender']); //设置发件人
		$mail->setReceiver($whoisemail_info['email']); //设置收件人，多个收件人，调用多次
        $hosts = $this->top_domain($this->config['site_url']);
        $mail_title = "{$this->config['site_name']}({$hosts})：Whois邮箱绑定确认信";
        $data['code'] =  sha1(md5($this->user_session['uid'].$whoisemail_info['email']));
       // dump($data['code']);die;
        $data['uid'] = $this->user_session['uid'];
        $data['mail_id'] =intval($_POST['id']);
        D('Domain_whoisemail')->data($data)->save();
      //  $send_url =  U('Account/yzwhoisemail',array('code'=> $data['code'] ),true,false,true);
        $send_url =  $this->config['site_url'].'/yzemail/'.$data['code'];
        $mail_body = <<<Eof

尊敬的用户: <br/>
您好, <br/>
您在{$this->config['site_name']}({$hosts})申请将邮箱{$whoisemail_info['email']}绑定到您的帐户，以便WHOIS信息验证。请点击以下地址进行确认：<br/>
 <a href="{$send_url}"> <font color='red' size='2'>{$send_url}</font> </a> <br/>
(如果链接地址无法直接点击，请将上面的链接地址拷贝到浏览器的地址栏进行激活)<br/>
确认后，你提交的域名如果联系人邮件为{$whoisemail_info['email']}，将会自动通过验证。<br/>
如果您从未提交过邮箱绑定，请直接删除该邮件，可能是其它会员操作不当引起的，给您带来的不便表示歉意。<br/>

如果您有什么疑问或建议，欢迎给我们提供，客服邮箱：{$this->config['site_email']}<br/>

感谢您对{$this->config['site_name']}({$hosts})的支持!<br/>

{$this->config['site_name']}({$hosts})客服部<br/>
{$this->config['site_url']}<br/>
联系电话：+86  {$this->config['site_phone']}<br/>
客服邮箱: {$this->config['site_email']}

Eof;
		$mail->setMail($mail_title , $mail_body); //设置邮件主题、内容
		$mail->sendMail(); //发送
       // echo "<javascript>alert('ok');</javascript>";
        $this->success('邮件发送成功。');
//		dump($mail);
//		dump($mail->error());
	}

    public function yzwhoisemail(){
        if($_GET){
            $Db_whoisemail = D('Domain_whoisemail');
            $condition['code'] = strval($_GET['code']);
            $condition['uid'] = $this->user_session['uid'];
            $get_check_data =  $Db_whoisemail->field(true)->where($condition)->find();
            if(!empty($get_check_data)){
                $data['is_check'] = 1;
                $data['code'] = '';
                $data['mail_id'] = $get_check_data['mail_id'];
                $Db_whoisemail->data($data)->save();
                $this->assign('jumpUrl',U('Account/whoisemail'));
                $this->success('验证成功。');
                //$this->redirect(, array(),0,'验证成功');
                //$this->redirect(U('Account/whoisemail'), array('check'=>1,'ml'=>$get_check_data['mail_id']),0,'验证成功');

            }else{
                $this->assign('jumpUrl',U('Account/whoisemail'));
                $this->success('该链接已失效。');
                //$this->redirect(U('Account/whoisemail'),  array(),0,'该链接已失效！');
            }

        }

    }

	public function delwhoisemail(){
		if(IS_POST){
			if(empty($_POST['id'])) $this->error('请携带删除的ID');
			if(D('Domain_whoisemail')->where(array('mail_id'=>$_POST['id'],'uid'=>$this->user_session['uid']))->delete()){
				$this->success('删除邮箱成功');
			}else{
				$this->error('删除失败，请重试');
			}
		}else{
			$this->error('非法访问');
		}
	}

        public function updateprofile(){
            $id = $this->user_session['uid'];
            $databases_user = M('User');
            if($_POST){
                $data = $databases_user->save($_POST);
//                echo $databases_user->getLastSql();
                if($data){
                    $this->success('修改成功');
                }  else {
                    $this->success('修改失败 ');
                }
            }  else {
                $where = '`uid`='.$id;
                $user_data = $databases_user->where($where)->find();
                $this->assign('user_data',$user_data);
                $this->display();
            }

        }

        public function updatepwd(){
	        //如果设置了 保护问题，就先验证保护问题
	        $_SESSION['pawd_status'] = 0;
	        if($_SESSION['pawd_status'] === 1){
		        $Db_Logprotect = M('Logprotect');
		        $pro_condition['uid']=$_SESSION['user']['uid'];
		        $count = $Db_Logprotect->where($pro_condition)->count();
		        if($count > 0){
			        $this->redirect('Account/logprotect_check', array('check' => '0'), 0);
		        }
	        }

            if($_POST){
                if($_POST['pwd']['oldpwd'] == "" ||$_POST['pwd']['pwd'] == "" ||$_POST['pwd']['repwd'] == ""  )$this->error('都不可以为空 ');
                if($_POST['pwd']['pwd'] != $_POST['pwd']['repwd'])$this->error('两次密码不一致 ');
                if(is_array($_POST['pwd'])&&!empty($_POST['pwd']))extract($_POST['pwd']);
                $id = $this->user_session['uid'];
                $databases_user = M('User');
                $user_data = $databases_user->where('`uid`='.$id)->find();
                $oldpwd = md5($oldpwd);
                $pwd = md5($pwd);
                $where = '`uid`='.$id;
                $data['pwd'] = $pwd;
                if($oldpwd == $user_data['pwd']){
                    $info = $databases_user->where($where)->save($data);
//                    echo $databases_user->getLastSql();
//                    die;
                unset ($_SESSION['user']);
	            unset($_SESSION['pawd_status']);
                $this->success('修改成功请重新登录');
                }  else {
                    $this->error('请输入正确旧密码 ');
                }
            }else {
                $this->display();
            }
        }


        public function setbankcard(){
            $where['uid'] = $this->user_session['uid'];
            $databases_bank_card = M('Bank_card');
            $info = $databases_bank_card->where($where)->order('id DESC')->select();
//            dump($info);
            $this->assign('info',$info);
            $this->display();
        }

        public function setbankcard_data(){
            $_POST['uid'] = $this->user_session['uid'];
//            $_POST['nickname'] = $this->user_session['nickname'];
            $_POST['addtime'] = time();

            $databases_bank_card = M('Bank_card');
            $info = $databases_bank_card->add($_POST);
            if($info){
                echo "1";
            }  else {
                echo "2";
            }
        }

        //银行卡信息
        public function getbyIdbankcard(){
            $id = intval($_POST['id']);
            $databases_bank_card = M('Bank_card');
            $card_info = $databases_bank_card->where(array('id'=>$id))->find();
            $json = json_encode($card_info);
            exit($json);
        }

        public function upbankcard(){
            $databases_bank_card = M('Bank_card');
            if($databases_bank_card->save($_POST)){
                echo 1;
            }else{
                echo 2;
            }
        }


        public function delbank(){
            $where['id'] = intval($_GET['id']);
            $databases_bank_card = M('Bank_card');
            $databases_bank_card->where($where)->delete();
            $this->success('删除成功');
        }


        public function withdrawal(){
            $where['uid'] =  $this->user_session['uid'];
            $databases_bank_card = M('Bank_card');
            $databases_user = M('User');
            $bank_card_list = $databases_bank_card->where($where)->select();
            $user_info = $databases_user->where(array('uid'=>$this->user_session['uid']))->find();
            $this->assign('bank_card_list',$bank_card_list);
            $this->assign('user_info',$user_info);
            $this->display();
        }

        public function withdrawal_data(){
            if($_POST){
                $databases_user = M('User');
                $databases_usser_deal_record = M('User_deal_record');
                $_POST['uid'] = $this->user_session['uid'] ;
                $_POST['addtime'] = time();
                $where['uid'] = $_POST['uid'];
                $_SESSION['user']['now_money'] -=$_POST['txtmoeny'];
                $info = $databases_usser_deal_record->add($_POST);
                if($info){
                    if($databases_user->where($where)->setDec('now_money',$_POST['txtmoeny'])){
                        $this->success('申请提现成功');
                    }  else {
                        $this->error('申请提现异常');
                    }
                }
            }
        }

        public function withdrawal_record(){
            $id=  $this->user_session['uid'];
            if($_POST){
                if (!empty($_POST['startTime'])) {
                        $where    .= "AND ud.addtime >= ".strtotime($_POST['startTime'])." ";
                }
                if (!empty($_POST['endTime'])) {
                    $endtime = strtotime($_POST['endTime'])+86399;
                        $where .= "AND ud.addtime <= ".$endtime." ";
                }
            }
            $count = M()->table(array(C('DB_PREFIX').'user_deal_record'=>'ud',C('DB_PREFIX').'bank_card'=>'bc'))
                            ->field('ud.txtmoeny,ud.addtime,ud.state,ud.bank_card_id,bc.account')
                            ->where('ud.uid='.$id.' AND ud.bank_card_id = bc.id '.$where)->order('ud.id desc' )->count();
            import('@.ORG.system_page');
            $p = new Page($count, 8);
            $wr_list = M()->table(array(C('DB_PREFIX').'user_deal_record'=>'ud',C('DB_PREFIX').'bank_card'=>'bc'))
                            ->field('ud.txtmoeny,ud.addtime,ud.state,ud.bank_card_id,bc.account')
                            ->where('ud.uid='.$id.' AND ud.bank_card_id = bc.id '.$where)->limit($p->firstRow . ',' . $p->listRows)->order('ud.id desc' )->select();
            $this->assign('wr_list',$wr_list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }

	//财务明细列表
	public function record(){
		$Db_user_money_list = D('User_money_list');
		$condition = null;
		if (!empty($_POST)) {
			date_default_timezone_set('PRC');
			if (!empty($_POST['startTime'])) {
				$condition    = " `time` >= ".strtotime($_POST['startTime']);
			}
			if (isset($_POST['endTime'])) {
				$endtime = strtotime($_POST['endTime']) ? strtotime($_POST['endTime']) : time();
				if (!empty($_POST['startTime'])){
						$condition  .= " AND `time` <= ".($endtime + 86399);
				}else{
					$condition  .= " `time` <= ". ($endtime + 86399);
				}
			}
			if (!empty($_POST['SearchAlltype'])) {
				$condition  .= " AND `type` IN(" . $_POST['SearchAlltype'] .")";
				$type = $_POST['SearchAlltype'];
			}
		}
		if(isset($_GET['type']) && intval($_GET['type']) === 1){
			$condition  .= "  `type` = " .  intval($_GET['type']);
			$type = intval($_GET['type']);
		}

		$this->assign('SearchAlltype',$type);

		if(!empty($condition)){
			$condition  .= " AND `uid` = " . $this->user_session['uid'];
		}else{
			$condition  .= " `uid` = " . $this->user_session['uid'];
		}

		$count_user_money = $Db_user_money_list->where($condition)->count();
		import('@.ORG.system_page');
		$p = new Page($count_user_money, 15);
		$user_money_list = $Db_user_money_list->field(true)->where($condition)->order('`time` DESC,`pigcms_id` ASC')->limit($p->firstRow . ',' .
			$p->listRows)->select();
		//echo $Db_user_money_list->getLastSql();
		$this->assign('user_money_list', $user_money_list);
		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);
		$this->display();
	}

	//冻结详细
	public function frozendetail(){
		$Db_user_freeze = M('User_freeze');
		$condition['uid'] =  $this->user_session['uid'];
		$count_user_freeze = $Db_user_freeze->where($condition)->count();
		import('@.ORG.system_page');
		$p = new Page($count_user_freeze, 15);
		$user_freeze_list = $Db_user_freeze->field(true)->where($condition)->order('`freezetime` DESC,`id` ASC')->limit($p->firstRow . ',' .
			$p->listRows)->select();

		$this->assign('user_freeze_list', $user_freeze_list);
		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);
		$this->display();
	}

	public function recharge(){
		$pay_method = D('Config')->get_pay_method($notOnline,1,true);
		$this->assign('pay_method', $pay_method);
		$this->display();
	}
	public function recharge_save(){
		$data_user_recharge_order['uid'] = $this->user_session['uid'];
		$money = floatval($_GET['money']);
		if(empty($money) || $money > 200000){
			$this->error('请输入有效的金额！最高不能超过20万元。');
		}
		$data_user_recharge_order['money'] = $money;
		$data_user_recharge_order['add_time'] = $_SERVER['REQUEST_TIME'];
		if($order_id = D('User_recharge_order')->data($data_user_recharge_order)->add()){
			redirect(U('Index/Pay/go_pay',array('order_id'=>$order_id,'order_type'=>'recharge','pay_type'=>$_GET['pay_type'])));
		}
	}

    public function  videoauction(){
        $uid = $this->user_session['uid'];
        $where = ' vd.`lastFidId` =  vdl.`pigcms_id`  AND vdl.`uid` = '.$uid;
        $count_video =  M()->table(array(C('DB_PREFIX').'Videosale_domains_list'=>'vdl',C('DB_PREFIX').'Videosale_domains'=>'vd'))->where($where)->count();
        if(!empty($count_video)){
            import('@.ORG.system_page');
            $p = new Page($count_video, 25);
            $field = 'vd.`domains`,vd.`now_money`,vdl.`money`,vdl.`time`';
            $videosale =  M()->table(array(C('DB_PREFIX').'Videosale_domains_list'=>'vdl',C('DB_PREFIX').'Videosale_domains'=>'vd'))->field($field)->where($where)->order('`time` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();

            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);

        }
        $this->assign('list', $videosale);

        $this->display();
    }

    public function videoauction_success(){
        $uid = $this->user_session['uid'];
        $where = ' vd.`lastFidId` =  vdl.`pigcms_id`  AND vdl.`uid` = '.$uid .' AND vd.`domain_status` = 2';
        $counts =  M()->table(array(C('DB_PREFIX').'Videosale_domains_list'=>'vdl',C('DB_PREFIX').'Videosale_domains'=>'vd'))->where($where)->count();
        import('@.ORG.system_page');
        $p = new Page($counts, 25);
        $field = 'vd.`domains`,vd.`now_money`';
        $list_video =  M()->table(array(C('DB_PREFIX').'Videosale_domains_list'=>'vdl',C('DB_PREFIX').'Videosale_domains'=>'vd'))->field($field)->where($where)->order('vd.`domain_id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
       
        $pagebar = $p->show();
        $this->assign('pagebar', $pagebar);

        $this->assign('list', $list_video);
        $this->display();
    }

    public function mibiao(){
        $Db_user = M('User');
        $Db_mibiao = M('Mibiao');
       // $userinfo = $Db_user->field('`uid`,`email`,`qq`,`phone`')->where('`uid`='.$this->user_session['uid'])->find();
        $userinfo = $Db_mibiao->where('`uid`='.$this->user_session['uid'])->find();
        if(empty($userinfo)){
            $userinfo = $Db_user->field('`uid`,`email`,`qq`,`phone`')->where('`uid`='.$this->user_session['uid'])->find();
        }
        $this->assign('uid',$this->user_session['uid']);
        $this->assign('mibiaoinfo',$userinfo);
        $this->display();

    }

    public function mibiaoupdate(){
        $Db_user = M('User');
        $Db_mibiao = M('Mibiao');
        if(IS_POST){
            $mid  =  intval($_POST['mid']);
            $_POST['uid'] = $this->user_session['uid'];

                if($mid <= 0){
                        //add
                    if($Db_mibiao->add($_POST)){
                        $this->success('设置成功');exit;
                    }else{
                        $this->error('设置失败，请重新设置');exit;
                    }
                }else{
                    //save
                    if($Db_mibiao->data($_POST)->save()){
                        $this->success('更新成功');exit;
                    }else{
                        $this->error('设置更新失败，请重新设置');exit;
                    }
                }
        }

    }

    public function whois(){
        if(IS_POST){
            $check_domains  = $_POST['arr_yms'];
            $count = count($check_domains);
            $uid = intval($_POST['whoid']);
            $result_list = array();
            // import("@.Action.WhoisAction");
            $Db_cache_search_domains = D('Cache_search_domains');
            $Db_domain_whoisemail = D('Domain_whoisemail');
            for($i = 0; $i < $count ;$i++){
                // echo $i.'==>'.$check_domains[$i].'---';
                $keyword = $check_domains[$i];
                $get_reg_email = $Db_cache_search_domains->field('`cache_id`,`domainName`,`registrarEmail`')->where("`domainName`='{$keyword}'")->find();
                if($get_reg_email['registrarEmail']){ //系统已经获取信息
                    $lowemail = strtolower($get_reg_email['registrarEmail']);
                    $is_uid_email =   $Db_domain_whoisemail->field('`is_check`')->where("`uid` = $uid AND `email` = '{$lowemail}'   AND `is_check` = 1")->find();
                    if($is_uid_email){ //whois 邮箱已经验证是本人
                        $result_list[$i]  = 1;
                    }else{
                        $result_list[$i]  = 0;
                    }
                    //echo  $i.'==>'.$result_list[$i].'---';
                }else{
                    $result_list[$i]  = 2;

                } // TODO 需要重新获取whois信息
                //  WhoisAction::index('True');
                //dump($result_list);
            }

            echo json_encode($result_list);die;

        }else{
            echo "";
        }
    }


}
