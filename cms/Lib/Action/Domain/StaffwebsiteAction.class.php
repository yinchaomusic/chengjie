<?php

class StaffwebsiteAction extends BaseAction{
    protected function _initialize(){
        parent::_initialize();
        session_start();
        $static_path   = './tpl/Domain/'.C('DEFAULT_THEME').'/static/';
        $static_public = './static/';
        $this->assign('static_path',$static_path);
        $this->assign('static_public',$static_public);
        $aba = $this->staff_session = session('staff');
        $this->assign('staff_session',$this->staff_session);
        if(ACTION_NAME != "login" && ACTION_NAME != "checkLogin"){
            if($_SESSION['staff']==false){
                $this->error('请先登录',U('Staff/login'));
            }
        }
    }
    
    
    // 代购列表
    public  function  listing_purchasing(){
        $Db_web_entrust = M('Web_entrust');
        
        $searchtype = $_POST['search_type'];
        $keyword = trim($_POST['keyword']);
        $where['wstatus'] = 0 ;//初始状态
        $where['entr_type'] = 1 ;  //代购
        if(!empty($keyword)){
            if($searchtype == 'phone'){
                $where['phone'] = array('like',"%$keyword%");
            }
            if($searchtype == 'uid'){
                $where['user_id'] = array('like',"%$keyword%");
            }
            if($searchtype == 'email'){
                $where['email'] = array('like',"%$keyword%");
            }
        }

        $count = $Db_web_entrust->where($where)->count();
        import('@.ORG.system_page');
        $p = new Page($count, 15);
        $not_entrust_list = $Db_web_entrust->where($where)->order('`createdate` DESC')->limit($p->firstRow . ',' .
            $p->listRows)->select();
        //dump($Db_web_entrust->getLastSql());exit;
         
        $pagebar = $p->show();
        
        $this->assign('pagebar', $pagebar);
        $this->assign('bt_list',$not_entrust_list);
        $this->display();
    }
    
    //代售  列表
    public  function  listing_consignee(){
        $Db_web_entrust = M('Web_entrust');
    
        $searchtype = $_POST['search_type'];
        $keyword = trim($_POST['keyword']);
        $where['wstatus'] = 0;//初始状态
        $where['entr_type'] = 2 ;  //代售
        if(!empty($keyword)){
            if($searchtype == 'phone'){
                $where['phone'] = array('like',"%$keyword%");
            }
            if($searchtype == 'uid'){
                $where['user_id'] = array('like',"%$keyword%");
            }
            if($searchtype == 'email'){
                $where['email'] = array('like',"%$keyword%");
            }
        }
    
        $count = $Db_web_entrust->where($where)->count();
        import('@.ORG.system_page');
        $p = new Page($count, 15);
        $not_entrust_list = $Db_web_entrust->where($where)->order('`createdate` DESC')->limit($p->firstRow . ',' .
            $p->listRows)->select();
        //dump($Db_web_entrust->getLastSql());exit;
         
        $pagebar = $p->show();
    
        $this->assign('pagebar', $pagebar);
        $this->assign('bt_list',$not_entrust_list);
        $this->display();
    }
    
    //详情
    public function sitedetails(){
        $Db_web_entrust = M('Web_entrust');
        $id = intval($_GET['id']);
        $website_info = $Db_web_entrust->where(array('id'=>$id))->find();
        $this->assign('info',$website_info);
        $this->display();
    }
    
    
    //审核通过
    public function pass(){
         $Db_web_entrust = M('Web_entrust');
         $check_list = $Db_web_entrust->where(array('id'=>$_POST['id']))->find();
         if(empty($check_list)){
             echo 2 ; exit ;
         }
         if($check_list['wstatus'] != 0){
             echo 2 ; exit ;
         }
         $data['wstatus'] = 1;  //审核通过
         $data['updatedate'] = date("Y-m-d H:i:s" ,time());
         $result =  $Db_web_entrust->where(array('id'=>$_POST['id']))->data($data)->save();
         echo $result ;
    }
    
    //审核不存在
    public function back(){
        $Db_web_entrust = M('Web_entrust');
        $check_list = $Db_web_entrust->where(array('id'=>$_POST['id']))->find();
        if(empty($check_list)){
            echo 2 ; exit ;
        }
        if($check_list['wstatus'] != 0){
            echo 2 ; exit ;
        }
        $data['wstatus'] = 2;  //审核不通过
        $data['updatedate'] = date("Y-m-d H:i:s" ,time());
        $data['fail_reason'] = $_POST['fail_reason'];
        $result =  $Db_web_entrust->where(array('id'=>$_POST['id']))->data($data)->save();
        echo $result ;
    }
    
    
    //结束
    public function over(){
        $Db_web_entrust = M('Web_entrust');
        $check_list = $Db_web_entrust->where(array('id'=>$_POST['id']))->find();
        if(empty($check_list)){
            echo 2 ; exit ;
        }
        if($check_list['wstatus'] != 1){
            echo 2 ; exit ;
        }
        $data['wstatus'] = 4;  //已结束
        $data['updatedate'] = date("Y-m-d H:i:s" ,time());
        $result =  $Db_web_entrust->where(array('id'=>$_POST['id']))->data($data)->save();
        echo $result ;
    }
    
}

?>