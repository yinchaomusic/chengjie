<?php
/*
 * 用户前台基础类
 *
 */
class BaseAction extends CommonAction{
	protected $user_level_info;
	protected $topdomain;

    protected function _initialize(){
	    define('RI_LIXI',0.005); //日利息 0.5%/日
		parent::_initialize();
		if(!function_exists('fdksajflkjsadlkjblkfndlqkwtnGroupqwlkrIndexqwrewqrmbvlknasdfa')){
			redirect('http://www.pigcms.com');
		}
		
		$this->static_path   = './tpl/Domain/'.C('DEFAULT_THEME').'/static/';
		$this->static_public = './static/';
		$this->assign('static_path',$this->static_path);
		$this->assign('static_public',$this->static_public);
			//ini_set("session.cookie_domain",'chengjie.com');
	    //底部导航==》关于我们
		if(IS_GET){
			$Db_about = M('About');
			$condition_about['state'] = 1;
			//只取4条
			$footer_aoubt_list = $Db_about->field(true)->where($condition_about)->limit(4)->select();
			$this->assign('footer_aoubt_list',$footer_aoubt_list);
		}
        //获取网站名称和网站顶级域名

        $this->assign('now_site_name',$this->config['site_name']);
        $this->assign('now_site_short_url',self::top_domain($this->config['site_url']));
	    $this->topdomain = self::top_domain($this->config['site_url']);

//	    //左侧菜单【帮助中心】==》协议规则读取
//	    $Db_domain_traderule = M('Domain_traderule');
//	    $condition_rule['status'] = 1;
//	    $trade_rule_list =  $Db_domain_traderule->field('`rid`,`title`,`url`')->where($condition_rule)->order('`sorts` DESC,`update_time` DESC,`rid`
//	    ASC')
//		    ->limit
//	    (10)
//		    ->select();
////dump($trade_rule_list);
//	    $this->assign('trade_rule_list',$trade_rule_list);

	    //左侧菜单【帮助中心】==
		if(IS_GET){
			$Db_news_category = M('News_category');
			$footer_cate_news_conditioin['cat_key']  = 'user_guide';
			$footer_cate_news_conditioin['cat_state']    = 1;
			$footer_cate_guide_info =   $Db_news_category->field(true)->where($footer_cate_news_conditioin)->find();
			$Db_news = M('News');
			$footer_news_condition['state'] = 1;
			$footer_news_condition['cat_id'] = $footer_cate_guide_info['cat_id'];
			$footer_guide_list =  $Db_news->field('`news_id`,`news_title`')->limit(3)->where($footer_news_condition)->select();

			$this->assign('footer_guide_list',$footer_guide_list);
		}
		if(IS_GET){
			$user_level = M('user_level');
			$level_list = $user_level->select();
			$this->assign('level_list',$level_list);
        }
		
		$user_info = D('User')->get_user($this->user_session['uid']);
		$this->user_level_info = $this->user_Level($user_info['score_count']);
		$this->assign('user_level_info',$this->user_level_info);
	}
        
        public function is_collection($id){
            $databases_domain_collection = M('Domain_collection');
            $show = $databases_domain_collection->where(array('uid'=>  $this->user_session['uid'],'domain_id'=>$id))->find();
            if($show){
                return 1;
            }  else {
                return 2;
            }
        }

	public function regular_domain($domain)

	{
		if (substr ( $domain, 0, 7 ) == 'http://') {
			$domain = substr ( $domain, 7 );
		}
		if (strpos ( $domain, '/' ) !== false) {
			$domain = substr ( $domain, 0, strpos ( $domain, '/' ) );
		}
		return strtolower ( $domain );
	}
	public  function top_domain($domain) {
		$domain = self::regular_domain ( $domain );
		$iana_root = array (
			'com',
			'cc',
			'cn',
			'org',
			'ac',
			'ad',
			'ae',
			'aero',
			'af',
			'ag',
			'ai',
			'al',
			'am',
			'an',
			'ao',
			'aq',
			'ar',
			'arpa',
			'as',
			'asia',
			'at',
			'au',
			'aw',
			'ax',
			'az',
			'ba',
			'bb',
			'bd',
			'be',
			'bf',
			'bg',
			'bh',
			'bi',
			'biz',
			'bj',
			'bl',
			'bm',
			'bn',
			'bo',
			'bq',
			'br',
			'bs',
			'bt',
			'bv',
			'bw',
			'by',
			'bz',
			'ca',
			'cat',
			'cd',
			'cf',
			'cg',
			'ch',
			'ci',
			'ck',
			'cl',
			'cm',
			'co',
			'coop',
			'cr',
			'cu',
			'cv',
			'cw',
			'cx',
			'cy',
			'cz',
			'de',
			'dj',
			'dk',
			'dm',
			'do',
			'dz',
			'ec',
			'edu',
			'ee',
			'eg',
			'eh',
			'er',
			'es',
			'et',
			'eu',
			'fi',
			'fj',
			'fk',
			'fm',
			'fo',
			'fr',
			'ga',
			'gb',
			'gd',
			'ge',
			'gf',
			'gg',
			'gh',
			'gi',
			'gl',
			'gm',
			'gn',
			'gov',
			'gp',
			'gq',
			'gr',
			'gs',
			'gt',
			'gu',
			'gw',
			'gy',
			'hk',
			'hm',
			'hn',
			'hr',
			'ht',
			'hu',
			'id',
			'ie',
			'il',
			'im',
			'in',
			'info',
			'int',
			'io',
			'iq',
			'ir',
			'is',
			'it',
			'je',
			'jm',
			'jo',
			'jobs',
			'jp',
			'ke',
			'kg',
			'kh',
			'ki',
			'km',
			'kn',
			'kp',
			'kr',
			'kw',
			'ky',
			'kz',
			'la',
			'lb',
			'lc',
			'li',
			'lk',
			'lr',
			'ls',
			'lt',
			'lu',
			'lv',
			'ly',
			'ma',
			'mc',
			'md',
			'me',
			'mf',
			'mg',
			'mh',
			'mil',
			'mk',
			'ml',
			'mm',
			'mn',
			'mo',
			'mobi',
			'mp',
			'mq',
			'mr',
			'ms',
			'mt',
			'mu',
			'museum',
			'mv',
			'mw',
			'mx',
			'my',
			'mz',
			'na',
			'name',
			'nc',
			'ne',
			'net',
			'nf',
			'ng',
			'ni',
			'nl',
			'no',
			'np',
			'nr',
			'nu',
			'nz',
			'om',

			'pa',
			'pe',
			'pf',
			'pg',
			'ph',
			'pk',
			'pl',
			'pm',
			'pn',
			'pr',
			'pro',
			'ps',
			'pt',
			'pw',
			'py',
			'qa',
			're',
			'ro',
			'rs',
			'ru',
			'rw',
			'sa',
			'sb',
			'sc',
			'sd',
			'se',
			'sg',
			'sh',
			'si',
			'sj',
			'sk',
			'sl',
			'sm',
			'sn',
			'so',
			'sr',
			'ss',
			'st',
			'su',
			'sv',
			'sx',
			'sy',
			'sz',
			'tc',
			'td',
			'tel',
			'tf',
			'tg',
			'th',
			'tj',
			'tk',
			'tl',
			'tm',
			'tn',
			'to',
			'tp',
			'tr',
			'travel',
			'tt',
			'tv',
			'tw',
			'tz',
			'ua',
			'ug',
			'uk',
			'um',
			'us',
			'uy',
			'uz',
			'va',
			'vc',
			've',
			'vg',
			'vi',
			'vn',
			'vu',
			'wf',
			'ws',
			'xxx',
			'ye',
			'yt',
			'za',
			'zm',
			'zw'
		);
		$sub_domain = explode ( '.', $domain );
		$top_domain = '';
		$top_domain_count = 0;
		for($i = count ( $sub_domain ) - 1; $i >= 0; $i --) {
			if ($i == 0) {
				// just in case of something like NAME.COM
				break;
			}
			if (in_array ( $sub_domain [$i], $iana_root )) {
				$top_domain_count ++;
				$top_domain = '.' . $sub_domain [$i] . $top_domain;
				if ($top_domain_count >= 2) {
					break;
				}
			}
		}
		$top_domain = $sub_domain [count ( $sub_domain ) - $top_domain_count - 1] . $top_domain;
		return $top_domain;
	}
        
        
        public function user_Level($level_count)
	{
            $databases_user_level = M('User_level');
            $user_level_list = $databases_user_level->select();
            foreach ($user_level_list as $k=>$v){
                if($level_count>=$v['integral']){
                    $level_info['lname'] = $v['lname'];
                    $level_info['boon'] = $v['boon'];
                    $level_info['img'] = $v['img'];
                }
            }
            return $level_info;
        }
}
