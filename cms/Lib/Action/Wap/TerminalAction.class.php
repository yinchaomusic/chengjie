<?php
/*
 * 终端域名
 *
 */
class TerminalAction extends BaseAction{
    
	public function index(){
            $this->display();

	}
        
        public function terminal_add(){
            $databases_terminal = M('Terminal');
            $row = $databases_terminal->where(array('domain_name'=>$_POST['domain_name'],'uid'=>  $this->user_session['uid']))->find();
            if($row){
                exit(json_encode(array('error'=>'3','msg'=>'添加失败，请不要重复提交域名!！','dom_id'=>'domain_name')));
            }
            $_POST['staff_id'] = $this->user_session['staff_uid'];
            $_POST['uid'] = $this->user_session['uid'];
            $_POST['add_time'] = time();
            $info = $databases_terminal->add($_POST);
            if($info){
                exit(json_encode(array('error'=>'0','msg'=>'添加成功，等待经纪人审核！','dom_id'=>'domain_name')));
            }  else {
                exit(json_encode(array('error'=>'1','msg'=>'添加失败，请重试！','dom_id'=>'domain_name')));
            }
        }
        
}