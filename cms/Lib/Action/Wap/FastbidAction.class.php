<?php

/**
 * Class FastbidAction
 * 极速竞价
 */


class FastbidAction extends BaseAction{

	public function index(){
		$typeid = $_GET['typeid'];
		if($typeid == 1){
				$orderby = '`money` DESC';
				$this->assign('status_type',1);
		}elseif($typeid == 2 ){
				$orderby = '`length` DESC';
				$this->assign('status_type',2);
		}else{
				$orderby = '`add_time` ASC';
				$this->assign('status_type',0);
		}
		
		$this->assign('error_no',0);
		$databaseDomains = D('Domains');
		$conditionDomains['status'] = '1';
		$conditionDomains['type'] = '2';
		$allCount = $databaseDomains->where($conditionDomains)->count();
		$fastbidList = $databaseDomains->field('`domain_id`,`domain`,`label`,`money`,`type`,`startTime`,`endTime`','`desc`')->where($conditionDomains)->order($orderby)->limit(20)->select();
		
		foreach($fastbidList as &$domainValue){
		$domainValue['money'] = floatval($domainValue['money']);
		$domainValue['over_time'] = getOverTime($domainValue['endTime']);
		switch($domainValue['type']){
			case '1':
				$domainValue['url'] = U('Hotsale/selling',array('id'=>$domainValue['domain_id']));
				break;
			case '2':
				$domainValue['url'] = U('Fastbid/detail',array('id'=>$domainValue['domain_id']));
				break;
			default:
				$domainValue['url'] = U('Buydomains/detail',array('id'=>$domainValue['domain_id']));
		}
	}
			
		
		$tradelist_count = array('id'=>1);
		$this->assign('fastbidList',$fastbidList);
//            dump($fastbidList);
		$this->display();
	}
	
	//专题拍卖
	public function lists(){
		$typeid = $_GET['typeid'];
		if($typeid == 1){
				$orderby = '`money` DESC';
				$this->assign('status_type',1);
		}elseif($typeid == 2 ){
				$orderby = '`length` DESC';
				$this->assign('status_type',2);
		}else{
				$orderby = '`add_time` ASC';
				$this->assign('status_type',0);
		}
		$page = $_GET['page'] ? $_GET['page'] : '1';
		$this->assign('error_no',0);
		$databaseDomains = D('Domains');
		$conditionDomains['status'] = '1';
		$conditionDomains['type'] = '2';
		if($_GET['id']){
			$conditionDomains['domain_id'] = $_GET['id'];
		}
		$allCount = $databaseDomains->where($conditionDomains)->count();
		$fastbidList = $databaseDomains->field('`domain_id`,`domain`,`label`,`money`,`type`,`startTime`,`endTime`','`desc`')->where($conditionDomains)->order($orderby)->limit((($page-1)*3).',3')->select();
		
		foreach($fastbidList as &$domainValue){
			$domainValue['id'] = $domainValue['domain_id'];
			$domainValue['remarks'] = $domainValue['label'];
			$domainValue['createDate'] = date('Y-m-d H:i:s',$domainValue['add_time']);
			$domainValue['updateDate'] = date('Y-m-d H:i:s',$domainValue['startTime']);
			$domainValue['name'] = $domainValue['domain'];
			$domainValue['clientId'] = 0;
			$domainValue['beginTime'] = date('Y-m-d H:i:s',$domainValue['startTime']);
			$domainValue['endTime'] = date('Y-m-d H:i:s',$domainValue['endTime']);
			
			//保证金
			$plusPrice = getFastbidPriceList($domainValue['money']);
			$domainValue['deposit'] = $plusPrice['depositPrice'];
			
			$domainValue['description'] = $domainValue['desc'];
			$domainValue['type'] = '00';
			$domainValue['reservePrice'] = 999999;
			$domainValue['increment'] = $plusPrice['plusPrice'];
			$domainValue['status'] = '03';
			$domainValue['bonusShareTotal'] = 0;
			$domainValue['bonusShareNumber'] = 0;
			$domainValue['bonusSecond'] = 0;
			$domainValue['currAmount'] = floatval($domainValue['money']);
			
			//是否有关注
			$domainValue['attentioned'] = false;
			if($_SESSION['openid'] && M('Domain_collection')->where(array('uid'=>$this->user_session['uid'],'domain_id'=>$domainValue['domain_id']))->find()){
				$domainValue['attentioned'] = true;
			}
			
			
			$domainValue['hasNews'] = false;
			$domainValue['bidCount'] = false;
			
			//用户信息
			$tmpUser = M('User')->where(array('uid'=>$domainValue['uid']))->find();
			$domainValue['client']['id'] = $tmpUser['uid'];
			$domainValue['client']['remarks'] = '';
			$domainValue['client']['createDate'] = date('Y-m-d H:i:s',$tmpUser['add_time']);
			$domainValue['client']['updateDate'] = date('Y-m-d H:i:s',$tmpUser['last_time']);
			$domainValue['client']['dyid'] = 0;
			$domainValue['client']['openid'] = $tmpUser['openid'];
			$domainValue['client']['name'] = $tmpUser['nickname'];
			$domainValue['client']['nickname'] = $tmpUser['nickname'];
			$domainValue['client']['email'] = $tmpUser['email'];
			$domainValue['client']['mobile'] = $tmpUser['phone'];
			$domainValue['client']['qq'] = $tmpUser['qq'];
			$domainValue['client']['wx'] = $tmpUser['qq'];
			$domainValue['client']['photo'] = empty($tmpUser['avatar']) ? $this->static_path.'images/ui_1_03_03.png' : $tmpUser['avatar'];
			
			//出价记录
			$domainValue['bidCount'] = 0;
			$domainValue['bidList'] = array();
			if($domainValue['lastFidId']){
				$fidList = D('')->table(array(C('DB_PREFIX').'domains_fastbid_list'=>'dfl',C('DB_PREFIX').'user'=>'u'))->where("`dfl`.`domain_id`='{$domainValue['domain_id']}' AND `dfl`.`uid`=`u`.`uid`")->order('`dfl`.`pigcms_id` DESC')->select();
				foreach($fidList as $key=>$value){
					$fidList[$key]['bidAmount'] = $value['money'];
					$fidList[$key]['bidTime'] = date('m-d H:i:s',$value['time']);
					$fidList[$key]['bidCount'] = 0;
					$fidList[$key]['type'] = '普通出价';
					$fidList[$key]['photo'] = empty($value['avatar']) ? $this->static_path.'images/ui_1_03_03.png' : $value['avatar'];
				}
				if($fidList){
					$domainValue['bidCount'] = count($fidList);
					$domainValue['bidList']  = array_slice($fidList,0,3);
				}
			}
			
			$domainValue['attentionCount'] = -1;
			$domainValue['attentionList'] = array();
		}
		
		$json_show['count'] = $allCount;
		$json_show['domainList'] = $fastbidList;
		
		list($s1, $s2) = explode(' ', microtime()); 
		
		$json_show['sysServerTime'] = sprintf('%.0f', (floatval($s1) + floatval($s2)) * 1000);
		// echo ($json_show['sysServerTime']);exit;
		$json_show['shareBonusEnable'] = '0';
		$json_show['currClient'] = '';
		$json_show['userOpenid'] = '';
		if($_SESSION['openid']){
			$now_user = D('User')->get_user($_SESSION['openid'],'openid');
			if($now_user){
				$currClient['nickname'] = $now_user['nickname'];
				$currClient['id'] = $now_user['uid'];
				$currClient['is_follow'] = $now_user['is_follow'];
				$json_show['currClient'] = $currClient;
				$json_show['userOpenid'] = $_SESSION['openid'];
			}
		}
		
		// dump($json_show);exit;
		$this->assign('json_show',$json_show);
		if(IS_AJAX){
			header('Content-Type: application/json;charset=UTF-8');
			
			if($fastbidList){
				$return['data'] = $json_show;
				$return['msg'] = '';
				$return['type'] = 'success';
			}else{
				$return['data'] = array();
				$return['msg'] = '没有更多域名了';
				$return['type'] = 'warn';
			}
			echo json_encode($return);
		}else{
			$this->display();
		}
	}
	public function lists_detail(){
		$domain_id = intval($_GET['id']);
		$databaseDomains = D('Domains');
		$now_domain = $databaseDomains->field(true)->where(array('domain_id'=>$domain_id))->find();$this->assign('fidList',$fidList);
		$this->assign('now_domain',$now_domain);
		
		$fidList = D('')->table(array(C('DB_PREFIX').'domains_fastbid_list'=>'dfl',C('DB_PREFIX').'user'=>'u'))->where("`dfl`.`domain_id`='{$now_domain['domain_id']}' AND `dfl`.`uid`=`u`.`uid`")->order('`dfl`.`pigcms_id` DESC')->select();
		foreach($fidList as $key=>$value){
			$fidList[$key]['bidAmount'] = $value['money'];
			$fidList[$key]['money'] =  getFriendMoney($value['money'],true);
			$fidList[$key]['bidTime'] = date('m-d H:i:s',$value['time']);
			$fidList[$key]['bidCount'] = 0;
			$fidList[$key]['type'] = '普通出价';
			$fidList[$key]['photo'] = empty($value['avatar']) ? $this->static_path.'images/ui_1_03_03.png' : $value['avatar'];
		}
		// dump($fidList);
		$this->assign('fidList',$fidList);
		$this->display();
	}
	public function paimai_detail(){
		$domain_id = intval($_GET['id']);
		$databaseDomains = D('Domains');
		$now_domain = $databaseDomains->field(true)->where(array('domain_id'=>$domain_id))->find();$this->assign('fidList',$fidList);
		$this->assign('now_domain',$now_domain);
		
		$fidList = D('')->table(array(C('DB_PREFIX').'domains_fastbid_list'=>'dfl',C('DB_PREFIX').'user'=>'u'))->where("`dfl`.`domain_id`='{$now_domain['domain_id']}' AND `dfl`.`uid`=`u`.`uid`")->order('`dfl`.`pigcms_id` DESC')->select();
		foreach($fidList as $key=>$value){
			$fidList[$key]['bidAmount'] = $value['money'];
			$fidList[$key]['money'] =  getFriendMoney($value['money'],true);
			$fidList[$key]['bidTime'] = date('m-d H:i:s',$value['time']);
			$fidList[$key]['bidCount'] = 0;
			$fidList[$key]['type'] = '普通出价';
			$fidList[$key]['photo'] = empty($value['avatar']) ? $this->static_path.'images/ui_1_03_03.png' : $value['avatar'];
		}
		// dump($fidList);
		$this->assign('fidList',$fidList);
		$this->display();
	}
	public function paimai_error(){
		$this->display();
	}
	public function ibuyData(){
		$typeid = $_GET['typeid'];
		if($typeid == 1){
				$orderby = '`money` DESC';
				$this->assign('status_type',1);
		}elseif($typeid == 2 ){
				$orderby = '`length` DESC';
				$this->assign('status_type',2);
		}else{
				$orderby = '`add_time` ASC';
				$this->assign('status_type',0);
		}
		$page = $_GET['page'] ? $_GET['page'] : '1';
		$this->assign('error_no',0);
		$databaseDomains = D('Domains');
		$conditionDomains['status'] = '1';
		$conditionDomains['type'] = '2';
		if($_GET['id']){
			$conditionDomains['domain_id'] = $_GET['id'];
		}else if($_REQUEST['domainIdList']){
			$conditionDomains['domain_id'] = array('in',$_REQUEST['domainIdList']);
		}
		$allCount = $databaseDomains->where($conditionDomains)->count();
		$fastbidList = $databaseDomains->field('`domain_id`,`domain`,`label`,`money`,`type`,`startTime`,`endTime`','`desc`')->where($conditionDomains)->order($orderby)->select();
		
		foreach($fastbidList as &$domainValue){
			$domainValue['id'] = $domainValue['domain_id'];
			$domainValue['remarks'] = $domainValue['label'];
			$domainValue['createDate'] = date('Y-m-d H:i:s',$domainValue['add_time']);
			$domainValue['updateDate'] = date('Y-m-d H:i:s',$domainValue['startTime']);
			$domainValue['name'] = $domainValue['domain'];
			$domainValue['clientId'] = 0;
			$domainValue['beginTime'] = date('Y-m-d H:i:s',$domainValue['startTime']);
			$domainValue['endTime'] = date('Y-m-d H:i:s',$domainValue['endTime']);
			
			//保证金
			$plusPrice = getFastbidPriceList($domainValue['money']);
			$domainValue['deposit'] = $plusPrice['depositPrice'];
			
			$domainValue['description'] = $domainValue['desc'];
			$domainValue['type'] = '00';
			$domainValue['reservePrice'] = 999999;
			$domainValue['increment'] = $plusPrice['plusPrice'];
			$domainValue['status'] = '03';
			$domainValue['bonusShareTotal'] = 0;
			$domainValue['bonusShareNumber'] = 0;
			$domainValue['bonusSecond'] = 0;
			$domainValue['currAmount'] = floatval($domainValue['money']);
			
			$domainValue['attentioned'] = false;
			//是否有关注
			$domainValue['attentioned'] = false;
			if($_SESSION['openid'] && M('Domain_collection')->where(array('uid'=>$this->user_session['uid'],'domain_id'=>$domainValue['domain_id']))->find()){
				$domainValue['attentioned'] = true;
			}
			
			$domainValue['hasNews'] = false;
			
			//用户信息
			$tmpUser = M('User')->where(array('uid'=>$domainValue['uid']))->find();
			$domainValue['client']['id'] = $tmpUser['uid'];
			$domainValue['client']['remarks'] = '';
			$domainValue['client']['createDate'] = date('Y-m-d H:i:s',$tmpUser['add_time']);
			$domainValue['client']['updateDate'] = date('Y-m-d H:i:s',$tmpUser['last_time']);
			$domainValue['client']['dyid'] = 0;
			$domainValue['client']['openid'] = $tmpUser['openid'];
			$domainValue['client']['name'] = $tmpUser['nickname'];
			$domainValue['client']['nickname'] = $tmpUser['nickname'];
			$domainValue['client']['email'] = $tmpUser['email'];
			$domainValue['client']['mobile'] = $tmpUser['phone'];
			$domainValue['client']['qq'] = $tmpUser['qq'];
			$domainValue['client']['wx'] = $tmpUser['qq'];
			$domainValue['client']['photo'] = empty($tmpUser['avatar']) ? $this->static_path.'images/ui_1_03_03.png' : $tmpUser['avatar'];
			
			//出价记录
			$domainValue['bidCount'] = 0;
			$domainValue['bidList'] = array();
			if($domainValue['lastFidId']){
				$fidList = M('')->table(array(C('DB_PREFIX').'domains_fastbid_list'=>'dfl',C('DB_PREFIX').'user'=>'u'))->where("`dfl`.`domain_id`='{$domainValue['domain_id']}' AND `dfl`.`uid`=`u`.`uid`")->order('`dfl`.`pigcms_id` DESC')->select();
				foreach($fidList as $key=>$value){
					$fidList[$key]['bidAmount'] = $value['money'];
					$fidList[$key]['bidTime'] = date('m-d H:i:s',$value['time']);
					$fidList[$key]['bidCount'] = 0;
					$fidList[$key]['type'] = '普通出价';
					$fidList[$key]['photo'] = empty($value['avatar']) ? $this->static_path.'images/ui_1_03_03.png' : $value['avatar'];
				}
				if($fidList){
					$domainValue['bidCount'] = count($fidList);
					$domainValue['bidList']  = array_slice($fidList,0,3);
				}
			}
			
			//关注域名人列表
			
			$domainValue['attentionList'] = M('')->table(array(C('DB_PREFIX').'domain_collection'=>'dc',C('DB_PREFIX').'user'=>'u'))->where("`dc`.`domain_id`='{$domainValue['domain_id']}' AND `dc`.`uid`=`u`.`uid`")->order('`dc`.`id` DESC')->select();
			if($domainValue['attentionList']){
				foreach($domainValue['attentionList'] as &$attentionValue){
					$attentionValue['avatar'] = empty($attentionValue['avatar']) ? $this->static_path.'images/ui_1_03_03.png' : $attentionValue['avatar'];
				}
			}else{
				$domainValue['attentionList'] = array();
			}
			$domainValue['attentionCount'] = count($domainValue['attentionList']);
		}
		
		$json_show['ibuyData'] = $fastbidList;
		
		list($s1, $s2) = explode(' ', microtime()); 
		
		$json_show['sysServerTime'] = sprintf('%.0f', (floatval($s1) + floatval($s2)) * 1000);
		$json_show['timeStamp'] = $json_show['sysServerTime'];

		// dump($json_show);exit;
		$this->assign('json_show',$json_show);
		if(IS_AJAX){
			header('Content-Type: application/json;charset=UTF-8');
			
			if($fastbidList){
				$return['data'] = $json_show;
				$return['msg'] = '';
				$return['type'] = 'success';
			}else{
				$return['data'] = array();
				$return['msg'] = '没有更多域名了';
				$return['type'] = 'warn';
			}
			echo json_encode($return);
		}else{
			$this->display();
		}
	}
	public function share(){
		header('Content-Type: application/json;charset=UTF-8');
		$share = new WechatShare($this->config,$_SESSION['openid'],htmlspecialchars_decode($_GET['href']));
		$share->getSgin();
		$return['data'] = $share->sign_data;
		$return['type'] = 'success';
		$return['data']['shareClientId'] = $_SESSION['openid'];
		echo json_encode($return);
	}
	//域名收藏
	public function collection(){
		header('Content-Type: application/json;charset=UTF-8');
		$_POST['uid'] = $this->user_session['uid'];
		if(!$_POST['uid']){
			exit(json_encode(array('type'=>'error','error'=>'3','msg'=>'请先登录！')));
		}
		$databases_domain_collection = M('Domain_collection');
		$row = $databases_domain_collection->where(array('uid'=>$_POST['uid'],'domain_id'=>$_POST['domain_id']))->select();
		if($row){
			$info = $databases_domain_collection->where(array('uid'=>$_POST['uid'],'domain_id'=>$_POST['domain_id']))->delete();
			if($info){
				exit(json_encode(array('type'=>'success','error'=>'1','msg'=>'已取消收藏！')));
			}
		}  else {
			$info = $databases_domain_collection->add($_POST);
			if($info){
				exit(json_encode(array('type'=>'success','error'=>'0','msg'=>'收藏成功！')));
			}else{
				exit(json_encode(array('type'=>'error','error'=>'2','msg'=>'失败请重试！')));
			}
		}
		
	}
	public function detail(){
		$this->assign('error_no',0);
		$domain_id = intval($_GET['id']);
		$collection_show = $this->is_collection($domain_id);
		$this->assign('collection_show',$collection_show);
		if(empty($domain_id)){
			$this->assign('error_no',1);
			$this->assign('redirect_url',U('Fastbid/index'));
			$this->assign('erro_msg','咦？您是怎么进来的？这是人家的私密地方哦 <br\/> 快！带我回去。');
		}
		$databaseDomains = D('Domains');
		$now_domain = $databaseDomains->field(true)->where(array('domain_id'=>$domain_id))->find();
		if(empty($now_domain)){
			$this->assign('error_no',1);
			$this->assign('redirect_url',U('Fastbid/index'));
			$this->assign('erro_msg','当前域名不存在或已被删除。 <br\/> 快点带本宫回去。');
		}
		if($now_domain['status'] != 1){
			$this->assign('error_no',1);
			$this->assign('redirect_url',U('Fastbid/index'));
			$this->assign('erro_msg','当前域名正在审核中或者当前域名已经出售。 <br\/> 快点带本宫回去。');

		}
		if($now_domain['endTime'] < $_SERVER['REQUEST_TIME']){
			$this->endFastBid($now_domain);
			$this->assign('error_no',1);
			$this->assign('redirect_url',U('Fastbid/index'));
			$this->assign('erro_msg','当前竞价已结束。 <br\/> 快点带本宫回去。');

		}

		D('Domains')->where(array('domain_id'=>$domain_id))->setInc('hits');

		$now_domain['overplus_time']    = $now_domain['endTime'] - $_SERVER['REQUEST_TIME'];
		$now_domain['overplus_time_d']  = floor($now_domain['overplus_time']/86400);
		$now_domain['overplus_time_dh'] = $now_domain['overplus_time']%86400;

		$now_domain['overplus_time_h']  = floor($now_domain['overplus_time_dh']/3600);
		$now_domain['overplus_time_hm']  = $now_domain['overplus_time_dh']%3600;

		$now_domain['overplus_time_m']  = floor($now_domain['overplus_time_hm']/60);
		$now_domain['overplus_time_ms']  = $now_domain['overplus_time_hm']%60;

		$now_domain['overplus_time_s']  = floor($now_domain['overplus_time_ms']/60);
		$now_domain['hits'] += 1;
		if($now_domain['lastFidId'] && $now_domain['money'] >= $now_domain['mark_money']){
			$now_domain['plusPrice'] = getFastbidPriceList($now_domain['money']);
			$now_domain['pleasePrice'] = $now_domain['plusPrice']['plusPrice'] + $now_domain['money'];
		}else{
			$now_domain['plusPrice'] = getFastbidPriceList($now_domain['mark_money']);
			$now_domain['pleasePrice'] = $now_domain['plusPrice']['plusPrice'] + $now_domain['mark_money'];
		}
		$now_domain['money'] = getFriendMoney($now_domain['money'],true);
		$now_domain['mark_money'] = getFriendMoney($now_domain['mark_money'],true);
		$this->assign('now_domain',$now_domain);

                if($now_domain['lastFidId']){
			$fidList = D('Domains_fastbid_list')->where(array('domain_id'=>$now_domain['domain_id']))->order('`pigcms_id` DESC')->select();
			foreach($fidList as $key=>$value){
				$fidList[$key]['money'] = getFriendMoney($value['money'],true);
			}
			$this->assign('fidList',$fidList);
		}
//                dump($now_domain['pleasePrice']);

		//$allPlusPrice = getFastbidPriceList(-1);
		 //出价记录
//		$html_tr = '';
//		$status = 0;
//		 foreach($allPlusPrice as $value){
//			 if(){
//
//			 }
//			 $html_tr .="<tr> <td>{$value['']}</td><td></td> <td></td><td></td></tr>";
//		 }
//
//$html=<<<HTMLTB
//\<article\> \<section\>\<ul class="explain_list"\>\<li\>\<span\>拍卖出价范围\<\/span\>\<span\>保证金\<\/span\>\<\/li\>\<li\>\<span\>1-199元\<\/span\>\<span\>20元\<\/span\>\<\/li\>\<\/ul\>\<\/section\>\<\/article\>
//HTMLTB;


		//$this->assign('allPlusPrice',$allPlusPrice);
	//	$this->assign('html',$html);

		$this->display();

	}

        
    public function fastbidmoney(){
		if(empty($this->user_session)){
                    exit(json_encode(array('error'=>'1','msg'=>'请先登录！')));
		}
		$this->user_session = D('User')->get_user($this->user_session['uid']);
		$domain_id = $_POST['auctionId'];
		if(empty($domain_id)){
                    exit(json_encode(array('error'=>'2','msg'=>'请携带ID！')));
		}
		$databaseDomains = D('Domains');
		$now_domain = $databaseDomains->field(true)->where(array('domain_id'=>$domain_id))->find();
		if(empty($now_domain)){
                    exit(json_encode(array('error'=>'3','msg'=>'当前域名不存在或已被删除！')));
		}
		if($now_domain['type'] != 2){
                    exit(json_encode(array('error'=>'4','msg'=>'当前域名不属于竞价！')));
		}
		if($this->user_session['uid'] == $now_domain['uid']){
                    exit(json_encode(array('error'=>'5','msg'=>'您不能购买自己的ID！')));
		}
		if($now_domain['status'] != '1' || $now_domain['endTime'] < $_SERVER['REQUEST_TIME']){
                    exit(json_encode(array('error'=>'6','msg'=>'当前竞价已结束！')));
		}
		if($now_domain['lastFidId'] && $now_domain['money'] >= $now_domain['mark_money']){
			$now_domain['plusPrice'] = getFastbidPriceList($now_domain['money']);
			$now_domain['pleasePrice'] = $now_domain['plusPrice']['plusPrice'] + $now_domain['money'];
		}else{
			$now_domain['plusPrice'] = getFastbidPriceList($now_domain['mark_money']);
			$now_domain['pleasePrice'] = $now_domain['plusPrice']['plusPrice'] + $now_domain['mark_money'];
		}
		if($_POST['offer'] < $now_domain['pleasePrice']){
                    exit(json_encode(array('error'=>'7','msg'=>'您的价格必须大于或等于 ￥'.$now_domain['pleasePrice'])));
		}
		if($this->user_session['now_money'] < $now_domain['plusPrice']['depositPrice']){
                    exit(json_encode(array('error'=>'8','msg'=>'余额不足')));
		}
		if(in_array(1,$this->user_session['permissions'])){
			$freeze = 0;
		}else{
			$freeze = $now_domain['plusPrice']['depositPrice'];
		}
		$databaseUser = D('User');
		$conditionUser['uid'] = $this->user_session['uid'];
		$dataUser['now_money'] = $this->user_session['now_money'] - $freeze;
		$dataUser['freeze_money'] = $this->user_session['freeze_money'] + $freeze;
		if($databaseUser->where($conditionUser)->data($dataUser)->save()){
			//添加到冻结记录表
			$data_user_freeze['uid'] = $this->user_session['uid'];
			$data_user_freeze['freezetime'] = $_SERVER['REQUEST_TIME'];
			$data_user_freeze['freezemoney'] = $freeze;
			$data_user_freeze['info'] = '竞价域名 '.$now_domain['domain'].'冻结保证金';
			$data_user_freeze['type'] = '6';
			$freeze_id = D('User_freeze')->data($data_user_freeze)->add();
			
			//添加到资金记录表
			D('User_money_list')->add_row($this->user_session['uid'],12,$freeze,$dataUser['now_money'],'竞价域名 '.$now_domain['domain'].' 冻结保证金');
			
			//添加到竞价记录表
			$dataDomainsFastbidList['time'] = time();
			$dataDomainsFastbidList['freeze_id'] = $freeze_id;
			$dataDomainsFastbidList['domain_id'] = $now_domain['domain_id'];
			$dataDomainsFastbidList['money'] = $_POST['offer'];
			$dataDomainsFastbidList['uid'] = $this->user_session['uid'];
			$lastFidId = D('Domains_fastbid_list')->data($dataDomainsFastbidList)->add();
			
			//修改域名状态
			$dataDomain = array('money'=>$_POST['offer'],'lastFidId'=>$lastFidId);
			if(time()-300 >= $now_domain['endTime']){
				$dataDomain['endTime'] = $now_domain['endTime']+300;
			}
			D('Domains')->where(array('domain_id'=>$now_domain['domain_id']))->data($dataDomain)->save();
			
			
			//将上一位用户的保证金还回去
			if($now_domain['lastFidId']){
				$lastFid = D('Domains_fastbid_list')->field(true)->where(array('pigcms_id'=>$now_domain['lastFidId']))->find();
				
				$condition_last_user_freeze['id'] = $lastFid['freeze_id'];
				$now_freeze = D('User_freeze')->where($condition_last_user_freeze)->find();
				
				$databaseUser = D('User');
				$lastFidUser = $databaseUser->get_user($lastFid['uid']);
				$conditionLastFidUser['uid'] = $lastFidUser['uid'];
				$dataLastFidUser['now_money'] = $lastFidUser['now_money'] + $now_freeze['freezemoney'];
				$dataLastFidUser['freeze_money'] = $lastFidUser['freeze_money'] - $now_freeze['freezemoney'];
				if($databaseUser->where($conditionLastFidUser)->data($dataLastFidUser)->save()){
					D('User_freeze')->where($condition_last_user_freeze)->delete();
					// dump(D('User_freeze')->getLastSql());
					D('User_money_list')->add_row($lastFidUser['uid'],13,$now_freeze['freezemoney'],$dataLastFidUser['now_money'],'竞价域名 '.$now_domain['domain'].' 解冻保证金');
				}
			}
                        exit(json_encode(array('error'=>'0','msg'=>'成功')));
		}else{
                        exit(json_encode(array('error'=>'9','msg'=>'异常')));
		}
	}

	public function showdetail(){
		$allPlusPrice = getFastbidPriceList(-1);
		$this->assign('allPlusPrice',$allPlusPrice);
		$this->display();
	}

	public function showpricerec(){
		$domain_id = intval($_GET['id']);
		if($domain_id <= 0){
			$this->assign('error_no',1);
			$this->assign('redirect_url',U('Fastbid/index'));
			$this->assign('erro_msg','咦？您是怎么进来的？这是人家的私密地方哦 <br\/> 快！带我回去。');
		}
		$Db_domains = D('Domains');
		$now_domain = $Db_domains->field('`lastFidId`, `domain_id`')->where('`status` = 1 AND  `domain_id` = ' .$domain_id )->find();

		if($now_domain['lastFidId']){
			$fidList = D('Domains_fastbid_list')->where(array('domain_id'=>$now_domain['domain_id']))->order('`pigcms_id` DESC')->select();
			foreach($fidList as $key=>$value){
				$fidList[$key]['money'] = getFriendMoney($value['money'],true);
			}

			$this->assign('fidList',$fidList);
		}
		$this->display();
	}

	function getOverTime($expireTime){
		$overTime = $expireTime - $_SERVER['REQUEST_TIME'];
		$time = '';
		if($overTime/86400 > 1){
			if(floor(($overTime%86400)/3600) != 23){
				$time .= floor($overTime/86400).'天';
				if($overTime%86400 != 0){
					$time .= ceil(($overTime%86400)/3600).'时';
				}
			}else{
				$time .= (floor($overTime/86400)+1).'天';
			}

		}else if($overTime/3600 > 1){
			if(floor(($overTime%3600)/60) != 59){
				$time .= floor($overTime/3600).'时';
				if($overTime%3600 != 0){
					$time .= ceil(($overTime%3600)/60).'分';
				}
			}else{
				$time .= (floor($overTime/3600)+1).'时';
			}
		}else if($overTime/60 > 1){
			if(floor(($overTime%3600)/60) != 59){
				$time .= floor($overTime/60).'分';
				if($overTime%60 != 0){
					$time .= floor(($overTime%60)/60).'秒';
				}
			}else{
				$time .= (floor($overTime/60)+1).'分';
			}
		}else{
			$time .= floor($overTime/60).'秒';
		}
		return $time;
	}
}