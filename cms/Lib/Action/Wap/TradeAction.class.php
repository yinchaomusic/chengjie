<?php



class TradeAction extends BaseAction{



	public function tradesearch(){

		//查询 域名 交易相关信息
		$keyword = strtolower(trim($_GET['keyword']));
		$get_str =  array();
		$get_str = explode('.',$keyword);
		$this->assign('error_no',0);
		if(count($get_str) < 0|| $keyword ==''){
			$this->assign('error_no',1);
			$this->assign('erro_msg','咦？你从哪里来的啊？这里不是地球啊。<br\/> 亲，我们还是回地球吧。');
		}
		$top_domans = $get_str[0];
		//域名相关交易
		$Db_domains = M('Domains');
		$domains_con['status'] = 1;
		$domains_con['domain'] = array('like','%'.$top_domans.'%');
		$tradelist_count = $Db_domains->field("`type` ,count(`type`) as counts")->where($domains_con)
			->group('`type`')->order('`type` DESC')
			->select();
		$this->assign('no_data',0);
		if(count($tradelist_count) <= 0){
			$this->assign('no_data',2);
			$this->assign('erro_msg','还没有此域名相关的交易记录哦。<br\/> 我们看看别的吧。');
		}
		$domains_com['status'] = 1;
		//$domains_con['domain'] = array('like','%'.$top_domans.'%');
		$tradelist_count = $Db_domains->field("`type` ,count(`type`) as counts")->where($domains_com)
			->group('`type`')->order('`type` DESC')
			->limit(16)
			->select();

//`type` tinyint(1) NOT NULL COMMENT '0议价，1一口价，2竞价，3优质',
		//2 竞价
		$domains_condition_ja['type'] = 2;
		$domains_condition_ja['status'] = 1;

		$domains_ja_list = $Db_domains->field(true)->where($domains_condition_ja)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')
			->limit(4)->select();

		 foreach($domains_ja_list as &$val){
			 $val['over_time'] = getOverTime($val['endTime']);
		 }


		$this->assign('domains_ja_list', $domains_ja_list);

		//1 一口价域名
		$domains_condition_ykj['type'] = 1;
		$domains_condition_ykj['status'] = 1;
		$domains_ykj_list = $Db_domains->field(true)->where($domains_condition_ykj)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit
		(4)->select();
		$this->assign('domains_ykj_list', $domains_ykj_list);

		//0 -议价域名
		$domains_condition_yj['type'] = 0;
		$domains_condition_yj['status'] = 1;
		$domains_yj_list = $Db_domains->field(true)->where($domains_condition_yj)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit
		(4)->select();
		$this->assign('domains_yj_list', $domains_yj_list);

		// 3 优质
		$domains_condition_yz['type'] = 3;
		$domains_condition_yz['status'] = 1;
		$domains_yz_list = $Db_domains->field(true)->where($domains_condition_yz)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit
		(4)->select();
		$this->assign('domains_yz_list', $domains_yz_list);


		$this->assign('tradelist_count',$tradelist_count);
		$this->display();

	}

	function getOverTime($expireTime){
		$overTime = $expireTime - $_SERVER['REQUEST_TIME'];
		$time = '';
		if($overTime/86400 > 1){
			if(floor(($overTime%86400)/3600) != 23){
				$time .= floor($overTime/86400).'天';
				if($overTime%86400 != 0){
					$time .= ceil(($overTime%86400)/3600).'时';
				}
			}else{
				$time .= (floor($overTime/86400)+1).'天';
			}

		}else if($overTime/3600 > 1){
			if(floor(($overTime%3600)/60) != 59){
				$time .= floor($overTime/3600).'时';
				if($overTime%3600 != 0){
					$time .= ceil(($overTime%3600)/60).'分';
				}
			}else{
				$time .= (floor($overTime/3600)+1).'时';
			}
		}else if($overTime/60 > 1){
			if(floor(($overTime%3600)/60) != 59){
				$time .= floor($overTime/60).'分';
				if($overTime%60 != 0){
					$time .= floor(($overTime%60)/60).'秒';
				}
			}else{
				$time .= (floor($overTime/60)+1).'分';
			}
		}else{
			$time .= floor($overTime/60).'秒';
		}
		return $time;
	}
}