<?php
/*
 * 批量交易 域名
 *
 */
class PldomainAction extends BaseAction{
	public function index(){
            $typeid = intval($_GET['typeid']) ? intval($_GET['typeid']) : 0;
            $this->assign('typeid',$typeid);
            if($typeid == 1){
                    $orderby = '`total_price` DESC';
            }else{
                    $orderby = '`add_time` DESC';
            }
            $bulk_trading = M('Bulk_trading');
            $count = $bulk_trading->where(array('status'=>1))->count();
            import('@.ORG.system_page');
            $p = new Page($count, 12);
            $domain_list = $bulk_trading->field(true)->where(array('status'=>1))->order($orderby)->limit($p->firstRow . ',' . $p->listRows)->select();
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            foreach ($domain_list as $k=>$v){
                $domain_list[$k]['domain_list'] = unserialize($v['domain_list']);
            }
            $this->assign('domain_list',$domain_list);

            $this->display();
	}
	public function detail(){
            $bulk_id = intval($_GET['id']);
            $bulk_trading = M('Bulk_trading');
            $where = '`bulk_id` = '.$bulk_id;
            $domain_list = $bulk_trading->where($where)->find();
            $domain_list['domain_list'] = unserialize($domain_list['domain_list']);
            $domain_list['unit_price'] = $domain_list['total_price']/$domain_list['amount'];
            $domain_list['margin_boon'] = intval($this->user_level_info['boon']/100*$domain_list['total_price']);
            $this->assign('domain_list',$domain_list);
//            dump($domain_list);
            $this->display();
	}
        //用户确认购买叫保证金
        public function buy_margin(){
            $databases_bulk_trading = M('Bulk_trading');
            $databases_order = M('Order');
            $databases_order_info = M('Order_info');
            $databases_user = M('User');
            $databases_user_freeze = M('User_freeze');
            $databases_user_money_list = M('User_money_list');
            $bulk_info = $databases_bulk_trading->where(array('bulk_id'=>  intval($_POST['bulk_id'])))->find();
            if($bulk_info['status'] == 2){//判断此域名是否在出售
                exit (json_encode(array('error'=>'1',"msg"=>"此域名正在出售")));
            }
            if($bulk_info['uid'] == $this->user_session){
                exit (json_encode(array('error'=>'2',"msg"=>"不能购买自己出售的域名")));
            }
            $user_info = $databases_user->where(array('uid'=>  $this->user_session['uid']))->find();
            $_POST['boon'] = $this->user_level_info['boon']/100;
            if($user_info['now_money'] < $bulk_info['total_price']*$_POST['boon']){//钱
                exit (json_encode(array('error'=>'3',"msg"=>"余额不足，请充值")));
            }
            
            $data['info'] = "批量交易用户保证金冻结";
            $data['type'] = 5;
            $data['freezetime'] = time();
            $data['uid'] = $this->user_session['uid'];
            $data['freezemoney'] = $bulk_info['total_price']*$_POST['boon'];
            $f_back = $databases_user_freeze->add($data);
            if($f_back){
                $order_data['addTime'] = time();
                $order_data['total_price'] = $bulk_info['total_price'];
                $order_data['trade_type'] = 2;
                $order_data['domain_id'] = intval($_POST['bulk_id']);
                $order_data['domainName'] = $bulk_info['title'];
                $order_data['payTye'] = 1;
                $order_data['buy_id'] = $this->user_session['uid'];
                $order_data['staff_id'] = $this->user_session['staff_uid'];
                if($order_data['staff_id']>0)  $order_data['status'] = 1;
                $order_data['freeze_id'] = $f_back;
                $order_data['addTime'] = time();
                $order_data['yes_no'] = 3;
                $order_data['sell_id'] = $bulk_info['uid'];
                $o_back = $databases_order->add($order_data);
                
                $order_info_data['order_id'] = $o_back;
                $order_info_data['info'] = "批量交易买家以支付保证金";
                $order_info_data['time'] = time();
                $order_info_data['operator'] = $this->user_session['uid'];
                $databases_order_info->add($order_info_data);
                
                if($o_back){
                    $u_back = $databases_user->where(array('uid'=>  $this->user_session['uid']))->setDec('now_money',$data['freezemoney']); // 用户的金额减少
                    if($u_back){
                        $user_money_list_data['type'] = 13;
                        $user_money_list_data['uid'] = $this->user_session['uid'];
                        $user_money_list_data['money'] = $data['freezemoney'];
                        $user_money_list_data['desc'] = "批量交易保证金冻结";
                        $user_money_list_data['now_money'] = $user_info['now_money']-$data['freezemoney'];
                        $user_money_list_data['time'] = time();
                        $uml_back = $databases_user_money_list->add($user_money_list_data);
                        if($uml_back){
                            $databases_bulk_trading->where(array('bulk_id'=>  intval($_POST['bulk_id'])))->setField("status",2);
                            exit (json_encode(array('error'=>'0',"msg"=>"保证金支付成功，如果购买请支付全款")));
                        }  else {
                            exit (json_encode(array('error'=>'4',"msg"=>"保证金支付失败，如果需要请重试")));
                        }
                    }  else {
//                        用户的金额减少
                        exit (json_encode(array('error'=>'5',"msg"=>"保证金支付失败，如果需要请重试")));
                    }
                }  else {
//                    订单
                    exit (json_encode(array('error'=>'6',"msg"=>"保证金支付失败，如果需要请重试")));
                }
            }  else {
//                冻结
                exit (json_encode(array('error'=>'7',"msg"=>"保证金支付失败，如果需要请重试")));
            }
        }
        
        
        public function freeze_margin(){

            $databases_purchase = M('Purchase');
            $databases_user = M('User');
            $databases_user_freeze = M('User_freeze');
            $databases_user_money_list = M('User_money_list');
            
            $data['info'] = "委托代购保证金冻结资金";
            $data['type'] = 3;
            $data['freezetime'] = time();
            $data['uid'] = $_POST['uid'];
            $data['freezemoney'] = $_POST['margin'];
            $user_money_list_data['type'] = 13;
            $user_money_list_data['uid'] = $_POST['uid'];
            $user_money_list_data['money'] = $_POST['margin'];
            $user_money_list_data['desc'] = "委托代购保证金冻结";
            $user_money_list_data['now_money'] = $_POST['now_money']-$_POST['margin'];
            $user_money_list_data['time'] = time();
//            $user_money_list_data['ip'] = get_client_ip();
            
            $u_info = $databases_user->where(array('uid'=>$_POST['uid']))->setDec('now_money',$_POST['margin']); // 用户的金额减少
            if($u_info){
                $f_info = $databases_user_freeze->add($data);
                if($f_info){
                    $p_info = $databases_purchase-> where(array("ph_id"=>$_POST['ph_id']))->setField(array('status'=>4,'freeze_id'=>$f_info));
//                    echo $databases_purchase->getLastSql();
                    if($p_info){
                        $databases_user_money_list->add($user_money_list_data);
                        echo 1;
                    }  else {
                        echo 2;
                    }
                }  else {
                    echo 2;
                }
            }  else {
                echo 2;
            }
        }
        
        
        
        
        
}