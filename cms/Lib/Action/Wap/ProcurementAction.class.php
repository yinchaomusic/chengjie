<?php
/*
 * 域名代购
 *
 */
class ProcurementAction extends BaseAction{
	public function index(){
         $d = trim($_GET['d']);
         $this->assign('d',$d);
            $this->display();
	}
        public function add(){
            
             $domain = trim($_POST['domain']);
             $check_domain =  $this->top_domain($domain);
             $check_domain = explode('.', $check_domain);

             
            if($check_domain[1])
            {
                $purchase = M('Purchase');
                $data['staff_id']=$_SESSION['user']['staff_uid'];
                if($data['staff_id']){
                    $data['status'] = 1;
                }
                $data['uid']=$_SESSION['user']['uid'];
                if(empty ($data['uid']))exit(json_encode(array('error'=>'2','msg'=>'请先登录！')));
                $data['uname']=$_SESSION['user']['nickname'];
                $data['add_time']= time();
                $data['domain']=$_POST['domain'];
                $data['buyers_price']=$_POST['buyers_price'];
                $data['message']=$_POST['message'];
                $data['type']=0;
                $row = $purchase->add($data);
                if($row){
                    exit(json_encode(array('error'=>'0','msg'=>'增加代购成功！')));
                }  else {
                    exit(json_encode(array('error'=>'1','msg'=>'增加代购失败！')));
                }
            }else{
                exit(json_encode(array('error'=>'2','msg'=>'请输入正确的域名！')));
            }
        }
}