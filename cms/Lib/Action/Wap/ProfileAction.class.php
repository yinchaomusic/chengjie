<?php

/**
 * 我 - 控制器
 */

class ProfileAction extends  BaseAction{
        protected function _initialize(){
		parent::_initialize();
		if(empty($this->user_session)){
			redirect(U('Login/login'));
		}
	}
	public function index(){
            $id = $this->user_session['uid'];
            $databases_order = M('Order');
            $where = '(`buy_id`='.$id.' OR sell_id = '.$id.') AND (`status` = 0 OR `status` = 1)';
            $count_order = $databases_order->where($where)->count();
            $this->assign('count_order',$count_order);
            
            $this->display();
	}

	/**
	 * 点击我的头像
	 */
	public function profile(){
            $uid = $this->user_session['uid'];
            $databases_user = M('User');
            $user_info = $databases_user->where(array('uid'=>$uid))->find();
            $this->assign('user_info',$user_info);
            //获取我的米表
            $mburl = M('Mibiao')->field('mburl')->where(array('uid'=>$uid))->find();
           // dump($mburl);
            $this->assign('mburl',$mburl['mburl']);
            $this->display();
	}
        public function user_show(){
            $uid = $this->user_session['uid'];
            $databases_user = M('User');
            $user_info = $databases_user->where(array('uid'=>$uid))->find();
            $this->assign('user_info',$user_info);
//            dump($user_info);
            $this->display();
        }
        
        public function user_save(){
            $databases_user = M('User');
            $info = $databases_user->where(array('uid'=>  $this->user_session['uid']))->save($_POST);
            if($info){
                exit(json_encode(array('error'=>'0','msg'=>'修改成功。')));
            }  else {
                exit(json_encode(array('error'=>'1','msg'=>'失败请重试。')));
            }
            
        }

	/**
	 * 我的交易
	 */
	public function transaction(){
            $id = $this->user_session['uid'];
            $databases_order = M('Order');
            $where = '(`buy_id`='.$id.' OR sell_id = '.$id.') AND (`status` = 0 OR `status` = 1)';
            $count_order = $databases_order->where($where)->count();
            import('@.ORG.system_page');
            $p = new Page($count_order, 10);
            $order_list = $databases_order->where($where)->order('`order_id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
            $this->assign("order_list",$order_list);
//            dump($order_list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
	}

	/**
	 * 账户余额
	 */
        public function withdrawal(){
            $databases_user = M('User');
            $user_info = $databases_user->field('`uid`,`now_money`,`freeze_money`')->where(array('uid'=>$this->user_session['uid']))->find();
            $this->assign('user_info',$user_info);
            $this->display();
        }
        //冻结明细列表
        public function user_freeze(){
            $Db_user_freeze = M('User_freeze');
            $condition['uid'] =  $this->user_session['uid'];
            $count_user_freeze = $Db_user_freeze->where($condition)->count();
            import('@.ORG.system_page');
            $p = new Page($count_user_freeze, 15);
            $user_freeze_list = $Db_user_freeze->field(true)->where($condition)->order('`freezetime` DESC,`id` ASC')->limit($p->firstRow . ',' .
                $p->listRows)->select();

            $this->assign('user_freeze_list', $user_freeze_list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }
        public function user_money_list(){
            $databases_user_money_list = M('User_money_list');
            $user_money_list_info = $databases_user_money_list->where(array('uid'=>  $this->user_session['uid']))->order('pigcms_id desc')->select();
            $this->assign('user_money_list_info',$user_money_list_info);
            $this->display();
            
        }

     public function record(){
         $Db_user_money_list = D('User_money_list');
         $condition = null;
         if (!empty($_POST)) {
             date_default_timezone_set('PRC');
             if (!empty($_POST['startTime'])) {
                 $condition    = " `time` >= ".strtotime($_POST['startTime']);
             }
             if (isset($_POST['endTime'])) {
                 $endtime = strtotime($_POST['endTime']) ? strtotime($_POST['endTime']) : time();
                 if (!empty($_POST['startTime'])){
                     $condition  .= " AND `time` <= ".($endtime + 86399);
                 }else{
                     $condition  .= " `time` <= ". ($endtime + 86399);
                 }
             }
             if (!empty($_POST['SearchAlltype'])) {
                 $condition  .= " AND `type` IN(" . $_POST['SearchAlltype'] .")";
                 $type = $_POST['SearchAlltype'];
             }
         }
         if(isset($_GET['type']) && intval($_GET['type']) === 1){
             $condition  .= "  `type` = " .  intval($_GET['type']);
             $type = intval($_GET['type']);
         }

         $this->assign('SearchAlltype',$type);

         if(!empty($condition)){
             $condition  .= " AND `uid` = " . $this->user_session['uid'];
         }else{
             $condition  .= " `uid` = " . $this->user_session['uid'];
         }

         $count_user_money = $Db_user_money_list->where($condition)->count();
         import('@.ORG.system_page');
         $p = new Page($count_user_money, 15);
         $user_money_list = $Db_user_money_list->field(true)->where($condition)->order('`time` DESC,`pigcms_id` ASC')->limit($p->firstRow . ',' .
             $p->listRows)->select();
         $this->assign('user_money_list', $user_money_list);
         $pagebar = $p->show();
         $this->assign('pagebar', $pagebar);
         $this->display();
     }
        /**
	 * 我的域名
	 */
        public function mydomain(){
            
        }
        
        
        public function is_check_phone(){
            $this->display();
        }
        public  function sendCode(){
            $phone = I('phone', 0);
            $phone = trim($_POST['phone']);
            if (preg_match("/^1[0-9]{10}$/", $phone)) {
                if (! $phone) {
                    exit(json_encode(array('error'=>'4','msg'=>'手机号有误，请重新输入')));
                }
                $code = mt_rand(1000, 9999);
                $text = '您的验证码是：' . $code . '。此验证码20分钟内有效，请不要把验证码泄露给其他人。如非本人操作，可不用理会！';
                    $send_time = time();
                    $expire_time  = time() + 1200;

                $return = Sms::sendSms(array( 'content' => $text, 'mobile' => $phone, 'uid' => $_SESSION['user']['uid'], 'type' => 'domains_user',
                    'send_time'=>$send_time,'expire_time'=>$expire_time,'extra'=>$code));
                exit(json_encode(array('error'=>'0','msg'=>'获取验证码成功，请及时查看~','expire_time'=>$expire_time)));
            } else {
                exit(json_encode(array('error'=>'4','msg'=>'手机号有误，请重新输入')));
            }
            if (! $phone) {
                    $this->returnCode('20042001');
            }

	}
        
        public function bindphone(){
            if(IS_POST){
                $Db_Sms_record = M('Sms_record');
                $check_condition['uid'] = $_SESSION['user']['uid'];
                $check_condition['type'] = 'domains_user';
                $check_condition['extra'] = trim($_POST['txtcode']);
                $check_condition['phone'] = trim($_POST['phone']);
                $get_check = $Db_Sms_record->field('`uid`,`extra`,`expire_time`')->order('`pigcms_id` DESC')->where($check_condition)->find();
                if($get_check['expire_time'] > time() && !empty($get_check['extra'])){
                    $check_data['is_check_phone'] = 1;
                    $check_data['phone'] = trim($_POST['phone']);
                    $check_data['uid'] = $_SESSION['user']['uid'];
                    $Db_user = M('User');
                    if($Db_user->data($check_data)->save()){
                        exit(json_encode(array('error'=>'0','msg'=>'手机验证成功~')));
                    }else{
                        exit(json_encode(array('error'=>'1','msg'=>'校验出现异常，请稍候再试。~')));
                    }
                }else{
                    exit(json_encode(array('error'=>'2','msg'=>'校验码已超时，或者手机号不存在请重新获取~')));
                }

            }
	}









        public function logout(){
            session('user',null);
            exit(json_encode(array('error'=>'0','msg'=>'退出成功')));
        }
        
        

}