<?php

/**
 * WAP查询 类
 *
 */
class SearchAction extends BaseAction{

    public function index(){

        //获取域名后缀
        $databaseDomainSuffix = D('Domain_suffix');
        $conditionDomainSuffix['is_hots'] = '1';
        $suffixList = $databaseDomainSuffix->field('`id`,`suffix`')->where($conditionDomainSuffix)->order('`sorts` DESC,`id` ASC')->select();
        $this->assign('suffixList',$suffixList);

        //主题属性
        $databaseDomainTreasure = D('Domain_treasure');
        $conditionDomainTreasure['status'] = '1';
        $treasureList = $databaseDomainTreasure->field(true)->where($conditionDomainTreasure)->order('`sorts` DESC,`id` ASC')->select();
        $this->assign('treasureList',$treasureList);


        $this->display();
    }

    public function search(){
        //交易类型 type: 0议价，1一口价，2竞价，3优质
        // theme: 主题
        //dlength 域名长度
        //nowpirce 当前价格
        // isinclude 是否包含
        // suffix 域名后缀
        /**
         * Array
        (
        [keyword] => 222.cn
        [type] => 2,1,0,3
        [theme] => 2,4,5,7,8
        [dlength] => 2
        [nowprice] => 2
        [isinclude] => 1
        [suffix] => 1,7,14
        )
         *
         * Array
        (
        [keyword] => 222
        [type] => all
        [theme] => all
        [dlength] => 3
        [nowprice] => 6
        [isinclude] => 3
        [suffix] => all
        )
         */
       // file_put_contents('1211111111.log',print_r($_POST,true));


        //查询 域名 交易相关信息
        //域名关键词
        $keyword = strtolower(trim($_POST['keyword']));
        if(!empty($keyword)){
            $get_str =  array();
            $get_str = explode('.',$keyword);
            $this->assign('error_no',0);
            if(count($get_str) >= 2){
                $search_condition['domain'] =  $keyword;
                if(empty($get_str[1])){
                    $search_condition['domain'] = array('LIKE','%'.$keyword.'%');
                }
            }else{
                $search_condition['domain'] = array('LIKE','%'.$keyword.'%');
            }
            $top_domains = $get_str[0];


        }
//        //域名关键词 是否包含
//        if($_POST['isinclude'] != ''){
//            switch(intval($_POST['isinclude'])){
//                case '1':
//                    $main_domain_two = array('LIKE','%'.$_POST['isinclude'].'%');
//                    break;
//                case '2':
//                    $main_domain_two = array('LIKE',$_POST['isinclude'].'%');
//                    break;
//                case '3':
//                    $main_domain_two = array('LIKE','%'.$_POST['isinclude']);
//                    break;
//            }
//        }

        //主题
        if($_POST['theme'] !== ''){
            if($_POST['theme'] !='all'){
                $get_theme = explode(',',$_POST['theme']);
                if(!empty($get_theme[1])){
                    $search_condition['theme_id'] = array('in',$_POST['theme']);
                }else{
                    $search_condition['theme_id'] = intval($_POST['theme']);
                }

            }

        }

        //交易类型
        if($_POST['type'] !== ''){
            if($_POST['type'] !='all'){
                $get_type = explode(',',$_POST['type']);
                if(!empty($get_type[1])){
                    $search_condition['type'] = array('in',$_POST['type']);
                }else{
                    $search_condition['type'] = intval($_POST['type']);
                }

            }else{
                $type = 'all';
            }
        }

        //域名长度
        if($_POST['dlength'] !== ''){
            if($_POST['dlength'] !='all'){
                $search_condition['dlength'] = intval($_POST['dlength']);
            }
        }



        //后缀 suffix
        if($_POST['suffix'] !== ''){

            if($_POST['suffix'] !='all'){
                $get_type = explode(',',$_POST['suffix']);
                if(!empty($get_type[1])){
                    $search_condition['suffix'] = array('in',$_POST['suffix']);
                }else{
                    $search_condition['suffix'] = intval($_POST['suffix']);
                }

            }

        }

        //判断域名价格
        if(!empty($_POST['nowprice']) &&  $_POST['nowprice'] !== 'all'){
            if(intval($_POST['nowprice']) === 1){
                $search_condition['money'] = array(array('egt',0),array('elt',500));
            }else if(intval($_POST['nowprice']) === 2 ){
                $search_condition['money'] = array(array('egt',500),array('elt',1000));
            }else  if(intval($_POST['nowprice']) === 3 ){
                $search_condition['money'] = array(array('egt',1000),array('elt',20000));
            }else  if(intval($_POST['nowprice']) === 4 ){
                $search_condition['money'] = array(array('egt',20000),array('elt',100000));
            }else  if(intval($_POST['nowprice']) === 5 ){
                $search_condition['money'] = array(array('egt',100000),array('elt',200000));
            }else  if(intval($_POST['nowprice']) === 6 ){
                $search_condition['money'] = array(array('egt',200000),array('elt',500000));
            }else  if(intval($_POST['nowprice']) === 7 ){
                $search_condition['money'] =  array('egt',500000);
            }
        }

        $Db_domains = M('Domains');
        $search_condition['status'] = '1';
       // $allCount = $Db_domains->where($search_condition)->count();
//        $tradelist_count = $Db_domains->field("`type` ,count(`type`) as counts")->where($search_condition)
//            ->group('`type`')->order('`type` DESC')
//            ->select();
        //dump($search_condition);
        $tradelist = $Db_domains->where($search_condition)
            ->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')
            ->limit(20)
            ->select();
        foreach($tradelist as &$v){
            $v['money'] = number_format($v['money']);
        }
       // dump($tradelist);
        //  //交易类型 type: 0议价 yj，1一口价 ykj，2竞价jj，3优质yz
//        $tradelistnews = array();
//        foreach($tradelist as $k=>$v){
//           switch($v['type']){
//               case 1:
//                   $tradelistnews['ykj'][] = $v;
//                 //  break;
//                    continue;
//               case 2:
//                   $tradelistnews['jj'][] = $v;
//                   continue;
//               case 3:
//                   $tradelistnews['yz'][] = $v;
//                   continue;
//               case 0:
//                   $tradelistnews['yj'][]   =$v;
//                   continue;
//           }
//
//        }
//         exit(json_encode(array('list'=>$tradelistnews,'conts'=>$tradelist_count)));

          if(!empty($tradelist)){
              exit(json_encode($tradelist));
          }else{
              exit(json_encode(array('error'=>1)));
          }


//        dump($tradelistnews);
//        $this->assign('tradelistnews',$tradelistnews);
//        $this->display();
//        if(count($tradelist_count) <= 0){
//            $this->assign('error_no',1);
//            $this->assign('erro_msg','咦？还没有此域名相关的交易记录哦。<br\/> 我们看看别的吧。');
//        }

//`type` tinyint(1) NOT NULL COMMENT '0议价，1一口价，2竞价，3优质',
        //2 竞价
//        $domains_condition_ja['type'] = 2;
//        $domains_condition_ja['status'] = 1;
//
//        $domains_ja_list = $Db_domains->field(true)->where($domains_condition_ja)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')
//            ->limit(4)->select();
//
//        foreach($domains_ja_list as &$val){
//            $val['over_time'] = getOverTime($val['endTime']);
//        }
//
//
//        $this->assign('domains_ja_list', $domains_ja_list);
//
//        if($type == 'all'){
//            //1 一口价域名
//            $search_condition['type'] = 1;
//            $domains_ykj_list = $Db_domains->field(true)->where($search_condition)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit
//            (4)->select();
//            $this->assign('domains_ykj_list', $domains_ykj_list);
//
//            //0 -议价域名
//            $search_condition['type'] = 0;
//            $domains_yj_list = $Db_domains->field(true)->where($search_condition)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit
//            (4)->select();
//            $this->assign('domains_yj_list', $domains_yj_list);
//
//            // 3 优质
//            $search_condition['type'] = 3;
//            $domains_yz_list = $Db_domains->field(true)->where($search_condition)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit
//            (4)->select();
//            $this->assign('domains_yz_list', $domains_yz_list);
//
//            ///，2竞价
//            $search_condition['type'] = 2;
//            $domains_jingjia_list = $Db_domains->field(true)->where($search_condition)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit
//            (4)->select();
//            $this->assign('domains_jingjia_list', $domains_jingjia_list);
//
//        }
//        //1 一口价域名
//        $domains_condition_ykj['type'] = 1;
//        $domains_condition_ykj['status'] = 1;
//        $domains_ykj_list = $Db_domains->field(true)->where($domains_condition_ykj)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit
//        (4)->select();
//        $this->assign('domains_ykj_list', $domains_ykj_list);
//
//        //0 -议价域名
//        $domains_condition_yj['type'] = 0;
//        $domains_condition_yj['status'] = 1;
//        $domains_yj_list = $Db_domains->field(true)->where($domains_condition_yj)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit
//        (4)->select();
//        $this->assign('domains_yj_list', $domains_yj_list);
//
//        // 3 优质
//        $domains_condition_yz['type'] = 3;
//        $domains_condition_yz['status'] = 1;
//        $domains_yz_list = $Db_domains->field(true)->where($domains_condition_yz)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit
//        (4)->select();
//        $this->assign('domains_yz_list', $domains_yz_list);
//
//
//        $this->assign('tradelist_count',$tradelist_count);
        //echo json_encode(array(1));
    }
}