<?php

/**
 * Class ProblemAction
 * 问题反馈
 */
class ProblemAction extends BaseAction{

	public function index(){
		if($this->isAjax()) {
		//	dump($_POST);die;
			$_POST['title'] = strval($_POST['title']);
			$_POST['content'] = strval($_POST['content']);
			$_POST['addtime'] = time();
			$_POST['uid'] = $this->user_session['uid'];
			if(empty($_POST['title'])) exit(json_encode(array('error'=>1,'msg'=>'标题不能为空！')));
			if(empty($_POST['content'])) exit(json_encode(array('error'=>1,'msg'=>'问题详细不能为空！')));
			if(M('Problem')->data($_POST)->add()){
				exit(json_encode(array('error'=>0,'msg'=>'问题已经提交成功，我们会尽快恢复您的！')));
			}else{
				exit(json_encode(array('error'=>1,'msg'=>'提交问题失败，请重试！')));
			}
		}
		$this->display();
	}

//	Public function add_problem(){
//
//		if($this->isAjax()) {
//			$_POST['title'] = strval($_POST['title']);
//			$_POST['content'] = strval($_POST['content']);
//			$_POST['addtime'] = time();
//			$_POST['uid'] = $this->user_session['uid'];
//			if(empty($_POST['title'])) exit(json_encode(array('error'=>1,'msg'=>'标题不能为空！')));
//			if(empty($_POST['content'])) exit(json_encode(array('error'=>1,'msg'=>'问题详细不能为空！')));
//			if(M('Problem')->data($_POST)->add()){
//				exit(json_encode(array('error'=>0,'msg'=>'问题已经提交成功，我们会尽快恢复您的！')));
//			}else{
//				exit(json_encode(array('error'=>1,'msg'=>'提交问题失败，请重试！')));
//			}
//		}
//	}

	public function plist(){
		$problem = M('Problem');
		$uid = $this->user_session['uid'];
		$count_problem = $problem->where(array('uid'=>$uid))->count();
		import('@.ORG.system_page');
		$p = new Page($count_problem, 10);
		$problem_list = $problem->field(true)->where(array('uid'=>$uid))->order('`id` DESC')->limit($p->firstRow . ',' . $p->listRows)->select();
		$pagebar = $p->show();
		$this->assign('pagebar', $pagebar);
		$this->assign('problem_list',$problem_list);

		$this->display();
	}

	public function detail(){
		if($this->isAjax()) {
			$id = intval($_POST['id']) ? intval($_POST['id']) :0 ;
			if($id > 0){
				$problem = M('Problem');
				$pbshow = $problem->where('`id` = '.$id)->find();
				if($pbshow){
					$pbshow['addtime'] = date('Y-m-d H:i:s',$pbshow['addtime']);
					$pbshow['replytime'] = date('Y-m-d H:i:s',$pbshow['replytime']);
					exit(json_encode(array('error'=>0,'resultdata'=>$pbshow)));

				}else{
					exit(json_encode(array('error'=>1,'msg'=>'没有查到该记录信息！')));
				}

			}else{
				exit(json_encode(array('error'=>1,'msg'=>'查看详情失败，请重试！')));
			}


		}

	}

}