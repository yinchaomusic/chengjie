<?php

/**
 *
 *
 */
class HomeAction extends BaseAction{

	public function index(){

        //获取最新一条
        //	新手指南 news_category  =>		user_guide
        $Db_news_category = M('News_category');
        //$Db_news = M('News');
        $news_cat_condition['cat_key'] = 'user_guide';
        $news_cat_condition['cat_state'] = 1;
        $new_cat = $Db_news_category->where($news_cat_condition)->find();
        $this->assign('new_cat',$new_cat);

        //站内公告	帮助中心里的网站公告	notice
        $news_cat_condition['cat_key'] = 'notice';
        $news_cat_condition['cat_state'] = 1;
        $cat_nocice = $Db_news_category->where($news_cat_condition)->find();
        $this->assign('cat_nocice',$cat_nocice);
        //获取最新一条展示
        $Db_news = M('News');
        $news_notice_con['cat_id'] = $cat_nocice['cat_id'];
        $news_notice_con['state'] = 1;
        //$news_notice_con['state'] = 1;

        $last_news = $Db_news->field('`news_id`,`news_title`,`add_time`')->where($news_notice_con)->order('`news_id` DESC')->find();
        $this->assign('last_news',$last_news);

        //域名资讯		domains_info
        $news_cat_condition['cat_key'] = 'domains_info';
        $news_cat_condition['cat_state'] = 1;
        $cat_info = $Db_news_category->where($news_cat_condition)->find();
        $this->assign('cat_info',$cat_info);
        $news_info_con['cat_id'] = $cat_info['cat_id'];
        $news_info_con['state'] = 1;
        $last_info = $Db_news->field('`news_id`,`news_title`,`add_time`')->where($news_info_con)->order('`news_id` DESC')->find();
        $this->assign('last_info',$last_info);

        $this->display();
	}
        public function news_list (){
            $databases_news = M('News');
            $news_cat_id = $_GET['cat_id'];
            $news_list = $databases_news->where(array('cat_id'=>  intval($news_cat_id)))->select();
            echo $databases_news->getLastSql();
            $this->assign('news_list',$news_list);
//            dump($news_list);
            $this->display();
            
//            $news_list = json_encode($news_list);
//            exit ($news_list);
        }
        public function news_show(){
            $databases_news = M('News');
            $news_id = $_GET['news_id'];
            $news_show = $databases_news->where(array('news_id'=>  intval($news_id)))->find();
            $this->assign('news_show',$news_show);
            $this->display();
        }
        
        /*
         * 发现
         */
        public function found(){
            $this->display();
        }
        
        
        /*
         * 批量交易 域名
         */
        
        public function pldomain(){
            $this->display();
        }
        //xiangqing
        public function selling(){
            $bulk_id = intval($_GET['bulk_id']);
            $bulk_trading = M('Bulk_trading');
            $where = '`bulk_id` = '.$bulk_id;
            $domain_list = $bulk_trading->where($where)->find();
            $domain_list['domain_list'] = unserialize($domain_list['domain_list']);
            $domain_list['unit_price'] = $domain_list['total_price']/$domain_list['amount'];
            $domain_list['margin_boon'] = intval($this->user_level_info['boon']/100*$domain_list['total_price']);
            $this->assign('domain_list',$domain_list);
            $this->display();
	}
        
        /*
         * 域名中介
         */
        public function escrow(){
            $this->display();
        }
        
        //        添加中介交易
        public function new_deal(){
            $order = M('Order');
            if($_POST){
                if($_POST['dprice'] == 'buy'){
                    $data['sell_id'] = $_POST['user'];
                    $data['buy_id'] = $this->user_session['uid'];
                    $data['yes_no'] = 2;
                }  else {
                    $data['buy_id'] = $_POST['user'];
                    $data['sell_id'] = $this->user_session['uid'];
                    $data['yes_no'] = 1;
                }
                if($_POST['roledprice'] == 'one'){
                    $data['payTye'] = 1;
                }  elseif ($_POST['roledprice'] == 'two') {
                    $data['payTye'] = 2;
                }  elseif ($_POST['roledprice'] == 'three') {
                    $data['payTye'] = 3;
                }
                $data['total_price'] = $_POST['money'];
                $data['trade_type'] = 1;
                $data['domainName'] = $_POST['domainName'];
                $data['staff_id'] = $this->user_session['staff_uid'];
                $data['addTime'] = time();
                if($data['staff_id']){
                    $data['status'] = 1;
                }
                $row = $order->add($data);
                if($row){
                    $this->success('添加成功',U('Escrow/index'));
                }  else {
                    $this->error('添加失败');
                }
            }
        }
        
        /*
         * 我的米表
         */
        public function mblist(){

            $uid = intval($_GET['mmid']);
            $typeid = intval($_GET['typeid']) ? intval($_GET['typeid']) : 0;
            $this->assign('error_no',0);
            $this->assign('typeid',$typeid);
            if($uid < 1){
                redirect(U('Home/index'));
            }

            //域名后缀
            $databaseDomainSuffix = D('Domain_suffix');
            $conditionDomainSuffix['is_hots'] = '1';
            $suffixList = $databaseDomainSuffix->field('`id`,`suffix`')->where($conditionDomainSuffix)->order('`sorts` DESC,`id` ASC')->select();
            $this->assign('suffixList',$suffixList);

            //主题属性
            $databaseDomainTreasure = D('Domain_treasure');
            $conditionDomainTreasure['status'] = '1';
            $treasureList = $databaseDomainTreasure->field(true)->where($conditionDomainTreasure)->order('`sorts` DESC,`id` ASC')->select();
            $this->assign('treasureList',$treasureList);

            $Db_domains = D("Domains");
            $userinfo = D('Mibiao')->where(array('uid'=>$uid))->find();
            if(empty($userinfo)){
                $this->assign('jumpUrl',$_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : U('Home/index'));
                $this->error('该用户没有开通米表功能');
            }
            $domains_condition['status'] = '1';
            $domains_condition['uid'] = $uid;
            $count_domains = $Db_domains->where($domains_condition)->count();
            // dump($Db_domains);
            import('@.ORG.domain_page');
            $p = new Page($count_domains, 30);
            if($typeid == 1){
                $orderby = '`money` DESC';
            }else if($typeid ==2){
                $orderby = '`length` ASC';
            }else{
               $orderby = '`add_time` DESC';
            }
            $domains_list = $Db_domains->field(true)->where($domains_condition)->order($orderby)->limit($p->firstRow . ',' . $p->listRows)->select();

            $page = $p->show();
            $this->assign('page',$page);
            $this->assign("count_domains",$count_domains);
            $this->assign('domains_list', $domains_list);
            $this->assign('userinfo', $userinfo);

            $this->display();
        }
        
        /*
         * 终端域名
         */
        public function terminal(){
            $this->display();
        }
        
        


}