<?php

/**
 * Class BuyerAction
 * 我是买家
 */

class BuyerAction extends  BaseAction{
        protected function _initialize(){
		parent::_initialize();
		if(empty($this->user_session)){
			redirect(U('Login/login'));
		}
	}
	public function index(){
            $databases_purchase = M('Purchase');
            $purchase_count = $databases_purchase->where(array('uid'=>  $this->user_session['uid']))->count();
            $this->assign('purchase_count',$purchase_count);
            $this->display();
	}

	



	/**
	 *参与的视频竞价
	 */
	public function  videoauction(){
            $uid = $this->user_session['uid'];
            if($_GET['status'] == 1){
                $where = ' vd.`lastFidId` =  vdl.`pigcms_id`  AND vdl.`uid` = '.$uid .' AND vd.`domain_status` = 2';
                $order = 'vd.`domain_id` DESC';
                $this->assign('status_type',1);
            } else {
                $where = ' vd.`lastFidId` =  vdl.`pigcms_id`  AND vdl.`uid` = '.$uid;
                $order = '`time` DESC';
                $this->assign('status_type',0);
            }
            $count_video =  M()->table(array(C('DB_PREFIX').'Videosale_domains_list'=>'vdl',C('DB_PREFIX').'Videosale_domains'=>'vd'))->where($where)->count();
            if(!empty($count_video)){
                import('@.ORG.system_page');
                $p = new Page($count_video, 25);
                $field = 'vd.`domains`,vd.`now_money`,vdl.`money`,vdl.`time`';
                $videosale =  M()->table(array(C('DB_PREFIX').'Videosale_domains_list'=>'vdl',C('DB_PREFIX').'Videosale_domains'=>'vd'))->field($field)->where($where)->order($order)->limit($p->firstRow . ',' . $p->listRows)->select();

                $pagebar = $p->show();
                $this->assign('pagebar', $pagebar);

            }
            $this->assign('list', $videosale);
            $this->display();
        }
        
//        Public function videoauction_success(){
//            $uid = $this->user_session['uid'];
//            $where = ' vd.`lastFidId` =  vdl.`pigcms_id`  AND vdl.`uid` = '.$uid .' AND vd.`domain_status` = 2';
//            $counts =  M()->table(array(C('DB_PREFIX').'Videosale_domains_list'=>'vdl',C('DB_PREFIX').'Videosale_domains'=>'vd'))->where($where)->count();
//            import('@.ORG.system_page');
//            $p = new Page($counts, 25);
//            $field = 'vd.`domains`,vd.`now_money`';
//            $list_video =  M()->table(array(C('DB_PREFIX').'Videosale_domains_list'=>'vdl',C('DB_PREFIX').'Videosale_domains'=>'vd'))->field($field)->where($where)->order($order)->limit($p->firstRow . ',' . $p->listRows)->select();
//
//            $pagebar = $p->show();
//            $this->assign('pagebar', $pagebar);
//
//            $this->assign('list', $list_video);
//            $this->display();
//        }

	/**
	 * 代购委托
	 */
	public function purchasing(){
            if($_GET['status'] == 1){
                //代购中
                $where = "`uid` = ".$this->user_session['uid']." AND status = 4 OR status = 5 OR status = 8 OR status = 9";
                $this->assign('status_type',1);
            } elseif ($_GET['status'] == 2) {
                //代购成功
                $where = array('uid'=>$this->user_session['uid'],'status'=>6);
                $this->assign('status_type',2);
            } elseif ($_GET['status'] == 3) {
                //代购失败
                $where = array('uid'=>$this->user_session['uid'],'status'=>7);
                $this->assign('status_type',3);
            } else {
                $where = "`uid` = ".$this->user_session['uid']." AND (status = 0 OR status = 1 OR status = 2 OR status = 3)";
                $this->assign('status_type',0);
            }
            $databases_purchase = M('Purchase');
            $list = $databases_purchase->where($where)->order("ph_id desc")->select();
            $this->assign('list',$list);
            $this->display();
	}
        
        
        
        
        //委托代购保证金
        public function freeze_margin(){
            $databases_purchase = M('Purchase');
            $databases_user = M('User');
            $databases_user_freeze = M('User_freeze');
            $databases_user_money_list = M('User_money_list');
            $user_info = $databases_user->where(array('uid'=>  $this->user_session['uid']))->find();
            $ph_info = $databases_purchase->where(array('ph_id'=>  intval($_POST['ph_id']),'uid'=>  $this->user_session['uid']))->find();
            $ph_info['margin'] = $ph_info['buyers_price']*0.08;
            $data['info'] = "委托代购保证金冻结资金";
            $data['type'] = 3;
            $data['freezetime'] = time();
            $data['uid'] = $this->user_session['uid'];
            $data['freezemoney'] = $ph_info['margin'];
            $user_money_list_data['type'] = 13;
            $user_money_list_data['uid'] = $this->user_session['uid'];
            $user_money_list_data['money'] = $ph_info['margin'];
            $user_money_list_data['desc'] = "委托代购保证金冻结";
            $user_money_list_data['now_money'] = $user_info['now_money']-$ph_info['margin'];
            $user_money_list_data['time'] = time();
            $u_info = $databases_user->where(array('uid'=>$this->user_session['uid']))->setDec('now_money',$ph_info['margin']); // 用户的金额减少
            $databases_user->where(array('uid'=>$this->user_session['uid']))->setInc('freeze_money',$ph_info['margin']); // 用户的冻结金额增加
            if($u_info){
                $f_info = $databases_user_freeze->add($data);
                if($f_info){
                    $p_info = $databases_purchase-> where(array("ph_id"=>$_POST['ph_id']))->setField(array('status'=>4,'freeze_id'=>$f_info));
                    if($p_info){
                        $databases_user_money_list->add($user_money_list_data);
                        exit(json_encode(array('error'=>'0','msg'=>'委托代购保证金冻结成功！')));
                    }  else {
                        exit(json_encode(array('error'=>'1','msg'=>'委托代购保证金冻结失败！')));
                    }
                }  else {
                    exit(json_encode(array('error'=>'2','msg'=>'冻结资金增加失败！')));
                }
            }  else {
                exit(json_encode(array('error'=>'3','msg'=>'用户金额减少失败！')));
            }
        }
        
        //代购付款
        public function payment(){
            $databases_purchase = M('Purchase');
            $databases_user = M('User');
            $databases_user_freeze = M('User_freeze');
            $databases_user_money_list = M('User_money_list');
            $ph_info = $databases_purchase->where(array('ph_id'=>  intval($_POST['ph_id'])))->find();
            $user_info = $databases_user->where(array('uid'=> intval($this->user_session['uid'])))->find();
            $ph_info['margin'] = $ph_info['buyers_price']*0.08;
            $ph_info['poundage'] = $ph_info['buyers_price']*0.08;
            $ph_info['again_pay'] = $ph_info['buyers_price']+$ph_info['poundage'];
            $user_info['now_money'] = $user_info['now_money']+$ph_info['margin'];
            if($user_info['now_money'] < $ph_info['again_pay']){
                exit(json_encode(array('error'=>'1','msg'=>'用户金额不足，支付失败！')));
            }
            $user_money_list_data['type'] = 13;
            $user_money_list_data['uid'] = $this->user_session['uid'];
            $user_money_list_data['money'] = $ph_info['margin'];
            $user_money_list_data['desc'] = "委托代购保证金解冻";
            $user_money_list_data['now_money'] = $user_info['now_money'];
            $user_money_list_data['time'] = time();
            $u_m_l_d = $databases_user_money_list->add($user_money_list_data);

            if($u_m_l_d){
            }  else {
                exit(json_encode(array('error'=>'2','msg'=>'金额信息添加失败，支付失败！')));
            }
            $user_money_list_info['type'] = 4;
            $user_money_list_info['uid'] = $this->user_session['uid'];
            $user_money_list_info['money'] = $ph_info['again_pay'];
            $user_money_list_info['desc'] = "委托代购买家付款记录";
            $user_money_list_info['now_money'] = $user_info['now_money']-$ph_info['again_pay'];
            $user_money_list_info['time'] = time();
            $u_m_l_i = $databases_user_money_list->add($user_money_list_info);
            if($u_m_l_i){
            }  else {
                exit(json_encode(array('error'=>'3','msg'=>'金额信息添加失败，支付失败！')));
            }
            $user_data['uid'] = $this->user_session['uid'];
            $user_data['now_money'] = $user_info['now_money']-$ph_info['again_pay'];
            $user_data['freeze_money'] = $user_info['freeze_money']-$ph_info['margin'];

            $data['id'] = $ph_info['freeze_id'];
            $data['thawtime'] = time();
            $data['status'] = 1;
            $user_row = $databases_user->save($user_data);
            if($user_row){
                $freeze_row = $databases_user_freeze->save($data);
                if($freeze_row){
                    $ph_row = $databases_purchase->where(array('ph_id'=>  intval($_POST['ph_id'])))->setField('status',5);
                    if($ph_row){
                        exit(json_encode(array('error'=>'0','msg'=>'付款完成，支付成功！')));
                    }else{
                        exit(json_encode(array('error'=>'4','msg'=>'支付失败！')));
                    }
                }  else {
                    exit(json_encode(array('error'=>'5','msg'=>'冻结资金变动失败，支付失败！')));
                }
            }  else {
                exit(json_encode(array('error'=>'5','msg'=>'用户金额变动失败，支付失败！')));
            }
        }
        
        //删除代购
        public function purchasing_del(){
            $databases_purchase = M('Purchase');
            $databases_purchase->where(array('ph_id'=>intval($_POST['ph_id']),'uid'=>  $this->user_session['uid']))->delete();
            exit(json_encode(array('error'=>'0','msg'=>'删除成功！')));
        }
        
        public function purchasing_save(){
            $databases_purchase = M('Purchase');
            $ph_id = intval($_GET['ph_id']);
            $show = $databases_purchase->where(array('ph_id'=>$ph_id,'uid'=>  $this->user_session['uid']))->find();
            $this->assign('show',$show);
            $this->display();
        }
        public function purchasing_save_data(){
            $databases_purchase = M('Purchase');
            $_POST['uid'] = $this->user_session['uid'];
            $row = $databases_purchase->where(array('uid'=>$_POST['uid'],'ph_id'=>$_POST['ph_id']))->save($_POST);
            if($row){
                exit(json_encode(array('error'=>'0','msg'=>'修改成功！')));
            }  else {
                exit(json_encode(array('error'=>'1','msg'=>'修改失败，请重试！')));
            }
        }
        //代购域名确认收到域名更改状态
        public function pay_confirm(){
            $databases_purchase = M('Purchase');
            $databases_user = M('User');
            $ph_info = $databases_purchase->where(array('ph_id'=>  intval($_POST['ph_id'])))->find();
            $user_info = $databases_user->where(array('uid'=> intval($this->user_session['uid'])))->find();
            $ph_info['poundage'] = $ph_info['buyers_price']*0.08;
            $ph_info['again_pay'] = $ph_info['buyers_price']+$ph_info['poundage'];
            $score_count = $ph_info['again_pay']*$this->config['meal_score'];

            $databases_user->where(array('uid'=>  $this->user_session['uid']))->setInc('score_count',$score_count);
            $databases_purchase = M('Purchase');
            $info  = $databases_purchase->where(array('ph_id'=>  intval($_POST['ph_id'])))->setField("status",6);
            if($info){
                exit(json_encode(array('error'=>'0','msg'=>'确认收货成功！')));
            }  else {
                exit(json_encode(array('error'=>'1','msg'=>'确认收货失败！')));
            }
        }
        
        public function cancel(){
            $ph_id = $_POST['ph_id'];
            $databases_user_freeze = M('User_freeze');
            $databases_purchase = M('Purchase');
            $databases_user = M('User');
            $purchase_info = $databases_purchase->where(array('uid'=> $this->user_session['uid'],'ph_id'=>$ph_id))->find();
            $user_freeze_info = $databases_user_freeze->where(array('uid'=>  $this->user_session['uid'],'id'=>$purchase_info['freeze_id']))->find();
            $freeze_save = $databases_user_freeze->where(array('id'=>$purchase_info['freeze_id']))->setField('status',2);
            $databases_user->where(array('uid'=>  $this->user_session['uid']))->setDec('freeze_money',$purchase_info['freezemoney']); // 用户的冻结金额
//            echo $databases_user_freeze->getLastSql();
//            dump($freeze_info);die;

//            $user_money_list_data['type'] = 6;
//            $user_money_list_data['uid'] = $this->user_session['uid'];
//            $user_money_list_data['money'] = $ph_info['margin'];
//            $user_money_list_data['desc'] = "委托代购保证金因用户取消变更为违约金";
//            $user_money_list_data['now_money'] = $user_info['now_money'];
//            $user_money_list_data['time'] = time();
//            $u_m_l_d = $databases_user_money_list->add($user_money_list_data);
//
//
            if($freeze_save){
                $data['status'] = 7;
                $data['why'] = "用户取消了委托代购";
                $ph_info = $databases_purchase->where(array('ph_id'=>$ph_id))->save($data);
                if($ph_info){
                    exit(json_encode(array('error'=>'0','msg'=>'取消成功，冻结资金已作为违约金！')));
                }  else {
                    exit(json_encode(array('error'=>'1','msg'=>'取消异常！')));
                }
            }  else {
                 exit(json_encode(array('error'=>'2','msg'=>'取消异常！')));
            }
        }
        
	/**
	 * 参与的竞价
	 */
         public function partakefast(){
             $uid =  $this->user_session['uid'];
             if($_GET['status'] == 1){
                //正在竞价
                $where = 'dolt.uid='.$uid .' AND dolt.domain_id = do.domain_id AND do.status =1 ';
                $this->assign('status_type',1);
            } elseif ($_GET['status'] == 2) {
                //已经标的
                $where = 'dolt.uid='.$uid .' AND dolt.domain_id = do.domain_id AND do.status = 2 AND do.lastFidId = '.$uid;
                $this->assign('status_type',2);
            } elseif ($_GET['status'] == 3) {
                //未标的
                $where = 'dolt.uid='.$uid .' AND dolt.domain_id = do.domain_id AND do.status = 2 AND do.lastFidId != '.$uid;
                $this->assign('status_type',3);
            } else {
                //所有记录
                $where = 'dolt.uid='.$uid .' AND dolt.domain_id = do.domain_id';
                $this->assign('status_type',0);
            }
            $count =  M()->table(array(C('DB_PREFIX').'domains_fastbid_list'=>'dolt',C('DB_PREFIX').'domains'=>'do'))
                            ->where($where)->group('dolt.domain_id')->order('dolt.pigcms_id desc')->select();
            $count = count($count);
            import('@.ORG.system_page');
            $p = new Page($count, 10);
            $list =  M()->table(array(C('DB_PREFIX').'domains_fastbid_list'=>'dolt',C('DB_PREFIX').'domains'=>'do'))
                            ->where($where)->limit($p->firstRow . ',' . $p->listRows)->group('dolt.domain_id')->order('dolt.pigcms_id desc')->select();
//            echo M()->getLastSql(); 
            $this->assign('list',$list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
        }
        
        
        /*我给出的报价*/
	public function negotiation(){
            if($_GET['status'] == 1){
                //正在议价
                $where = array('buy_uid'=>  $this->user_session['uid'],'status'=>0);
                $this->assign('status_type',1);
            } elseif ($_GET['status'] == 2) {
                //议价成功
                $where = array('buy_uid'=>  $this->user_session['uid'],'status'=>1);
                $this->assign('status_type',2);
            } elseif ($_GET['status'] == 3) {
                //议价失败
                $where = array('buy_uid'=>  $this->user_session['uid'],'status'=>2);
                $this->assign('status_type',3);
            } else {
                //所有记录
                $where = array('buy_uid'=>  $this->user_session['uid']);
                $this->assign('status_type',0);
            }
            
            $databases_domains_quote  = D('Domains_quote');
            $domains_quote_list = $databases_domains_quote->field(true)->where($where)->order('`last_time` DESC')->select();
            $this->assign('domains_quote_list',$domains_quote_list);
            $this->display();
	}
        
        public function oneprice(){
            $quote_id = intval($_POST['quote_id']);
            $now_quote = D('Domains_quote')->field(true)->where(array('quote_id'=>$quote_id))->find();
            $money = intval($_POST['money']);
            if($money <= $now_quote['new_money']){
                    $this->error('新的报价不能低于老的报价');
            }
            $dataQuote = array(
                    'new_money'=>$money,
                    'last_time'=>$_SERVER['REQUEST_TIME'],
                    'invalid_time'=>$_SERVER['REQUEST_TIME'] + 604800,
                    'last_offer' => $this->user_session['uid'] == $now_quote['buy_uid'] ? '0' : '1',
            );
            if(D('Domains_quote')->where(array('quote_id'=>$quote_id))->data($dataQuote)->save()){
                    $dataQuoteList['quote_id'] = $quote_id;
                    $dataQuoteList['is_sell'] = $this->user_session['uid'] == $now_quote['buy_uid'] ? '0' : '1';
                    $dataQuoteList['money'] = $money;
                    $dataQuoteList['time'] = $_SERVER['REQUEST_TIME'];
                    D('Domains_quote_list')->data($dataQuoteList)->add();
                    $this->success('报价成功');
            }else{
                    $this->error('报价失败');
            }
         }
         
         
        
        public function negotiation_cancel(){
            $quote_id = intval($_POST['quote_id']);
            $dataQuote = array(
                    'status' => '2',
                    'last_offer' => '1',
                    'last_time'=>$_SERVER['REQUEST_TIME'],
            );
            if(D('Domains_quote')->where(array('quote_id'=>$quote_id))->data($dataQuote)->save()){
                    exit(json_encode(array('error'=>'0','msg'=>'取消成功！')));
            }else{
                    exit(json_encode(array('error'=>'1','msg'=>'取消异常！')));
            }
        }
        
        public function negotiation_accept(){
            $quote_id = intval($_POST['quote_id']);
            $now_quote = D('Domains_quote')->field(true)->where(array('quote_id'=>$quote_id))->find();
            
            if(($this->user_session['uid'] == $now_quote['buy_uid'] && $now_quote['last_offer'] == 1) || ($this->user_session['uid'] == $now_quote['sell_uid'] && $now_quote['last_offer'] == 0)){
                $dataQuote = array(
                        'last_time'=>$_SERVER['REQUEST_TIME'],
                        'last_offer' => $this->user_session['uid'] == $now_quote['buy_uid'] ? '0' : '1',
                        'status' => '1',
                );
                if(D('Domains_quote')->where(array('quote_id'=>$quote_id))->data($dataQuote)->save()){
                        D('Domains')->where(array('domain_id'=>$now_quote['domain_id']))->data(array('status'=>'2'))->save();
                        $dataOrder['addTime'] = $_SERVER['REQUEST_TIME'];
                        $dataOrder['total_price'] = $now_quote['new_money'];
                        $dataOrder['domain_id'] = $now_quote['domain_id'];
                        $dataOrder['domainName'] = $now_quote['domain'];
                        $dataOrder['trade_type'] = '0';
                        $dataOrder['status'] = '0';
                        $dataOrder['buy_id'] = $now_quote['buy_uid'];
                        $dataOrder['sell_id'] = $now_quote['sell_uid'];
                        $dataOrder['staff_id'] = '0';
                        $dataOrder['payTye'] = '2';
                        $dataOrder['yes_no'] = '3';
                        $order_id = D('Order')->data($dataOrder)->add();

                        $dataOrderInfo['order_id'] = $order_id;
                        $dataOrderInfo['info'] = $this->user_session['uid'] == $now_quote['buy_uid'] ? '买家接受了交易，等待买家付款中' : '卖家接受了交易，等待买家付款中';
                        $dataOrderInfo['time'] = $_SERVER['REQUEST_TIME'];
                        D('Order_info')->data($dataOrderInfo)->add();
                        exit(json_encode(array('error'=>'0','msg'=>'操作成功！')));
                }else{
                        exit(json_encode(array('error'=>'1','msg'=>'操作失败！')));
                }
            }                             
        }
        
        
        
        /**
	 * 已经买到域名
	 */
	public function deals_complete(){
            $id = $this->user_session['uid'];
            if($_GET['status'] == 1){
                //正在进行
                $where = '(`buy_id`='.$id.' OR sell_id = '.$id.') AND (`status` = 0 OR `status` = 1)';
                $this->assign('status_type',1);
            } elseif ($_GET['status'] == 2) {
                //成功
                $where = '(`buy_id`='.$id.' OR sell_id = '.$id.' )  AND `status` = 2 ';
                $this->assign('status_type',2);
            } elseif ($_GET['status'] == 3) {
                //失败
                $where = '(`buy_id`='.$id.' OR sell_id = '.$id.' )  AND `status` = 3 ';
                $this->assign('status_type',3);
            } else {
                //所有记录
                $where = '(`buy_id`='.$id.' OR sell_id = '.$id.')';
                $this->assign('status_type',0);
            }
            $databases_order = M('Order');
            $count_order = $databases_order->where($where)->count();
            import('@.ORG.system_page');
            $p = new Page($count_order, 18);
            $order_list = $databases_order->where($where)->order('`order_id` DESC')->select();
//            echo $databases_order->getLastSql();
            
            foreach($order_list as $k=>$v){
                if($v['buy_id'] == $id){
                    $order_list[$k]['behavior'] = "1";
                    if($v['payTye'] == 1){
                        $order_list[$k]['my_payTye'] = "1";//为1 是我自己支付  为2是各付50
                    }  elseif ($v['payTye'] == 3) {
                        $order_list[$k]['my_payTye'] = "2";
                    }
                }  elseif ($v['sell_id'] == $id) {
                    $order_list[$k]['behavior'] = "2";
                    if($v['payTye'] == 2){
                        $order_list[$k]['my_payTye'] = "1";//为1 是我自己支付  为2是各付50
                    }  elseif ($v['payTye'] == 3) {
                        $order_list[$k]['my_payTye'] = "2";
                    }
                }
            }
//            dump($order_list);
            $this->assign("order_list",$order_list);
            $pagebar = $p->show();
            $this->assign('pagebar', $pagebar);
            $this->display();
	}
        
        
        public function dealdetails(){
            $id = $this->user_session['uid'];
            $deal_id = intval($_GET['id']);
            $databases_order = M('Order');
            $databases_order_info = M('Order_info');
            $order_info = $databases_order->where(array('order_id'=>$deal_id))->find();
            $order_info_list = $databases_order_info->where(array('order_id'=>$deal_id))->order('id DESC')->select();
            if($order_info['buy_id'] == $id){
                    $order_info['behavior'] = "1";
                    if($order_info['payTye'] == 1){
                        $order_info['my_payTye'] = "1";//为1 是我自己支付  为2是各付50
                    }  elseif ($order_info['payTye'] == 3) {
                        $order_info['my_payTye'] = "2";
                    }
                }  elseif ($order_info['sell_id'] == $id) {
                    $order_info['behavior'] = "2";
                    if($order_info['payTye'] == 2){
                        $order_info['my_payTye'] = "1";//为1 是我自己支付  为2是各付50
                    }  elseif ($order_info['payTye'] == 3) {
                        $order_info['my_payTye'] = "2";
                    }
                }
            $this->assign('order_info',$order_info);
            $this->assign('order_info_list',$order_info_list);
            $this->display();
        }

         //订单支付
        public function order_pay(){
            $databases_order = M('Order');
            $databases_order_info = M('Order_info');
            $databases_user = M('User');
            $databases_user_freeze = M('User_freeze');
            $databases_user_money_list = M('User_money_list');
            $order_info = $databases_order->where(array('order_id'=>  intval($_POST['order_id'])))->find();
            $freeze_info = $databases_user_freeze->where(array('id'=>$order_info['freeze_id']))->find();
            $user_info = $databases_user->where(array('uid'=> intval($this->user_session['uid'])))->find();
            $user_level_info = $this->user_Level($user_info['score_count']);
            $boon = $user_level_info['boon']/100;
            $poundage = $order_info['total_price']*$boon;
            if($user_info['now_money']+$freeze_info['freezemoney'] < $order_info['total_price']+$poundage){
                $this->error('您的资金不足');die;
            }
            $user_freeze_data['thawtime'] = time();
            $user_freeze_data['status'] = 1;
            $order_freeze_info = $databases_user_freeze->where(array('id'=>$order_info['freeze_id']))->save($user_freeze_data);
            if($order_freeze_info){
                $user_money_list_data['type'] = 3;
                $user_money_list_data['uid'] = $this->user_session['uid'];
                $user_money_list_data['desc'] = "冻结资金释放";
                $user_money_list_data['money'] = $freeze_info['freezemoney'];
                $user_money_list_data['now_money'] = $freeze_info['freezemoney']+$user_info['now_money'];
                $user_money_list_data['time'] = time();
                $databases_user_money_list->add($user_money_list_data);
                $now_money_total = $freeze_info['freezemoney']+$user_info['now_money'];
                $minus_total = $order_info['total_price']+$poundage;
                $now_money = $now_money_total-$minus_total;
                $user_save_info = $databases_user->where(array('uid'=>  $this->user_session['uid']))->setField('now_money',$now_money);
                if($user_save_info){
                    $user_money_list_data_two['type'] = 4;
                    $user_money_list_data_two['uid'] = $this->user_session['uid'];
                    $user_money_list_data_two['money'] = $minus_total;
                    $user_money_list_data_two['now_money'] = $now_money;
                    $user_money_list_data_two['desc'] = "购买支付所用";
                    $user_money_list_data_two['time'] = time();
                    $databases_user_money_list->add($user_money_list_data_two);
                    $order_data['yes_no'] = 4;
                    if($_POST['transfer_type'] == 2){
                        $order_data['registrar'] = $_POST['registrar'];
                        $order_data['registrar_id'] = $_POST['registrar_id'];
                    }
                    $order_data['transfer_type'] = $_POST['transfer_type'];
                    $order_data['order_id'] = $_POST['order_id'];
                    $order_save_info = $databases_order->save($order_data);
                    if($order_save_info){
                        $order_info_data['order_id'] = intval($_POST['order_id']);
                        $order_info_data['info'] = "买家已付款，等待卖家提供域名";
                        $order_info_data['time'] = time();
                        $databases_order_info->add($order_info_data);
                        $this->success('支付成功');
                    }  else {
                        $this->error('支付异常1');die;
                    }
                }  else {
                    $this->error('支付异常2');die;
                }
            }  else {
                $this->error('支付异常3');die;
            }
        }
        
        
        //        确认收货
        public function successful (){
            $databases_order = M('Order');
            $databases_order_info = M('Order_info');
            $databases_user = M('User');
            $order_info = $databases_order->where(array('order_id'=>$_GET['order_id']))->find();
            $order_data['order_id'] = intval($_GET['order_id']);
            $order_data['yes_no'] = 10;
            $order_data['status'] = 2;
            $order_data['up_time'] = time();
            $score_count = $order_info['total_price']*$this->config['user_score_get'];
            $order_back = $databases_order->save($order_data);
            if($order_back){
                $order_info_data['order_id'] = intval($_GET['order_id']);
                $order_info_data['info'] = "买家确认收货，交易达成";
                $order_info_data['time'] = time();
                $databases_order_info->add($order_info_data);
                if($order_info['buy_id']){
                    $databases_user->where(array('uid'=>$order_info['buy_id']))->setInc('score_count',$score_count);
                }
                if ($order_info['sell_id']) {
                    $databases_user->where(array('uid'=>$order_info['sell_id']))->setInc('score_count',$score_count);
                }
                if($order_info['trade_type'] == 0){
                    $databases_domains = M('Domains');
                    $databases_domains->where(array('domain_id'=>$order_info['domain_id']))->setField('status',3);
                }  elseif ($order_info['trade_type'] == 2) {
                    $databases_bulk_trading = M('Bulk_trading');
                    $databases_bulk_trading->where(array('bulk_id'=>$order_info['domain_id']))->setField('status',3);
                }  elseif ($order_info['trade_type'] == 3) {

                }
                exit(json_encode(array('error'=>'0','msg'=>'交易达成！')));
            }  else {
                exit(json_encode(array('error'=>'1','msg'=>'交易异常！')));
            }

        }
        
        
        public function change_dealdetails(){
            if(IS_POST){
                $Db_order = M('Order');
                $_POST['order_id'] =  intval($_POST['order_id']);
                $_POST['yes_no'] =  9;
                $_POST['registrar'] =  strval($_POST['registrar']);
                $_POST['registrar_id'] =  strval($_POST['registrar_id']);
                if($Db_order->data($_POST)->save()){
                    $Db_order_info = M('Order_info');
                    $info['order_id'] =$_POST['order_id'];
                    $info['info'] =  '卖家已提交域名等待买家确认';
                    $info['time'] = time();
                    $Db_order_info->data($info)->add();
                    $this->success('域名提交成功');
                }else{
                    $this->error('操作失败，请重新操作。');
                }
            }
        }
    
    
        
        
}