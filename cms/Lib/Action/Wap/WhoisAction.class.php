<?php
/*
 * whois 查询
 *
 *
 */
class WhoisAction extends BaseAction{
	public function whois($call = '',$keywords= ''){
		//dump($_GET);die;
		$keyword = strtolower(trim($_GET['keyword']));
		if(IS_POST){
			$keyword = strtolower(trim($_POST['keyword']));
		}
//		if(empty($keyword)){
//			$keyword = $keywords;
//		}
		$get_str =  array();
		$get_str = explode('.',$keyword);
		$this->assign('error_no',0);
		if(count($get_str) < 0){
			$this->assign('error_no',1);
			$this->assign('redirect_url',U('Home/index'));
			$this->assign('erro_msg','咦？你从哪里来的啊？这里不是地球啊。<br\/> 亲，我们还是回地球吧。');
		}
		$top_domains = $get_str[0];
		if(empty($get_str[1])){
			$keyword = $keyword .'.com';
		}
		$this->assign('keyword',$keyword);

		if($this->topdomain == 'zhu.cn' || $this->topdomain == 'dev.com'){
			//Self
			$Db_cache_search_domains = D('Cache_search_domains');
			if(isset($_GET['type']) && trim($_GET['type']) =='now'){
				$check_whoisResult = $Db_cache_search_domains->field('`cache_id`,`domainName`')->where("`domainName`='{$keyword}'")->find();
				//更新数据
				if(!empty($check_whoisResult)){
					import('@.ORG.NewWhois');
					$newInfo = new NewWhois();
					$whoisResult = $newInfo->getNewWhois($keyword);
					if(!empty($whoisResult['domainName'])){
						$whoisResult['update_time'] = time();
						$whoisResult['cache_id'] = $check_whoisResult['cache_id'];
						$Db_cache_search_domains->data($whoisResult)->save();
						$this->redirect('Whois/index',array('keyword'=>$whoisResult['domainName']));
					}else{
						import('@.ORG.whois');
						$whoisClass = new whois();
						$whoisResult = $whoisClass->getWhois($keyword);
						if(!empty($whoisResult['domainName'])){
							$whoisResult['update_time'] = time();
							$whoisResult['cache_id'] = $check_whoisResult['cache_id'];
							$Db_cache_search_domains->data($whoisResult)->save();
							$this->redirect('Whois/index',array('keyword'=>$whoisResult['domainName']));
						}
					}
				}
			}
			$whoisResult = $Db_cache_search_domains->field(true)->where("`domainName`='{$keyword}'")->find();
			if(!empty($whoisResult)){
				$this->assign('whoisResult',$whoisResult);
			}else{

				import('@.ORG.NewWhois');
				$newInfo = new NewWhois();
				$whoisResult = $newInfo->getNewWhois($keyword);
				if(!empty($whoisResult['domainName'])){
					$whoisResult['update_time'] = time();
					$Db_cache_search_domains->data($whoisResult)->add();
				}else{
					import('@.ORG.whois');
					$whoisClass = new whois();
					$whoisResult = $whoisClass->getWhois($keyword);
					if(!empty($whoisResult['domainName'])){
						$whoisResult['update_time'] = time();
						$Db_cache_search_domains->data($whoisResult)->add();
					}
					$whoisResult['domainName'] = $keyword;
					$whoisResult['update_time'] = time();
				}
			}



		}else{
			//Other
			if(trim($_GET['type']) == 'now' && isset($_GET['type'])) $type = '&type=now';
			$WhoisAPI = 'http://demo.zhu.cn/index.php?c=Whois&a=whoisinfo&keyword='.$keyword.$type;
			//$WhoisAPI = 'http://www.dev.com/index.php?c=Whois&a=whoisinfo&keyword='.$keyword.$type;
			import('ORG.Net.Http');
			$http = new Http();
			$whoisResult = Http::curlGet($WhoisAPI);
			$whoisResult =json_decode($whoisResult,true);
		}


		$this->assign('whoisResult',$whoisResult);

		if($call =='True'){}else{
			$this->display();
		}


	}

	//api
	public function whoisinfo($keyword = ''){

	}


	public function register(){
		//R('[项目名://][分组名/]模块名/操作名',array('参数1','参数2'…))
		//R('Admin/User/info') //表示调用Admin分组的User模块的info操作方法
		//R('Admin://Tool/User/info') //调用 Admin 项目 Tool 分组的 User 模块的 info 方法
		/*
		 * @param1 必须是 xx.com 或者是 xx.com,xx.cn,xx.net,xxx.com.cn 的格式
		 * @param2 如果是wap端过来的get请求，必须加上 'wap'=>'True'
		 */

		$keyword = strtolower(trim($_GET['keyword']));
		$get_str =  array();
		$get_str = explode('.',$keyword);
		if(count($get_str) < 0){
			$this->redirect('Wap/Home/index','',0,'您进来的方式不对。');exit;
		}
		$no_explode = false;
		$top_domans = $get_str[0];
		if(empty($get_str[1])){
			$keyword_now = $keyword .'.com';
			$keyword_now .= ','.$keyword .'.cn';
			$keyword_now .= ','.$keyword .'.net';
			$keyword_now .= ','.$keyword .'.com.cn';
			$keyword = $keyword_now;
			//$this->assign('keyword',$keyword);
		}else{
			//one
			$no_explode = true;
		}
		/***
		 *
		 *
		 * 万网提供了域名查询接口，接口采用HTTP协议：
		接口URL：http://panda.www.net.cn/cgi-bin/check.cgi
		接口参数：area_domain，接口参数值为标准域名，例：52bong.com
		调用举例：
		http://panda.www.net.cn/cgi-bin/check.cgi?area_domain=52bong.com
		 * 返回结果说明：

		200 返回码，200表示返回成功
		52bong.com  表示当前查询的域名
		211 : Domain exists 返回结果的原始信息，主要有以下几种
		 *
		original=210 : Domain name is available     表示域名可以注册
		original=211 : Domain exists                表示域名已经注册
		original=212 : Domain name is invalid       表示查询的域名无效
		original=213 : Time out 查询超时
		 *
		 *
		 *
		 */

		//域名是否注册
		$server_url = 'http://panda.www.net.cn/cgi-bin/check.cgi?area_domain=';

		$put_url =$server_url . $keyword;

		import('ORG.Net.Http');
		$http = new Http();
		$get_str = Http::curlGet($put_url);
		$oneresult = 0;
		$is_reg = 0;
		if($no_explode){
			$lstr_explode = simplexml_load_string($get_str);
			$lstr_explode =json_encode($lstr_explode);
			$lstr_explode =json_decode($lstr_explode,true);
			$status = explode(':',$lstr_explode['original']);

			$oneresult = 1;
			switch(intval($status[0])){
				case 210:
					$is_reg = 1;
					break;
					//array_push($lstr_explode,$is_reg);
				case 211:
					$is_reg = 0;
					break;
					//array_push($lstr_explode,$is_reg);
			}

			$this->assign('is_reg',$is_reg);

		}else{
			$get_info = str_replace('")','',ltrim($get_str,'("'));
			$str_explode = explode('#',$get_info);
			$count = count($str_explode);
			$lstr_explode = array();
			for($i = 0,$j =0; $i< $count ;$i++,$j++){
				$lstr_explode[$i] = explode('|',$str_explode[$i]);

//
//					switch($lstr_explode[$i]){
//						case 210 :
//							$lstr_explode[$i]['is_reg']  = 1;
//						case 211 :
//							$lstr_explode[$i]['is_reg']  = 0;
//					}


			}
		}
		$this->assign('oneresult',$oneresult);
		//echo json_encode($lstr_explode);die;
		//var_dump($lstr_explode);

		//猜你喜欢
		$Db_domains = M('Domains');
		$domains_con['status'] = 1;
		$domains_con['domain'] = array('like','%'.$top_domans.'%');
		$likeits = $Db_domains->field('`domain_id`,`type`,`domain`,`money`')->where($domains_con)->limit(3)->select();
		$this->assign('likeits',$likeits);
		$this->assign('result',$lstr_explode);
		$this->display();
	}






}