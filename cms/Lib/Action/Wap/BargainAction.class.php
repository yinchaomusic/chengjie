<?php

/**
 * Class BargainAction
 * 优质域名
 */


class BargainAction extends BaseAction{
	public function index(){
            $Db_domains = D("Domains");
//            if($_POST){
//                $order = $_POST['order']." desc";
//            }else{
//                $order = "domain_id desc";
//            }
            $typeid = $_GET['typeid'];
            if($typeid == 1){
                    $orderby = '`money` DESC';
            }elseif($typeid == 2 ){
                    $orderby = '`length` DESC';
            }else{
                    $orderby = '`add_time` DESC';
            }
            $domains_condition_youzhi['type'] = '3';
            $domains_condition_youzhi['status'] = '1';
            $count_domains_youzhi = $Db_domains->where($domains_condition_youzhi)->count();
            import('@.ORG.domain_page');
	    $p = new Page($count_domains_youzhi, 20);
	    $domains_youzhi_list = $Db_domains->field(true)->where($domains_condition_youzhi)->order($orderby)->limit($p->firstRow . ',' . $p->listRows)->select();
//            echo $Db_domains->getLastSql();
            $page = $p->show();
            $this->assign('page',$page);
            $this->assign("count_domains_youzhi",$count_domains_youzhi);
            $this->assign('domains_youzhi_list', $domains_youzhi_list);
            if($_POST){
                foreach ($domains_youzhi_list as $k=>$v){
                $domains_list.= '<li><a><h4>'.$v['domain'].'</h4><p style="color: #75b227">价格：￥'.$v['money'].'元</p><p>1&nbsp;</p></a></li>';
                }
//                echo json_encode($domains_list);die;
//                dump($domains_list);die;
            }
//            dump($domains_youzhi_list);
		$this->display();
	}
        
//        域名收藏
        public function collection(){
            $_POST['uid'] = $this->user_session['uid'];
            if(!$_POST['uid']){
                exit(json_encode(array('error'=>'3','msg'=>'请先登录！')));
            }
            $databases_domain_collection = M('Domain_collection');
            $row = $databases_domain_collection->where(array('uid'=>$_POST['uid'],'domain_id'=>$_POST['domain_id']))->select();
            if($row){
                $info = $databases_domain_collection->where(array('uid'=>$_POST['uid'],'domain_id'=>$_POST['domain_id']))->delete();
                if($info){
                    exit(json_encode(array('error'=>'1','msg'=>'已取消收藏！')));
                }
            }  else {
                $info = $databases_domain_collection->add($_POST);
                if($info){
                    exit(json_encode(array('error'=>'0','msg'=>'收藏成功！')));
                }else{
                    exit(json_encode(array('error'=>'2','msg'=>'失败请重试！')));
                }
            }
            
        }

	public function detail(){
            $domain_id = $_GET['id'];
            $collection_show = $this->is_collection($domain_id);
            $this->assign('collection_show',$collection_show);
            if(empty($domain_id)){
                    redirect(U('Home/index'));
            }
            $databaseDomains = D('Domains');
            $now_domain = $databaseDomains->field(true)->where(array('domain_id'=>$domain_id))->find();
            if(empty($now_domain)){
                $this->assign('error_no',1);
                    $this->assign('erro_msg','当前域名不存在或已被删除。');
            }
            if($now_domain['status'] != 1){
                $this->assign('error_no',1);
                $this->assign('erro_msg','当前域名正在审核中或者已经出售。');
            }
            D('Domains')->where(array('domain_id'=>$domain_id))->setInc('hits');

            //找到域名的经纪人
            $now_domain_user = D('User')->field('`staff_uid`')->where(array('uid'=>$now_domain['uid']))->find();
            if($now_domain_user['staff_uid']){
                    $now_staff = D('Escrow_staff')->field('`name`,`qq`,`phone`')->where(array('id'=>$now_domain_user['staff_uid']))->find();
                    $this->assign('now_staff',$now_staff);
            }
            $now_domain['hits'] += 1;
            $now_domain['money'] = rtrim(rtrim($now_domain['money'],'0'),'.');
            $this->assign('now_domain',$now_domain);

            $this->display();
	}

}