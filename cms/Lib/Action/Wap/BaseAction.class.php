<?php

/***
 * wap 端
 *
 */
class BaseAction extends CommonAction{
        protected $user_level_info;
		public $is_wexin_browser = false;
		public $token = '';
		public $wecha_id = '';
	public function __construct(){
		parent::__construct();
		if(!function_exists('utyjfsldDSAqkfjlfdslkjfldsawapfjdslakfHDFfjlsaf')){
			redirect('http://www.pigcms.com');
		}
            ini_set("session.cookie_domain",$this->config['video_auction_domain']);    
        $user_info = D('User')->field('`score_count`')->where(array('uid'=>  $this->user_session['uid']))->find();
		$this->user_level_info = $this->user_Level($user_info['score_count']);
		$this->assign('user_level_info',$this->user_level_info);
           
		//is weixin brower		
		if(strpos($_SERVER['HTTP_USER_AGENT'],'MicroMessenger') !== false){
			$this->is_wexin_browser = true;
		}
		$this->assign('is_wexin_browser',$this->is_wexin_browser);
	// unset($_SESSION); unset($_SESSION['openid']);echo $_SESSION['openid'];  die;
		if($this->is_wexin_browser && empty($_SESSION['openid'])){
				 
			$this->authorize_openid();
		}
		
		if(GROUP_NAME == 'Wap' && MODULE_NAME == 'Home'){
           $this->assign('home_active',1);
		}
		if(GROUP_NAME == 'Wap' && MODULE_NAME == 'Follow'){
			$this->assign('follow_active',1);
		}
		if(GROUP_NAME == 'Wap' && MODULE_NAME == 'Find'){
			$this->assign('find_active',1);
		}
		if(GROUP_NAME == 'Wap' && (MODULE_NAME == 'Profile' || MODULE_NAME == 'Seller' || MODULE_NAME == 'Buyer')){
			$this->assign('profile_active',1);
		}

		//判断微信浏览器，如果是则获取用户是否已经关注平台公众号
		if($this->config['wechat_follow_txt_url'] && $this->config['wechat_follow_txt_txt'] && !empty($_GET['openid'])){
		
			if($this->config['wechat_follow_show_open']){
				$invote_follow = true;
			}else{
				if($this->user_session['uid']){
					$now_user = D('User')->get_user($this->user_session['uid']);
					if(empty($now_user)){
						$invote_follow = true;
					}
				}else{
					$invote_follow = true;
				}
			}
			
			if($invote_follow){
				$invote_user = D('User')->get_user($_GET['openid'],'openid');
				if($invote_user){
					$invote_nickname = !empty($invote_user['truename']) ? $invote_user['truename'] : $invote_user['nickname'];
					$invote_array = array(
						'url'=>$this->config['wechat_follow_txt_url'],
						'txt'=> str_replace('{nickname}',$invote_nickname,$this->config['wechat_follow_txt_txt']),
						'avatar'=>$invote_user['avatar'],
					);
					$this->assign('invote_array',$invote_array);
				}
			}
		}
		
		
		//获取网站名称和网站顶级域名
		$this->assign('now_site_name',$this->config['site_name']);
		$this->assign('now_site_short_url',self::top_domain($this->config['site_url']));

		$this->static_path   = './tpl/Wap/'.C('DEFAULT_THEME').'/static/';
		$this->static_public = './static/';
		//$this->wap_theme_path   = './tpl/Wap/'.C('DEFAULT_THEME').'/static/ratchet/';
		$this->assign('static_path',$this->static_path);
		$this->assign('static_public',$this->static_public);
		//$this->assign('wap_theme_path',$this->wap_theme_path);

		//判断开关网站
		if($this->config['site_close'] == 2 || $this->config['site_close'] == 3){
			$this->assign('title','网站关闭');
			$this->assign('jumpUrl','-1');
			$this->error_tips($this->config['site_close_reason'] ? $this->config['site_close_reason'] : '网站临时关闭');
		}
			//分享
		if($this->is_wexin_browser || $_SESSION['openid']){
			$share = new WechatShare($this->config,$_SESSION['openid']);
			$this->shareScript = $share->getSgin();
			$this->assign('shareScript', $this->shareScript);
			$this->hideScript = $share->gethideOptionMenu();
			$this->assign('hideScript', $this->hideScript);
		}
		
		//判断分享获得分享关系
		if(!empty($_GET['openid']) && $_SESSION['openid'] && $_GET['openid'] != $_SESSION['openid']){
			$spread_user = D('User')->get_user($_GET['openid'],'openid');
			if(!empty($spread_user)){
				$now_user = D('User')->get_user($_SESSION['openid'],'openid');
				if(empty($now_user)){
					D('User_spread')->where(array('openid'=>$_SESSION['openid']))->delete();
					D('User_spread')->data(array('spread_openid'=>$_GET['openid'],'spread_uid'=>$spread_user['uid'],'openid'=>$_SESSION['openid']))->add();
				}
			}
		}

	}
       
	public function authorize_openid(){

		if(empty($_GET['code']) || empty($_SESSION['weixin']['state'])){
			$_SESSION['weixin']['state']   = md5(uniqid());
			$customeUrl = preg_replace('#&code=(\w+)#','',$this->config['site_url'].$_SERVER['REQUEST_URI']);
			$oauthUrl = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$this->config['wechat_appid'].'&redirect_uri='.urlencode($customeUrl).'&response_type=code&scope=snsapi_base&state='.$_SESSION['weixin']['state'].'#wechat_redirect';
			redirect($oauthUrl);exit;
		}else if(isset($_GET['code']) && isset($_GET['state']) && ($_GET['state'] == $_SESSION['weixin']['state'])){
			unset($_SESSION['weixin']);
			import('ORG.Net.Http');
			$http = new Http();
			$return = $http->curlGet('https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$this->config['wechat_appid'].'&secret='.$this->config['wechat_appsecret'].'&code='.$_GET['code'].'&grant_type=authorization_code');
			$jsonrt = json_decode($return,true);
			if($jsonrt['errcode']){
				$error_msg_class = new GetErrorMsg();
				$this->error_tips('授权发生错误：'.$error_msg_class->wx_error_msg($jsonrt['errcode']),U('Home/index'));
			}

			if($jsonrt['openid']){
				$_SESSION['openid'] = $jsonrt['openid'];
				
				//如果存在即 自动登录
				$result = D('User')->autologin('openid',$jsonrt['openid']);
			
				if(empty($result['error_code'])){
					$now_user = $result['user'];
					session('user',$now_user);
					$this->user_session = session('user');
					if($_SERVER['REQUEST_TIME'] - $now_user['last_weixin_time'] > 259200){	//缓存3天
						$customeUrl = preg_replace('#&code=(\w+)#','',$this->config['site_url'].$_SERVER['REQUEST_URI']);
						redirect(U('Login/weixin',array('referer'=>urlencode($customeUrl))));
					}
				}
			}else{
				redirect(U('Profile/index'));
			}
		}else{
			redirect(U('Profile/index'));
		}
	}

	
	public function is_collection($id){
        $databases_domain_collection = M('Domain_collection');
        $show = $databases_domain_collection->where(array('uid'=>  $this->user_session['uid'],'domain_id'=>$id))->find();
         if($show){
              return 1;
        }  else {
                return 2;
        }
     }
     
     
        public function user_Level($level_count){
            $databases_user_level = M('User_level');
            $user_level_list = $databases_user_level->select();
            foreach ($user_level_list as $k=>$v){
                if($level_count>=$v['integral']){
                    $level_info['lname'] = $v['lname'];
                    $level_info['boon'] = $v['boon'];
                    $level_info['img'] = $v['img'];
                }
            }
            return $level_info;
        }
        
        
     public function regular_domain($domain){
		if (substr ( $domain, 0, 7 ) == 'http://') {
			$domain = substr ( $domain, 7 );
		}
		if (strpos ( $domain, '/' ) !== false) {
			$domain = substr ( $domain, 0, strpos ( $domain, '/' ) );
		}
		return strtolower ( $domain );
	}
        
        public  function top_domain($domain) {
		$domain = self::regular_domain ( $domain );
		$iana_root = array (
			'com',
			'cc',
			'cn',
			'org',
			'ac',
			'ad',
			'ae',
			'aero',
			'af',
			'ag',
			'ai',
			'al',
			'am',
			'an',
			'ao',
			'aq',
			'ar',
			'arpa',
			'as',
			'asia',
			'at',
			'au',
			'aw',
			'ax',
			'az',
			'ba',
			'bb',
			'bd',
			'be',
			'bf',
			'bg',
			'bh',
			'bi',
			'biz',
			'bj',
			'bl',
			'bm',
			'bn',
			'bo',
			'bq',
			'br',
			'bs',
			'bt',
			'bv',
			'bw',
			'by',
			'bz',
			'ca',
			'cat',
			'cd',
			'cf',
			'cg',
			'ch',
			'ci',
			'ck',
			'cl',
			'cm',
			'co',
			'coop',
			'cr',
			'cu',
			'cv',
			'cw',
			'cx',
			'cy',
			'cz',
			'de',
			'dj',
			'dk',
			'dm',
			'do',
			'dz',
			'ec',
			'edu',
			'ee',
			'eg',
			'eh',
			'er',
			'es',
			'et',
			'eu',
			'fi',
			'fj',
			'fk',
			'fm',
			'fo',
			'fr',
			'ga',
			'gb',
			'gd',
			'ge',
			'gf',
			'gg',
			'gh',
			'gi',
			'gl',
			'gm',
			'gn',
			'gov',
			'gp',
			'gq',
			'gr',
			'gs',
			'gt',
			'gu',
			'gw',
			'gy',
			'hk',
			'hm',
			'hn',
			'hr',
			'ht',
			'hu',
			'id',
			'ie',
			'il',
			'im',
			'in',
			'info',
			'int',
			'io',
			'iq',
			'ir',
			'is',
			'it',
			'je',
			'jm',
			'jo',
			'jobs',
			'jp',
			'ke',
			'kg',
			'kh',
			'ki',
			'km',
			'kn',
			'kp',
			'kr',
			'kw',
			'ky',
			'kz',
			'la',
			'lb',
			'lc',
			'li',
			'lk',
			'lr',
			'ls',
			'lt',
			'lu',
			'lv',
			'ly',
			'ma',
			'mc',
			'md',
			'me',
			'mf',
			'mg',
			'mh',
			'mil',
			'mk',
			'ml',
			'mm',
			'mn',
			'mo',
			'mobi',
			'mp',
			'mq',
			'mr',
			'ms',
			'mt',
			'mu',
			'museum',
			'mv',
			'mw',
			'mx',
			'my',
			'mz',
			'na',
			'name',
			'nc',
			'ne',
			'net',
			'nf',
			'ng',
			'ni',
			'nl',
			'no',
			'np',
			'nr',
			'nu',
			'nz',
			'om',

			'pa',
			'pe',
			'pf',
			'pg',
			'ph',
			'pk',
			'pl',
			'pm',
			'pn',
			'pr',
			'pro',
			'ps',
			'pt',
			'pw',
			'py',
			'qa',
			're',
			'ro',
			'rs',
			'ru',
			'rw',
			'sa',
			'sb',
			'sc',
			'sd',
			'se',
			'sg',
			'sh',
			'si',
			'sj',
			'sk',
			'sl',
			'sm',
			'sn',
			'so',
			'sr',
			'ss',
			'st',
			'su',
			'sv',
			'sx',
			'sy',
			'sz',
			'tc',
			'td',
			'tel',
			'tf',
			'tg',
			'th',
			'tj',
			'tk',
			'tl',
			'tm',
			'tn',
			'to',
			'tp',
			'tr',
			'travel',
			'tt',
			'tv',
			'tw',
			'tz',
			'ua',
			'ug',
			'uk',
			'um',
			'us',
			'uy',
			'uz',
			'va',
			'vc',
			've',
			'vg',
			'vi',
			'vn',
			'vu',
			'wf',
			'ws',
			'xxx',
			'ye',
			'yt',
			'za',
			'zm',
			'zw'
		);
		$sub_domain = explode ( '.', $domain );
		$top_domain = '';
		$top_domain_count = 0;
		for($i = count ( $sub_domain ) - 1; $i >= 0; $i --) {
			if ($i == 0) {
				// just in case of something like NAME.COM
				break;
			}
			if (in_array ( $sub_domain [$i], $iana_root )) {
				$top_domain_count ++;
				$top_domain = '.' . $sub_domain [$i] . $top_domain;
				if ($top_domain_count >= 2) {
					break;
				}
			}
		}
		$top_domain = $sub_domain [count ( $sub_domain ) - $top_domain_count - 1] . $top_domain;
		return $top_domain;
	}



}