<?php

/**
 * Class VideoSaleAction
 * WAP端 视频拍卖交易
 */
class VideoSaleAction extends BaseAction {
//    protected $now_term;
//	Public function __construct(){
//		parent::__construct();
////       if(strtolower(MODULE_NAME) == 'videosale'){
////          switch(strtolower(ACTION_NAME)){
////	          case 'index':
////		          $this->assign('video_active',1);
////		          $this->assign('video_title','拍卖出价');
////		          break;
////	          case 'chatroom':
////		          $this->assign('video_active',2);
////		          $this->assign('video_title','聊天室');
////		          break;
////	          case 'watlist':
////		          $this->assign('video_active',3);
////		          $this->assign('video_title','待拍列表');
////		          break;
////	          case 'seeding':
////		          $this->assign('video_active',4);
////		          $this->assign('video_title','视频直播');
////		          break;
////          }
////       }
////		$this->assign('close_error',0);
////		if($this->config['video_auction_close'] == 0){
////			$this->assign('close_error',1);
////			$this->assign('jumptoUrl',U('Home/index'));
////			$this->assign('close_reason',$this->config['video_auction_close_reason']);
////		}
////		$now_term = D('Videosale_term')->field(true)->where(array('is_open'=>'1'))->order('`term_id` DESC')->find();
////		$this->assign('now_term',$now_term);
////		if(empty($now_term)){
////			$this->assign('jumptoUrl',U('Home/index'));
////			$this->assign('close_error',1);
////			$this->assign('close_reason','视频拍卖活动已结束');
////		}
////		$this->now_term = $now_term;
//
//
//	}

	public function index(){
		$this->assign('close_error',0);
		if($this->config['video_auction_close'] == 0){
			$this->assign('close_error',1);
			$this->assign('jumptoUrl',U('Home/index'));
			$this->assign('close_reason',$this->config['video_auction_close_reason']);
		}
		$now_term = D('Videosale_term')->field(true)->where(array('is_open'=>'1'))->order('`term_id` DESC')->find();
		if(empty($now_term)){
			$this->assign('jumptoUrl',U('Home/index'));
			$this->assign('close_error',1);
			$this->assign('close_reason','视频拍卖活动已结束');
		}
		$this->assign('now_term',$now_term);

	//	$now_term = D('Videosale_term')->field(true)->where(array('is_open'=>'1'))->order('`term_id` DESC')->find();
//
//		$this->assign('now_term',$now_term);
//		if(empty($now_term)){
//			$this->assign('jumpUrl',U('Index/index'));
//			$this->error('视频拍卖活动已结束！');
//		}
		$this->display();
	}
	//在线会员列表
	public function getFirstDatas(){
		//在线会员列表
		$onlineUsers = S('video_onlineUsers');
		$unlineUsers = S('video_unlineUsers');
		if(empty($onlineUsers)) $onlineUsers = array();
		if(empty($unlineUsers)) $unlineUsers = array();
		if($this->user_session){
			if(empty($onlineUsers[$this->user_session['uid']])){
				$nowUser = array(
					'uid'=>$this->user_session['uid'],
					'is_sys_op'=>$this->user_session['is_sys_op'],
					'chujia_name'=>$this->user_session['chujia_name'],
					'name'=>$this->user_level_info['lname'],
					'pic'=>$this->user_level_info['img'],
					'time'=>$_SERVER['REQUEST_TIME'],
					'uninxtime'=>$_SERVER['REQUEST_TIME'],
				);
				$onlineUsers[$this->user_session['uid']] = $nowUser;
			}else{
				$onlineUsers[$this->user_session['uid']]['time'] = $_SERVER['REQUEST_TIME'];
			}
			unset($unlineUsers[$this->user_session['uid']]);
		}

		//遍历清除10秒钟没反应的人
		foreach($onlineUsers as $key=>$value){
			if($value['time'] < $_SERVER['REQUEST_TIME'] - 10){
				$value['time'] = $value['uninxtime'] = $_SERVER['REQUEST_TIME'];
				$unlineUsers[$value] = $value;
				unset($onlineUsers[$key]);
			}
		}
		foreach($unlineUsers as $key=>$value){
			if($value['time'] < $_SERVER['REQUEST_TIME'] - 10){
				unset($unlineUsers[$key]);
			}
		}
		S('video_onlineUsers',$onlineUsers,0);
		S('video_unlineUsers',$unlineUsers,0);

		$return['onlineUsers'] = $onlineUsers;

		//待拍域名列表
		$domainList = S('video_domainList_'.$_POST['term_id']);
		if(empty($domainList)){
			$domainList = $this->updateCacheTerm($_POST['term_id']);
		}
		if(!empty($domainList)){
			$plusPriceArr = getFastbidPriceList($domainList[0]['now_money']);
			$domainList[0]['plusPrice'] = $plusPriceArr['plusPrice'];
			$domainList[0]['pleasePrice'] = $plusPriceArr['plusPrice'] + $domainList[0]['now_money'];
			$domainList[0]['now_money'] = getFriendMoney($domainList[0]['now_money'],true);
			$domainList[0]['mark_money'] = getFriendMoney($domainList[0]['mark_money'],true);
		}else{
			$domainList = 'null';
		}
		$return['domainList'] = $domainList;

		//出价记录
		$priceHistory = S('priceHistory_'.$domainList[0]['domain_id']);
		if(!is_array($priceHistory)){
			$priceHistory = array();
		}
		$return['priceHistory'] = $priceHistory;

		//聊天记录
		$msgHistory = S('msgHistory_'.$domainList[0]['domain_id']);
		if(!is_array($msgHistory)){
			$msgHistory = array();
		}
		$return['msgHistory'] = $msgHistory;

		//锁住屏幕
		$return['lockScreen'] = S('userScreenLock');
		if(empty($return['lockScreen'])){
			$return['lockScreen'] = '0';
		}

		$return['lastAjaxTime'] = time();

		$this->success($return);
	}
	/*锁住用户屏幕*/
	public function lockScreen(){
		S('userScreenLock',$_POST['lockScreen'],0);
		$this->success('操作成功');
	}
	/*结束拍卖*/
	public function finishDomain(){
		$now_domain = D('Videosale_domains')->where(array('domain_id'=>$_POST['domainId']))->find();
		if(D('Videosale_domains')->where(array('domain_id'=>$_POST['domainId']))->data(array('domain_status'=>'2'))->save()){
			if(!empty($now_domain['uid']) && !empty($now_domain['lastFidId'])){
				$lastFid = D('Videosale_domains_list')->field(true)->where(array('pigcms_id'=>$now_domain['lastFidId']))->find();
				$now_freeze = D('User_freeze')->where(array('id'=>$lastFid['freeze_id']))->find();

				//添加到订单表
				$dataOrder['addTime'] = $_SERVER['REQUEST_TIME'];
				$dataOrder['total_price'] = $now_domain['now_money'];
				$dataOrder['domainName'] = $now_domain['domains'];
				$dataOrder['trade_type'] = '0';
				$dataOrder['status'] = '0';
				$dataOrder['buy_id'] = $lastFid['uid'];
				$dataOrder['sell_id'] = $now_domain['uid'];
				$dataOrder['staff_id'] = '0';
				$dataOrder['payTye'] = '2';
				$dataOrder['yes_no'] = '3';
				$dataOrder['freeze_id'] = $lastFid['freeze_id'];
				$order_id = D('Order')->data($dataOrder)->add();
				// dump($dataOrder);exit;
				//添加到订单详细信息表
				$dataOrderInfo['order_id'] = $order_id;
				$dataOrderInfo['info'] =  '买家视频拍卖竞得域名，等待买家付款中 买家已冻结保证金 '.getFriendMoney($now_freeze['freezemoney'],true) .' 元';
				$dataOrderInfo['time'] = $_SERVER['REQUEST_TIME'];
				D('Order_info')->data($dataOrderInfo)->add();
			}

			S('priceHistory_'.$_POST['domainId'],array());
			$this->updateCacheTerm($_POST['term_id']);
			$this->success('操作成功');
		}else{
			$this->error('操作失败');
		}
	}
	public function ajaxCheck(){
		set_time_limit(0);
		session_write_close();
		for($i=0;$i<30;$i++){
			//在线会员列表
			$onlineUsers = S('video_onlineUsers');
			$unlineUsers = S('video_unlineUsers');
			if(empty($onlineUsers)) $onlineUsers = array();
			if(empty($unlineUsers)) $unlineUsers = array();
			if($this->user_session){
				if(empty($onlineUsers[$this->user_session['uid']])){
					$nowUser = array(
						'uid'=>$this->user_session['uid'],
						'is_sys_op'=>$this->user_session['is_sys_op'],
						'chujia_name'=>$this->user_session['chujia_name'],
						'name'=>$this->user_level_info['lname'],
						'pic'=>$this->user_level_info['img'],
						'time'=>time(),
						'uninxtime'=>time(),
					);
					$onlineUsers[$this->user_session['uid']] = $nowUser;
				}else{
					$onlineUsers[$this->user_session['uid']]['time'] = time();
				}
				unset($unlineUsers[$this->user_session['uid']]);
			}
			//遍历清除10秒钟没反应的人
			foreach($onlineUsers as $key=>$value){
				if($value['time'] < time() - 10){
					$value['time']  = $value['uninxtime'] = time();
					$unlineUsers[$value['uid']] = $value;
					unset($onlineUsers[$key]);
				}
			}
			foreach($unlineUsers as $key=>$value){
				if($value['time'] < time() - 10){
					unset($unlineUsers[$key]);
				}
			}
			S('video_onlineUsers',$onlineUsers,0);
			S('video_unlineUsers',$unlineUsers,0);

			if($onlineUsers){
				foreach($onlineUsers as $key=>$value){
					if($value['uninxtime'] < $_POST['lastAjaxTime']){
						unset($onlineUsers[$key]);
					}
				}
			}
			if($unlineUsers){
				foreach($unlineUsers as $key=>$value){
					if($value['uninxtime'] < $_POST['lastAjaxTime']){
						unset($unlineUsers[$key]);
					}
				}
			}
			if($onlineUsers) $return['onlineUsers'] = $onlineUsers;
			if($unlineUsers) $return['unlineUsers'] = $unlineUsers;

			//待拍域名列表
			$domainList = S('video_domainList_'.$_POST['term_id']);
			if(empty($domainList)){
				$domainList = $this->updateCacheTerm($_POST['term_id']);
			}
			if($domainList[0]){
				if($domainList[0]['uninxtime'] >= $_POST['lastAjaxTime'] || $domainList[0]['domain_id'] != $_POST['domainId']){
					$plusPriceArr = getFastbidPriceList($domainList[0]['now_money']);
					$domainList[0]['plusPrice'] = $plusPriceArr['plusPrice'];
					$domainList[0]['pleasePrice'] = $plusPriceArr['plusPrice'] + $domainList[0]['now_money'];
					$domainList[0]['now_money'] = getFriendMoney($domainList[0]['now_money'],true);
					$domainList[0]['mark_money'] = getFriendMoney($domainList[0]['mark_money'],true);
					$return['nowDomainInfo'] = $domainList[0];
				}
			}else{
				$this->updateCacheTerm($_POST['term_id']);
				$return['nowDomainInfo'] = 'null';
			}
			//出价记录
			$priceHistory = S('priceHistory_'.$domainList[0]['domain_id']);
			if(is_array($priceHistory)){
				foreach($priceHistory as $key=>$value){
					if($value['uninxtime'] < $_POST['lastAjaxTime']){
						unset($priceHistory[$key]);
					}
				}
				if($priceHistory) $return['priceHistory'] = $priceHistory;
			}
			// file_put_contents('./runtime/cache.php',$priceHistory);

			//聊天记录
			$msgHistory = S('msgHistory_'.$domainList[0]['domain_id']);
			if(is_array($msgHistory)){
				foreach($msgHistory as $key=>$value){
					if($value['uninxtime'] < $_POST['lastAjaxTime']){
						unset($msgHistory[$key]);
					}
				}
				if($msgHistory) $return['msgHistory'] = $msgHistory;
			}

			$lockScreen = S('userScreenLock');
			if($lockScreen != $_POST['lockScreen']){
				$return['lockScreen'] = $lockScreen;
			}

			if($return){
				$return['lastAjaxTime'] = time();
				$this->success($return);exit();
			}
			usleep(1000000);
		}
		$return['lastAjaxTime'] = time();
		$this->success($return);exit();
	}
	public function updateCacheTerm($term_id){
		$domainList = D('Videosale_domains')->field('sort,term_id,domain_status',true)->where(array('term_id'=>$_POST['term_id'],'domain_status'=>'1'))->order('`sort` DESC,`domain_id` DESC')->select();
		foreach($domainList as &$val){
				$val['domains'] = mb_substr($val['domains'],0,8,'utf-8');
		}
		//file_put_contents('s.log',print_r($domainList));
		S('video_domainList_'.$term_id,$domainList,0);
		if(!empty($domainList)){
			$domainList[0]['uninxtime'] = time();
		}
		return $domainList;
	}
	public function sendprice(){
		if(empty($this->user_session)){
			$this->success('5');exit();
		}
		$this->user_session = D('User')->get_user($this->user_session['uid']);
		$databaseDomains = D('Videosale_domains');
		$now_domain = $databaseDomains->field(true)->where(array('domain_id'=>$_POST['domainId']))->find();
		// dump($databaseDomains);
		if(empty($now_domain)){
			$this->error(array(1001,'当前域名不存在或已被删除'));
		}
		if($now_domain['domain_status'] == '2'){
			$this->error(array(1002,'当前域名已经拍卖结束'));
		}else if($now_domain['domain_status'] != '1'){
			$this->error(array(1003,'当前域名状态异常'));
		}
		if($now_domain['lastFidId'] && $now_domain['now_money'] >= $now_domain['mark_money']){
			$now_domain['plusPrice'] = getFastbidPriceList($now_domain['now_money']);
			$now_domain['pleasePrice'] = $now_domain['plusPrice']['plusPrice'] + $now_domain['now_money'];
		}else{
			$now_domain['plusPrice'] = getFastbidPriceList($now_domain['mark_money']);
			$now_domain['pleasePrice'] = $now_domain['plusPrice']['plusPrice'] + $now_domain['mark_money'];
		}
		if($_POST['offer'] < $now_domain['pleasePrice']){
			//$this->updateCacheTerm($_POST['term_id']);	//清空当前域名拍卖组的缓存
			$this->error(array(1004,$now_domain['pleasePrice']));
		}
		if(in_array(1,$this->user_session['permissions'])){
			$freeze = 0;
		}else{
			$freeze = $now_domain['plusPrice']['depositPrice'];
		}

		$not_use_free = false;
		if($this->user_session['group_id']){

			$not_use_free = true;
		}else{
			$not_use_free = false;
		}

		if($this->user_session['now_money'] < $freeze && !$not_use_free){
			$this->error(array(1005,'帐户余额不够缴费保证金，您还需要充值 ￥'.($freeze-$this->user_session['now_money'])));
		}
		$databaseUser = D('User');
		$conditionUser['uid'] = $this->user_session['uid'];
		$dataUser['now_money'] = $this->user_session['now_money'] - $freeze;
		$dataUser['freeze_money'] = $this->user_session['freeze_money'] + $freeze;
		if($databaseUser->where($conditionUser)->data($dataUser)->save()){
			$priceHistory = S('priceHistory_'.$_POST['domainId']);
			if(empty($priceHistory)) $priceHistory = array();
			$nowPrice = array(
				'uid'=>$this->user_session['uid'],
				'is_sys_op'=>$this->user_session['is_sys_op'],
				'chujia_name'=>$this->user_session['chujia_name'],
				'price'=>$_POST['offer'],
				'pic'=>$this->user_level_info['img'],
				'time'=>date('H:i'),
				'uninxtime'=>time(),
			);
			array_push($priceHistory,$nowPrice);
			S('priceHistory_'.$_POST['domainId'],$priceHistory,0);

			//添加到冻结记录表
			$data_user_freeze['uid'] = $this->user_session['uid'];
			$data_user_freeze['freezetime'] = $_SERVER['REQUEST_TIME'];
			$data_user_freeze['freezemoney'] = $freeze;
			$data_user_freeze['info'] = '视频拍卖域名 '.$now_domain['domain'].'冻结保证金';
			$data_user_freeze['type'] = '6';
			$freeze_id = D('User_freeze')->data($data_user_freeze)->add();

			//添加到资金记录表
			D('User_money_list')->add_row($this->user_session['uid'],12,$freeze,$dataUser['now_money'],'视频拍卖域名 '.$now_domain['domain'].' 冻结保证金');

			//添加到竞价记录表
			$dataDomainsFastbidList['time'] = time();
			$dataDomainsFastbidList['freeze_id'] = $freeze_id;
			$dataDomainsFastbidList['domain_id'] = $now_domain['domain_id'];
			$dataDomainsFastbidList['money'] = $_POST['offer'];
			$dataDomainsFastbidList['uid'] = $this->user_session['uid'];
			$lastFidId = D('Videosale_domains_list')->data($dataDomainsFastbidList)->add();

			//修改域名状态
			$dataDomain = array('now_money'=>$_POST['offer'],'lastFidId'=>$lastFidId);
			$databaseDomains->where(array('domain_id'=>$now_domain['domain_id']))->data($dataDomain)->save();


			//将上一位用户的保证金还回去
			if($now_domain['lastFidId']){
				$lastFid = D('Videosale_domains_list')->field(true)->where(array('pigcms_id'=>$now_domain['lastFidId']))->find();

				$condition_last_user_freeze['id'] = $lastFid['freeze_id'];
				$now_freeze = D('User_freeze')->where($condition_last_user_freeze)->find();

				$databaseUser = D('User');
				$lastFidUser = $databaseUser->get_user($lastFid['uid']);
				$conditionLastFidUser['uid'] = $lastFidUser['uid'];
				$dataLastFidUser['now_money'] = $lastFidUser['now_money'] + $now_freeze['freezemoney'];
				$dataLastFidUser['freeze_money'] = $lastFidUser['freeze_money'] - $now_freeze['freezemoney'];
				if($databaseUser->where($conditionLastFidUser)->data($dataLastFidUser)->save()){
					D('User_freeze')->where($condition_last_user_freeze)->delete();
					// dump(D('User_freeze')->getLastSql());
					D('User_money_list')->add_row($lastFidUser['uid'],13,$now_freeze['freezemoney'],$dataLastFidUser['now_money'],'视频拍卖域名 '.$now_domain['domain'].' 解冻保证金');
				}
			}
			//更新当前域名拍卖组的缓存
			$domainList = S('video_domainList_'.$_POST['term_id']);

			if(empty($domainList)){
				$domainList = $this->updateCacheTerm($_POST['term_id']);
			}
			$plusPriceArr = getFastbidPriceList($_POST['offer']);
			$domainList[0]['plusPrice'] = $plusPriceArr['plusPrice'];
			$domainList[0]['pleasePrice'] = $plusPriceArr['plusPrice'] + $_POST['offer'];
			$domainList[0]['now_money'] = $_POST['offer'];
			$domainList[0]['mark_money'] = $domainList[0]['mark_money'];
			$domainList[0]['uninxtime'] = time();
			S('video_domainList_'.$_POST['term_id'],$domainList,0);

			$this->success($_POST['offer']);
		}else{
			$this->error(array(1006,'异常，请重试'));
		}
	}
	public function sendmsg(){
		$msgHistory = S('msgHistory_'.$_POST['domainId']);
		if(empty($msgHistory)) $msgHistory = array();
		$time = date('H:i:s');
		$nowMsg = array(
			'uid'=>$this->user_session['uid'],
			'is_sys_op'=>$this->user_session['is_sys_op'],
			'chujia_name'=>$this->user_session['chujia_name'],
			'msg'=>htmlspecialchars_decode($_POST['msg']),
			'name'=>$this->user_level_info['lname'],
			'pic'=>$this->user_level_info['img'],
			'time'=>$time,
			'uninxtime'=>time(),
		);
		array_push($msgHistory,$nowMsg);
		S('msgHistory_'.$_POST['domainId'],$msgHistory);
		S('msgHistoryTime_'.$_POST['domainId'],$time);
	}
	public function framevideo(){
		$this->display();
	}
	public function detail(){
		$domain_id = $_GET['id'];
		if(empty($domain_id)){
			redirect(U('Fastbid/index'));
		}
		$databaseDomains = D('Domains');
		$now_domain = $databaseDomains->field(true)->where(array('domain_id'=>$domain_id))->find();
		if(empty($now_domain)){
			$this->assign('jumpUrl',U('Fastbid/index'));
			$this->error('当前域名不存在或已被删除');
		}
		if($now_domain['status'] != 1){
			$this->assign('jumpUrl',U('Fastbid/index'));
			$this->error($now_domain['status'] == 0 ? '当前域名正在审核中' : '当前域名已经出售');
		}
		if($now_domain['endTime'] < $_SERVER['REQUEST_TIME']){
			$this->endFastBid($now_domain);
			$this->error('当前竞价已结束');
		}
		D('Domains')->where(array('domain_id'=>$domain_id))->setInc('hits');

		$now_domain['overplus_time']    = $now_domain['endTime'] - $_SERVER['REQUEST_TIME'];
		$now_domain['overplus_time_d']  = floor($now_domain['overplus_time']/86400);
		$now_domain['overplus_time_dh'] = $now_domain['overplus_time']%86400;
		// echo $now_domain['overplus_time_d'].'<br/>';
		// echo $now_domain['overplus_time_dh'].'<br/><br/><br/>';
		$now_domain['overplus_time_h']  = floor($now_domain['overplus_time_dh']/3600);
		$now_domain['overplus_time_hm']  = $now_domain['overplus_time_dh']%3600;
		// echo $now_domain['overplus_time_h'].'<br/>';
		// echo $now_domain['overplus_time_hm'].'<br/><br/><br/>';
		$now_domain['overplus_time_m']  = floor($now_domain['overplus_time_hm']/60);
		$now_domain['overplus_time_ms']  = $now_domain['overplus_time_hm']%60;
		// echo $now_domain['overplus_time_m'].'<br/>';
		// echo $now_domain['overplus_time_ms'].'<br/>';
		$now_domain['overplus_time_s']  = floor($now_domain['overplus_time_ms']/60);
		$now_domain['hits'] += 1;
		if($now_domain['lastFidId'] && $now_domain['money'] >= $now_domain['mark_money']){
			$now_domain['plusPrice'] = getFastbidPriceList($now_domain['money']);
			$now_domain['pleasePrice'] = $now_domain['plusPrice']['plusPrice'] + $now_domain['money'];
		}else{
			$now_domain['plusPrice'] = getFastbidPriceList($now_domain['mark_money']);
			$now_domain['pleasePrice'] = $now_domain['plusPrice']['plusPrice'] + $now_domain['mark_money'];
		}
		$now_domain['money'] = getFriendMoney($now_domain['money'],true);
		$now_domain['mark_money'] = getFriendMoney($now_domain['mark_money'],true);
		$this->assign('now_domain',$now_domain);

		if($now_domain['lastFidId']){
			$fidList = D('Domains_fastbid_list')->where(array('domain_id'=>$now_domain['domain_id']))->order('`pigcms_id` DESC')->select();
			foreach($fidList as $key=>$value){
				$fidList[$key]['money'] = getFriendMoney($value['money'],true);
			}
			$this->assign('fidList',$fidList);
		}

		$allPlusPrice = getFastbidPriceList(-1);
		$this->assign('allPlusPrice',$allPlusPrice);

		$this->display();
	}
	public function fastbidmoney(){
		if(empty($this->user_session)){
			$this->success('5');exit();
		}
		$this->user_session = D('User')->get_user($this->user_session['uid']);
		$domain_id = $_POST['auctionId'];
		if(empty($domain_id)){
			$this->error(U('Buydomains/index'));
		}
		$databaseDomains = D('Domains');
		$now_domain = $databaseDomains->field(true)->where(array('domain_id'=>$domain_id))->find();
		if(empty($now_domain)){
			$this->error('当前域名不存在或已被删除');
		}
		if($now_domain['type'] != 2){
			$this->error('当前域名不属于竞价');
		}
		if($this->user_session['uid'] == $now_domain['uid']){
			$this->success('4');exit();
		}
		if($now_domain['status'] != '1' || $now_domain['endTime'] < $_SERVER['REQUEST_TIME']){
			$this->endFastBid($now_domain);
			$this->success('0');exit();
		}
		if($now_domain['lastFidId'] && $now_domain['money'] >= $now_domain['mark_money']){
			$now_domain['plusPrice'] = getFastbidPriceList($now_domain['money']);
			$now_domain['pleasePrice'] = $now_domain['plusPrice']['plusPrice'] + $now_domain['money'];
		}else{
			$now_domain['plusPrice'] = getFastbidPriceList($now_domain['mark_money']);
			$now_domain['pleasePrice'] = $now_domain['plusPrice']['plusPrice'] + $now_domain['mark_money'];
		}
		if($_POST['offer'] < $now_domain['pleasePrice']){
			$this->error('您的价格必须大于或等于 ￥'.$now_domain['pleasePrice']);exit();
		}
		if($this->user_session['now_money'] < $now_domain['plusPrice']['plusPrice']){
			$this->success('1');exit();
		}

		$freeze = $now_domain['plusPrice']['plusPrice'];
		$databaseUser = D('User');
		$conditionUser['uid'] = $this->user_session['uid'];
		$dataUser['now_money'] = $this->user_session['now_money'] - $freeze;
		$dataUser['freeze_money'] = $this->user_session['freeze_money'] + $freeze;
		if($databaseUser->where($conditionUser)->data($dataUser)->save()){
			//添加到冻结记录表
			$data_user_freeze['uid'] = $this->user_session['uid'];
			$data_user_freeze['freezetime'] = $_SERVER['REQUEST_TIME'];
			$data_user_freeze['freezemoney'] = $freeze;
			$data_user_freeze['info'] = '竞价域名 '.$now_domain['domain'].'冻结保证金';
			$data_user_freeze['type'] = '6';
			$freeze_id = D('User_freeze')->data($data_user_freeze)->add();

			//添加到资金记录表
			D('User_money_list')->add_row($this->user_session['uid'],12,$freeze,$dataUser['now_money'],'竞价域名 '.$now_domain['domain'].' 冻结保证金');

			//添加到竞价记录表
			$dataDomainsFastbidList['time'] = time();
			$dataDomainsFastbidList['freeze_id'] = $freeze_id;
			$dataDomainsFastbidList['domain_id'] = $now_domain['domain_id'];
			$dataDomainsFastbidList['money'] = $_POST['offer'];
			$dataDomainsFastbidList['uid'] = $this->user_session['uid'];
			$lastFidId = D('Domains_fastbid_list')->data($dataDomainsFastbidList)->add();

			//修改域名状态
			$dataDomain = array('money'=>$_POST['offer'],'lastFidId'=>$lastFidId);
			if(time()-300 >= $now_domain['endTime']){
				$dataDomain['endTime'] = $now_domain['endTime']+300;
			}
			D('Domains')->where(array('domain_id'=>$now_domain['domain_id']))->data($dataDomain)->save();


			//将上一位用户的保证金还回去
			if($now_domain['lastFidId']){
				$lastFid = D('Domains_fastbid_list')->field(true)->where(array('pigcms_id'=>$now_domain['lastFidId']))->find();

				$condition_last_user_freeze['id'] = $lastFid['freeze_id'];
				$now_freeze = D('User_freeze')->where($condition_last_user_freeze)->find();

				$databaseUser = D('User');
				$lastFidUser = $databaseUser->get_user($lastFid['uid']);
				$conditionLastFidUser['uid'] = $lastFidUser['uid'];
				$dataLastFidUser['now_money'] = $lastFidUser['now_money'] + $now_freeze['freezemoney'];
				$dataLastFidUser['freeze_money'] = $lastFidUser['freeze_money'] - $now_freeze['freezemoney'];
				if($databaseUser->where($conditionLastFidUser)->data($dataLastFidUser)->save()){
					D('User_freeze')->where($condition_last_user_freeze)->delete();
					// dump(D('User_freeze')->getLastSql());
					D('User_money_list')->add_row($lastFidUser['uid'],13,$now_freeze['freezemoney'],$dataLastFidUser['now_money'],'竞价域名 '.$now_domain['domain'].' 解冻保证金');
				}
			}

			$this->success('10');
		}else{
			// dump($databaseUser);
			$this->success('7');
		}
	}
	public function sold(){
		$Db_domains = D("Domains");
		//type =>3 -优质域名
		$domains_condition_youzhi['type'] = '3';
		$domains_condition_youzhi['status'] = '2';
		$count_domains_youzhi = $Db_domains->where($domains_condition_youzhi)->count();

		import('@.ORG.domain_page');
		$p = new Page($count_domains_youzhi, 24);
		$domains_youzhi_list = $Db_domains->field(true)->where($domains_condition_youzhi)->order('`add_time` DESC,`is_hot` DESC,`domain_id` ASC')->limit($p->firstRow . ',' . $p->listRows)->select();

		$page = $p->show();
		$this->assign('page',$page);
		$this->assign("count_domains_youzhi",$count_domains_youzhi);
		$this->assign('domains_youzhi_list', $domains_youzhi_list);

		//推荐域名
		$domains_condition_speity['type'] = '3';
		$domains_condition_speity['status']    = '1';
		$domains_condition_speity['domain_id']  = array('egt','(SELECT FLOOR(MAX(`domain_id`)*RAND()) FROM `pigcms_domains`)');
		$domains_speity_list = $Db_domains->field(true)->where($domains_condition_speity)->order('`add_time` DESC, `domain_id` DESC')->limit(10)->select();
		$this->assign('domains_speity_list',$domains_speity_list);

		$this->display();
	}
}