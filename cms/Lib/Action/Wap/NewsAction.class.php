<?php

class NewsAction extends  BaseAction{

	public function newslist(){

		$cat_id = intval($_GET['cat_id']) ? intval($_GET['cat_id']) : 0;
		if($cat_id > 0){
			$Db_news = M('News');
			$Db_news_category = M('News_category');
			$cat_title = $Db_news_category->field('`cat_name`')->where('`cat_id` = '. $cat_id )->find();
			$list_condition['cat_id'] = $cat_id;
			$list_condition['state'] = 1;

			$count =$Db_news->where($list_condition)->count();
			import('@.ORG.system_page');
			$p = new Page($count, 20);
			$list = $Db_news->field(true)->where($list_condition)->order('`add_time` DESC,`news_id` DESC')->limit($p->firstRow.','.$p->listRows)->select();

			$pagebar = $p->show();
			$this->assign('list',$list);
			$this->assign('cat_title', $cat_title);
			$this->assign('count', $count);
			$this->assign('pagebar',$pagebar);
			$this->assign('error_no',0);
		}else{
			$this->assign('error_no',1);
			$this->assign('erro_msg','咦？你从哪里来的啊？这里不是地球啊。\n 来，我们还是回地球吧。');
		}

		$this->display();
	}
        
        public function show(){
            $news_id = intval($_GET['news_id']) ? intval($_GET['news_id']): 0 ;
            if($news_id > 0){
                $Db_news = M('News');
                $news_show = $Db_news->where(array('news_id'=>$news_id))->find();
                $Db_news_category = M('News_category');
                $category = $Db_news_category->field('`cat_name`,`cat_key`')->where(array('cat_id'=>$news_show['cat_id']))->find();
                $this->assign('category',$category);
                $this->assign('news_show',$news_show);
            }else{
                    $this->assign('error_no',1);
                    $this->assign('erro_msg','咦？你从哪里来的啊？这里不是地球啊。\n 来，我们还是回地球吧。');
            }
            $this->display();
        }

}