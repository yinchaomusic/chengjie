<?php

/**
 * Class SellerAction
 * 我是卖家
 */

class SellerAction extends  BaseAction{
        protected function _initialize(){
		parent::_initialize();
		if(empty($this->user_session)){
			redirect(U('Login/login'));
		}
	}

	public function index(){

		//我是所有域名


		$this->display();
	}

	public function getglist(){
		if($this->isAjax()) {
			$group_list = D('Domains_group')->field(true)->where(array('uid' => $this->user_session['uid']))->order('`group_id` ASC')->select();
			echo json_encode($group_list);exit;
		}
	}

	public function allsell(){
		$Dbdomains = M('domains');
		if(IS_POST){

			$keyword = trim($_POST['search_key']);
			if(!empty($keyword)){
				$get_str =  array();
				$get_str = explode('.',$keyword);
				if(empty($get_str[1])){ //like %xxx%
					$conditions['domain'] = array('LIKE','%'.$keyword.'%');
					$this->assign('keyword',$keyword);
				}else{ // =
					$conditions['domain'] = $keyword;
				}
			}else{
				if(intval($_POST['domain_id']) > 0){
					$datas['domain_id'] = intval($_POST['domain_id']);
					$money = intval($_POST['money']);
					if(!empty($money)){
						$datas['money'] = intval($_POST['money']);
					}
					$desc = trim($_POST['desc']);
					if(!empty($desc)){
						$datas['desc'] = trim($_POST['desc']);
					}

					$datas['group_id'] = intval($_POST['glistid']);
					if(intval($_POST['type']) == 2){
						$datas['mark_money'] = intval($_POST['money']);
					}
					// 0议价 yj，1一口价 ykj，2竞价jj，3优质yz
					if( intval($_POST['settype'])== 3 ){
						$datas['status'] = 0;
					}
					$datas['type'] = intval($_POST['settype']);
					$Dbdomains->data($datas)->save();
				}
			}


		}
		$fields = '`domain_id`,`domain`,`uid`,`group_id`,`desc`,`money`,`mark_money`,`type`,`status`';
		$conditions['uid'] =  $this->user_session['uid'];
		$conditions['status'] =  array('in','0,1'); //0待审核，1正常

		$mydomains =  $Dbdomains->field($fields)->where($conditions)->order('`type` ASC,`add_time` DESC')->select();
		foreach($mydomains as $k=>$val){
			$mydomains[$k]['gname'] = D('Domains_group')->field('`group_id`,`group_name`')->where(array('uid'=>$this->user_session['uid'],
				'group_id'=>$val['group_id']))->find();
		}

        $this->assign('mydomains',$mydomains);
		$this->display();
	}

	public function xdel(){
		if($this->isAjax()){
			//echo json_encode($_POST['id']);
			$domain_id = intval($_POST['id']) ?intval($_POST['id']) :0;
			if($domain_id >0){
				$Dbdomains = M('Domains');

				if($Dbdomains->where(array('domain_id'=>$domain_id,'uid'=>$this->user_session['uid']))->delete()){
					exit(json_encode(array('error'=>1,'msg'=>'删除成功')));
				}else{
					exit(json_encode(array('error'=>0,'msg'=>'删除失败！请重试')));
				}
			}else{
				exit(json_encode(array('error'=>0,'msg'=>'Error')));
			}
		}else{
			exit(json_encode(array('error'=>0,'msg'=>'Error')));
		}
	}

	public function allsell_yj(){
		$Dbdomains = M('domains');
		if(IS_POST){
			$keyword = trim($_POST['search_key']);
			if(!empty($keyword)){
				$get_str =  array();
				$get_str = explode('.',$keyword);
				if(empty($get_str[1])){ //like %xxx%
					$conditions['domain'] = array('LIKE','%'.$keyword.'%');
					$this->assign('keyword',$keyword);
				}else{ // =
					$conditions['domain'] = $keyword;
				}
			}else{
				if(intval($_POST['domain_id']) > 0){
					$datas['domain_id'] = intval($_POST['domain_id']);
					$money = intval($_POST['money']);
					if(!empty($money)){
						$datas['money'] = intval($_POST['money']);
					}
					$desc = trim($_POST['desc']);
					if(!empty($desc)){
						$datas['desc'] = trim($_POST['desc']);
					}
					$datas['group_id'] = intval($_POST['glistid']);
					if(intval($_POST['type']) == 2){
						$datas['mark_money'] = intval($_POST['money']);
					}
					// 0议价 yj，1一口价 ykj，2竞价jj，3优质yz
					if( intval($_POST['settype'])== 3 ){
						$datas['status'] = 0;
					}
					$datas['type'] = intval($_POST['settype']);
					$Dbdomains->data($datas)->save();
				}
			}


		}
		$fields = '`domain_id`,`domain`,`uid`,`group_id`,`desc`,`money`,`mark_money`,`type`,`status`';
		$conditions['uid'] =  $this->user_session['uid'];
		$conditions['status'] =  1;
		$conditions['type'] =  0;

		$mydomains =  $Dbdomains->field($fields)->where($conditions)->order('`type` ASC,`add_time` DESC')->select();
		foreach($mydomains as $k=>$val){
			$mydomains[$k]['gname'] = D('Domains_group')->field('`group_id`,`group_name`')->where(array('uid'=>$this->user_session['uid'],
				'group_id'=>$val['group_id']))->find();
		}

		$this->assign('mydomains',$mydomains);
		$this->display();
	}

	public function allsell_jj(){
		$Dbdomains = M('domains');
		if(IS_POST){
			$keyword = trim($_POST['search_key']);
			if(!empty($keyword)){
				$get_str =  array();
				$get_str = explode('.',$keyword);
				if(empty($get_str[1])){ //like %xxx%
					$conditions['domain'] = array('LIKE','%'.$keyword.'%');
					$this->assign('keyword',$keyword);
				}else{ // =
					$conditions['domain'] = $keyword;
				}
			}else{
				if(intval($_POST['domain_id']) > 0){
					$datas['domain_id'] = intval($_POST['domain_id']);
					$money = intval($_POST['money']);
					if(!empty($money)){
						$datas['money'] = intval($_POST['money']);
						$datas['mark_money'] = intval($_POST['money']);
					}
					$desc = trim($_POST['desc']);
					if(!empty($desc)){
						$datas['desc'] = trim($_POST['desc']);
					}
					$datas['group_id'] = intval($_POST['glistid']);
					$Dbdomains->data($datas)->save();
				}
			}


		}
		$fields = '`domain_id`,`domain`,`uid`,`group_id`,`desc`,`money`,`mark_money`,`type`,`status`,`apply_time`';
		$conditions['uid'] =  $this->user_session['uid'];
		$conditions['status'] =  1;
		$conditions['type'] =  2; //2竞价

		$mydomains =  $Dbdomains->field($fields)->where($conditions)->order('`type` ASC,`add_time` DESC')->select();
		foreach($mydomains as $k=>$val){
			$mydomains[$k]['gname'] = D('Domains_group')->field('`group_id`,`group_name`')->where(array('uid'=>$this->user_session['uid'],
				'group_id'=>$val['group_id']))->find();
		}

		$this->assign('mydomains',$mydomains);
		$this->display();
	}

	public function allsell_ykj(){
		$Dbdomains = M('domains');
		if(IS_POST){
			$keyword = trim($_POST['search_key']);
			if(!empty($keyword)){
				$get_str =  array();
				$get_str = explode('.',$keyword);
				if(empty($get_str[1])){ //like %xxx%
					$conditions['domain'] = array('LIKE','%'.$keyword.'%');
					$this->assign('keyword',$keyword);
				}else{ // =
					$conditions['domain'] = $keyword;
				}
			}else{
				if(intval($_POST['domain_id']) > 0){
					$datas['domain_id'] = intval($_POST['domain_id']);
					$money = intval($_POST['money']);
					if(!empty($money)){
						$datas['money'] = intval($_POST['money']);
					}
					$desc = trim($_POST['desc']);
					if(!empty($desc)){
						$datas['desc'] = trim($_POST['desc']);
					}
					$datas['group_id'] = intval($_POST['glistid']);
					$Dbdomains->data($datas)->save();
				}
			}


		}
		$fields = '`domain_id`,`domain`,`uid`,`group_id`,`desc`,`money`,`mark_money`,`type`,`status`,`startTime`';
		$conditions['uid'] =  $this->user_session['uid'];
		$conditions['status'] =  1;
		$conditions['type'] =  1; //1一口价

		$mydomains =  $Dbdomains->field($fields)->where($conditions)->order('`type` ASC,`add_time` DESC')->select();
		foreach($mydomains as $k=>$val){
			$mydomains[$k]['gname'] = D('Domains_group')->field('`group_id`,`group_name`')->where(array('uid'=>$this->user_session['uid'],
				'group_id'=>$val['group_id']))->find();
		}

		$this->assign('mydomains',$mydomains);
		$this->display();
	}

	public function allsell_yz(){
		$Dbdomains = M('domains');
		if(IS_POST){
			$keyword = trim($_POST['search_key']);
			if(!empty($keyword)){
				$get_str =  array();
				$get_str = explode('.',$keyword);
				if(empty($get_str[1])){ //like %xxx%
					$conditions['domain'] = array('LIKE','%'.$keyword.'%');
					$this->assign('keyword',$keyword);
				}else{ // =
					$conditions['domain'] = $keyword;
				}
			}else{
				if(intval($_POST['domain_id']) > 0){
					$datas['domain_id'] = intval($_POST['domain_id']);
					$money = intval($_POST['money']);
					if(!empty($money)){
						$datas['money'] = intval($_POST['money']);
					}
					$desc = trim($_POST['desc']);
					if(!empty($desc)){
						$datas['desc'] = trim($_POST['dedsc']);
					}
					$datas['group_id'] = intval($_POST['glistid']);
					$Dbdomains->data($datas)->save();
				}
			}


		}
		$fields = '`domain_id`,`domain`,`uid`,`group_id`,`desc`,`money`,`mark_money`,`type`,`status`,`startTime`';
		$conditions['uid'] =  $this->user_session['uid'];
		$conditions['status'] =  1;
		$conditions['type'] =  3; //3优质

		$mydomains =  $Dbdomains->field($fields)->where($conditions)->order('`type` ASC,`add_time` DESC')->select();
		foreach($mydomains as $k=>$val){
			$mydomains[$k]['gname'] = D('Domains_group')->field('`group_id`,`group_name`')->where(array('uid'=>$this->user_session['uid'],
				'group_id'=>$val['group_id']))->find();
		}

		$this->assign('mydomains',$mydomains);
		$this->display();
	}

	public function ban(){
		//下架域名
		if(IS_POST){
			$conditionDomain['uid'] = $this->user_session['uid'];
			$conditionDomain['domain_id'] = intval($_POST['id']);
			if(D('Domains')->where($conditionDomain)->data(array('type'=>'0'))->save()){
				exit(json_encode(array('error'=>1,'msg'=>'下架成功,下架域名')));
			}else{
				exit(json_encode(array('error'=>0,'msg'=>'下架失败，请重试')));
			}
		}
	}

	public function adddomainss(){
		$group_list = D('Domains_group')->field(true)->where(array('uid' => $this->user_session['uid']))->order('`group_id` ASC')->select();
		$this->assign('group_list',$group_list);
		$this->display();
	}

	public function adddomainss_save(){
		if($this->isAjax()){
/**
 * array(4) {
["domain"] => string(7) "q22.com"
["price"] => string(8) "43432423"
["group_id"] => string(1) "4"
["desc"] => string(36) "定时开机刘飞的酸辣粉就的"
}
 */

			if(trim($_POST['domain']) == ''){
				 $error = 1;
				 $msg = '域名必须要填写的哦';
				 $title = '域名必填';
				exit(json_encode(array('error'=>$error,'msg'=>$msg,'title'=>$title)));
			}
			$databaseDomainsGroup = D('Domains_group');
			$group_id = intval($_POST['group_id']);
			if($group_id){
				$nowGroup = $databaseDomainsGroup->field('`group_id`')->where(array('group_id'=>$group_id,'uid'=>$this->user_session['uid']))->find();
				if(empty($nowGroup)){
					$error = 1;
					$msg = '你选择的分组不存在';
					$title = '域名必填';
					exit(json_encode(array('error'=>$error,'msg'=>$msg,'title'=>$title)));
				}
			}
			//分析提交域名

			$keyword = strtolower(trim($_POST['domain']));
			$get_str =  array();
			$get_str = explode('.',$keyword);

			if(count($get_str) < 0){
				$error = 2;
				$msg   = '域名不合法';
				$title = '域名必填';
				exit(json_encode(array('error'=>$error,'msg'=>$msg,'title'=>$title)));
			}

			if(empty($get_str[1])){
				$error = 3;
				$msg   = '域名后缀必须填写';
				$title = '域名必填';
				exit(json_encode(array('error'=>$error,'msg'=>$msg,'title'=>$title)));
			}
			$top_domains = $get_str[0];

			if(!preg_match('/^\w+$/',$top_domains)){
				$error = 2;
				$msg   = '目前只支持英文域名';
				$title = '域名必填';
				exit(json_encode(array('error'=>$error,'msg'=>$msg,'title'=>$title)));
			}
			$suffix = '.'.$get_str[1];
			//找出系统支持的后缀列表
			$suffix_list = D('Domain_suffix')->field('`suffix`')->where(array('status'=>'1','suffix'=>$suffix))->find();

			if(empty($suffix_list)){
				$error = 3;
				$msg   = '域名后缀不合法或者暂时不支持该后缀的域名';
				$title = '域名必填';
				exit(json_encode(array('error'=>$error,'msg'=>$msg,'title'=>$title)));
			}

//TODO

			//检查是否已经存在同样的域名

			$Db_DomainCheck = D('Domains_check');

			if($Db_DomainCheck->where(array('domain'=>$keyword,'uid'=>$this->user_session['uid']))->find()){
				unset($_POST);
				$error = 3;
				$msg   = '已添加过该域名';
				$title = '域名必填';
				exit(json_encode(array('error'=>$error,'msg'=>$msg,'title'=>$title)));

			}else{
				$dataDomainCheck['uid']      = $this->user_session['uid'];
				$dataDomainCheck['group_id'] = intval($_POST['group_id']);
				$dataDomainCheck['add_time'] = $_SERVER['REQUEST_TIME'];
				$dataDomainCheck['is_check'] = '0';
				$dataDomainCheck['price'] = intval($_POST['price']);
				$dataDomainCheck['domain'] = $keyword;
				$dataDomainCheck['intro'] = strval($_POST['intro']);

				if($Db_DomainCheck->data($dataDomainCheck)->add()){
					if(intval($_POST['group_id'])){
						$databaseDomainsGroup->where(array('group_id'=>intval($_POST['group_id'])))->setInc('domain_count');
					}
					unset($_POST);
					$error = 0;
					$msg   = '域名：'.$keyword.' 添加成功。';
					$title = '成功';
					exit(json_encode(array('error'=>$error,'msg'=>$msg,'title'=>$title)));
				}else{
					unset($_POST);
					$error = 3;
					$msg   = '添加域名失败，请重新提交';
					$title = '域名必填';
					exit(json_encode(array('error'=>$error,'msg'=>$msg,'title'=>$title)));
				}
			}
		}
	}

	//我的批量交易
	public function pldomain(){
		$databaseBulkTrading = D('Bulk_trading');

		$status = intval($_GET['type']);

		//`status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态 0未审核 1未交易 2正在进行 3交易完成 4审核失败',
		if($status == 0){
			$this->assign('type',-1);
		}else{
			$this->assign('type',intval($_GET['type']));
		}

		$all = false;
		switch($status){
			case 1:
				$conditionBulkTrading['status'] = '0';
				break;
			case 2://审核失败
				$conditionBulkTrading['status'] = '4';
				break;
			case 3://出售中
				$conditionBulkTrading['status'] = '2';
				break;
			case 4://已出售
				$conditionBulkTrading['status'] = '3';
				break;
			default:
				$all = true;
				break;
		}
		if($all){
			$cBulkTrading['uid'] = $this->user_session['uid'];
		//	$tradingList = $databaseBulkTrading->where($cBulkTrading)->select();
		//	$return['total'] = $databaseBulkTrading->where($cBulkTrading)->count();
			$bulk_lists = $databaseBulkTrading->field(true)->where($cBulkTrading)->order('`bulk_id` DESC')->select();
		}else{
			//$tradingList = $databaseBulkTrading->where($conditionBulkTrading)->select();
			//$return['total'] = $databaseBulkTrading->where($conditionBulkTrading)->count();
			$conditionBulkTrading['uid'] = $this->user_session['uid'];
			$bulk_lists = $databaseBulkTrading->field(true)->where($conditionBulkTrading)->order('`bulk_id` DESC')->select();
		}
		$this->assign('bulk_lists',$bulk_lists);
		$this->display();
	}

	public function pldel(){
		if($this->isAjax()){

			$bulk_id = intval($_POST['id']) ?intval($_POST['id']) :0;
			if($bulk_id >0){
				$Db_Bulk_trading =D('Bulk_trading');
				if($Db_Bulk_trading->where(array('bulk_id'=>$bulk_id,'uid'=>$this->user_session['uid']))->delete()){
					exit(json_encode(array('error'=>1,'msg'=>'删除成功')));
				}else{
					exit(json_encode(array('error'=>0,'msg'=>'删除失败！请重试')));
				}
			}else{
				exit(json_encode(array('error'=>0,'msg'=>'Error')));
			}
		}else{
			exit(json_encode(array('error'=>0,'msg'=>'Error')));
		}

	}

	public function getplist(){
		if($this->isAjax()){
			$bulk_id = intval($_POST['id']) ?intval($_POST['id']) :0;
			if($bulk_id >0){
				$databaseBulkTrading = D('Bulk_trading');
				$condition['bulk_id'] = $bulk_id;
				$condition['uid'] = $this->user_session['uid'];

				$get_domains = $databaseBulkTrading->where($condition)->find();

				if($get_domains){
					$get_domains['domain_list'] = unserialize($get_domains['domain_list']);

					$check_domains  = $get_domains['domain_list'];

					$Db_cache_search_domains = D('Cache_search_domains');
					$Db_domain_whoisemail = D('Domain_whoisemail');
					foreach($check_domains as $k=>$v){
						$get_reg_email[$k] = $Db_cache_search_domains->field('`cache_id`,`domainName`,`registrarEmail`')->where("`domainName`='{$v}'")->find();
						if(empty($get_reg_email[$k])){
							//入库whois
							$WhoisAPI = 'http://demo.zhu.cn/index.php?c=Whois&a=whoisinfo&keyword='.$v;
							import('ORG.Net.Http');
							$http = new Http();
							$whoisResult = Http::curlGet($WhoisAPI);
							$whoisResult =json_decode($whoisResult,true);
							if($whoisResult['registrarEmail']){
								$lowemail = strtolower($whoisResult['registrarEmail']);
							}
						}else{
							if($get_reg_email['registrarEmail']){
								$lowemail = strtolower($get_reg_email['registrarEmail']);
							}else{
								$get_domains['ischeck'][] = '验证whois失败';
								$get_domains['checkcalss'][] = 'font_warning';
							}
						}
						$is_uid_email =   $Db_domain_whoisemail->field('`is_check`')->where("`uid` = {$this->user_session['uid']} AND `email` = '{$lowemail}'   AND `is_check` = 1")->find();
						if($is_uid_email){
							$get_domains['ischeck'][] = '验证whois成功';
							$get_domains['checkcalss'][] = 'font_success';
						}else{
							$get_domains['ischeck'][] = '验证whois失败';
							$get_domains['checkcalss'][] = 'font_warning';
						}

					}

					switch($get_domains['status']){
						case 0: //'状态 0未审核 1未交易 2正在进行 3交易完成 4审核失败';
						    $get_domains['statustitle'] = '未审核';
						    $get_domains['fcalss'] = 'font_default';
							break;
						case 1:
							$get_domains['statustitle'] = '展示中';
							$get_domains['fcalss'] = 'font_info';
							break;
						case 2:
							$get_domains['statustitle'] = '正在进行';
							$get_domains['fcalss'] = 'font_important';
							break;
						case 3:
							$get_domains['statustitle'] = '交易完成';
							$get_domains['fcalss'] = 'font_success';
							break;
						case 4:
							$get_domains['statustitle'] = '审核失败';
							$get_domains['fcalss'] = 'font_warning';
							break;
					}



					exit(json_encode($get_domains));
				}else{
					exit(json_encode(array('error'=>0,'msg'=>'暂无数据')));
				}
			}else{
				exit(json_encode(array('error'=>0,'msg'=>'非法操作')));
			}
		}
	}

	//批量交易添加
	public function adddomains_pl(){
		$this->display();
	}

	public function adddomainss_pl_save(){
		if($this->isAjax()){
			$okArr = array();
			$errorArr = array();
			$domains = trim($_POST['domain_list']);
			$meaning = trim($_POST['meaning']);
			$title = trim($_POST['title']);
			$total_price = intval($_POST['total_price']);
			if(empty($domains)) exit(json_encode(array('error'=>1,'msg'=>'非法操作')));
			if(empty($meaning)) exit(json_encode(array('error'=>1,'msg'=>'好的含义可以让买家更容易动心')));
			if(empty($title)) exit(json_encode(array('error'=>1,'msg'=>'好的标题可以让买家更容易动心')));
			if(empty($total_price)) exit(json_encode(array('error'=>1,'msg'=>'填写合理的价格可以让买家更容易动心')));
			$domains = rtrim($domains,',');
			$postDomainTmpArr = explode(',',$domains);
			if(count($postDomainTmpArr) < 2){
				exit(json_encode(array('error'=>1,'msg'=>'添加批量出售域名最少2个')));
			}
			$postDomainArr = array();
			foreach($postDomainTmpArr as $value){
				$value = trim($value);
				if(!empty($value)){
					$postDomainArr[] = trim($value);
				}
			}

			//找出系统支持的后缀列表
			$suffix_list = D('Domain_suffix')->field('`suffix`')->where(array('status'=>'1'))->select();
			$suffixArr = array();
			foreach($suffix_list as $value){
				$suffixArr[] = $value['suffix'];
			}
			foreach($postDomainArr as $key=>$value){
				if(substr_count($value,'.') == 0){
					$errorArr[] = array('error'=>1,'domain'=>$value,'msg'=>'域名不合法');
					continue;
				}
				$tmpDomain = str_replace($suffixArr,'',$value);
				if(empty($tmpDomain)){
					$errorArr[] = array('error'=>1,'domain'=>$value,'msg'=>'域名不合法');
					continue;
				}
				if($tmpDomain == $value){
					$errorArr[] = array('error'=>1,'domain'=>$value,'msg'=>'域名后缀不合法');
					continue;
				}
				if(substr_count($tmpDomain,'.') > 0){
					$errorArr[] = array('error'=>1,'domain'=>$value,'msg'=>'请填写顶级域名');
					continue;
				}
				if(!preg_match('/^\w+$/',$tmpDomain)){
					$errorArr[] = array('error'=>1,'domain'=>$value,'msg'=>'目前只支持英文域名');
					continue;
				}
				$okArr[] = $value;
			}
			if($okArr){
				$databaseBulkTrading = D('Bulk_trading');
				$dataBulkTrading['uid'] = $this->user_session['uid'];
				$dataBulkTrading['domain_list'] = serialize($okArr);
				$dataBulkTrading['title'] = $title;
				$dataBulkTrading['total_price'] = $total_price;
				$dataBulkTrading['meaning'] = $meaning;
				$dataBulkTrading['amount'] = count($okArr);
				$dataBulkTrading['group_id'] = 0;
				$dataBulkTrading['add_time'] = $_SERVER['REQUEST_TIME'];
				$dataBulkTrading['status'] = '0';
				if(!$databaseBulkTrading->data($dataBulkTrading)->add()){
					exit(json_encode(array('error'=>1,'msg'=>'添加失败，请重试')));
				}
			}
			exit(json_encode(array('error'=>0,'msg'=>'成功添加批量域名','errorArr'=>$errorArr,'okArr'=>$okArr)));
		}
	}

	//验证域名
	public function withcheck(){
		$databaseDomainCheck = D('Domains_check');
		$conditionDomainCheck['uid'] = $this->user_session['uid'];
		$conditionDomainCheck['is_check'] = '0';
		$return['total'] = $databaseDomainCheck->where($conditionDomainCheck)->count();
		$return['lists'] = $databaseDomainCheck->field(true)->where($conditionDomainCheck)->order('`domain_id` DESC')->select();
		if($return['lists']){
			foreach($return['lists'] as $key=>$value){
				$return['lists'][$key]['add_time'] = date('Y-m-d H:i:s',$value['add_time']);
			}
		}else{
			$return['lists'] = array();
		}
		$this->assign('domians_check',$return);

		//$this->success($return);
		$this->display();
	}

	//删除待验证的域名
	public function xddel(){
		if($this->isAjax()){
			$domain_id = intval($_POST['id']) ?intval($_POST['id']) :0;
			if($domain_id >0){
				$Db_domains_check =D('Domains_check');
				if($Db_domains_check->where(array('domain_id'=>$domain_id,'uid'=>$this->user_session['uid']))->delete()){
					exit(json_encode(array('error'=>1,'msg'=>'删除成功')));
				}else{
					exit(json_encode(array('error'=>0,'msg'=>'删除失败！请重试')));
				}
			}else{
				exit(json_encode(array('error'=>0,'msg'=>'Error')));
			}
		}else{
			exit(json_encode(array('error'=>0,'msg'=>'Error')));
		}
	}

	public function checkwithdomain(){
		$databaseDomainCheck = D('Domains_check');
		$conditionDomainCheck['uid'] = $this->user_session['uid'];
		$conditionDomainCheck['domain_id'] = intval($_POST['id']);
		$nowDomain = $databaseDomainCheck->field(true)->where($conditionDomainCheck)->find();

		if(empty($nowDomain)){
			exit(json_encode(array('error'=>2,'msg'=>'当前域名不存在')));
		}
		if($_POST['type'] == 0){
			import('@.ORG.NewWhois');
			$newInfo = new NewWhois();
			$whoisResult = $newInfo->getNewWhois($nowDomain['domain']);
			//$whoisResult = $whoisClass->getWhois($nowDomain['domain']);
			if(empty($whoisResult) || empty($whoisResult['registrarEmail'])){
				exit(json_encode(array('error'=>1,'msg'=>'当前域名没找到whois信息,请联系经纪人')));
			}
			// dump($whoisResult);
			if(D('Domain_whoisemail')->where(array('uid'=>$this->user_session['uid'],'email'=>$whoisResult['registrarEmail'],'is_check'=>'1'))->find()){
				if($databaseDomainCheck->where(array('uid'=>$this->user_session['uid'],'domain_id'=>$_POST['id']))->data(array('is_check'=>'1'))->save()){
					//添加到domains域名出售列表，议价
					$dataDomains['uid'] = $this->user_session['uid'];
					$dataDomains['domain'] = $nowDomain['domain'];
					$dataDomains['group_id'] = $nowDomain['group_id'];
					$dataDomains['desc'] = $nowDomain['intro'];
					$dataDomains['money'] = $nowDomain['price'];
					$dataDomains['add_time'] = $_SERVER['REQUEST_TIME'];

					//找出系统支持的后缀列表
					$suffix_list = D('Domain_suffix')->field('`id`,`suffix`')->where(array('status'=>'1'))->select();
					$suffixArr = array();
					$tmpSuffix = array();
					foreach($suffix_list as $value){
						if(str_replace($value['suffix'],'',$nowDomain['domain']) != $nowDomain['domain']){
							if(empty($tmpSuffix) || strlen($value['suffix']) > strlen($tmpSuffix['suffix'])){
								$tmpSuffix = $value;
							}
						}
						$suffixArr[$value['id']] = $value['suffix'];
					}


					$tmpDomain = str_replace($suffixArr,'',$nowDomain['domain']);
					$dataDomains['main_domain'] = $tmpDomain;

					//通过文件匹配域名类型
					import('@.ORG.domainStyle');
					$domainStyle = new domainStyle();
					$styleResult = $domainStyle->getStyle($tmpDomain);
					$dataDomains['styleType'] = $styleResult['styleType'];
					$dataDomains['label'] = $styleResult['label'];
					$dataDomains['style'] = implode(',',$styleResult['style']);

					$dataDomains['suffix'] = $tmpSuffix['id'];
					$dataDomains['length'] = strlen($tmpDomain);
					$dataDomains['status'] = '1';
					if(D('Domains')->data($dataDomains)->add()){
						$databaseDomainCheck->where(array('domain_id'=>$_POST['id']))->delete();
						exit(json_encode(array('error'=>0,'msg'=>'验证成功')));
					}else{
						$databaseDomainCheck->where(array('domain_id'=>$_POST['id']))->data(array('is_check'=>'0'))->save();
						exit(json_encode(array('error'=>1,'msg'=>'验证时发生错误')));
					}
				}else{
					exit(json_encode(array('error'=>1,'msg'=>'验证失败，请重试')));
				}
			}else{
				exit(json_encode(array('error'=>2,'msg'=>'该whois对应的邮箱不是您的邮箱，请添加邮箱')));
			}
		}  else if($_POST['type'] == 1){
			if($databaseDomainCheck->where(array('domain_id'=>  intval($_POST['id']),'uid'=>  $this->user_session['uid']))->delete()){
				//echo 1;
				exit(json_encode(array('error'=>0,'msg'=>'验证成功')));
			}  else {
				//echo 2;
				exit(json_encode(array('error'=>2,'msg'=>'验证失败')));
			}
		}
	}

	public function sellnegotiation(){
		$status = intval($_GET['type']) ? intval($_GET['type']) : 0;
		// `status` int(11) NOT NULL COMMENT '0等待，1成功，2失败',
		$this->assign('type',$status);
		switch($status){
			case 1:
				$condition_quote['status'] = 0;
				break;
			case 2:
				$condition_quote['status'] = 1;
				break;
			case 3:
				$condition_quote['status'] = 2;
				break;
		}

//		if($this->isAjax()){  //Ajax POST start
//			$quote_id = intval($_POST['id']);
//			$condition_quote['quote_id'] = $quote_id;
//			$now_quote = D('Domains_quote')->field(true)->where($condition_quote)->find();
//			if(!empty($now_quote)){
//				//$this->error('当前报价域名不存在');
////				$this->assign('error',1);
////				$this->assign('msg','当前报价域名不存在');
//				//echo '<script>alert("验证码不正确，请重新输入！"); </script>';
//				exit(json_encode(array('error'=>2,'msg'=>'验证失败')));
//			}
//
//		} //Ajax POST end

		if(IS_POST){
			$keyword = strtolower(trim($_POST['search_key']));
			if($keyword){
				$get_str =  array();
				$get_str = explode('.',$keyword);
				if(empty($get_str[1])){ //like %xxx%
					$condition_quote['domain'] = array('LIKE','%'.$keyword.'%');
					//$this->assign('keyword',$keyword);
				}else{
					$condition_quote['domain'] = $keyword;
				}
			}
		}

		$condition_quote['sell_uid'] = $this->user_session['uid'];
		$Db_domains_quote = M('Domains_quote');
		$quote  =$Db_domains_quote->where($condition_quote)->order('`quote_id` DESC')->select();
		$this->assign('quote',$quote);

		$this->display();
	}

	public function get_negotiation(){
		$quote_id = intval($_POST['id']);
		$condition_quote['quote_id'] = $quote_id;
		$Db_Domains_quote = D('Domains_quote');
		$now_quote = $Db_Domains_quote->field(true)->where($condition_quote)->find();
		if(empty($now_quote)){
			exit(json_encode(array('error'=>1,'msg'=>'当前报价域名不存在')));
		}
		if($this->user_session['uid'] != $now_quote['buy_uid'] && $this->user_session['uid'] != $now_quote['sell_uid']){
			exit(json_encode(array('error'=>1,'msg'=>'您没有权限操作此报价单')));
		}
		//查询卖家的挂牌价格
		$conditionDomains['domain_id'] = $now_quote['domain_id'];
		$conditionDomains['status'] = '1';
		$nowDomain = D('Domains')->field('`money`')->where($conditionDomains)->find();
		if(empty($nowDomain)){
			exit(json_encode(array('error'=>1,'msg'=>'当前域名不存在或已被删除')));
		}

		$quote_list =  M('Domains_quote_list')->where($condition_quote)->order('`time` DESC')->select();
		$first_money = number_format($nowDomain['money']);
		foreach($quote_list as &$val){
			$val['time'] = date('Y-m-d H:i:s',$val['time']);
		}
		exit(json_encode(array('error'=>0,'resultdata'=>$quote_list,'first_money'=>$first_money)));
	}

	public function updatenegotiation(){
		if(!IS_POST){
		  exit();
		}
		$quote_id = intval($_POST['id']);
		$condition_quote['quote_id'] = $quote_id;
		$now_quote = D('Domains_quote')->field(true)->where($condition_quote)->find();
		if(empty($now_quote)){
			exit(json_encode(array('error'=>1,'msg'=>'当前报价域名不存在')));
		}
		if($this->user_session['uid'] != $now_quote['buy_uid'] && $this->user_session['uid'] != $now_quote['sell_uid']){
			exit(json_encode(array('error'=>1,'msg'=>'您没有权限操作此报价单')));
		}


		switch(intval($_POST['type'])){
			case '1':
				if(($this->user_session['uid'] == $now_quote['buy_uid'] && $now_quote['last_offer'] == 1) || ($this->user_session['uid'] == $now_quote['sell_uid'] && $now_quote['last_offer'] == 0)){
					$dataQuote = array(
						'last_time'=>$_SERVER['REQUEST_TIME'],
						'last_offer' => $this->user_session['uid'] == $now_quote['buy_uid'] ? '0' : '1',
						'status' => '1',
					);
					if(D('Domains_quote')->where($condition_quote)->data($dataQuote)->save()){

						D('Domains')->where(array('domain_id'=>$now_quote['domain_id']))->data(array('status'=>'2'))->save();

						$dataOrder['addTime'] = $_SERVER['REQUEST_TIME'];
						$dataOrder['total_price'] = $now_quote['new_money'];
						$dataOrder['domain_id'] = $now_quote['domain_id'];
						$dataOrder['domainName'] = $now_quote['domain'];
						$dataOrder['trade_type'] = '0';
						$dataOrder['status'] = '0';
						$dataOrder['buy_id'] = $now_quote['buy_uid'];
						$dataOrder['sell_id'] = $now_quote['sell_uid'];
						$dataOrder['staff_id'] = '0';
						$dataOrder['payTye'] = '2';
						$dataOrder['yes_no'] = '3';
						$order_id = D('Order')->data($dataOrder)->add();

						$dataOrderInfo['order_id'] = $order_id;
						$dataOrderInfo['info'] = $this->user_session['uid'] == $now_quote['buy_uid'] ? '买家接受了交易，等待买家付款中' : '卖家接受了交易，等待买家付款中';
						$dataOrderInfo['time'] = $_SERVER['REQUEST_TIME'];
						D('Order_info')->data($dataOrderInfo)->add();
                                                exit(json_encode(array('error'=>0,'msg'=>'操作成功')));
					}else{
                                                exit(json_encode(array('error'=>1,'msg'=>'操作失败')));
					}
				}else{
                                        exit(json_encode(array('error'=>2,'msg'=>'您没有权限操作此报价单')));
				}
				break;
			case '2':
				$money = intval($_POST['money']);
				if($money <= $now_quote['new_money']){

					exit(json_encode(array('error'=>1,'msg'=>'新的报价不能低于老的报价')));
				}
				$dataQuote = array(
					'new_money'=>$money,
					'last_time'=>$_SERVER['REQUEST_TIME'],
					'invalid_time'=>$_SERVER['REQUEST_TIME'] + 604800,
					'last_offer' => $this->user_session['uid'] == $now_quote['buy_uid'] ? '0' : '1',
				);
				if(D('Domains_quote')->where($condition_quote)->data($dataQuote)->save()){
					$dataQuoteList['quote_id'] = $quote_id;
					$dataQuoteList['is_sell'] = $this->user_session['uid'] == $now_quote['buy_uid'] ? '0' : '1';
					$dataQuoteList['money'] = $money;
					$dataQuoteList['time'] = $_SERVER['REQUEST_TIME'];
					D('Domains_quote_list')->data($dataQuoteList)->add();

					exit(json_encode(array('error'=>0,'msg'=>'报价成功')));
				}else{
					exit(json_encode(array('error'=>1,'msg'=>'报价失败')));
				}
				break;
			case '3':
				$dataQuote = array(
					'status' => '2',
					'last_offer' => '1',
					'last_time'=>$_SERVER['REQUEST_TIME'],
				);
				if(D('Domains_quote')->where($condition_quote)->data($dataQuote)->save()){
					exit(json_encode(array('error'=>0,'msg'=>'取消成功')));
				}else{

					exit(json_encode(array('error'=>1,'msg'=>'取消失败')));
				}
				break;
		}
	}
        
        
        public function group(){
            $group_list = D('Domains_group')->field(true)->where(array('uid'=>$this->user_session['uid']))->order('`group_id` ASC')->select();
            $this->assign('group_list',$group_list);
            $this->display();
        }
        
        public function group_add(){
            if(IS_POST){
                if(empty($_POST['group_name'])) $this->error('域名分组名称不能为空');
                $_POST['uid'] = $this->user_session['uid'];
                if(D('Domains_group')->data($_POST)->add()){
                    exit(json_encode(array('error'=>0,'msg'=>'添加分组成功')));
                }else{
                    exit(json_encode(array('error'=>1,'msg'=>'添加失败，请重试')));
                }
            }else{
                $this->display();
            }
	}
        
        public function group_save(){
            if(IS_POST){
                if(empty($_POST['group_id'])) exit(json_encode(array('error'=>1,'msg'=>'请携带要修改的ID')));
                if(D('Domains_group')->where(array('group_id'=>$_POST['group_id'],'uid'=>$this->user_session['uid']))->save($_POST)){
                    $this->success('修改成功');
                }else{
                    $this->error('修改失败');
                }
            }else{
                $this->error('非法访问');
            }
            
        }
        
        public function group_del(){
            if(IS_POST){
                if(empty($_POST['id'])) exit(json_encode(array('error'=>1,'msg'=>'请携带要删除的ID')));
                if(D('Domains_group')->where(array('group_id'=>$_POST['id'],'uid'=>$this->user_session['uid']))->delete()){
                    exit(json_encode(array('error'=>0,'msg'=>'删除分组成功')));
                }else{
                    exit(json_encode(array('error'=>2,'msg'=>'删除失败，请重试')));
                }
            }else{
                exit(json_encode(array('error'=>3,'msg'=>'非法访问')));
            }
        }
        
        
        
        public function terminal(){
            if($_GET['status'] == 1){
                //推荐中
                $where =array('uid'=>  $this->user_session['uid'],'status'=>1);
                $this->assign('status_type',1);
            } elseif ($_GET['status'] == 2) {
                //推荐成功
                $where =array('uid'=>  $this->user_session['uid'],'status'=>3);
                $this->assign('status_type',2);
            } elseif ($_GET['status'] == 3) {
                //推荐失败
                $where =array('uid'=>  $this->user_session['uid'],'status'=>4);
                $this->assign('status_type',3);
            } else {
                //审核
                $where = array('uid'=>  $this->user_session['uid']);
                $this->assign('status_type',0);
            }
            $databases_terminal = M('Terminal');
            $list = $databases_terminal->where($where)->select();
            $this->assign('list',$list);
            $this->display();
        }
        
        public function terminal_save(){
            $databases_terminal = M('Terminal');
            $save_info = $databases_terminal->where(array("id"=>  intval($_POST['id']),"uid"=>  $this->user_session['uid']))->save($_POST);
            if($save_info){
                $this->success('修改成功');
            }  else {
                $this->error('修改失败');
            }
        }
        
        public function terminal_del(){
            $databases_terminal = M('Terminal');
            if($databases_terminal->where(array("id"=>  intval($_POST['id']),"uid"=>  $this->user_session['uid']))->delete()){
                exit(json_encode(array('error'=>0,'msg'=>'删除成功')));
            }  else {
                exit(json_encode(array('error'=>1,'msg'=>'删除失败')));
            }
        }
        
        public function mibiao(){
            $Db_user = M('User');
            $Db_mibiao = M('Mibiao');
            $userinfo = $Db_mibiao->where('`uid`='.$this->user_session['uid'])->find();
            if(empty($userinfo)){
                $userinfo = $Db_user->field('`uid`,`email`,`qq`,`phone`')->where('`uid`='.$this->user_session['uid'])->find();
            }
            $this->assign('uid',$this->user_session['uid']);
            $this->assign('mibiaoinfo',$userinfo);
            $this->display();

        }
        
        public function mibiaoupdate(){
            $Db_user = M('User');
            $Db_mibiao = M('Mibiao');
            if(IS_POST){
                $mid  =  intval($_POST['mid']);
                $_POST['uid'] = $this->user_session['uid'];

                    if($mid <= 0){
                            //add
                        if($Db_mibiao->add($_POST)){
                            $this->success('设置成功');exit;
                        }else{
                            $this->error('设置失败，请重新设置');exit;
                        }
                    }else{
                        //save
                        if($Db_mibiao->data($_POST)->save()){
                            $this->success('更新成功');exit;
                        }else{
                            $this->error('设置更新失败，请重新设置');exit;
                        }
                    }
            }
        }
   
}