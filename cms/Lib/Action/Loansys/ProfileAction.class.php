<?php

/**
 * Class ProfileAction
 */

class ProfileAction extends BaseAction{
	protected function _initialize(){
		parent::_initialize();
		if(empty($this->user_session)){
			redirect(U('Login/index'));
		}
		$user_session = D('User')->get_user($this->user_session['uid']);
		$user_session['last'] = $this->user_session['last'];
		$this->user_session = $user_session;
		$this->assign('user_session',$this->user_session);

	}
	//个人信息
	public function update(){

		if(IS_POST){

			$Db_user = D('User');
			$updata['birthday'] = trim($_POST['birthday']);
			$updata['youaddress'] = trim($_POST['youaddress']);
			$updata['sex']      = intval($_POST['sex']);
			$updata['uid']      = $this->user_session['uid'];
			if($Db_user->data($updata)->save()){
				$this->success('信息更新成功！');exit;
			}else{
				$this->error('信息更新失败！请重试~');exit;
			}
		}
		$this->display();
	}

	//头像上传
	public function avatar(){
		$Db_user = D('User');
		//$avatar =  $Db_user->field('')->where()->find();
		if(IS_POST){
			$image = D('Image')->handle($this->system_session['id'], 'avatar', 0, array('size' => 10), false);

			if (!$image['error']) {
				$_POST = array_merge($_POST, str_replace('/upload/avatar/', '', $image['url']));
			} else {
				$this->error( $image['msg']);exit;
				//exit(json_encode(array('msg'=>$image['msg'])));
			}
			$_POST['uid']       =  $this->user_session['uid'];
			// 不允许多次提交

			if($id = $Db_user->data($_POST)->save()){
				D('Image')->update_table_id('/upload/avatar/' . $_POST['avatar'], $id, 'avatar');
				unset($_POST);
				$_POST =null;
				$this->success('头像上传成功！');exit;
			}else{
				$this->error('头像上传失败！请重试~');exit;
			}

		}
		$this->display();
	}

	//身份证认真
	public function IdCard(){
		$Db_id_card  = D('Id_card');
		$condition_id = array('uid'=>$this->user_session['uid']);
		$result_idcard = $Db_id_card->field('`card_id`,`status`')->where($condition_id)->find();

		if(IS_POST){

			$image = D('Image')->handle($this->system_session['id'], 'idcard', 0, array('size' => 10), false);

			if (!$image['error']) {
				$_POST = array_merge($_POST, str_replace('/upload/idcard/', '', $image['url']));
			} else {
				$this->error( $image['msg']);exit;
			}

			$_POST['pull_time'] = $_SERVER['REQUEST_TIME'];
			$_POST['uid']       =  $this->user_session['uid'];
			$_POST['id_number'] =  trim($_POST['id_number']);

			// 不允许多次提交

			if($id = $Db_id_card->data($_POST)->add()){
				D('Image')->update_table_id('/upload/idcard/' . $_POST['facadeFile'], $id, 'idcard');
				D('Image')->update_table_id('/upload/idcard/' . $_POST['reverseSideFile'], $id, 'idcard');
				unset($_POST);
				$_POST =null;
				$this->success('身份信息提交成功，请耐心等待审核！');exit;
			}else{
				$this->error('身份信息提交失败！请重试~');exit;
			}

		}

		$this->assign('result_idcard',$result_idcard);
		$this->display();
	}

	//交易密码
	public function transactionPassword(){
		$istp = intval($_GET['istp']) ? intval($_GET['istp']) : 0;
		$this->assign('istp',$istp);
		if(IS_POST){
			$Password = trim($_POST['Password']);
			$NewTransactionPassword = trim($_POST['NewTransactionPassword']);
			$NewTransactionPasswordAgain = trim($_POST['NewTransactionPasswordAgain']);
			if(empty($Password) || empty($NewTransactionPassword) || empty($NewTransactionPasswordAgain)){
				$this->error('登录密码和交易密码必须填写！');exit;
			}
			if($NewTransactionPassword != $NewTransactionPasswordAgain){
				$this->error('两次交易密码不一致！');exit;
			}
			$Db_user = D('User');
			$pwd = md5($Password);
			$condition_user['pwd'] = $pwd;
			$condition_user['uid'] = $this->user_session['uid'];
			$now_user = $Db_user->field(true)->where($condition_user)->find();

			if(empty($now_user)){
				$this->error('登录密码错误！');exit;
			}
			$Db_loan_trade_pass = D('Loan_trade_pass');
			$trade_pass = $Db_loan_trade_pass->where('`uid`='.$this->user_session['uid'])->find();

			if($istp == 1){
				$OldTransactionPassword = trim($_POST['OldTransactionPassword']);
				if(empty($OldTransactionPassword)){
					$this->error('原交易密码必须填写！');exit;
				}
				$change_cond['uid'] = $this->user_session['uid'];
				$change_cond['trade_password'] = md5($OldTransactionPassword);
				$trade_pass_change = $Db_loan_trade_pass->where($change_cond)->find();

				//修改密码
				if(!empty($trade_pass_change)){
					$pass_add['trade_password'] =md5($NewTransactionPassword);
					$pass_add['uid'] = $this->user_session['uid'];
					$Db_loan_trade_pass->save($pass_add);
					$this->success('交易密码修改成功！');exit;
				}else{
					$this->error('原交易密码错误！');exit;
				}
			}else{
				//第一次设置密码.
				if(empty($trade_pass)){
					$pass_add['trade_password'] = md5($NewTransactionPassword);
					$pass_add['status']  = 1;
					$pass_add['uid'] = $this->user_session['uid'];

					$Db_loan_trade_pass->add($pass_add);
					$this->success('交易密码设置成功！');exit;
   			    }else{
					$pass_save['trade_password'] = md5($NewTransactionPassword);
					$pass_save['uid'] = $this->user_session['uid'];
					$Db_loan_trade_pass->where('`uid`='.$this->user_session['uid'])->data($pass_save)->save();
					$this->success('交易密码修改成功！');exit;
				}
			}
		}
		$this->display();
	}
}