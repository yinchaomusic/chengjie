<?php
/*
 * 新闻
 *
 */
class NewsAction extends BaseAction {

    protected function _initialize()
    {
        parent::_initialize();
	    $cat_key = trim($_GET['cat_key']) ? trim($_GET['cat_key']) : 'Loansys_help_cj';
	    $this->assign('cat_key',$cat_key);


       /*
        //左侧菜单【帮助中心】==
        $Db_news_category = M('News_category');
        $cate_news_conditioin_f['cat_key']  = 'user_guide';
        $cate_news_conditioin_f['cat_state']    = 1;
        $cate_guide_left =   $Db_news_category->field(true)->where($cate_news_conditioin_f)->find();
        $Db_news_left = M('News');
        $left_news_condition['state'] = 1;
        $left_news_condition['cat_id'] = $cate_guide_left['cat_id'];
        $left_guide_list =  $Db_news_left->field('`news_id`,`news_title`')->where($left_news_condition)->select();


        $this->assign('left_guide_list',$left_guide_list);
        if( ACTION_NAME == 'guide'){
            $this->assign('cate_guide_left',$cate_guide_left);
        }
	    */
    }
    public function index(){
        $news_cate_condition['cat_key'] = trim($_GET['cat_key']);
        $Db_News          = M('News');
        $Db_news_category = M('News_category');
      //  $news_cate_condition['cat_key'] = 'index_news_top1';
        $cate_list = $Db_news_category->field('`cat_id`,`cat_key`,`cat_name`')->where($news_cate_condition)->find();

        $news_list_condition['cat_id'] = $cate_list['cat_id'];
        $news_list_condition['state'] = 1;

        $order_list_count = $Db_News->where($news_list_condition)->count();
        import('@.ORG.system_page');
        $p = new Page($order_list_count, 20);
        $news_list = $Db_News->where($news_list_condition)
                              ->order('`add_time` DESC,`news_id` ASC ')
                                ->limit($p->firstRow . ',' .$p->listRows)
                                ->select();

        $pagebar = $p->show();
        $this->assign('pagebar', $pagebar);

        $this->assign('cate_list',$cate_list);
        $this->assign('news_list',$news_list);

        $this->display();
    }

    public function contact(){
	    $page = isset($_GET['page']) ? trim($_GET['page']) : '' ;
	    $Db_about = M('About');
	    $condition_about['state'] = 1;
	    $condition_about['page'] = 'Loansys_contact';
	    $aoubt_list = $Db_about->field(true)->where($condition_about)->find();

	    $this->assign("show_title",'联系我们');

        $this->assign('cat_key','contact');
	    $this->assign('aoubt_list',$aoubt_list);
        $this->display();
    }

    public function show(){
        $news_condition['news_id'] = intval(trim($_GET['news_id']));
        $news_condition['state'] = 1;
        $Db_News          = M('News');
        $Db_news_category = M('News_category');
        $news_show = $Db_News->field(true)->where($news_condition)->find();
        $cate  = $Db_news_category->field('`cat_id`,`cat_key`,`cat_name`')->where('`cat_id`='.$news_show['cat_id'])->find();
        //$cat_key  = $Db_news_category->field('`cat_id`,`cat_key`,`cat_name`')->where('`cat_id`='.$news_show['cat_id'])->getField('cat_key');

        /*
				if(empty($news_show)){
					$this->assign('nocontents',1);
				}

				if( $news_condition['news_id'] > 0 ){
					$pre  = $news_condition['news_id'] - 1;
					$later =  $news_condition['news_id'] + 1;
					$news_pre_show = $Db_News->field('`news_id`,`news_title`,`cat_id`')->where('`news_id` = '.$pre)->find();
					if(empty($news_pre_show)){
						$this->assign('pre',1);
					}
					$news_later_show = $Db_News->field('`news_id`,`news_title`,`cat_id`')->where('`news_id` = '.$later)->find();
					if(empty($news_later_show)){
						$this->assign('later',1);
					}
					$this->assign('news_pre_show',$news_pre_show);
					$this->assign('news_later_show',$news_later_show);

				}*/
        $this->assign('cat_key',$cate['cat_key']);

        $this->assign('cate',$cate);
        //dump($news_show);
        $this->assign('news_show',$news_show);



        $this->display();
    }

    public function links(){
        $Db_flink = M('Flink');
        $condition['status'] = 1;
        $count =$Db_flink->where($condition)->count();
        import('@.ORG.system_page');
        $p = new Page($count, 103);
        $list = $Db_flink->field(true)->where($condition)->order('`sort` DESC,id DESC')->limit($p->firstRow.','.$p->listRows)->select();

        $pagebar = $p->show();
        $this->assign('link_list',$list);
        //$this->assign('keyWord', $keyWord);
        $this->assign('count', $count);
        $this->assign('pagebar',$pagebar);
        $this->display();
    }

    public function aboutus(){
        $page = isset($_GET['page']) ? trim($_GET['page']) : '' ;
        $Db_about = M('About');
        $condition_about['state'] = 1;

	    //显示关于我们
	    $condition_about['page'] = 'Loansys_about';
	    $aoubt_list = $Db_about->field(true)->where($condition_about)->find();
        $this->assign('cat_key','aboutus');
        $this->assign("show_title",'关于我们');
        $this->assign('aoubt_list',$aoubt_list);

        $this->display();
    }

//    //交易规则
//    Public function traderule(){
//        $rid = isset($_GET['rid']) ? intval($_GET['rid']) : 0 ;
//        if($rid > 0){
//            $Db_domain_traderule = M('Domain_traderule');
//            $condition_rult['rid'] = $rid;
//            $condition_rult['status'] = 1;
//            $traderule_list = $Db_domain_traderule->field('`title`,`content`')->where($condition_rult)->find();
//        }else{
//            $this->error("找不到该记录",'/');
//        }
//        $this->assign('aoubt_list',$traderule_list);
//        $this->display('aboutus');
//    }

    public function guide(){
        $news_id = isset($_GET['news_id']) ? intval($_GET['news_id']) : 0;
        $Db_news = M('News');
        if($news_id > 0){
           $news_list_condition['news_id'] = $news_id;
           $news_list_condition['state'] = 1;
           $get_guide_list = $Db_news->field(true)->where($news_list_condition)->find();

           $this->assign('get_guide_list',$get_guide_list);
        }
        $this->display();

    }


}