<?php

class FundAction extends BaseAction{
	protected function _initialize(){
		parent::_initialize();
		if(empty($this->user_session)){
			redirect(U('Login/index'));
		}
		$user_session = D('User')->get_user($this->user_session['uid']);
		$user_session['last'] = $this->user_session['last'];
		$this->user_session = $user_session;
		$this->assign('user_session',$this->user_session);
	}
	public function account(){
		$pay_method = D('Config')->get_pay_method($notOnline,1,true);
		$this->assign('pay_method', $pay_method);
		$this->display();
	}

	public function recharge_save(){
		$data_user_recharge_order['uid'] = $this->user_session['uid'];
		$money = floatval($_GET['money']);
		if(empty($money) || $money > 200000){
			$this->error('请输入有效的金额！最高不能超过20万元。');
		}
		$data_user_recharge_order['money'] = $money;
		$data_user_recharge_order['add_time'] = $_SERVER['REQUEST_TIME'];
		if($order_id = D('User_recharge_order')->data($data_user_recharge_order)->add()){
			redirect(U('Index/Pay/go_pay',array('order_id'=>$order_id,'order_type'=>'recharge','pay_type'=>$_GET['pay_type'])));
		}
	}

	public function withdrawal(){
		$where['uid'] =  $this->user_session['uid'];
		$databases_bank_card = M('Bank_card');
		$databases_user = M('User');
		$bank_card_list = $databases_bank_card->where($where)->select();
		$user_info = $databases_user->where(array('uid'=>$this->user_session['uid']))->find();
		$this->assign('bank_card_list',$bank_card_list);

		$this->assign('user_info',$user_info);


			$uid=  $this->user_session['uid'];

			$count = M()->table(array(C('DB_PREFIX').'user_deal_record'=>'ud',C('DB_PREFIX').'bank_card'=>'bc'))
				->where('ud.uid='.$uid.' AND ud.bank_card_id = bc.id ')->order('ud.id desc' )->count();

			import('@.ORG.system_page');
			$p = new Page($count, 12);
			$wr_list = M()->table(array(C('DB_PREFIX').'user_deal_record'=>'ud',C('DB_PREFIX').'bank_card'=>'bc'))
				->field('ud.txtmoeny,ud.addtime,ud.state,ud.bank_card_id,bc.account,ud.decMenory,bc.bankName,bc.account')
				->where('ud.uid='.$uid.' AND ud.bank_card_id = bc.id ')->limit($p->firstRow . ',' . $p->listRows)->order('ud.id desc' )->select();

			$this->assign('wr_list',$wr_list);
			$pagebar = $p->show();
			$this->assign('pagebar', $pagebar);
			$this->display();
	}

	public function withdrawal_data(){
		if($_POST){

			$TransactionPassword = trim($_POST['TransactionPassword']);
			if(empty($TransactionPassword)){
				$return = array('error'=>-1,'title'=>'交易密码错误','msg'=>'请填写正确的交易密码！');
				$this->ajaxReturn($return);exit;
			}
			$Db_loan_trade_pass = D('Loan_trade_pass');
			$pwd = md5($TransactionPassword);
			$condition_user['trade_password'] = $pwd;
			$condition_user['uid'] = $this->user_session['uid'];
			$loan_trade_pass = $Db_loan_trade_pass->where($condition_user)->find();
			if(empty($loan_trade_pass)){
				$return = array('error'=>-1,'title'=>'交易密码错误','msg'=>'请填写正确的交易密码！');
				$this->ajaxReturn($return);exit;
			}
			//file_put_contents(time().'xxxxxxxx',print_r($_POST,true));

			$Db_user = M('User');
			$Db_User_deal_record = M('User_deal_record');
			//{'getAmount':getAmount,'TransactionPassword':TransactionPassword,'bankid':bankid}
			$deal_record['uid'] = $this->user_session['uid'] ;
			$deal_record['bank_card_id'] = intval($_POST['bankid']);
			$deal_record['txtmoeny'] =  $_POST['getAmount'];
			$deal_record['addtime'] = time();
			$deal_record['state'] = 0;

			$condition['uid'] = $this->user_session['uid'];
			$_SESSION['user']['now_money'] -=$_POST['getAmount'];

			$info = $Db_User_deal_record->add($deal_record);
			if($info){
				if($Db_user->where($condition)->setDec('now_money',$_POST['getAmount'])){
					$return = array('error'=>0,'title'=>'申请提现成功','msg'=>'申请提现成功，请耐心等待审核！');
					$this->ajaxReturn($return);
					//$this->success('申请提现成功');
				}  else {
					$return = array('error'=>-1,'title'=>'申请提现失败','msg'=>'请您重新操作一下！');
					$this->ajaxReturn($return);
				}
			}
		}
	}


	//提现银行
	public function bankcard(){
		$databases_bank_card = M('Bank_card');
		$condition['uid'] =  $this->user_session['uid'];
		$bankList = $databases_bank_card->where($condition)->select();
		$this->assign('bankList',$bankList);
		$this->display();
	}

	public function addBankCard(){
		if(IS_POST){
			//file_put_contents('xxx.log',print_r($_POST,true));
			$bank_data['uid'] = $this->user_session['uid'];
			$bank_data['nickname'] = $this->user_session['nickname'];
			$bank_data['city'] = $_POST['city'];
			$bank_data['bankName'] = $_POST['BankName'];
			$bank_data['subBank'] = $_POST['BranchName'];
			$bank_data['account'] = $_POST['CardNumber'];
			$bank_data['state'] = 0;
			$bank_data['addtime'] = time();

			$databases_bank_card = M('Bank_card');

			if($databases_bank_card->add($bank_data)){
				exit(json_encode(array('error'=>0,'title'=>'银行卡信息添加成功','msg'=>'您已经成功添加一张银行卡信息')));
			}  else {
				exit(json_encode(array('error'=>1,'title'=>'银行卡信息添加失败','msg'=>'添加失败，请重新添加')));
			}
		}
		$this->display();
	}

	public function bankdel(){
		if($this->isAjax()) {
			$_POST['id'] = intval($_POST['id']) ? intval($_POST['id']) : 0;
			$type = intval($_POST['type']);
			if ($_POST['id'] > 0 &&  $type == 1) {
				$Db_Bank_card = D('Bank_card');
				// 7 审核失败的才可以删除,
				if ($Db_Bank_card->where(array('id' => intval($_POST['id']), 'uid' => $this->user_session['uid']))->delete()) {
					exit(json_encode(array('error' => 0, 'msg' => '删除成功', 'isreload' => 0)));
				} else {
					exit(json_encode(array('error' => 1, 'msg' => '删除失败', 'isreload' => 0)));
				}
			}
		}

	}

	public function showdetail(){
		if(IS_POST){
			$id = intval($_POST['id']) ?  intval($_POST['id']) : 0;
			if($id > 0){
				$condition['uid'] =  $this->user_session['uid'];
				$condition['id'] = $id;
				$Db_Bank_card = D('Bank_card');
				$breachsList = $Db_Bank_card->where($condition)->select();
				if(empty($breachsList)){
//					$return = array('error'=>-1,'title'=>'获取信息失败','msg'=>'系统无法找到该条记录！');
//					$this->ajaxReturn($return);
					exit(json_encode(array('error'=>-1,'title'=>'获取信息失败','msg'=>'系统无法找到该条记录！')));
				}
				$table = '';
				$table .= '<div class="table-responsive">
					 	<table class=" table table-bordered table-striped table-hover">
							<thead>
						<tr>
							<th>开户人</th>
							<th>开户行</th>
							<th>支行</th>
							<th>账号</th>
							<th>地址</th>
							<th>绑定日期</th>

						</tr>
						</thead>
						<tbody class="text-center">';

				foreach($breachsList as $val){
					$date = date('Y-m-d',$val['addtime']);
					$table .= "<tr>
							<td>{$val['nickname']}</td>
							<td>{$val['bankName']}</td>
							<td>{$val['subBank']}</td>
							<td>{$val['account']}</td>
							<td>{$val['city']}</td>
							<td>{$date}</td>

							</tr>";
				}


				$table .= '</tbody></table></div>';

				exit(json_encode(array('error'=>0,'title'=>'体现银行卡详细信息列表','msg'=>'成功','htmldata'=>$table)));
			}else{

				exit(json_encode(array('error'=>-1,'title'=>'获取信息失败','msg'=>'系统无法找到该条记录！')));
			}
		}
	}

	//资金记录
	public function record(){
		$Db_user_money_list = D('User_money_list');

		$conditions['uid'] = $this->user_session['uid'];
		$conditions['typeofs'] = 1;
		$start = trim($_GET['start']);
		$end = trim($_GET['end']);
		$purpose = trim($_GET['purpose']);
		if(!empty($start) && !empty($end)){
			//开始日期-结束日期
			$start = strtotime($start);
			$end  = strtotime($end);
			$conditions['time'] = array('between',array($start,$end));
		}
		if(!empty($purpose)){
			$purpose = explode(',',$purpose);
			$purpose  = implode(',',$purpose);
			$conditions['type'] = array('in',$purpose);
		}
		$user_money_list = $Db_user_money_list->where($conditions)->order('`time` DESC')->select();
		//dump($user_money_list);
		$this->assign('user_money_list',$user_money_list);
		$this->display();
	}

	//冻结记录
	public function freezes(){
		$Db_user_money_list = M('User_freeze');
		$condition['typeofs'] = 1;
		$condition['uid'] = $this->user_session['uid'];
		$user_money_list = $Db_user_money_list->where($condition)->select();
		$this->assign('user_money_list',$user_money_list);
		$this->display();
	}
}