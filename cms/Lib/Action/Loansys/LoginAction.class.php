<?php
/*
 * 用户登录
 *
 */

class LoginAction extends BaseAction{
    public function index(){
		if($this->user_session){
			redirect(U('Financing/index'));
		}else{
			$this->display();
		}
    }

    public function reg(){
        $this->display();
    }
    public function check(){
        if($this->isAjax()){
            $database_user = D('User');
			$account = trim($_POST['account']);
			if(preg_match('/^\d+$/',$account)){
				$condition_user['phone'] = $account;
			}else{
				$condition_user['email'] = $account;
			}
            $now_user = $database_user->field(true)->where($condition_user)->find();
            if(empty($now_user)){
                exit(json_encode(array('error'=>'2','msg'=>'用户名不存在！','dom_id'=>'account')));
            }
            $pwd = md5($_POST['pwd']);
            if($pwd != $now_user['pwd']){
                exit(json_encode(array('error'=>'3','msg'=>'密码错误！','dom_id'=>'pwd')));
            }
            if($now_user['status'] == 0){
                exit(json_encode(array('error'=>'4','msg'=>'您被禁止登录！请联系工作人员获得详细帮助。','dom_id'=>'account')));
            }else if($now_user['status'] == 2){
                exit(json_encode(array('error'=>'5','msg'=>'您的帐号正在审核中，请耐心等待或联系工作人员审核。','dom_id'=>'account')));
            }elseif($now_user['is_check_email'] == 0){
                exit(json_encode(array('error'=>'6','msg'=>"您的邮箱尚未验证，请您及时登录邮箱【 {$now_user['email']} 】确认注册邮件。",'uuid'=>$now_user['uid'],'dom_id'=>'account')));
            }

//            $user_level_info = D('Level')->user_Level($now_user['score_count']);
//            $now_user['lname']=$user_level_info['lname'];
//            $now_user['boon']=$user_level_info['boon'];

            $data_user['uid'] = $now_user['uid'];
            $data_user['last_ip'] = get_client_ip(1);
            $data_user['last_time'] = $_SERVER['REQUEST_TIME'];

            $Db_loginlog = M('Loginlog');

            if($database_user->data($data_user)->save()){
				if(!empty($now_user['last_ip'])){
					import('ORG.Net.IpLocation');
					$IpLocation = new IpLocation();
					$last_location = $IpLocation->getlocation(long2ip($now_user['last_ip']));
					$now_user['last']['country'] = iconv('GBK','UTF-8',$last_location['country']);
					$now_user['last']['area'] = iconv('GBK','UTF-8',$last_location['area']);
				}
                $databases_user_group = M('User_group');
                $group_info = $databases_user_group->where(array('group_id'=>$now_user['group_id']))->find();
                $now_user['permissions'] = explode(',',$group_info['permissions_id']);
                session('user',$now_user);
                //登录日志
                $IpLocation = new IpLocation();
                $log_location = $IpLocation->getlocation(long2ip(get_client_ip(1)));
                $log_user['last']['country'] = iconv('GBK','UTF-8',$log_location['country']);
                $log_user['last']['area'] = iconv('GBK','UTF-8',$log_location['area']);
                $log_data['uid'] = $now_user['uid'];
                $log_data['log_ip'] = get_client_ip(1);
                $log_data['log_location'] =$log_user['last']['country'] . $log_user['last']['area'];
                $log_data['log_status'] = 1;
                $log_data['log_time'] = time();
                $Db_loginlog->data($log_data)->add();
                exit(json_encode(array('error'=>'0','msg'=>'登录成功,现在跳转~','dom_id'=>'account')));
            }else{
                //登录日志失败
                exit(json_encode(array('error'=>'6','msg'=>'登录信息保存失败,请重试！','dom_id'=>'account')));
            }
        }else{
            exit('deney Access !');
        }
    }
    public function reg_check(){
        if($this->isAjax()){
            if(md5($_POST['verify']) != $_SESSION['domain_reg_verify']){
                exit(json_encode(array('error'=>'1','msg'=>'验证码不正确！','dom_id'=>'verify')));
            }
            $database_user = D('User');
            //姓名，-- 防止 重复姓名的
//            $nickname['nickname'] = trim($_POST['nickname']);
//            $now_name = $database_user->field('`uid`')->field(true)->where($nickname)->find();
//            if(!empty($now_name)){
//                exit(json_encode(array('error'=>'3','msg'=>'该姓名已经存在！','dom_id'=>'email')));
//            }

            //邮箱
            $email['email'] = trim($_POST['email']);
            $now_email = $database_user->field('`uid`')->field(true)->where($email)->find();
            if(!empty($now_email)){
                exit(json_encode(array('error'=>'4','msg'=>'邮箱已经存在！','dom_id'=>'email')));
            }

            //手机号
            $phone['phone'] =trim($_POST['phone']);
            $now_phone = $database_user->field('`uid`')->field(true)->where($phone)->find();
            file_put_contents('phone.log',$database_user->getLastSql());
            if(!empty($now_phone)){
                exit(json_encode(array('error'=>'5','msg'=>'手机号已经存在！','dom_id'=>'phone')));
            }

            $config = D('Config')->get_config();
            $this->assign('config',$config);

            $_POST['uid'] = null;
            if($config['merchant_verify']){
                $_POST['status'] = 2;
            }else{
                $_POST['status'] = 1;
            }

            $_POST['pwd'] = md5($_POST['pwd']);
            $_POST['reg_ip'] = get_client_ip(1);
            $_POST['reg_time'] = $_SERVER['REQUEST_TIME'];
            $_POST['login_count'] = 0;
            $_POST['reg_from'] = 0;
            $_POST['email_code'] =  sha1(md5(time().'reg_user'));
            $_POST['is_check_email'] = 0;
            $_POST['add_time'] = time();


            if($insert_id=$database_user->data($_POST)->add()){
                //我的米表信息录入
                $mibiao['uid'] = $insert_id;
                $mibiao['email'] = trim($_POST['email']);
                $mibiao['qq'] = trim($_POST['qq']);
                $mibiao['phone'] = trim($_POST['phone']);
                M('Mibiao')->data($mibiao)->add();
               // M('Merchant_score')->add(array('parent_id'=>$insert_id,'type'=>1));
//                if($config['merchant_verify']){
//                    exit(json_encode(array('error'=>'0','msg'=>'注册成功,请耐心等待审核或联系工作人员审核。~','dom_id'=>'account')));
//                }else{


                print json_encode(array('error'=>'0','title'=>"恭喜您注册成功",'msg'=>'注册成功,请先验证邮箱【'.$mibiao['email'].'】才可以登录，如果找不到邮箱，请检查是否在垃圾邮箱里。正在跳转登录页~','dom_id'=>'account'));
               // exit(json_encode(array('error'=>'0','title'=>"恭喜您注册成功",'msg'=>'注册成功,正在跳转登录页~','dom_id'=>'account')));
               // }

        import('@.ORG.smtp');
        $mail = new smtp();
        $whoisemail_info['email'] =trim($_POST['email']);
        $mail->setServer($this->config['mail_config_smtp_host'],
            $this->config['mail_config_smtp_user'],
            $this->config['mail_config_smtp_pwd'],
            $this->config['mail_config_smtp_port'],
            $this->config['mail_config_smtp_auth']);
        $mail->setFrom($this->config['mail_config_sender']); //设置发件人
        $mail->setReceiver($whoisemail_info['email']); //设置收件人，多个收件人，调用多次
        $hosts = $this->top_domain($this->config['site_url']);
        $mail_title = "{$this->config['site_name']}({$hosts})：会员注册确认信";
//        $data['email_code'] =  sha1(md5(time().'reg_user'));
//        $data['uid'] = $insert_id;
//        $database_user->data($data)->save();
		$send_url =  $this->config['site_url'].'/yzregemail/'.$_POST['email_code'].'/'.$insert_id;
        $mail_body = <<<Eof

尊敬的用户: <br/>
您好, <br/>
欢迎注册成为{$this->config['site_name']}({$hosts})！<br/>

为了保证每个会员的电子邮件真实性，您还需要最后一个步骤才能完成绑定，请点击下面的链接地址进行确认：<br/>
 <a href="{$send_url}"> <font color='red' size='2'>{$send_url}</font> </a> <br/>
(如果链接地址无法直接点击，请将上面的链接地址拷贝到浏览器的地址栏进行激活)<br/>
请注意：会员帐户只有激活后才能使用！<br/>
如果您没有注册成为我们会员，那可能是输入错误，该邮件为系统自动发送邮件，请删除该邮件，为您带来的不便表示歉意。<br/>


如果您有什么疑问或建议，欢迎给我们提供，客服邮箱：{$this->config['site_email']}<br/>

感谢您对{$this->config['site_name']}({$hosts})的支持!<br/>

{$this->config['site_name']}({$hosts})客服部<br/>
{$this->config['site_url']}<br/>
联系电话：+86  {$this->config['site_phone']}<br/>
客服邮箱: {$this->config['site_email']}

Eof;
        $mail->setMail($mail_title , $mail_body); //设置邮件主题、内容
        $mail->sendMail(); //发送

            }else{
                exit(json_encode(array('error'=>'6','title'=>"抱歉，注册失败",'msg'=>'注册失败,请重试！','dom_id'=>'account')));
            }
        }else{
            exit('deney Access !');
        }
    }

    public function resetmail(){
        if($this->isAjax()){
            if(intval($_POST['uuid']) <= 0){
                exit(json_encode(array('error'=>'1','msg'=>'uuid非法！')));
            }
            $database_user = D('User');
            $cons['uid'] = intval($_POST['uuid']);
            $now_phone = $database_user->field(true)->where($cons)->find();

            if(empty($now_phone)){
                exit(json_encode(array('error'=>'5','msg'=>'没有相关数据！','dom_id'=>'phone')));
            }
            $config = D('Config')->get_config();
            $this->assign('config',$config);
                $_emailcode['email_code'] =  sha1(md5(time().'reg_user'));
                $_emailcode['uid']      =  intval($_POST['uuid']);
              if($database_user->data($_emailcode)->save()){
                  echo json_encode(array('error'=>'0','title'=>"恭喜您发送成功",'msg'=>'注册验证码重新成功,请先验证邮箱【'.$now_phone['email'].'】才可以登录，如果找不到邮箱，请检查是否在垃圾邮箱里。'));
                  import('@.ORG.smtp');
                  $mail = new smtp();
                  $whoisemail_info['email'] =$now_phone['email'];
                  $mail->setServer($this->config['mail_config_smtp_host'],
                      $this->config['mail_config_smtp_user'],
                      $this->config['mail_config_smtp_pwd'],
                      $this->config['mail_config_smtp_port'],
                      $this->config['mail_config_smtp_auth']);
                  $mail->setFrom($this->config['mail_config_sender']); //设置发件人
                  $mail->setReceiver($whoisemail_info['email']); //设置收件人，多个收件人，调用多次
                  $hosts = $this->top_domain($this->config['site_url']);
                  $mail_title = "{$this->config['site_name']}({$hosts})：会员注册确认信";
                  $send_url =  $this->config['site_url'].'/yzregemail/'.$_emailcode['email_code'].'/'.$now_phone['uid'];
                  $mail_body = <<<Eof

尊敬的用户: <br/>
您好, <br/>
欢迎注册成为{$this->config['site_name']}({$hosts})！<br/>

为了保证每个会员的电子邮件真实性，您还需要最后一个步骤才能完成绑定，请点击下面的链接地址进行确认：<br/>
 <a href="{$send_url}"> <font color='red' size='2'>{$send_url}</font> </a> <br/>
(如果链接地址无法直接点击，请将上面的链接地址拷贝到浏览器的地址栏进行激活)<br/>
请注意：会员帐户只有激活后才能使用！<br/>
如果您没有注册成为我们会员，那可能是输入错误，该邮件为系统自动发送邮件，请删除该邮件，为您带来的不便表示歉意。<br/>


如果您有什么疑问或建议，欢迎给我们提供，客服邮箱：{$this->config['site_email']}<br/>

感谢您对{$this->config['site_name']}({$hosts})的支持!<br/>

{$this->config['site_name']}({$hosts})客服部<br/>
{$this->config['site_url']}<br/>
联系电话：+86  {$this->config['site_phone']}<br/>
客服邮箱: {$this->config['site_email']}

Eof;
                  $mail->setMail($mail_title , $mail_body); //设置邮件主题、内容
                  $mail->sendMail(); //发送

              }else{
                  exit(json_encode(array('error'=>'7','title'=>"抱歉，重新发送失败",'msg'=>'重新发送失败,请重试！')));
              }


        }else{
            exit('deney Access !');
        }
    }

    public function yzemail(){
        if($_GET){
            $database_user = D('User');
            $condition['email_code'] = strval($_GET['email_code']);
            $condition['uid'] = intval($_GET['check_id']);
            $get_check_data =  $database_user->field('`uid`,`is_check_email`,`email_code`,`email`')->where($condition)->find();

            if(!empty($get_check_data)){
                $data['is_check_email'] = 1;
                $data['email_code'] = '';
                $data['uid'] = $condition['uid'];
                $database_user->data($data)->save();
                $whoisemail['uid'] = $get_check_data['uid'];
                $whoisemail['email'] = $get_check_data['email'];
                $whoisemail['is_check'] = 1;
                D('Domain_whoisemail')->add($whoisemail);

                $this->assign('jumpUrl',U('Login/index'));
                $this->success('验证成功。');
            }else{
                $this->assign('jumpUrl',U('Login/reg'));
                $this->success('该链接已失效。');
            }
        }
    }

    public function logout(){
        session('user',null);
        header('Location: '.U('Login/index'));
    }
    public function verify(){
        $verify_type = $_GET['type'];
        if(empty($verify_type)){exit;}
        import('ORG.Util.Image');
        Image::buildImageVerify(4,1,'jpeg',53,26,'domain_'.$verify_type.'_verify');
    }

    public function findpsw(){

        $this->display();
    }


    public function first_check(){
        $_SESSION['find_the_password_for_email'] = null;
        if($this->isAjax()){
            if(md5($_POST['verify']) != $_SESSION['domain_findpass_verify']){
                exit(json_encode(array('error'=>'1','msg'=>'验证码不正确！')));
            }
            $database_user = D('User');

            //邮箱
            $email['email'] = trim($_POST['email']);
            $now_email = $database_user->field('`uid`')->field(true)->where($email)->find();
            if(empty($now_email)){
                exit(json_encode(array('error'=>'4','msg'=>'邮箱不存在！')));
            }else{
                //发送 邮箱验证码
                $config = D('Config')->get_config();
                $this->assign('config',$config);

                $_POST['email'] = trim($_POST['email']);
                $_POST['uid'] = $now_email['uid'];
                $_POST['find_pass_code'] =  mt_rand(100000,999999);
                if($insert_id=$database_user->data($_POST)->save()){

                    $_SESSION['find_the_password_for_email'] = $_POST['email'];
                    echo json_encode(array('error'=>'0','title'=>"恭喜",'msg'=>'邮件成功,请及时登录邮箱验证~'));

                    import('@.ORG.smtp');
                    $mail = new smtp();
                    $whoisemail_info['email'] =trim($_POST['email']);
                    $mail->setServer($this->config['mail_config_smtp_host'],
                        $this->config['mail_config_smtp_user'],
                        $this->config['mail_config_smtp_pwd'],
                        $this->config['mail_config_smtp_port'],
                        $this->config['mail_config_smtp_auth']);
                    $mail->setFrom($this->config['mail_config_sender']); //设置发件人
                    $mail->setReceiver($whoisemail_info['email']); //设置收件人，多个收件人，调用多次
                    $hosts = $this->top_domain($this->config['site_url']);
                    $mail_title = "{$this->config['site_name']}({$hosts})：验证码发送通知";


                    $mail_body = <<<Eof

尊敬的用户: <br/>
您好, <br/>
您的验证码【 <font color=red>{$_POST['find_pass_code']} </font>】,请勿泄露给他人！<br/>

如果您有什么疑问或建议，欢迎给我们提供，客服邮箱：{$this->config['site_email']}<br/>

感谢您对{$this->config['site_name']}({$hosts})的支持!<br/>

{$this->config['site_name']}({$hosts})客服部<br/>
{$this->config['site_url']}<br/>
联系电话：+86  {$this->config['site_phone']}<br/>
客服邮箱: {$this->config['site_email']}

Eof;
                    $mail->setMail($mail_title , $mail_body); //设置邮件主题、内容
                    $mail->sendMail(); //发送

                }else{
                    exit(json_encode(array('error'=>'6','title'=>"抱歉，系统繁忙",'msg'=>'系统繁忙，请重试！')));
                }

            }

        }else{
            exit('deney Access !');
        }
    }

    public function secondset(){
        if(!empty($_SESSION['find_the_password_for_email'])){
            $this->assign('email',$_SESSION['find_the_password_for_email']);
        }
        $this->display();
    }

    public function seconde_check(){
        $_SESSION['rest_user_password_uid'] = null;
        if($this->isAjax()){
            $database_user = D('User');
            //邮箱
            $email['email'] = trim($_POST['email']);
            $email['find_pass_code'] = trim($_POST['find_pass_code']);
            $now_email = $database_user->field('`uid`,`find_pass_code`')->field(true)->where($email)->find();

            if(empty($now_email)){
                exit(json_encode(array('error'=>'4','msg'=>'邮箱或者校验码不对！')));
            }else{
                $_SESSION['rest_user_password_uid'] = $now_email['uid'];
                $restdata['find_pass_code'] = '';
                $restdata['uid'] = $now_email['uid'];
                $database_user->data($restdata)->save();
                exit(json_encode(array('error'=>'0','title'=>"恭喜,可以重置密码了",'msg'=>'马上跳转到设置新密码页面,请耐心等待~')));
            }

        }else{
            exit('deney Access !');
        }
    }

    public function thirdstep(){
        $this->display();
    }

    public function third_check(){
        if($this->isAjax()){
            $database_user = D('User');
            $resetpwd['pwd']        = trim($_POST['txtnewpwd']);
            $repeatpwd              = trim($_POST['repeatpwd']);
            if($resetpwd['pwd']     !=  $repeatpwd){
                exit(json_encode(array('error'=>'4','msg'=>'两次输入的密码不一致！')));
            }
            $resetpwd['email'] = $_SESSION['find_the_password_for_email'];
            $resetpwd['uid']   = $_SESSION['rest_user_password_uid'];
            $where['email'] =  $_SESSION['find_the_password_for_email'];
            $where['uid'] = $_SESSION['rest_user_password_uid'];
            $now_email = $database_user->field('`uid`')->field(true)->where($where)->find();

            if(empty($now_email)){
                exit(json_encode(array('error'=>'4','msg'=>'非法操作，系统已拒绝！')));
            }else{
                $_SESSION['rest_user_password_uid']      = '';
                $_SESSION['find_the_password_for_email'] = '';
                $database_user->data($resetpwd)->save();
                exit(json_encode(array('error'=>'0','title'=>"恭喜,可以使用新密码了",'msg'=>'您好，现在可以使用新密码登录系统了。')));
            }

        }else{
            exit('deney Access !');
        }

    }
	
	 public function weixin() {
        $weixin_state = md5(uniqid());
        $_SESSION['weixin']['state'] = $weixin_state;
        $_SESSION['weixin']['referer'] = !empty($_GET['referer']) ? htmlspecialchars_decode($_GET['referer']) : $this->config['site_url'];

        $return_url = $this->config['site_url'] . '/source/web_weixin_back.php';
        redirect('https://open.weixin.qq.com/connect/qrconnect?appid=' . $this->config['login_weixin_appid'] . '&redirect_uri=' . $return_url . '&response_type=code&scope=snsapi_login&state=' . $weixin_state . '#wechat_redirect');
    }

    public function weixin_back() {
        $referer = !empty($_SESSION['weixin']['referer']) ? $_SESSION['weixin']['referer'] : $this->config['site_url'];
        if (isset($_GET['code']) && isset($_GET['state']) && ($_GET['state'] == $_SESSION['weixin']['state'])) {
            unset($_SESSION['weixin']['state']);
            import('ORG.Net.Http');
            $http = new Http();
            $return = $http->curlGet('https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . $this->config['login_weixin_appid'] . '&secret=' . $this->config['login_weixin_appsecret'] . '&code=' . $_GET['code'] . '&grant_type=authorization_code');

            $jsonrt = json_decode($return, true);
            if ($jsonrt['errcode']) {
                $error_msg_class = new GetErrorMsg();
                $this->error('授权发生错误：' . $error_msg_class->wx_error_msg($jsonrt['errcode']), $this->config['site_url'] . '/index.php?c=Login&a=index');
            }

            $return = $http->curlGet('https://api.weixin.qq.com/sns/userinfo?access_token=' . $jsonrt['access_token'] . '&openid=' . $jsonrt['openid'] . '&lang=zh_CN');
            $jsonrt = json_decode($return, true);
            if ($jsonrt['errcode']) {
                $error_msg_class = new GetErrorMsg();
                $this->error('授权发生错误：' . $error_msg_class->wx_error_msg($jsonrt['errcode']), $this->config['site_url'] . '/index.php&c=Login&a=index');
            }

            /* 优先使用 unionid 登录 */
           /* if (!empty($jsonrt['unionid'])) {
                $this->autologin('union_id', $jsonrt['unionid'], $referer);
            }*/
            /* 再次使用 openid 登录 */
            $this->autologin('openid', $jsonrt['openid'], $referer);

            /* 注册用户 */
            $data_user = array(
                'web_openid' => $jsonrt['openid'],
                'union_id' => ($jsonrt['unionid'] ? $jsonrt['unionid'] : ''),
                'nickname' => $jsonrt['nickname'],
                'sex' => $jsonrt['sex'],
                'province' => $jsonrt['province'],
                'city' => $jsonrt['city'],
                'avatar' => $jsonrt['headimgurl'],
            );
            $_SESSION['weixin']['user'] = $data_user;
			file_put_contents('referer.log'. __LINE__ ,$referer);
			file_put_contents('data_user.log',print_r($data_user,true));
            $this->assign('referer', $referer);
            $this->display();
        } else {
            $this->error('访问异常！请重新登录。', $this->config['site_url'] . '/index.php?c=Login&a=index');
        }
    }

    public function weixin_bind() {
        if (empty($_SESSION['weixin']['user'])) {
            $this->error('微信授权失效，请重新登录！');
        }
        $login_result = D('User')->checkin($_POST['phone'], $_POST['pwd']);
        if ($login_result['error_code']) {
            exit(json_encode($login_result));
        } else {
            $now_user = $login_result['user'];
            $condition_user['uid'] = $now_user['uid'];
            $data_user['web_openid'] = $_SESSION['weixin']['user']['web_openid'];
            if ($_SESSION['weixin']['user']['union_id']) {
                $data_user['union_id'] = $_SESSION['weixin']['user']['union_id'];
            }
            if (empty($now_user['avatar'])) {
                $data_user['avatar'] = $_SESSION['weixin']['user']['avatar'];
            }
            if (empty($now_user['sex'])) {
                $data_user['sex'] = $_SESSION['weixin']['user']['sex'];
            }
            if (empty($now_user['province'])) {
                $data_user['province'] = $_SESSION['weixin']['user']['province'];
            }
            if (empty($now_user['city'])) {
                $data_user['city'] = $_SESSION['weixin']['user']['city'];
            }

            if (D('User')->where($condition_user)->data($data_user)->save()) {
                unset($_SESSION['weixin']);
                session('user', $now_user);
                setcookie('login_name', $now_user['phone'], $_SERVER['REQUEST_TIME'] + 10000000, '/');

                $this->success('登录成功！');
            } else {
                $this->error('绑定失败！请重试。');
            }
        }
    }

    public function weixin_nobind() {
        if (empty($_SESSION['weixin']['user'])) {
            $this->error('微信授权失效，请重新登录！');
        }
        $reg_result = D('User')->autoreg($_SESSION['weixin']['user']);
        if ($reg_result['error_code']) {
            $this->error($reg_result['msg']);
        } else {
            $login_result = D('User')->autologin('web_openid', $_SESSION['weixin']['user']['web_openid']);
            if ($login_result['error_code']) {
                $this->error($login_result['msg'], $this->config['site_url'] . '/index.php?c=Login&a=index');
            } else {
                $now_user = $login_result['user'];
                session('user', $now_user);
                $referer = !empty($_SESSION['weixin']['referer']) ? $_SESSION['weixin']['referer'] : $this->config['site_url'];

                unset($_SESSION['weixin']);
                $this->success('登录成功！', $referer);
                exit;
            }
        }
    }



    protected function autologin($field, $value, $referer) {
        $result = D('User')->autologin($field, $value);
        if (empty($result['error_code'])) {
            $now_user = $result['user'];
            session('user', $now_user);
            $this->success('登录成功！', $referer);
            exit;
        } else if ($result['error_code'] && $result['error_code'] != 1001) {
            $this->error($result['msg'], U('Login/index'));
        }
    }
	
	public function ajax_weixin_login(){
		for ($i = 0; $i < 6; $i++) {
            $database_login_qrcode = D('Login_qrcode');
            $condition_login_qrcode['id'] = $_GET['qrcode_id'];
			
            $now_qrcode = $database_login_qrcode->field('`uid`')->where($condition_login_qrcode)->find();
			file_put_contents('loginqrcode.log',print_r($now_qrcode,true));
            if (!empty($now_qrcode['uid'])) {
                if ($now_qrcode['uid'] == -1) {
                    $data_login_qrcode['uid'] = 0;
                    $database_login_qrcode->where($condition_login_qrcode)->data($data_login_qrcode)->save();
                    exit('reg_user');
                }
				
                $database_login_qrcode->where($condition_login_qrcode)->delete();
                $result = D('User')->autologin('uid', $now_qrcode['uid']);
                if (empty($result['error_code'])) {
                    session('user', $result['user']);
                    exit('true');
                } else if ($result['error_code'] == 1001) {
                    exit('no_user');
                } else if ($result['error_code']) {
                    exit('false');
                }
            }
            if ($i == 5) {
                exit('false');
            }
            sleep(3);
        }
	}




}