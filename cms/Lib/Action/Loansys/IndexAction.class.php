<?php
/*
 * 首页
 *
 */
class IndexAction extends BaseAction {
    public function index(){

		$Db_News          = M('News');
	    $Db_news_category = M('News_category');
		$news_cate_condition['cat_key'] = 'Loansys_help_li'; //我要理财
		$cate_list = $Db_news_category->field(true)->where($news_cate_condition)->find();

		$news_list_condition['cat_id'] = $cate_list['cat_id'];
	    $news_list_condition['state'] = 1;
	    $licai_list = $Db_News->field(true)->where($news_list_condition)->order('`add_time` DESC,`news_id` ASC ')
		    ->limit(3)
		    ->select();

	    $this->assign('cate_list',$cate_list);
		$this->assign('licai_list',$licai_list);


	    $Xnews_cate_condition['cat_key'] = 'Loansys_help_ji'; //我要借款
	    $Xcate_list = $Db_news_category->field(true)->where($Xnews_cate_condition)->find();

	    $Xnews_list_condition['cat_id'] = $Xcate_list['cat_id'];
	    $Xnews_list_condition['state'] = 1;
	    $jiekuanList = $Db_News->field(true)->where($Xnews_list_condition)->order('`add_time` DESC,`news_id` ASC ')
		    ->limit(3)
		    ->select();
	    //dump($news_list);
	    $this->assign('Xcate_list',$Xcate_list);
	    $this->assign('jiekuanList',$jiekuanList);


	    $ccj_cate_condition['cat_key'] = 'Loansys_help_cj'; //常见问题
	    $cj_list = $Db_news_category->field(true)->where($ccj_cate_condition)->find();

	    $cj_list_condition['cat_id'] = $cj_list['cat_id'];
	    $cj_list_condition['state'] = 1;
	    $changjianList = $Db_News->field(true)->where($cj_list_condition)->order('`add_time` DESC,`news_id` ASC ')
		    ->limit(3)
		    ->select();
	    //dump($news_list);
	    $this->assign('cj_list',$cj_list);
	    $this->assign('changjianList',$changjianList);


	    $notice_condition['cat_key'] = 'daikuan_notice';//公告
	    $notices = $Db_news_category->where($notice_condition)->find();

	    $notice_list_con['cat_id'] = $notices['cat_id'];
	    $notice_list = $Db_News->field(true)->where($notice_list_con)->order('`add_time` DESC,`news_id` ASC ')
		    ->limit(5)
		    ->select();

	    $this->assign('notices',$notices);
	    $this->assign('notice_list',$notice_list);


	    $this->display();
    }
    
    
    
	
	public function group_index_sort(){
		$group_id = $_POST['id'];
		$database_index_group_hits = D('Index_group_hits_'.substr(dechex($group_id),-1));
		$data_index_group_hits['group_id'] = $group_id;
		$data_index_group_hits['ip']		= get_client_ip();
		if(!$database_index_group_hits->field('`group_id`')->where($data_index_group_hits)->find()){
			$condition_group['group_id'] = $group_id;
			if(M('Group')->where($condition_group)->setDec('index_sort')){
				$data_index_group_hits['time'] = $_SERVER['REQUEST_TIME'];
				$database_index_group_hits->data($data_index_group_hits)->add();
			}
		}
	}
}