<?php

/**
 * Api Server Base
 * check install yar
 *
 */
class YbaseAction{


	/**
	 * 构架函数
	 * @access Public
	 */
	public function __construct(){
		//判断扩展是否存在
		if(!extension_loaded('yar')){
           return $this->yerror('ERR_YAR');
		}
		//实例化 Yar_Server
//		$server = new Yaf_Server();
//		$server->handle();
	}

	protected function verifySign(){
		$request  = $_REQUEST;
		//Only valid in 30 seconds
		if(CUR_TIMESTAMP - $request['time'] > 30){
			$rep['code']  = 1001;
			$rep['error'] = 'error sign';

			return json_encode($rep);
		}

		$newSign = self::generateSign($i);

		if(strtolower($newSign) != $sign){
			$rep['code']  = 1001;
			$rep['error'] = 'error sign';

			return json_encode($rep);
		}
	}

	protected static function generateSign($parameters){
		$signPars = '';
		foreach($parameters as $k => $v) {
			if(isset($v) && 'sign' != $k) {
				$signPars .= $k . '=' . $v . '&';
			}
		}

		$signPars .= 'key='.lsfjdsfjdslfjsdl213;
		return strtolower(md5($signPars));
	}




	protected function yresponse($data){
		return json_encode($data, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * @param $param array
	 * @return json
 	 */
	public function index($param){
		if(is_array($param)){
			$param =array_merge($param,array('a'=>111,'b'=>222,'c'=>3333,'d'=>4444,'method'=> __FUNCTION__ ,'class'=>__CLASS__ ));
			return json_encode($param);
		}else{
			return json_encode(array('error'=>1));
		}

	}

	public function ytest($param){
		if(is_array($param)){
			$param =array_merge($param,array('a'=>111,'b'=>222,'c'=>3333,'d'=>4444,'method'=> __FUNCTION__ ,'class'=>__CLASS__ ));
			return json_encode($param);
		}else{
			return json_encode(array('error'=>1));
		}

	}

	/**
	 * Response
	 *
	 * @param string $format : json, xml, jsonp, string
	 * @param array $data:
	 * @param boolean $die: die if set to true, default is true
	 */
	public static function response($data, $format = 'json', $die = TRUE) {
		switch($format){
			default:
			case 'json':
//				$file = FUNC_PATH.'/F_String.php';
//				Yaf_Loader::import($file);
				if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && strtolower($_SERVER["HTTP_X_REQUESTED_WITH"])=="xmlhttprequest"){
					$data = JSON($data);
				}else if(isset($_REQUEST['ajax'])){
					$data = JSON($data);
				}else{
					//dump($data); die; // URL 测试打印数组出来
					echo json_encode($data); die;
				}
				break;

			case 'jsonp':
				$data = $_GET['jsoncallback'] .'('. json_encode($data) .')';
				break;

			case 'string':
				break;
		}

		echo $data;

		if($die){
			die;
		}
	}

	protected function yerror($error){
		switch($error){
			case 'ERR_YAR':
				$respon['code'] = 600000;
				$respon['error'] ='not install Yar';
				break;
			case 'ERR_NO_DATA':
				$respon['code'] = 600001;
				$respon['error'] = 'no data';
				break;
			case 'ERR_UNKONWN':
				$respon['code'] = 600002;
				$respon['error'] = 'unknown error';
				break;
			case 'ERR_DUPLICATED':
				$respon['code']  = 600003;
				$respon['error'] = 'operation duplicated';
				break;

			case 'ERR_FAIL_RES_UPDATE':
				$respon['code'] = 600004;
				$respon['error'] = 'failed to update';
				break;

			case 'ERR_FAIL_UPLOAD_TO_TMP':
				$respon['code'] = 600005;
				$respon['error'] = 'failed to upload to tmp';
				break;

			case 'ERR_NO_FILE_RECEIVED':
				$respon['code'] = 600006;
				$respon['error'] = 'empty file received';
				break;
			case 'ERR_MISSING':
				$respon['code'] = 600007;
				$respon['error'] ='missing parameters';
				break;

		}
		return json_encode($respon);
	}

	/**
	 * 魔术方法 有不存在的操作的时候执行
	 * @access Public
	 * @param string $method 方法名
	 * @param array $args 参数
	 * @return mixed
	 */
	public function __call($method,$args){}
}


$service = new Yar_Server(new YbaseAction());

$service->handle();
