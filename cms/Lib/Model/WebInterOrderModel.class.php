<?php
class WebInterOrderModel extends Model {
    protected $tableName = 'web_inter_order';
    
    // 自动验证设置
    protected $_validate = array(
        array('inter_obj', 'require', '发布方类型必填！'),//1为必须验证
        array('domain','require', '域名必填！'),//1为必须验证
        array('price','0,50000000','价格输入有误',1,'between'),
        array('ip_pc','0,500000000','PC端日IP输入有误',1,'between'),
        array('ip_app','0,50000000','移动日IP输入有误',1,'between'),
        array('pr','0,500000000','PR值输入有误',1,'between'),
        array('month_income','0,50000000','月收入输入有误',1,'between'),
        array('description', 'require', '描述必填'),
    );
}

?>