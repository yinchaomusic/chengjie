<?php
class LevelModel extends Model
{
	/**
	 * @param int $level_count 积分数
	 */
	public function user_Level($level_count)
	{
            $databases_user_level = M('User_level');
            $user_level_list = $databases_user_level->select();
            foreach ($user_level_list as $k=>$v){
                if($level_count>=$v['integral']){
                    $level_info['lname'] = $v['lname'];
                    $level_info['boon'] = $v['boon'];
                }
            }
            return $level_info;
        }
}