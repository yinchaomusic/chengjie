<?php
class WebEntrustModel extends Model {
    protected $tableName = 'web_entrust';
    
    // 自动验证设置
     protected $_validate = array(
        array('entr_type', 'require', '交易类型必填！'),//1为必须验证
        array('domain','require', '域名必填！'),//1为必须验证
         array('domain','','域名已经存在！',0,'unique',1), // 在新增的时候验证name字段是否唯一
        array('price','0,50000000','价格输入有误',1,'between'),
         array('pc_baidu','0,10000','PC端百度权重输入有误',1,'between'),
         array('app_baidu','0,10000','APP端百度权重输入有误',1,'between'),
         array('pc_ip','0,500000000','PC端日IP输入有误',1,'between'),
         array('app_ip','0,50000000','移动日IP输入有误',1,'between'),
         array('pr','0,500000000','PR值输入有误',1,'between'),
         array('month_income','0,50000000','月收入输入有误',1,'between'),
        array('description', 'require', '描述必填'),
    );
   
}

?>